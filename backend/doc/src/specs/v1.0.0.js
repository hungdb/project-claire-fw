// ======================= SERVER SETTINGS =============================
/**
 * @api {CONFIG} web-server-setting
 * @apiGroup README
 * @apiDescription
 * This is an template api doc for RPC-style. You should only change the actions, not endpoints.
 * If OAuth2 is implemented, every request to endpoint (/rpc, /batch) must include Authorization token in its header.
 * Some example server settings are listed here in an "API block" for purpose of version tracking.
 * @apiVersion 1.0.0
 * @apiParam {String} Access-Control-Allow-Origin \*
 * @apiParam {String} Access-Control-Allow-Methods GET, POST, OPTIONS
 * @apiParam {String} Request-Limit 10 MB
 * @apiParam {String} Request-Timeout 300 s
 */

/**
 * @apiDefine AuthorizationHeader
 * @apiHeader (Request headers) {String} Authorization Users unique access-key
 */

/**
 * @apiDefine LoggedInUser Permission required
 * User must possess the API token to execute this action
 */

/**
 * @apiDefine AdminOnly Admin role required
 * User must login as admin to perform this action
 */

// ======================= ENDPOINTS =============================
/**
 * @api {POST} /rpc RPC
 * @apiName rpc
 * @apiGroup Endpoints
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * The endpoint for single action request
 *
 * @apiParam {String} action Name of the action
 * @apiParam {Object} params Params for the action to be executed
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} data
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} data
 * @apiError {String} error.code The code of error that occurred, depends on which action had been executed. Common errors are:
 * - REQUEST_MALFORMED: the rpc request is malformed
 * - ACTION_NOT_FOUND: the provided action name is incorrect
 * - INVALID_PARAM_PROVIDED: the provided param is invalid
 * - INTERNAL_SERVER_ERROR: the server failed to execute the action
 */


// ======================= ACTIONS =============================
/**
 * @api {ACTION} create_role_permission Create new permission for specific role
 * @apiName create_role_permission
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Add a permission for specific role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} roleId <br><code>required</code>
 * @apiParam {Number} permissionId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *  "action": "create_role_permission",
 *  "roleId": 2,
 *	"permissionId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Id of Role Permission
 * @apiSuccess {Number} result.roleId Id of given role
 * @apiSuccess {String} result.permissionId Id of permission
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - ACCESS_DENIED: authorization token problems
 * - NOT_FOUND_ERROR: The role with roleId is not existed
 * - DUPLICATED_ERROR: The role permission is existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 78,
 *      "roleId": 1,
 *      "permissionId": 1
 *  }
 * }
 */

/**
 * @api {ACTION} init_role_permission Initial Role Permission Default for system
 * @apiName init_role_permission
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Initial Role Permission
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "init_role_permission"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true
 * }
 */

/**
 * @api {ACTION} remove_role_permission Remove permission for the given role
 * @apiName remove_role_permission
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Remove a permission for specific role by roleId
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} rolePermissionId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_rol_permission",
 *     "rolePermissionId": 2
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the roleId, permission_id does not comply with the policy
 * - NOT_FOUND_ERROR: the RolePermission is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */

/**
 * @api {ACTION} list_all_role_permission List all permissions
 * @apiName list_all_role_permission
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all permissions
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_all_role_permission"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.Id
 * @apiSuccess {Number} result.roleId
 * @apiSuccess {String} result.permissionId
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the roleId, permission_id does not comply with the policy
 * - NOT_FOUND_ERROR: the role with roleId is not existed
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 1,
 *          "roleId": 1,
 *          "permissionId": 1
 *      },
 *      {
 *          "id": 2,
 *          "roleId": 1,
 *          "permissionId": 2
 *      },
 *      {
 *          "id": 3,
 *          "roleId": 1,
 *          "permissionId": 3
 *      },
 *      {
 *          "id": 4,
 *          "roleId": 1,
 *          "permissionId": 4
 *      },
 *      {
 *          "id": 5,
 *          "roleId": 1,
 *          "permissionId": 5
 *      }
 *  ]
 * }
 */

/**
 * @api {ACTION} update_role_permission Update the existing role permission
 * @apiName update_role_permission
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the content of the role permission by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} rolePermissionId Id of the role permission <br><code>required</code>
 * @apiParam {Number} roleId Id of role will be updated <br><code>required</code>
 * @apiParam {String} permissionId Id of the permission <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_role_permission",
 *     "rolePermissionId": 1,
 *     "roleId" : 1,
 *     "permissionId": 2
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId or name does not comply with the policy
 * - NOT_FOUND_ERROR: the role permission or role does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} list_permission_of_role List all permissions of specific role
 * @apiName list_permission_of_role
 * @apiGroup RolePermission
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all permissions of specific role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_permission_of_role",
 *     "roleId": 2
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id
 * @apiSuccess {String} result.name
 * @apiSuccess {Boolean} result.isSuperUser
 * @apiSuccess {Boolean} result.isEditable
 * @apiSuccess {Object[]} result.permissions
 * @apiSuccess {Number} result.permissions.id
 * @apiSuccess {Number} result.permissions.permissionId
 * @apiSuccess {Object} result.permissions.Permission
 * @apiSuccess {Number} result.permissions.Permission.id
 * @apiSuccess {String} result.permissions.Permission.name
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the roleId does not comply with the policy
 * - NOT_FOUND_ERROR: the role with roleId is not existed
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 2,
 *      "name": "unauthenticated",
 *      "isSuperUser": false,
 *      "isEditable": true,
 *      "permissions": [
 *          {
 *              "id": 81,
 *              "permissionId": 81,
 *              "Permission": {
 *                  "id": 81,
 *                  "name": "create_oauth_request"
 *              }
 *          },
 *          {
 *              "id": 82,
 *              "permissionId": 82,
 *              "Permission": {
 *                  "id": 82,
 *                  "name": "get_oauth_token"
 *              }
 *          },
 *          {
 *              "id": 83,
 *              "permissionId": 83,
 *              "Permission": {
 *                  "id": 83,
 *                  "name": "grant_owned_oauth_request"
 *              }
 *          }
 *      ]
 *  }
 * }
 */


/**
 * @api {ACTION} list_roles Get the list of roles
 * @apiName list_roles
 * @apiGroup Role
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_roles"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of group
 * @apiSuccess {String} result.name The name of group
 * @apiSuccess {Boolean} result.isSuperUser
 * @apiSuccess {Boolean} result.isEditable
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - TOKEN_EXPIRED: the token is expired
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 1,
 *          "name": "super_admin",
 *          "isSuperUser": false,
 *          "isEditable": true
 *      },
 *      {
 *          "id": 2,
 *          "name": "unauthenticated",
 *          "isSuperUser": false,
 *          "isEditable": true
 *      }
 *  ]
 *}
 *
 */

/**
 * @api {ACTION} create_role Create new role
 * @apiName create_role
 * @apiGroup Role
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the role <br><code>required</code>
 * @apiParam {Boolean}  super_user
 * @apiParam {Boolean} editable
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_role",
 *     "name": "normal_user",
 *     "super_user": false,
 *     "editable": false
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the group
 * @apiSuccess {String} result.name Name of the group
 * @apiSuccess {Boolean} result.isSuperUser
 * @apiSuccess {Boolean} result.isEditable
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "isSuperUser": false,
 *      "isEditable": true,
 *      "id": 4,
 *      "name": "normal_user"
 *  }
 *}
 */

/**
 * @api {ACTION} update_role Update the existing role
 * @apiName update_role
 * @apiGroup Role
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the role by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} roleId Id of role will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the role <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_role",
 *     "roleId": 1,
 *     "name": "normal_user"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of group is duplicated
 * - NOT_FOUND_ERROR: the group does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_role Delete specific role
 * @apiName remove_role
 * @apiGroup Role
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific role from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} roleId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_role",
 *     "roleId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId does not comply with the policy
 * - NOT_FOUND_ERROR: the group is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */

/**
 * @api {ACTION} get_role Get information specific role
 * @apiName get_role
 * @apiGroup Role
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get information of the specific role from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} roleId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_role",
 *     "roleId": 2
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id The id of the role
 * @apiSuccess {String} result.name The name of the role
 * @apiSuccess {Boolean} result.isSuperUser
 * @apiSuccess {Boolean} result.isEditable
 * @apiSuccess {Object[]} result.permission
 * @apiSuccess {Number} result.permission.id The Id of permission
 * @apiSuccess {String} result.permission.permission The name of permission
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId does not comply with the policy
 * - NOT_FOUND_ERROR: the group is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 2,
 *      "name": "unauthenticated",
 *      "isSuperUser": false,
 *      "isEditable": true,
 *      "permissions": [
 *          {
 *              "id": 74,
 *              "permission": "create_oauth_request"
 *          },
 *          {
 *              "id": 75,
 *              "permission": "get_oauth_token"
 *          },
 *          {
 *              "id": 76,
 *              "permission": "grant_owned_oauth_request"
 *          }
 *      ]
 *  }
 * }
 */


/**
 * @api {ACTION} list_project_role Get the list of project role
 * @apiName list_project_role
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the project role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_project_role"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of project role
 * @apiSuccess {Number} result.name The name of project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 6,
 *          "name": "Comtor"
 *      },
 *      {
 *          "id": 3,
 *          "name": "Developer"
 *      },
 *      {
 *          "id": 2,
 *          "name": "Leader"
 *      },
 *      {
 *          "id": 1,
 *          "name": "Owner"
 *      },
 *      {
 *          "id": 5,
 *          "name": "Project Manager"
 *      },
 *      {
 *          "id": 4,
 *          "name": "Tester"
 *      }
 *  ]
 *}
 */

/**
 * @api {ACTION} create_project_role Create new project role
 * @apiName create_project_role
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Project Role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the project role <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_project_role",
 *     "name": "Comtor"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the project role
 * @apiSuccess {String} result.name Name of the project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 6,
 *      "name": "Comtor"
 *  }
 * }
 */

/**
 * @api {ACTION} update_project_role Update the existing project role
 * @apiName update_project_role
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the project role by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectRoleId Id of Project role will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the project role <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_project_role",
 *     "projectRoleId": 1,
 *     "name": "Owner"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of project role is duplicated
 * - NOT_FOUND_ERROR: the project role does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_project_role Delete specific project role
 * @apiName remove_project_role
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific project role from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectRoleId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_project_role",
 *     "projectRoleId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId does not comply with the policy
 * - NOT_FOUND_ERROR: the group is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */

/**
 * @api {ACTION} add_user_into_project Add user into project with role
 * @apiName add_user_into_project
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Add user into project with role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId Id of the project <br><code>required</code>
 * @apiParam {Number} userId Id of the user <br><code>required</code>
 * @apiParam {Number} projectRoleId Id of the project role <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 * "action":"add_user_into_project",
 * "projectId": 1,
 * "userId": 8,
 * "projectRoleId": 3
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the record user in project with role
 * @apiSuccess {Number} result.projectId ID of the project
 * @apiSuccess {Number} result.userId ID of the user
 * @apiSuccess {Number} result.projectRoleId ID of the project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the user and project is duplicated
 * - NOT_FOUND_ERROR: the user or project or project role isn't founded
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *       "id": 11,
 *       "projectId": 1,
 *       "userId": 8,
 *       "projectRoleId": 3
 *   }
 * }
 */

/**
 * @api {ACTION} list_users_by_project Get the list users of project
 * @apiName list_users_by_project
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list users of the project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId Id of the project <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_users_by_project",
 *     "projectId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The ID of user project role
 * @apiSuccess {Number} result.userId The ID of user
 * @apiSuccess {Number} result.projectRoleId The ID of project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: project id is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *       {
 *           "id": 1,
 *           "userId": 1,
 *           "projectRoleId": 1,
 *           "User": {
 *               "id": 1,
 *               "token": "{\"token\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJncmFudFR5cGUiOiJjb2RlIiwiZGVsZWdhdGUiOjYsIm93bmVyIjoxLCJyZXF1ZXN0Ijo1NjAsImlhdCI6MTU0MTY0MTA0MiwiZXhwIjoxNTQxNjYyNjQyfQ.oW4u_VbVPziLDmzYWnyA9Yy1IMxRDAhlqDUXEBlocT7Eft9JVdaTGa73WyUxDnvQw6mLlM1Qb_JLAefGsy5BOunN9X7mY89UVa0F0VZ762pVGibbgg2wfRQUcSmH5f5nQ_moYH3TBQcPnbXUHc-vZKo2ANZeFEk-pjmXMYZEvw4GJbBfx7GDqQko7fBDRWUlwlowOQpndlPYdGaMrDHRiVtC0q8vx8syhosR-B5mAogKgRLzg784K9e9FtnPALzO0lF8YZvk-xe1x7dh5HqOBnluCq02oMCGtG4wyYhGknU69ENPJOWo2aHhy7xEbaOvlWFE4J2lOEd8aMTqXNFizw\",\"tokenExpiration\":1541662642066,\"refreshToken\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjozMTg1LCJpYXQiOjE1NDE2NDEwNDIsImV4cCI6MTU0MTY4NDI0Mn0.d8ukaIWjb1ELxJYPPHbdG92a2FfWred7VcREVid68SHli4RsiaqDKd1QPl-bulr2yXrK2VUkZpk0dMT5fVODw8_czV7C6xonU-9_icSfJwyRlG-41VnBIQqup9bSm3lg4z14YWjo_hrvc8G22MBk6WbMhJMYJlXonQGCU9tIHTJrfyldJjEDe1hFmSQtj7WeizFaJzTuBalWwsTVs3hCbCljCXIyUuL5aIjft6kEMRbNMZuybwB2NMPVVIai6fRm8aNUzMVj0xa4iHjbiDjw00_qlnBEyy5kUpYPvBSiBILl03LN9Qbm8qxtgLBez0SBNgKzzcl9HWUdn_gaHQtShA\"}",
 *               "name": "admin",
 *               "avatar": "dev-identity-bap-jp/profile-photos/1"
 *           },
 *           "ProjectRole": {
 *               "id": 1,
 *               "name": "Owner"
 *           }
 *       },
 *       {
 *           "id": 2,
 *           "userId": 2,
 *           "projectRoleId": 2,
 *           "User": {
 *               "id": 2,
 *               "token": null,
 *               "name": null,
 *               "avatar": null
 *           },
 *           "ProjectRole": {
 *               "id": 2,
 *               "name": "Leader"
 *           }
 *       },
 *  ]
 *}
 */

/**
 * @api {ACTION} update_role_user_in_project Update the role of existing user in project
 * @apiName update_role_user_in_project
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the role of existing user in project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userProjectRoleId Id of User Project Role will be updated <br><code>required</code>
 * @apiParam {Number} projectRoleId Id of the project role <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_role_user_in_project",
 *     "userProjectRoleId": 1,
 *     "projectRoleId": 2
 *
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId or name does not comply with the policy
 * - NOT_FOUND_ERROR: the user project role does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_user_from_project Delete specific user project role
 * @apiName remove_user_from_project
 * @apiGroup ProjectRole
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific project role from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectRoleId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_user_from_project",
 *     "projectId": 1,
 *     "userId": 4
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId does not comply with the policy
 * - NOT_FOUND_ERROR: the user in project is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */



/**
 * @api {ACTION} list_groups Get the list of group
 * @apiName list_groups
 * @apiGroup Group
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the group
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_groups"
 * }
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of group
 * @apiSuccess {Number} result.name The name of group
 *
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "name": "groupA"
 *          }
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} create_group Create new group
 * @apiName create_group
 * @apiGroup Group
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Group
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the group <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_group",
 *     "name": "Group 1"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the group
 * @apiSuccess {String} result.name Name of the group
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
        "result": {
            "id": 1,
            "name": "Group 1"
        }
 * }
 *
 */

/**
 * @api {ACTION} update_group Update the existing group
 * @apiName update_group
 * @apiGroup Group
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the group by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} groupId Id of Group will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the group <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_group",
 *     "groupId": 1,
 *     "name": "Group A"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of group is duplicated
 * - NOT_FOUND_ERROR: the group does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_group Delete specific group
 * @apiName remove_group
 * @apiGroup Group
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific group from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} groupId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_group",
 *     "groupId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the groupId does not comply with the policy
 * - NOT_FOUND_ERROR: the group is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */

/**
 * @api {ACTION} get_group Get information of specific group
 * @apiName get_group
 * @apiGroup Group
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the group
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} groupId
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_group",
 *     "groupId": 1
 * }
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id The Id of group
 * @apiSuccess {Number} result.name The name of group
 * @apiSuccess {Object[]} result.projects list project
 * @apiSuccess {Number} result.projects.id The Id of the project
 * @apiSuccess {String} result.projects.name The name of the project
 * @apiSuccess {String} result.projects.description The description of the project
 * @apiSuccess {Boolean} result.projects.isNotificationEnable Notification of project
 * @apiSuccess {Boolean} result.projects.isActive The status of the project
 * @apiSuccess {Number} result.projects.groupId The id of the group
 * @apiSuccess {Number} result.projects.permissionSchemeId The id of the permissionScheme
 * @apiSuccess {Number} result.projects.workflowSchemeId The id of the workflow
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: group not found
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 1,
 *      "name": "Group 1",
 *      "projects": [
 *          {
 *              "id": 1,
 *              "groupId": 1,
 *              "name": "Project 1",
 *              "description": "abc",
 *              "isActive": true,
 *              "isNotificationEnable": false,
 *              "permissionSchemeId": 1,
 *              "workflowSchemeId": 1
 *          }
 *      ]
 *  }
 * }
 *
 */


/**
 * @api {ACTION} list_projects Get the list of project
 * @apiName list_projects
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_projects"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of the project
 * @apiSuccess {String} result.name The name of the project
 * @apiSuccess {String} result.description The description of the project
 * @apiSuccess {Boolean} result.isActive The status of project
 * @apiSuccess {Boolean} result.isNotificationEnable Notification of project
 * @apiSuccess {Number} result.groupId The id of the group
 * @apiSuccess {Object} result.Group
 * @apiSuccess {Number} result.Group.id
 * @apiSuccess {String} result.Group.name
 * @apiSuccess {Number} result.permissionSchemeId The id of the permission scheme
 * @apiSuccess {Object} result.PermissionScheme
 * @apiSuccess {Number} result.PermissionScheme.id
 * @apiSuccess {String} result.PermissionScheme.name
 * @apiSuccess {Number} result.workflowId The id of the workflow
 * @apiSuccess {Object} result.Workflow
 * @apiSuccess {Number} result.Workflow.id
 * @apiSuccess {String} result.Workflow.name
 * @apiSuccess {Object[]} result.users The list of user belonged to the project
 * @apiSuccess {Number} result.users.userId The id of user
 * @apiSuccess {Object} result.users.ProjectRole  Project Role of user
 * @apiSuccess {Number} result.users.ProjectRole.id Id of project Role
 * @apiSuccess {String} result.users.ProjectRole.name Name of project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 1,
 *          "groupId": 1,
 *          "name": "Project 1",
 *          "description": "abc",
 *          "isActive": true,
 *          "isNotificationEnable": false,
 *          "permissionSchemeId": 1,
 *          "workflowSchemeId": 1,
 *          "Group": {
 *              "id": 1,
 *              "name": "Group 1"
 *          },
 *          "Workflow": {
 *              "id": 1,
 *              "name": "Workflow 1"
 *          },
 *          "PermissionScheme": {
 *              "id": 1,
 *              "name": "PermissionScheme 1"
 *          },
 *          "environments": [],
 *          "users": [
 *              {
 *                  "userId": 1,
 *                  "ProjectRole": {
 *                      "id": 1,
 *                      "name": "Owner"
 *                  }
 *              },
 *              {
 *                  "userId": 3,
 *                  "ProjectRole": {
 *                      "id": 2,
 *                      "name": "Leader"
 *                  }
 *              }
 *          ]
 *      }
 *  ]
 * }
 *
 */

/**
 * @api {ACTION} create_project Create new project
 * @apiName create_project
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} name Name of the project <br><code>required</code>
 * @apiParam {String} description The description of project <br><code>required</code>
 * @apiParam {Boolean} isActive The status of project
 * @apiParam {Boolean} isNotificationEnable Notification of project
 * @apiParam {Number} groupId The id of group <br><code>required</code>
 * @apiParam {Number} permissionSchemeId The id of permission scheme <br><code>required</code>
 * @apiParam {Number} workflowSchemeId The id of workflow <br><code>required</code>
 * @apiParam {Object[]} users List id of user in project
 * @apiParam {Number} users.userId Id of user
 * @apiParam {Number} users.projectRoleId Project role of user
 *
 * @apiParamExample {json} Param example:
 * {
 *    "action": "create_project",
 *    "name": "Project 1",
 *    "isNotificationEnable": true,
 *    "isActive": true,
 *    "description": "abc",
 *    "users": [
 *      {
 *        "userId":3,
 *        "projectRoleId": 2
 *      }
 *    ],
 *    "groupId": 1,
 *    "permissionSchemeId": 1,
 *    "workflowSchemeId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id The Id of the project
 * @apiSuccess {String} result.name The name of the project
 * @apiSuccess {String} result.description The description of the project
 * @apiSuccess {Boolean} result.isNotificationEnable Notification of project
 * @apiSuccess {Boolean} result.isActive The status of the project
 * @apiSuccess {Number} result.groupId The id of the group
 * @apiSuccess {Number} result.permissionSchemeId The id of the permissionScheme
 * @apiSuccess {Number} result.workflowSchemeId The id of the workflow
 * @apiSuccess {Object[]} result.users The list of users in project
 * @apiSuccess {Number} result.users.id
 * @apiSuccess {Number} result.users.projectId The id of the project
 * @apiSuccess {Number} result.users.userId The id of the user
 * @apiSuccess {Object} result.users.ProjectRole
 * @apiSuccess {Number} result.users.ProjectRole.id Id of project role
 * @apiSuccess {String} result.users.ProjectRole.name Name of project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 * - NOT_FOUND_ERROR: the group or workflow or permissionScheme not existed
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "isActive": true,
 *      "isNotificationEnable": false,
 *      "id": 1,
 *      "name": "Project 1",
 *      "description": "abc",
 *      "permissionSchemeId": 1,
 *      "workflowSchemeId": 1,
 *      "groupId": 1,
 *      "users": [
 *          {
 *              "userId": 1,
 *              "projectRoleId": 1,
 *              "ProjectRole": {
 *                  "id": 1,
 *                  "name": "Owner"
 *              }
 *          },
 *          {
 *              "userId": 3,
 *              "projectRoleId": 2,
 *              "ProjectRole": {
 *                  "id": 2,
 *                  "name": "Leader"
 *              }
 *          }
 *      ]
 *  }
 * }
 */

/**
 * @api {ACTION} update_project Update the existing project
 * @apiName update_project
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the information of the project by Id
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId Id of Project will be updated <br><code>required</code>
 * @apiParam {String} name Name of the project <br><code>required</code>
 * @apiParam {String} description The description of project <br><code>required</code>
 * @apiParam {Boolean} isActive The status of project
 * @apiParam {Boolean} isNotificationEnable Notification of project
 * @apiParam {Number} groupId The id of group <br><code>required</code>
 * @apiParam {Number} permissionSchemeId The id of permission scheme <br><code>required</code>
 * @apiParam {Number} workflowSchemeId The id of workflow <br><code>required</code>
 * @apiParam {Object[]} users List id of user in project
 * @apiParam {Number} users.userId Id of user
 * @apiParam {Number} users.projectRoleId Project role of user
 *
 * @apiParamExample {json} Param example:
 * {
 *    "action": "update_project",
 *    "projectId": 1,
 *    "name": "Project 1",
 *    "isNotificationEnable": true,
 *    "isActive": true,
 *    "description": "abc",
 *    "users": [
 *      {
 *        "userId":3,
 *        "projectRoleId": 2
 *      }
 *    ],
 *    "groupId": 1,
 *    "permissionSchemeId": 1,
 *    "workflowSchemeId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the projectId does not comply with the policy
 * - DUPLICATED_ERROR: the name of project is duplicated
 * - NOT_FOUND_ERROR: the project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 */

/**
 * @api {ACTION} remove_project Delete specific project
 * @apiName remove_project
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific project from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId  <br><code>required</code>
 *
 * @apiParam {json} Param example
 * {
 *     "action": "remove_project",
 *     "projectId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the projectId does not comply with the policy
 * - NOT_FOUND_ERROR: the project is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} list_project_by_userId Get the list of project of specific user
 * @apiName list_project_by_userId
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the project of specific user
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId <code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_project_by_userId",
 *     "userId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id The Id of the user
 * @apiSuccess {Object[]} result.projects List project
 * @apiSuccess {Number} result.projects.id Id of userProjectRole
 * @apiSuccess {Object} result.projects.Project Project information
 * @apiSuccess {String} result.projects.Project.description The description of the project
 * @apiSuccess {Boolean} result.projects.Project.isActive The status of project
 * @apiSuccess {Boolean} result.projects.Project.isNotificationEnable Notification of project
 * @apiSuccess {Number} result.projects.Project.groupId The id of the group
 * @apiSuccess {Number} result.projects.Project.permissionSchemeId The id of the permissionScheme
 * @apiSuccess {Number} result.projects.Project.workflowSchemeId The id of the workflow
 * @apiSuccess {Object} result.projects.ProjectRole  Project Role of user
 * @apiSuccess {Number} result.projects.ProjectRole.id Id of project Role
 * @apiSuccess {String} result.projects.ProjectRole.name Name of project role
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 *  {
 *  "success": true,
 *  "result": {
 *      "id": 1,
 *      "projects": [
 *          {
 *              "id": 1,
 *              "Project": {
 *                  "id": 1,
 *                  "groupId": 1,
 *                  "name": "Project 1",
 *                  "description": "abc",
 *                  "isActive": true,
 *                  "isNotificationEnable": false,
 *                  "permissionSchemeId": 1,
 *                  "workflowSchemeId": 1
 *              },
 *              "ProjectRole": {
 *                  "id": 1,
 *                  "name": "Owner"
 *              }
 *          }
 *      ]
 *  }
 * }
 */


/**
 * @api {ACTION} list_workflow 1-Get the list of workflow
 * @apiName list_workflow
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the workflow
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_workflow"
 * }
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of workflow
 * @apiSuccess {String} result.name The name of workflow
 *
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "name": "Workflow 1"
 *          }
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} create_workflow 1-Create new workflow
 * @apiName create_workflow
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Workflow
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the workflow <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_workflow",
 *     "name": "Workflow 1"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the workflow
 * @apiSuccess {String} result.name Name of the workflow
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of workflow is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
        "result": {
            "id": 1,
            "name": "Workflow 1"
        }
 * }
 *
 */

/**
 * @api {ACTION} update_workflow 1-Update the existing workflow
 * @apiName update_workflow
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the workflow by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowId Id of Workflow will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the workflow <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_workflow",
 *     "workflowId": 1,
 *     "name": "Workflow A"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of workflow is duplicated
 * - NOT_FOUND_ERROR: the workflow does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_workflow 1-Delete specific workflow
 * @apiName remove_workflow
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific workflow from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_workflow",
 *     "workflowId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowId does not comply with the policy
 * - NOT_FOUND_ERROR: the workflow is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} get_workflow 1-Get information of the specific workflow
 * @apiName get_workflow
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get information the specific workflow from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_workflow",
 *     "workflowId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the workflow
 * @apiSuccess {String} result.name Name of the workflow
 * @apiSuccess {Object[]} result.detail
 * @apiSuccess {Number} result.detail.id
 * @apiSuccess {Object} result.detail.fromStates
 * @apiSuccess {Number} result.detail.fromStates.id
 * @apiSuccess {String} result.detail.fromStates.name
 * @apiSuccess {String} result.detail.fromStates.effortType
 * @apiSuccess {Object} result.detail.toStates
 * @apiSuccess {Number} result.detail.toStates.id
 * @apiSuccess {String} result.detail.toStates.name
 * @apiSuccess {String} result.detail.toStates.effortType
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowId does not comply with the policy
 * - NOT_FOUND_ERROR: the workflow is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 1,
 *      "name": "Workflow 1",
 *      "detail": [
 *          {
 *              "id": 1,
 *              "fromStates": {
 *                  "id": 1,
 *                  "name": "ToDo",
 *                  "effortType": "untouched"
 *              },
 *              "toStates": {
 *                  "id": 2,
 *                  "name": "In Progress",
 *                  "effortType": "progress"
 *              }
 *          },
 *          {
 *              "id": 2,
 *              "fromStates": {
 *                  "id": 2,
 *                  "name": "In Progress",
 *                  "effortType": "progress"
 *              },
 *              "toStates": {
 *                  "id": 3,
 *                  "name": "Done",
 *                  "effortType": "holding"
 *              }
 *          },
 *          {
 *              "id": 3,
 *              "fromStates": {
 *                  "id": 3,
 *                  "name": "Done",
 *                  "effortType": "holding"
 *              },
 *              "toStates": {
 *                  "id": 4,
 *                  "name": "Testing",
 *                  "effortType": "progress"
 *              }
 *          },
 *          {
 *              "id": 4,
 *              "fromStates": {
 *                  "id": 4,
 *                  "name": "Testing",
 *                  "effortType": "progress"
 *              },
 *              "toStates": {
 *                  "id": 5,
 *                  "name": "Close",
 *                  "effortType": "terminated"
 *              }
 *          }
 *      ]
 *  }
 * }
 */


/**
 * @api {ACTION} list_workflow_state_by_workflowId 2-Get the list of workflow state
 * @apiName list_workflow_state_by_workflowId
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the workflow state of workflow
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowId Id of the workflow <br><code>required</code>
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_workflow_state_by_workflowId",
 *     "workflowId": 1
 * }
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of workflow state
 * @apiSuccess {String} result.name The name of workflow state
 * @apiSuccess {Number} result.effortType The effortType of workflow state
 * @apiSuccess {Number} result.workflowId The id of workflow
 * @apiSuccess {Number} result.ordinalNumber The ordinalNumber of workflow state in workflow
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 1,
 *          "name": "To Do",
 *          "effortType": "untouched",
 *          "workflowId": 1,
 *          "ordinalNumber": 1
 *      },
 *      {
 *          "id": 2,
 *          "name": "In Progress",
 *          "effortType": "progress",
 *          "workflowId": 1,
 *          "ordinalNumber": 2
 *      }
 *  ]
 *}
 */

/**
 * @api {ACTION} create_workflow_state 2-Create new workflow state
 * @apiName create_workflow_state
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Workflow state
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the workflow state <br><code>required</code>
 * @apiParam {String="untouched","holding", "progress", "terminated"} effortType Type of workflow state <br><code>required</code>
 * @apiParam {Number} workflowId The id of workflow <br><code>required</code>
 * @apiParam {Number} ordinalNumber The ordinalNumber of workflow state in workflow <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *      "action": "create_workflow_state",
 *     	"name": "Close",
 *      "effortType": "terminated",
 *      "workflowId": 1,
 *      "ordinalNumber": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the workflow
 * @apiSuccess {String} result.name Name of the workflow state
 * @apiSuccess {String} result.effortType effortType of the workflow state
 * @apiSuccess {Number} result.workflowId The id of workflow
 * @apiSuccess {Number} result.ordinalNumber The ordinalNumber of workflow state in workflow
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: workflow is not found
 * - DUPLICATED_ERROR: the name of workflow state is duplicated <br>
 *     Or if this workflow have a workflow state but state have effort type which is untouched or terminated <br>
 *     Or {workflowId, ordinalNumber} does existed
 * - PARAM_INVALID: effortType is invalid
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 3,
 *      "name": "Close",
 *      "effortType": "terminated",
 *      "workflowId": 1,
 *      "ordinalNumber": 6
 *  }
 * }
 */

/**
 * @api {ACTION} update_workflow_state 2-Update the existing workflow state
 * @apiName update_workflow_state
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the workflow by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowStateId Id of Workflow state will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the workflow state <br><code>required</code>
 * @apiParam {String="untouched","holding", "progress", "terminated"} effortType Type of workflow state <br><code>required</code>
 * @apiParam {Number} workflowId The id of workflow <br><code>required</code>
 * @apiParam {Number} ordinalNumber The ordinalNumber of workflow state in workflow <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_workflow_state",
 *     "workflowStateId": 1,
 *     "name": "ToDo1",
 *     "effortType": "untouched,
 *     "workflowId": 1,
 *     "ordinalNumber": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of workflow state is duplicated, <br>
 *     Or If this workflow have an other workflow state but it have effort type which is untouched or terminated, <br>
 *     Or {workflowId, ordinalNumber} does existed
 * - NOT_FOUND_ERROR: the workflow state does not exist or workflow does not existed
 * - PARAM_INVALID: effortType is invalid
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_workflow_state 2-Delete specific workflow state
 * @apiName remove_workflow_state
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific workflow state from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowStateId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_workflow_state",
 *     "workflowStateId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowStateId does not comply with the policy
 * - NOT_FOUND_ERROR: the workflow state is not existed in the system
 * - REMOVE_REFERENCED_RECORDS: this workflow state does used
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */


/**
 * @api {ACTION} create_workflow_detail 3-Create new workflow detail
 * @apiName create_workflow_detail
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new Workflow detail
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowId Id of the workflow <br><code>required</code>
 * @apiParam {Number} fromState Id of workflow state chosen to do fromState <br><code>required</code>
 * @apiParam {Number} toState Id of workflow state chosen to do toState <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *    "action": "create_workflow_detail",
 *    "workflowId": 1,
 *    "fromState":4,
 *    "toState": 5
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the workflow state
 * @apiSuccess {Number} result.workflowId Id of the workflow
 * @apiSuccess {Number} result.fromState Id of the workflow state chosen to do fromState
 * @apiSuccess {Number} result.toState Id of the workflow state chosen to do toState
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: workflowId or fromState or toState does not comply with the policy
 * - NOT_FOUND_ERROR: if workflow or fromState or toState is not existed
 * - NOT_CHANGE_STATE_ERROR : if fromState = toState
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 4,
 *        "workflowId": 1,
 *        "fromState": 4,
 *        "toState": 5
 *    }
 * }
 */

/**
 * @api {ACTION} update_workflow_detail 3-Update the existing workflow detail
 * @apiName update_workflow_detail
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the workflow detail
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowDetailId Id of Workflow detail will be updated <br><code>required</code>
 * @apiParam {Number} workflowId Id of Workflow <br><code>required</code>
 * @apiParam {Number} fromState Id of workflow state chosen to do fromState <br><code>required</code>
 * @apiParam {Number} toState Id of workflow state chosen to do toState <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_workflow_detail",
 *     "workflowId": 2,
 *     "workflowDetailId": 1,
 *     "fromState": "ToDo1",
 *     "toState": "untouched
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowDetailId or workflowId or fromState or toState does not comply with the policy
 * - DUPLICATED_ERROR: information of workflow detail is duplicated
 * - NOT_FOUND_ERROR: the workflow detail or workflow or fromState or toState does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_workflow_detail 3-Delete specific workflow state
 * @apiName remove_workflow_detail
 * @apiGroup Workflow
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific workflow state from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} workflowDetailId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_workflow_detail",
 *     "workflowDetailId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the workflowDetailId does not comply with the policy
 * - NOT_FOUND_ERROR: the workflow state is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */


/**
 * @api {ACTION} list_permission_scheme Get the list of permissionScheme
 * @apiName list_permission_scheme
 * @apiGroup PermissionScheme
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the permissionScheme
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_permission_scheme"
 * }
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of permissionScheme
 * @apiSuccess {String} result.name The name of permissionScheme
 *
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "name": "PermissionScheme 1"
 *          }
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} create_permission_scheme Create new permissionScheme
 * @apiName create_permission_scheme
 * @apiGroup PermissionScheme
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new permissionScheme
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} name Name of the permissionScheme <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_permission_scheme",
 *     "name": "PermissionScheme 1"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the permissionScheme
 * @apiSuccess {String} result.name Name of the permissionScheme
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of permissionScheme is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": {
 *          "id": 1,
 *          "name": "PermissionScheme 1"
 *      }
 * }
 *
 */

/**
 * @api {ACTION} update_permission_scheme Update the existing permissionScheme
 * @apiName update_permission_scheme
 * @apiGroup PermissionScheme
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the name of the permissionScheme by name
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} permissionSchemeId Id of permissionScheme will be updated <br><code>required</code>
 * @apiParam {String{6..30}} name Name of the permissionScheme <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_permission_scheme",
 *     "permissionSchemeId": 1,
 *     "name": "PermissionScheme A"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the permissionSchemeId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of permissionScheme is duplicated
 * - NOT_FOUND_ERROR: the permissionScheme does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_permission_scheme Delete specific permissionScheme
 * @apiName remove_permission_scheme
 * @apiGroup PermissionScheme
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific permissionScheme from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} permissionSchemeId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_permission_scheme",
 *     "permissionSchemeId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the permissionSchemeId does not comply with the policy
 * - NOT_FOUND_ERROR: the permissionScheme is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} get_permission_scheme 1-Get information of the specific permission scheme
 * @apiName get_permission_scheme
 * @apiGroup PermissionScheme
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get information the specific permission scheme from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} permissionSchemeId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_permission_scheme",
 *     "permissionSchemeId": 3
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the permission scheme
 * @apiSuccess {String} result.name Name of the permission scheme
 * @apiSuccess {Object[]} result.listRoles
 * @apiSuccess {Number} result.listRoles.projectRoleId
 * @apiSuccess {Object} result.listRoles.ProjectRole
 * @apiSuccess {Number} result.listRoles.ProjectRole.id
 * @apiSuccess {String} result.listRoles.ProjectRole.name
 * @apiSuccess {Object[]} result.listPermissions
 * @apiSuccess {String} result.listPermissions.permission
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the permissionSchemeId does not comply with the policy
 * - NOT_FOUND_ERROR: the permission scheme is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
    "success": true,
    "result": {
        "id": 3,
        "name": "Default Permission Scheme",
        "listRoles": [
            {
                "projectRoleId": 1,
                "ProjectRole": {
                    "id": 1,
                    "name": "Owner"
                }
            },
            {
                "projectRoleId": 1,
                "ProjectRole": {
                    "id": 1,
                    "name": "Owner"
                }
            },
            {
                "projectRoleId": 2,
                "ProjectRole": {
                    "id": 2,
                    "name": "Leader"
                }
            }
        ],
        "listPermissions": [
            {
                "permission": "asdsd"
            },
            {
                "permission": "dfgsdg"
            },
            {
                "permission": "fgfh"
            }
        ]
    }
  }
 */


/**
 * @api {ACTION} create_sprint Create new sprint
 * @apiName create_sprint
 * @apiGroup Sprint
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new sprint
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} name Name of the sprint <br><code>required</code>
 * @apiParam {Number} projectId Id of project contain sprint <br><code>required</code>
 * @apiParam {String="unactive", "active", "completed"} status Status of sprint <br><code>required</code>
 * @apiParam {Date} startDate The date when sprint started <br><code>required</code>
 * @apiParam {Date} endDate The date when sprint ended <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *  "action": "create_sprint",
 *  "name": "Sprint 1",
 *  "projectId": 1,
 *  "startDate": "2018-10-28T06:50:30.000Z",
 *  "endDate": "2018-11-09T06:50:30.000Z",
 *  "status": "unactive"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the sprint
 * @apiSuccess {String} result.name Name of the sprint
 * @apiSuccess {Number} result.projectId Id of project contain sprint
 * @apiSuccess {String} result.status Status of sprint
 * @apiSuccess {Date} result.startDate The date when sprint started
 * @apiSuccess {Date} result.endDate The date when sprint ended
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of sprint is duplicated in one project
 * - NOT_FOUND_ERROR: Project which contain sprint is not existed
 * - PARAM_INVALID: Status of sprint is not in {"unactive", "active", "completed"}
 * - TIME_ERROR: if startDate after endDate
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 1,
 *        "name": "Sprint 1",
 *        "projectId": 1,
 *        "status": "unactive",
 *        "startDate": "2018-10-28T06:50:30.000Z",
 *        "endDate": "2018-11-09T06:50:30.000Z"
 *    }
 * }
 *
 */

/**
 * @api {ACTION} update_sprint Update the existing sprint
 * @apiName update_sprint
 * @apiGroup Sprint
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update information of the Sprint
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} sprintId Id of sprint will be updated <br><code>required</code>
 * @apiParam {String} name Name of the sprint <br><code>required</code>
 * @apiParam {Number} projectId Id of project contain sprint <br><code>required</code>
 * @apiParam {String="unactive", "active", "completed"} status Status of sprint <br><code>required</code>
 * @apiParam {Date} startDate The date when sprint started <br><code>required</code>
 * @apiParam {Date} endDate The date when sprint ended <br><code>required</code>
 *
 *
 * @apiParamExample {json} Param example
 * {
 *  "action": "update_sprint",
 *  "sprintId": 1,
 *  "name": "Sprint 1",
 *  "projectId": 1,
 *  "startDate": "2018-10-28T06:50:30.000Z",
 *  "endDate": "2018-11-09T06:50:30.000Z",
 *  "status": "unactive"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the permissionSchemeId or name does not comply with the policy
 * - DUPLICATED_ERROR: the name of sprint is duplicated
 * - NOT_FOUND_ERROR: the sprint does not exist
 * - PARAM_INVALID: Status of sprint is not in {"unactive", "active", "completed"}
 * - TIME_ERROR: if startDate after endDate
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 */

/**
 * @api {ACTION} remove_sprint Delete specific sprint
 * @apiName remove_sprint
 * @apiGroup Sprint
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific sprint from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} sprintId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_sprint",
 *     "sprintId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the sprintId does not comply with the policy
 * - NOT_FOUND_ERROR: the sprint is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} get_sprint Get information of the specific sprint
 * @apiName get_sprint
 * @apiGroup Sprint
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get information the specific sprint from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} sprintId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_sprint",
 *     "sprintId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the sprint
 * @apiSuccess {String} result.name Name of the sprint
 * @apiSuccess {Number} result.projectId Id of project contain sprint
 * @apiSuccess {String} result.status Status of sprint
 * @apiSuccess {Date} result.startDate The Date when sprint started
 * @apiSuccess {Date} result.endDate the date when sprint ended
 * @apiSuccess {Object} result.Project
 * @apiSuccess {Number} result.Project.id Id of project contain sprint
 * @apiSuccess {Number} result.Project.groupId Id of group contain project
 * @apiSuccess {String} result.Project.name Name of  project contain sprint
 * @apiSuccess {String} result.Project.description Description of  project contain sprint
 * @apiSuccess {Boolean} result.Project.isActive Status of project contain sprint
 * @apiSuccess {Boolean} result.Project.isNotificationEnable Notification of project contain sprint
 * @apiSuccess {Number} result.Project.permissionSchemeId Id of permissionScheme contain project
 * @apiSuccess {Number} result.Project.workflowSchemeId Id of workflow contain project
 * @apiSuccess {Object} result.Project.PermissionScheme
 * @apiSuccess {Number} result.Project.PermissionScheme.id Id of permissionScheme contain project
 * @apiSuccess {String} result.Project.PermissionScheme.name Name of permissionScheme contain project
 * @apiSuccess {Object} result.Project.Workflow
 * @apiSuccess {Number} result.Project.Workflow.id Id of workflow contain project
 * @apiSuccess {String} result.Project.Workflow.name Name of workflow contain project
 * @apiSuccess {Object} result.Project.Group
 * @apiSuccess {Number} result.Project.Group.id Id of group contain project
 * @apiSuccess {String} result.Project.Group.name Name of group contain project
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the sprintId does not comply with the policy
 * - NOT_FOUND_ERROR: the sprint is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": {
 *      "id": 1,
 *      "name": "Sprint 1",
 *      "projectId": 1,
 *      "status": "unactive",
 *      "startDate": "2018-10-28T06:50:30.000Z",
 *      "endDate": "2018-11-09T06:50:30.000Z",
 *      "Project": {
 *          "id": 1,
 *          "groupId": 1,
 *          "name": "Project 1",
 *          "description": "abc",
 *          "isActive": true,
 *          "isNotificationEnable": false,
 *          "permissionSchemeId": 1,
 *          "workflowSchemeId": 1,
 *          "Workflow": {
 *              "id": 1,
 *              "name": "Workflow 1"
 *          },
 *          "PermissionScheme": {
 *              "id": 1,
 *              "name": "PermissionScheme 1"
 *          },
 *          "Group": {
 *              "id": 1,
 *              "name": "Group 1"
 *          }
 *      }
 *  }
 * }
 */

/**
 * @api {ACTION} get_sprints_by_projectId Get list sprints of the specific project
 * @apiName get_sprints_by_projectId
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get list sprints of the specific project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "get_sprints_by_projectId",
 *     "projectId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Unique ID of the sprint
 * @apiSuccess {String} result.name Name of the sprint
 * @apiSuccess {Number} result.projectId Id of project contain sprint
 * @apiSuccess {String} result.status Status of sprint
 * @apiSuccess {Date} result.startDate The Date when sprint started
 * @apiSuccess {Date} result.endDate the date when sprint ended
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the projectId does not comply with the policy
 * - NOT_FOUND_ERROR: the project is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *      {
 *          "id": 1,
 *          "name": "Sprint 1",
 *          "projectId": 1,
 *          "status": "unactive",
 *          "startDate": "2018-10-28T06:50:30.000Z",
 *          "endDate": "2018-11-09T06:50:30.000Z"
 *      }
 *  ]
 * }
 */


/**
 * @api {ACTION} create_allocation Create new allocation
 * @apiName create_allocation
 * @apiGroup Allocation
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new allocation
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId Id of user <br><code>required</code>
 * @apiParam {Number} sprintId Id of sprint <br><code>required</code>
 * @apiParam {Number{1-100}} effortPercent Effort percent of user <br><code>required</code>
 * @apiParam {Date} startDate The date when allocation started <br><code>required</code>
 * @apiParam {Date} endDate The date when allocation ended <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *   "action": "create_allocation",
 *   "userId": 2,
 *   "sprintId": 1,
 *   "startDate": "2018-10-28T06:50:30.000Z",
 *   "endDate": "2018-11-09T06:50:30.000Z",
 *   "effortPercent": 50
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the allocation
 * @apiSuccess {Number} result.userId Id of the user
 * @apiSuccess {Number} result.sprintId Id of sprint contain this allocation
 * @apiSuccess {Number} result.effortPercent Effort percent which user contribute for sprint
 * @apiSuccess {Date} result.startDate The date when allocation started
 * @apiSuccess {Date} result.endDate The date when allocation ended
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of sprint is duplicated in one project
 * - NOT_FOUND_ERROR: User or sprint which contain allocation is not existed
 * - PARAM_INVALID: effortPercent > 100 or effortPercent exceed the allowed number.
 * - TIME_ERROR: if startDate after endDate or {startDate, endDate} not within time of sprint
 * - DUPLICATED_ERROR: {userId, sprintId} in allocation is unique
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 1,
 *        "userId": 2,
 *        "sprintId": 1,
 *        "effortPercent": 50,
 *        "startDate": "2018-10-28T06:50:30.000Z",
 *        "endDate": "2018-11-09T06:50:30.000Z"
 *    }
 * }
 *
 */

/**
 * @api {ACTION} update_allocation Update the existing allocation
 * @apiName update_allocation
 * @apiGroup Allocation
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update information of the allocation
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} allocationId Id of allocation <br><code>required</code>
 * @apiParam {Number{1-100}} effortPercent Effort percent of user <br><code>required</code>
 * @apiParam {Date} startDate The date when allocation started <br><code>required</code>
 * @apiParam {Date} endDate The date when allocation ended <br><code>required</code>
 *
 *
 * @apiParamExample {json} Param example
 * {
 *   "action": "update_allocation",
 *   "allocationId": 1,
 *   "startDate": "2018-10-28T06:50:30.000Z",
 *   "endDate": "2018-11-09T06:50:30.000Z",
 *   "effortPercent": 75
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of sprint is duplicated in one project
 * - NOT_FOUND_ERROR: Allocation with allocationId is not existed
 * - PARAM_INVALID: effortPercent > 100 or effortPercent exceed the allowed number.
 * - TIME_ERROR: if startDate after endDate or {startDate, endDate} not within time of sprint
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 */

/**
 * @api {ACTION} remove_allocation Delete specific allocation
 * @apiName remove_allocation
 * @apiGroup Allocation
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific allocation from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} allocationId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_allocation",
 *     "allocationId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the allocationId does not comply with the policy
 * - NOT_FOUND_ERROR: the allocation is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} list_allocation_of_user List allocation of specific user
 * @apiName list_allocation_of_user
 * @apiGroup Allocation
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List allocation of specific user
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_allocation_of_user",
 *     "userId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Unique ID of user
 * @apiSuccess {String} result.name username of the user
 * @apiSuccess {String} result.avatar link avatar of user
 * @apiSuccess {Object[]} result.allocations list allocations of user
 * @apiSuccess {Number} result.allocations.id ID of allocation
 * @apiSuccess {Number} result.allocations.effortPercent effort's percent in that allocation of user
 * @apiSuccess {Number} result.allocations.startDate startDate of allocation
 * @apiSuccess {Number} result.allocations.endDate endDate of allocation
 * @apiSuccess {Number} result.allocations.project project's name of allocation
 * @apiSuccess {Array[Array[]]} result.dataChart data for chart
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the userId does not comply with the policy
 * - NOT_FOUND_ERROR: the user is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *   "success": true,
 *   "result": {
 *       "id": 1,
 *       "name": "admin",
 *       "avatar": "dev-identity-bap-jp/profile-photos/1",
 *       "allocations": [
 *           {
 *               "id": 1,
 *               "effortPercent": 20,
 *               "startDate": "2018-11-07T08:08:28.000Z",
 *               "endDate": "2018-11-14T08:08:28.000Z",
 *               "project": "TRIAM Network"
 *           },
 *           {
 *               "id": 2,
 *               "effortPercent": 50,
 *               "startDate": "2018-11-15T08:08:28.000Z",
 *               "endDate": "2018-11-21T08:08:28.000Z",
 *               "project": "TRIAM Network"
 *           },
 *           {
 *               "id": 3,
 *               "effortPercent": 60,
 *               "startDate": "2018-11-03T08:08:28.000Z",
 *               "endDate": "2018-11-11T08:08:28.000Z",
 *               "project": "Greenbox wallet"
 *           },
 *           {
 *               "id": 4,
 *               "effortPercent": 50,
 *               "startDate": "2018-11-15T08:08:28.000Z",
 *               "endDate": "2018-11-19T08:08:28.000Z",
 *               "project": "Greenbox wallet"
 *           }
 *       ],
 *       "dataChart": [ 
 *          [ 'Date', 'TRIAM Network', 'Greenbox wallet' ],
 *          [ '2018/11/3', 0, 60 ],
 *          [ '2018/11/4', 0, 60 ],
 *          [ '2018/11/5', 0, 60 ],
 *          [ '2018/11/6', 0, 60 ],
 *          [ '2018/11/7', 20, 60 ],
 *          [ '2018/11/8', 20, 60 ],
 *          [ '2018/11/9', 20, 60 ],
 *          [ '2018/11/10', 20, 60 ],
 *          [ '2018/11/11', 20, 60 ],
 *          [ '2018/11/12', 20, 0 ],
 *          [ '2018/11/13', 20, 0 ],
 *          [ '2018/11/14', 20, 0 ],
 *          [ '2018/11/15', 50, 50 ],
 *          [ '2018/11/16', 50, 50 ],
 *          [ '2018/11/17', 50, 50 ],
 *          [ '2018/11/18', 50, 50 ],
 *          [ '2018/11/19', 50, 50 ],
 *          [ '2018/11/20', 50, 0 ],
 *          [ '2018/11/21', 50, 0 ] 
 *      ]
 *   }
 * }
 */

/**
 * @api {ACTION} list_allocation_of_sprint List allocation of specific sprint
 * @apiName list_allocation_of_sprint
 * @apiGroup Allocation
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List allocation of specific sprint
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} sprintId <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_allocation_of_sprint",
 *     "sprintId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id
 * @apiSuccess {String} result.name
 * @apiSuccess {Number} result.projectId
 * @apiSuccess {String} result.status
 * @apiSuccess {Date} result.startDate
 * @apiSuccess {Date} result.endDate
 * @apiSuccess {Object[]} result.allocations
 * @apiSuccess {Number} result.allocations.id Unique ID of the allocation
 * @apiSuccess {Number} result.allocations.userId Id of the user
 * @apiSuccess {Number} result.allocations.sprintId Id of sprint contain this allocation
 * @apiSuccess {Number} result.allocations.effortPercent Effort percent which user contribute for sprint
 * @apiSuccess {Date} result.allocations.startDate The date when allocation started
 * @apiSuccess {Date} result.allocations.endDate The date when allocation ended
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the sprintId does not comply with the policy
 * - NOT_FOUND_ERROR: the sprint is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 1,
 *        "name": "Sprint 1",
 *        "projectId": 1,
 *        "status": "unactive",
 *        "startDate": "2018-10-28T06:50:30.000Z",
 *        "endDate": "2018-11-09T06:50:30.000Z",
 *        "allocation": [
 *            {
 *                "id": 1,
 *                "userId": 2,
 *                "sprintId": 1,
 *                "effortPercent": 75,
 *                "startDate": "2018-10-28T06:50:30.000Z",
 *                "endDate": "2018-11-09T06:50:30.000Z"
 *            },
 *            {
 *                "id": 2,
 *                "userId": 3,
 *                "sprintId": 1,
 *                "effortPercent": 20,
 *                "startDate": "2018-10-28T06:50:30.000Z",
 *                "endDate": "2018-11-08T06:50:30.000Z"
 *            }
 *        ]
 *    }
 * }
 */


/**
 * @api {ACTION} create_task Create new task
 * @apiName create_task
 * @apiGroup Task
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new task
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} content Content of the task <br><code>required</code>
 * @apiParam {Number} estimate estimate of task
 * @apiParam {Number} sprintId Id of sprint contain task <br><code>required</code>
 * @apiParam {String} tag Tag of task <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *  "action": "create_task",
 *  "content": "Task 1",
 *  "estimate": 3,
 *  "sprintId": 1,
 *  "tag": "bug"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the task
 * @apiSuccess {String} result.content content of the task
 * @apiSuccess {Number} result.estimate estimate of task
 * @apiSuccess {Number} result.sprintId Id of sprint contain task
 * @apiSuccess {String} result.tag Tag of task
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of task is duplicated in one sprint
 * - NOT_FOUND_ERROR: Sprint which contain task is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 1,
 *        "content": "Task 1",
 *        "estimate": 3,
 *        "sprintId": 1,
 *        "tag": "bug"
 *    }
 * }
 *
 */

/**
 * @api {ACTION} update_task Update the existing task
 * @apiName update_task
 * @apiGroup Task
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update information of the Task
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} taskId Id of task will be updated
 * @apiParam {String} content Content of the task <br><code>required</code>
 * @apiParam {Number} estimate estimate of task
 * @apiParam {Number} sprintId Id of sprint contain task <br><code>required</code>
 * @apiParam {String} tag Tag of task <br><code>required</code>*
 *
 * @apiParamExample {json} Param example
 * {
 *  "action": "update_task",
 *  "taskId": 1,
 *  "content": "Task 1",
 *  "estimate": 4,
 *  "sprintId": 1,
 *  "tag": "bug"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of task is duplicated in one sprint
 * - NOT_FOUND_ERROR: Sprint which contain task is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 */

/**
 * @api {ACTION} remove_task Delete specific task
 * @apiName remove_task
 * @apiGroup Task
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific task from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} taskId
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_task",
 *     "taskId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the taskId does not comply with the policy
 * - NOT_FOUND_ERROR: the task is not existed in the system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 */

/**
 * @api {ACTION} get_tasks_by_sprintId Get list tasks of the specific sprint
 * @apiName list_task_by_sprintId
 * @apiGroup Task
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get list tasks of the specific sprint
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} sprintId
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_task_by_sprintId",
 *     "sprintId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Unique ID of the task
 * @apiSuccess {String} result.content Content of the task
 * @apiSuccess {Number} result.estimate estimate of task
 * @apiSuccess {Number} result.sprintId Id of sprint contain task
 * @apiSuccess {String} result.tag Tag of tasks
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the sprintId does not comply with the policy
 * - NOT_FOUND_ERROR: Sprint which contain task is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true,
 *  "result": [
 *    {
 *      "action": "update_task",
 *      "taskId": 1,
 *      "content": "Task 1",
 *      "estimate": 4,
 *      "sprintId": 1,
 *      "tag": "bug"
 *    }
 *  ]
 * }
 */

/**
 * @api {ACTION} create_task_state Create new task state
 * @apiName create_task_state
 * @apiGroup TaskState
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new task state
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} taskId Id of task <br><code>required</code>
 * @apiParam {Number} fromState Id of state that task is from<br><code>required</code>
 * @apiParam {Number} sprintId Id of state that task is to<br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *  "taskId": 1,
 *  "fromState": 3,
 *  "toState": 2,
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the task state
 * @apiSuccess {Number} result.taskId Id of task
 * @apiSuccess {Number} result.fromState Id of state that task is from
 * @apiSuccess {Number} result.toState Id of state that task is to
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: Task or state is not existed
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *        "id": 1,
 *        "taskId": 1,
 *        "fromState": 3,
 *        "toState": 2,
 *    }
 * }
 *
 */


/**
 * @api {ACTION} create_item_check Create item check
 * @apiName create_item_check
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create item check
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} name Name of new item check <br><code>required</code>
 * @apiParam {String} description Description of new item check
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "create_item_check",
 *     "name": "Check mongodb",
 *     "description": "This is a description of item check"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of new item check
 * @apiSuccess {String} result.name The name of new item check
 * @apiSuccess {Boolean} result.description Description of new item check
 * @apiSuccess {Boolean} result.createdAt Timestamp create new item check
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - TOKEN_EXPIRED: the token is expired,
 * - DUPLICATED_ERROR: Name of item check is duplicated
 *
 * @apiSuccessExample {json} Success example:
 *  {
 *    "success": true,
 *    "result": {
 *        "createdAt": "2018-11-08T08:00:32.660Z",
 *        "id": 12,
 *        "name": "Check mongodb",
 *        "description": "This is a description of item check"
 *    }
 *  }
 */

/**
 * @api {ACTION} update_item_check Update information of specific item check
 * @apiName update_item_check
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update information of specific item check
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} itemCheckId Id of specific item check  <br><code>required</code>
 * @apiParam {String} name Name of specific item check <br><code>required</code>
 * @apiParam {String} description Description of specific item check
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "update_item_check",
 *     "itemCheckId": 1,
 *     "name": "Check mongodb",
 *     "description": "This is a description of item check"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the new name of specific item check is duplicated
 * - NOT_VALID_ID: itemCheckId does not comply with the policy
 * - NOT_FOUND_ERROR: item check is not found
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true
 * }
 */

/**
 * @api {ACTION} remove_item_check Delete specific item check
 * @apiName remove_item_check
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete specific item check
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} itemCheckId Id of specific item check  <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "remove_item_check",
 *     "itemCheckId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_VALID_ID: itemCheckId does not comply with the policy
 * - NOT_FOUND_ERROR: item check is not found
 *
 * @apiSuccessExample {json} Success example:
 * {
 *  "success": true
 * }
 */

/**
 * @api {ACTION} list_all_item_check List all item check of system
 * @apiName list_all_item_check
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all item check of system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_all_item_check"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Id of item check
 * @apiSuccess {String} result.name Name of item check
 * @apiSuccess {String} result.description Description of item check
 * @apiSuccess {Date} result.createdAt Timestamp when item check been created
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 *  {
 *    "success": true,
 *    "result": [
 *        {
 *            "id": 4,
 *            "name": "Check transfer ddas",
 *            "description": "abcfghfgfdgfd",
 *            "createdAt": "2018-11-06T07:49:03.000Z"
 *        },
 *        {
 *            "id": 6,
 *            "name": "check db",
 *            "description": "hsdfhsdf",
 *            "createdAt": "2018-11-06T08:55:36.000Z"
 *        }
 *    ]
 *  }
 */

/**
 * @api {ACTION} list_checked_item_of_project List all item check which checked of project
 * @apiName list_checked_item_of_project
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all item check which checked of project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId Id of project to list item checked <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "list_checked_item_of_project",
 *     "projectId": 1
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Id of check list of project
 * @apiSuccess {Number} result.projectId Id of project
 * @apiSuccess {Number} result.checkItemId Id of item check
 * @apiSuccess {Boolean} result.isChecked Status check of item check in project <code>true</code>
 * @apiSuccess {Object} result.CheckListItem Information of item check which checked in project
 * @apiSuccess {Number} result.CheckListItem.id Id of item check
 * @apiSuccess {String} result.CheckListItem.name Name of item check
 * @apiSuccess {String} result.CheckListItem.description Description of item check
 * @apiSuccess {Number} result.CheckListItem.createdAt Timestamp of item check when item check been created
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_VALID_ID: projectId does not comply with the policy
 * - NOT_FOUND_ERROR: project is not found
 *
 * @apiSuccessExample {json} Success example:
 *  {
 *    "success": true,
 *    "result": [
 *        {
 *            "id": 1,
 *            "projectId": 1,
 *            "checkItemId": 6,
 *            "isChecked": true,
 *            "CheckListItem": {
 *                "id": 6,
 *                "name": "check db",
 *                "description": "hsdfhsdf",
 *                "createdAt": "2018-11-06T08:55:36.000Z"
 *            }
 *        },
 *        {
 *            "id": 7,
 *            "projectId": 1,
 *            "checkItemId": 4,
 *            "isChecked": true,
 *            "CheckListItem": {
 *                "id": 4,
 *                "name": "Check transfer ddas",
 *                "description": "abcfghfgfdgfd",
 *                "createdAt": "2018-11-06T07:49:03.000Z"
 *            }
 *        }
 *    ]
 *  }
 */

/**
 * @api {ACTION} check_uncheck_item Check or uncheck item of specific project
 * @apiName check_uncheck_item
 * @apiGroup Checklist
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Check or uncheck item of specific project
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} projectId Id of project <br><code>required</code>
 * @apiParam {Number} checkItemId Id of item check <br><code> required </code>
 * @apiParam {Boolean} isChecked Status which want to check for item check of project <br><code> required </code>
 *
 * @apiParamExample {json} Param example
 * {
 *     "action": "check_uncheck_item",
 *     "projectId": 1,
 *     "checkItemId": 12,
 *     "isChecked": true
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of new item check
 * @apiSuccess {Number} result.projectId Id of project
 * @apiSuccess {Number} result.checkItemId Id of item check
 * @apiSuccess {Boolean} result.isChecked isChecked Status which want to check for item check of project
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - TOKEN_EXPIRED: the token is expired
 * - NOT_FOUND_ERROR: project or item check not found
 * - NOT_VALID_ID: projectId of checkItemId does not comply with the policy
 *
 * @apiSuccessExample {json} Success example:
 *  {
 *    "success": true,
 *    "result": {
 *        "id": 9,
 *        "projectId": 1,
 *        "checkItemId": 12,
 *        "isChecked": true
 *    }
 *  }
 */


/**
 * @api {ACTION} list_environments Get the list of environment
 * @apiName list_environments
 * @apiGroup Environment
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the environment
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} [projectId] The id of the project
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of the environment
 * @apiSuccess {String} result.name The name of the environment
 * @apiSuccess {String} result.serverHealthCheck The url of the health check server
 * @apiSuccess {Number} result.HcThresholdDown The maximum times to check service is dead
 * @apiSuccess {Number} result.HcThresholdUp The maximum times to check service is alive
 * @apiSuccess {Number} result.projectId The Id of project
 * @apiSuccess {Number} result.createdAt The create date
 * @apiSuccess {Number} result.updatedAt The latest update date
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: the project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "name": "staging",
 *              "serverHealthCheck": "http://www.healthcheck.staging.com",
 *              "projectId": 1,
 *              "HcThresholdDown": 5,
 *              "HcThresholdUp": 5,
 *              "createdAt": "2018-08-15T06:50:30.000Z",
 *              "updatedAt": "2018-08-15T06:50:30.000Z"
 *          }
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} get_environment Get specific environment
 * @apiName get_environment
 * @apiGroup Environment
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the environment by environmentId
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} envId The id of the project
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of the environment
 * @apiSuccess {String} result.name The name of the environment
 * @apiSuccess {String} result.serverHealthCheck The url of the health check server
 * @apiSuccess {Number} result.HcThresholdDown The maximum times to check service is dead
 * @apiSuccess {Number} result.HcThresholdUp The maximum times to check service is alive
 * @apiSuccess {Number} result.projectId The Id of project
 * @apiSuccess {Object[]} result.configs The list of configuration of the environment
 * @apiSuccess {Number} result.config.id The Id of configuration
 * @apiSuccess {String} result.config.key The key of configuration
 * @apiSuccess {String} result.config.value The value of configuration
 * @apiSuccess {Object} result.healthCheckData
 * @apiSuccess {Number} result.createdAt The create date
 * @apiSuccess {Number} result.updatedAt The latest update date
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: the project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "name": "staging",
 *              "serverHealthCheck": "http://www.healthcheck.staging.com",
 *              "projectId": 1,
 *              "HcThresholdDown": 5,
 *              "HcThresholdUp": 5,
 *              "createdAt": "2018-08-15T06:50:30.000Z",
 *              "updatedAt": "2018-08-15T06:50:30.000Z",
 *              "configs": [
 *              {
 *                  "id": 1,
 *                  "key": "DB_NAME",
 *                  "value": "project_bap_lcl",
 *                  "environmentId": 1
 *              }
 *              ],
 *              "healthCheckData": {}
 *          }
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} create_environment Create new environment
 * @apiName create_environment
 * @apiGroup Environment
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new environment
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} name Name of the environment
 * @apiParam {Boolean} serverHealthCheck The url of the Server Health Check
 * @apiParam {Number} projectId The id of project
 * @apiParam {Number} hcThresholdDown The maximum times to check service is dead
 * @apiParam {Number} hcThresholdUp The maximum times to check service is alive
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} update_environment Update the existing environment
 * @apiName update_environment
 * @apiGroup Environment
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the information of the environment by Id
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} envId Id of the environment
 * @apiParam {String} name Name of the environment
 * @apiParam {Boolean} serverHealthCheck The url of the server health check
 * @apiParam {Number} projectId The id of project
 * @apiParam {Number} hcThresholdDown The maximum times to check service is dead
 * @apiParam {Number} hcThresholdUp The maximum times to check service is alive
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the envId does not comply with the policy
 * - DUPLICATED_ERROR: the name of environment is duplicated
 * - NOT_FOUND_ERROR: the environment or project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_environment Delete specific environment
 * @apiName remove_environment
 * @apiGroup Environment
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific environment from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} envId
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the envId does not comply with the policy
 * - NOT_FOUND_ERROR: the environment is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */


/**
 * @api {ACTION} list_configurations Get the list of configuration
 * @apiName list_configurations
 * @apiGroup Configuration
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get the list of the configuration by environmentId
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} envId The id of the environment
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id The Id of the configuration
 * @apiSuccess {String} result.key The key of the configuration
 * @apiSuccess {String} result.value The value of the configuration
 * @apiSuccess {Number} result.environmentId The id of the environment
 * @apiSuccess {Number} result.createdAt The create date
 * @apiSuccess {Number} result.updatedAt The latest update date
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: the project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *       {
 *           "id": 1,
 *           "key": "DB_NAME",
 *           "value": "project_bap_lcl",
 *           "createdAt": "2018-09-14T02:29:19.000Z",
 *           "updatedAt": "2018-09-14T02:42:15.000Z",
 *           "environmentId": 1
 *       }
 *       ]
 * }
 *
 */

/**
 * @api {ACTION} create_configuration Create new configuration
 * @apiName create_configuration
 * @apiGroup Configuration
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Create new configuration
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} key The key of the configuration
 * @apiParam {String} value The value of the configuration
 * @apiParam {Number} envId The id of environment
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - DUPLICATED_ERROR: the name of group is duplicated
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result":
 *            {
 *                "id": 1,
 *                "key": "DB_NAME",
 *                "value": "project_bap_lcl",
 *                "createdAt": "2018-09-14T02:29:19.000Z",
 *                "updatedAt": "2018-09-14T02:42:15.000Z",
 *                "environmentId": 1
 *            }
 * }
 *
 */

/**
 * @api {ACTION} update_configuration Update the existing configuration
 * @apiName update_configuration
 * @apiGroup Configuration
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update the information of the configuration by Id
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} configId Id of the configuration
 * @apiParam {String} key Name of the configuration
 * @apiParam {String} value The value of the configuration
 * @apiParam {Number} envId The id of environment
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the envId does not comply with the policy
 * - DUPLICATED_ERROR: the name of environment is duplicated
 * - NOT_FOUND_ERROR: the environment or project does not exist
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true
 * }
 *
 */

/**
 * @api {ACTION} remove_configuration Delete specific configuration
 * @apiName remove_configuration
 * @apiGroup Configuration
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Delete the specific configuration from the system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} configId The id of the configuration
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the envId does not comply with the policy
 * - NOT_FOUND_ERROR: the environment is not existed in the system
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 * }
 *
 */


/**
 * @api {ACTION} list_users List all users
 * @apiName list_users
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all users with input search fields
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} [username]
 * @apiParam {Boolean} [isActive]
 * @apiParam {String} [role]
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id
 * @apiSuccess {String} result.username
 * @apiSuccess {String} result.firstName
 * @apiSuccess {String} result.lastName
 * @apiSuccess {String} result.email
 * @apiSuccess {String} result.phoneNumber
 * @apiSuccess {Number} result.gender
 * @apiSuccess {Number} result.birthday
 * @apiSuccess {String} result.updatedAt RolePermission's updated date
 * @apiSuccess {String} result.createdAt RolePermission's created date
 * @apiSuccess {Object[]} result.roles Roles of user
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the roleId, permission_id does not comply with the policy
 * - NOT_FOUND_ERROR: the role with roleId is not existed
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": [
 *          {
 *              "id": 1,
 *              "username": "admin",
 *              "isActive": true,
 *              "firstName": "Luke",
 *              "lastName": "Young",
 *              "birthday": 13213213,
 *              "gender": 1,
 *              "email": "admin@bap.jp",
 *              "createdAt": "2018-08-15T04:07:02.000Z",
 *              "updatedAt": "2018-08-15T04:07:02.000Z",
 *              "roles":  [
 *                    {
 *                   "id": 93,
 *                   "roleId": 1,
 *                   "userId": 1,
 *                   "role": {
 *                       "id": 1,
 *                       "name": "super_admin",
 *                       "super_user": true,
 *                       "editable": false,
 *                       "createdAt": "2018-08-28T04:31:43.000Z",
 *                       "updatedAt": "2018-08-28T04:31:43.000Z"
 *                   }
 *               }
 *              ]
 *          },
 *      ]
 * }
 *
 */

/**
 * @api {ACTION} list_current_users List all current users in system
 * @apiName list_current_users
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * List all current users
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} [limit]  the number of users returned in a query result
 * @apiParam {Number} [offset] specify which row to start from retrieving data
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Object[]} result.users
 * @apiSuccess {Number} result.users.id
 * @apiSuccess {String} result.users.name
 * @apiSuccess {String} result.users.avatar
 * @apiSuccess {Object[]} result.users.roles Roles of user
 * @apiSuccess {Number} result.total Total of current users in system
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": true,
 *      "result": {
 *        "users": [
 *           {
 *               "id": 1,
 *               "name": "admin",
 *               "avatar": "dev-identity-bap-jp/profile-photos/1",
 *               "roles": [
 *                   {
 *                       "id": 1,
 *                       "userId": 1,
 *                       "roleId": 1,
 *                       "Role": {
 *                           "id": 1,
 *                           "name": "super_admin",
 *                           "isSuperUser": false,
 *                           "isEditable": true
 *                       }
 *                   }
 *               ]
 *           },
 *           {
 *               "id": 2,
 *               "name": "u2",
 *               "avatar": null,
 *               "roles": []
 *           },
 *           {
 *               "id": 3,
 *               "name": "u3",
 *               "avatar": null,
 *               "roles": []
 *           },
 *           {
 *               "id": 4,
 *               "name": "u4",
 *               "avatar": null,
 *               "roles": []
 *           },
 *           {
 *               "id": 5,
 *               "name": "u5",
 *               "avatar": null,
 *               "roles": []
 *           }
 *        ],
 *        "total": 79
 *    }
 * }
 *
 */

/**
 * @api {ACTION} add_user Add user into system
 * @apiName add_user
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Add user into system
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId Id of the user <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 * "action":"add_user",
 * "userId": 8,
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.id Unique ID of the record user in system
 * @apiSuccess {Number} result.token token of the user (default: '', update when user login)
 * @apiSuccess {Number} result.role role of the user (default: user)
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: the user or project or project role isn't founded
 * - USER_EXISTED: the user has existed in system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result": {
 *       "id": 100,
 *       "token": "",
 *       "role": {
 *           "id": 2,
 *           "userId": 8,
 *           "roleId": 3
 *       }
 *  }
 * }
 */


/**
 * @api {ACTION} update_user_role Update user's role
 * @apiName update_user_role
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update user's role
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId Id of user will be updated
 * @apiParam {Number} roleId Id of role
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the userId and roleId does not comply with the policy
 * - USER_NOT_FOUND: User with userId or roleId does not exist
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} update_user_info Update specific user's information from system
 * @apiName update_user_info
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update user's information by basic fields
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} userId The id of specific user
 * @apiParam {String} firstName The Firstname of specific User
 * @apiParam {String} lastName The Lastname of specific User
 * @apiParam {String} email The Email of specific User
 * @apiParam {String} phoneNumber The phoneNumber of specific User
 * @apiParam {String[]} roles The role of specific User
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} read_user_info Get specific user's information
 * @apiName read_user_info
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get user's information by userId
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} userId The if of specific user
 * @apiParam {String[]} info The string information array of user. Available values are:
 * - <code>'id'</code>: the id of user.
 * - <code>'username'</code>: the username of user
 * - <code>'firstName'</code>: the firstName of user
 * - <code>'lastName'</code>: the lastName of user
 * - <code>'gender'</code>: the gender of user
 * - <code>'birthday'</code>: the birthday of user
 * - <code>'email'</code>: the email of user
 * - <code>'isActive'</code>: the status of user
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {String} result.id The id of User
 * @apiSuccess {String} result.username The username of User
 * @apiSuccess {String} result.firstName The Firstname of user
 * @apiSuccess {String} result.lastName The Lastname of user
 * @apiSuccess {Number} result.birthday The Date of birth of user
 * @apiSuccess {Number} result.gender The User's gender
 * @apiSuccess {String} result.email The Email of user
 * @apiSuccess {String} result.isActive The Avatar of user
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true",
 *      result: {
 *          "id": 1,
 *          "username": "admin",
 *          "isActive": true,
 *          "firstName": "Luke",
 *          "lastName": "Young",
 *          "birthday": 13213213,
 *          "gender": 1,
 *          "email": "admin@bap.jp"
 *      }
 * }
 *
 */

/**
 * @api {ACTION} get_owned_roles Get roles of user
 * @apiName get_owned_roles
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get roles of user
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId Id of the user <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 * "action":"get_owned_roles",
 * "userId": 1,
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object[]} result
 * @apiSuccess {Number} result.id Unique ID of the record userRole in system
 * @apiSuccess {Number} result.userId Unique ID of the record user in system
 * @apiSuccess {Number} result.roleId Unique ID of the record role in system
 * @apiSuccess {Object} result.role detail role of the user
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - NOT_FOUND_ERROR: the user isn't founded
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result":  [
 *      {
 *           "id": 1,
 *           "userId": 1,
 *           "roleId": 1,
 *           "Role": {
 *              "id": 1,
 *              "name": "super_admin",
 *               "isSuperUser": false,
 *               "isEditable": true
 *           }
 *      }
 *   ]
 * }
 */

/**
 * @api {ACTION} get_my_role_and_permission Get roles and permission of logged in user
 * @apiName get_my_role_and_permission
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get roles and permission of logged in user
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParamExample {json} Param example
 * {
 * "action":"get_my_role_and_permission"
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Array[String]} result.roles roles of current user in system
 * @apiSuccess {Array[String]} result.permissions permission of the current user in system
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN: the token provided is not correct
 * - NOT_FOUND_ERROR: the user isn't founded
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result":  {
 *      "roles": [
 *          "super_admin"
 *      ],
 *      "permissions": [
 *          "create_group",
 *          "update_group",
 *          "remove_group",
 *          "get_group",
 *          "list_groups",
 *          "create_workflow",
 *          "update_workflow",
 *          "remove_workflow",
 *          "get_workflow",
 *          "list_workflow",
 *          "create_workflow_state",
 *          "update_workflow_state",
 *          "remove_workflow_state",
 *          "list_workflow_state",
 *          "create_workflow_detail",
 *          "update_workflow_detail",
 *          "remove_workflow_detail",
 *          "create_permission_scheme",
 *          "update_permission_scheme",
 *          "remove_permission_scheme",
 *          "list_permission_scheme",
 *          "get_permission_scheme",
 *          "create_permission_scheme_detail",
 *          "update_permission_scheme_detail",
 *          "remove_permission_scheme_detail",
 *          "list_all_permissions",
 *          "list_permission_by_permissionScheme_and_projectRole",
 *          "create_project",
 *          "update_project",
 *          "remove_project",
 *          "get_project",
 *          "list_projects",
 *          "list_project_by_userId",
 *          "list_project_environments",
 *          "get_project_alert_subscription",
 *          "update_project_alert_subscription",
 *          "list_my_project",
 *          "get_health_check_data",
 *          "create_project_role",
 *          "update_project_role",
 *          "remove_project_role",
 *          "list_project_role",
 *          "list_permission_of_role_project",
 *          "add_user_into_project",
 *          "list_users_by_project",
 *          "update_role_user_in_project",
 *          "remove_user_from_project",
 *          "create_sprint",
 *          "get_sprint",
 *          "get_sprints_by_projectId",
 *          "update_sprint",
 *          "remove_sprint",
 *          "create_task",
 *          "update_task",
 *          "update_task_backlog",
 *          "remove_task",
 *          "list_task_by_sprintId",
 *          "list_task_backlog_by_project",
 *          "add_estimate",
 *          "get_detail_task",
 *          "suggest_tag",
 *          "create_task_state",
 *          "create_allocation",
 *          "update_allocation",
 *          "remove_allocation",
 *          "list_allocation_of_user",
 *          "list_allocation_of_sprint",
 *          "get_role",
 *          "create_role",
 *          "update_role",
 *          "remove_role",
 *          "list_roles",
 *          "create_role_permission",
 *          "remove_role_permission",
 *          "list_all_role_permission",
 *          "update_role_permission",
 *          "list_permission_of_role",
 *          "list_all_permission",
 *          "add_permission",
 *          "list_users",
 *          "list_current_users",
 *          "add_user",
 *          "update_user_role",
 *          "read_user_info",
 *          "promote_user",
 *          "get_owned_roles",
 *          "list_operation_logs",
 *          "login",
 *          "get_my_role_and_permission",
 *          "create_environment",
 *          "update_environment",
 *          "remove_environment",
 *          "get_environment",
 *          "list_environments",
 *          "list_configurations",
 *          "create_configuration",
 *          "update_configuration",
 *          "remove_configuration"
 *      ]
 *  }
 * }
 */

/**
 * @api {ACTION} promote_user Update role of user
 * @apiName promote_user
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update user's information by basic fields
 *
 * @apiPermission AdminOnly
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {Number} userId The if of specific user
 * @apiParam {String[]} roles The role will be updated
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} login Get roles of user
 * @apiName login
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get roles of user
 *
 * @apiParam {Number} grantType grantType of login request to identity system (in this system is "oauth")<br><code>required</code>
 * @apiParam {Number} grantType grantData contain code (return from identity) and callbackUrl <br><code>required</code>
 *
 * @apiParamExample {json} Param example
 * {
 * "action":"login",
 * "grantType": "oauth",
 * "grantData": ["abc", "localhost:3002"]
 * }
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} result.userId Unique ID of the record user in system
 * @apiSuccess {Array[]} result.userRole roles of the user
 * @apiSuccess {String} result.tokenData token data of the user
 * @apiSuccess {String} result.username username of the user
 * @apiSuccess {String} result.avatar avatar of the user
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - INVALID_GRANT_TYPE: the grant type is invalid
 * - USER_NOT_FOUND: the user isn't founded in system
 *
 * @apiSuccessExample {json} Success example:
 * {
 *    "success": true,
 *    "result":  {
 *           "userId": 1,
 *           "userRole": ["super admin"],
 *           "tokenData": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJncmFudFR5cGUiOiJjb2RlIiwiZGVsZWdhdGUiOjYsIm93bmVyIjoxLCJyZXF1ZXN0Ijo3MTMsImlhdCI6MTU0Mjc2NTY0MSwiZXhwIjoxNTQyNzg3MjQxfQ.Y6eOysoRxxUloL31CuNZ-SDxiQbp0OQ4sn2ffdpgDC-3n6SkGdZAf2tCUjgGkGCHrUNDcHVGSmiqTVxoKjXAI7Tt43q4zd0CgpetYNHDfA-HsDdMVfA1Gf90qInHQlkrMQeezCHpd9b3z9EsxQ5NEpaJzWGhxoCpz20UOMSAqNPaE_Bs327hoKapWwTkiolwSWEmp7gl8wAigjE-Qb1J0jJZdt5tlJWmKmYRPphsBafw6WAAJHWxEbwkTiA7jYi78lOKm8Dfm0g1nZI6R_fkpYTwW9Snib_Pz2zzXURTKD10_S7k4268BQAZd9KjJubk1hcXHljb-S_sDGWklRgyWQ",
 *           "username": "admin",
 *           "avatar": "dev-identity-bap-jp/profile-photos/1"
 *      }
 *   ]
 * }
 */

/**
 * @api {ACTION} update_owned_password Update password for loggedUser
 * @apiName update_owned_password
 * @apiGroup Account
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update password for loggedUser by oldPassword and newPassword
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String{6..30}} oldPassword
 * - At least one letter, one capital letter, one special symbol
 * @apiParam {String{6..30}} newPassword
 * - At least one letter, one capital letter, one special symbol
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 * - USER_NOT_FOUND: the user with username does not exist
 * - WRONG_PASSWORD_ERROR: the Old Password is not correct
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} update_owned_info Update info of loggedUser
 * @apiName update_owned_info
 * @apiGroup Account
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update info of loggedUser
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} firstName The Firstname of logged User
 * @apiParam {String} lastName The Lastname of logged User
 * @apiParam {Number} birthday The Date of birth of logged User
 * @apiParam {Number} gender The Gender of logged User
 * @apiParam {String} email The Email of logged User
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} read_owned_info View info of loggedUser
 * @apiName read_owned_info
 * @apiGroup Account
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * View info of logged User
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String[]} info An array including information of logged User. Available values are:
 * - <code>'id'</code>: the id of user.
 * - <code>'username'</code>: the username of user
 * - <code>'firstName'</code>: the firstName of user
 * - <code>'lastName'</code>: the lastName of user
 * - <code>'gender'</code>: the gender of user
 * - <code>'birthday'</code>: the birthday of user
 * - <code>'email'</code>: the email of user
 * - <code>'isActive'</code>: the status of user
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {String} result.id The id of logged user
 * @apiSuccess {String} result.firstName The Firstname of logged user
 * @apiSuccess {String} result.lastName The Lastname of logged user
 * @apiSuccess {Number} result.birthday The Date of birth of logged user
 * @apiSuccess {Number} result.gender The User's logged gender
 * @apiSuccess {String} result.email The Email of logged user
 * @apiSuccess {Boolean} result.isActive The status of logged user
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 * - INVALID_PARAM_PROVIDED: the username and password does not comply with the policy
 *
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      success: "true"
 * }
 *
 */

/**
 * @api {ACTION} create_oauth_app Create oauth app
 * @apiName create_oauth_app
 * @apiGroup OAuthApp
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Register an OAuth app that belongs to an user
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} name The unique OAuth app name
 * @apiParam {String[]} callbackUrls The callback urls to redirect when the OAuth app is granted access
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} data The success result object
 * @apiSuccess {String} data.id Id of the newly created app
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - DUPLICATED_ERROR: The name for OAuth app had been chosen by an other user.
 * - INVALID_CALLBACK_URLS: The supplied callback urls are malformed
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true",
 *      "data": {
 *          "id": "kwjqju1287jwh1kuyw1"
 *      }
 * }
 *
 */

/**
 * @api {ACTION} update_owned_oauth_app Create oauth app
 * @apiName update_owned_oauth_app
 * @apiGroup OAuthApp
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Update an OAuth app that belongs to an user
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} appId The id of the OAuth app
 * @apiParam {String} [name] The new name of the OAuth app
 * @apiParam {String[]} [callbackUrls] The new callback urls
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_FOUND_ERROR: The OAuth app can not be found
 * - DUPLICATED_ERROR: The name for OAuth app had been chosen by an other user.
 * - INVALID_CALLBACK_URLS: The supplied callback urls are malformed
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true",
 * }
 *
 */

/**
 * @api {ACTION} remove_owned_oauth_app Remove oauth app
 * @apiName remove_owned_oauth_app
 * @apiGroup OAuthApp
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Remove an OAuth app that belongs to an user
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} appId The id of the OAuth app
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_FOUND_ERROR: The OAuth app can not be found
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true",
 * }
 *
 */

/**
 * @api {ACTION} get_owned_oauth_app Get oauth app
 * @apiName get_owned_oauth_app
 * @apiGroup OAuthApp
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get OAuth apps that belongs to an user
 *
 * @apiPermission LoggedInUser
 *
 * @apiUse AuthorizationHeader
 *
 * @apiParam {String} appId The id of the OAuth app
 * @apiParam {String[]} info
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} data The result object
 * @apiSuccess {String} data.id Id of the app
 * @apiSuccess {String} data.name Name of the app
 * @apiSuccess {String[]} data.callbackUrls Callback url of the app
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code Specific errors are:
 * - NOT_FOUND_ERROR: The OAuth app can not be found
 * - NOT_AUTHENTICATED_ERROR: no authorization token presents in the request header
 * - INVALID_TOKEN_ERROR: the token provided is not correct
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true",
 *      "data": {
 *          "id": "q2owuicyo12781jhdsh213g1j2h3g",
 *          "name": "googleMapAPI",
 *          "callbackUrls": "https://www.maps.google.com"
 *      }
 * }
 *
 */

/**
 * @api {ACTION} get_oauth_token Get access token
 * @apiName get_oauth_token
 * @apiGroup OAuth
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * Get an access token that can later be used to authenticate with the Service API Server.
 *
 * @apiParam {String} grantType The type of token granting. Available values are:
 * - <code>'password'</code>: the username/password authentication scheme.
 * - <code>'oauth'</code>: the token/provider authentication scheme.
 * - <code>'code'</code>: the OAuth authentication scheme.
 * - <code>'refresh'</code>: the refresh token authentication scheme.
 * - <code>'api_key'</code>: API key authentication scheme for 3rd party application
 * @apiParam {String[]} grantData The necessary data for token issuing, depending on <code>grantType</code>:
 * - [username, password] for <code>'password'</code> grantType
 * - [username, token, provider] for <code>'oauth'</code> grantType
 * - [refreshToken] for <code>'refresh'</code> grantType
 * - [code, appId, appKey, redirectUrl(optional)] for <code>'code'</code> grantType
 * - [appId, appKey] for <code>'api_key'</code> grantType
 * @apiParam {String} [scope] The access scope of the token. The scope restricts the commands that the access token can execute. The effect of scope must be implemented at the API service.
 * - <code>'full_scope'</code> The most powerful scope. The access token which is granted by this scope can call any command. Note: two-factor-authentication protected command still require TFA code to be executed.
 * - <code>'basic_scope'</code> The default and recommended scope for <code>'password'</code> or <code>'oauth'</code>. Within this type of scope, the token can invoke any basic command (non-related identity & setting changing).
 * Only request for <code>'full_scope'</code> when necessary.
 * - <code>'restricted_scope'</code> The token can invoke some very basic actions, like get user display name, user profile photo. This is recommended for most 3-rd party apps.
 * - <code>'app_scope'</code> The scope within which the token can only invoke "neutral" commands. Neutral commands are usually not state-changing actions and do not delegate to any user. This is used for <code>'api_key'</code> grant type only.
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} data The success object
 * @apiSuccess {String} data.token The access token
 * @apiSuccess {String} data.refreshToken The access refresh token to renew the access token when it expires
 * @apiSuccess {Number} data.tokenExpiration The Unix timestamp when the access token expires
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error Error object
 * @apiError {Object} error.code Possible values depend on <code>grantType</code>:
 * - Common error:
 *   - <code>'INVALID_GRANT_TYPE'</code>: The grant type is not recognizable.
 *   - <code>'INVALID_ACCESS_SCOPE'</code>: The scope value is invalid.
 
 * - Grant type <code>'password'</code>
 *   - <code>'USER_NOT_FOUND'</code>: Can not find the user with the provided <code>username</code>
 *   - <code>'AUTH_NOT_FOUND'</code>: Can not find the authentication scheme for this user
 *   - <code>'WRONG_PASSWORD_ERROR'</code>: the provided password is not correct
 
 * - Grant type <code>'oauth'</code>
 *   - <code>'USER_NOT_FOUND'</code>: Can not find the user with the provided <code>username</code>
 *   - <code>'AUTH_NOT_FOUND'</code>: Can not find the authentication scheme for this user
 *   - <code>'OAUTH_PROVIDER_NOT_SUPPORTED'</code>: The provided OAuth provider is not supported
 *   - <code>'INVALID_OAUTH_TOKEN'</code>: The provided token is not recognizable by the oauth provider or the UUID does not match the registered authentication.
 
 * - Grant type <code>'refresh'</code>
 *   - <code>'TOKEN_REVOKED'</code>: The refresh token is no longer valid, either expired or has been revoked
 
 * - Grant type <code>'code'</code>
 *   - <code>'OAUTH_REQUEST_NOT_FOUND'</code>: The OAuth request can not be found for provided <code>'code'</code> or the request has expired
 *   - <code>'APP_NOT_FOUND'</code>: Can not authenticate with the provided <code>appId</code> and <code>appKey</code>
 *   - <code>'SCOPE_NOT_ALLOWED'</code>: The scope is not allowed
 *   - <code>'CALLBACK_URL_NOT_MATCHED'</code>: Provided redirect url does not match with the app info.
 */

/**
 * @api {ACTION} create_oauth_request Generate oauth request
 * @apiName create_oauth_request
 * @apiGroup OAuth
 * @apiVersion 1.0.0
 *
 * @apiDescription Generate an OAuth request to be granted
 *
 * @apiParam {String} appId Id of the app that request the oauth access
 * @apiParam {String} userId Id of user
 * @apiParam {String} state The state of the OAuth request
 * @apiParam {String} scope The scope that the app request
 * @apiParam {String} redirectUrl The url for success redirect
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} data The result object
 * @apiSuccess {String} data.id The id of the generated request
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error The error object
 * @apiError {Object} error.code Possible values are:
 * - INVALID_ACCESS_SCOPE: The scope value is invalid.
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true",
 *      "data": {
 *          "id": "q2owuicyo12781jhdsh213g1j2h3g"
 *      }
 * }
 */

/**
 * @api {ACTION} grant_owned_oauth_request Grant oauth request
 * @apiName grant_owned_oauth_request
 * @apiGroup OAuth
 * @apiVersion 1.0.0
 *
 * @apiDescription Grant an OAuth request
 *
 * @apiParam {String} requestId Id of the request to be granted
 * @apiParam {String} isAccepted <code>true</code> to accept and <code>false</code> to reject the request
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error The error object
 * @apiError {Object} error.code Possible values are:
 * - <code>'NOT_FOUND_ERROR'</code>: The OAuth request can not be found
 *
 * @apiSuccessExample {json} Success example:
 * {
 *      "success": "true"     
 * }
 */

/**
 * @api {ACTION} get_project_alert_subscription Get project alert subscription
 * @apiName get_project_alert_subscription
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription Get user setting of project alert subscription. Null will be returned in result if the subscription is not found.
 *
 * @apiParam {String} projectId Id of the project
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} id Id of the subscription
 * @apiSuccess {Number} projectId Id of current project
 * @apiSuccess {Number} userId The current user
 * @apiSuccess {Number[]} environmentIds Array of environment ids of the project that user subscribed to alert
 * @apiSuccess {Number[]} methodIds Array of types of method that user wants to receive notification
 *
 */

/**
 * @api {ACTION} update_project_alert_subscription Update project alert subscription
 * @apiName update_project_alert_subscription
 * @apiGroup Project
 * @apiVersion 1.0.0
 *
 * @apiDescription Update user setting of project alert subscription. If there are no subscription then insert new.
 *
 * @apiParam {String} projectId Id of the project
 * @apiParam {Number[]]} [environmentIds] Ids of the environments to subscribe
 * @apiParam {Number[]} [methodIds] Ids of the method, 1 for email and 2 for SMS
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Number} id Id of the subscription
 * @apiSuccess {Number} projectId Id of current project
 * @apiSuccess {Number} userId The current user
 * @apiSuccess {Number[]} environmentIds Array of environment ids of the project that user subscribed to alert
 * @apiSuccess {Number[]} methodIds Array of types of method that user wants to receive notification
 *
 */