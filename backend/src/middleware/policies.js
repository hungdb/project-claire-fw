import { errors, jsonError } from '../utils/system';
import { UserRole } from '../models/schema/UserRole';
import { User } from '../models/schema/User';
import { Role } from '../models/schema/Role';

const isSuperAdmin = () => {
  return async (req, res, next) => {
    if (!req.principal || !req.principal.owner) return res.json(jsonError(errors.NOT_AUTHENTICATED_ERROR));
    let user = await User.findByPk(req.principal.owner);
    if (!user) return res.json(jsonError(errors.USER_NOT_FOUND));
    let superAdmin = await UserRole.findOne({
      where: { userId: user.id },
      include: [{ model: Role, where: { name: getEnv('SUPER_ADMIN_ROLE') } }]
    });
    if (!superAdmin) return res.json(jsonError(errors.PERMISSION_DENIED));
    return next();
  };
};

const requireToken = () => {
  return async (req, res, next) => {
    if (!req.principal || (!req.principal.owner && !req.principal.delegate))
      return res.json(jsonError(errors.NOT_AUTHENTICATED_ERROR));
    return next();
  };
};

export { isSuperAdmin, requireToken };
