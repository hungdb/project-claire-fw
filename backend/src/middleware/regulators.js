import { logger } from '../utils/system';
import { CronService } from '../services/CronService';
import { JWTService } from '../services/JWTService';

const requestLogger = () => {
  return (req, res, next) => {
    if (['GET', 'POST', 'PUT', 'DELETE'].indexOf(req.method) >= 0)
      logger.verbose({
        url: req.url,
        action: req.body && req.body.action,
        params: req.body
      });
    next();
  };
};

const parseToken = () => {
  return (req, res, next) => {
    let authorization = req.header('authorization');
    if (!authorization) {
      req.principal = {};
      return next();
    }

    JWTService.decodeToken({
      token: authorization.indexOf('Bearer ') === 0 ? authorization.substring('Bearer '.length) : authorization
    }).then(result => {
      // if (result.success && result.result.owner && !!CronService.DATA.userData[result.result.owner]) {
      if (result.success) {
        req.principal = result.result;
      } else {
        req.principal = {};
      }
      return next();
    });
  };
};

const uploadFileToBody = () => {
  return (req, res, next) => {
    if (req.files) {
      let keys = Object.keys(req.files);
      for (let i = 0; i < keys.length; i++) {
        req.body[keys[i]] = req.files[keys[i]];
      }
    }
    return next();
  };
};

const checkPermissions = () => {
  return async (req, res, next) => {
    let result = CronService.DATA.unauthenticatedPermissions || [];

    // console.log("check permission principal", req.principal);

    if (!!req.principal.owner) {
      result = (CronService.DATA.userPermissions[req.principal.owner] || []).concat(result);
    } else if (!!req.principal.delegate) {
      result = (CronService.DATA.appPermissions || []).concat(result);
    }

    req.principal.permissions = result;
    return next();
  };
};

export default [requestLogger(), parseToken(), uploadFileToBody(), checkPermissions()];
