import { isValidObjectId } from '../utils/validation';
import { errors, jsonError } from '../utils/system';
import { config } from '../core/config';
import { calculateLength } from '../utils/common';
import Joi from 'joi';

const GRANT_TYPES = {
  OAUTH: 'oauth'
};

const ACCESS_SCOPES = {};

const validation = object => {
  return (req, res, next) => {
    const result = Joi.validate(
      req.body,
      Joi.object()
        .keys(object)
        .options({ allowUnknown: true })
    );
    const action = req.body && req.body.action;
    if (result.error) {
      const firstError = calculateLength(result.error.details) ? result.error.details[0].message.replace(/"/g, '') : '';
      return res.json(
        jsonError(errors.MISSING_REQUIRED_VALUE, {
          action,
          message: firstError
        })
      );
    }
    req.body = result.value;
    return next();
  };
};

const valueRequired = ({ attributes }) => {
  return (req, res, next) => {
    let missing = attributes.some(attr => {
      return !req.body || req.body[attr] === '' || req.body[attr] === undefined || req.body[attr] === null;
    });
    if (missing) return res.json(jsonError(errors.MISSING_REQUIRED_VALUE));
    return next();
  };
};

const validObjectId = ({ attributes }) => {
  return (req, res, next) => {
    let invalid = attributes.some(attr => {
      if (!req.body || !req.body[attr]) return false;
      return !isValidObjectId(req.body[attr].toString());
    });
    if (invalid) return res.json(jsonError(errors.NOT_VALID_ID));
    return next();
  };
};

const validScope = ({ attributes }) => {
  const scopes = Object.keys(ACCESS_SCOPES).map(key => ACCESS_SCOPES[key]);
  return (req, res, next) => {
    let invalid = attributes.some(attr => {
      if (!req.body || !req.body[attr]) return false;
      return scopes.indexOf(req.body[attr]) === -1;
    });
    if (invalid) return res.json(jsonError(errors.INVALID_ACCESS_SCOPE));
    return next();
  };
};

const validGrantType = ({ attributes }) => {
  const types = Object.keys(GRANT_TYPES).map(key => GRANT_TYPES[key]);
  return (req, res, next) => {
    let invalid = attributes.some(attr => {
      if (!req.body || !req.body[attr]) return false;
      return types.indexOf(req.body[attr]) === -1;
    });
    if (invalid) return res.json(jsonError(errors.INVALID_GRANT_TYPE));
    return next();
  };
};

const validFile = ({ attributes }) => {
  return (req, res, next) => {
    let missing = attributes.some(attr => {
      let flag = true;
      if (
        !req.files ||
        req.files[attr] === '' ||
        req.files[attr] === undefined ||
        req.files[attr] === null ||
        typeof req.files[attr] !== 'object'
      )
        flag = false;

      if (flag) {
        const files = req.files[attr];
        if (!Array.isArray(files) && !config.fileUpload.settings.mimes.includes(files.mimetype)) {
          return true;
        } else {
          for (let i = 0; i < files.length; i++) {
            if (!config.fileUpload.settings.mimes.includes(files[i].mimetype)) {
              return true;
            }
          }
        }
      }
      return false;
    });
    if (missing) return res.json(jsonError(errors.INVALID_FILE));
    return next();
  };
};

const valueInArrayObjectRequired = ({ array, attributes }) => {
  return (req, res, next) => {
    let missing = true;
    if (req.body && Array.isArray(req.body[array]) && req.body[array].length > 0) {
      missing = req.body[array].some(item => {
        return attributes.some(attr => {
          return !item || item[attr] === '' || item[attr] === undefined || item[attr] === null;
        });
      });
    }
    if (missing) return res.json(jsonError(errors.MISSING_REQUIRED_VALUE));
    return next();
  };
};

const valueInArrayRequired = ({ attributes }) => {
  return (req, res, next) => {
    let missing = true;
    if (req.body) {
      missing = attributes.some(attr => {
        if (!Array.isArray(req.body[attr]) || req.body[attr].length === 0) return true;
        return req.body[attr].some(item => {
          return item === '' || item === undefined || item === null;
        });
      });
    }
    if (missing) return res.json(jsonError(errors.MISSING_REQUIRED_VALUE));
    return next();
  };
};

const validObjectIdInArrayObject = ({ array, attributes }) => {
  return (req, res, next) => {
    let invalid = true;
    if (req.body && Array.isArray(req.body[array]) && req.body[array].length > 0) {
      invalid = req.body[array].some(item => {
        return attributes.some(attr => {
          return !isValidObjectId(item && item[attr].toString());
        });
      });
    }
    if (invalid) return res.json(jsonError(errors.NOT_VALID_ID));
    return next();
  };
};

const hasAtLeastOneValue = ({ attributes }) => {
  return (req, res, next) => {
    let missing = true;
    if (req.body) {
      const hasAtLeastOneValue = attributes.some(attr => {
        const item = req.body[attr];
        return item !== '' && item !== undefined && item !== null;
      });
      missing = !hasAtLeastOneValue;
    }
    if (missing) return res.json(jsonError(errors.MISSING_REQUIRED_VALUE));
    return next();
  };
};

export {
  valueRequired,
  validObjectId,
  validScope,
  validGrantType,
  validFile,
  valueInArrayObjectRequired,
  valueInArrayRequired,
  validObjectIdInArrayObject,
  hasAtLeastOneValue,
  validation
};
