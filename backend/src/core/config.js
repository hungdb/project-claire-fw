import path from 'path';
import { allowMimeTypes } from '../utils/mime_types';

const config = {
  //-- see express-session for more options
  session: {
    enabled: false,
    settings: {
      secret: 'replace this for a more secure secret',
      resave: false,
      saveUninitialized: true,
      cookie: { secure: true }
    }
  },

  //-- see socket.io for more options
  socketIO: {
    enabled: true,
    settings: {
      jwt: {
        enabled: true,
        settings: {
          handshake: true
        }
      },
      redis: {
        enabled: true
      },
      options: {
        pingInterval: 10000,
        pingTimeout: 5000,
        cookie: false
      }
    }
  },

  static: {
    enabled: true,
    settings: [{ mount: '/apidoc', root: path.join(__dirname, './doc/dist'), options: {} }]
  },

  //-- see express-fileupload for more settings, file upload enable FormData parsing
  fileUpload: {
    enabled: true,
    settings: {
      mimes: allowMimeTypes,
      limits: {
        files: 3, //-- maximum number of files
        fileSize: 50 * 1024 * 1024 //-- max file size in byte
      }
    }
  },

  //-- see npm body-parser for more settings
  bodyParser: {
    enabled: true,
    settings: {
      json: { enabled: true, settings: {} },
      urlencoded: { enabled: true, settings: { extended: false } }
    }
  },

  CORS: {
    enabled: true,
    settings: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    }
  },

  logRotator: {
    enabled: true
  }
};

export { config };
