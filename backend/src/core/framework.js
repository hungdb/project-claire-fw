import { RPCController } from '../controllers/RPCControler';
import { CallbackController } from '../controllers/CallbackController';
import { NotFoundController } from '../controllers/NotFoundController';
import { UserService } from '../services/UserService';
import { CronService } from '../services/CronService';
import { ProjectService } from '../services/ProjectService';
import { RedisService } from '../services/RedisService';
import { NotificationService } from '../services/NotificationService';
import { TaskService } from '../services/TaskService';
import { PermissionSchemeService } from '../services/PermissionSchemeService';
import { ProjectRoleService } from '../services/ProjectRoleService';
import { SprintService } from '../services/SprintService';
import { WorkflowService } from '../services/WorkflowService';
import { AllocationService } from '../services/AllocationService';
import { S3Service } from '../services/S3Service';
import { JWTService } from '../services/JWTService';
import { CheckListService } from '../services/CheckListService';
import { SocketService } from '../services/SocketService';
import { SkillService } from '../services/SkillService';
import { IssueService } from '../services/IssueService';
import { FirebaseService } from '../services/FirebaseService';
import { BehaviorService } from '../services/BehaviorService';
import { AllocationLabelService } from '../services/AllocationLabelService';
import { SalePriorityService } from '../services/SalePriorityService';
import { KPICoefficientService } from '../services/KPICoefficientService';
import { SaleStatusService } from '../services/SaleStatusService';
import { SaleCategoryService } from '../services/SaleCategoryService';
import { SaleService } from '../services/SaleService';
import { AllocationPlanService } from '../services/AllocationPlanService';
import { MarketService } from '../services/MarketService';
import { CustomerService } from '../services/CustomerService';

import { s as userSchema } from '../models/schema/User';
import { s as groupSchema } from '../models/schema/Group';
import { s as projectSchema } from '../models/schema/Project';
import { s as permissionSchema } from '../models/schema/PermissionScheme';
import { s as permissionDetailSchema } from '../models/schema/PermissionSchemeDetail';
import { s as projectRoleSchema } from '../models/schema/ProjectRole';
import { s as roleSchema } from '../models/schema/Role';
import { s as rolePermissionSchema } from '../models/schema/RolePermission';
import { s as userRoleSchema } from '../models/schema/UserRole';
import { s as workflowSchema } from '../models/schema/Workflow';
import { s as workflowDetailSchema } from '../models/schema/WorkflowDetail';
import { s as workflowStateSchema } from '../models/schema/WorkflowState';
import { s as userProjectRoleSchema } from '../models/schema/UserProjectRole';
import { s as sprintSchema } from '../models/schema/Sprint';
import { s as taskSchema } from '../models/schema/Task';
import { s as allocationSchema } from '../models/schema/Allocation';
import { s as configurationSchema } from '../models/schema/Configuration';
import { s as environmentSchema } from '../models/schema/Environment';
import { s as taskStateSchema } from '../models/schema/TaskState';
import { s as documentSchema } from '../models/schema/Document';
import { s as alertSubscriptionSchema } from '../models/schema/AlertSubscription';
import { s as checkListItemSchema } from '../models/schema/CheckListItem';
import { s as checkListSchema } from '../models/schema/CheckList';
import { s as notificationSchema } from '../models/schema/Notification';
import { s as commentSchema } from '../models/schema/Comment';
import { s as attachmentSchema } from '../models/schema/Attachment';
import { s as dependencySchema } from '../models/schema/TaskDependency';
import { s as skillSchema } from '../models/schema/Skill';
import { s as userSkillSchema } from '../models/schema/UserSkill';
import { s as logAlertSchema } from '../models/schema/LogAlert';
import { s as userNoteSchema } from '../models/schema/UserNote';
import { s as taskTagSchema } from '../models/schema/TaskTag';
import { s as issueSchema } from '../models/schema/Issue';
import { s as behaviorSchema } from '../models/schema/Behavior';
import { s as logBehaviorSprintSchema } from '../models/schema/LogBehaviorSprint';
import { s as kpiSkillSchema } from '../models/schema/KPISkill';
import { s as kpiBehaviorSchema } from '../models/schema/KPIBehavior';
import { s as kpiSprintSchema } from '../models/schema/KPISprint';
import { s as logDetailBehaviorSchema } from '../models/schema/LogDetailBehavior';
import { s as allocationLabelSchema } from '../models/schema/AllocationLabel';
import { s as kpiCoefficientSchema } from '../models/schema/KPICoefficient';
import { s as saleStatusSchema } from '../models/schema/SaleStatus';
import { s as saleCategorySchema } from '../models/schema/SaleCategory';
import { s as salePrioritySchema } from '../models/schema/SalePriority';
import { s as saleSchema } from '../models/schema/Sale';
import { s as allocationPlanSchema } from '../models/schema/AllocationPlan';
import { s as marketSchema } from '../models/schema/Market';
import { s as customerSchema } from '../models/schema/Customer';

const environments = ['LCL', 'PRO', 'STG', 'DEV'];

const controllers = {
  RPCController,
  CallbackController,
  NotFoundController
};

const services = {
  JWTService,
  UserService,
  ProjectService,
  RedisService,
  NotificationService,
  PermissionSchemeService,
  ProjectRoleService,
  SprintService,
  WorkflowService,
  TaskService,
  AllocationService,
  CronService,
  CheckListService,
  S3Service,
  SocketService,
  SkillService,
  IssueService,
  FirebaseService,
  BehaviorService,
  AllocationLabelService,
  KPICoefficientService,
  SalePriorityService,
  SaleStatusService,
  SaleCategoryService,
  SaleService,
  AllocationPlanService,
  MarketService,
  CustomerService
};

const schemas = {
  userSchema,
  groupSchema,
  projectSchema,
  permissionSchema,
  permissionDetailSchema,
  projectRoleSchema,
  roleSchema,
  rolePermissionSchema,
  userRoleSchema,
  workflowSchema,
  workflowDetailSchema,
  workflowStateSchema,
  userProjectRoleSchema,
  sprintSchema,
  taskSchema,
  allocationSchema,
  configurationSchema,
  environmentSchema,
  taskStateSchema,
  // permissionOfRoleSchema,
  alertSubscriptionSchema,
  checkListItemSchema,
  checkListSchema,
  notificationSchema,
  commentSchema,
  attachmentSchema,
  documentSchema,
  dependencySchema,
  skillSchema,
  userSkillSchema,
  logAlertSchema,
  userNoteSchema,
  taskTagSchema,
  issueSchema,
  behaviorSchema,
  logBehaviorSprintSchema,
  kpiSkillSchema,
  kpiBehaviorSchema,
  kpiSprintSchema,
  logDetailBehaviorSchema,
  allocationLabelSchema,
  kpiCoefficientSchema,
  salePrioritySchema,
  saleStatusSchema,
  saleCategorySchema,
  saleSchema,
  allocationPlanSchema,
  marketSchema,
  customerSchema
};

export { environments, controllers, services, schemas };
