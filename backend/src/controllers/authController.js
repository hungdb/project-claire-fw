import axios from 'axios';
import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { errors, jsonError, jsonSuccess } from '../utils/system';
import { UserService } from '../services/UserService';
import { FirebaseService } from '../services/FirebaseService';
import { JWTService } from '../services/JWTService';

const GRANT_TYPES = {
  OAUTH: 'oauth',
  API_KEY: 'api_key'
};

module.exports = {
  login: {
    policies: [
      validation({
        grantType: Joi.string()
          .trim()
          .required()
          .valid(Object.values(GRANT_TYPES)),
        grantData: Joi.array()
          .required()
          .items(
            Joi.string()
              .trim()
              .required(),
            Joi.string()
              .trim()
              .required(),
            Joi.string()
              .trim()
              .required()
          )
          .length(3)
      })
    ],
    action: async (principal, params) => {
      switch (params.grantType) {
        case GRANT_TYPES.OAUTH: {
          let loginResult = await UserService.login({
            code: params.grantData[1],
            callbackUrl: params.grantData[2]
          });
          if (!loginResult.success) {
            return loginResult;
          }
          const tokenSocket = await JWTService.signToken({
            data: { userId: loginResult.result.userId }
          });
          if (!tokenSocket.success) {
            return tokenSocket;
          }
          return jsonSuccess(
            Object.assign(loginResult.result, {
              tokenSocket: tokenSocket.result
            })
          );
        }
        case GRANT_TYPES.API_KEY: {
        }
      }
    }
  },

  get_access_token: {
    policies: [
      validation({
        loginToken: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      //-- check login token against identity server
      //-- get access token
      let accessTokenResult = await axios.post(UserService.DATA.IDENTITY_URL, {
        action: 'get_access_token',
        loginToken: params.loginToken
      });

      if (!accessTokenResult.data['success']) {
        //-- this token had been revoked, user need re-login
        return jsonError(errors.TOKEN_EXPIRED);
      }

      //-- decode the loginToken and retrieve information
      let decoded = await JWTService.decodeIdentityLoginToken({
        token: params.loginToken
      });

      if (!decoded.success) {
        return decoded;
      }
      decoded = decoded.result;

      return JWTService.signToken({
        data: {
          owner: decoded.owner,
          delegate: decoded.delegate
        }
      });
    }
  },

  logout: {
    policies: [requireToken()],
    action: async (principal, params) => {
      const userResult = await UserService.getUserById({
        userId: principal.owner
      });
      if (!userResult.success) return jsonError(errors.USER_NOT_FOUND);

      const tokenFirebase = userResult.result && userResult.result.tokenFirebase;
      if (tokenFirebase) {
        let listAllTasksAndProjects = await UserService.listMyTasksAndProjects({
          userId: principal.owner
        });
        listAllTasksAndProjects = listAllTasksAndProjects.result;
        for (let i = 0; i < listAllTasksAndProjects.length; i++) {
          if (listAllTasksAndProjects[i].type === 'project')
            FirebaseService.unsubscribeFromTopic(tokenFirebase, `project_${listAllTasksAndProjects[i].value}`);
          else if (listAllTasksAndProjects[i].type === 'task')
            FirebaseService.unsubscribeFromTopic(tokenFirebase, `task_${listAllTasksAndProjects[i].value}`);
        }
      }
      return jsonSuccess();
    }
  }
};
