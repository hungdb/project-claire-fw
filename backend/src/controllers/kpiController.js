import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { KPI_TYPES } from '../utils/type';
import { BehaviorService } from '../services/BehaviorService';
import { SkillService } from '../services/SkillService';
import { SprintService } from '../services/SprintService';
import { TYPES } from '../models/schema/KPISprint';

module.exports = {
  get_list_kpi_to_chart: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        type: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: (principal, params) => {
      const { type } = params;
      switch (type) {
        case KPI_TYPES.skill:
          return SkillService.getListKpiSkill({
            userId: params.userId
          });
        case KPI_TYPES.projectMemberSprint:
          return SprintService.getListKpiSprint({
            userId: params.userId,
            type: TYPES.member
          });
        case KPI_TYPES.projectLeaderSprint:
          return SprintService.getListKpiSprint({
            userId: params.userId,
            type: TYPES.leader
          });
        default:
          return BehaviorService.getListKpiBehavior({
            userId: params.userId
          });
      }
    }
  },

  get_my_list_kpi_to_chart: {
    policies: [
      requireToken(),
      validation({
        type: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: (principal, params) => {
      const { type } = params;
      switch (type) {
        case KPI_TYPES.skill:
          return SkillService.getListKpiSkill({
            userId: principal.owner
          });
        case KPI_TYPES.projectMemberSprint:
          return SprintService.getListKpiSprint({
            userId: principal.owner,
            type: TYPES.member
          });
        case KPI_TYPES.projectLeaderSprint:
          return SprintService.getListKpiSprint({
            userId: principal.owner,
            type: TYPES.leader
          });
        default:
          return BehaviorService.getListKpiBehavior({
            userId: principal.owner
          });
      }
    }
  }
};
