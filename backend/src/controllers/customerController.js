import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { CustomerService } from '../services/CustomerService';
import { config } from '../core/config';
import { CUSTOMER_STATUS } from '../models/schema/Customer';
import { S3Service } from '../services/S3Service';
import { allowMimeTypesByType } from '../utils/mime_types';

module.exports = {
  create_customer: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string().required(),
        marketId: Joi.number()
          .integer()
          .min(1)
          .required(),
        note: Joi.string().allow(['', null]),
        company: Joi.string().allow([''], null),
        status: Joi.number()
          .integer()
          .valid(CUSTOMER_STATUS.ACTIVE, CUSTOMER_STATUS.INACTIVE),
        avatar: Joi.object({
          data: Joi.binary().required(),
          mimetype: Joi.string()
            .trim()
            .required()
            .valid(allowMimeTypesByType('image'))
        }),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(allowMimeTypesByType('image'))
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
      })
    ],
    action: async (principal, params) => {
      return CustomerService.createCustomer({
        name: params.name,
        marketId: params.marketId,
        note: params.note,
        company: params.company,
        status: params.status,
        files: params.files,
        avatar: params.avatar
      });
    }
  },

  update_customer: {
    policies: [
      requireToken(),
      validation({
        id: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string().allow(['', null]),
        marketId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        note: Joi.string().allow([''], null),
        company: Joi.string().allow([''], null),
        status: Joi.number()
          .integer()
          .valid(CUSTOMER_STATUS.ACTIVE, CUSTOMER_STATUS.INACTIVE),
        avatar: Joi.object({
          data: Joi.binary().required(),
          mimetype: Joi.string()
            .trim()
            .required()
            .valid(allowMimeTypesByType('image'))
        })
      })
    ],
    action: async (principal, params) => {
      return CustomerService.updateCustomer({
        id: params.id,
        name: params.name,
        marketId: params.marketId,
        note: params.note,
        company: params.company,
        status: params.status,
        avatar: params.avatar
      });
    }
  },

  remove_customer: {
    policies: [
      requireToken(),
      validation({
        id: Joi.number().required()
      })
    ],
    action: async (principal, params) => {
      return CustomerService.removeCustomer({
        id: params.id
      });
    }
  },

  get_list_customer: {
    policies: [
      requireToken(),
      validation({
        page: Joi.number()
          .integer()
          .min(1)
          .allow([''], null),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow([''], null),
        name: Joi.string().allow([''], null),
        marketId: Joi.number()
          .integer()
          .min(1)
          .allow([''], null),
        company: Joi.string().allow([''], null),
        status: Joi.number().valid(CUSTOMER_STATUS.ACTIVE, CUSTOMER_STATUS.INACTIVE)
      })
    ],
    action: async (principal, params) => {
      return CustomerService.getListCustomer({
        page: params.page,
        limit: params.limit,
        name: params.name,
        marketId: params.marketId,
        company: params.company,
        status: params.status
      });
    }
  },

  add_attachment_by_customer: {
    policies: [
      requireToken(),
      validation({
        customerId: Joi.number()
          .integer()
          .required()
          .min(1),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(allowMimeTypesByType('image'))
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
          .required()
      })
    ],
    action: async (principal, params) => {
      return CustomerService.addAttachment({
        customerId: params.customerId,
        files: params.files,
        userId: principal.owner
      });
    }
  },

  remove_attachment_by_customer: {
    policies: [
      requireToken(),
      validation({
        attachmentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return CustomerService.removeAttachment({
        attachmentId: params.attachmentId
      });
    }
  },

  get_list_attachment_by_customer: {
    policies: [
      requireToken(),
      validation({
        customerId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return CustomerService.getListAttachment({
        customerId: params.customerId
      });
    }
  },

  get_signed_url_customer: {
    policies: [
      requireToken(),
      validation({
        filePath: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return await S3Service.getSignedUrl({
        bucket: getEnv('S3_BUCKET'),
        filePath: params.filePath
      });
    }
  }
};
