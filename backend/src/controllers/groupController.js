import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { ProjectService } from '../services/ProjectService';

module.exports = {
  // Group
  create_group: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        isActive: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return ProjectService.createGroup({
        name: params.name,
        isActive: !!params.isActive
      });
    }
  },

  update_group: {
    policies: [
      requireToken(),
      validation({
        groupId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        isActive: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return ProjectService.updateGroup({
        groupId: params.groupId,
        name: params.name,
        isActive: !!params.isActive
      });
    }
  },

  remove_group: {
    policies: [
      requireToken(),
      validation({
        groupId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.deleteGroup({
        groupId: params.groupId
      });
    },
    log: true
  },

  get_group: {
    policies: [
      requireToken(),
      validation({
        groupId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      if (!params) params = {};
      return ProjectService.getGroup({
        groupId: params.groupId
      });
    }
  },

  list_groups: {
    policies: [requireToken()],
    action: async principal => {
      return ProjectService.listGroups();
    }
  },

  list_my_project_groups: {
    policies: [requireToken()],
    action: async principal => {
      return ProjectService.listMyGroups({
        userId: principal.owner
      });
    }
  },

  toggle_active_group: {
    policies: [
      requireToken(),
      validation({
        groupId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.toggleIsActiveGroup({
        groupId: params.groupId
      });
    }
  }
};
