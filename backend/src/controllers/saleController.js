import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { S3Service } from '../services/S3Service';
import { SaleService } from '../services/SaleService';
import { config } from '../core/config';

module.exports = {
  create_sale: {
    policies: [
      requireToken(),
      validation({
        customerId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        categoryId: Joi.number()
          .integer()
          .required()
          .min(1),
        priorityId: Joi.number()
          .integer()
          .required()
          .min(1),
        statusId: Joi.number()
          .integer()
          .required()
          .min(1),
        pic: Joi.number()
          .integer()
          .required()
          .min(1),
        company: Joi.string()
          .trim()
          .allow(['', null]),
        appointmentStartDate: Joi.date().required(),
        appointmentPlace: Joi.string()
          .trim()
          .required(),
        discussionContent: Joi.string()
          .trim()
          .allow(['', null]),
        memoMeeting: Joi.string()
          .trim()
          .allow(['', null]),
        memoProgress: Joi.string()
          .trim()
          .allow(['', null]),
        otherMemo: Joi.string()
          .trim()
          .allow(['', null]),
        contact: Joi.string()
          .trim()
          .allow(['', null]),
        appointmentEndDate: Joi.date()
          .required()
          .min(Joi.ref('appointmentStartDate')),
        note: Joi.string()
          .trim()
          .allow(['', null]),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
      })
    ],
    action: async (principal, params) => {
      return await SaleService.createSale({
        categoryId: params.categoryId,
        company: params.company,
        discussionContent: params.discussionContent,
        contact: params.contact,
        priorityId: params.priorityId,
        statusId: params.statusId,
        memoProgress: params.memoProgress,
        pic: params.pic,
        memoMeeting: params.memoMeeting,
        appointmentStartDate: params.appointmentStartDate,
        appointmentPlace: params.appointmentPlace,
        otherMemo: params.otherMemo,
        actor: principal.owner,
        appointmentEndDate: params.appointmentEndDate,
        note: params.note,
        files: params.files,
        customerId: params.customerId
      });
    }
  },

  update_sale: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1),
        categoryId: Joi.number()
          .integer()
          .required()
          .min(1),
        priorityId: Joi.number()
          .integer()
          .required()
          .min(1),
        statusId: Joi.number()
          .integer()
          .required()
          .min(1),
        pic: Joi.number()
          .integer()
          .required()
          .min(1),
        company: Joi.string()
          .trim()
          .allow(['', null]),
        appointmentStartDate: Joi.date().required(),
        appointmentPlace: Joi.string()
          .trim()
          .required(),
        discussionContent: Joi.string()
          .trim()
          .allow(['', null]),
        memoMeeting: Joi.string()
          .trim()
          .allow(['', null]),
        memoProgress: Joi.string()
          .trim()
          .allow(['', null]),
        otherMemo: Joi.string()
          .trim()
          .allow(['', null]),
        contact: Joi.string()
          .trim()
          .allow(['', null]),
        appointmentEndDate: Joi.date()
          .required()
          .min(Joi.ref('appointmentStartDate')),
        note: Joi.string()
          .trim()
          .allow(['', null]),
        customerId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SaleService.updateSale({
        saleId: params.saleId,
        categoryId: params.categoryId,
        company: params.company,
        discussionContent: params.discussionContent,
        contact: params.contact,
        priorityId: params.priorityId,
        statusId: params.statusId,
        memoProgress: params.memoProgress,
        pic: params.pic,
        memoMeeting: params.memoMeeting,
        appointmentStartDate: params.appointmentStartDate,
        appointmentPlace: params.appointmentPlace,
        otherMemo: params.otherMemo,
        actor: principal.owner,
        appointmentEndDate: params.appointmentEndDate,
        note: params.note,
        customerId: params.customerId
      });
    }
  },

  remove_sale: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SaleService.removeSale({
        saleId: params.saleId
      });
    }
  },

  get_list_sale: {
    policies: [
      requireToken(),
      validation({
        categoryId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        priorityId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        statusId: Joi.number()
          .integer()
          .min(1)
          .allow(['', -1, null]),
        pic: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        start: Joi.date().allow(['', null]),
        end: Joi.date().allow(['', null]),
        page: Joi.number()
          .integer()
          .required()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .required()
          .min(1)
          .allow(['', null]),
        keywords: Joi.string()
          .trim()
          .allow(['', null]),
        customerId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      let filter = {
        categoryId: params.categoryId,
        priorityId: params.priorityId,
        statusId: params.statusId,
        pic: params.pic,
        keywords: params.keywords,
        start: params.start,
        end: params.end,
        customerId: params.customerId
      };
      return SaleService.getListSale({
        filter,
        page: params.page || 1,
        limit: params.limit || 10
      });
    }
  },

  detail_sale: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SaleService.detailSale({
        saleId: params.saleId
      });
    }
  },

  add_attachment_by_sale: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
          .required()
      })
    ],
    action: async (principal, params) => {
      return SaleService.addAttachment({
        saleId: params.saleId,
        files: params.files,
        userId: principal.owner
      });
    }
  },

  remove_attachment_by_sale: {
    policies: [
      requireToken(),
      validation({
        attachmentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SaleService.removeAttachment({
        attachmentId: params.attachmentId
      });
    }
  },

  list_attachment_by_sale: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SaleService.listAttachmentBySale({
        saleId: params.saleId,
        limit: params.limit || 10,
        offset: params.offset || 0
      });
    }
  },
  get_signed_url_sale: {
    policies: [
      requireToken(),
      validation({
        filePath: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return await S3Service.getSignedUrl({
        bucket: getEnv('S3_BUCKET'),
        filePath: params.filePath
      });
    }
  }
};
