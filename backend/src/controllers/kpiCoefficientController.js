import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { KPICoefficientService } from '../services/KPICoefficientService';
import { KPI_TYPE } from '../models/schema/KPICoefficient';

module.exports = {
  update_kpi_coefficient: {
    policies: [
      requireToken(),
      validation({
        id: Joi.number()
          .integer()
          .required()
          .min(1),
        data: Joi.object().required()
      })
    ],
    action: async (principal, params) => {
      return await KPICoefficientService.updateKPICoefficient({
        id: params.id,
        data: params.data
      });
    }
  },

  get_kpi_coefficients: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return KPICoefficientService.getKPICoefficients();
    }
  },

  get_detail_kpi_coefficient: {
    policies: [
      requireToken(),
      validation({
        type: Joi.number()
          .integer()
          .required()
          .valid(Object.values(KPI_TYPE))
      })
    ],
    action: async (principal, params) => {
      return KPICoefficientService.getDetailKPICoefficient({
        type: params.type
      });
    }
  }
};
