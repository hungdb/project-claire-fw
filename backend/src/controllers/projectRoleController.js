import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { projectDepend } from '../utils/rpc_conditions';
import { validation } from '../middleware/validators';
import { ProjectRoleService } from '../services/ProjectRoleService';
import { SocketService } from '../services/SocketService';
import { Notification, NOTIFICATION_TYPE } from '../models/schema/Notification';
import { CronService } from '../services/CronService';
import { getTokenFirebase, truncateString } from '../utils/common';
import { FirebaseService } from '../services/FirebaseService';

module.exports = {
  //ProjectRole
  create_project_role: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        priorityLevel: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return ProjectRoleService.createProjectRole({
        name: params.name,
        priorityLevel: params.priorityLevel
      });
    }
  },

  update_project_role: {
    policies: [
      requireToken(),
      validation({
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        priorityLevel: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return ProjectRoleService.updateProjectRole({
        name: params.name,
        projectRoleId: params.projectRoleId,
        priorityLevel: params.priorityLevel
      });
    }
  },

  remove_project_role: {
    policies: [
      requireToken(),
      validation({
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectRoleService.deleteProjectRole({
        projectRoleId: params.projectRoleId
      });
    }
  },

  list_project_role: {
    policies: [requireToken()],
    action: async principal => {
      return ProjectRoleService.listProjectRole();
    }
  },

  list_permission_of_role_project: {
    policies: [
      requireToken(),
      validation({
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectRoleService.listPermissionsOfRoleProject({
        projectRoleId: params.projectRoleId
      });
    }
  },

  add_user_into_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleIds: Joi.array().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await ProjectRoleService.addUserIntoProject({
        projectId: params.projectId,
        userId: params.userId,
        projectRoleIds: params.projectRoleIds
      });
      if (result.success) {
        const project = result.result;
        let actorInfo = CronService.DATA.userData[principal.owner];
        let userInfo = CronService.DATA.userData[params.userId];
        let data = {
          subType: 'add_user_to_project',
          project: {
            id: project.id,
            name: project.name
          },
          actor: {
            id: principal.owner,
            name: actorInfo && actorInfo.username,
            avatar: actorInfo && actorInfo.profilePhoto
          },
          content: `You was added to project ${project.name} by ${actorInfo && actorInfo.username}`
        };

        Notification.create({
          actor: principal.owner,
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT,
          type: 'change_role_project',
          content: `User ${userInfo && userInfo.username} was added to project ${project.name}`,
          data: JSON.stringify(data)
        });
        SocketService.joinRoom({
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT
        });
        SocketService.notificationToSpecificUser({
          userId: params.userId,
          type: 'change_role_project',
          data: data
        });

        // web push firebase
        const token = await getTokenFirebase(params.userId);
        const tokenActor = await getTokenFirebase(principal.owner);
        FirebaseService.subscribeToTopic(token, `project_${project.id}`);
        FirebaseService.subscribeToTopic(tokenActor, `project_${project.id}`);
        FirebaseService.sendToDevice({
          data: {
            title: truncateString(`Project member changed`, 20),
            body: truncateString(`${actorInfo && actorInfo.username} has added you to the project ${project.name}`, 30),
            click_action: `${getEnv('DOMAIN')}projects/${project.groupId}/${project.id}`,
            icon: `${getEnv('LINK_ICON')}${actorInfo && actorInfo.profilePhoto}`
          },
          registrationToken: token
        });
      }
      return result;
    }
  },

  add_all_user_into_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleIds: Joi.array().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectRoleService.addMultipleUserIntoProject({
        projectId: params.projectId,
        projectRoleIds: params.projectRoleIds
      });
    }
  },

  list_users_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectRoleService.listUsersByProject({
        projectId: params.projectId
      });
    }
  },

  update_role_user_in_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleIds: Joi.array().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await ProjectRoleService.updateRoleUserInProject({
        projectId: params.projectId,
        userId: params.userId,
        projectRoleIds: params.projectRoleIds
      });
      if (result.success) {
        const project = result.result;
        SocketService.joinRoom({
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT
        });
        let actor = CronService.DATA.userData[principal.owner];
        let user = CronService.DATA.userData[params.userId];
        let data = {
          subType: 'update_project_role_of_user',
          project: {
            id: project.id,
            name: project.name
          },
          actor: {
            id: principal.owner,
            name: actor && actor.username,
            avatar: actor && actor.profilePhoto
          },
          content: `You was changed role in project ${project.name} by ${actor && actor.username}!`
        };

        Notification.create({
          actor: principal.owner,
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT,
          type: 'change_role_project',
          content: `User ${user && user.username} was changed role in project ${project.name}`,
          data: JSON.stringify(data)
        });

        SocketService.notificationToSpecificUser({
          userId: params.userId,
          type: 'change_role_project',
          data: data
        });
        // web push firebase
        const token = await getTokenFirebase(params.userId);
        const tokenActor = await getTokenFirebase(principal.owner);
        FirebaseService.subscribeToTopic(token, `project_${project.id}`);
        FirebaseService.subscribeToTopic(tokenActor, `project_${project.id}`);
        FirebaseService.sendToDevice({
          data: {
            title: truncateString(`Project member changed`, 20),
            body: truncateString(`${actor && actor.username} has added you to the project ${project.name}`, 30),
            click_action: `${getEnv('DOMAIN')}projects/${project.groupId}/${project.id}`,
            icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
          },
          registrationToken: token
        });
      }
      return result;
    }
  },

  remove_user_from_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await ProjectRoleService.deleteUserProject({
        userId: params.userId,
        projectId: params.projectId
      });
      if (result.success) {
        const project = result.result;
        let actor = CronService.DATA.userData[principal.owner];
        let user = CronService.DATA.userData[params.userId];
        let data = {
          subType: 'remove_user_from_project',
          project: {
            id: project.id,
            name: project.name
          },
          actor: {
            id: principal.owner,
            name: actor && actor.username,
            avatar: actor && actor.profilePhoto
          },
          content: `You was removed from project ${project.name} by ${actor && actor.username}!`
        };

        Notification.create({
          actor: principal.owner,
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT,
          type: 'change_role_project',
          content: `User ${user && user.username} was removed from project ${project.name}`,
          data: JSON.stringify(data)
        });

        SocketService.notificationToSpecificUser({
          userId: params.userId,
          type: 'change_role_project',
          data: data
        });

        SocketService.leaveRoom({
          userId: params.userId,
          notificationableId: project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT
        });

        // web push firebase
        const tokenActor = await getTokenFirebase(principal.owner);
        FirebaseService.subscribeToTopic(tokenActor, `project_${project.id}`);
        const token = await getTokenFirebase(params.userId);
        FirebaseService.unsubscribeFromTopic(token, `project_${project.id}`);
        FirebaseService.sendToDevice({
          data: {
            title: truncateString(`Project member changed`, 20),
            body: truncateString(`${actor && actor.username} has added you to the project ${project.name}`, 30),
            click_action: `${getEnv('DOMAIN')}projects/${project.groupId}/${project.id}`,
            icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
          },
          registrationToken: token
        });
      }
      return result;
    }
  },

  list_managers_by_userId: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectRoleService.getListManagersByUser({
        userId: params.userId
      });
    }
  }
};
