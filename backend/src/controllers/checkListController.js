import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { CheckListService } from '../services/CheckListService';
import { projectDepend } from '../utils/rpc_conditions';
import { CheckList } from '../models/schema/CheckList';

module.exports = {
  // Checklist
  create_item_check: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return CheckListService.createItemCheck({
        name: params.name,
        description: params.description
      });
    }
  },

  update_item_check: {
    policies: [
      requireToken(),
      validation({
        itemCheckId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return CheckListService.updateItemCheck({
        itemCheckId: params.itemCheckId,
        name: params.name,
        description: params.description
      });
    }
  },

  list_all_item_check: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return CheckListService.listAllItem();
    }
  },

  check_uncheck_item: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        checkItemId: Joi.number()
          .integer()
          .required()
          .min(1),
        isChecked: Joi.boolean().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return CheckListService.checkUncheckItem({
        projectId: params.projectId,
        checkItemId: params.checkItemId,
        isChecked: params.isChecked
      });
    }
  },

  add_item_check_to_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        checkItemId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return CheckListService.addItemCheckToProject({
        projectId: params.projectId,
        checkItemId: params.checkItemId
      });
    }
  },

  list_item_check_of_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return CheckListService.listItemCheckOfProject({
        projectId: params.projectId
      });
    }
  },

  remove_item_check: {
    policies: [
      requireToken(),
      validation({
        itemCheckId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let checkItem = await CheckList.findByPk(params.itemCheckId);
        return checkItem && checkItem.projectId;
      })
    },
    action: async (principal, params) => {
      return CheckListService.removeItemCheck({
        itemCheckId: params.itemCheckId
      });
    }
  },

  remove_item_check_of_project: {
    policies: [
      requireToken(),
      validation({
        itemId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let checkItem = await CheckList.findByPk(params.itemId);
        return checkItem && checkItem.projectId;
      })
    },
    action: async (principal, params) => {
      return CheckListService.removeItemCheckOfProject({
        itemId: params.itemId
      });
    }
  }
};
