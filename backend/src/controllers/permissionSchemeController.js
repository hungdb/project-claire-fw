import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { PermissionSchemeService } from '../services/PermissionSchemeService';
import { errors, jsonError } from '../utils/system';
import { validPermissionCondition } from '../utils/common';

module.exports = {
  // PermissionScheme
  create_permission_scheme: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.createPermissionScheme({
        name: params.name
      });
    }
  },

  update_permission_scheme: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.updatePermissionScheme({
        permissionSchemeId: params.permissionSchemeId,
        name: params.name
      });
    }
  },

  remove_permission_scheme: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.deletePermissionScheme({
        permissionSchemeId: params.permissionSchemeId
      });
    }
  },

  list_permission_scheme: {
    policies: [requireToken()],
    action: async principal => {
      return PermissionSchemeService.listPermissionScheme();
    }
  },

  get_permission_scheme: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.getPermissionScheme({
        permissionSchemeId: params.permissionSchemeId
      });
    }
  },

  // PermissionSchemeDetail
  create_permission_scheme_detail: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1),
        permission: Joi.string()
          .trim()
          .required(),
        permissionConditions: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      let scope = validPermissionCondition(params.permissionConditions);
      if (!scope) return jsonError(errors.MALFORMED_REQUEST_ERROR);
      return PermissionSchemeService.createPermissionSchemeDetail({
        permissionSchemeId: params.permissionSchemeId,
        projectRoleId: params.projectRoleId,
        permission: params.permission,
        permissionConditions: params.permissionConditions
      });
    }
  },

  update_permission_scheme_detail: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeDetailId: Joi.number()
          .integer()
          .required()
          .min(1),
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1),
        permission: Joi.string()
          .trim()
          .required(),
        permissionConditions: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      if (!params.permissionConditions) {
        params.permissionConditions = JSON.stringify([]);
      }
      let scope = validPermissionCondition(params.permissionCondition);
      if (!scope) return jsonError(errors.MALFORMED_REQUEST_ERROR);
      return PermissionSchemeService.updatePermissionSchemeDetail({
        permissionSchemeDetailId: params.permissionSchemeDetailId,
        permissionSchemeId: params.permissionSchemeId,
        projectRoleId: params.projectRoleId,
        permission: params.permission,
        permissionConditions: params.permissionConditions
      });
    }
  },

  remove_permission_scheme_detail: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeDetailId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.deletePermissionSchemeDetail({
        permissionSchemeDetailId: params.permissionSchemeDetailId
      });
    }
  },

  list_all_permissions: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return PermissionSchemeService.listAllPermissions();
    }
  },

  list_permission_by_permissionScheme_and_projectRole: {
    policies: [
      requireToken(),
      validation({
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectRoleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return PermissionSchemeService.listPermissionByPermissionSchemeAndProjectRole({
        permissionSchemeId: params.permissionSchemeId,
        projectRoleId: params.projectRoleId
      });
    }
  }
};
