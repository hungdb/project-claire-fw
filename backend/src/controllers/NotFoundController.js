const NotFoundController = require('express').Router();
import { errors, jsonError } from '../utils/system';

NotFoundController.use((req, res) => {
  return res.json(jsonError(errors.NOT_FOUND, { action: req.body && req.body.action }));
});

export { NotFoundController };
