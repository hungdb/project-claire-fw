import Joi from 'joi';
import { requireToken, isSuperAdmin } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { jsonError, jsonSuccess, errors } from '../utils/system';
// import { subset } from '../utils/rpc_conditions';
import { TYPE_LIST_USER } from '../utils/type';
import { UserService } from '../services/UserService';
import { NotificationService } from '../services/NotificationService';
import { CronService } from '../services/CronService';
import { FirebaseService } from '../services/FirebaseService';
import { UserRole } from '../models/schema/UserRole';
import { Role } from '../models/schema/Role';

module.exports = {
  //Role
  get_role: {
    policies: [
      requireToken(),
      validation({
        roleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.getRole({
        roleId: params.roleId
      });
    }
  },

  create_role: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return UserService.addRole({
        name: params.name,
        isSuperUser: false,
        isEditable: true,
        isSystemRole: false
      });
    }
  },

  update_role: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        roleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.updateRole({
        roleId: params.roleId,
        name: params.name
      });
    }
  },

  remove_role: {
    policies: [
      requireToken(),
      validation({
        roleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.deleteRole({
        roleId: params.roleId
      });
    },
    log: true
  },

  list_roles: {
    policies: [requireToken()],
    action: async principal => {
      return UserService.listRoles();
    }
  },

  //Role Permission
  create_role_permission: {
    policies: [
      requireToken(),
      validation({
        roleId: Joi.number()
          .integer()
          .required()
          .min(1),
        permission: Joi.string()
          .trim()
          .required(),
        permissionConditions: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return UserService.addRolePermission({
        roleId: params.roleId,
        permission: params.permission,
        permissionConditions: params.permissionConditions
      });
    }
  },

  remove_role_permission: {
    policies: [
      requireToken(),
      validation({
        rolePermissionId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.deleteRolePermission({
        rolePermissionId: params.rolePermissionId
      });
    },
    log: true
  },

  list_all_role_permission: {
    policies: [requireToken()],
    action: async principal => {
      return UserService.listAllRolePermission();
    }
  },

  update_role_permission: {
    policies: [
      requireToken(),
      validation({
        rolePermissionId: Joi.number()
          .integer()
          .required()
          .min(1),
        roleId: Joi.number()
          .integer()
          .required()
          .min(1),
        permission: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return UserService.updateRolePermission({
        rolePermissionId: params.rolePermissionId,
        roleId: params.roleId,
        permission: params.permission
      });
    }
  },

  list_permission_of_role: {
    policies: [
      requireToken(),
      validation({
        roleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.listPermissionsOfRole({
        roleId: params.roleId
      });
    }
  },

  // User
  list_users: {
    policies: [
      requireToken(),
      validation({
        username: Joi.string()
          .trim()
          .allow(['', null]),
        sort: Joi.string()
          .trim()
          .allow(['', null]),
        isActive: Joi.boolean().allow(['', null])
      })
    ],
    action: async (principal, params) => {
      let sort = 'asc';
      if (params.sort) sort = 'desc';

      return UserService.getUsers({
        username: params.username,
        sort,
        isActive: params.isActive
      });
    }
  },

  list_current_users: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null]),
        isActive: Joi.boolean().allow(['', null]),
        type: Joi.number()
          .valid(Object.values(TYPE_LIST_USER))
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      const valid = Object.values(TYPE_LIST_USER);
      return UserService.getCurrentUsers({
        limit: params.limit,
        offset: params.offset || 0,
        isActive: params.isActive,
        type: params.type && valid.includes(params.type) ? params.type : TYPE_LIST_USER.DEFAULT
      });
    }
  },

  add_user: {
    policies: [
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        roleId: Joi.number()
          .integer()
          .required()
          .min(1),
        username: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return UserService.addUser({
        userId: params.userId,
        username: params.username,
        roleId: params.roleId
      });
    }
  },

  update_user_role: {
    policies: [
      requireToken(),
      validation({
        userRoleId: Joi.number()
          .integer()
          .required()
          .min(1),
        newRoleId: Joi.number()
          .integer()
          .required()
          .min(1),
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      //-- only super_admin user can promote super_admin role
      let currentUserRole = await UserRole.findOne({
        where: { userId: principal.owner }
      });
      let currentUserRoleInfo = await Role.findByPk(currentUserRole.roleId);

      let promotingRole = await Role.findByPk(params.newRoleId);
      if (promotingRole.isSuperUser && !currentUserRoleInfo.isSuperUser) {
        return jsonError(errors.SUPER_USER_REQUIRED);
      }

      return UserService.updateUserRole({
        userRoleId: params.userRoleId,
        userId: params.userId,
        newRoleId: params.newRoleId
      });
    }
  },

  read_my_info: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return UserService.getUserInfo({
        userId: principal.owner
      });
    }
  },

  read_user_info: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
        // info: Joi.array()
      })
    ],
    // permissionConditions: {
    //   subset: subset(['info'])
    // },
    action: async (principal, params) => {
      return UserService.getUserInfo({
        userId: params.userId
        // info: params.info
      });
    }
  },

  get_my_role_and_permission: {
    policies: [requireToken()],
    action: (principal, params) => {
      return UserService.getMyRoleAndPermission({
        userId: principal.owner
      }).then(roleResult => {
        if (!roleResult.success) return roleResult;

        return jsonSuccess({
          roles: roleResult.result,
          permissions: CronService.DATA.userPermissions[principal.owner]
        });
      });
    }
  },

  get_my_notifications: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return NotificationService.getNotificationsByUser({
        userId: principal.owner,
        limit: params.limit || 20,
        offset: params.offset || 0
      });
    }
  },

  read_notification: {
    policies: [
      requireToken(),
      validation({
        notificationId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return NotificationService.readNotification({
        userId: principal.owner,
        notificationId: params.notificationId
      });
    }
  },

  store_token_firebase: {
    policies: [
      requireToken(),
      validation({
        tokenFirebase: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      const data = await UserService.storeTokenFirebase({
        userId: principal.owner,
        tokenFirebase: params.tokenFirebase
      });
      if (data.success) {
        let listAllTasksAndProjects = await UserService.listMyTasksAndProjects({
          userId: principal.owner
        });
        listAllTasksAndProjects = listAllTasksAndProjects.result;
        for (let i = 0; i < listAllTasksAndProjects.length; i++) {
          if (listAllTasksAndProjects[i].type === 'project')
            FirebaseService.subscribeToTopic(params.tokenFirebase, `project_${listAllTasksAndProjects[i].value}`);
          else if (listAllTasksAndProjects[i].type === 'task')
            FirebaseService.subscribeToTopic(params.tokenFirebase, `task_${listAllTasksAndProjects[i].value}`);
        }
      }
      return data;
    }
  },

  // 'send_to_one_user':{
  //   policies: [
  //     requireToken(),
  //   ],
  //   action: async (principal, params) => {
  //     const token = await getTokenFirebase(principal.owner);
  //     await FirebaseService.sendToDevice({
  //       data: {
  //         title: 'Firebase',
  //         body: 'Firebase is awesome',
  //         click_action: 'http://localhost:3002/',
  //         icon: 'https://s3-ap-southeast-1.amazonaws.com/dev-identity-bap-jp/profile-photos/31'
  //       },
  //       registrationToken: token
  //     });
  //     return jsonSuccess()
  //   }
  // },
  //
  // 'send_to_specific_topic': {
  //   policies: [
  //     requireToken(),
  //   ],
  //   action: async (principal, params) => {
  //     await FirebaseService.sendToTopic({
  //       data: {
  //         title: 'Firebase',
  //         body: 'Firebase is awesome',
  //         click_action: 'http://localhost:3002/',
  //         icon: 'https://s3-ap-southeast-1.amazonaws.com/dev-identity-bap-jp/profile-photos/31'
  //       },
  //       topic: 'project_2'
  //     });
  //     return jsonSuccess()
  //   }
  // },

  read_all_notifications: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return NotificationService.readAllNotifications({
        userId: principal.owner
      });
    }
  },

  // UserNote
  create_user_note: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return UserService.createUserNote({
        userId: params.userId,
        content: params.content
      });
    }
  },

  update_user_note: {
    policies: [
      requireToken(),
      validation({
        userNoteId: Joi.number()
          .integer()
          .required()
          .min(1),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return UserService.updateUserNote({
        userNoteId: params.userNoteId,
        content: params.content
      });
    }
  },

  remove_user_note: {
    policies: [
      requireToken(),
      validation({
        userNoteId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return UserService.removeUserNote({
        userNoteId: params.userNoteId
      });
    }
  },

  list_notes_of_specific_user: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        evaluationDate: Joi.date().allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return UserService.listNoteOfSpecificUser({
        userId: params.userId,
        evaluationDate: params.evaluationDate
      });
    }
  },

  update_user_mentor: {
    policies: [
      isSuperAdmin(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        mentor: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return UserService.addUserMentor({
        userId: params.userId,
        mentor: params.mentor
      });
    }
  }
};
