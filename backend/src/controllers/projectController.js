import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { ProjectService } from '../services/ProjectService';
import { S3Service } from '../services/S3Service';
import { CronService } from '../services/CronService';
import { Attachment } from '../models/schema/Attachment';
import { projectDepend } from '../utils/rpc_conditions';
import { Document } from '../models/schema/Document';
import { Environment } from '../models/schema/Environment';
import { ACTIVE_STATUS } from '../models/schema/Project';
import { config } from '../core/config';

module.exports = {
  // Project
  create_project: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        groupId: Joi.number()
          .integer()
          .required()
          .min(1),
        description: Joi.string()
          .trim()
          .allow('', null)
          .default(''),
        permissionSchemeId: Joi.number()
          .integer()
          .required()
          .min(1),
        workflowSchemeId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.createProject({
        name: params.name.trim(),
        description: params.description,
        groupId: params.groupId,
        permissionSchemeId: params.permissionSchemeId,
        workflowSchemeId: params.workflowSchemeId
      });
    }
  },

  update_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .allow(['', null]),
        isActive: Joi.number()
          .integer()
          .valid(Object.values(ACTIVE_STATUS))
          .allow(['', null]),
        groupId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectRate: Joi.number()
          .min(0)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return ProjectService.updateProject({
        projectId: params.projectId,
        name: params.name.trim(),
        isActive: params.isActive,
        groupId: params.groupId,
        projectRate: params.projectRate
      });
    }
  },

  remove_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.deleteProject({
        projectId: params.projectId
      });
    },
    log: true
  },

  get_detail_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getDetailProject({
        projectId: params.projectId
      });
    }
  },

  list_projects: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return ProjectService.listProjects({});
    }
  },

  // 'list_project_by_userId': {
  //   policies: [
  //     requireToken(),
  //     validation({
  //       userId: Joi.number().integer().required().min(1)
  //     })
  //   ],
  //   action: async (principal, params) => {
  //     return ProjectService.listProjectsByUserId({
  //       userId: params.userId
  //     })
  //   }
  // },

  list_project_environments: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.listProjectEnvironments({
        projectId: params.projectId
      });
    }
  },

  get_project_alert_subscription: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getUserAlertSubscriptions({
        userId: principal.owner,
        projectId: params.projectId
      });
    }
  },

  update_project_alert_subscription: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        environmentIds: Joi.array(),
        methodIds: Joi.array()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.updateUserAlertSubscriptions({
        userId: principal.owner,
        projectId: params.projectId,
        environmentIds: params.environmentIds,
        methodIds: params.methodIds
      });
    }
  },

  update_call_method_project_alert_subscription: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        environmentIds: Joi.array(),
        methodIds: Joi.array()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.updateUserAlertSubscriptions({
        userId: principal.owner,
        projectId: params.projectId,
        environmentIds: params.environmentIds,
        methodIds: params.methodIds
      });
    }
  },

  update_project_alert_subscription_by_user: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        environmentIds: Joi.array(),
        methodIds: Joi.array()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.updateUserAlertSubscriptions({
        userId: params.userId,
        projectId: params.projectId,
        environmentIds: params.environmentIds,
        methodIds: params.methodIds
      });
    }
  },

  list_my_projects: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return ProjectService.listProjects({
        userId: principal.owner
      });
    }
  },

  get_health_check_data: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let env = await Environment.findByPk(params.envId);
        return env && env.projectId;
      })
    },
    action: async (principal, params) => {
      return CronService.getHealthCheckData({
        envId: params.envId
      });
    }
  },

  get_users_of_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getUsersOfProject({
        projectId: params.projectId
      });
    }
  },

  get_users_of_all_project: {
    policies: [
      requireToken(),
      validation({
        isActive: Joi.number()
          .integer()
          .valid(Object.values(ACTIVE_STATUS))
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return ProjectService.getUsersOfAllProject({
        isActive: params.isActive
      });
    }
  },

  // Document wiki
  create_document_wiki: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        subject: Joi.string()
          .trim()
          .required(),
        content: Joi.string()
          .trim()
          .required(),
        parentId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.createDocumentWiki({
        projectId: params.projectId,
        userId: principal.owner,
        subject: params.subject,
        content: params.content,
        parentId: params.parentId
      });
    }
  },

  update_document_wiki: {
    policies: [
      requireToken(),
      validation({
        documentId: Joi.number()
          .integer()
          .required()
          .min(1),
        subject: Joi.string()
          .trim()
          .allow(['', null]),
        content: Joi.string()
          .trim()
          .allow(['', null]),
        parentId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let doc = await Document.findByPk(params.documentId);
        return doc && doc.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.updateDocumentWiki({
        documentId: params.documentId,
        subject: params.subject,
        content: params.content,
        userId: principal.owner,
        parentId: params.parentId
      });
    }
  },

  remove_document_wiki: {
    policies: [
      requireToken(),
      validation({
        documentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let doc = await Document.findByPk(params.documentId);
        return doc && doc.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.removeDocumentWiki({
        documentId: params.documentId,
        userId: principal.owner
      });
    }
  },

  get_detail_document_wiki: {
    policies: [
      requireToken(),
      validation({
        documentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const doc = await Document.findByPk(params.documentId);
        return doc && doc.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getDetailDocumentWiki({
        documentId: params.documentId
      });
    }
  },

  list_documents_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        subject: Joi.string()
          .trim()
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.listDocumentsByProject({
        projectId: params.projectId,
        subject: params.subject,
        limit: params.limit,
        offset: params.offset
      });
    }
  },

  // Attachment wiki
  add_attachment_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return parseInt(params.projectId);
      })
    },
    action: async (principal, params) => {
      return ProjectService.addAttachment({
        projectId: params.projectId,
        files: params.files,
        userId: principal.owner
      });
    }
  },

  remove_attachment_by_project: {
    policies: [
      requireToken(),
      validation({
        attachmentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let attachment = await Attachment.findByPk(params.attachmentId);
        return attachment && attachment.attachmentTableId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.removeAttachment({
        attachmentId: params.attachmentId
      });
    }
  },

  list_attachment_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.listAttachmentByProject({
        projectId: params.projectId,
        name: params.name,
        limit: params.limit,
        offset: params.offset
      });
    }
  },

  get_signed_url: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        filePath: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return await S3Service.getSignedUrl({
        bucket: getEnv('S3_BUCKET'),
        filePath: params.filePath,
        projectId: params.projectId
      });
    }
  },

  get_data_file: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        filePath: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return await S3Service.getDataFile({
        bucket: getEnv('S3_BUCKET'),
        filePath: params.filePath
      });
    }
  },

  workflow_migrate: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1),
        workflowStateIds: Joi.array().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.workflowMigrate({
        projectId: params.projectId,
        workflowId: params.workflowId,
        workflowStateIds: params.workflowStateIds
      });
    }
  },

  get_users_alert_subscription_of_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getUsersAlertSubscriptionOfProject({
        projectId: params.projectId
      });
    }
  },
  // list alert subscription
  get_list_alert_subscription: {
    policies: [
      requireToken(),
      validation({
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        userId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getListAlertSubscriptions({
        page: params.page,
        limit: params.limit,
        projectId: params.projectId,
        userId: params.userId
      });
    }
  }
};
