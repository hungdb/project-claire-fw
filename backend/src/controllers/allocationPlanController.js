import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { AllocationPlanService } from '../services/AllocationPlanService';

module.exports = {
  create_allocation_plan: {
    policies: [
      requireToken(),
      validation({
        data: Joi.array()
          .required()
          .items(
            Joi.object().keys({
              userId: Joi.number()
                .integer()
                .required()
                .min(1),
              effortPercent: Joi.number()
                .required()
                .min(0)
                .max(100),
              startDate: Joi.date().required(),
              endDate: Joi.date()
                .min(Joi.ref('startDate'))
                .required()
            })
          )
      })
    ],
    action: async (principal, params) => {
      return AllocationPlanService.createAllocationPlan({
        actor: principal.owner,
        data: params.data
      });
    }
  },

  update_allocation_plan: {
    policies: [
      requireToken(),
      validation({
        allocationPlanId: Joi.number()
          .integer()
          .required()
          .min(1),
        effortPercent: Joi.number()
          .required()
          .min(0)
          .max(100),
        startDate: Joi.date().required(),
        endDate: Joi.date()
          .min(Joi.ref('startDate'))
          .required()
      })
    ],
    action: async (principal, params) => {
      return AllocationPlanService.updateAllocationPlan({
        allocationPlanId: params.allocationPlanId,
        actor: principal.owner,
        effortPercent: params.effortPercent,
        startDate: params.startDate,
        endDate: params.endDate
      });
    }
  },

  remove_allocation_plan: {
    policies: [
      requireToken(),
      validation({
        allocationPlanId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return AllocationPlanService.removeAllocationPlan({
        allocationPlanId: params.allocationPlanId
      });
    }
  },

  list_allocation_plans: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return AllocationPlanService.listAllocationPlans();
    }
  },

  list_allocations_plan_of_users: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return AllocationPlanService.listAllocationsPlanOfUsers();
    }
  }
};
