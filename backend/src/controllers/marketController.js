import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { MarketService } from '../services/MarketService';

module.exports = {
  create_market: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return MarketService.createMarket({
        name: params.name.trim()
      });
    }
  },

  update_market: {
    policies: [
      requireToken(),
      validation({
        id: Joi.number().required(),
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return MarketService.updateMarket({
        id: params.id,
        name: params.name.trim()
      });
    }
  },

  remove_market: {
    policies: [
      requireToken(),
      validation({
        id: Joi.number().required()
      })
    ],
    action: async (principal, params) => {
      return MarketService.removeMarket({
        id: params.id
      });
    }
  },

  get_list_market: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return MarketService.getListMarket();
    }
  }
};
