import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { SkillService } from '../services/SkillService';
import { projectDepend } from '../utils/rpc_conditions';
import { Sprint } from '../models/schema/Sprint';
import { SKILL_TYPES } from '../models/schema/UserSkill';

module.exports = {
  // Skill
  create_skill: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null]),
        linkIcon: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SkillService.createSkill({
        name: params.name,
        description: params.description,
        linkIcon: params.linkIcon
      });
    }
  },

  update_skill: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null]),
        linkIcon: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SkillService.updateSkill({
        skillId: params.skillId,
        name: params.name,
        description: params.description,
        linkIcon: params.linkIcon
      });
    }
  },

  remove_skill: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.removeSkill({
        skillId: params.skillId
      });
    }
  },

  get_skill: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.getSkill({
        skillId: params.skillId
      });
    }
  },

  list_skills: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .allow(['', null]),
        isStandard: Joi.boolean().allow([null])
      })
    ],
    action: async (principal, params) => {
      return SkillService.listSkills({
        limit: params.limit,
        page: params.page,
        isStandard: params.isStandard
      });
    }
  },

  // UserSkill
  create_user_skill: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        skillId: Joi.number()
          .integer()
          .required()
          .min(1),
        evaluationPoint: Joi.number()
          .required()
          .min(0)
          .max(100),
        isStandard: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return SkillService.createUserSkill({
        actor: principal.owner,
        userId: params.userId,
        skillId: params.skillId,
        evaluationPoint: params.evaluationPoint,
        isStandard: params.isStandard === true
      });
    }
  },

  update_user_skill: {
    policies: [
      requireToken(),
      validation({
        userSkillId: Joi.number()
          .integer()
          .required()
          .min(1),
        evaluationPoint: Joi.number()
          .required()
          .min(0)
          .max(100),
        isStandard: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return SkillService.updateUserSkill({
        userSkillId: params.userSkillId,
        evaluationPoint: params.evaluationPoint,
        isStandard: params.isStandard
      });
    }
  },

  remove_user_skill: {
    policies: [
      requireToken(),
      validation({
        userSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.removeUserSkill({
        userSkillId: params.userSkillId
      });
    }
  },

  get_user_skill: {
    policies: [
      requireToken(),
      validation({
        userSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.getUserSkill({
        userSkillId: params.userSkillId
      });
    }
  },

  list_skills_by_user: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        type: Joi.number()
          .integer()
          .valid([Object.values(SKILL_TYPES)])
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SkillService.listUserSkills({
        userId: params.userId,
        type: params.type
      });
    }
  },

  list_users_by_skill: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.listUsersBySkill({
        skillId: params.skillId
      });
    }
  },

  list_skills_by_user_with_latest_evaluation: {
    params: [{ name: 'userId', type: 'number' }],
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SkillService.listSkillsByUserWithLastestEvaluation({
        userId: params.userId
      });
    }
  },

  list_allocations_of_users: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return SkillService.listAllocationsOfUsers();
    }
  },

  get_skills_of_all_users: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .allow(['', null]),
        skillId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: (principal, params) => {
      return SkillService.getSkillOfAllUsers({
        name: params.name,
        skillId: params.skillId,
        type: SKILL_TYPES.normal
      });
    }
  },

  add_user_skill_in_sprint: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        skillId: Joi.number()
          .integer()
          .required()
          .min(1),
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        evaluationPoint: Joi.number()
          .min(0)
          .max(100)
          .default(0),
        note: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: (principal, params) => {
      return SkillService.addUserSkillInSprint({
        actor: principal.owner,
        userId: params.userId,
        skillId: params.skillId,
        evaluationPoint: params.evaluationPoint ? params.evaluationPoint : 0,
        sprintId: params.sprintId,
        type: SKILL_TYPES.project,
        note: params.note
      });
    }
  },

  add_my_skill_in_sprint: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .required()
          .min(1),
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        evaluationPoint: Joi.number()
          .min(0)
          .max(100)
          .default(0),
        note: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: (principal, params) => {
      return SkillService.addUserSkillInSprint({
        actor: principal.owner,
        userId: principal.owner,
        skillId: params.skillId,
        evaluationPoint: params.evaluationPoint ? params.evaluationPoint : 0,
        sprintId: params.sprintId,
        type: SKILL_TYPES.yourSelf,
        note: params.note
      });
    }
  },

  // Kpi skill
  create_kpi_skill: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        reviewPoint: Joi.number()
          .required()
          .min(0)
          .max(100),
        testPoint: Joi.number()
          .required()
          .min(0)
          .max(100),
        evaluatedAt: Joi.date()
      })
    ],
    action: (principal, params) => {
      return SkillService.createKpiSkill({
        actor: principal.owner,
        userId: params.userId,
        reviewPoint: params.reviewPoint,
        testPoint: params.testPoint,
        evaluatedAt: params.evaluatedAt ? params.evaluatedAt : new Date()
      });
    }
  },

  get_kpi_skill: {
    policies: [
      requireToken(),
      validation({
        kpiSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: (principal, params) => {
      return SkillService.getKpiSkillById({
        kpiSkillId: params.kpiSkillId
      });
    }
  },

  remove_kpi_skill: {
    params: [{ name: 'kpiSkillId', type: 'number' }],
    policies: [
      requireToken(),
      validation({
        kpiSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: (principal, params) => {
      return SkillService.removeKpiSkillById({
        kpiSkillId: params.kpiSkillId
      });
    }
  },

  get_list_kpi_skill: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: (principal, params) => {
      return SkillService.getListKpiSkill({
        userId: params.userId,
        page: params.page,
        limit: params.limit
      });
    }
  },

  list_my_user_skills: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SkillService.listUserSkills({
        userId: principal.owner,
        page: params.page,
        limit: params.limit,
        sprintId: params.sprintId,
        projectId: params.projectId,
        type: SKILL_TYPES.yourSelf
      });
    }
  },

  list_user_skills_by_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      return SkillService.listUserSkills({
        sprintId: params.sprintId,
        type: SKILL_TYPES.project
      });
    }
  },

  remove_my_user_skill: {
    policies: [
      requireToken(),
      validation({
        userSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      const paramUserSkill = {
        actor: principal.owner,
        userSkillId: params.userSkillId,
        type: SKILL_TYPES.yourSelf
      };
      const userSkill = await SkillService.getUserSkill(paramUserSkill);
      if (!userSkill.success) return userSkill;
      return SkillService.removeUserSkill(paramUserSkill);
    }
  },

  remove_user_skill_in_sprint: {
    policies: [
      requireToken(),
      validation({
        userSkillId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const paramUserSkill = {
          userSkillId: params.userSkillId,
          type: SKILL_TYPES.project
        };
        const userSkill = await SkillService.getUserSkill(paramUserSkill);
        return userSkill.success && userSkill.result && userSkill.result.projectId;
      })
    },
    action: async (principal, params) => {
      const paramUserSkill = {
        actor: principal.owner,
        userSkillId: params.userSkillId,
        type: SKILL_TYPES.project
      };
      const userSkill = await SkillService.getUserSkill(paramUserSkill);
      if (!userSkill.success) return userSkill;
      return SkillService.removeUserSkill(paramUserSkill);
    }
  }
};
