import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { SalePriorityService } from '../services/SalePriorityService';

module.exports = {
  create_sale_priority: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        ordinalNumber: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SalePriorityService.createSalePriority({
        name: params.name.trim(),
        ordinalNumber: params.ordinalNumber
      });
    }
  },

  update_sale_priority: {
    policies: [
      requireToken(),
      validation({
        salePriorityId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        ordinalNumber: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SalePriorityService.updateSalePriority({
        salePriorityId: params.salePriorityId,
        name: params.name.trim(),
        ordinalNumber: params.ordinalNumber
      });
    }
  },

  remove_sale_priority: {
    policies: [
      requireToken(),
      validation({
        salePriorityId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SalePriorityService.removeSalePriority({
        salePriorityId: params.salePriorityId
      });
    }
  },

  get_list_sale_priority: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return SalePriorityService.listSalePriority();
    }
  }
};
