import groupController from './groupController';
import workflowController from './workflowController';
import permissionSchemeController from './permissionSchemeController';
import projectController from './projectController';
import projectRoleController from './projectRoleController';
import sprintController from './sprintController';
import taskController from './taskController';
import allocationController from './allocationController';
import userController from './userController';
import notificationController from './notificationController';
import checkListController from './checkListController';
import skillController from './skillController';
import authController from './authController';
import behaviorController from './behaviorController';
import allocationLabelController from './allocationLabelController';
import { errors, jsonError, logger } from '../utils/system';
import { editableFields, regexStringValue, subset } from '../utils/rpc_conditions';
import issueController from './issueController';
import kpiController from './kpiController';
import kpiCoefficientController from './kpiCoefficientController';
import salePriorityController from './salePriorityController';
import saleStatusController from './saleStatusController';
import saleCategoryController from './saleCategoryController';
import saleController from './saleController';
import allocationPlanController from './allocationPlanController';
import marketController from './marketController';
import customerController from './customerController';
import staticController from './staticController';

const RPCController = require('express').Router();

const middlewareRun = (middleware, req, res, cb) => {
  if (!middleware || !middleware.length) return cb();

  let i = 0;
  let process = layer => {
    return layer(req, res, () => {
      setImmediate(() => {
        i++;
        if (res.headersSent) return;
        if (i === middleware.length) return cb();
        return process(middleware[i]);
      });
    });
  };
  process(middleware[i]);
};

class RegexStringValue {
  fields = [];

  constructor(fields) {
    this.fields = fields;
  }

  static permit(conditions) {
    return {
      name: 'regexStringValue',
      value:
        !!conditions &&
        map(c => {
          return { name: c.name, value: c.value };
        })
    };
  }

  static isSubset(sub, parent) {
    return false;
  }

  evaluate(params, conditions) {
    return !this.fields.some(f => {
      return !(!conditions || (!!params[f] && params[f].length && params[f].every(field => true)));
    });
  }
}

class EditableFields {
  fields = [];

  constructor(fields) {
    this.fields = fields;
  }

  static permit(conditions) {
    return { name: 'editableFields', value: conditions || null };
  }

  static isSubset(sub, parent) {
    return false;
  }

  evaluate(params, conditions) {
    return !this.fields.some(f => {
      return false;
    });
  }
}

class Subset {
  fields = [];

  constructor(fields) {
    this.fields = fields;
  }

  static permit(conditions) {
    return { name: 'subset', value: conditions || null };
  }

  static isSubset(sub, parent) {
    // console.log('checking isSubset', sub, parent);
    return !parent || (sub && sub.length && sub.every(s => parent.indexOf(s) >= 0));
  }

  evaluate(params, conditions) {
    return !this.fields.some(f => {
      return !(!conditions || (!!params[f] && params[f].length && params[f].every(field => indexOf(field) >= 0)));
    });
  }
}

const rpcConditions = {
  subset: Subset,
  regexStringValue: RegexStringValue,
  editableFields: EditableFields
};

const rpcActions = {
  ...authController,
  ...groupController,
  ...workflowController,
  ...permissionSchemeController,
  ...projectController,
  ...projectRoleController,
  ...sprintController,
  ...taskController,
  ...allocationController,
  ...userController,
  ...notificationController,
  ...checkListController,
  ...skillController,
  ...issueController,
  ...behaviorController,
  ...kpiController,
  ...allocationLabelController,
  ...kpiCoefficientController,
  ...salePriorityController,
  ...saleStatusController,
  ...saleCategoryController,
  ...saleController,
  ...allocationPlanController,
  ...marketController,
  ...customerController,
  ...staticController
};

RPCController.post('/api/rpc', (req, res) => {
  // let principal = req.principal;

  let commandName = req.body.action;

  // let params = req.body;

  // let headers = req.headers;

  if (!commandName || !rpcActions[commandName]) {
    return res.json(jsonError(errors.MALFORMED_REQUEST_ERROR, { action: commandName }));
  }

  //-- param validation check
  middlewareRun(rpcActions[commandName].validation, req, res, () => {
    middlewareRun(rpcActions[commandName].policies, req, res, () => {
      (async () => {
        //-- permission check
        let permissions = req.principal['permissions'].filter(p => p && p.permission === commandName);

        if (!permissions.length) return jsonError(errors.ACCESS_DENIED);

        let conditionCheck = false;

        for (let iPer = 0; iPer < permissions.length; iPer++) {
          let p = permissions[iPer];
          let conditionOK = !!Array.isArray(p.permissionCondition);

          for (let iCon = 0; iCon < p.permissionCondition.length; iCon++) {
            let setting = p.permissionCondition[iCon];
            conditionOK =
              conditionOK &&
              rpcActions[commandName].permissionConditions &&
              rpcActions[commandName].permissionConditions[setting.name] &&
              (await rpcActions[commandName].permissionConditions[setting.name].evaluate(req.body, setting.value));
          }

          if (conditionOK) {
            conditionCheck = true;
            break;
          }
        }

        if (!conditionCheck) return jsonError(errors.ACCESS_CONDITION_FAILED);

        if (!rpcActions[commandName].action) return jsonError(errors.NOT_IMPLEMENTED_ERROR);

        return await rpcActions[commandName].action(req.principal, req.body);
      })()
        .then(result => {
          // logger.verbose(result);
          if (!result.success) {
            result.error = {
              ...result.error,
              extra: {
                ...result.error.extra,
                action: commandName
              }
            };
          }
          return res.json(result);
        })
        .catch(err => {
          logger.error(err);
          return res.json(jsonError(errors.SYSTEM_ERROR, { action: commandName }));
        });
    });
  });
});

// RPCController.use((req, res) => {
//   return res.json(jsonError(errors.NOT_FOUND, { action: req.body && req.body.action }));
// });

export { RPCController, rpcActions };
