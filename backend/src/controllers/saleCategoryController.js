import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { SaleCategoryService } from '../services/SaleCategoryService';

module.exports = {
  create_sale_category: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return SaleCategoryService.createSaleCategory({
        name: params.name
      });
    }
  },

  get_list_sale_category: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return SaleCategoryService.getListSaleCategory();
    }
  },

  update_sale_category: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return SaleCategoryService.updateSaleCategory({
        saleId: params.saleId,
        name: params.name
      });
    }
  },

  remove_sale_category: {
    policies: [
      requireToken(),
      validation({
        saleId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SaleCategoryService.removeSaleCategory({
        saleId: params.saleId
      });
    }
  }
};
