import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { ProjectService } from '../services/ProjectService';
import { NotificationService } from '../services/NotificationService';
import { projectDepend } from '../utils/rpc_conditions';
import { Environment } from '../models/schema/Environment';

module.exports = {
  //Environment
  create_environment: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.createEnvironment({
        name: params.name,
        // serverHC: params.serverHealthCheck,
        projectId: params.projectId
        // hcThresholdDown: params.hcThresholdDown,
        // hcThresholdUp: params.hcThresholdUp
      });
    }
  },

  update_environment: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .allow(['', null]),
        serverHealthCheck: Joi.string()
          .trim()
          .allow(['', null]),
        hcThresholdDown: Joi.number()
          .integer()
          .allow(['', null]),
        hcThresholdUp: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let env = await Environment.findByPk(params.envId);
        return env && env.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.updateEnvironment({
        envId: params.envId,
        name: params.name,
        serverHC: params.serverHealthCheck,
        // projectId: params.projectId,
        hcThresholdDown: params.hcThresholdDown,
        hcThresholdUp: params.hcThresholdUp
      });
    }
  },

  remove_environment: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let env = await Environment.findByPk(params.envId);
        return env && env.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.deleteEnvironment({
        envId: params.envId
      });
    },
    log: true
  },

  get_environment: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1),
        secretKey: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let env = await Environment.findByPk(params.envId);
        return env && env.projectId;
      })
    },
    action: async (principal, params) => {
      return ProjectService.getEnvironment({
        envId: params.envId,
        secretKey: params.secretKey
      });
    }
  },

  list_environments: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return ProjectService.listEnvironments({
        projectId: params.projectId
      });
    }
  },

  //Configuration
  list_configurations: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.listConfigurations({
        envId: params.envId
      });
    }
  },

  create_configuration: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1),
        key: Joi.string()
          .trim()
          .required(),
        value: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return ProjectService.createConfiguration({
        envId: params.envId,
        key: params.key,
        value: params.value
      });
    }
  },

  update_configuration: {
    policies: [
      requireToken(),
      validation({
        envId: Joi.number()
          .integer()
          .required()
          .min(1),
        key: Joi.string()
          .trim()
          .required(),
        value: Joi.string()
          .trim()
          .required(),
        configId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.updateConfiguration({
        configId: params.configId,
        key: params.key,
        value: params.value,
        envId: params.envId
      });
    }
  },

  remove_configuration: {
    policies: [
      requireToken(),
      validation({
        configId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return ProjectService.deleteConfiguration({
        configId: params.configId
      });
    },
    log: true
  },

  toggle_notification_enable: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        isEnabled: Joi.boolean().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return NotificationService.toggleNotificationOfProject({
        projectId: params.projectId,
        isEnabled: params.isEnabled
      });
    }
  },

  get_logs_of_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return NotificationService.getLogsOfProject({
        projectId: params.projectId
      });
    }
  },

  active_inactive_alert_system: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return NotificationService.activeInactiveAlertSystem();
    }
  },

  get_state_of_alert_system: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return NotificationService.getStateOfAlertSystem();
    }
  },

  toggle_notification_enable_group: {
    policies: [
      requireToken(),
      validation({
        groupId: Joi.number()
          .integer()
          .required()
          .min(1),
        isEnabled: Joi.boolean().required()
      })
    ],
    action: async (principal, params) => {
      return NotificationService.toggleNotificationOfProjectGroup({
        groupId: params.groupId,
        isEnabled: params.isEnabled
      });
    }
  }
};
