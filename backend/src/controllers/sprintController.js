import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { SprintService } from '../services/SprintService';
import { projectDepend } from '../utils/rpc_conditions';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { Allocation } from '../models/schema/Allocation';
import { Task } from '../models/schema/Task';
import { SocketService } from '../services/SocketService';
import { NOTIFICATION_TYPE } from '../models/schema/Notification';
import { errors, jsonError } from '../utils/system';
import { getTokenFirebase, truncateString } from '../utils/common';
import { FirebaseService } from '../services/FirebaseService';
import { CronService } from '../services/CronService';
import { WORK_MANAGEMENT_POINTS, PEOPLE_MANAGEMENT_POINTS, PROCESS_POINTS } from '../models/schema/KPISprint';

module.exports = {
  // Sprint
  create_sprint: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        startDate: Joi.date().required(),
        endDate: Joi.date()
          .min(Joi.ref('startDate'))
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.createSprint({
        name: params.name,
        projectId: params.projectId,
        startDate: params.startDate,
        endDate: params.endDate
      });
    }
  },

  get_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.getSprint({
        sprintId: params.sprintId
      });
    }
  },

  get_sprints_by_projectId: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.getSprintByProjectId({
        projectId: params.projectId
      });
    }
  },

  update_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        status: Joi.string()
          .trim()
          .required()
          .valid(Object.values(STATUS)),
        startDate: Joi.date().required(),
        endDate: Joi.date()
          .min(Joi.ref('startDate'))
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      //-- dont allow active sprint if there is no allocation or task
      if (params.status === STATUS.ACTIVE) {
        let allocations = await Allocation.findAll({
          where: { sprintId: params.sprintId }
        });
        if (!allocations.length) return jsonError(errors.NO_ALLOCATION_IN_SPRINT);
        let tasks = await Task.findAll({
          where: { sprintId: params.sprintId }
        });
        if (!tasks.length) return jsonError(errors.NO_TASK_IN_SPRINT);
      }

      // if (params.status !== STATUS.ACTIVE && params.status !== STATUS.COMPLETED && params.status !== STATUS.UNACTIVE)
      //   return jsonError(errors.STATUS_ERROR);

      let result = await SprintService.updateSprint({
        sprintId: params.sprintId,
        name: params.name,
        status: params.status,
        startDate: params.startDate,
        endDate: params.endDate
      });
      if (result.success) {
        const check = result.check;
        const sprint = result.result;
        if (check && [STATUS.ACTIVE, STATUS.COMPLETED].includes(params.status)) {
          let actor = CronService.DATA.userData[principal.owner];
          let data = {
            subType: params.status === STATUS.ACTIVE ? 'active_sprint' : 'completed_sprint',
            sprint: {
              id: sprint.id,
              name: sprint.name,
              project: {
                id: sprint.Project.id,
                name: sprint.Project.name
              },
              status: params.status
            },
            content: `${sprint.name} is ${params.status}!`
          };

          SocketService.notificationToRoom({
            userId: principal.owner,
            notificationableId: sprint.Project.id,
            notificationableType: NOTIFICATION_TYPE.PROJECT,
            type: 'time_active_sprint_change',
            data: data
          });
          SocketService.reloadBoard({
            userId: principal.owner,
            notificationableId: sprint.Project.id,
            notificationableType: NOTIFICATION_TYPE.PROJECT
          });
          SocketService.storeNotificationToUserOfRoomProject({
            projectId: sprint.Project.id,
            data: data,
            userId: principal.owner,
            type: 'time_active_sprint_change',
            content: `${sprint.name} is ${params.status}!`
          });

          // web push firebase
          let token = await getTokenFirebase(principal.owner);
          // FirebaseService.subscribeToTopic(token, `project_${sprint.projectId}`);
          FirebaseService.sendMessageToTopic({
            data: {
              title: truncateString(params.status === STATUS.ACTIVE ? 'Active sprint' : 'Completed sprint', 20),
              body: truncateString(
                `${actor && actor.username} has changed status of sprint is ${
                  params.status === STATUS.ACTIVE ? 'active' : 'completed'
                } `,
                30
              ),
              click_action: `${getEnv('DOMAIN')}sprints/${sprint.projectId}/board/1`,
              icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
            },
            topic: `project_${sprint.projectId}`,
            token
          });
        }
      }
      return result;
    }
  },

  remove_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.deleteSprint({
        sprintId: params.sprintId
      });
    }
  },

  get_sprints_diff_completed_by_projectId: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.getSprintDiffCompleted({
        projectId: params.projectId
      });
    }
  },

  sprint_progress_of_member: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await SprintService.getMembersOfSprint({
        sprintId: params.sprintId
      });
      if (!result.success) return result;
      return SprintService.getSprintProgressOfMember({
        sprintId: params.sprintId,
        users: result.result
      });
    }
  },

  get_members_of_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      return SprintService.getMembersOfSprint({
        sprintId: params.sprintId
      });
    }
  },

  // KPI sprint (project)
  create_kpi_sprint: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        workManagementPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(WORK_MANAGEMENT_POINTS)),
        peopleManagementPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(PEOPLE_MANAGEMENT_POINTS)),
        processPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(PROCESS_POINTS))
      })
    ],
    action: async (principal, params) => {
      return SprintService.createKpiSprint({
        actor: principal.owner,
        userId: params.userId,
        sprintId: params.sprintId,
        projectId: params.projectId,
        workManagementPoint: params.workManagementPoint,
        peopleManagementPoint: params.peopleManagementPoint,
        processPoint: params.processPoint
      });
    }
  },

  remove_kpi_sprint: {
    policies: [
      requireToken(),
      validation({
        kpiSprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SprintService.removeKpiSprint({
        actor: principal.owner,
        kpiSprintId: params.kpiSprintId
      });
    }
  },

  update_kpi_sprint: {
    policies: [
      requireToken(),
      validation({
        kpiSprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        workManagementPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(WORK_MANAGEMENT_POINTS)),
        peopleManagementPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(PEOPLE_MANAGEMENT_POINTS)),
        processPoint: Joi.number()
          .required()
          .min(0)
          .valid(Object.values(PROCESS_POINTS))
      })
    ],
    action: async (principal, params) => {
      return SprintService.updateKpiSprint({
        actor: principal.owner,
        kpiSprintId: params.kpiSprintId,
        workManagementPoint: params.workManagementPoint,
        peopleManagementPoint: params.peopleManagementPoint,
        processPoint: params.processPoint
      });
    }
  },

  get_list_kpi_sprint: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        type: Joi.number()
          .integer()
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return SprintService.getListKpiSprint({
        userId: params.userId,
        projectId: params.projectId,
        type: params.type,
        page: params.page,
        limit: params.limit
      });
    }
  }
};
