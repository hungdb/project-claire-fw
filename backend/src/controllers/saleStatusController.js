import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { SaleStatusService } from '../services/SaleStatusService';

module.exports = {
  create_sale_status: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return SaleStatusService.createStatus({
        name: params.name.trim()
      });
    }
  },

  update_sale_status: {
    policies: [
      requireToken(),
      validation({
        statusId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return SaleStatusService.updateStatus({
        statusId: params.statusId,
        name: params.name.trim()
      });
    }
  },

  get_list_sale_status: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return SaleStatusService.getListStatus();
    }
  },

  remove_sale_status: {
    policies: [
      requireToken(),
      validation({
        statusId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return SaleStatusService.removeStatus({
        statusId: params.statusId
      });
    }
  }
};
