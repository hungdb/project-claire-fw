import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { AllocationLabelService } from '../services/AllocationLabelService';

module.exports = {
  //Allocation Label
  create_allocation_label: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return AllocationLabelService.createLabel({
        name: params.name
      });
    }
  },

  get_list_allocation_labels: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return AllocationLabelService.getListAllocationLabels();
    }
  },

  update_allocation_label: {
    policies: [
      requireToken(),
      validation({
        labelId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required()
      })
    ],
    action: async (principal, params) => {
      return AllocationLabelService.updateLabelAllocation({
        labelId: params.labelId,
        name: params.name
      });
    }
  },

  remove_allocation_label: {
    policies: [
      requireToken(),
      validation({
        labelId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return AllocationLabelService.deleteLabelAllocation({
        labelId: params.labelId
      });
    }
  }
};
