import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { TaskService } from '../services/TaskService';
import { TagService } from '../services/TagService';
import { SocketService } from '../services/SocketService';
import { Notification, NOTIFICATION_TYPE } from '../models/schema/Notification';
import { projectDepend } from '../utils/rpc_conditions';
import { Task, TASK_PRIORITIES, TASK_TYPE } from '../models/schema/Task';
import { Comment } from '../models/schema/Comment';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { Attachment } from '../models/schema/Attachment';
import { CronService } from '../services/CronService';
import { Project } from '../models/schema/Project';
import { WorkflowState } from '../models/schema/WorkflowState';
import { User } from '../models/schema/User';
import { getTokenFirebase, truncateString } from '../utils/common';
import { FirebaseService } from '../services/FirebaseService';
import { jsonError, errors } from '../utils/system';
import { config } from '../core/config';

module.exports = {
  //Task
  create_task: {
    policies: [
      requireToken(),
      validation({
        content: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null])
          .default(null),
        estimate: Joi.number().min(0),
        sprintId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        issueId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        tag: Joi.array().default([]),
        // milestone: Joi.string().trim().allow(['', null]),
        priority: Joi.number()
          .integer()
          .valid(Object.values(TASK_PRIORITIES))
          .allow(['', null]),
        dependencies: Joi.array().allow(['', null]),
        assignee: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        dueDate: Joi.date().allow(['', null]),
        type: Joi.number()
          .integer()
          .valid(Object.values(TASK_TYPE))
          .allow(['', null]),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single(),
        userRelate: Joi.array().default([])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await TaskService.createTask({
        content: params.content,
        description: params.description,
        estimate: params.estimate,
        sprintId: params.sprintId,
        projectId: params.projectId,
        tag: params.tag,
        milestone: params.milestone,
        priority: params.priority,
        dependencies: params.dependencies,
        assignee: params.assignee,
        dueDate: params.dueDate,
        files: params.files,
        type: params.type,
        userId: principal.owner,
        issueId: params.issueId ? params.issueId : null,
        userRelate: params.userRelate
      });
      if (result.success) {
        const newTags = params.tag && Array.isArray(params.tag) ? params.tag : [];
        TagService.storeTags({
          newTags,
          projectId: params.projectId
        });
      }
      return result;
    }
  },

  update_task_backlog: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .required()
          .min(1),
        content: Joi.string()
          .trim()
          .required(),
        description: Joi.string()
          .trim()
          .allow(['', null])
          .default(null),
        estimate: Joi.number()
          .min(0)
          .allow(['', null]),
        tag: Joi.array(),
        // milestone: Joi.array().trim(),
        priority: Joi.number()
          .integer()
          .valid(Object.values(TASK_PRIORITIES))
          .allow(['', null]),
        dependencies: Joi.array(),
        assignee: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        dueDate: Joi.date().allow(['', null]),
        type: Joi.number()
          .integer()
          .valid(Object.values(TASK_TYPE))
          .allow(['', null]),
        userRelate: Joi.array().allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await TaskService.updateBacklogTask({
        taskId: params.taskId,
        content: params.content,
        estimate: params.estimate,
        tag: params.tag,
        description: params.description,
        milestone: params.milestone,
        priority: params.priority,
        dependencies: params.dependencies,
        assignee: params.assignee,
        dueDate: params.dueDate,
        type: params.type,
        userId: principal.owner,
        userRelate: params.userRelate && Array.isArray(params.userRelate) ? params.userRelate : null
      });
      if (result.success) {
        const newTags = params.tag && Array.isArray(params.tag) ? params.tag : [];
        TagService.storeTags({
          newTags,
          projectId: params.projectId
        });
      }
      return result;
    }
  },

  remove_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.deleteTask({
        taskId: params.taskId
      });
    }
  },

  list_task_by_sprintId: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.listTaskBySprintId({
        sprintId: params.sprintId
      });
    }
  },

  list_task_backlog_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        tags: Joi.array().allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.listTaskBacklogByProject({
        projectId: params.projectId,
        tags: params.tags,
        limit: params.limit,
        offset: params.offset
      });
    }
  },

  filter_task_backlog_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        key: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.filterTaskBacklogByProject({
        projectId: params.projectId,
        key: params.key
      });
    }
  },

  filter_tasks_of_project: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        assignee: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        sprintId: Joi.number()
          .integer()
          .min(1)
          .allow(['', -1, null]),
        status: Joi.number()
          .integer()
          .min(1)
          .allow(['', -1, null]),
        priority: Joi.number()
          .integer()
          .valid(Object.values(TASK_PRIORITIES))
          .allow(['', null]),
        startDate: Joi.date().allow(['', null]),
        key: Joi.string()
          .trim()
          .allow(['', null]),
        type: Joi.number()
          .integer()
          .valid(Object.values(TASK_TYPE))
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.filterTasksOfProject({
        limit: params.limit || 10,
        page: params.page || 1,
        projectId: params.projectId,
        assignee: params.assignee,
        userId: principal.owner,
        sprintId: params.sprintId,
        status: params.status,
        startDate: params.startDate,
        priority: params.priority,
        key: params.key,
        type: params.type
      });
    }
  },

  add_estimate: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .required()
          .min(1),
        estimate: Joi.number()
          .integer()
          .required()
          .min(0)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.addEstimate({
        taskId: params.taskId,
        estimate: params.estimate
      });
    }
  },

  get_detail_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.getTaskById({
        taskId: params.taskId
      });
    }
  },

  suggest_tag: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return TagService.suggestTags({
        projectId: params.projectId
      });
    }
  },

  get_milestone_suggestions: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.getMilestoneSuggestions({
        projectId: params.projectId
      });
    }
  },

  update_sprint_of_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.any().when('type', {
          is: 1,
          then: Joi.any(),
          otherwise: Joi.number()
            .integer()
            .min(1)
            .required()
        }),
        sprintId: Joi.number()
          .integer()
          .min(1)
          .required(),
        type: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      // type = 1 add all tasks (backlog) to sprint
      if (params.type === 1) {
        return await TaskService.updateSprintMultipleTasks({
          type: 'add',
          sprintId: params.sprintId
        });
      } else {
        if (!params.taskId) return jsonError(errors.MISSING_REQUIRED_VALUE);
        let result = await TaskService.updateSprintTask({
          taskId: params.taskId,
          sprintId: params.sprintId
        });
        if (result.success) {
          const sprint = await Sprint.findByPk(params.sprintId);
          const task = await Task.findOne({
            where: { id: params.taskId },
            include: [{ model: Project }, { model: Sprint }]
          });
          if (sprint.status === STATUS.ACTIVE) {
            let actor = CronService.DATA.userData[principal.owner];
            let data = {
              subType: 'new_task_of_sprint',
              task: {
                id: task.id,
                content: task.content,
                project: {
                  id: task.Project && task.Project.id,
                  name: task.Project && task.Project.name
                },
                sprint: {
                  id: sprint.id,
                  name: sprint.name
                }
              },
              actor: {
                id: principal.owner,
                name: actor && actor.username,
                avatar: actor && actor.profilePhoto
              },
              content: `New task was added to ${sprint.name} by ${actor && actor.username}!`
            };

            SocketService.notificationToRoom({
              userId: principal.owner,
              notificationableId: task.Project && task.Project.id,
              notificationableType: NOTIFICATION_TYPE.PROJECT,
              type: 'new_task',
              data: data
            });
            SocketService.reloadBoard({
              userId: principal.owner,
              notificationableId: task.Project && task.Project.id,
              notificationableType: NOTIFICATION_TYPE.PROJECT
            });
            SocketService.storeNotificationToUserOfRoomProject({
              projectId: task.Project && task.Project.id,
              data: data,
              userId: principal.owner,
              type: 'new_task',
              content: `New task was added to ${sprint.name} by ${actor && actor.username}!`
            });

            // web push firebase
            let token = await getTokenFirebase(principal.owner);
            // FirebaseService.subscribeToTopic(token, `project_${task.projectId}`);
            FirebaseService.sendMessageToTopic({
              data: {
                title: truncateString(`New task to active sprint`, 20),
                body: truncateString(`${actor && actor.username} has added new task to current sprint`, 30),
                click_action: `${getEnv('DOMAIN')}sprints/${task.projectId}/board/2/${task.id}`,
                icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
              },
              topic: `project_${task.projectId}`,
              token
            });
          }
        }
        return result;
      }
    }
  },

  update_task_to_backlog: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.any().when('type', {
          is: 1,
          then: Joi.any(),
          otherwise: Joi.number()
            .integer()
            .min(1)
            .required()
        }),
        sprintId: Joi.any().when('type', {
          is: 1,
          then: Joi.number()
            .integer()
            .min(1)
            .required(),
          otherwise: Joi.any()
        }),
        type: Joi.number()
          .integer()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      // type = 1 move all tasks of sprint with status untouched to backlog
      if (params.type === 1) {
        if (!params.sprintId) return jsonError(errors.MISSING_REQUIRED_VALUE);
        return await TaskService.updateSprintMultipleTasks({
          type: 'remove',
          sprintId: params.sprintId
        });
      } else {
        if (!params.taskId) return jsonError(errors.MISSING_REQUIRED_VALUE);
        let result = await TaskService.updateTaskToBacklog({
          taskId: params.taskId
        });
        if (result.success) {
          const status = result.result.status;
          const oldSprint = result.result.oldSprint;
          const task = await Task.findOne({
            where: { id: params.taskId },
            include: [{ model: Sprint }, { model: WorkflowState }, { model: Project }]
          });
          if (status === STATUS.ACTIVE) {
            let actor = CronService.DATA.userData[principal.owner];
            // fire socket and store database
            let data = {
              subType: 'remove_task_from_sprint',
              task: {
                id: task.id,
                content: task.content,
                project: {
                  id: task.Project && task.Project.id,
                  name: task.Project && task.Project.name
                },
                sprint: oldSprint.id
              },
              actor: {
                id: principal.owner,
                name: actor && actor.username,
                avatar: actor && actor.profilePhoto
              },
              content: `Task ${task.content} was removed out of ${oldSprint.name} by ${actor && actor.username}!`
            };

            SocketService.notificationToRoom({
              userId: principal.owner,
              notificationableId: task.Project.id,
              notificationableType: NOTIFICATION_TYPE.PROJECT,
              type: 'remove_task',
              data: data
            });
            SocketService.reloadBoard({
              userId: principal.owner,
              notificationableId: task.Project.id,
              notificationableType: NOTIFICATION_TYPE.PROJECT
            });
            SocketService.storeNotificationToUserOfRoomProject({
              projectId: task.Project.id,
              data: data,
              userId: principal.owner,
              type: 'remove_task',
              content: `Task ${task.content} was removed out of ${oldSprint.name} by ${actor && actor.username}!`
            });

            // web push firebase
            let token = await getTokenFirebase(principal.owner);
            // FirebaseService.subscribeToTopic(token, `project_${task.projectId}`);
            FirebaseService.sendMessageToTopic({
              data: {
                title: truncateString(`Remove task from active sprint`, 20),
                body: truncateString(`${actor && actor.username} has removed one task from current sprint`, 30),
                click_action: `${getEnv('DOMAIN')}sprints/${task.projectId}/board/2/${task.id}`,
                icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
              },
              topic: `project_${task.projectId}`,
              token
            });
          }
        }
        return result;
      }
    }
  },

  statistical_sprint_active_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.statisticalSprint({
        projectId: params.projectId
      });
    }
  },

  progress_statistical_all_tasks_by_project: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.statisticalTasksByStatus({
        projectId: params.projectId
      });
    }
  },

  list_current_project_by_userId: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return TaskService.listMyCurrentAllocationOfProject({
        userId: principal.owner
      });
    }
  },

  list_my_current_tasks: {
    policies: [requireToken()],
    action: async (principal, params) => {
      return TaskService.listMyCurrentTasks({
        userId: principal.owner
      });
    }
  },

  list_task_of_all_users: {
    policies: [
      requireToken(),
      validation({
        typeComparison: Joi.number()
          .integer()
          .allow(['', null]),
        estimate: Joi.number()
          .min(0)
          .allow(['', null]),
        date: Joi.string().allow(['', null]),
        name: Joi.string()
          .trim()
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return TaskService.listTaskOfAllUsers({
        typeComparison: params.typeComparison,
        date: params.date,
        estimate: params.estimate || 8,
        name: params.name || '',
        projectId: params.projectId
      });
    }
  },

  list_users_of_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.getUserOfTask({
        taskId: params.taskId
      });
    }
  },

  assign_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        userId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findOne({ where: { id: params.taskId } });
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await TaskService.assignTask({
        taskId: params.taskId,
        userId: params.userId
      });
      if (result.success && principal.owner !== params.userId) {
        let actor = CronService.DATA.userData[principal.owner];
        let task = await Task.findOne({
          where: { id: params.taskId },
          include: [{ model: WorkflowState }, { model: Project }]
        });
        SocketService.joinRoom({
          userId: params.userId,
          notificationableId: params.taskId,
          notificationableType: NOTIFICATION_TYPE.TASK
        });

        let data = {
          task: {
            id: task.id,
            content: task.content,
            sprintId: task.sprintId,
            project: {
              id: task.projectId,
              name: task.Project && task.Project.name
            }
          },
          user: {
            id: params.userId,
            name: CronService.DATA.userData[params.userId].username,
            avatar: CronService.DATA.userData[params.userId].profilePhoto
          },
          content: `You was assign to task ${task.content} of project ${task.Project && task.Project.name}!`
        };
        SocketService.notificationToSpecificUser({
          userId: params.userId,
          type: 'assign_task_project',
          data: data
        });
        SocketService.reloadBoard({
          userId: principal.owner,
          notificationableId: task.Project && task.Project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT
        });
        Notification.create({
          data: JSON.stringify(data),
          actor: principal.owner,
          userId: params.userId,
          notificationableId: params.taskId,
          notificationableType: NOTIFICATION_TYPE.TASK,
          type: 'assign_task_project',
          content: `User ${CronService.DATA.userData[params.userId].username} was assign to task ${
            task.content
          } of project ${task.Project && task.Project.name}!`
        });

        // web push firebase
        const token = await getTokenFirebase(params.userId);
        FirebaseService.subscribeToTopic(token, `task_${task.id}`);
        FirebaseService.sendToDevice({
          data: {
            title: truncateString(`Assign task`, 20),
            body: truncateString(`${actor && actor.username} has assign you to the task`, 30),
            click_action: `${getEnv('DOMAIN')}sprints/${task.projectId}/board/1/${task.id}`,
            icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
          },
          registrationToken: token
        });
      }
      return result;
    }
  },

  // TaskState
  create_task_state: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        toState: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await TaskService.createTaskState({
        taskId: params.taskId,
        toState: params.toState,
        userId: principal.owner
      });
      if (result.success) {
        let task = await Task.findByPk(params.taskId, {
          include: [
            { model: WorkflowState },
            {
              model: User,
              as: 'assigneeAs',
              attributes: ['id', 'name', 'avatar']
            },
            { model: Project }
          ]
        });
        let stateTo = await WorkflowState.findByPk(params.toState);
        const oldUser = result.result.oldUser;
        let data = {};
        if (oldUser !== principal.owner) {
          let actor = CronService.DATA.userData[principal.owner];
          SocketService.joinRoom({
            userId: principal.owner,
            notificationableId: params.taskId,
            notificationableType: NOTIFICATION_TYPE.TASK
          });
          data = {
            subType: 'task_state_change',
            sprintId: task.sprintId,
            task: {
              id: task.id,
              content: task.content,
              state: stateTo.name,
              sprintId: task.sprintId,
              projectId: task.projectId
            },
            actor: {
              id: principal.owner,
              name: actor && actor.username,
              avatar: actor && actor.profilePhoto
            },
            content: `User ${actor.username} moved a task ${task.content} of project ${task.Project &&
              task.Project.name}!`
          };
          SocketService.notificationToSpecificUser({
            userId: oldUser,
            type: 'task_state_change',
            data: data
          });
          Notification.create({
            actor: principal.owner,
            userId: oldUser,
            notificationableId: task.id,
            notificationableType: NOTIFICATION_TYPE.TASK,
            type: 'task_state_change',
            content: `User ${actor.username} moved a task ${task.content} of project ${task.Project &&
              task.Project.name}!`,
            data: JSON.stringify(data)
          });

          // web push firebase
          const token = await getTokenFirebase(oldUser);
          const tokenOwner = await getTokenFirebase(principal.owner);
          FirebaseService.subscribeToTopic(token, `task_${task.id}`);
          FirebaseService.subscribeToTopic(tokenOwner, `task_${task.id}`);
          FirebaseService.sendToDevice({
            data: {
              title: truncateString(`Change state of task`, 20),
              body: truncateString(`${actor && actor.username} has changed your task`, 30),
              click_action: `${getEnv('DOMAIN')}sprints/${task.projectId}/board/2/${task.id}`,
              icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
            },
            registrationToken: token
          });
        }
        SocketService.reloadBoard({
          userId: principal.owner,
          notificationableId: task.Project && task.Project.id,
          notificationableType: NOTIFICATION_TYPE.PROJECT
        });

        return {
          success: result.success,
          result: result.result.taskState
        };
      }
      return result;
    }
  },

  //CRUD comment of task
  create_comment: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await TaskService.createComment({
        userId: principal.owner,
        taskId: params.taskId,
        content: params.content
      });
      if (result.success) {
        let actor = CronService.DATA.userData[principal.owner];
        await TaskService.updateTotalCommentsOfTask({ taskId: params.taskId });
        const comment = result.result;
        // define data response
        const task = await Task.findByPk(params.taskId);
        let data = {
          subType: 'create_comment',
          sprintId: task.sprintId,
          id: comment.id,
          userId: comment.User && comment.User.id,
          taskId: task.id,
          content: comment.content,
          createdAt: comment.createdAt,
          task: {
            id: task.id,
            content: task.content,
            sprintId: task.sprintId,
            projectId: task.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };
        //fire socket
        SocketService.joinRoom({
          userId: comment.User && comment.User.id,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK
        });
        SocketService.notificationToRoom({
          userId: comment.User && comment.User.id,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK,
          type: 'new_comment',
          data: data
        });
        //store database
        SocketService.storeNotificationToUserOfRoomTask({
          data: data,
          userId: comment.User && comment.User.id,
          taskId: task.id,
          type: 'new_comment',
          content: `User ${comment.User && comment.User.name} commented in task ${task.content}!`
        });

        // web push firebase
        const token = await getTokenFirebase(principal.owner);
        // FirebaseService.subscribeToTopic(token, `task_${task.id}`);
        FirebaseService.sendMessageToTopic({
          data: {
            title: truncateString(`Comment task`, 20),
            body: truncateString(`${actor && actor.username} has commented of task`, 30),
            click_action: `${getEnv('DOMAIN')}sprints/${task.projectId}/board/2/${task.id}`,
            icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
          },
          topic: `task_${task.id}`,
          token
        });
      }
      return result;
    }
  },

  list_comments_by_taskId: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.listCommentsByTaskId({
        taskId: params.taskId,
        limit: params.limit || 10,
        offset: params.offset || 0
      });
    }
  },

  list_comments_by_userId: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .min(1)
          .required(),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return TaskService.listCommentsByUserId({
        userId: principal.owner,
        limit: params.limit || 10,
        offset: params.offset || 0
      });
    }
  },

  update_comment: {
    policies: [
      requireToken(),
      validation({
        commentId: Joi.number()
          .integer()
          .min(1)
          .required(),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let comment = await Comment.findByPk(params.commentId);
        let task = await Task.findByPk(comment.commentTableId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await TaskService.updateComment({
        commentId: params.commentId,
        content: params.content,
        userId: principal.owner
      });
      if (result.success) {
        const comment = result.result;
        const task = await Task.findByPk(comment.commentTableId);
        // define data response
        let data = {
          id: comment.id,
          sprintId: task.sprintId,
          userId: comment.User && comment.User.id,
          taskId: task.id,
          content: comment.content,
          createdAt: comment.createdAt,
          task: {
            id: task.id,
            content: task.content,
            sprintId: task.sprintId,
            projectId: task.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };
        //fire socket
        SocketService.joinRoom({
          userId: comment.User && comment.User.id,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK
        });
        SocketService.notificationToRoom({
          userId: comment.User && comment.User.id,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK,
          type: 'update_comment',
          data: data
        });
      }
      return result;
    }
  },

  delete_comment: {
    policies: [
      requireToken(),
      validation({
        commentId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let comment = await Comment.findByPk(params.commentId);
        let task = await Task.findByPk(comment.commentTableId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await TaskService.deleteComment({
        commentId: params.commentId,
        userId: principal.owner
      });
      if (result.success) {
        const comment = result.result;
        await TaskService.updateTotalCommentsOfTask({
          taskId: comment.commentTableId,
          type: 'sub'
        });
        const task = await Task.findByPk(comment.commentTableId);
        // define data response
        let data = {
          id: comment.id,
          sprintId: task.sprintId,
          userId: comment.User && comment.User.id,
          taskId: task.id,
          content: comment.content,
          createdAt: comment.createdAt,
          task: {
            id: task.id,
            content: task.content,
            sprintId: task.sprintId,
            projectId: task.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };
        //fire socket
        SocketService.joinRoom({
          userId: comment.userId,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK
        });
        SocketService.notificationToRoom({
          userId: comment.userId,
          notificationableId: task.id,
          notificationableType: NOTIFICATION_TYPE.TASK,
          type: 'delete_comment',
          data: data
        });
      }
      return result;
    }
  },

  // Attachment Task
  list_attachment_by_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.listAttachmentByTask({
        taskId: params.taskId,
        limit: params.limit || getEnv('PAGINATE_ATTACHMENT_LIMIT'),
        offset: params.offset || 0
      });
    }
  },

  add_attachment_by_task: {
    policies: [
      requireToken(),
      validation({
        taskId: Joi.number()
          .integer()
          .min(1)
          .required(),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let task = await Task.findByPk(params.taskId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.addAttachment({
        taskId: params.taskId,
        files: params.files,
        projectId: params.projectId,
        userId: principal.owner
      });
    }
  },

  remove_attachment_by_task: {
    policies: [
      requireToken(),
      validation({
        attachmentId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let attachment = await Attachment.findByPk(params.attachmentId);
        if (!attachment) return false;

        let task = await Task.findByPk(attachment.attachmentTableId);
        return task && task.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.removeAttachment({
        attachmentId: params.attachmentId
      });
    }
  },

  close_all_issue: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .min(1)
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return TaskService.closeAllIssue({
        projectId: params.projectId
      });
    }
  }
};
