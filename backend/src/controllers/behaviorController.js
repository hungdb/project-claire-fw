import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { BehaviorService } from '../services/BehaviorService';
import { TYPE_APPLY, TYPE_ACTION, Behavior } from '../models/schema/Behavior';
import { projectDepend } from '../utils/rpc_conditions';
import { Sprint } from '../models/schema/Sprint';
import { LogBehaviorSprint } from '../models/schema/LogBehaviorSprint';
import { User } from '../models/schema/User';
import { LOG_DETAIL_BEHAVIOR_TABLE_TYPE, LogDetailBehavior } from '../models/schema/LogDetailBehavior';
import { jsonSuccess } from '../utils/system';
import { KPIBehavior } from '../models/schema/KPIBehavior';
import { TYPE_GET_BEHAVIOR } from '../utils/type';

module.exports = {
  list_behaviors: {
    policies: [requireToken()],
    action: async principal => {
      return BehaviorService.listBehavior({
        type: TYPE_GET_BEHAVIOR.DEFAULT
      });
    }
  },

  list_behaviors_for_sprint: {
    policies: [requireToken()],
    action: async principal => {
      return BehaviorService.listBehavior({
        type: TYPE_GET_BEHAVIOR.SPRINT,
        isApply: true
      });
    }
  },

  create_behavior: {
    policies: [
      requireToken(),
      validation({
        activity: Joi.string()
          .trim()
          .required(),
        point: Joi.number()
          .required()
          .min(0),
        typeApply: Joi.number()
          .integer()
          .required()
          .valid(Object.values(TYPE_APPLY)),
        typeAction: Joi.number()
          .integer()
          .required()
          .valid(Object.values(TYPE_ACTION)),
        decider: Joi.string().trim(),
        note: Joi.string().trim(),
        isApply: Joi.boolean(),
        startDate: Joi.date().allow(['', null]),
        endDate: Joi.date().allow(['', null]),
        description: Joi.string().allow(['', null]),
        typeSubApply: Joi.number().allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return BehaviorService.addBehavior({
        activity: params.activity,
        point: params.point,
        typeApply: params.typeApply,
        typeAction: params.typeAction,
        decider: params.decider,
        note: params.note,
        isApply: params.isApply,
        startDate: params.startDate,
        endDate: params.endDate,
        description: params.description,
        typeSubApply: params.typeSubApply
      });
    }
  },

  update_behavior: {
    policies: [
      requireToken(),
      validation({
        behaviorId: Joi.number()
          .integer()
          .required()
          .min(1),
        activity: Joi.string()
          .trim()
          .required(),
        point: Joi.number()
          .required()
          .min(0),
        typeApply: Joi.number()
          .integer()
          .required()
          .valid(Object.values(TYPE_APPLY)),
        typeAction: Joi.number()
          .integer()
          .required()
          .valid(Object.values(TYPE_ACTION)),
        decider: Joi.string().trim(),
        note: Joi.string().trim(),
        isApply: Joi.boolean(),
        startDate: Joi.date().allow(['', null]),
        endDate: Joi.date().allow(['', null]),
        description: Joi.string().allow(['', null]),
        typeSubApply: Joi.number().allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return BehaviorService.updateBehavior({
        behaviorId: params.behaviorId,
        activity: params.activity,
        point: params.point,
        typeApply: params.typeApply,
        typeAction: params.typeAction,
        decider: params.decider,
        note: params.note,
        isApply: params.isApply,
        startDate: params.startDate,
        endDate: params.endDate,
        description: params.description,
        typeSubApply: params.typeSubApply
      });
    }
  },

  remove_behavior: {
    policies: [
      requireToken(),
      validation({
        behaviorId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return BehaviorService.deleteBehavior({
        behaviorId: params.behaviorId
      });
    }
  },

  log_behavior_sprint: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        behaviors: Joi.array().required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await BehaviorService.addLogBehaviorSprint({
        actor: principal.owner,
        userId: params.userId,
        sprintId: params.sprintId,
        behaviors: params.behaviors
      });
      if (!result.success) return result;

      const resultLogDetail = await BehaviorService.createLogDetailBehaviorWithLogBehaviorSprint({
        behaviors: params.behaviors,
        logBehaviorSprints: result.result
      });

      if (!resultLogDetail.success) return resultLogDetail;

      const include = [
        {
          model: User,
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        {
          model: LogDetailBehavior,
          as: 'logDetailBehaviors',
          where: {
            logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
          },
          include: [
            {
              model: Behavior,
              as: 'behavior',
              attributes: ['id', 'activity', 'point', 'typeApply', 'typeAction']
            }
          ],
          required: false
        }
      ];

      const results = [];
      for (let i = 0; i < result.result.length; i++) {
        const logBehaviorSprint = result.result[i];
        const logBehaviorSprintFull = await LogBehaviorSprint.findByPk(logBehaviorSprint.id, {
          include
        });
        results.push(logBehaviorSprintFull);
      }

      return jsonSuccess(results);
    }
  },

  create_kpi_behavior: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1),
        behaviors: Joi.array().required(),
        evaluatedAt: Joi.date().required()
      })
    ],
    action: async (principal, params) => {
      const result = await BehaviorService.createKpiBehavior({
        actor: principal.owner,
        userId: params.userId,
        behaviors: params.behaviors,
        evaluatedAt: params.evaluatedAt
      });
      if (!result.success) return result;

      const resultLogDetail = await BehaviorService.createLogDetailBehaviorWithKpiBehavior({
        behaviors: params.behaviors,
        kpiBehaviorId: result.result.id
      });

      if (!resultLogDetail.success) return resultLogDetail;

      const include = [
        {
          model: User,
          as: 'user',
          attributes: ['id', 'name', 'avatar']
        },
        {
          model: User,
          as: 'infoActor',
          attributes: ['id', 'name', 'avatar']
        },
        {
          model: LogDetailBehavior,
          as: 'logDetailBehaviors',
          where: {
            logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
          },
          include: [
            {
              model: Behavior,
              as: 'behavior',
              attributes: ['id', 'activity', 'point', 'typeApply', 'typeAction']
            }
          ],
          required: false
        }
      ];

      const logKpiBehavior = result.result;
      const logKpiBehaviorFull = await KPIBehavior.findByPk(logKpiBehavior.id, {
        include
      });

      return jsonSuccess(logKpiBehaviorFull);
    }
  },

  remove_kpi_behavior: {
    policies: [
      requireToken(),
      validation({
        kpiBehaviorId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return BehaviorService.removeKpiBehavior({
        kpiBehaviorId: params.kpiBehaviorId
      });
    }
  },

  get_list_kpi_behavior: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        date: Joi.date(),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return BehaviorService.getListKpiBehavior({
        userId: params.userId,
        date: params.date,
        limit: params.limit,
        page: params.page
      });
    }
  },

  get_list_log_behavior_in_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: (principal, params) => {
      return BehaviorService.getListLogBehaviorInSprint({
        sprintId: params.sprintId
      });
    }
  },

  remove_log_behavior_in_sprint: {
    policies: [
      requireToken(),
      validation({
        logBehaviorId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let sprint = await LogBehaviorSprint.findByPk(params.logBehaviorId, {
          include: [{ model: Sprint }]
        });
        return sprint && sprint.Sprint && sprint.Sprint.projectId;
      })
    },
    action: (principal, params) => {
      return BehaviorService.removeLogBehaviorInSprint({
        owner: principal.owner,
        logBehaviorId: params.logBehaviorId
      });
    }
  },

  create_multiple_kpi_behavior: {
    policies: [
      requireToken(),
      validation({
        data: Joi.array()
          .items({
            username: Joi.string()
              .trim()
              .required(),
            codes: Joi.array().required(),
            evaluatedAt: Joi.date()
          })
          .required()
      })
    ],
    action: async (principal, params) => {
      const resultKpiBehaviors = await BehaviorService.createMultipleKpiBehavior({
        data: params.data,
        actor: principal.owner
      });
      if (!resultKpiBehaviors.success) return resultKpiBehaviors;

      const kpiBehaviorIds = resultKpiBehaviors.result.map(item => item.id);
      const logDetailParams = params.data.map((item, index) => ({
        ...item,
        kpiBehaviorId: kpiBehaviorIds[index]
      }));
      const resultLogDetail = await BehaviorService.createLogDetailBehaviorWithKpiBehaviors({
        data: logDetailParams
      });

      if (!resultLogDetail.success) return resultLogDetail;

      const logKpiBehaviorFull = await BehaviorService.getListKpiBehaviorWithLog({
        ids: kpiBehaviorIds,
        logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
      });

      return jsonSuccess(logKpiBehaviorFull);
    }
  }
};
