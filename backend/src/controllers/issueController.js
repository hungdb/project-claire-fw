import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { IssueService } from '../services/IssueService';
import { projectDepend } from '../utils/rpc_conditions';
import { Issue, ISSUE_STATUS } from '../models/schema/Issue';
import { getTokenFirebase, parseJSONIsArray, truncateString } from '../utils/common';
import { SocketService } from '../services/SocketService';
import { NOTIFICATION_TYPE } from '../models/schema/Notification';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { FirebaseService } from '../services/FirebaseService';
import { CronService } from '../services/CronService';
import { config } from '../core/config';
import { TASK_TYPE } from '../models/schema/Task';

module.exports = {
  create_issue: {
    policies: [
      requireToken(),
      validation({
        subject: Joi.string()
          .trim()
          .required(),
        content: Joi.string()
          .trim()
          .allow(['', null]),
        userRelate: Joi.array().default([]),
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        estimate: Joi.number().min(0),
        assignee: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single(),
        typeTask: Joi.number()
          .integer()
          .valid(Object.values(TASK_TYPE))
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return await IssueService.createIssue({
        subject: params.subject,
        content: params.content || null,
        userRelate: params.userRelate,
        projectId: params.projectId,
        creator: principal.owner,
        estimate: params.estimate,
        assignee: params.assignee,
        files: params.files,
        typeTask: params.typeTask
      });
    }
  },

  update_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1),
        subject: Joi.string().trim(),
        content: Joi.string()
          .trim()
          .allow(['', null]),
        status: Joi.number()
          .integer()
          .valid(Object.values(ISSUE_STATUS)),
        userRelate: Joi.array().allow(['', null]),
        estimate: Joi.number().min(0),
        assignee: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        typeTask: Joi.number()
          .integer()
          .valid(Object.values(TASK_TYPE))
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.updateIssue({
        issueId: params.issueId,
        subject: params.subject,
        content: params.content,
        status: params.status,
        userRelate: params.userRelate && Array.isArray(params.userRelate) ? params.userRelate : null,
        estimate: params.estimate,
        assignee: params.assignee,
        typeTask: params.typeTask
      });
    }
  },

  remove_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      const checkRemoveIssue = await IssueService.checkRemoveIssue({
        issueId: params.issueId
      });
      if (!checkRemoveIssue.success) return checkRemoveIssue;
      return IssueService.deleteIssue({
        issueId: params.issueId
      });
    }
  },

  get_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.getIssue({
        issueId: params.issueId
      });
    }
  },

  get_list_issue: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        subject: Joi.string()
          .trim()
          .allow(['', null]),
        status: Joi.number()
          .integer()
          .valid(Object.values(ISSUE_STATUS))
          .allow(['', 4, null]),
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        creator: Joi.number()
          .integer()
          .min(1)
          .allow(['', 0, null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.getListIssue({
        limit: params.limit || 10,
        page: params.page || 1,
        projectId: params.projectId,
        status: params.status,
        subject: params.subject,
        creator: params.creator
      });
    }
  },

  statical_status_of_issue: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.statisticalIssues({
        projectId: params.projectId
      });
    }
  },

  // comment issue
  create_comment_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await IssueService.createCommentIssue({
        issueId: params.issueId,
        content: params.content,
        userId: principal.owner
      });
      if (result.success) {
        const comment = result.result;
        await IssueService.updateTotalCommentsOfIssue({
          issueId: comment.commentTableId
        });
        const issue = await Issue.findByPk(comment.commentTableId);
        let actor = CronService.DATA.userData[principal.owner];
        // define data response
        let data = {
          subType: 'create_comment_issue',
          id: comment.id,
          userId: comment.User && comment.User.id,
          issueId: issue.id,
          content: comment.content,
          createdAt: comment.createdAt,
          issue: {
            id: issue.id,
            subject: issue.subject,
            content: issue.content,
            projectId: issue.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };
        //fire socket
        SocketService.joinRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE
        });
        SocketService.notificationToRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE,
          type: 'new_comment_issue',
          data: data
        });
        //store database
        SocketService.storeNotificationToUserOfRoomIssue({
          data: data,
          userId: comment.User && comment.User.id,
          issueId: issue.id,
          userRelate: parseJSONIsArray(issue.userRelate),
          type: 'new_comment_issue',
          content: `User ${comment.User.name} commented in issue ${issue.subject}!`
        });
        // web push firebase
        const token = await getTokenFirebase(principal.owner);
        // FirebaseService.subscribeToTopic(token, `issue_${issue.id}`);
        FirebaseService.sendMessageToTopic({
          data: {
            title: truncateString(`Comment issue`, 20),
            body: truncateString(`${actor && actor.username} has commented of issue`, 30),
            click_action: `${getEnv('DOMAIN')}sprints/${issue.projectId}/issues/3/${issue.id}`,
            icon: `${getEnv('LINK_ICON')}${actor && actor.profilePhoto}`
          },
          topic: `issue_${issue.id}`,
          token
        });
      }

      return result;
    }
  },

  list_comments_issue_by_issueId: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.listCommentIssueByIssueId({
        issueId: params.issueId,
        limit: params.limit || 10,
        page: params.page || 1
      });
    }
  },

  list_comments_issue_by_userId: {
    policies: [
      requireToken(),
      validation({
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return IssueService.listMyIssue({
        userId: principal.owner,
        limit: params.limit || 10,
        page: params.page || 1
      });
    }
  },

  update_comment_issue: {
    policies: [
      requireToken(),
      validation({
        commentIssueId: Joi.number()
          .integer()
          .required()
          .min(1),
        content: Joi.string()
          .trim()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let commentIssue = await Comment.findByPk(params.commentIssueId);
        let issue = await Issue.findByPk(commentIssue.commentTableId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await IssueService.updateCommentIssue({
        commentIssueId: params.commentIssueId,
        content: params.content,
        userId: principal.owner
      });
      if (result.success) {
        const comment = result.result;
        const issue = await Issue.findByPk(comment.commentTableId);
        // define data response
        let data = {
          subType: 'update_comment_issue',
          id: comment.id,
          userId: comment.User && comment.User.id,
          issueId: issue.id,
          content: comment.content,
          createdAt: comment.createdAt,
          issue: {
            id: issue.id,
            subject: issue.subject,
            content: issue.content,
            projectId: issue.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };
        SocketService.joinRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE
        });
        SocketService.notificationToRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE,
          type: 'update_comment_issue',
          data: data
        });
      }
      return result;
    }
  },

  delete_comment_issue: {
    policies: [
      requireToken(),
      validation({
        commentIssueId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let commentIssue = await Comment.findByPk(params.commentIssueId);
        let issue = await Issue.findByPk(commentIssue.commentTableId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await IssueService.deleteCommentIssue({
        commentIssueId: params.commentIssueId,
        userId: principal.owner
      });
      if (result.success) {
        const comment = result.result;
        await IssueService.updateTotalCommentsOfIssue({
          issueId: comment.commentTableId,
          type: 'sub'
        });
        const issue = await Issue.findByPk(comment.commentTableId);
        // define data response
        let data = {
          subType: 'delete_comment_issue',
          id: comment.id,
          userId: comment.User && comment.User.id,
          issueId: issue.id,
          content: comment.content,
          createdAt: comment.createdAt,
          issue: {
            id: issue.id,
            subject: issue.subject,
            content: issue.content,
            projectId: issue.projectId
          },
          actor: {
            id: comment.User && comment.User.id,
            name: comment.User && comment.User.name,
            avatar: comment.User && comment.User.avatar
          }
        };

        SocketService.joinRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE
        });
        SocketService.notificationToRoom({
          userId: comment.User && comment.User.id,
          notificationableId: issue.id,
          notificationableType: NOTIFICATION_TYPE.ISSUE,
          type: 'delete_comment_issue',
          data: data
        });
      }
      return result;
    }
  },

  create_multiple_issue: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        data: Joi.array()
          .items(
            Joi.object({
              subject: Joi.string()
                .trim()
                .required(),
              content: Joi.string().trim(),
              estimate: Joi.number().min(0),
              assignee: Joi.string()
                .trim()
                .allow(['', null]),
              type: Joi.number()
                .integer()
                .valid(Object.values(TASK_TYPE)),
              userRelate: Joi.array()
                .allow(['', null])
                .default([])
            })
          )
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.createMultipleIssue({
        data: params.data,
        projectId: params.projectId,
        creator: principal.owner
      });
    }
  },

  add_attachment_by_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1),
        files: Joi.array()
          .items({
            data: Joi.binary().required(),
            mimetype: Joi.string()
              .trim()
              .required()
              .valid(config.fileUpload.settings.mimes)
          })
          .max(config.fileUpload.settings.limits.files)
          .single()
          .required()
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.addAttachment({
        issueId: params.issueId,
        files: params.files,
        userId: principal.owner
      });
    }
  },

  remove_attachment_by_issue: {
    policies: [
      requireToken(),
      validation({
        attachmentId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const attachment = await Attachment.findOne({
          id: params.attachmentId,
          attachmentTableType: ATTACHMENT_TYPE.ISSUE
        });
        if (!attachment) return false;

        const issue = await Issue.findByPk(attachment.attachmentTableId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.removeAttachment({
        attachmentId: params.attachmentId
      });
    }
  },

  list_attachment_by_issue: {
    policies: [
      requireToken(),
      validation({
        issueId: Joi.number()
          .integer()
          .required()
          .min(1),
        offset: Joi.number()
          .integer()
          .min(0)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const issue = await Issue.findByPk(params.issueId);
        return issue && issue.projectId;
      })
    },
    action: async (principal, params) => {
      return IssueService.listAttachmentByIssue({
        issueId: params.issueId,
        limit: params.limit || getEnv('PAGINATE_ATTACHMENT_LIMIT'),
        offset: params.offset || 0
      });
    }
  }
};
