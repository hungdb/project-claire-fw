import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { WorkflowService } from '../services/WorkflowService';
import { projectDepend } from '../utils/rpc_conditions';
import { EFFORT_TYPES } from '../models/schema/WorkflowState';

module.exports = {
  // Workflow
  create_workflow: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        allowParallelTask: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.createWorkflow({
        name: params.name,
        allowParallelTask: !!params.allowParallelTask
      });
    }
  },

  update_workflow: {
    policies: [
      requireToken(),
      validation({
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        allowParallelTask: Joi.boolean()
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.updateWorkflow({
        workflowId: params.workflowId,
        name: params.name,
        allowParallelTask: !!params.allowParallelTask
      });
    }
  },

  remove_workflow: {
    policies: [
      requireToken(),
      validation({
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.deleteWorkflow({
        workflowId: params.workflowId
      });
    }
  },

  get_workflow: {
    policies: [
      requireToken(),
      validation({
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.getWorkflow({
        workflowId: params.workflowId
      });
    }
  },

  list_workflow: {
    policies: [requireToken()],
    action: async principal => {
      return WorkflowService.listWorkflow();
    }
  },

  //WorkflowState
  create_workflow_state: {
    policies: [
      requireToken(),
      validation({
        name: Joi.string()
          .trim()
          .required(),
        effortType: Joi.string()
          .trim()
          .required()
          .valid(Object.values(EFFORT_TYPES)),
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1),
        ordinalNumber: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.createWorkflowState({
        name: params.name,
        effortType: params.effortType,
        workflowId: params.workflowId,
        ordinalNumber: params.ordinalNumber
      });
    }
  },

  update_workflow_state: {
    policies: [
      requireToken(),
      validation({
        workflowStateId: Joi.number()
          .integer()
          .required()
          .min(1),
        name: Joi.string()
          .trim()
          .required(),
        effortType: Joi.string()
          .trim()
          .required()
          .valid(Object.values(EFFORT_TYPES)),
        ordinalNumber: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.updateWorkflowState({
        workflowStateId: params.workflowStateId,
        name: params.name,
        effortType: params.effortType,
        ordinalNumber: params.ordinalNumber
      });
    }
  },

  remove_workflow_state: {
    policies: [
      requireToken(),
      validation({
        workflowStateId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.deleteWorkflowState({
        workflowStateId: params.workflowStateId
      });
    }
  },

  list_workflow_state_by_workflowId: {
    policies: [
      requireToken(),
      validation({
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.listWorkflowStateByWorkflowId({
        workflowId: params.workflowId
      });
    }
  },

  list_workflow_state_by_projectId: {
    policies: [
      requireToken(),
      validation({
        projectId: Joi.number()
          .integer()
          .required()
          .min(1),
        type: Joi.number()
          .integer()
          .allow(['', null]),
        assignee: Joi.number()
          .min(1)
          .integer()
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        return params.projectId;
      })
    },
    action: async (principal, params) => {
      return WorkflowService.listWorkflowStateByProjectId({
        projectId: params.projectId,
        type: params.type,
        userId: principal.owner,
        assignee: params.assignee
      });
    }
  },

  //WorkflowDetail
  create_workflow_detail: {
    policies: [
      requireToken(),
      validation({
        workflowId: Joi.number()
          .integer()
          .required()
          .min(1),
        fromState: Joi.number()
          .integer()
          .required()
          .min(1),
        toState: Joi.number()
          .integer()
          .required()
          .min(1),
        description: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.createWorkflowDetail({
        workflowId: params.workflowId,
        fromState: params.fromState,
        toState: params.toState,
        description: params.description
      });
    }
  },

  update_workflow_detail: {
    policies: [
      requireToken(),
      validation({
        workflowDetailId: Joi.number()
          .integer()
          .required()
          .min(1),
        fromState: Joi.number()
          .integer()
          .required()
          .min(1),
        toState: Joi.number()
          .integer()
          .required()
          .min(1),
        description: Joi.string()
          .trim()
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      return WorkflowService.updateWorkflowDetail({
        workflowDetailId: params.workflowDetailId,
        fromState: params.fromState,
        toState: params.toState,
        description: params.description
      });
    }
  },

  remove_workflow_detail: {
    policies: [
      requireToken(),
      validation({
        workflowDetailId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: (principal, params) => {
      return WorkflowService.deleteWorkflowDetail({
        workflowDetailId: params.workflowDetailId
      });
    }
  }
};
