import { jsonSuccess } from '../utils/system';

module.exports = {
  redirect_check_in: {
    policies: [],
    action: async (principal, params) => jsonSuccess()
  },

  redirect_identity: {
    policies: [],
    action: async (principal, params) => jsonSuccess()
  },

  redirect_asset: {
    policies: [],
    action: async (principal, params) => jsonSuccess()
  },

  filter_user_on_board: {
    policies: [],
    permissionConditions: {
      project_depend: true
    },
    action: async (principal, params) => jsonSuccess()
  }
};
