import Joi from 'joi';
import { requireToken } from '../middleware/policies';
import { validation } from '../middleware/validators';
import { projectDepend } from '../utils/rpc_conditions';
import { TYPE_USER_FILTER_ALLOCATION } from '../utils/type';
import { Sprint } from '../models/schema/Sprint';
import { Allocation } from '../models/schema/Allocation';
import { AllocationService } from '../services/AllocationService';

module.exports = {
  create_allocation: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1),
        data: Joi.array()
          .required()
          .items(
            Joi.object().keys({
              userId: Joi.number()
                .integer()
                .required()
                .min(1),
              effortPercent: Joi.number()
                .required()
                .min(0)
                .max(100),
              startDate: Joi.date().required(),
              endDate: Joi.date()
                .min(Joi.ref('startDate'))
                .required(),
              label: Joi.number()
                .integer()
                .min(1)
                .allow(['', null])
            })
          )
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        const sprint = await Sprint.findByPk(params.sprintId);
        return sprint && sprint.projectId;
      })
    },
    action: async (principal, params) => {
      const result = await AllocationService.createAllocation({
        sprintId: params.sprintId,
        data: params.data
      });
      if (result.success) AllocationService.storeAllAllocationToRedis();
      return result;
    }
  },

  update_allocation: {
    policies: [
      requireToken(),
      validation({
        allocationId: Joi.number()
          .integer()
          .required()
          .min(1),
        effortPercent: Joi.number()
          .required()
          .min(0)
          .max(100),
        startDate: Joi.date().required(),
        endDate: Joi.date()
          .min(Joi.ref('startDate'))
          .required(),
        label: Joi.number()
          .integer()
          .min(1)
          .allow(['', null])
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let allocation = await Allocation.findByPk(params.allocationId, {
          include: [{ model: Sprint }]
        });
        return allocation && allocation.Sprint && allocation.Sprint.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await AllocationService.updateAllocation({
        allocationId: params.allocationId,
        effortPercent: params.effortPercent,
        startDate: params.startDate,
        endDate: params.endDate,
        label: params.label
      });
      if (result.success) AllocationService.storeAllAllocationToRedis();
      return result;
    }
  },

  remove_allocation: {
    policies: [
      requireToken(),
      validation({
        allocationId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    permissionConditions: {
      project_depend: projectDepend(async params => {
        let allocation = await Allocation.findByPk(params.allocationId, {
          include: [{ model: Sprint }]
        });
        return allocation && allocation.Sprint && allocation.Sprint.projectId;
      })
    },
    action: async (principal, params) => {
      let result = await AllocationService.deleteAllocation({
        allocationId: params.allocationId
      });
      if (result.success) AllocationService.storeAllAllocationToRedis();
      return result;
    }
  },

  list_allocation_of_user: {
    policies: [
      requireToken(),
      validation({
        userId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return AllocationService.getListAllocationOfUser({
        userId: params.userId
      });
    }
  },

  list_allocation_of_sprint: {
    policies: [
      requireToken(),
      validation({
        sprintId: Joi.number()
          .integer()
          .required()
          .min(1)
      })
    ],
    action: async (principal, params) => {
      return AllocationService.getListAllocationOfSprint({
        sprintId: params.sprintId
      });
    }
  },

  filter_allocation: {
    policies: [
      requireToken(),
      validation({
        skillId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        projectId: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        startDate: Joi.date().allow(['', null]),
        endDate: Joi.date().allow(['', null]),
        page: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        limit: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        effortPercent: Joi.number()
          .min(0)
          .max(100)
          .allow(['', null]),
        name: Joi.string()
          .trim()
          .allow(['', null]),
        label: Joi.number()
          .integer()
          .min(1)
          .allow(['', null]),
        type: Joi.number()
          .integer()
          .valid(Object.values(TYPE_USER_FILTER_ALLOCATION))
          .allow(['', null])
      })
    ],
    action: async (principal, params) => {
      const valid = Object.values(TYPE_USER_FILTER_ALLOCATION);
      return AllocationService.filterAllocation({
        startDate: params.startDate,
        endDate: params.endDate,
        effortPercent: params.effortPercent,
        skillId: params.skillId,
        name: params.name,
        projectId: params.projectId,
        limit: params.limit || 10,
        page: params.page || 1,
        label: params.label,
        actor: principal.owner,
        type: params.type && valid.includes(params.type) ? params.type : TYPE_USER_FILTER_ALLOCATION.DEFAULT
      });
    }
  }
};
