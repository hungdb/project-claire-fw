import { NotificationService } from '../services/NotificationService';
import { AllocationService } from '../services/AllocationService';

const CallbackController = require('express').Router();

CallbackController.post('/callback/twilio', (req, res) => {
  let data = req.body;
  let sid = data['CallSid'];
  if (!!sid) {
    //-- end this call, user has answered
    NotificationService.endPhoneCall(sid);
  }
  return res.end();
});

// hotfix allocation error
CallbackController.post('/hotfix/allocation', async (req, res) => {
  const { allocationId, endDate, secret } = req.body;
  if (secret !== 'dongvd') return res.end('secret failed');
  if (!allocationId || !endDate) return res.end('params failed');
  let result = await AllocationService.updateAllocation({
    allocationId: allocationId,
    endDate: endDate
  });
  if (!result.success) return res.end('update allocation failed');
  AllocationService.storeAllAllocationToRedis();
  return res.end('success');
});

export { CallbackController };
