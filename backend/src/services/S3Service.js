import aws from 'aws-sdk';
// import uuidv1 from "uuid/v1";
import { jsonError, jsonSuccess, errors, logger } from '../utils/system';
// import {getExtension} from "../utils/common";

class S3Service {
  static async boot() {
    aws.config.update({
      accessKeyId: getEnv('AWS_ACCESS_KEY_ID'),
      secretAccessKey: getEnv('AWS_ACCESS_KEY_SECRET'),
      region: 'ap-southeast-1',
      signatureVersion: 'v4'
    });

    S3Service.DATA = {
      S3_BUCKET: getEnv('S3_BUCKET'),
      S3_ATTACHMENT_FOLDER: 'attachment-files',
      s3Client: new aws.S3({ apiVersion: '2006-03-01' })
    };
  }

  static async uploadArrayFileToS3({ bucket, files, fileNames, contentTypes, realNames }) {
    let tmp = [];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      let uploadParams = {
        bucket,
        fileName: fileNames[i],
        fileContent: file.data,
        realName: realNames[i],
        contentType: contentTypes[i]
      };
      let result = await S3Service.uploadFileToS3(uploadParams);

      //-- if not success to upload one file then abort all
      if (!result.success) {
        await S3Service.removeArrayFileS3({
          bucket,
          arrayFile: tmp
        });
        return result;
      }

      tmp.push({
        fileName: file.name,
        filePath: `${uploadParams.bucket}/${uploadParams.fileName}`,
        fileType: file.mimetype
      });
    }

    return jsonSuccess(tmp);
  }

  static async uploadFileToS3({ bucket, fileName, fileContent, realName, contentType, ACL }) {
    const params = {
      Body: fileContent,
      Bucket: bucket,
      Key: fileName,
      ContentType: contentType,
      ContentDisposition: `inline; filename=${encodeURIComponent(realName)}`
    };
    if (ACL) params.ACL = ACL;
    return new Promise(resolve => {
      S3Service.DATA.s3Client.putObject(params, err => {
        if (err) {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess());
      });
    });
  }

  static async getSignedUrl({ bucket, filePath }) {
    return new Promise(resolve => {
      try {
        const arrPath = filePath.split('/');
        const fileName = `${arrPath[1]}/${arrPath[2]}`;
        const params = {
          Bucket: bucket,
          Key: fileName
        };
        S3Service.DATA.s3Client.getSignedUrl('getObject', params, (err, url) => {
          if (err) {
            logger.error(err);
            return resolve(jsonError(errors.SYSTEM_ERROR));
          }
          return resolve(jsonSuccess(url));
        });
      } catch (e) {
        return resolve(jsonError(errors.SYSTEM_ERROR));
      }
    });
  }

  static async getSignedUrlUpload({ bucket, fileName, fileContent }) {
    return new Promise(resolve => {
      const params = {
        Body: fileContent,
        Bucket: bucket,
        Key: fileName
      };
      S3Service.DATA.s3Client.getSignedUrl('putObject', params, (err, url) => {
        if (err) {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess(url));
      });
    });
  }

  static async uploadFileToS3WithExpires({ bucket, fileName, fileContent, dateExpired }) {
    return new Promise(resolve => {
      S3Service.DATA.s3Client.putObject(
        {
          Body: fileContent,
          Bucket: bucket,
          Key: fileName,
          Expires: dateExpired
        },
        err => {
          if (err) {
            logger.error(err);
            return resolve(jsonError(errors.SYSTEM_ERROR));
          }
          return resolve(jsonSuccess());
        }
      );
    });
  }

  static async removeFileS3({ bucket, fileName }) {
    return new Promise(resolve => {
      S3Service.DATA.s3Client.deleteObject(
        {
          Bucket: bucket,
          Key: fileName
        },
        err => {
          if (err) {
            logger.error(err);
            return resolve(jsonError(errors.SYSTEM_ERROR));
          }
          return resolve(jsonSuccess());
        }
      );
    });
  }

  static async removeArrayFileS3({ bucket, arrayFile }) {
    try {
      await Promise.all(arrayFile.map(fileName => S3Service.removeFileS3({ bucket, fileName })));
      return jsonSuccess();
    } catch (err) {
      logger.error(err);
      return jsonError(errors.SYSTEM_ERROR);
    }
  }

  static async getDataFile({ bucket, filePath }) {
    return new Promise(resolve => {
      try {
        const arrPath = filePath.split('/');
        const fileName = `${arrPath[1]}/${arrPath[2]}`;
        const params = {
          Bucket: bucket,
          Key: fileName
        };
        // const fileType = getExtension();
        const typeFile = fileName.slice(fileName.indexOf('.') + 1, fileName.length);
        S3Service.DATA.s3Client.getObject(params, (err, data) => {
          if (err) {
            logger.error(err);
            return resolve(jsonError(errors.NOT_FOUND_ERROR));
          }
          // fs.writeFileSync(path.join(__dirname, '../text.xlsx'), data.Body);
          return resolve(jsonSuccess(Object.assign(data, { typeFile: typeFile })));
        });
      } catch (e) {
        logger.error(e);
        return resolve(jsonError(errors.SYSTEM_ERROR));
      }
    });
  }

  static async removeAttachmentsS3({ attachments }) {
    const tobeDeleted = Array.isArray(attachments) ? attachments : [attachments];
    return new Promise(async resolve => {
      try {
        for (let i = 0; i < tobeDeleted.length; i += 1) {
          const attachment = tobeDeleted[i];
          const arrPath = attachment.url.split('/');
          const result = await S3Service.removeFileS3({
            bucket: getEnv('S3_BUCKET'),
            fileName: `${arrPath[1]}/${arrPath[2]}`
          });
          if (!result.success) return resolve(result);
        }
        return resolve(jsonSuccess);
      } catch (e) {
        return resolve(jsonError(errors.REMOVE_FILE_S3_FAILED));
      }
    });
  }
}

export { S3Service };
