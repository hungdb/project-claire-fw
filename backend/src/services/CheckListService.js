import { Op } from 'sequelize';
import { sequelize } from '../core/boot';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { CheckListItem } from '../models/schema/CheckListItem';
import { CheckList } from '../models/schema/CheckList';
import { Project } from '../models/schema/Project';

class CheckListService {
  static async boot() {}

  static async createItemCheck({ name, description }) {
    let item = await CheckListItem.findOne({ where: { name: name.trim() } });
    if (item) return jsonError(errors.DUPLICATED_ERROR);
    item = await CheckListItem.create({
      name: name.trim(),
      description
    });
    return jsonSuccess(item);
  }

  static async updateItemCheck({ itemCheckId, name, description }) {
    let item = await CheckListItem.findByPk(itemCheckId);
    if (!item) return jsonError(errors.NOT_FOUND_ERROR);
    let updatedItem = await CheckListItem.findOne({
      where: {
        name: name.trim(),
        id: { [Op.ne]: itemCheckId }
      }
    });
    if (updatedItem) return jsonError(errors.DUPLICATED_ERROR);
    item = await item.update({
      name: name ? name.trim() : item.name,
      description: description ? description : item.description
    });
    return jsonSuccess(item);
  }

  static async listAllItem() {
    let items = await CheckListItem.findAll();
    return jsonSuccess(items);
  }

  static async checkUncheckItem({ projectId, checkItemId, isChecked }) {
    let checkItem = await CheckList.findOne({
      where: { checkItemId, projectId }
    });
    if (!checkItem) return jsonError(errors.NOT_FOUND_ERROR);
    checkItem = await checkItem.update({ isChecked });
    return jsonSuccess(checkItem);
    // let item = await CheckListItem.findByPk(checkItemId);
    // let project = await Project.findByPk(projectId);
    // let itemCheckOfProject = await CheckList.findOne(
    //   {where: {projectId: projectId, checkItemId: checkItemId}}
    // );
    // if(!item || !project || !itemCheckOfProject)
    //   return jsonError(errors.NOT_FOUND_ERROR);
    //
    // await itemCheckOfProject.update({
    //   isChecked: isChecked
    // });
    // let listItemsOfProject = await CheckList.findAll({
    //   where: {projectId: projectId},
    //   include: [
    //     {model: CheckListItem}
    //   ],
    //   order: [
    //     ['id', 'ASC']
    //   ]
    // });
    // return jsonSuccess(listItemsOfProject);
  }

  static async listItemCheckOfProject({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    let listItemsCheck = await CheckList.findAll({
      where: { projectId },
      include: [{ model: CheckListItem }],
      order: [['id', 'ASC']]
    });
    return jsonSuccess(listItemsCheck);
  }

  static async removeItemCheck({ itemCheckId }) {
    let item = await CheckListItem.findByPk(itemCheckId);
    if (!item) return jsonError(errors.NOT_FOUND_ERROR);
    let listItemChecked = await CheckList.findAll({
      where: { checkItemId: itemCheckId, isChecked: true }
    });
    if (listItemChecked.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            CheckList.destroy({
              where: { checkItemId: itemCheckId, isChecked: false },
              transaction: t
            }),
            item.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // await CheckList.destroy({where: {checkItemId: itemCheckId, isChecked: false}});
    // await item.destroy();
    // return jsonSuccess();
  }

  static async addItemCheckToProject({ projectId, checkItemId }) {
    let item = await CheckListItem.findByPk(checkItemId);
    if (!item) return jsonError(errors.NOT_FOUND_ERROR);

    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let itemCheckOfProject = await CheckList.findOne({
      where: { projectId, checkItemId }
    });
    if (itemCheckOfProject) return jsonError(errors.DUPLICATED_ERROR);

    itemCheckOfProject = await CheckList.create({
      projectId: projectId,
      checkItemId: checkItemId,
      isChecked: false
    });
    itemCheckOfProject.dataValues.CheckListItem = item;
    return jsonSuccess(itemCheckOfProject);
  }

  static async removeItemCheckOfProject({ itemId }) {
    let itemCheckOfProject = await CheckList.findByPk(itemId);
    if (!itemCheckOfProject) return jsonError(errors.NOT_FOUND_ERROR);
    await itemCheckOfProject.destroy();
    return jsonSuccess();
  }
}

export { CheckListService };
