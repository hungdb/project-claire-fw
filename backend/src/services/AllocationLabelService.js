import { errors, jsonError, jsonSuccess } from '../utils/system';
import { AllocationLabel } from '../models/schema/AllocationLabel';
import { Allocation } from '../models/schema/Allocation';

class AllocationLabelService {
  static async boot() {
    // let check = await AllocationLabel.findAll();
    // if (!check.length) {
    //   let init = await AllocationLabel.bulkCreate([
    //     { name: 'Frontend' }, { name: 'Backend' }, { name: 'App' }, { name: 'iOS' }, { name: 'Android' },
    //     { name: 'Blockchain' }, { name: 'AI' }, { name: 'Comtor' }, { name: 'Tester'}, { name: 'Design'},
    //     { name: 'Network'}
    //   ]);
    //   if (!init)
    //     return jsonError();
    // }
    // return jsonSuccess();
  }

  static async createLabel({ name }) {
    let label = await AllocationLabel.findOne({ where: { name: name.trim() } });
    if (label) return jsonError(errors.DUPLICATED_ERROR);

    let newLabel = await AllocationLabel.create({ name: name.trim() });
    return jsonSuccess(newLabel);
  }

  static async getListAllocationLabels() {
    const allocationLabels = await AllocationLabel.findAll();
    return jsonSuccess(allocationLabels);
  }

  static async updateLabelAllocation({ labelId, name }) {
    let label = await AllocationLabel.findByPk(labelId);
    if (!label) return jsonError(errors.NOT_FOUND_ERROR);

    let query = {};
    if (name) query.name = name.trim();

    label = await label.update(query);
    return jsonSuccess(label);
  }

  static async deleteLabelAllocation({ labelId }) {
    let label = await AllocationLabel.findByPk(labelId);
    if (!label) return jsonError(errors.NOT_FOUND_ERROR);
    let check = await Allocation.findOne({ where: { label: labelId } });
    if (check) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    const remove = await label.destroy();
    if (!remove) return jsonError(errors.SYSTEM_ERROR);
    return jsonSuccess();
  }
}

export { AllocationLabelService };
