import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess } from '../utils/system';
import { Skill } from '../models/schema/Skill';
import { UserSkill, SKILL_TYPES } from '../models/schema/UserSkill';
import { getKPICoefficient } from '../utils/common';
import { User } from '../models/schema/User';
import { customDataAllocation, paginate } from '../utils/common';
import moment from 'moment';
import { Allocation } from '../models/schema/Allocation';
import { Sprint } from '../models/schema/Sprint';
import { Project } from '../models/schema/Project';
import { KPISkill } from '../models/schema/KPISkill';
import { KPI_TYPE } from '../models/schema/KPICoefficient';
import { UserRole } from '../models/schema/UserRole';
import { Role } from '../models/schema/Role';

class SkillService {
  static async boot() {
    SkillService.DATA = {
      MAXIMUM_POINT_OF_SKILL: +getEnv('MAXIMUM_POINT_OF_SKILL'),
      PAGINATE_LIMIT_DEFAULT: +getEnv('PAGINATE_LIMIT_DEFAULT')
    };
  }

  static async createSkill({ name, description, linkIcon }) {
    let skill = await Skill.findOne({ where: { name } });
    if (skill) return jsonError(errors.DUPLICATED_ERROR);
    skill = await Skill.create({ name, description, linkIcon });
    return jsonSuccess(skill);
  }

  static async updateSkill({ skillId, name, description, linkIcon }) {
    let skill = await Skill.findByPk(skillId);
    if (!skill) return jsonError(errors.NOT_FOUND_ERROR);
    let dataUpdate = {
      description: description ? description : null,
      linkIcon: linkIcon ? linkIcon : null
    };
    if (name) {
      let updatedSkill = await Skill.findOne({
        where: { name, id: { [Op.ne]: skillId } }
      });
      if (updatedSkill) return jsonError(errors.DUPLICATED_ERROR);
      dataUpdate.name = name;
    }

    await skill.update(dataUpdate);
    return jsonSuccess(skill);
  }

  static async removeSkill({ skillId }) {
    const skill = await Skill.findByPk(skillId);
    if (!skill) return jsonError(errors.NOT_FOUND_ERROR);

    const removeSkill = await Skill.destroy({ where: { id: skillId } });
    if (!removeSkill) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess(skill);
  }

  static async getSkill({ skillId }) {
    const skill = await Skill.findByPk(skillId);
    if (!skill) return jsonError(errors.NOT_FOUND_ERROR);

    return jsonSuccess(skill);
  }

  static async listSkills({ limit, page, isStandard }) {
    let result;
    let query = {};
    if (isStandard === true || isStandard === false) {
      query = { where: { isStandard } };
    }

    const include = [
      {
        model: UserSkill,
        as: 'userSkill',
        required: false,
        ...query
      }
    ];

    if (limit && page) {
      const data = await paginate({ model: Skill, query: { include, distinct: true }, page, limit });
      if (!data.success) {
        return data;
      }
      result = data.result;
    } else {
      let skills = await Skill.findAll({
        include,
        order: [['name', 'ASC']]
      });
      result = {
        data: skills,
        total: skills.length
      };
    }
    return jsonSuccess(result);
  }

  static async createUserSkill({ actor, userId, skillId, evaluationPoint, isStandard }) {
    const promises = await Promise.all([User.findByPk(userId), Skill.findByPk(skillId)]);
    if (!promises[0]) return jsonError(errors.USER_NOT_FOUND);
    if (!promises[1]) return jsonError(errors.SKILL_NOT_FOUND);

    if (isStandard) {
      const isExistSkillStandard = await UserSkill.findOne({
        where: { userId, isStandard }
      });
      if (isExistSkillStandard) return jsonError(errors.DUPLICATED_ERROR);
    }

    // if (evaluationPoint && (evaluationPoint > +getEnv('MAXIMUM_POINT_OF_SKILL') || evaluationPoint < 0 || isNaN(evaluationPoint)))
    //   return jsonError(errors.PARAM_INVALID);

    const userSkill = await UserSkill.create({
      actor,
      userId,
      skillId,
      evaluationPoint,
      isStandard,
      type: SKILL_TYPES.normal
    });
    const data = await UserSkill.findByPk(userSkill.id, {
      include: [{ model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] }]
    });
    return jsonSuccess(data);
  }

  static async updateUserSkill({ userSkillId, evaluationPoint, isStandard }) {
    const userSkill = await UserSkill.findByPk(userSkillId, {
      include: [{ model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] }]
    });
    if (!userSkill) return jsonError(errors.NOT_FOUND_ERROR);

    const data = {};
    // if (evaluationPoint) {
    // if (evaluationPoint && (evaluationPoint > +getEnv('MAXIMUM_POINT_OF_SKILL') || evaluationPoint < 0 || isNaN(evaluationPoint)))
    //   return jsonError(errors.PARAM_INVALID);
    data.evaluationPoint = evaluationPoint;
    // }

    if (isStandard) {
      const isExistSkillStandard = await UserSkill.findOne({
        where: {
          userId: userSkill.userId,
          isStandard,
          id: { [Op.ne]: userSkillId }
        }
      });
      if (isExistSkillStandard) return jsonError(errors.DUPLICATED_ERROR);
    }

    if (isStandard === true || isStandard === false) data.isStandard = isStandard;

    await userSkill.update(data);
    return jsonSuccess(userSkill);
  }

  static async removeUserSkill({ userSkillId }) {
    const userSkill = await UserSkill.findByPk(userSkillId);
    if (!userSkill) return jsonError(errors.NOT_FOUND_ERROR);

    const removeUserSkill = await UserSkill.destroy({
      where: { id: userSkillId }
    });
    if (!removeUserSkill) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess(userSkill);
  }

  static async getUserSkill({ userSkillId, type, actor }) {
    let query = { id: userSkillId };
    if (type === SKILL_TYPES.normal || type === SKILL_TYPES.project || type === SKILL_TYPES.yourSelf) {
      query.type = type;
    }
    if (actor) {
      const user = await User.findByPk(actor);
      if (!user) return jsonError(errors.NOT_FOUND_ERROR);
      query.actor = actor;
    }
    const userSkill = await UserSkill.findOne({
      where: query,
      include: [
        { model: User, as: 'user', attributes: ['id', 'name', 'avatar'] },
        { model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] }
      ]
    });
    if (!userSkill) return jsonError(errors.NOT_FOUND_ERROR);
    return jsonSuccess(userSkill);
  }

  static async listUserSkills({ userId, sprintId, projectId, type, limit, page }) {
    let query = {};
    if (userId) {
      const user = await User.findByPk(userId);
      if (!user) return jsonError(errors.USER_NOT_FOUND);
      query.userId = userId;
    }

    if (sprintId) {
      const sprint = await Sprint.findByPk(sprintId);
      if (!sprint) return jsonError(errors.SPRINT_NOT_FOUND);
      query.sprintId = sprintId;
    }

    if (projectId) {
      const project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.PROJECT_NOT_FOUND);
      query.projectId = projectId;
    }

    const skillTypesValid = Object.values(SKILL_TYPES);
    if (skillTypesValid.includes(type)) {
      query.type = type;
    }

    const include = [
      {
        model: User,
        as: 'user',
        attributes: ['id', 'name', 'avatar', 'isActive']
      },
      {
        model: User,
        as: 'actorAs',
        attributes: ['id', 'name', 'avatar', 'isActive']
      },
      { model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] },
      { model: Project, as: 'project', attributes: ['id', 'name'] },
      { model: Sprint, as: 'sprint', attributes: ['id', 'name'] }
    ];

    const data = await paginate({
      model: UserSkill,
      query: { where: query, include },
      page: page || 1,
      limit: limit || SkillService.DATA.PAGINATE_LIMIT_DEFAULT
    });
    if (!data.success) return data;
    return jsonSuccess(page || limit ? data.result : data.result.data);
  }

  static async listUsersBySkill({ skillId }) {
    const listUserSkillsBySkill = await UserSkill.findAll({
      where: { skillId },
      include: [{ model: User, as: 'user', attributes: ['id', 'name', 'avatar'] }]
    });
    const listUsersBySkill = listUserSkillsBySkill.map(item => {
      return {
        id: item.user.id,
        name: item.user.name,
        avatar: item.user.avatar
      };
    });
    return jsonSuccess(listUsersBySkill);
  }

  static async listSkillsByUserWithLastestEvaluation({ userId }) {
    const listUserSkillsByUser = await UserSkill.findAll({
      where: {
        userId,
        type: SKILL_TYPES.normal
      },
      include: [{ model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] }]
    });
    let result = [];
    let tmp = [];
    for (let i = 0; i < listUserSkillsByUser.length; i++) {
      if (!tmp.includes(listUserSkillsByUser[i].skill.id)) {
        tmp.push(listUserSkillsByUser[i].skill.id);
        result.push(listUserSkillsByUser[i]);
      } else {
        const itemUpdated = result.find(s => s.skill.id === listUserSkillsByUser[i].skill.id);
        if (moment(itemUpdated.createdAt) < moment(listUserSkillsByUser[i].createdAt)) {
          const updateIndex = result.findIndex(s => s.skill.id === listUserSkillsByUser[i].skill.id);
          if (updateIndex > -1) {
            result[updateIndex] = listUserSkillsByUser[i];
          }
        }
      }
    }
    return jsonSuccess(result);
  }

  static async listAllocationsOfUsers() {
    let result = [];
    const skills = await Skill.findAll({
      include: [
        {
          model: UserSkill,
          as: 'userSkill',
          where: { isStandard: true },
          include: [
            {
              model: User,
              as: 'user',
              where: { isActive: true },
              attributes: ['id', 'name', 'avatar'],
              include: [
                {
                  model: Allocation,
                  as: 'allocationsAs',
                  include: [{ model: Sprint, include: [{ model: Project }] }]
                }
              ]
            }
          ]
        }
      ]
    });

    for (let i = 0; i < skills.length; i++) {
      let skill = { skill: skills[i] };
      let listUsers = [];
      let users = skills[i].userSkill;
      if (users.length) {
        for (let j = 0; j < users.length; j++) {
          let user = users[j].user;
          if (!user) continue;
          user.dataValues.allocations = customDataAllocation(user.dataValues.allocationsAs);
          listUsers.push(user);
          if (j === users.length - 1) skill = Object.assign(skill, { users: listUsers });
        }
      } else {
        skill = Object.assign(skill, { users: listUsers });
      }
      result.push(skill);
    }
    return jsonSuccess(result);
  }

  static async getSkillOfAllUsers({ name, skillId, type }) {
    let queryUser = {
      id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') },
      isActive: true
    };
    let querySkill = {};
    let queryUserSkill = {};
    if (name) queryUser = Object.assign({}, { name: { [Op.like]: name } });
    if (skillId) querySkill = Object.assign({}, { skillId });
    const typeValid = Object.values(SKILL_TYPES);
    if (type && typeValid.includes(type)) queryUserSkill.type = type;
    const skillOfUsers = await User.findAll({
      where: queryUser,
      attributes: ['id', 'name', 'avatar'],
      include: [
        {
          model: UserSkill,
          as: 'userSkills',
          attributes: ['id', 'evaluationPoint', 'isStandard', 'type'],
          include: [
            {
              model: Skill,
              as: 'skill',
              attributes: ['id', 'name', 'linkIcon', 'createdAt'],
              where: querySkill
            }
          ],
          where: queryUserSkill,
          required: false
        },
        {
          model: UserRole,
          as: 'roles',
          include: [
            {
              model: Role,
              where: { name: { [Op.ne]: getEnv('CUSTOMER_ROLE') } }
            }
          ],
          required: true
        }
      ]
    });
    return jsonSuccess(skillOfUsers);
  }

  static async addUserSkillInSprint({ actor, userId, skillId, evaluationPoint, sprintId, type, note }) {
    if (type !== SKILL_TYPES.project && type !== SKILL_TYPES.yourSelf) {
      return jsonError(errors.SKILL_TYPE_IS_INVALID);
    }
    // if (evaluationPoint && evaluationPoint > +getEnv('MAXIMUM_POINT_OF_SKILL')) {
    //   return jsonError(errors.PARAM_INVALID);
    // }

    const promises = await Promise.all([User.findByPk(userId), Skill.findByPk(skillId), Sprint.findByPk(sprintId)]);

    if (!promises[0]) return jsonError(errors.USER_NOT_FOUND);
    if (!promises[1]) return jsonError(errors.SKILL_NOT_FOUND);
    if (!promises[2]) return jsonError(errors.SPRINT_NOT_FOUND);

    let userSkill = await UserSkill.findOne({
      where: { actor, userId, skillId, sprintId, type }
    });
    if (userSkill) {
      // update
      const dataUpdate = { evaluationPoint };
      // if (evaluationPoint)
      //   dataUpdate.evaluationPoint = evaluationPoint;
      if (note) dataUpdate.note = note;
      await userSkill.update(dataUpdate);
    } else {
      // create new
      userSkill = await UserSkill.create({
        actor,
        userId,
        skillId,
        evaluationPoint,
        projectId: promises[2].projectId,
        sprintId,
        type,
        note
      });
    }

    const data = await UserSkill.findByPk(userSkill.id, {
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        {
          model: User,
          as: 'actorAs',
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        { model: Skill, as: 'skill', attributes: ['id', 'name', 'description'] }
      ]
    });
    return jsonSuccess(data);
  }

  static async createKpiSkill({ actor, userId, reviewPoint, testPoint, evaluatedAt }) {
    const user = await User.findByPk(userId);
    if (!user) {
      return jsonError(errors.USER_NOT_FOUND);
    }
    let dataSkillServiceDATA = await getKPICoefficient(KPI_TYPE.KPI_Skill);
    if (!dataSkillServiceDATA) return jsonError(errors.SYSTEM_ERROR);

    // if (reviewPoint && (reviewPoint < 0 || reviewPoint > SkillService.DATA.MAXIMUM_POINT_OF_SKILL)) {
    //   return jsonError(errors.PARAM_INVALID);
    // }
    //
    // if (testPoint && (testPoint < 0 || testPoint > SkillService.DATA.MAXIMUM_POINT_OF_SKILL)) {
    //   return jsonError(errors.PARAM_INVALID);
    // }

    const point =
      dataSkillServiceDATA.percentSkillReview * reviewPoint + dataSkillServiceDATA.percentSkillTest * testPoint;

    const kpiSkill = await KPISkill.create({
      actor,
      userId,
      point: +point.toFixed(5),
      reviewPoint,
      testPoint,
      evaluatedAt
    });
    return jsonSuccess(kpiSkill);
  }

  static async getKpiSkillById({ kpiSkillId }) {
    const kpiSkill = await KPISkill.findByPk(kpiSkillId);
    return kpiSkill ? jsonSuccess(kpiSkill) : jsonError(errors.NOT_FOUND_ERROR);
  }

  static async removeKpiSkillById({ kpiSkillId }) {
    const kpiSkill = await KPISkill.findByPk(kpiSkillId);

    if (!kpiSkill) {
      return jsonError(errors.NOT_FOUND_ERROR);
    }

    const isRemovedKpiSkill = await KPISkill.destroy({
      where: { id: kpiSkillId }
    });
    return isRemovedKpiSkill ? jsonSuccess(isRemovedKpiSkill) : jsonError(errors.SYSTEM_ERROR);
  }

  static async getListKpiSkill({ userId, page, limit }) {
    const query = {};
    if (userId) {
      const user = await User.findByPk(userId);
      if (!user) return jsonError(errors.USER_NOT_FOUND);
      query.userId = userId;
    }

    const include = [
      { model: User, as: 'user', attributes: ['id', 'name', 'avatar'] },
      { model: User, as: 'infoActor', attributes: ['id', 'name', 'avatar'] }
    ];
    if (limit && page) {
      const data = await paginate({
        model: KPISkill,
        query: { where: query, include },
        page,
        limit
      });
      return data.success ? jsonSuccess(data.result) : data;
    }

    const kpiSkills = await KPISkill.findAll({
      where: query,
      include
    });
    return jsonSuccess({
      data: kpiSkills,
      total: kpiSkills.length
    });
  }
}

export { SkillService };
