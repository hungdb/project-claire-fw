import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Project } from '../models/schema/Project';
import { Workflow } from '../models/schema/Workflow';
import { Task } from '../models/schema/Task';
import { WorkflowDetail } from '../models/schema/WorkflowDetail';
import { EFFORT_TYPES, WorkflowState } from '../models/schema/WorkflowState';
import { TaskState } from '../models/schema/TaskState';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { User } from '../models/schema/User';
import { getRelateUsersOfTask, getRelateUsersOfTaskTerminated } from '../utils/common';
import { sequelize } from '../core/boot';

class WorkflowService {
  static async boot() {}

  static async createWorkflow({ name, allowParallelTask }) {
    let workflow = await Workflow.findOne({ where: { name } });
    if (workflow) return jsonError(errors.DUPLICATED_ERROR);
    workflow = await Workflow.create({ name, allowParallelTask });
    return jsonSuccess(workflow);
  }

  static async updateWorkflow({ workflowId, name, allowParallelTask }) {
    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);
    let updateWorkflow = await Workflow.findOne({
      where: {
        name,
        id: { [Op.ne]: workflowId }
      }
    });
    if (updateWorkflow) return jsonError(errors.DUPLICATED_ERROR);
    await workflow.update({ name, allowParallelTask });
    return jsonSuccess();
  }

  static async deleteWorkflow({ workflowId }) {
    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);
    let projects = await Project.findAll({
      where: { workflowSchemeId: workflowId }
    });
    if (projects.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);
    // await WorkflowDetail.destroy({ where: { workflowId } });
    // await workflow.destroy();
    // return jsonSuccess();
    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            WorkflowDetail.destroy({
              where: { workflowId: workflowId },
              transaction: t
            }),
            WorkflowState.destroy({
              where: { workflowId: workflowId },
              transaction: t
            }),
            workflow.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async createWorkflowState({ name, effortType, workflowId, ordinalNumber }) {
    // let array = Object.values(EFFORT_TYPES);
    // if (!array.includes(effortType))
    //   return jsonError(errors.PARAM_INVALID);

    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);

    let checkName = await WorkflowState.findOne({
      where: { name, workflowId }
    });

    let checkOrdinalNumber = await WorkflowState.findOne({
      where: { workflowId, ordinalNumber }
    });

    let checkEffortType;
    if (effortType === EFFORT_TYPES.UNTOUCHED || effortType === EFFORT_TYPES.TERMINATED) {
      checkEffortType = await WorkflowState.findOne({
        where: { workflowId, effortType }
      });
    }

    if (checkName || checkOrdinalNumber || checkEffortType) return jsonError(errors.DUPLICATED_ERROR);

    let workflowState = await WorkflowState.create({
      name: name,
      effortType: effortType,
      workflowId: workflowId,
      ordinalNumber: ordinalNumber
    });
    return jsonSuccess(workflowState);
  }

  static async listWorkflow() {
    let workflows = await Workflow.findAll();
    return jsonSuccess(workflows);
  }

  static async updateWorkflowState({ workflowStateId, name, effortType, ordinalNumber }) {
    // if (effortType) {
    //   let array = Object.values(EFFORT_TYPES);
    //   if(!array.includes(effortType))
    //     return jsonError(errors.PARAM_INVALID);
    // }
    let checkName, checkOrdinalNumber, checkEffortType;

    let workflowState = await WorkflowState.findByPk(workflowStateId);
    if (!workflowStateId) return jsonError(errors.NOT_FOUND_ERROR);

    if (name)
      checkName = await WorkflowState.findOne({
        where: {
          name,
          workflowId: workflowState.workflowId,
          id: { [Op.ne]: workflowStateId }
        }
      });

    if (ordinalNumber)
      checkOrdinalNumber = await WorkflowState.findOne({
        where: {
          workflowId: workflowState.workflowId,
          ordinalNumber,
          id: { [Op.ne]: workflowStateId }
        }
      });

    if (effortType && (effortType === EFFORT_TYPES.UNTOUCHED || effortType === EFFORT_TYPES.TERMINATED)) {
      checkEffortType = await WorkflowState.findOne({
        where: {
          workflowId: workflowState.workflowId,
          effortType,
          id: { [Op.ne]: workflowStateId }
        }
      });
    }
    if (checkName || checkOrdinalNumber || checkEffortType) return jsonError(errors.DUPLICATED_ERROR);

    workflowState = await workflowState.update({
      name: name ? name : workflowState.name,
      effortType: effortType ? effortType : workflowState.effortType,
      ordinalNumber: ordinalNumber ? ordinalNumber : workflowState.ordinalNumber
    });
    return jsonSuccess(workflowState);
  }

  static async createWorkflowDetail({ workflowId, fromState, toState, description }) {
    if (fromState === toState) return jsonError(errors.NOT_CHANGE_STATE_ERROR);

    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);

    let allowStates = [];
    const workflowStates = await WorkflowState.findAll({
      where: { workflowId }
    });
    workflowStates.map(state => allowStates.push(state.id));

    if (!allowStates.includes(fromState) || !allowStates.includes(toState)) return jsonError(errors.PARAM_INVALID);

    let workflowDetail = await WorkflowDetail.findOne({
      where: { workflowId, fromState, toState }
    });
    if (workflowDetail) return jsonError(errors.DUPLICATED_ERROR);

    workflowDetail = await WorkflowDetail.create({
      workflowId,
      fromState,
      toState,
      description
    });
    const result = await WorkflowDetail.findByPk(workflowDetail.id, {
      attributes: ['id', 'description'],
      include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
    });
    return jsonSuccess(result);
  }

  static async getWorkflow({ workflowId }) {
    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);

    workflow.dataValues.detail = await WorkflowDetail.findAll({
      where: { workflowId },
      attributes: ['id', 'description'],
      include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
    });
    return jsonSuccess(workflow);
  }

  static async deleteWorkflowState({ workflowStateId }) {
    let workflowState = await WorkflowState.findByPk(workflowStateId);
    if (!workflowState) return jsonError(errors.NOT_FOUND_ERROR);

    let taskStates = await TaskState.findAll({
      where: {
        [Op.or]: [{ fromState: workflowStateId }, { toState: workflowStateId }]
      }
    });
    let workflowDetails = await WorkflowDetail.findAll({
      where: {
        [Op.or]: [{ fromState: workflowStateId }, { toState: workflowStateId }]
      }
    });
    if (taskStates.length > 0 || workflowDetails.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    await workflowState.destroy();
    return jsonSuccess();
  }

  static async listWorkflowStateByWorkflowId({ workflowId }) {
    let workflowStates = await WorkflowState.findAll({
      where: { workflowId }
    });
    return jsonSuccess(workflowStates);
  }

  static async listWorkflowStateByProjectId({ projectId, type, userId, assignee }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let workflowStates = await WorkflowState.findAll({
      where: { workflowId: project.workflowSchemeId },
      order: [['ordinalNumber', 'ASC']]
    });

    const sprint = await Sprint.findOne({
      where: { projectId, status: STATUS.ACTIVE }
    });
    if (workflowStates && sprint) {
      for (let i = 0; i < workflowStates.length; i += 1) {
        const workflowState = workflowStates[i];
        let conditions = { state: workflowState.id, sprintId: sprint.id };
        // my task
        if (type && type === 2) {
          conditions = Object.assign(conditions, { assignee: userId });
        } else if (assignee) {
          // by user
          conditions = Object.assign(conditions, { assignee });
        }

        let tasks =
          (await Task.findAll({
            where: conditions,
            include: [
              { model: WorkflowState },
              {
                model: User,
                as: 'assigneeAs',
                attributes: ['id', 'name', 'avatar']
              }
            ]
          })) || [];
        if (workflowState.effortType === EFFORT_TYPES.HOLDING || workflowState.effortType === EFFORT_TYPES.PROGRESS) {
          for (let n = 0; n < tasks.length; n += 1) {
            const taskId = tasks[n].id;
            let relateUsers = await getRelateUsersOfTask(taskId);
            tasks[n].dataValues.relateUsers = relateUsers.users;
          }
        }
        if (workflowState.effortType === EFFORT_TYPES.TERMINATED) {
          for (let m = 0; m < tasks.length; m += 1) {
            const taskId = tasks[m].id;
            tasks[m].dataValues.relateUsers = await getRelateUsersOfTaskTerminated(taskId);
          }
        }
        workflowState.dataValues.tasks = tasks;
      }
    }
    return jsonSuccess(workflowStates);
  }

  static async updateWorkflowDetail({ workflowDetailId, fromState, toState, description }) {
    let workflowDetail = await WorkflowDetail.findByPk(workflowDetailId);
    if (!workflowDetail) return jsonError(errors.NOT_FOUND_ERROR);

    let stateFrom = fromState ? fromState : workflowDetail.fromState;
    let stateTo = toState ? toState : workflowDetail.toState;
    if (stateFrom === stateTo) return jsonError(errors.NOT_CHANGE_STATE_ERROR);

    let allowStates = [];
    const workflowStates = await WorkflowState.findAll({
      where: { workflowId: workflowDetail.workflowId }
    });
    workflowStates.map(state => allowStates.push(state.id));
    if (!allowStates.includes(stateFrom) || !allowStates.includes(stateTo)) return jsonError(errors.PARAM_INVALID);

    let updatedWorkflowDetail = await WorkflowDetail.findOne({
      where: {
        workflowId: workflowDetail.workflowId,
        fromState: stateFrom,
        toState: stateTo,
        id: { [Op.ne]: workflowDetailId }
      }
    });
    if (updatedWorkflowDetail) return jsonError(errors.DUPLICATED_ERROR);

    updatedWorkflowDetail = await workflowDetail.update({
      fromState: stateFrom,
      toState: stateTo,
      description
    });
    const result = await WorkflowDetail.findByPk(workflowDetailId, {
      attributes: ['id', 'description'],
      include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
    });
    return jsonSuccess(result);
  }

  static async deleteWorkflowDetail({ workflowDetailId }) {
    let workflowDetail = await WorkflowDetail.findByPk(workflowDetailId);
    if (!workflowDetail) return jsonError(errors.NOT_FOUND_ERROR);
    await workflowDetail.destroy();
    return jsonSuccess();
  }
}

export { WorkflowService };
