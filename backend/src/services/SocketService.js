import { UserProjectRole } from '../models/schema/UserProjectRole';
import { Notification, NOTIFICATION_TYPE } from '../models/schema/Notification';
import { Task } from '../models/schema/Task';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { TaskState } from '../models/schema/TaskState';
import { Project } from '../models/schema/Project';
import { Comment, COMMENT_TYPE } from '../models/schema/Comment';
import { Issue } from '../models/schema/Issue';
import { Op } from 'sequelize';
import _ from 'lodash';
import { parseJSONIsArray } from '../utils/common';

class SocketService {
  static async boot() {
    global.global.io.on('connection', async function(socket) {
      let room = `room_user_${socket.decoded_token.owner}`;
      socket.join(room);
      // init room_project
      let projects = await UserProjectRole.findAll({
        where: {
          userId: socket.decoded_token.owner,
          deletedAt: { [Op.eq]: null }
        },
        attributes: ['projectId'],
        raw: true
      });
      for (let i = 0; i < projects.length; i++) {
        socket.join(`room_project_${projects[i].projectId}`);
      }

      //init room_task
      let tasks = await Task.findAll({
        include: [{ model: Sprint, where: { status: STATUS.ACTIVE } }]
      });

      let currentTasks = tasks.filter(
        task => task.creator === socket.decoded_token.owner || task.assignee === socket.decoded_token.owner
      );
      for (let j = 0; j < currentTasks.length; j++) {
        socket.join(`room_task_${currentTasks[j].id}`);
      }

      let taskDrag = tasks.filter(
        task => !(task.creator === socket.decoded_token.owner || task.assignee === socket.decoded_token.owner)
      );
      let listTaskComment = [];
      for (let i = 0; i < taskDrag.length; i++) {
        let taskStates = await TaskState.findAll({
          where: {
            taskId: taskDrag[i].id,
            editor: socket.decoded_token.owner
          }
        });
        if (taskStates.length) {
          socket.join(`room_task_${taskDrag[i].id}`);
        } else {
          await listTaskComment.push(taskDrag[i].id);
        }
      }
      let myTasksComment = await Comment.findAll({
        where: {
          userId: socket.decoded_token.owner,
          commentTableId: { [Op.in]: listTaskComment },
          commentTableType: COMMENT_TYPE.TASK
        }
      });
      for (let i = 0; i < myTasksComment.length; i++) {
        socket.join(`room_task_${myTasksComment[i].commentTableId}`);
      }

      //get all rooms of specific user
      // let allRooms = [];
      // global.io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
      //   if (err) { /* unknown id */ }
      //   allRooms = [...rooms]
      // });

      //init room_issue
      let issues = await Issue.findAll();
      for (let i = 0; i < issues.length; i++) {
        if (issues[i].userRelate) {
          const userRelate = parseJSONIsArray(issues[i].userRelate);
          if (userRelate.includes(socket.decoded_token.owner)) {
            socket.join(`room_issue_${issues[i].id}`);
          }
        }
      }
    });
  }

  static joinRoom({ userId, notificationableId, notificationableType }) {
    let room;
    if (notificationableType === NOTIFICATION_TYPE.PROJECT) {
      room = `room_project_${notificationableId}`;
    } else if (notificationableType === NOTIFICATION_TYPE.TASK) {
      room = `room_task_${notificationableId}`;
    }
    global.io.in(`room_user_${userId}`).clients((err, clients) => {
      global.io.of('/').adapter.remoteJoin(clients[0], room, err => {});
    });
  }

  static leaveRoom({ userId, notificationableId, notificationableType }) {
    let room;
    if (notificationableType === NOTIFICATION_TYPE.PROJECT) {
      room = `room_project_${notificationableId}`;
    } else if (notificationableType === NOTIFICATION_TYPE.TASK) {
      room = `room_task_${notificationableId}`;
    }
    global.io.in(`room_user_${userId}`).clients((err, clients) => {
      global.io.of('/').adapter.remoteLeave(clients[0], room, err => {});
    });
  }

  static notificationToRoom({ userId, notificationableId, notificationableType, type, data }) {
    let room;
    if (notificationableType === NOTIFICATION_TYPE.PROJECT) {
      room = `room_project_${notificationableId}`;
    } else if (notificationableType === NOTIFICATION_TYPE.TASK) {
      room = `room_task_${notificationableId}`;
    } else if (notificationableType === NOTIFICATION_TYPE.ISSUE) {
      room = `room_issue_${notificationableId}`;
    }
    global.io.in(`room_user_${userId}`).clients((err, clients) => {
      // TODO what the heck is clients[0]?
      if (clients && clients[0]) {
        global.io.sockets.connected[clients[0]].broadcast.to(room).emit('notification', { type, data });
      }
    });
  }

  static notificationToSpecificUser({ userId, type, data }) {
    let room = `room_user_${userId}`;
    global.io.to(room).emit('notification', { type, data });
  }

  static reloadBoard({ userId, notificationableId, notificationableType }) {
    let room;
    if (notificationableType === NOTIFICATION_TYPE.PROJECT) {
      room = `room_project_${notificationableId}`;
    } else if (notificationableType === NOTIFICATION_TYPE.TASK) {
      room = `room_task_${notificationableId}`;
    }
    global.io.in(`room_user_${userId}`).clients((err, clients) => {
      // TODO what the heck is clients[0]?
      if (clients && clients[0]) {
        global.io.sockets.connected[clients[0]].broadcast.to(room).emit('reload_board');
      }
    });
  }

  static async storeNotificationToUserOfRoomProject({ userId, projectId, type, content, data }) {
    let usersOfRoomProject = await UserProjectRole.findAll({
      where: {
        projectId,
        deletedAt: { [Op.eq]: null }
      },
      attributes: ['userId']
    });
    usersOfRoomProject = usersOfRoomProject.map(user => {
      return user.userId;
    });
    let x = usersOfRoomProject => usersOfRoomProject.filter((v, i) => usersOfRoomProject.indexOf(v) === i);
    let users = x(usersOfRoomProject).filter(user => user !== userId);
    let arrayNotifications = users.map(user => {
      return {
        data: JSON.stringify(data),
        actor: userId,
        userId: user,
        notificationableId: projectId,
        notificationableType: NOTIFICATION_TYPE.PROJECT,
        type: type,
        content: content
      };
    });
    await Notification.bulkCreate(arrayNotifications);
  }

  static async storeNotificationToUserOfRoomTask({ userId, taskId, type, content, data }) {
    let task = await Task.findOne({
      where: { id: taskId },
      include: [{ model: Project }, { model: Sprint }]
    });
    let usersOfRoomTask = [];
    usersOfRoomTask = [...usersOfRoomTask, task.creator];
    usersOfRoomTask = usersOfRoomTask.includes(task.assignee) ? usersOfRoomTask : [...usersOfRoomTask, task.assignee];
    let taskStates = await TaskState.findAll({
      where: {
        taskId: task.id
      },
      attributes: ['editor'],
      raw: true
    });
    let editors = taskStates.map(taskState => {
      return taskState.editor;
    });
    usersOfRoomTask = [...usersOfRoomTask, ...editors];

    let usersComment = await Comment.findAll({
      where: { commentTableId: taskId, commentTableType: COMMENT_TYPE.TASK },
      attributes: ['userId']
    });
    usersComment = usersComment.map(user => {
      return user.userId;
    });
    usersOfRoomTask = [...usersOfRoomTask, ...usersComment];
    let x = usersOfRoomTask => usersOfRoomTask.filter((v, i) => usersOfRoomTask.indexOf(v) === i);
    let users = x(usersOfRoomTask).filter(user => user !== userId);
    let arrayNotifications = users.map(user => {
      return {
        data: JSON.stringify(data),
        actor: userId,
        userId: user,
        notificationableId: taskId,
        notificationableType: NOTIFICATION_TYPE.TASK,
        type: type,
        content: content
      };
    });
    await Notification.bulkCreate(arrayNotifications);
  }

  static async storeNotificationToUserOfRoomIssue({ userId, issueId, userRelate, type, content, data }) {
    let usersOfRoomIssue = Array.isArray(userRelate) ? [...userRelate] : [];

    let usersComment = await Comment.findAll({
      where: { commentTableId: issueId, commentTableType: COMMENT_TYPE.ISSUE },
      attributes: ['userId']
    });
    usersComment = usersComment.map(user => {
      return user.userId;
    });
    usersOfRoomIssue = [...usersOfRoomIssue, ...usersComment];

    let users = _.uniq(usersOfRoomIssue);
    users = users.filter(user => user !== userId);
    let arrayNotifications = users.map(user => {
      return {
        data: JSON.stringify(data),
        actor: userId,
        userId: user,
        notificationableId: issueId,
        notificationableType: NOTIFICATION_TYPE.ISSUE,
        type: type,
        content: content
      };
    });
    await Notification.bulkCreate(arrayNotifications);
  }
}

export { SocketService };
