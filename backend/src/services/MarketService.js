import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess } from '../utils/system';
import { Market } from '../models/schema/Market';
import { Customer } from '../models/schema/Customer';
class MarketService {
  static async createMarket({ name }) {
    const isMarketExist = await Market.findOne({ where: { name } });
    if (isMarketExist) return jsonError(errors.DUPLICATED_ERROR);

    const marketNew = await Market.create({ name });

    return jsonSuccess(marketNew);
  }

  static async updateMarket({ id, name }) {
    let market = await Market.findById(id);
    if (!market) return jsonError(errors.NOT_FOUND_ERROR);

    const isMarketExist = await Market.findOne({
      where: {
        name,
        id: {
          [Op.ne]: id
        }
      }
    });

    if (isMarketExist) return jsonError(errors.DUPLICATED_ERROR);

    const dataUpdate = {
      name: name ? name : market.name,
      updatedAt: new Date()
    };

    market = await market.update(dataUpdate);

    return jsonSuccess(market);
  }

  static async removeMarket({ id }) {
    let market = await Market.findById(id);
    if (!market) return jsonError(errors.NOT_FOUND_ERROR);

    const isCustomerInMarket = await Customer.findOne({
      where: {
        marketId: id
      }
    });

    if (isCustomerInMarket) return jsonError(errors.ERROR_RESOURCE_DEPENDENCY);

    market = await market.destroy();

    return jsonSuccess();
  }

  static async getListMarket() {
    const listMarket = await Market.findAll();
    return jsonSuccess(listMarket);
  }
}

export { MarketService };
