import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import { Op } from 'sequelize';
import { rpcActions } from '../controllers/RPCControler';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { TYPE_LIST_USER } from '../utils/type';
import { Role } from '../models/schema/Role';
import { RolePermission } from '../models/schema/RolePermission';
import { UserRole } from '../models/schema/UserRole';
import { User } from '../models/schema/User';
import { getPermissionsForSpecificUser } from '../utils/common';
import { UserNote } from '../models/schema/UserNote';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { Task } from '../models/schema/Task';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { TaskState } from '../models/schema/TaskState';
import { Comment, COMMENT_TYPE } from '../models/schema/Comment';
import { sequelize } from '../core/boot';

class UserService {
  static async boot() {
    //-- init DATA
    UserService.DATA = {
      SUPER_ADMIN_ROLE: getEnv('SUPER_ADMIN_ROLE'),
      UNAUTHENTICATED_ROLE: getEnv('UNAUTHENTICATED_ROLE'),
      APP_ROLE: getEnv('APP_ROLE'),

      IDENTITY_URL: getEnv('IDENTITY_URL'),
      OAUTH_APP_ID: getEnv('OAUTH_APP_ID'),
      OAUTH_APP_KEY: getEnv('OAUTH_APP_KEY'),
      SUPER_ADMIN_ID: parseInt(getEnv('SUPER_ADMIN_ID')),
      IDENTITY_LOGIN_TOKEN: null,
      IDENTITY_ACCESS_TOKEN: null
    };

    //-- login and request access token for list users
    logger.verbose('Logging in to identity...');
    let reloadResult = await UserService.reloadIdentityLoginToken();
    if (!reloadResult.success) {
      return reloadResult;
    }

    //-- reload access token
    logger.verbose('Fetching access token...');
    reloadResult = await UserService.reloadIdentityAccessToken();
    if (!reloadResult.success) {
      return reloadResult;
    }

    //-- create default roles
    let superAdminRole = await Role.findOne({
      where: { name: UserService.DATA.SUPER_ADMIN_ROLE }
    });
    if (!superAdminRole) {
      superAdminRole = await Role.create({
        name: UserService.DATA.SUPER_ADMIN_ROLE,
        isSuperUser: true,
        isEditable: false,
        isSystemRole: false
      });
    }

    let unauthenticatedRole = await Role.findOne({
      where: { name: UserService.DATA.UNAUTHENTICATED_ROLE }
    });
    if (!unauthenticatedRole) {
      unauthenticatedRole = await Role.create({
        name: UserService.DATA.UNAUTHENTICATED_ROLE,
        editable: false,
        isSystemRole: true
      });
    }

    let appRole = await Role.findOne({
      where: { name: UserService.DATA.APP_ROLE }
    });
    if (!appRole) {
      await Role.create({
        name: UserService.DATA.APP_ROLE,
        editable: true,
        isSystemRole: true
      });
    }

    //-- default role permissions
    let adminPermissions = Object.keys(rpcActions).map(permissionName => ({
      roleId: superAdminRole.id,
      permission: permissionName
    }));

    let unauthenticatedPermissions = ['login', 'get_access_token', 'get_my_role_and_permission', 'logout'].map(
      permissionName => ({
        roleId: unauthenticatedRole.id,
        permission: permissionName
      })
    );

    //-- wipe out existing admin and unauthenticated permissions
    await RolePermission.destroy({ where: { roleId: superAdminRole.id } });
    await RolePermission.destroy({ where: { roleId: unauthenticatedRole.id } });
    await RolePermission.bulkCreate([...adminPermissions, ...unauthenticatedPermissions]);

    return jsonSuccess();
  }

  static async reloadIdentityLoginToken() {
    let loginTokenResult;
    try {
      loginTokenResult = await axios.post(UserService.DATA.IDENTITY_URL, {
        action: 'get_oauth_token',
        grantType: 'api_key',
        grantData: [UserService.DATA.OAUTH_APP_ID, UserService.DATA.OAUTH_APP_KEY]
      });

      if (!loginTokenResult.data['success']) {
        logger.error('Fail to authenticate to Identity server');
        return jsonError(errors.SYSTEM_ERROR);
      }

      UserService.DATA.IDENTITY_LOGIN_TOKEN = loginTokenResult.data['result']['token'];
      return jsonSuccess();
    } catch (err) {
      logger.error('Cannot connect to Identity API server');
      return jsonError(errors.SYSTEM_ERROR);
    }
  }

  static async reloadIdentityAccessToken(retry = true) {
    let accessTokenResult = await axios.post(UserService.DATA.IDENTITY_URL, {
      action: 'get_access_token',
      loginToken: UserService.DATA.IDENTITY_LOGIN_TOKEN
    });

    if (!accessTokenResult.data['success']) {
      if (retry) {
        let result = await this.reloadIdentityLoginToken();
        if (!result.success) {
          return result;
        }
        return await this.reloadIdentityAccessToken(false);
      }
      logger.error('Fail to reload login token');
      return jsonError(errors.SYSTEM_ERROR);
    } else {
      UserService.DATA.IDENTITY_ACCESS_TOKEN = accessTokenResult.data['result']['token'];
      return jsonSuccess();
    }
  }

  static async getUserById({ userId }) {
    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);
    return jsonSuccess(user);
  }

  static async getRole({ roleId }) {
    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    role['dataValues']['permissions'] = await RolePermission.findAll({
      where: { roleId },
      attributes: ['id', 'permission']
    });
    return jsonSuccess(role);
  }

  static async listRoles() {
    let roles = await Role.findAll({});
    return jsonSuccess(roles);
  }

  static async addRole({ name, isSuperUser, isEditable, isSystemRole }) {
    let role = await Role.findOne({ where: { name } });
    if (role) return jsonError(errors.DUPLICATED_ERROR);

    role = await Role.create({
      name,
      isSuperUser,
      isEditable,
      isSystemRole
    });
    return jsonSuccess(role);
  }

  static async updateRole({ roleId, name }) {
    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    if (role.isSystemRole || role.isSuperUser) return jsonError(errors.READ_ONLY_VALUE);

    let updateRole = await Role.findOne({
      where: {
        name,
        id: { [Op.ne]: roleId }
      }
    });
    if (updateRole) return jsonError(errors.DUPLICATED_ERROR);

    role = await role.update({ name });
    return jsonSuccess(role);
  }

  static async deleteRole({ roleId }) {
    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    if (role.isSystemRole || role.isSuperUser) return jsonError(errors.READ_ONLY_VALUE);

    // await RolePermission.destroy({where: {roleId}});
    // await UserRole.destroy({where: {roleId}});
    // await role.destroy();
    // return jsonSuccess();

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            RolePermission.destroy({ where: { roleId }, transaction: t }),
            UserRole.destroy({ where: { roleId }, transaction: t }),
            role.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async listAllRolePermission() {
    let rolePermissions = await RolePermission.findAll({
      include: [{ model: Role }]
    });
    return jsonSuccess(rolePermissions);
  }

  static async listPermissionsOfRole({ roleId }) {
    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);
    role['dataValues']['permissions'] = await RolePermission.findAll({
      where: { roleId }
    });
    return jsonSuccess(role);
  }

  static async addRolePermission({ roleId, permission, permissionConditions }) {
    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    if (!role.isEditable) return jsonError(errors.READ_ONLY_VALUE);

    const data = {
      roleId,
      permission
    };

    if (permissionConditions) data.permissionConditions = permissionConditions;

    let rolePermission = await RolePermission.create(data);
    return jsonSuccess(rolePermission);
  }

  static async deleteRolePermission({ rolePermissionId }) {
    let rolePermission = await RolePermission.findByPk(rolePermissionId);
    if (!rolePermission) return jsonError(errors.NOT_FOUND_ERROR);

    let role = await Role.findByPk(rolePermission.roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    if (!role.isEditable) return jsonError(errors.READ_ONLY_VALUE);

    await rolePermission.destroy();
    return jsonSuccess();
  }

  static async getUserInfo({ userId }) {
    let user = await User.findByPk(userId, {
      attributes: ['id', 'name', 'avatar']
    });
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);

    return jsonSuccess(user);
  }

  static async getUsers({ limit = 10, offset = 0, username, sort, isActive, ids }) {
    let doGet = async () => {
      return await axios.post(
        UserService.DATA.IDENTITY_URL,
        {
          action: 'list_users',
          username,
          isActive,
          ids,
          info: [
            'id',
            'firstname',
            'lastname',
            'username',
            'profilePhoto',
            'email',
            'isActive',
            'phoneNumber',
            'branchId'
          ]
        },
        { headers: { Authorization: UserService.DATA.IDENTITY_ACCESS_TOKEN } }
      );
    };

    let result = await doGet();
    if (!result || !result.data['success']) {
      //-- token may have expired, try again
      await UserService.reloadIdentityAccessToken();
      result = await doGet();
    }

    if (!result || !result.data['success']) {
      return result.data;
    }

    let users = result.data.result;
    users = _.orderBy(users, ['username'], sort);
    // users = users.filter(user => user.id !== +getEnv('SUPER_ADMIN_ID'));
    for (let i = 0; i < users.length; i++) {
      users[i].roles =
        (await UserRole.findAll({
          where: { userId: users[i].id },
          include: [{ model: Role, attributes: ['id', 'name'] }]
        })) || [];
      const user = await User.findOne({
        where: { id: users[i].id },
        attributes: ['id', 'name', 'avatar', 'mentor', 'tokenFirebase'],
        include: [
          {
            model: User,
            as: 'infoMentor',
            attributes: ['id', 'name', 'avatar', 'mentor']
          }
        ]
      });
      users[i].infoMentor = (user && user.infoMentor) || null;
    }
    // handle pagination
    // users = users.slice(offset, limit + offset);
    return jsonSuccess(users);
  }

  static async getCurrentUsers({ limit, offset, isActive, type }) {
    let query = {};
    if (isActive === true || isActive === false) query = Object.assign(query, { isActive });
    if (type === TYPE_LIST_USER.DEFAULT)
      query = Object.assign(query, {
        id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') }
      });
    const result = await User.findAndCountAll({
      where: query,
      attributes: ['id', 'name', 'avatar', 'mentor', 'isActive'],
      include: [
        { model: User, as: 'infoMentor', attributes: ['id', 'name', 'avatar'] },
        { model: UserRole, as: 'roles' }
      ],
      limit,
      offset
    });
    return jsonSuccess({
      users: result.rows,
      total: result.count
    });
  }

  static async login({ code, callbackUrl }) {
    //-- get login token
    let tokenResult = await axios.post(UserService.DATA.IDENTITY_URL, {
      action: 'get_oauth_token',
      grantType: 'code',
      grantData: [code, UserService.DATA.OAUTH_APP_ID, UserService.DATA.OAUTH_APP_KEY, callbackUrl]
    });

    if (!tokenResult.data['success']) return jsonError(errors.INVALID_TOKEN);

    //-- get access token
    let accessTokenResult = await axios.post(UserService.DATA.IDENTITY_URL, {
      action: 'get_access_token',
      loginToken: tokenResult.data['result']['token']
    });

    if (!accessTokenResult.data['success']) return jsonError(errors.INVALID_TOKEN);

    // const identity = jwtDecode(tokenResult.data['result']['token']);
    // if (!identity)
    //   return jsonError(errors.INVALID_TOKEN);

    //-- get user info
    let userInfo = await axios.post(
      UserService.DATA.IDENTITY_URL,
      {
        action: 'read_owned_info',
        info: ['profilePhoto', 'username', 'id']
      },
      {
        headers: {
          Authorization: accessTokenResult.data['result']['token']
        }
      }
    );

    // console.log('userinfo', userInfo);

    if (!userInfo.data['success']) {
      return jsonError(errors.PERMISSION_DENIED);
    }

    let identity = userInfo.data['result'];

    // if (!accessTokenResult.data['success'])
    //   return jsonError(errors.INVALID_TOKEN);

    // console.log('identity', identity);

    // const userId = identity['owner'];
    // if (!userId)
    //   return jsonError(errors.INVALID_TOKEN);

    const user = await User.findByPk(identity.id);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    //-- update token and avatar
    user.avatar = identity.profilePhoto;
    user.token = JSON.stringify(tokenResult.data['result']['token']);

    // if (!user.name || !user.avatar || !CronService.DATA.userData[userId]) {
    //   let response = await UserService.getUserInfo({
    //     userId: identity.id,
    //     info: ['username', 'phoneNumber', 'email', 'profilePhoto'],
    //     token: user.token
    //   });
    //
    //   if (!response.success)
    //     return response;
    //   CronService.DATA.userData[userId] = response.result;
    //   user.name = response.result && response.result.username;
    //   user.avatar = response.result && response.result.profilePhoto;
    // }

    await user.save();

    return jsonSuccess({
      userId: user.id,
      userRole: user.role,
      tokenData: tokenResult.data['result'],
      username: user.name,
      avatar: user.avatar
    });
  }

  static async addUser({ userId, username, roleId }) {
    let user = await User.findByPk(userId);
    if (user) return jsonError(errors.USER_EXISTED);

    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    user = await User.create({
      id: userId,
      name: username,
      token: ''
    });
    user.dataValues.role = await UserRole.create({
      userId,
      roleId
    });
    getPermissionsForSpecificUser(userId);
    return jsonSuccess(user);
  }

  static async updateUserRole({ userId, userRoleId, newRoleId }) {
    const role = await Role.findByPk(newRoleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    if (role.isSystemRole) return jsonError(errors.USER_CANNOT_ASSUME_SYSTEM_ROLE);

    let userRole = await UserRole.findByPk(userRoleId);
    if (!userRole) return jsonError(errors.NOT_FOUND_ERROR);

    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);

    if (user && user.id === UserService.DATA.SUPER_ADMIN_ID) {
      return jsonError(errors.SUPER_ADMIN_STAYS);
    }

    userRole = await userRole.update({
      roleId: newRoleId
    });

    getPermissionsForSpecificUser(userId);
    return jsonSuccess(userRole);
  }

  static async updateRolePermission({ rolePermissionId, roleId, permission }) {
    let rolePermission = await RolePermission.findByPk(rolePermissionId);
    if (!rolePermission) return jsonError(errors.NOT_FOUND_ERROR);

    let role = await Role.findByPk(roleId);
    if (!role) return jsonError(errors.NOT_FOUND_ERROR);

    let updateRolePermission = await RolePermission.findOne({
      where: {
        roleId,
        permission,
        id: { [Op.ne]: rolePermissionId }
      }
    });

    if (updateRolePermission) return jsonError(errors.DUPLICATED_ERROR);

    rolePermission = await rolePermission.update({
      roleId,
      permission
    });
    return jsonSuccess(rolePermission);
  }

  static async getMyRoleAndPermission({ userId }) {
    let result = [];
    const roles = await UserRole.findAll({
      where: { userId },
      include: [{ model: Role, attributes: ['name'] }]
    });

    roles.forEach(r => {
      if (r.Role && r.Role.name) {
        result.push(r.Role.name);
      }
    });

    return jsonSuccess(result);
  }

  static async createUserNote({ userId, content }) {
    const user = await User.findByPk(userId);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);

    let userNote = await UserNote.create({
      userId,
      content
    });
    return jsonSuccess(userNote);
  }

  static async updateUserNote({ userNoteId, content }) {
    let userNote = await UserNote.findByPk(userNoteId);
    if (!userNote) return jsonError(errors.NOT_FOUND_ERROR);
    if (content) userNote = await userNote.update({ content });
    return jsonSuccess(userNote);
  }

  static async removeUserNote({ userNoteId }) {
    let userNote = await UserNote.findByPk(userNoteId);
    if (!userNote) return jsonError(errors.NOT_FOUND_ERROR);
    await userNote.destroy();
    return jsonSuccess();
  }

  static async listNoteOfSpecificUser({ userId, evaluationDate }) {
    const user = await User.findByPk(userId);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);
    let query = { userId };
    if (evaluationDate) {
      query = Object.assign(query, {
        createdAt: { [Op.gte]: moment(evaluationDate) }
      });
    }
    let listNotes = await UserNote.findAll({
      where: query,
      order: [['createdAt', 'DESC']]
    });
    return jsonSuccess(listNotes);
  }

  static async addUserMentor({ userId, mentor }) {
    try {
      const data = {};
      if (userId === mentor) return jsonError(errors.MENTOR_INVALID);

      let user = await User.findByPk(userId);
      if (!user) return jsonError(errors.NOT_FOUND_ERROR);

      if (mentor !== null) {
        const mentorUser = await User.findByPk(mentor);
        if (!mentorUser) return jsonError(errors.NOT_FOUND_ERROR);
        data.mentor = mentor;
      } else {
        data.mentor = null;
      }

      if (user.mentor !== mentor) await user.update(data);
      const result = await User.findByPk(userId, {
        attributes: ['id', 'name', 'avatar'],
        include: [
          {
            model: User,
            as: 'infoMentor',
            attributes: ['id', 'name', 'avatar']
          }
        ]
      });
      return await jsonSuccess(result);
    } catch (error) {
      return jsonError(errors.SYSTEM_ERROR);
    }
  }

  static async storeTokenFirebase({ userId, tokenFirebase }) {
    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);
    await user.update({ tokenFirebase });
    return jsonSuccess();
  }

  static async listMyTasksAndProjects({ userId }) {
    let result = [];
    let projects = await UserProjectRole.findAll({
      where: { userId, deletedAt: { [Op.eq]: null } },
      attributes: ['projectId'],
      raw: true
    });
    projects = projects.map(project => project.projectId);
    const listProjects = _.uniq(projects);
    for (let i = 0; i < listProjects.length; i++) {
      result.push({
        type: 'project',
        value: listProjects[i]
      });
    }

    let tasks = await Task.findAll({
      include: [{ model: Sprint, where: { status: STATUS.ACTIVE } }]
    });
    let currentTasks = tasks.filter(task => task.creator === userId || task.assignee === userId);
    for (let j = 0; j < currentTasks.length; j++) {
      result.push({
        type: 'task',
        value: currentTasks[j].id
      });
    }
    let taskDrag = tasks.filter(task => !(task.creator === userId || task.assignee === userId));
    let listTaskComment = [];
    for (let i = 0; i < taskDrag.length; i++) {
      let taskStates = await TaskState.findAll({
        where: {
          taskId: taskDrag[i].id,
          editor: userId
        }
      });
      if (taskStates.length) {
        result.push({
          type: 'task',
          value: taskDrag[i].id
        });
      } else {
        await listTaskComment.push(taskDrag[i].id);
      }
    }
    let myTasksComment = await Comment.findAll({
      where: {
        userId,
        commentTableId: { [Op.in]: listTaskComment },
        commentTableType: COMMENT_TYPE.TASK
      }
    });
    myTasksComment = myTasksComment.map(comment => comment.commentTableId);
    const listTasks = _.uniq(myTasksComment);
    for (let i = 0; i < listTasks.length; i++) {
      result.push({
        type: 'task',
        value: listTasks[i]
      });
    }
    return jsonSuccess(result);
  }
}

export { UserService };
