import { errors, jsonError, jsonSuccess } from '../utils/system';
import { AllocationPlan } from '../models/schema/AllocationPlan';
import { User } from '../models/schema/User';
import { Skill } from '../models/schema/Skill';
import { getOverloadEffortPercentUsers, calculateLength } from '../utils/common';
import { Op } from 'sequelize';

class AllocationPlanService {
  static async boot() {}

  static async createAllocationPlan({ actor, data }) {
    const allocationPlanData = [];

    const userIds = data.map(item => item.userId);
    const listEffortOfUser = await AllocationPlan.findAll({ where: { userId: userIds } });
    const overloadUsers = getOverloadEffortPercentUsers(listEffortOfUser, data);
    if (calculateLength(overloadUsers)) {
      const overloadUserIds = overloadUsers.map(item => item.userId);
      let users = await User.findAll({ where: { id: overloadUserIds }, attributes: ['id', 'name'] });
      if (!calculateLength(users) || users.length !== overloadUsers.length) return jsonError(errors.NOT_FOUND_ERROR);

      users = users.map(user => {
        const overloadUser = overloadUsers.find(item => item.userId === user.id);
        return `${user.name}(${overloadUser ? overloadUser.currentEffort : 0}%)`;
      });
      const message = `${users.join(', ')} ${users.length === 1 ? 'has' : 'have'} effort overload`;
      return jsonError(errors.EFFORT_OVERLOAD_DETECTED, { message });
    }

    for (let i = 0; i < data.length; i++) {
      const { userId, effortPercent, startDate, endDate } = data[i];
      const allocationPlanItem = {
        userId,
        actor,
        effortPercent,
        startDate,
        endDate
      };

      allocationPlanData.push(allocationPlanItem);
    }

    const allocationPlans = await AllocationPlan.bulkCreate(allocationPlanData);
    const ids = allocationPlans.map(item => item.id);
    const result = await AllocationPlan.findAll({
      where: { id: ids },
      include: [
        {
          model: User,
          attributes: ['id', 'name', 'avatar', 'isActive'],
          where: { isActive: true }
        }
      ]
    });

    return jsonSuccess(result);
  }

  static async updateAllocationPlan({ allocationPlanId, actor, effortPercent, startDate, endDate }) {
    const allocationPlan = await AllocationPlan.findByPk(allocationPlanId);
    if (!allocationPlan) return jsonError(errors.NOT_FOUND_ERROR);

    const allocationPlanUpdateData = {
      actor,
      effortPercent,
      startDate,
      endDate,
      updatedAt: new Date()
    };
    let listEffortOfUser = await AllocationPlan.findAll({
      where: {
        userId: allocationPlan.userId,
        id: { [Op.ne]: allocationPlanId }
      }
    });

    const newEffort = {
      userId: allocationPlan.userId,
      effortPercent,
      startDate,
      endDate
    };

    const overloadUsers = getOverloadEffortPercentUsers(listEffortOfUser, newEffort);
    if (calculateLength(overloadUsers)) {
      let user = await User.findByPk(allocationPlan.userId, { attributes: ['id', 'name'] });
      if (!user) return jsonError(errors.NOT_FOUND_ERROR);

      const overloadUser = overloadUsers.find(item => item.userId === user.id);
      user = `${user.name}(${overloadUser ? overloadUser.currentEffort : 0}%)`;
      const message = `${user} has effort overload`;
      return jsonError(errors.EFFORT_OVERLOAD_DETECTED, { message });
    }

    await allocationPlan.update(allocationPlanUpdateData);

    const allocationUpdated = await AllocationPlan.findByPk(allocationPlanId, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar', 'isActive'] }]
    });

    return jsonSuccess(allocationUpdated);
  }

  static async removeAllocationPlan({ allocationPlanId }) {
    let allocationPlan = await AllocationPlan.findByPk(allocationPlanId);
    if (!allocationPlan) return jsonError(errors.NOT_FOUND_ERROR);

    await allocationPlan.destroy();
    return jsonSuccess();
  }

  static async listAllocationPlans() {
    let allocationPlans = await AllocationPlan.findAll({
      include: [
        {
          model: User,
          attributes: ['id', 'name', 'avatar', 'isActive'],
          where: { isActive: true }
        }
      ],
      order: [['createdAt', 'DESC']]
    });
    return jsonSuccess(allocationPlans);
  }

  static async listAllocationsPlanOfUsers() {
    const result = await User.findAll({
      where: { isActive: true, id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') } },
      attributes: ['id', 'name', 'avatar', 'mentor', 'isActive'],
      include: [
        {
          model: User,
          as: 'infoMentor',
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        { model: Skill, as: 'skills' },
        { model: AllocationPlan, as: 'allocationsPlanAs' }
      ]
    });
    return jsonSuccess(result);
  }
}

export { AllocationPlanService };
