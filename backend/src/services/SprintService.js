import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Project } from '../models/schema/Project';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { Task } from '../models/schema/Task';
import { Allocation } from '../models/schema/Allocation';
import { WorkflowState, EFFORT_TYPES } from '../models/schema/WorkflowState';
import { User } from '../models/schema/User';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import {
  countDayWorking,
  durationHoursWork,
  handleAllocationDataOfUser,
  paginate,
  formatDateTime
} from '../utils/common';
import { TaskState } from '../models/schema/TaskState';
import { ProjectRole } from '../models/schema/ProjectRole';
import { AllocationLabel } from '../models/schema/AllocationLabel';
import { KPISprint, TYPES } from '../models/schema/KPISprint';
import { getKPICoefficient } from '../utils/common';
import { KPI_TYPE } from '../models/schema/KPICoefficient';
import { sequelize } from '../core/boot';
import { messages } from '../utils/message_errors';

class SprintService {
  static async boot() {
    // set openCalculatedKpiSprint eq true to calculate again
    // const clearKpiSprint = await Promise.all([
    //   Sprint.update({ calculatedKpiSprint: false }, { where: { calculatedKpiSprint: true } }),
    //   KPISprint.destroy({ where: { type: TYPES.member } })
    // ]);
    // if (!clearKpiSprint) return jsonError(errors.SYSTEM_ERROR);
    // return jsonSuccess();
  }

  static async createSprint({ name, projectId, startDate, endDate }) {
    // if (startDate >= endDate)
    //   return jsonError(errors.TIME_ERROR);

    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let sprint = await Sprint.findOne({
      where: {
        projectId,
        [Op.or]: [
          { name },
          {
            endDate: {
              [Op.gte]: startDate
            }
          }
        ]
      }
    });
    if (sprint) return jsonError(errors.DUPLICATED_ERROR);

    sprint = await Sprint.create({
      name: name,
      projectId: projectId,
      status: STATUS.UNACTIVE,
      startDate: startDate,
      endDate: endDate
    });
    return jsonSuccess(sprint);
  }

  static async updateSprint({ sprintId, name, status, startDate, endDate }) {
    let sprint = await Sprint.findByPk(sprintId, {
      include: [{ model: Project }]
    });
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    const isStartDateChange = startDate > sprint.startDate;
    const isEndDateChange = endDate < sprint.endDate;
    if (isStartDateChange || isEndDateChange) {
      const allocation = await Allocation.findOne({ where: { sprintId } });
      if (allocation) return jsonError(errors.DEPENDENCIES_ERROR, { message: messages.cannot_update_time_sprint });
    }

    if (sprint.startDate > startDate) return jsonError(errors.TIME_ERROR);

    let check = false;
    if (status) check = sprint.status !== status;
    if (startDate >= endDate) return jsonError(errors.TIME_ERROR);
    // let array = Object.values(STATUS);
    // if (!array.includes(status))
    //   return jsonError(errors.PARAM_INVALID);

    const projectId = sprint.projectId;
    const sprintDuplicate = await Sprint.findOne({
      where: {
        id: { [Op.ne]: sprintId },
        projectId,
        [Op.or]: [
          { name },
          {
            startDate: { [Op.lte]: startDate },
            endDate: { [Op.gte]: startDate }
          },
          {
            startDate: { [Op.lte]: endDate },
            endDate: { [Op.gte]: endDate }
          }
        ]
      }
    });
    if (sprintDuplicate) return jsonError(errors.DUPLICATED_ERROR);

    if (status === STATUS.ACTIVE) {
      let isActive = await Sprint.findOne({
        where: {
          status: STATUS.ACTIVE,
          projectId,
          id: { [Op.ne]: sprintId }
        }
      });
      if (isActive) return jsonError(errors.DUPLICATED_ERROR);
    }

    if (status === STATUS.COMPLETED) {
      const listAllTasksTerminated = await Task.findAll({
        where: { sprintId },
        include: [
          {
            model: WorkflowState,
            where: { effortType: { [Op.ne]: EFFORT_TYPES.TERMINATED } }
          }
        ],
        attributes: ['id']
      });
      if (Array.isArray(listAllTasksTerminated) && listAllTasksTerminated.length > 0) {
        const idTasks = listAllTasksTerminated.map(item => item.id);
        await Task.update({ sprintId: null }, { where: { id: idTasks } });
      }
    }

    sprint = await sprint.update({
      name: name,
      status: status,
      startDate: startDate,
      endDate: endDate
    });
    let result = jsonSuccess(sprint);
    return { ...result, check };
  }

  static async deleteSprint({ sprintId }) {
    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    let tasks = await Task.findAll({ where: { sprintId } });
    if (tasks.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            Allocation.destroy({ where: { sprintId }, transaction: t }),
            sprint.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // await Allocation.destroy({where: {sprintId: sprintId}});
    // await sprint.destroy();
    // return jsonSuccess();
  }

  static async getSprint({ sprintId }) {
    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    const projectId = sprint.projectId;
    const startDateSprint = sprint.startDate;

    const promises = await Promise.all([
      Task.findAll({
        where: { sprintId },
        include: [{ model: WorkflowState }]
      }),
      Allocation.findAll({
        where: { sprintId },
        include: [{ model: User, attributes: ['id', 'name', 'avatar'] }, { model: AllocationLabel, as: 'labels' }]
      }),
      Sprint.findAll({
        where: {
          projectId,
          id: { [Op.ne]: sprintId },
          endDate: { [Op.lt]: startDateSprint }
        },
        order: [['endDate', 'DESC']],
        limit: 1
      })
    ]);
    sprint.dataValues.tasks = promises[0];
    sprint.dataValues.allocations = promises[1];

    if (Array.isArray(promises[2]) && promises[2].length === 1) {
      const beforeSprint = promises[2][0];
      sprint.dataValues.beforeAllocations = await Allocation.findAll({
        where: { sprintId: beforeSprint.id },
        include: [{ model: User, attributes: ['id', 'name', 'avatar'] }, { model: AllocationLabel, as: 'labels' }]
      });
    } else {
      sprint.dataValues.beforeAllocations = [];
    }
    return jsonSuccess(sprint);
  }

  static async getSprintByProjectId({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let sprints = await Sprint.findAll({
      where: { projectId },
      order: [['id', 'DESC']]
    });
    for (let i = 0; i < sprints.length; i++) {
      const sprint = sprints[i];
      let listMembers = [];
      let tmp = [];

      const promises = await Promise.all([
        Task.findAll({
          where: { sprintId: sprint.id },
          include: [{ model: WorkflowState }]
        }),
        Allocation.findAll({
          where: { sprintId: sprint.id },
          include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
        })
      ]);

      sprint.dataValues.totalTask = promises[0].length;
      sprint.dataValues.totalTaskCompleted = promises[0].reduce(
        (total, v) => (v.WorkflowState && v.WorkflowState.effortType === EFFORT_TYPES.TERMINATED ? ++total : total),
        0
      );
      sprint.dataValues.allocations = promises[1];
      let allAllocation = promises[1];
      for (let j = 0; j < allAllocation.length; j++) {
        if (!tmp.includes(allAllocation[j].userId)) {
          tmp.push(allAllocation[j].userId);
          listMembers.push(allAllocation[j]);
        }
      }
      sprint.dataValues.tasks = promises[0];
      sprint.dataValues.members = listMembers;
    }
    return jsonSuccess(sprints);
  }

  static async getSprintDiffCompleted({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let sprints = await Sprint.findAll({
      where: { projectId, status: { [Op.ne]: STATUS.COMPLETED } }
    });
    return jsonSuccess(sprints);
  }

  static async getSprintProgressOfMember({ sprintId, users }) {
    const sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      // get total time
      let totalTime = 0;
      const hoursWorkingOneDay = getEnv('HOURS_WORKING_IN_ONE_DAY');
      const allocations = await handleAllocationDataOfUser(user.id, sprintId);
      const timestamps = allocations.timestamps;
      const efforts = allocations.efforts;
      for (let j = 0; j < timestamps.length - 1; j++) {
        let daysWorking = countDayWorking(timestamps[j], timestamps[j + 1]);
        totalTime += daysWorking * hoursWorkingOneDay * (efforts[j] / 100);
      }
      user.dataValues.totalTime = totalTime;
      //get total time completed
      //get data for progress bar (rate of tasks done)

      let totalTimeCompleted = 0;
      let listAllTasksTerminated = await Task.findAll({
        where: { sprintId },
        include: [
          {
            model: WorkflowState,
            where: { effortType: EFFORT_TYPES.TERMINATED }
          }
        ]
      });
      for (let m = 0; m < listAllTasksTerminated.length; m++) {
        const taskTerminated = listAllTasksTerminated[m];
        let allTaskState = await TaskState.findAll({
          where: { taskId: taskTerminated.id, editor: user.id },
          include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
        });
        for (let n = 0; n < allTaskState.length - 1; n++) {
          const taskState = allTaskState[n];
          const nextTaskState = allTaskState[n + 1];
          if (taskState.toState === nextTaskState.fromState) {
            if (taskState && taskState.toStates && taskState.toStates.effortType === EFFORT_TYPES.PROGRESS) {
              totalTimeCompleted += durationHoursWork(taskState.createdAt, nextTaskState.createdAt);
            }
          }
        }
      }
      user.dataValues.totalTimeCompleted = totalTimeCompleted >= 0 ? totalTimeCompleted : 0;
    }
    return jsonSuccess(users);
  }

  static async getMembersOfSprint({ sprintId }) {
    const sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);
    const allocations = await Allocation.findAll({
      where: { sprintId: sprint.id },
      include: [{ model: User, attributes: ['id', 'name', 'avatar', 'isActive'] }]
    });
    let listMembers = [];
    let tmp = [];
    for (let j = 0; j < allocations.length; j++) {
      if (!tmp.includes(allocations[j].userId)) {
        tmp.push(allocations[j].userId);
        listMembers.push(allocations[j].User);
      }
    }
    return jsonSuccess(listMembers);
  }

  static async createKpiSprint({
    actor,
    userId,
    sprintId,
    projectId,
    workManagementPoint,
    peopleManagementPoint,
    processPoint
  }) {
    let dataSprintServiceDATA = await getKPICoefficient(KPI_TYPE.KPI_Leader);
    if (!dataSprintServiceDATA) return jsonError(errors.SYSTEM_ERROR);

    // const defaultWorkManagementPoints = Object.values(WORK_MANAGEMENT_POINTS);
    // if (defaultWorkManagementPoints.indexOf(workManagementPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }
    // const defaultPeopleManagementPoints = Object.values(PEOPLE_MANAGEMENT_POINTS);
    // if (defaultPeopleManagementPoints.indexOf(peopleManagementPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }
    // const defaultProcessPoints = Object.values(PROCESS_POINTS);
    // if (defaultProcessPoints.indexOf(processPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }

    const project = await Project.findByPk(projectId, {
      include: [
        { model: Sprint, as: 'sprints' },
        {
          model: UserProjectRole,
          as: 'users',
          where: { deletedAt: { [Op.eq]: null } }
        }
      ]
    });
    if (!project) {
      return jsonError(errors.PROJECT_NOT_FOUND);
    }

    const userIds = project.users.map(user => user.userId);
    if (userIds.indexOf(userId) < 0) {
      return jsonError(errors.USER_NOT_EXISTS_IN_PROJECT);
    }
    const sprintIds = project.sprints.map(sprint => sprint.id);
    if (sprintIds.indexOf(sprintId) < 0) {
      return jsonError(errors.SPRINT_NOT_EXISTS_IN_PROJECT);
    }

    const point =
      (project.projectRate *
        (workManagementPoint * dataSprintServiceDATA.percentWorkManagement +
          peopleManagementPoint * dataSprintServiceDATA.percentPeopleManagement +
          processPoint * dataSprintServiceDATA.percentDoProcess)) /
      100;
    const kpiSprint = await KPISprint.create({
      actor,
      userId,
      projectId,
      sprintId,
      point: +point.toFixed(5),
      workManagementPoint,
      peopleManagementPoint,
      processPoint,
      type: TYPES.leader
    });
    return jsonSuccess(kpiSprint);
  }

  static async removeKpiSprint({ actor, kpiSprintId }) {
    const kpiSprint = await KPISprint.findByPk(kpiSprintId);
    if (!kpiSprint) {
      return jsonError(errors.NOT_FOUND_ERROR);
    }
    if (actor !== kpiSprint.actor) {
      return jsonError(errors.PERMISSION_DENIED);
    }
    const isRemovedKpiSprint = await kpiSprint.destroy();
    return isRemovedKpiSprint ? jsonSuccess(isRemovedKpiSprint) : jsonError(errors.SYSTEM_ERROR);
  }

  static async updateKpiSprint({ actor, kpiSprintId, workManagementPoint, peopleManagementPoint, processPoint }) {
    let dataSprintServiceDATA = await getKPICoefficient(KPI_TYPE.KPI_Leader);
    if (!dataSprintServiceDATA) return jsonError(errors.SYSTEM_ERROR);

    // const defaultWorkManagementPoints = Object.values(WORK_MANAGEMENT_POINTS);
    // if (defaultWorkManagementPoints.indexOf(workManagementPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }
    // const defaultPeopleManagementPoints = Object.values(PEOPLE_MANAGEMENT_POINTS);
    // if (defaultPeopleManagementPoints.indexOf(peopleManagementPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }
    // const defaultProcessPoints = Object.values(PROCESS_POINTS);
    // if (defaultProcessPoints.indexOf(processPoint) < 0) {
    //   return jsonError(errors.PARAM_INVALID);
    // }

    let kpiSprint = await KPISprint.findByPk(kpiSprintId, {
      include: [{ model: Project, as: 'project' }, { model: Sprint, as: 'sprint' }]
    });
    if (!kpiSprint) {
      return jsonError(errors.NOT_FOUND_ERROR);
    }
    if (actor !== kpiSprint.actor) {
      return jsonError(errors.PERMISSION_DENIED);
    }

    const point =
      (kpiSprint.project.projectRate *
        (workManagementPoint * dataSprintServiceDATA.percentWorkManagement +
          peopleManagementPoint * dataSprintServiceDATA.percentPeopleManagement +
          processPoint * dataSprintServiceDATA.percentDoProcess)) /
      100;
    const dataUpdate = {
      point: +point.toFixed(5),
      workManagementPoint,
      peopleManagementPoint,
      processPoint
    };

    kpiSprint = await kpiSprint.update(dataUpdate);
    return jsonSuccess(kpiSprint);
  }

  static async getListKpiSprint({ userId, projectId, type, page, limit }) {
    const query = {};
    if (userId) {
      const user = await User.findByPk(userId);
      if (!user) return jsonError(errors.USER_NOT_FOUND);
      query.userId = userId;
    }
    if (projectId) {
      const project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.PROJECT_NOT_FOUND);
      query.projectId = projectId;
    }

    const types = Object.values(TYPES);
    if (type && types.indexOf(type) >= 0) {
      query.type = type;
    }
    const include = [
      { model: User, as: 'user', attributes: ['id', 'name', 'avatar'] },
      { model: User, as: 'infoActor', attributes: ['id', 'name', 'avatar'] },
      { model: Sprint, as: 'sprint' },
      {
        model: Project,
        as: 'project',
        include: [
          {
            model: UserProjectRole,
            as: 'users',
            where: { deletedAt: { [Op.eq]: null } },
            include: [{ model: User, attributes: ['id', 'name', 'avatar'] }, { model: ProjectRole }]
          }
        ]
      }
    ];
    if (limit && page) {
      const data = await paginate({
        model: KPISprint,
        query: { where: query, include, distinct: true },
        page,
        limit
      });
      return data.success ? jsonSuccess(data.result) : data;
    }

    const kpiSprints = await KPISprint.findAll({
      where: query,
      include
    });
    return jsonSuccess({
      data: kpiSprints,
      total: kpiSprints.length
    });
  }

  static async updateCalculatedKpiSprint({ sprintId, state }) {
    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) {
      return jsonError(errors.SPRINT_NOT_FOUND);
    }
    sprint = await sprint.update({
      calculatedKpiSprint: state
    });
    return jsonSuccess(sprint);
  }

  static async getListFinishedSprint({ calculatedKpiSprintState }) {
    const query = {
      status: STATUS.COMPLETED
    };
    if ([false, true].includes(calculatedKpiSprintState)) {
      query.calculatedKpiSprint = calculatedKpiSprintState;
    }
    return await Sprint.findAll({
      where: query,
      include: [{ model: Allocation, as: 'allocations' }, { model: KPISprint, as: 'kpiSprints' }, { model: Project }]
    });
  }

  static async createKpiSprintForMember({
    userId,
    sprintId,
    projectId,
    taskComplete,
    userResponsibilityPoint,
    userDedicationPoint,
    missDeadline,
    errorRelate,
    projectRate
  }) {
    let dataSprintServiceDATA = await getKPICoefficient(KPI_TYPE.KPI_Project);
    if (!dataSprintServiceDATA) return jsonError(errors.SYSTEM_ERROR);

    const point =
      (projectRate *
        (dataSprintServiceDATA.percentTaskComplete * taskComplete +
          dataSprintServiceDATA.percentResponsibility * userResponsibilityPoint +
          dataSprintServiceDATA.percentDedication * userDedicationPoint)) /
      100;

    const kpiSprint = await KPISprint.create({
      userId,
      projectId,
      sprintId,
      point: point > 0 ? +point.toFixed(5) : 0,
      taskComplete,
      missDeadline,
      errorRelate,
      type: TYPES.member
    });
    return jsonSuccess(kpiSprint);
  }
}

export { SprintService };
