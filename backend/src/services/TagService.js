import { errors, jsonError, jsonSuccess } from '../utils/system';
import { Project } from '../models/schema/Project';
import { TaskTag } from '../models/schema/TaskTag';
import { parseJSONIsArray } from '../utils/common';

class TagService {
  static async boot() {}

  static async storeTags({ newTags, projectId }) {
    try {
      let project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.NOT_FOUND_ERROR);

      let tagByProject = await TaskTag.findOne({ where: { projectId } });
      if (tagByProject) {
        let oldTags = parseJSONIsArray(tagByProject && tagByProject.tags);
        for (let i = 0; i < newTags.length; i++) {
          if (!oldTags.includes(newTags[i])) oldTags.push(newTags[i]);
        }
        await tagByProject.update({ tags: JSON.stringify(oldTags) });
      } else {
        await TaskTag.create({ projectId, tags: JSON.stringify(newTags) });
      }
      return jsonSuccess();
    } catch (e) {
      return jsonError(errors.SYSTEM_ERROR);
    }
  }

  static async suggestTags({ projectId }) {
    try {
      const project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.NOT_FOUND_ERROR);

      const data = await TaskTag.findOne({
        where: { projectId },
        attributes: ['tags']
      });
      return jsonSuccess(parseJSONIsArray(data && data.tags));
    } catch (e) {
      return jsonError(errors.SYSTEM_ERROR);
    }
  }
}

export { TagService };
