import fs from 'fs';
import path from 'path';
import jwt from 'jsonwebtoken';

import { errors, jsonError, jsonSuccess, logger } from '../utils/system';

class JWTService {
  static async boot() {
    //-- init DATA
    JWTService.DATA = {
      JWT_CERT: fs.readFileSync(path.join(__dirname, `../../assets/${getEnv('JWT_CERT')}`)),
      JWT_KEY: getEnv('JWT_KEY'),
      JWT_EXPIRE_TIME: getEnv('JWT_EXPIRE_TIME')
    };
  }

  static async signToken({ data }) {
    return new Promise(resolve => {
      jwt.sign(
        data,
        JWTService.DATA.JWT_KEY,
        { expiresIn: Number(JWTService.DATA.JWT_EXPIRE_TIME) },
        (error, token) => {
          if (error) {
            logger.log(error);
            return resolve(jsonError(errors.SYSTEM_ERROR));
          }
          return resolve(
            jsonSuccess({
              token,
              tokenExpiration: Date.now() + Number(getEnv('JWT_EXPIRE_TIME'))
            })
          );
        }
      );
    });
  }

  static async decodeToken({ token }) {
    // console.log("decoding", token);
    return new Promise(resolve => {
      jwt.verify(token, JWTService.DATA.JWT_KEY, (error, token) => {
        if (error) {
          logger.error(error);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess(token));
      });
    });
  }

  static async decodeIdentityLoginToken({ token }) {
    return new Promise(resolve => {
      jwt.verify(token, JWTService.DATA.JWT_CERT, (error, decode) => {
        if (error) {
          logger.error(error);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess(decode));
      });
    });
  }
}

export { JWTService };
