import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { ProjectRole } from '../models/schema/ProjectRole';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { PermissionSchemeDetail } from '../models/schema/PermissionSchemeDetail';
import { Project, ACTIVE_STATUS } from '../models/schema/Project';
import { PermissionScheme } from '../models/schema/PermissionScheme';
import { User } from '../models/schema/User';
import { Role } from '../models/schema/Role';
import { getPermissionsForSpecificUser } from '../utils/common';
import { sequelize } from '../core/boot';
import { UserRole } from '../models/schema/UserRole';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { Allocation } from '../models/schema/Allocation';

class ProjectRoleService {
  static async boot() {
    ProjectRoleService.DATA = {
      userRole: null,
      unauthenticatedRole: null,
      superAdminRole: null
    };

    ProjectRoleService.DATA.unauthenticatedRole = await Role.findOne({
      where: { name: getEnv('UNAUTHENTICATED_ROLE') }
    });
    ProjectRoleService.DATA.superAdminRole = await Role.findOne({
      where: { name: getEnv('SUPER_ADMIN_ROLE') }
    });
  }

  static async createProjectRole({ name, priorityLevel }) {
    let projectRole = await ProjectRole.findOne({
      where: { name: name.trim() }
    });
    if (projectRole) return jsonError(errors.DUPLICATED_ERROR);
    projectRole = await ProjectRole.create({
      name: name.trim(),
      priorityLevel
    });
    return jsonSuccess(projectRole);
  }

  static async updateProjectRole({ name, projectRoleId, priorityLevel }) {
    const data = { name: name.trim() };
    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);
    if (Number.isInteger(priorityLevel) || priorityLevel === null) {
      data.priorityLevel = priorityLevel;
    }
    let updatedProjectRole = await ProjectRole.findOne({
      where: {
        name: name.trim(),
        id: { [Op.ne]: projectRoleId }
      }
    });
    if (updatedProjectRole) return jsonError(errors.DUPLICATED_ERROR);
    projectRole = await projectRole.update(data);
    return jsonSuccess(projectRole);
  }

  static async deleteProjectRole({ projectRoleId }) {
    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);
    let promises = await Promise.all([
      UserProjectRole.findAll({ where: { projectRoleId } }),
      PermissionSchemeDetail.findAll({ where: { projectRoleId } })
    ]);
    if (promises[0].length > 0 || promises[1].length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);
    await projectRole.destroy();
    return jsonSuccess();
  }

  static async listProjectRole() {
    let projectRoles = await ProjectRole.findAll({
      order: [['id', 'ASC']]
    });
    return jsonSuccess(projectRoles);
  }

  static async listPermissionsOfRoleProject({ projectRoleId }) {
    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);
    projectRole.dataValues.permissions = await PermissionSchemeDetail.findAll({
      where: { projectRoleId },
      attributes: ['id', 'permissionSchemeId'],
      include: [{ model: PermissionScheme }]
    });
    return jsonSuccess(projectRole);
  }

  static async addMultipleUserIntoProject({ projectId, projectRoleIds }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.PROJECT_NOT_FOUND);

    let users = await User.findAll({
      where: {
        id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') },
        isActive: true
      },
      include: {
        model: UserRole,
        as: 'roles',
        include: [
          {
            model: Role,
            where: { name: { [Op.ne]: getEnv('CUSTOMER_ROLE') } }
          }
        ],
        required: true
      },
      attributes: ['id']
    });

    let arrayCreate = [];
    let arrayUpdate = [];

    for (let i = 0; i < projectRoleIds.length; i++) {
      const projectRoleId = projectRoleIds[i];
      const projectRole = await ProjectRole.findByPk(projectRoleId);
      if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);

      for (let j = 0; j < users.length; j++) {
        const userId = users[j].id;
        if (userId) {
          let userProjectRole = await UserProjectRole.findOne({
            where: {
              userId,
              projectId,
              projectRoleId
            }
          });
          if (!userProjectRole) {
            arrayCreate.push({ projectId, userId, projectRoleId });
          } else if (userProjectRole.deletedAt) {
            arrayUpdate.push(userProjectRole.id);
          }
        }
      }
    }

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              if (arrayCreate.length > 0) {
                await UserProjectRole.bulkCreate(arrayCreate, {
                  transaction: t
                });
              }
              if (arrayUpdate.length > 0) {
                await UserProjectRole.update(
                  { deletedAt: null },
                  { where: { id: { [Op.in]: arrayUpdate } }, transaction: t }
                );
              }
              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          users.map(user => {
            getPermissionsForSpecificUser(user.id);
          });
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async addUserIntoProject({ projectId, userId, projectRoleIds }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.PROJECT_NOT_FOUND);

    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    for (let i = 0; i < projectRoleIds.length; i++) {
      const projectRoleId = projectRoleIds[i];
      const projectRole = await ProjectRole.findByPk(projectRoleId);
      if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);

      let userProjectRole = await UserProjectRole.findOne({
        where: {
          userId,
          projectId,
          projectRoleId
        }
      });
      if (!userProjectRole) {
        await UserProjectRole.create({
          projectId,
          userId,
          projectRoleId
        });
      } else if (userProjectRole.deletedAt) {
        await userProjectRole.update({
          deletedAt: null
        });
      }
    }
    getPermissionsForSpecificUser(userId);
    return jsonSuccess(project);
    // let projectRole = await ProjectRole.findOne({where: {id: projectRoleId}});
    // let user = await User.findOne({where: {id: userId}});
    // let project = await Project.findOne({where: {id: projectId}});
    // if (!projectRole || !project || !user)
    //   return jsonError(errors.NOT_FOUND_ERROR);
    //
    // let existUser = await UserProjectRole.findOne({where: {userId, projectId, projectRoleId}});
    // if (existUser)
    //   return jsonError(errors.DUPLICATED_ERROR);
    //
    // projectRole = await UserProjectRole.create({
    //   projectId: projectId,
    //   userId: userId,
    //   projectRoleId: projectRoleId,
    // });
    // return jsonSuccess(projectRole);
  }

  static async listUsersByProject({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    let users = await UserProjectRole.findAll({
      where: {
        projectId,
        deletedAt: { [Op.eq]: null }
      },
      attributes: ['id', 'userId', 'projectRoleId'],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }, { model: ProjectRole }]
    });
    return jsonSuccess(users);
  }

  static async updateRoleUserInProject({ projectId, userId, projectRoleIds }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.PROJECT_NOT_FOUND);

    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    for (let i = 0; i < projectRoleIds.length; i++) {
      const projectRoleId = projectRoleIds[i];
      const projectRole = await ProjectRole.findByPk(projectRoleId);
      if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);
    }

    //-- get all project roles
    let userRoles = await UserProjectRole.findAll({
      where: { userId, projectId }
    });
    let assignedRoleNotDeleted = userRoles.filter(role => !role.deletedAt);
    let assignedRoleIdsNotDeleted = assignedRoleNotDeleted.map(role => role.projectRoleId);
    let assignedRoleDeleted = userRoles.filter(role => role.deletedAt);
    let assignedRoleIdsDeleted = assignedRoleDeleted.map(role => role.projectRoleId);

    let tobeAddedNotExists = projectRoleIds.filter(
      roleId => !assignedRoleIdsNotDeleted.includes(roleId) && !assignedRoleIdsDeleted.includes(roleId)
    );
    let tobeAddedExistsAndDeleted = projectRoleIds.filter(
      roleId => !assignedRoleIdsNotDeleted.includes(roleId) && assignedRoleIdsDeleted.includes(roleId)
    );
    let tobeRemoved = assignedRoleIdsNotDeleted.filter(roleId => !projectRoleIds.includes(roleId));
    if (tobeAddedNotExists.length) {
      await UserProjectRole.bulkCreate(
        tobeAddedNotExists.map(roleId => ({
          userId,
          projectId,
          projectRoleId: roleId
        }))
      );
    }

    if (tobeAddedExistsAndDeleted.length) {
      await UserProjectRole.update(
        {
          deletedAt: null
        },
        {
          where: {
            userId,
            projectId,
            projectRoleId: { [Op.in]: tobeAddedExistsAndDeleted }
          }
        }
      );
    }

    if (tobeRemoved.length) {
      // await UserProjectRole.destroy({
      //   where: {
      //     userId,
      //     projectId,
      //     projectRoleId: {
      //       [Op.in]: tobeRemoved
      //     },
      //   }
      // });
      await UserProjectRole.update(
        {
          deletedAt: new Date()
        },
        {
          where: {
            userId,
            projectId,
            projectRoleId: {
              [Op.in]: tobeRemoved
            }
          }
        }
      );
    }

    // let userProjectRole = await UserProjectRole.findOne({where: {id: roleCombinationId}});
    // if (!userProjectRole)
    //   return jsonError(errors.NOT_FOUND_ERROR);
    //
    // //-- the new role should not duplicated
    // let combination = await UserProjectRole.findOne({
    //   where: {
    //     userId: userProjectRole.userId,
    //     projectId: userProjectRole.projectId,
    //     projectRoleId
    //   }
    // });
    // if (combination)
    //   return jsonError(errors.DUPLICATED_ERROR);
    //
    // userProjectRole.update({projectRoleId});
    getPermissionsForSpecificUser(userId);
    return jsonSuccess(project);
  }

  static async deleteUserProject({ userId, projectId }) {
    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    // await UserProjectRole.destroy({
    //   where: {userId, projectId}
    // });
    await UserProjectRole.update(
      {
        deletedAt: new Date()
      },
      {
        where: { userId, projectId }
      }
    );
    getPermissionsForSpecificUser(userId);
    return jsonSuccess(project);
  }

  static async getListManagersByUser({ userId }) {
    const checkPM = await UserProjectRole.findOne({
      where: { userId, deletedAt: { [Op.eq]: null } },
      attributes: ['projectRoleId'],
      include: [
        {
          model: ProjectRole,
          where: { name: { [Op.in]: ['pm', 'project manager'] } }
        }
      ]
    });

    if (checkPM) return jsonSuccess([]);

    const userProjectRoles = await UserProjectRole.findAll({
      where: { userId, deletedAt: { [Op.eq]: null } },
      attributes: ['projectId'],
      include: [
        {
          model: Project,
          where: {
            isActive: ACTIVE_STATUS.active,
            name: { [Op.ne]: 'common' }
          },
          attributes: ['id', 'name'],
          include: [
            {
              model: Sprint,
              as: 'sprints',
              where: { status: STATUS.ACTIVE },
              include: [
                {
                  model: Allocation,
                  as: 'allocations',
                  where: { userId },
                  required: true
                }
              ],
              required: true
            },
            {
              model: UserProjectRole,
              as: 'users',
              where: {
                deletedAt: { [Op.eq]: null },
                [Op.and]: [{ userId: { [Op.ne]: userId } }, { userId: { [Op.ne]: getEnv('SUPER_ADMIN_ID') } }]
              },
              attributes: ['userId'],
              include: [
                {
                  model: User,
                  attributes: ['id', 'name', 'avatar'],
                  where: { isActive: true },
                  include: {
                    model: UserRole,
                    as: 'roles',
                    include: [
                      {
                        model: Role,
                        where: { name: { [Op.ne]: getEnv('CUSTOMER_ROLE') } },
                        required: true
                      }
                    ],
                    required: true
                  }
                },
                {
                  model: ProjectRole,
                  attributes: ['id', 'name'],
                  where: {
                    name: { [Op.in]: ['leader', 'pm', 'project manager'] }
                  },
                  required: true
                }
              ],
              required: true
            }
          ]
        }
      ]
    });

    const tmpProject = [];
    let tmpUser = [];
    let managers = [];
    const data = [];

    for (let i = 0; i < userProjectRoles.length; i++) {
      const role = userProjectRoles[i];
      if (!role.projectId || !role.Project || tmpProject.includes(role.projectId)) continue;
      tmpProject.push(role.projectId);
      const users = role.Project.users || [];

      if (users.length > 0) {
        for (let j = 0; j < users.length; j++) {
          const user = users[j];
          if (user && !tmpUser.includes(user.userId)) {
            tmpUser.push(user.userId);
            managers.push({
              id: user.User && user.User.id,
              name: user.User && user.User.name,
              avatar: user.User && user.User.avatar
            });
          }
        }
      }
      data.push({
        project: role.Project.name,
        managers: managers
      });
      tmpUser = [];
      managers = [];
    }

    return jsonSuccess(data);
  }
}

export { ProjectRoleService };
