import uuidv1 from 'uuid/v1';
import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { paginate, getExtension } from '../utils/common';
import { Sale } from '../models/schema/Sale';
import { SaleCategory } from '../models/schema/SaleCategory';
import { SalePriority } from '../models/schema/SalePriority';
import { SaleStatus } from '../models/schema/SaleStatus';
import { User } from '../models/schema/User';
import { google } from 'googleapis';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { S3Service } from './S3Service';
import { sequelize } from '../core/boot';
import { Customer } from '../models/schema/Customer';

class SaleService {
  static async boot() {
    SaleService.DATA = {
      CALENDAR_SCOPES: getEnv('CALENDAR_SCOPES'),
      CALENDAR_CLIENT_EMAIL: getEnv('CALENDAR_CLIENT_EMAIL'),
      CALENDAR_PRIVATE_KEY: getEnv('CALENDAR_PRIVATE_KEY').replace(/\\n/g, '\n')
    };
  }

  static async createEventGGCalendar({
    company,
    note,
    appointmentPlace,
    appointmentStartDate,
    appointmentEndDate,
    username
  }) {
    return new Promise(resolve => {
      const jwt = new google.auth.JWT(
        SaleService.DATA.CALENDAR_CLIENT_EMAIL,
        null,
        SaleService.DATA.CALENDAR_PRIVATE_KEY,
        SaleService.DATA.CALENDAR_SCOPES
      );
      jwt.authorize((err, res) => {
        if (err) return resolve(jsonError(errors.FAIL_AUTH_GOOGLE_CALENDAR));
        const calendar = google.calendar({ version: 'v3', jwt });
        let event = {
          summary: company,
          description: note,
          location: appointmentPlace,
          start: {
            dateTime: new Date(appointmentStartDate),
            timeZone: 'Asia/Ho_Chi_Minh'
          },
          end: {
            dateTime: new Date(appointmentEndDate),
            timeZone: 'Asia/Ho_Chi_Minh'
          },
          attendees: [{ email: `${username}@bap.jp` }],
          guestsCanInviteOthers: false
        };

        calendar.events.insert({ auth: jwt, calendarId: 'primary', resource: event }, (err, event) => {
          if (err || !(event && event.data && event.data.id))
            return resolve(jsonError(errors.FAIL_TO_CREATE_EVENT_GOOGLE_CALENDAR));
          return resolve(jsonSuccess(event.data.id));
        });
      });
    });
  }

  static async createSale({
    customerId,
    categoryId,
    company,
    discussionContent,
    contact,
    priorityId,
    statusId,
    memoProgress,
    pic,
    memoMeeting,
    appointmentStartDate,
    appointmentPlace,
    otherMemo,
    actor,
    appointmentEndDate,
    note,
    files
  }) {
    if (customerId) {
      let customer = await Customer.findByPk(customerId);
      if (!customer) return jsonError(errors.NOT_FOUND_ERROR);
    }

    let saleCategory = await SaleCategory.findByPk(categoryId);
    if (!saleCategory) return jsonError(errors.NOT_FOUND_ERROR);

    let salePriority = await SalePriority.findByPk(priorityId);
    if (!salePriority) return jsonError(errors.NOT_FOUND_ERROR);

    let saleStatus = await SaleStatus.findByPk(statusId);
    if (!saleStatus) return jsonError(errors.NOT_FOUND_ERROR);

    let user = await User.findByPk(pic);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);

    let event = await SaleService.createEventGGCalendar({
      company,
      note,
      appointmentPlace,
      appointmentStartDate,
      appointmentEndDate,
      username: user.name
    });
    if (!event.success) return event;

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const sale = await Sale.create(
                {
                  eventId: event.result,
                  categoryId,
                  company,
                  discussionContent,
                  contact,
                  priorityId,
                  statusId,
                  memoProgress,
                  pic,
                  memoMeeting,
                  appointmentStartDate,
                  appointmentPlace,
                  otherMemo,
                  actor,
                  appointmentEndDate,
                  note,
                  customerId: customerId ? customerId : null
                },
                { transaction: t }
              );

              const result = await Sale.findByPk(sale.id, {
                include: [
                  { model: SaleCategory, as: 'saleCategory' },
                  { model: SalePriority, as: 'salePriority' },
                  { model: SaleStatus, as: 'saleStatus' },
                  {
                    model: User,
                    as: 'picAs',
                    attributes: ['id', 'name', 'avatar']
                  },
                  {
                    model: User,
                    as: 'actorAs',
                    attributes: ['id', 'name', 'avatar']
                  },
                  {
                    model: Customer,
                    as: 'customer',
                    attributes: ['id', 'name']
                  }
                ],
                transaction: t
              });

              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];

              if (files) {
                const tobeUploaded = Array.isArray(files) ? files : [files];

                for (let i = 0; i < tobeUploaded.length; i++) {
                  const file = tobeUploaded[i];
                  if (!file) return jsonError(errors.PARAM_INVALID);
                  const uploadParams = {
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${sale.id}-${file.md5}-${uuidv1()}.${getExtension(
                      file.mimetype
                    )}`,
                    fileContent: file.data,
                    realName: file.name,
                    contentType: file.mimetype
                  };
                  uploadParamsArray.push(uploadParams);
                  let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                  dataAttachments.push({
                    userId: actor,
                    url: filePath,
                    name: file.name ? file.name : 'untitled',
                    mimeType: file.mimetype,
                    attachmentTableId: sale.id,
                    attachmentTableType: ATTACHMENT_TYPE.SALE
                  });
                }
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let resultUpload = await S3Service.uploadFileToS3(uploadParams);
                  if (!resultUpload.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(resultUpload);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }

              return resolve(result);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async updateEventGGCalendar({
    eventId,
    company,
    note,
    appointmentPlace,
    appointmentStartDate,
    appointmentEndDate,
    username
  }) {
    return new Promise(resolve => {
      const jwt = new google.auth.JWT(
        SaleService.DATA.CALENDAR_CLIENT_EMAIL,
        null,
        SaleService.DATA.CALENDAR_PRIVATE_KEY,
        SaleService.DATA.CALENDAR_SCOPES
      );
      jwt.authorize((err, res) => {
        if (err) return resolve(jsonError(errors.FAIL_AUTH_GOOGLE_CALENDAR));
        const calendar = google.calendar({ version: 'v3', jwt });
        let event = {
          summary: company,
          description: note,
          location: appointmentPlace,
          start: {
            dateTime: new Date(appointmentStartDate),
            timeZone: 'Asia/Ho_Chi_Minh'
          },
          end: {
            dateTime: new Date(appointmentEndDate),
            timeZone: 'Asia/Ho_Chi_Minh'
          },
          attendees: [{ email: `${username}@bap.jp` }],
          guestsCanInviteOthers: false
        };

        calendar.events.patch({ auth: jwt, calendarId: 'primary', resource: event, eventId }, (err, event) => {
          if (err) return resolve(jsonError(errors.FAIL_TO_UPDATE_EVENT_GOOGLE_CALENDAR));
          return resolve(jsonSuccess());
        });
      });
    });
  }

  static async updateSale({
    customerId,
    saleId,
    categoryId,
    company,
    discussionContent,
    contact,
    priorityId,
    statusId,
    memoProgress,
    memoMeeting,
    pic,
    appointmentStartDate,
    appointmentPlace,
    otherMemo,
    actor,
    appointmentEndDate,
    note
  }) {
    if (customerId) {
      let customer = await Customer.findByPk(customerId);
      if (!customer) return jsonError(errors.NOT_FOUND_ERROR);
    }

    let sale = await Sale.findByPk(saleId);
    if (!sale) return jsonError(errors.NOT_FOUND_ERROR);

    let saleCategory = await SaleCategory.findByPk(categoryId);
    if (!saleCategory) return jsonError(errors.NOT_FOUND_ERROR);

    let salePriority = await SalePriority.findByPk(priorityId);
    if (!salePriority) return jsonError(errors.NOT_FOUND_ERROR);

    let saleStatus = await SaleStatus.findByPk(statusId);
    if (!saleStatus) return jsonError(errors.NOT_FOUND_ERROR);

    let user = await User.findByPk(pic);
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);

    let event = await SaleService.updateEventGGCalendar({
      eventId: sale.eventId,
      company,
      note,
      appointmentPlace,
      appointmentStartDate,
      appointmentEndDate,
      username: user.name
    });
    if (!event.success) return event;

    await sale.update({
      categoryId,
      company,
      discussionContent: discussionContent || discussionContent === null ? discussionContent : sale.discussionContent,
      contact: contact ? contact : sale.contact,
      priorityId,
      statusId,
      memoProgress: memoProgress || memoProgress === null ? memoProgress : sale.memoProgress,
      pic,
      memoMeeting: memoMeeting || memoMeeting === null ? memoMeeting : sale.memoMeeting,
      appointmentStartDate,
      appointmentPlace,
      otherMemo: otherMemo || otherMemo === null ? otherMemo : sale.otherMemo,
      actor,
      appointmentEndDate,
      note: note || note === null ? note : sale.note,
      updatedAt: new Date(),
      customerId: customerId ? customerId : customerId === null || customerId === '' ? null : sale.customerId
    });

    const updateSale = await Sale.findByPk(sale.id, {
      include: [
        { model: SaleCategory, as: 'saleCategory' },
        { model: SalePriority, as: 'salePriority' },
        { model: SaleStatus, as: 'saleStatus' },
        { model: User, as: 'picAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'actorAs', attributes: ['id', 'name', 'avatar'] },
        { model: Customer, as: 'customer', attributes: ['id', 'name'] }
      ]
    });
    return jsonSuccess(updateSale);
  }

  static async removeEventGGCalendar({ eventId }) {
    return new Promise(resolve => {
      const jwt = new google.auth.JWT(
        SaleService.DATA.CALENDAR_CLIENT_EMAIL,
        null,
        SaleService.DATA.CALENDAR_PRIVATE_KEY,
        SaleService.DATA.CALENDAR_SCOPES
      );
      jwt.authorize((err, res) => {
        if (err) return resolve(jsonError(errors.FAIL_AUTH_GOOGLE_CALENDAR));
        const calendar = google.calendar({ version: 'v3', jwt });
        calendar.events.delete({ auth: jwt, calendarId: 'primary', eventId }, (err, event) => {
          if (err) return resolve(jsonError(errors.FAIL_TO_REMOVE_EVENT_GOOGLE_CALENDAR));
          return resolve(jsonSuccess());
        });
      });
    });
  }

  static async removeSale({ saleId }) {
    let sale = await Sale.findByPk(saleId);
    if (!sale) return jsonError(errors.NOT_FOUND_ERROR);

    let event = await SaleService.removeEventGGCalendar({
      eventId: sale.eventId
    });
    if (!event.success) return event;

    sale = await sale.update({ deletedAt: new Date() });
    return jsonSuccess(sale);
  }

  static async getListSale({ filter, page, limit }) {
    let query = {
      deletedAt: null
    };
    let queryStatus = {};

    if (filter.customerId) query.customerId = filter.customerId;
    if (filter.categoryId) query.categoryId = filter.categoryId;
    if (filter.pic) query.pic = filter.pic;
    if (filter.priorityId) query.priorityId = filter.priorityId;
    if (filter.start && filter.end) {
      if (filter.start >= filter.end) return jsonError(errors.TIME_ERROR);
      query = Object.assign(query, {
        [Op.or]: [
          {
            appointmentStartDate: { [Op.gte]: filter.start },
            appointmentEndDate: { [Op.lte]: filter.end }
          },
          {
            appointmentStartDate: { [Op.lte]: filter.start },
            appointmentEndDate: { [Op.gte]: filter.end }
          },
          {
            [Op.and]: [
              { appointmentStartDate: { [Op.gte]: filter.start } },
              { appointmentStartDate: { [Op.lte]: filter.end } }
            ]
          },
          {
            [Op.and]: [
              { appointmentEndDate: { [Op.gte]: filter.start } },
              { appointmentEndDate: { [Op.lte]: filter.end } }
            ]
          }
        ]
      });
    } else if (filter.start) {
      query = Object.assign(query, {
        [Op.or]: [
          { appointmentStartDate: { [Op.gte]: filter.start } },
          { appointmentEndDate: { [Op.gte]: filter.start } }
        ]
      });
    } else if (filter.end) {
      query = Object.assign(query, {
        [Op.or]: [{ appointmentEndDate: { [Op.lte]: filter.end } }, { appointmentStartDate: { [Op.lte]: filter.end } }]
      });
    }

    if (filter.keywords) {
      query = Object.assign(query, {
        [Op.or]: [
          { company: { [Op.like]: `%${filter['keywords']}%` } },
          { discussionContent: { [Op.like]: `%${filter['keywords']}%` } }
        ]
      });
    }

    if (filter.statusId === -1) {
      queryStatus = { name: { [Op.ne]: 'closed' } };
    } else if (filter.statusId) {
      query.statusId = filter.statusId;
    }

    const include = [
      { model: SaleCategory, as: 'saleCategory' },
      { model: SalePriority, as: 'salePriority' },
      { model: SaleStatus, as: 'saleStatus', where: queryStatus },
      { model: User, as: 'picAs', attributes: ['id', 'name', 'avatar'] },
      { model: User, as: 'actorAs', attributes: ['id', 'name', 'avatar'] },
      { model: Customer, as: 'customer', attributes: ['id', 'name'] }
    ];

    return await paginate({
      model: Sale,
      query: { where: query, include },
      page,
      limit
    });
  }

  static async detailSale({ saleId }) {
    let sale = await Sale.findOne({
      where: { deletedAt: null, id: saleId },
      include: [
        { model: SaleCategory, as: 'saleCategory' },
        { model: SalePriority, as: 'salePriority' },
        { model: SaleStatus, as: 'saleStatus' },
        { model: User, as: 'picAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'actorAs', attributes: ['id', 'name', 'avatar'] }
      ]
    });
    if (!sale) return jsonError(errors.NOT_FOUND_ERROR);
    return jsonSuccess(sale);
  }

  static async addAttachment({ saleId, files, userId }) {
    const sale = await Sale.findByPk(saleId);
    if (!sale) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let attachments = [];
              const tobeUploaded = Array.isArray(files) ? files : [files];

              for (let i = 0; i < tobeUploaded.length; i++) {
                const file = tobeUploaded[i];
                if (!file) return jsonError(errors.PARAM_INVALID);
                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${saleId}-${file.md5}-${uuidv1()}.${getExtension(
                    file.mimetype
                  )}`,
                  fileContent: file.data,
                  realName: file.name,
                  contentType: file.mimetype
                };
                uploadParamsArray.push(uploadParams);
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                dataAttachments.push({
                  userId,
                  url: filePath,
                  name: file.name ? file.name : 'untitled',
                  mimeType: file.mimetype,
                  attachmentTableId: saleId,
                  attachmentTableType: ATTACHMENT_TYPE.SALE
                });
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                attachments = await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }
              return resolve(attachments);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeAttachment({ attachmentId }) {
    const attachment = await Attachment.findOne({
      where: {
        id: attachmentId,
        attachmentTableType: ATTACHMENT_TYPE.SALE
      }
    });
    if (!attachment) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const arrPath = attachment.url.split('/');

              await Attachment.destroy({
                where: {
                  id: attachmentId,
                  attachmentTableType: ATTACHMENT_TYPE.SALE
                },
                transaction: t
              });

              const removeS3 = await S3Service.removeFileS3({
                bucket: S3Service.DATA.S3_BUCKET,
                fileName: `${arrPath[1]}/${arrPath[2]}`
              });

              if (!removeS3.success) return reject(removeS3);
              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          return resolve(jsonSuccess(attachment));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async listAttachmentBySale({ saleId, limit, offset }) {
    let sale = await Sale.findByPk(saleId);
    if (!sale) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAndCountAll({
      where: {
        attachmentTableId: saleId,
        attachmentTableType: ATTACHMENT_TYPE.SALE
      },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(attachments);
  }
}

export { SaleService };
