import redis from 'redis';
import assert from 'assert';
import { promisify } from 'util';
import { jsonError, jsonSuccess, errors, logger } from '../utils/system';

class RedisService {
  static async boot() {
    const client = redis.createClient({
      host: getEnv('REDIS_HOST'),
      port: getEnv('REDIS_PORT'),
      prefix: getEnv('REDIS_PREFIX')
    });

    RedisService.redisGet = promisify(client.get).bind(client);
    RedisService.redisSet = promisify(client.set).bind(client);
    RedisService.redisDel = promisify(client.del).bind(client);
    RedisService.redisIncr = promisify(client.incr).bind(client);
    RedisService.redisTtl = promisify(client.ttl).bind(client);
    RedisService.redisExists = promisify(client.exists).bind(client);
    RedisService.redisSadd = promisify(client.sadd).bind(client);
    RedisService.redisSmembers = promisify(client.smembers).bind(client);
    RedisService.redisSpop = promisify(client.spop).bind(client);
    RedisService.redisScard = promisify(client.scard).bind(client);
    RedisService.redisSort = promisify(client.sort).bind(client);
    RedisService.redisClean = promisify(client.flushall).bind(client);
    RedisService.redisKeys = promisify(client.keys).bind(client);

    return await new Promise(resolve => {
      client.on('connect', function() {
        resolve(jsonSuccess());
      });

      client.on('error', function(err) {
        logger.error(err);
        resolve(jsonError(errors.SYSTEM_ERROR));
      });
    });
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async keys(key) {
    assert(key, 'Redis service - method keys - "key" is require');
    return RedisService.redisKeys(key);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async get(key) {
    assert(key, 'Redis service - method get - "key" is require');
    return RedisService.redisGet(key);
  }

  /**
   *
   * @param key
   * @param data
   * @param expiresIn
   * @returns {Promise<any>}
   */
  static async stringsSet(key, data, expiresIn) {
    assert(key, 'Redis service - method stringSet - "key" is require');
    assert(data, 'Redis service - method stringSet - "data" is require');
    return RedisService.redisSet(key, data, 'EX', expiresIn);
  }

  /**
   *
   * @param key
   * @param data
   * @returns {Promise<any>}
   */
  static async stringsSetNoExpires(key, data) {
    assert(key, 'Redis service - method stringsSetNoExpires - "key" is require');
    assert(data, 'Redis service - method stringsSetNoExpires - "data" is require');
    return RedisService.redisSet(key, data);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async delete(key) {
    assert(key, 'Redis service - method delete - "key" is require');
    return RedisService.redisDel(key);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async increment(key) {
    assert(key, 'Redis service - method increment - "key" is require');
    return RedisService.redisIncr(key);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async getKeyRemainingTime(key) {
    assert(key, 'Redis service - method getKeyRemainingTime - "key" is require');
    return RedisService.redisTtl(key);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async checkExist(key) {
    assert(key, 'Redis service - method checkExist - "key" is require');
    return RedisService.redisExists(key);
  }

  /**
   *
   * @param key
   * @param data
   * @returns {Promise<any>}
   */
  static async sadd(key, data) {
    assert(key, 'Redis service - method sadd - "key" is require');
    assert(data, 'Redis service - method sadd - "data" is require');
    return RedisService.redisSadd(key, data);
  }

  /**
   *
   * @param key
   * @returns {Promise<any>}
   */
  static async smembers(key) {
    assert(key, 'Redis service - method smembers - "key" is require');
    return RedisService.redisSmembers(key);
  }

  /**
   * Remove and return one or multiple random members from a set
   * @param key: redis key
   * @param amount: number of members
   * @return {Promise.<array>} array members removed
   */
  static async spop(key, amount) {
    assert(key, 'Redis service - method spop - "key" is require');
    assert(amount, 'Redis service - method spop - "amount" is require');
    return RedisService.redisSpop(key, amount);
  }

  static async scard(key) {
    assert(key, 'Redis service - method scard - "key" is require');
    return RedisService.redisScard(key);
  }

  /**
   *
   * @param key
   * @param sort
   * @param limit
   * @returns {Promise<any>}
   */
  static async sort(key, sort = 'DESC', limit = 5) {
    assert(key, 'Redis service - method sort - "key" is require');
    return RedisService.redisSort(key, sort, 'by', 'total_*', 'limit', 0, limit);
  }

  /**
   *
   * @returns {*}
   */
  static async clean() {
    return RedisService.redisClean();
  }
}

export { RedisService };
