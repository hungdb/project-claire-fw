import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess } from '../utils/system';
import { SalePriority } from '../models/schema/SalePriority';

class SalePriorityService {
  static async createSalePriority({ name, ordinalNumber }) {
    const condition = { where: { [Op.or]: [{ name }, { ordinalNumber }] } };

    const isSalePriorityExist = await SalePriority.findOne(condition);
    if (isSalePriorityExist) return jsonError(errors.DUPLICATED_ERROR);

    const salePriorityNew = await SalePriority.create({
      name,
      ordinalNumber
    });

    return jsonSuccess(salePriorityNew);
  }

  static async updateSalePriority({ salePriorityId, name, ordinalNumber }) {
    let salePriorityExist = await SalePriority.findByPk(salePriorityId);
    if (!salePriorityExist) return jsonError(errors.NOT_FOUND_ERROR);

    let dataUpdate = {
      name: name ? name : salePriorityExist.name,
      ordinalNumber: ordinalNumber ? ordinalNumber : salePriorityExist.ordinalNumber
    };

    if (name) {
      const condition = {
        where: {
          [Op.or]: [{ name }, { ordinalNumber }],
          id: { [Op.ne]: salePriorityId }
        }
      };

      let updateSalePriority = await SalePriority.findOne(condition);
      if (updateSalePriority) return jsonError(errors.DUPLICATED_ERROR);

      dataUpdate.name = name;
    }

    if (ordinalNumber) {
      const condition = {
        where: { ordinalNumber, id: { [Op.ne]: salePriorityId } }
      };

      let updateSalePriority = await SalePriority.findOne(condition);
      if (updateSalePriority) return jsonError(errors.DUPLICATED_ERROR);

      dataUpdate.ordinalNumber = ordinalNumber;
    }

    dataUpdate.updatedAt = new Date();
    salePriorityExist = await salePriorityExist.update(dataUpdate);

    return jsonSuccess(salePriorityExist);
  }

  static async removeSalePriority({ salePriorityId }) {
    let salePriorityExist = await SalePriority.findByPk(salePriorityId);
    if (!salePriorityExist) return jsonError(errors.NOT_FOUND_ERROR);

    const removeSalePriority = await salePriorityExist.destroy();
    if (!removeSalePriority) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess(salePriorityExist);
  }

  static async listSalePriority() {
    let salePriorities = await SalePriority.findAll();
    return jsonSuccess(salePriorities);
  }
}

export { SalePriorityService };
