import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { PermissionScheme } from '../models/schema/PermissionScheme';
import { ProjectRole } from '../models/schema/ProjectRole';
import { PermissionSchemeDetail } from '../models/schema/PermissionSchemeDetail';
import { Project } from '../models/schema/Project';
import { updateUserPermission } from '../utils/common';
import { rpcActions } from '../controllers/RPCControler';
import { sequelize } from '../core/boot';

class PermissionSchemeService {
  static async boot() {}

  static async createPermissionScheme({ name }) {
    let permissionScheme = await PermissionScheme.findOne({
      where: { name: name.trim() }
    });
    if (permissionScheme) return jsonError(errors.DUPLICATED_ERROR);
    permissionScheme = await PermissionScheme.create({
      name: name.trim()
    });
    return jsonSuccess(permissionScheme);
  }

  static async updatePermissionScheme({ permissionSchemeId, name }) {
    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);
    let updatePermissionScheme = await PermissionScheme.findOne({
      where: {
        name: name.trim(),
        id: { [Op.ne]: permissionSchemeId }
      }
    });
    if (updatePermissionScheme) return jsonError(errors.DUPLICATED_ERROR);
    permissionScheme = await permissionScheme.update({ name: name.trim() });
    return jsonSuccess(permissionScheme);
  }

  static async deletePermissionScheme({ permissionSchemeId }) {
    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    let projects = await Project.findAll({ where: { permissionSchemeId } });
    if (projects.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            PermissionSchemeDetail.destroy({
              where: { permissionSchemeId },
              transaction: t
            }),
            permissionScheme.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // await PermissionSchemeDetail.destroy({ where: { permissionSchemeId } });
    // await permissionScheme.destroy();
    // return jsonSuccess();
  }

  static async listPermissionScheme() {
    let permissionSchemes = await PermissionScheme.findAll({
      order: [['id', 'ASC']]
    });
    return jsonSuccess(permissionSchemes);
  }

  static async createPermissionSchemeDetail({ permissionSchemeId, projectRoleId, permission, permissionConditions }) {
    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);

    let allowPermissions = Object.keys(rpcActions);
    if (!allowPermissions.includes(permission)) return jsonError(errors.PARAM_INVALID);

    let permissionSchemeDetail = await PermissionSchemeDetail.findOne({
      where: {
        permissionSchemeId,
        projectRoleId,
        permission
      }
    });
    if (permissionSchemeDetail) return jsonError(errors.DUPLICATED_ERROR);

    permissionSchemeDetail = await PermissionSchemeDetail.create({
      permissionSchemeId,
      projectRoleId,
      permission,
      permissionCondition: permissionConditions ? permissionConditions : JSON.stringify([])
    });
    //-- update user permission when permission scheme detail changed
    updateUserPermission(permissionSchemeDetail);
    return jsonSuccess(permissionSchemeDetail);
  }

  static async updatePermissionSchemeDetail({
    permissionSchemeDetailId,
    permissionSchemeId,
    projectRoleId,
    permission,
    permissionConditions
  }) {
    let permissionSchemeDetail = await PermissionSchemeDetail.findByPk(permissionSchemeDetailId);
    if (!permissionSchemeDetail) return jsonError(errors.NOT_FOUND_ERROR);

    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);

    let allowPermissions = Object.keys(rpcActions);
    if (!allowPermissions.includes(permission)) return jsonError(errors.PARAM_INVALID);

    let updatedPermissionSchemeDetail = await PermissionSchemeDetail.findOne({
      where: {
        permission,
        permissionSchemeId,
        projectRoleId,
        id: { [Op.ne]: permissionSchemeDetailId }
      }
    });
    if (updatedPermissionSchemeDetail) return jsonError(errors.DUPLICATED_ERROR);

    permissionSchemeDetail = await permissionSchemeDetail.update({
      permission,
      permissionSchemeId: permissionSchemeId,
      projectRoleId: projectRoleId,
      permissionCondition: permissionConditions ? permissionConditions : JSON.stringify([])
    });
    //-- update user permission when permission scheme detail changed
    updateUserPermission(permissionSchemeDetail);
    return jsonSuccess(permissionSchemeDetail);
  }

  static async deletePermissionSchemeDetail({ permissionSchemeDetailId }) {
    let permissionSchemeDetail = await PermissionSchemeDetail.findByPk(permissionSchemeDetailId);
    if (!permissionSchemeDetail) return jsonError(errors.NOT_FOUND_ERROR);
    await permissionSchemeDetail.destroy();
    //-- update user permission when permission scheme detail changed
    updateUserPermission(permissionSchemeDetail);
    return jsonSuccess();
  }

  static async getPermissionScheme({ permissionSchemeId }) {
    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    permissionScheme.dataValues.listPermissions = await PermissionSchemeDetail.findAll({
      where: { permissionSchemeId }
    });
    return jsonSuccess(permissionScheme);
  }

  static async listAllPermissions() {
    //-- only list permission that has project depend condition
    let permissions = Object.keys(rpcActions).map(permissionName => ({
      permissionName,
      projectRelated:
        !!rpcActions[permissionName].permissionConditions &&
        !!rpcActions[permissionName].permissionConditions.project_depend
    }));

    return jsonSuccess(permissions);
  }

  static async listPermissionByPermissionSchemeAndProjectRole({ permissionSchemeId, projectRoleId }) {
    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    let projectRole = await ProjectRole.findByPk(projectRoleId);
    if (!projectRole) return jsonError(errors.NOT_FOUND_ERROR);

    let listPermissions = await PermissionSchemeDetail.findAll({
      where: { permissionSchemeId, projectRoleId }
    });
    return jsonSuccess(listPermissions);
  }
}

export { PermissionSchemeService };
