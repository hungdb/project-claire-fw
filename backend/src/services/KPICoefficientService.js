import { errors, jsonError, jsonSuccess } from '../utils/system';
import { KPICoefficient, KPI_TYPE } from '../models/schema/KPICoefficient';

class KPICoefficientService {
  static async boot() {
    let check = await KPICoefficient.findAll();
    if (check.length === 0) {
      let initData = [
        {
          data: JSON.stringify({
            percentSkillReview: 0.7,
            percentSkillTest: 0.3
          }),
          type: KPI_TYPE.KPI_Skill
        },
        {
          data: JSON.stringify({
            percentWorkManagement: 0.5,
            percentPeopleManagement: 0.25,
            percentDoProcess: 0.25
          }),
          type: KPI_TYPE.KPI_Leader
        },
        {
          data: JSON.stringify({
            percentTaskComplete: 0.7,
            percentResponsibility: 0.2,
            percentDedication: 0.1
          }),
          type: KPI_TYPE.KPI_Project
        }
      ];
      let init = await KPICoefficient.bulkCreate(initData);
      if (!init) return init;
      return jsonSuccess();
    }
  }

  static async updateKPICoefficient({ id, data }) {
    let check = await KPICoefficient.findByPk(id);
    let currentData = JSON.parse(check.data);
    if (!check) return jsonError(errors.NOT_FOUND_ERROR);
    let newData;
    switch (check.type) {
      case KPI_TYPE.KPI_Skill:
        newData = {
          percentSkillReview:
            data.percentSkillReview && !isNaN(data.percentSkillReview)
              ? data.percentSkillReview
              : currentData.percentSkillReview,
          percentSkillTest:
            data.percentSkillTest && !isNaN(data.percentSkillTest)
              ? data.percentSkillTest
              : currentData.percentSkillTest
        };
        break;
      case KPI_TYPE.KPI_Leader:
        newData = {
          percentWorkManagement:
            data.percentWorkManagement && !isNaN(data.percentWorkManagement)
              ? data.percentWorkManagement
              : currentData.percentWorkManagement,
          percentPeopleManagement:
            data.percentPeopleManagement && !isNaN(data.percentPeopleManagement)
              ? data.percentPeopleManagement
              : currentData.percentPeopleManagement,
          percentDoProcess:
            data.percentDoProcess && !isNaN(data.percentDoProcess)
              ? data.percentDoProcess
              : currentData.percentDoProcess
        };
        break;
      case KPI_TYPE.KPI_Project:
        newData = {
          percentTaskComplete:
            data.percentTaskComplete && !isNaN(data.percentTaskComplete)
              ? data.percentTaskComplete
              : currentData.percentTaskComplete,
          percentResponsibility:
            data.percentResponsibility && !isNaN(data.percentResponsibility)
              ? data.percentResponsibility
              : currentData.percentResponsibility,
          percentDedication:
            data.percentDedication && !isNaN(data.percentDedication)
              ? data.percentDedication
              : currentData.percentDedication
        };
        break;
    }

    check.data = JSON.stringify(newData);
    await check.save();
    return jsonSuccess();
  }

  static async getKPICoefficients() {
    let data = await KPICoefficient.findAll();
    data.map(item => {
      item.data = JSON.parse(item.data);
      return item;
    });
    return jsonSuccess(data);
  }

  static async getDetailKPICoefficient({ type }) {
    let item = await KPICoefficient.findOne({ where: { type } });
    if (!item) return jsonError(errors.NOT_FOUND_ERROR);
    item.data = JSON.parse(item.data);
    return jsonSuccess(item);
  }
}

export { KPICoefficientService };
