import AWS from 'aws-sdk';
import twilio from 'twilio';
import { parseJSONSafe } from '../utils/common';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Notification, NOTIFICATION_TYPE } from '../models/schema/Notification';
import { Project } from '../models/schema/Project';
import { Group } from '../models/schema/Group';
import { User } from '../models/schema/User';
import { Task } from '../models/schema/Task';
import { WorkflowState } from '../models/schema/WorkflowState';
import { CronService } from './CronService';
import { RedisService } from './RedisService';
import path from 'path';
import { sequelize } from '../core/boot';

const fs = require('fs');

class NotificationService {
  static async boot() {
    CronService.CONST = {
      LOG_FILE_PATH: path.join(__dirname, '../../', getEnv('LOG_FILE_PATH'))
    };

    AWS.config.update({
      accessKeyId: getEnv('AWS_ACCESS_KEY_ID'),
      secretAccessKey: getEnv('AWS_ACCESS_KEY_SECRET'),
      region: 'us-west-2'
    });

    const accountSid = getEnv('TWILIO_ACCOUNT_SID');
    const authToken = getEnv('TWILIO_ACCOUNT_AUTH_TOKEN');
    NotificationService.twilio = twilio(accountSid, authToken);
    NotificationService.TWILIO_PHONE_NUMBER = getEnv('TWILIO_PHONE_NUMBER');
    NotificationService.SES = new AWS.SES();

    NotificationService.callsInProgress = [];
    // NotificationService.SNS = new AWS.SNS({apiVersion: '2010-03-31'});
    // await NotificationService.SNS.setSMSAttributes({
    //   attributes: {
    //     'DefaultSMSType': 'Transactional', /* highest reliability */
    //   }
    // }).promise();

    return jsonSuccess();
  }

  static async sendSMS(message, phoneNumber) {
    // var params = {
    //   Message: message, /* required */
    //   PhoneNumber: phoneNumber,
    // };
    // var publishTextPromise = NotificationService.SNS.publish(params).promise();
    // publishTextPromise
    //   .then((data) => {
    //     logger.verbose("MessageID is " + data.MessageId);
    //   })
    //   .catch((err) => {
    //     logger.error(err);
    //   });
    return new Promise(resolve => {
      NotificationService.twilio.messages
        .create({
          from: NotificationService.TWILIO_PHONE_NUMBER,
          body: message,
          to: phoneNumber
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(error => {
          console.log(error);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        })
        .done();
    });
  }

  static endPhoneCall(sid) {
    let callIndex = NotificationService.callsInProgress.findIndex(c => c.sid === sid);
    if (callIndex >= 0) {
      logger.info('Ending phone call: ' + sid);
      //-- update to end the call
      let call = NotificationService.callsInProgress[callIndex];
      call.call.update({ status: 'completed' });
      NotificationService.callsInProgress = [
        ...NotificationService.callsInProgress.slice(0, callIndex),
        ...NotificationService.callsInProgress.slice(callIndex + 1)
      ];
    }
  }

  static makePhoneCall(message, phoneNumber) {
    let call = NotificationService.twilio.calls.create({
      url: 'http://demo.twilio.com/docs/voice.xml',
      to: phoneNumber,
      from: NotificationService.TWILIO_PHONE_NUMBER,
      statusCallback: getEnv('API_SERVER_URL') + '/callback/twilio',
      statusCallbackMethod: 'POST',
      statusCallbackEvent: ['answered']
    });
    call
      .then(call => {
        NotificationService.callsInProgress.push({ sid: call.sid, call });
        logger.info('Making phone call: ' + call.sid);
      })
      .catch(err => {
        logger.error(err);
      });
  }

  static async sendEmail(fromEmail, toEmails, ccEmails, subject, content) {
    var params = {
      Destination: {
        CcAddresses: ccEmails,
        ToAddresses: toEmails
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: content
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        }
      },
      Source: fromEmail
    };
    let sendPromise = NotificationService.SES.sendEmail(params).promise();
    return new Promise(resolve => {
      sendPromise
        .then(data => {
          logger.verbose('EmailId is ' + data.MessageId);
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async getNotificationsByUser({ userId, limit, offset }) {
    const promises = await Promise.all([
      Notification.findAndCountAll({
        where: { userId },
        order: [['id', 'DESC']],
        include: [
          { model: User, as: 'userAs', attributes: ['id', 'name', 'avatar'] },
          { model: User, as: 'actorAs', attributes: ['id', 'name', 'avatar'] },
          { model: Project, as: NOTIFICATION_TYPE.PROJECT },
          {
            model: Task,
            as: NOTIFICATION_TYPE.TASK,
            include: [{ model: WorkflowState }]
          }
        ],
        limit,
        offset
      }),
      Notification.count({
        where: { userId, isRead: false }
      })
    ]);
    promises[0].totalNotRead = promises[1];
    return jsonSuccess(promises[0]);
  }

  static async readNotification({ userId, notificationId }) {
    let notification = await Notification.findOne({
      where: { id: notificationId, userId }
    });
    if (!notification) return jsonError(errors.NOT_FOUND_ERROR);
    await notification.update({ isRead: true });
    return jsonSuccess(notification);
  }

  static async readAllNotifications({ userId }) {
    await Notification.update({ isRead: true }, { where: { userId } });
    return jsonSuccess();
  }

  static async toggleNotificationOfProject({ projectId, isEnabled }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    project = await project.update({ isNotificationEnable: isEnabled });
    return jsonSuccess(project);
  }

  static async createLog(data) {
    const logFile = CronService.CONST.LOG_FILE_PATH;
    return await new Promise(resolve => {
      fs.appendFile(logFile, data + ',', (err, data) => {
        if (err) return resolve(jsonError(errors.SYSTEM_ERROR));
        return resolve(jsonSuccess(data));
      });
    });
  }

  static async getLogsOfProject({ projectId }) {
    // const project = await Project.findByPk(projectId);
    // if (!project)
    //   return jsonError(errors.PROJECT_NOT_FOUND);

    const logFile = CronService.CONST.LOG_FILE_PATH;
    let buf = await fs.readFileSync(logFile);
    let res = [];
    if (buf.toString().length) {
      let data = '[' + buf.toString();
      data = data.slice(0, -1) + ']'; // format to array[object]
      res = JSON.parse(data);
      res = res.filter(obj => Number(obj.projectId) === projectId);
    }
    return jsonSuccess(res.reverse());
  }

  static async activeInactiveAlertSystem() {
    const status = !CronService.DATA.isActiveAlert;
    CronService.DATA.isActiveAlert = status;
    await RedisService.stringsSetNoExpires('CRON_DATA', JSON.stringify(CronService.DATA));
    return jsonSuccess({ status });
  }

  static getStateOfAlertSystem() {
    return jsonSuccess({ status: CronService.DATA.isActiveAlert });
  }

  static async getAccountDetail() {
    return new Promise(resolve => {
      NotificationService.twilio.api.v2010
        .accounts(getEnv('TWILIO_ACCOUNT_SID'))
        .fetch()
        .then(account => {
          return resolve(jsonSuccess(account));
        })
        .catch(e => {
          logger.error(e);
          resolve(jsonError(errors.SYSTEM_ERROR));
        })
        .done();
    });
  }

  static async getTwilioFile(fileUrl) {
    const twilioUrl = `https://api.twilio.com${fileUrl}`;
    return new Promise(resolve => {
      NotificationService.twilio
        .request({
          method: 'GET',
          uri: twilioUrl
        })
        .then(response => {
          const data = parseJSONSafe(response.body);
          if (data === null || response.statusCode !== 200) {
            logger.error(response);
            return resolve(jsonError(response));
          }
          return resolve(jsonSuccess(data));
        })
        .catch(e => {
          logger.error(e);
          resolve(jsonError(errors.SYSTEM_ERROR));
        })
        .done();
    });
  }

  static async toggleNotificationOfProjectGroup({ groupId, isEnabled }) {
    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.NOT_FOUND_ERROR);

    // await group.update({ isNotificationEnable: isEnabled });
    // await Project.update({ isNotificationEnable: isEnabled }, { where: { groupId } });
    // return jsonSuccess();

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            group.update({ isNotificationEnable: isEnabled }, { transaction: t }),
            Project.update({ isNotificationEnable: isEnabled }, { where: { groupId } }, { transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }
}

export { NotificationService };
