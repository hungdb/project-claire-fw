import moment from 'moment';
import uuidv1 from 'uuid/v1';
import _ from 'lodash';
import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Task, TASK_TYPE, TASK_PRIORITIES } from '../models/schema/Task';
import { Project } from '../models/schema/Project';
import { Comment, COMMENT_TYPE } from '../models/schema/Comment';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { EFFORT_TYPES, WorkflowState } from '../models/schema/WorkflowState';
import { TaskState } from '../models/schema/TaskState';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { User } from '../models/schema/User';
import { UserRole } from '../models/schema/UserRole';
import { Role } from '../models/schema/Role';
import { Workflow } from '../models/schema/Workflow';
import { WorkflowDetail } from '../models/schema/WorkflowDetail';
import { Allocation } from '../models/schema/Allocation';
import { Issue, ISSUE_STATUS } from '../models/schema/Issue';
import { S3Service } from './S3Service';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { Dependency } from '../models/schema/TaskDependency';
import { sequelize } from '../core/boot';
import {
  getExtension,
  paginate,
  durationHoursWork,
  transferToArrayValue,
  detectCycle,
  getGraphTaskDependencies,
  getRelateUsersOfTask,
  getRelateUsersOfTaskTerminated,
  // getTotalWorkingDayInMonth,
  getAbbreviationProjectName,
  workloadAllocations
} from '../utils/common';

class TaskService {
  static async boot() {}

  static async createTask({
    content,
    description,
    estimate,
    sprintId,
    projectId,
    tag,
    milestone,
    priority,
    dependencies,
    assignee,
    dueDate,
    files,
    userId,
    type,
    issueId,
    userRelate
  }) {
    const dataDependencies = [];
    const dataTask = {
      content,
      projectId,
      creator: userId,
      description,
      estimate,
      dueDate,
      tag: JSON.stringify(tag),
      // milestone: JSON.stringify(milestone),
      priority: priority ? priority : TASK_PRIORITIES.LOW,
      type: type ? type : TASK_TYPE.NORMAL
    };

    // if ((priority && (priority < 1 || priority > 3)) || (estimate && (estimate < 0 || isNaN(estimate))))
    //   return jsonError(errors.PARAM_INVALID);
    if (parseInt(type) === TASK_TYPE.BUG) {
      for (let i = 0; i < userRelate.length; i += 1) {
        const userRelateExist = await User.findByPk(userRelate[i]);
        if (!userRelateExist) return jsonError(errors.NOT_FOUND_ERROR);
      }
      dataTask.userRelate = JSON.stringify(userRelate);
    }

    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.PROJECT_NOT_FOUND);

    let task = await Task.findOne({
      where: { content, projectId, description }
    });
    if (task) return jsonError(errors.DUPLICATED_ERROR);

    let state = await WorkflowState.findOne({
      where: {
        workflowId: project.workflowSchemeId,
        effortType: EFFORT_TYPES.UNTOUCHED
      }
    });
    if (!state) return jsonError(errors.STATE_UNTOUCHED_NOT_FOUND);
    dataTask.state = state.id;

    const taskNo = project.indexOfTask + 1;
    const abbreviationProjectName = getAbbreviationProjectName(project.name);
    let taskCode = `${abbreviationProjectName}${projectId}-${taskNo}`;

    const taskCodeExists = await Task.findOne({ where: { taskCode } });
    if (taskCodeExists) return jsonError(errors.DUPLICATED_TASK_CODE);
    dataTask.taskCode = taskCode;

    if (sprintId) {
      let sprint = await Sprint.findByPk(sprintId);
      if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);
      dataTask.sprintId = sprintId;
    }

    if (issueId) {
      let checkIssue = await Task.findOne({ where: { issueId } });
      if (checkIssue) return jsonError(errors.DUPLICATED_ERROR);
      let issue = await Issue.findByPk(issueId);
      if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
      dataTask.issueId = issueId;
    }

    if (assignee) {
      const assigneeExist = await User.findByPk(assignee);
      if (!assigneeExist) return jsonError(errors.NOT_FOUND_ERROR);
      dataTask.assignee = assignee;
    } else {
      dataTask.assignee = userId;
    }

    if (dependencies && Array.isArray(dependencies)) {
      for (let i = 0; i < dependencies.length; i += 1) {
        const dependency = dependencies[i];
        const parent = await Task.findByPk(dependency);
        if (!parent) return jsonError(errors.NOT_FOUND_ERROR);
        dataDependencies.push({ parentId: dependency });
      }
    }

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const taskNew = await Task.create(dataTask, { transaction: t });

              if (dataTask.issueId) {
                await Issue.update(
                  { status: ISSUE_STATUS.SOLVING },
                  { where: { id: dataTask.issueId }, transaction: t }
                );
              }

              await project.update({ indexOfTask: taskNo }, { transaction: t });

              if (dataDependencies.length > 0) {
                const dataStoreDependencies = [];
                for (let j = 0; j < dataDependencies.length; j += 1) {
                  const dataDependency = dataDependencies[j];
                  dataDependency.taskId = taskNew.id;
                  dataStoreDependencies.push(dataDependency);
                }
                await Dependency.bulkCreate(dataStoreDependencies, {
                  transaction: t
                });
              }

              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];

              if (files) {
                const tobeUploaded = Array.isArray(files) ? files : [files];

                for (let i = 0; i < tobeUploaded.length; i++) {
                  const file = tobeUploaded[i];
                  let uploadParams = {
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${taskNew.id}-${
                      file.md5
                    }-${uuidv1()}.${getExtension(file.mimetype)}`,
                    fileContent: file.data,
                    realName: file.name,
                    contentType: file.mimetype
                  };
                  uploadParamsArray.push(uploadParams);
                  let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                  dataAttachments.push({
                    userId,
                    url: filePath,
                    name: file.name ? file.name : 'untitled',
                    mimeType: file.mimetype,
                    attachmentTableId: taskNew.id,
                    attachmentTableType: ATTACHMENT_TYPE.TASK
                  });
                }
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }

              return resolve(taskNew);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async updateSprintTask({ taskId, sprintId }) {
    const sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    const task = await Task.findOne({
      where: { id: taskId },
      include: [{ model: Project }, { model: Sprint }]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    if (task.projectId !== sprint.projectId)
      return jsonError(errors.NOT_BELONGS_TO_THE_SAME_PROJECT, {
        message: "Task and sprint doesn't belongs to the same project"
      });

    if (sprint.status === STATUS.COMPLETED) return jsonError(errors.SPRINT_COMPLETED);
    await task.update({ sprintId });

    return jsonSuccess();
  }

  static async updateSprintMultipleTasks({ type, sprintId }) {
    let query = {};
    let data = {};

    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    switch (type) {
      case 'add':
        query = { where: { sprintId: null, projectId: sprint.projectId } };
        data = { sprintId };
        break;
      case 'remove':
        query = {
          where: { sprintId },
          include: [
            {
              model: WorkflowState,
              attributes: ['effortType'],
              where: { effortType: EFFORT_TYPES.UNTOUCHED }
            }
          ]
        };
        data = { sprintId: null };
        break;
    }
    await Task.update(data, query);
    return jsonSuccess();
  }

  static async updateTaskToBacklog({ taskId }) {
    const task = await Task.findOne({
      where: { id: taskId },
      include: [{ model: Sprint }, { model: WorkflowState }, { model: Project }]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const status = task && task.Sprint && task.Sprint && task.Sprint.status;
    if (status === STATUS.COMPLETED) return jsonError(errors.SPRINT_COMPLETED);

    if (task.WorkflowState.effortType !== EFFORT_TYPES.UNTOUCHED) {
      return jsonError(errors.CANNOT_CHANGE_TASK_TO_BACKLOG);
    }
    const oldSprint = {
      id: task.Sprint && task.Sprint.id,
      name: task.Sprint && task.Sprint.name
    };
    await task.update({ sprintId: null });
    return jsonSuccess({ status, oldSprint });
  }

  static async updateBacklogTask({
    taskId,
    content,
    estimate,
    tag,
    description,
    milestone,
    priority,
    dependencies,
    assignee,
    dueDate,
    userId,
    type,
    userRelate
  }) {
    const dataUpdateTask = {
      dueDate,
      content,
      description
    };

    let task = await Task.findOne({
      where: { id: taskId },
      include: [
        { model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'assigneeAs', attributes: ['id', 'name', 'avatar'] },
        { model: Project },
        { model: Dependency, as: 'dependencies' }
      ]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    let updatedTask = await Task.findOne({
      where: {
        content,
        // sprintId: task.sprintId,
        projectId: task.projectId,
        description,
        id: { [Op.ne]: taskId }
      }
    });
    if (updatedTask) return jsonError(errors.DUPLICATED_ERROR);

    if (tag) dataUpdateTask.tag = JSON.stringify(tag);

    // if (milestone)
    //   dataUpdateTask.milestone = JSON.stringify(milestone);

    if ((estimate && estimate > 0) || estimate === 0) dataUpdateTask.estimate = estimate;

    if (priority) {
      // if (priority < 1 || priority > 3)
      //   return jsonError(errors.PARAM_INVALID);
      dataUpdateTask.priority = priority;
    }

    if (assignee) {
      const assigneeExist = await User.findByPk(assignee);
      if (!assigneeExist) return jsonError(errors.NOT_FOUND_ERROR);
      dataUpdateTask.assignee = assignee;
    }

    if (type) {
      dataUpdateTask.type = type;
      if (type === TASK_TYPE.NORMAL) {
        dataUpdateTask.userRelate = JSON.stringify([]);
      }
    }
    const isUpdateBugTask = (type && type === TASK_TYPE.BUG) || (!type && task.type === TASK_TYPE.BUG);
    if (isUpdateBugTask && userRelate) {
      for (let i = 0; i < userRelate.length; i += 1) {
        const userRelateExist = await User.findByPk(userRelate[i]);
        if (!userRelateExist) return jsonError(errors.NOT_FOUND_ERROR);
      }
      dataUpdateTask.userRelate = JSON.stringify(userRelate);
    }

    let toAddDependencies = [];
    let toRemoveDependencyIds = [];

    let currentDependencies = await Dependency.findAll({
      where: { taskId: task.id }
    });

    if (dependencies && Array.isArray(dependencies)) {
      const graph = await getGraphTaskDependencies({ taskId, dependencies });
      const checkDependency = detectCycle({ graph: graph });
      if (!checkDependency) return jsonError(errors.DEPENDENCIES_ERROR);

      for (let i = 0; i < dependencies.length; i += 1) {
        let dependency = await Dependency.findOne({
          where: { parentId: dependencies[i], taskId: task.id }
        });
        if (!dependency) {
          toAddDependencies.push({
            parentId: dependencies[i],
            taskId: task.id
          });
        }
      }
    }

    currentDependencies.forEach(dep => {
      if (!dependencies.includes(dep.parentId)) {
        toRemoveDependencyIds.push(dep.id);
      }
    });

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            task.update(dataUpdateTask, { transaction: t }),
            Dependency.bulkCreate(toAddDependencies, { transaction: t }),
            Dependency.destroy({
              where: { id: toRemoveDependencyIds },
              transaction: t
            })
          ]);
        })
        .then(result => {
          return resolve(jsonSuccess(result[0]));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async createTaskState({ taskId, toState, userId }) {
    let task = await Task.findByPk(taskId, {
      include: [
        { model: WorkflowState },
        {
          model: User,
          as: 'assigneeAs',
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        { model: Project }
      ]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);
    let stateTo = await WorkflowState.findByPk(toState);
    let workflow = await Workflow.findByPk(stateTo.workflowId);
    if (!workflow.allowParallelTask) {
      let checkExistTaskProgress = await Task.findOne({
        where: { assignee: userId },
        include: [{ model: WorkflowState, where: { effortType: EFFORT_TYPES.PROGRESS } }]
      });
      if (checkExistTaskProgress && stateTo.effortType === EFFORT_TYPES.PROGRESS)
        return jsonError(errors.ONLY_ONE_TASK_IS_PROGRESS);
    }

    if (task.state === toState) return jsonError(errors.NOT_ALLOW_MOVE_THIS_STATE);

    let parentTasks = await Dependency.findAll({
      where: { taskId: taskId },
      include: [
        {
          model: Task,
          as: 'parentTask',
          include: [{ model: WorkflowState }]
        }
      ]
    });
    let check = parentTasks.every(
      task => task.parentTask && task.parentTask.WorkflowState.effortType === EFFORT_TYPES.TERMINATED
    );
    if (!check) return jsonError(errors.PARENT_TASK_IS_NOT_TERMINATED);

    let project = await Project.findByPk(task.projectId);

    if (
      task.WorkflowState &&
      task.WorkflowState.effortType === EFFORT_TYPES.PROGRESS &&
      (task.assigneeAs && task.assigneeAs.isActive && task.assigneeAs.id !== userId)
    )
      return jsonError(errors.MUST_BE_CURRENT_ASSIGNEE);

    let allowStates = [];
    await WorkflowDetail.findAll({
      where: { workflowId: project.workflowSchemeId, fromState: task.state },
      attributes: ['toState']
    }).map(state => {
      allowStates.push(state.toState);
    });
    if (!allowStates.includes(parseInt(toState))) return jsonError(errors.NOT_ALLOW_MOVE_THIS_STATE);

    let taskState = await TaskState.create({
      taskId: taskId,
      fromState: task.state,
      toState: toState,
      editor: userId
    });

    let oldUser = task.assignee;
    taskState = await TaskState.findByPk(taskState.id, {
      include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
    });
    const dataUpdateTask = {
      state: toState,
      assignee: userId
    };

    if (taskState.toStates.effortType === EFFORT_TYPES.PROGRESS && !task.startDate) {
      dataUpdateTask.startDate = taskState.createdAt || new Date();
    } else {
      dataUpdateTask.endDate = taskState.createdAt || new Date();
    }

    if (taskState.fromStates.effortType === EFFORT_TYPES.PROGRESS) {
      if (task.endDate) {
        dataUpdateTask.totalTime = task.totalTime + durationHoursWork(task.endDate, dataUpdateTask.endDate);
      } else {
        dataUpdateTask.totalTime = durationHoursWork(task.startDate, dataUpdateTask.endDate);
      }
    }

    await task.update(dataUpdateTask);
    return jsonSuccess({ taskState, oldUser });
  }

  static async deleteTask({ taskId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAll({
      where: {
        attachmentTableId: taskId,
        attachmentTableType: ATTACHMENT_TYPE.TASK
      }
    });

    const attachmentIds = transferToArrayValue(attachments);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            TaskState.destroy({ where: { taskId }, transaction: t }),
            Dependency.destroy({
              where: { [Op.or]: [{ parentId: taskId }, { taskId: taskId }] },
              transaction: t
            }),
            Comment.destroy({
              where: {
                commentTableType: COMMENT_TYPE.TASK,
                commentTableId: taskId
              },
              transaction: t
            }),
            Attachment.destroy({
              where: {
                id: attachmentIds,
                attachmentTableType: ATTACHMENT_TYPE.TASK
              },
              transaction: t
            }),
            task.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          S3Service.removeAttachmentsS3({ attachments });
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async deleteTaskDependency({ taskId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const count = await Dependency.count({
      where: { [Op.or]: [{ parentId: taskId }, { taskId: taskId }] }
    });
    if (!count) return jsonSuccess();

    const remove = await Dependency.destroy({
      where: { [Op.or]: [{ parentId: taskId }, { taskId: taskId }] }
    });
    if (!remove) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess();
  }

  static async deleteTaskComments({ taskId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const count = await Comment.count({
      where: { commentTableType: COMMENT_TYPE.TASK, commentTableId: taskId }
    });
    if (!count) return jsonSuccess();

    const remove = await Comment.destroy({
      where: { commentTableType: COMMENT_TYPE.TASK, commentTableId: taskId }
    });
    if (!remove) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess();
  }

  static async deleteTaskAttachments({ taskId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAll({
      where: {
        attachmentTableId: taskId,
        attachmentTableType: ATTACHMENT_TYPE.TASK
      }
    });

    for (let i = 0; i < attachments.length; i += 1) {
      const attachment = attachments[i];
      const arrPath = attachment.url.split('/');
      const remove = await Attachment.destroy({
        where: { id: attachment.id, attachmentTableType: ATTACHMENT_TYPE.TASK }
      });
      if (!remove) return jsonError(errors.SYSTEM_ERROR);
      const result = await S3Service.removeFileS3({
        bucket: getEnv('S3_BUCKET'),
        fileName: `${arrPath[1]}/${arrPath[2]}`
      });
      if (!result.success) return result;
    }
    return jsonSuccess();
  }

  static async listTaskBySprintId({ sprintId }) {
    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    let tasks = await Task.findAll({ where: { sprintId } });
    return jsonSuccess(tasks);
  }

  static async listTaskBacklogByProject({ projectId, tags, limit, offset }) {
    let query = { projectId, sprintId: null };

    if (tags && tags.hasOwnProperty('length') && tags.length !== 0) {
      let queryTags = [];
      tags.map(tag => {
        queryTags.push({ tag: { [Op.like]: `%${tag}%` } });
      });
      query = Object.assign(query, { [Op.or]: queryTags });
    }

    const paginate = limit && offset ? { limit, offset } : {};

    const tasks = await Task.findAndCountAll({
      where: query,
      ...paginate,
      include: [
        { model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] },
        { model: Project },
        {
          model: Dependency,
          as: 'dependencies',
          include: [{ model: Task, as: 'parentTask' }]
        }
      ]
    });
    return jsonSuccess(tasks);
  }

  static async filterTaskBacklogByProject({ projectId, key }) {
    let query = { projectId, sprintId: null };
    if (key) {
      query = Object.assign(query, {
        [Op.or]: [
          { content: { [Op.like]: `%${key}%` } },
          { taskCode: { [Op.like]: `%${key}%` } },
          { tag: { [Op.like]: `%${key}%` } }
        ]
      });
    }
    const tasks = await Task.findAll({
      where: query,
      include: [{ model: Project }]
    });
    return jsonSuccess(tasks);
  }

  static async filterTasksOfProject({
    limit,
    page,
    userId,
    projectId,
    assignee,
    sprintId,
    status,
    startDate,
    priority,
    key,
    type
  }) {
    let query = { projectId };
    let queryWorkflowState = {};
    if (assignee) {
      query = Object.assign(query, { assignee });
    }
    // default get all, -1 get backlog
    if (sprintId) {
      if (sprintId === -1) sprintId = null;
      query = Object.assign(query, { sprintId });
    }
    if (status) {
      // status = -1 get task not closed
      if (status === -1) {
        queryWorkflowState = Object.assign(queryWorkflowState, {
          effortType: { [Op.ne]: EFFORT_TYPES.TERMINATED }
        });
      } else {
        query = Object.assign(query, { state: status });
      }
    }
    if (priority) {
      query = Object.assign(query, { priority });
    }
    if (type) {
      query = Object.assign(query, { type });
    }
    if (startDate) {
      const dateFilter = moment(startDate).format('YYYY-MM-DD');
      query = Object.assign(query, {
        startDate: {
          [Op.gte]: moment(`${dateFilter}T00:00:00.000`),
          [Op.lte]: moment(`${dateFilter}T23:59:59.000`)
        }
      });
    }
    if (key) {
      query = Object.assign(query, {
        [Op.or]: [
          { content: { [Op.like]: `%${key}%` } },
          { taskCode: { [Op.like]: `%${key}%` } },
          { tag: { [Op.like]: `%${key}%` } }
        ]
      });
    }
    const queryParams = {
      where: query,
      include: [
        { model: Sprint },
        { model: User, as: 'assigneeAs', attributes: ['id', 'name', 'avatar'] },
        { model: WorkflowState, where: queryWorkflowState },
        { model: Project },
        { model: Dependency, as: 'dependencies' }
      ],
      distinct: true
    };
    const result = await paginate({
      model: Task,
      query: queryParams,
      page,
      limit
    });
    if (!result.success) {
      return result;
    }
    return jsonSuccess(result.result);
  }

  static async addEstimate({ taskId, estimate }) {
    let task = await Task.findOne({
      where: { id: taskId },
      include: [{ model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] }, { model: Project }]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    task = await task.update({ estimate });
    return jsonSuccess(task);
  }

  static async getTaskById({ taskId }) {
    let task = await Task.findByPk(taskId, {
      include: [
        { model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'assigneeAs', attributes: ['id', 'name', 'avatar'] },
        { model: WorkflowState },
        { model: Sprint },
        {
          model: Dependency,
          as: 'dependencies',
          include: [{ model: Task, as: 'parentTask' }]
        },
        { model: Issue }
      ]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    task.dataValues.detail = await TaskState.findAll({
      where: { taskId: taskId },
      attributes: ['editor', 'createdAt'],
      include: [
        { model: User, attributes: ['id', 'name', 'avatar'] },
        { model: WorkflowState, as: 'fromStates', attributes: ['name'] },
        { model: WorkflowState, as: 'toStates', attributes: ['name'] }
      ],
      order: [['createdAt', 'ASC']]
    });
    // members
    if (
      task.WorkflowState.effortType === EFFORT_TYPES.HOLDING ||
      task.WorkflowState.effortType === EFFORT_TYPES.PROGRESS
    ) {
      let relateUsers = await getRelateUsersOfTask(taskId);
      task.dataValues.relateUsers = relateUsers.users;
    }
    if (task.WorkflowState.effortType === EFFORT_TYPES.TERMINATED) {
      task.dataValues.relateUsers = await getRelateUsersOfTaskTerminated(taskId);
    }
    return jsonSuccess(task);
  }

  static async getMilestoneSuggestions({ projectId }) {
    let listMilestones = [];
    const tasks = await Task.findAll({
      where: { projectId },
      attributes: ['milestone']
    });
    tasks.map(item => {
      let data = JSON.parse(item.milestone);
      listMilestones = _.union(listMilestones, data);
    });
    return jsonSuccess(listMilestones);
  }

  static async statisticalSprint({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    const sprint = await Sprint.findOne({
      where: { projectId, status: STATUS.ACTIVE }
    });

    let totalEstimateAllTasks = 0;
    let totalEstimateTaskCompleted = 0;

    if (sprint) {
      totalEstimateAllTasks = await Task.sum('estimate', {
        where: { sprintId: sprint.id }
      });

      let taskCompleted = await Task.findAll({
        where: { sprintId: sprint.id },
        include: [
          {
            model: WorkflowState,
            where: { effortType: EFFORT_TYPES.TERMINATED }
          }
        ]
      });

      await taskCompleted.map(task => {
        totalEstimateTaskCompleted += task.estimate;
      });

      sprint.dataValues.totalEstimateAllTasks = totalEstimateAllTasks || 0;
      sprint.dataValues.totalEstimateTaskCompleted = totalEstimateTaskCompleted;
    }
    return jsonSuccess(sprint);
  }

  static async statisticalTasksByStatus({ projectId }) {
    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let result = await Promise.all(
      Object.values(EFFORT_TYPES).map(async item => {
        let count = await Task.count({
          where: { projectId },
          include: [{ model: WorkflowState, where: { effortType: item } }]
        });
        return {
          status: item,
          total: count
        };
      })
    );

    return jsonSuccess(result);
  }

  //CRUD comment
  static async createComment({ taskId, content, userId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    let taskComment = await Comment.create({
      commentTableId: taskId,
      commentTableType: COMMENT_TYPE.TASK,
      content: content,
      userId: userId
    });

    const data = await Comment.findByPk(taskComment.id, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    return jsonSuccess(data);
  }

  static async listCommentsByTaskId({ taskId, limit, offset }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    let comments = await Comment.findAndCountAll({
      where: {
        commentTableId: taskId,
        commentTableType: COMMENT_TYPE.TASK
      },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(comments);
  }

  static async listCommentsByUserId({ userId, limit, offset }) {
    let comments = await Comment.findAndCountAll({
      where: { userId, commentTableType: COMMENT_TYPE.TASK },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(comments);
  }

  static async updateComment({ commentId, content }) {
    let comment = await Comment.findByPk(commentId, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    if (!comment) return jsonError(errors.NOT_FOUND_ERROR);

    await comment.update({ content });

    return jsonSuccess(comment);
  }

  static async deleteComment({ commentId }) {
    let comment = await Comment.findByPk(commentId);
    if (!comment) return jsonError(errors.NOT_FOUND_ERROR);

    const remove = await comment.destroy();
    if (!remove) return jsonError(errors.SYSTEM_ERROR);
    return jsonSuccess(comment);
  }

  static async listMyCurrentAllocationOfProject({ userId }) {
    let user = await User.findOne({
      where: { id: userId },
      attributes: ['id']
    });
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);
    let currentProjectByUserId = await Allocation.findAll({
      where: { userId: userId },
      attributes: ['id', 'effortPercent'],
      include: [
        {
          model: Sprint,
          where: { status: STATUS.ACTIVE },
          include: [
            {
              attributes: ['id', 'name'],
              model: Project
            }
          ]
        }
      ]
    });
    let data = [];
    let listSprints = [];
    for (let i = 0; i < currentProjectByUserId.length; i++) {
      if (!listSprints.includes(currentProjectByUserId[i].Sprint.id)) {
        listSprints.push(currentProjectByUserId[i].Sprint.id);
        data.push(currentProjectByUserId[i].dataValues.Sprint.dataValues);
      }
    }

    for (let i = 0; i < data.length; i++) {
      let allocations = await Allocation.findAll({
        where: { userId: userId, sprintId: data[i].id },
        attributes: ['id', 'effortPercent', 'startDate', 'endDate']
      });
      data[i].efforts = [...allocations];
    }
    //get data for progress bar (rate of tasks done)
    for (let i = 0; i < listSprints.length; i++) {
      let totalTimeCompleted = 0;
      let listAllTasksTerminated = await Task.findAll({
        where: { sprintId: listSprints[i] },
        include: [
          {
            model: WorkflowState,
            where: { effortType: EFFORT_TYPES.TERMINATED }
          }
        ]
      });
      for (let i = 0; i < listAllTasksTerminated.length; i++) {
        let allTaskState = await TaskState.findAll({
          where: { taskId: listAllTasksTerminated[i].id, editor: userId },
          include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
        });
        for (let j = 0; j < allTaskState.length - 1; j++) {
          if (allTaskState[j].toState === allTaskState[j + 1].fromState) {
            if (
              allTaskState[j] &&
              allTaskState[j].toStates &&
              allTaskState[j].toStates.effortType === EFFORT_TYPES.PROGRESS
            ) {
              totalTimeCompleted += durationHoursWork(allTaskState[j].createdAt, allTaskState[j + 1].createdAt);
            }
          }
        }
      }
      data[i].totalTimeCompleted = totalTimeCompleted;
    }

    return jsonSuccess(data || []);
  }

  static async listMyCurrentTasks({ userId }) {
    let listMyTasksProgress = await Task.findAll({
      where: { assignee: userId },
      include: [
        { model: WorkflowState, where: { effortType: EFFORT_TYPES.PROGRESS } },
        { model: Sprint, attributes: ['id', 'name'] },
        { model: Project, attributes: ['id', 'name'] },
        { model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'assigneeAs', attributes: ['id', 'name', 'avatar'] }
      ]
    });
    for (let i = 0; i < listMyTasksProgress.length; i++) {
      listMyTasksProgress[i].dataValues.listTaskState = await TaskState.findAll({
        where: { taskId: listMyTasksProgress[i].id },
        include: [
          { model: User, attributes: ['id', 'name', 'avatar'] },
          { model: WorkflowState, as: 'fromStates' },
          { model: WorkflowState, as: 'toStates' }
        ]
      });
    }
    return jsonSuccess(listMyTasksProgress);
  }

  static async listTaskOfAllUsers({ typeComparison, date, estimate, name, projectId }) {
    let queryEstimate = {};
    let queryWorkflowState = { effortType: EFFORT_TYPES.PROGRESS };
    let queryDate = {};
    let queryTaskByModelSprint = { status: STATUS.ACTIVE };
    let includeQueryProject = [];

    if (typeComparison) {
      const comparisonEstimate = typeComparison === 1 ? Op.lt : typeComparison === 2 ? Op.eq : Op.gt;
      queryEstimate = Object.assign(queryEstimate, {
        estimate: { [comparisonEstimate]: estimate }
      });
    }

    if (date) {
      const dateFilter = moment(date, ['DD/MM/YYYY']).format('YYYY-MM-DD');
      queryWorkflowState.effortType = { [Op.ne]: EFFORT_TYPES.UNTOUCHED };
      queryDate = Object.assign(queryDate, {
        [Op.or]: [
          {
            endDate: {
              [Op.gte]: moment(`${dateFilter}T00:00:00.000`),
              [Op.lte]: moment(`${dateFilter}T23:59:59.000`)
            }
          },
          {
            endDate: { [Op.eq]: null }
          }
        ]
      });
    }

    if (projectId) {
      queryTaskByModelSprint = Object.assign(queryTaskByModelSprint, { projectId });
      includeQueryProject = [
        ...includeQueryProject,
        {
          model: Allocation,
          as: 'allocationsAs',
          include: [
            {
              model: Sprint,
              where: { projectId },
              required: true
            }
          ],
          required: true
        }
      ];
    }

    const promises = await Promise.all([
      User.findAll({
        where: {
          id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') },
          isActive: true,
          name: { [Op.like]: `%${name}%` }
        },
        include: [
          {
            model: User,
            as: 'infoMentor',
            attributes: ['id', 'name', 'avatar', 'mentor']
          },
          {
            model: UserRole,
            as: 'roles',
            include: [
              {
                model: Role,
                where: { name: { [Op.ne]: getEnv('CUSTOMER_ROLE') } }
              }
            ],
            required: true
          },
          ...includeQueryProject
        ],
        attributes: ['id', 'name', 'avatar', 'mentor']
      }),
      Task.findAll({
        where: { ...queryEstimate, ...queryDate },
        include: [
          { model: WorkflowState, where: queryWorkflowState },
          {
            model: Sprint,
            attributes: ['id', 'name'],
            where: queryTaskByModelSprint
          },
          { model: Project, attributes: ['id', 'name'] },
          {
            model: User,
            as: 'creatorAs',
            attributes: ['id', 'name', 'avatar']
          },
          {
            model: User,
            as: 'assigneeAs',
            attributes: ['id', 'name', 'avatar'],
            where: { name: { [Op.like]: `%${name}%` } }
          }
        ]
      })
    ]);

    promises[0].forEach(user => (user.dataValues.tasks = promises[1].filter(task => task.assignee === user.id)));
    return jsonSuccess(promises[0]);
  }

  static async assignTask({ userId, taskId }) {
    let task = await Task.findOne({
      where: { id: taskId },
      include: [{ model: WorkflowState }, { model: Project }]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);
    if (task.WorkflowState.effortType === EFFORT_TYPES.PROGRESS)
      return jsonError(errors.NOT_ALLOW_ASSIGN_TASK_IN_THIS_STATE);

    let usersOfProject = await UserProjectRole.findAll({
      where: {
        projectId: task.projectId,
        deletedAt: { [Op.eq]: null }
      }
    });
    usersOfProject = usersOfProject.map(user => user.userId);
    const allowUsers = _.uniq(usersOfProject);
    if (!allowUsers.includes(userId)) return jsonError(errors.PARAM_INVALID);

    await task.update({
      assignee: userId
    });
    return jsonSuccess(
      await Task.findOne({
        where: { id: taskId },
        include: [
          { model: WorkflowState },
          { model: Project },
          {
            model: User,
            as: 'assigneeAs',
            attributes: ['id', 'name', 'avatar']
          }
        ]
      })
    );
  }

  static async listAttachmentByTask({ taskId, limit, offset }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAndCountAll({
      where: {
        attachmentTableId: taskId,
        attachmentTableType: ATTACHMENT_TYPE.TASK
      },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(attachments);
  }

  static async addAttachment({ taskId, files, userId }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let attachments = [];

              const tobeUploaded = Array.isArray(files) ? files : [files];

              for (let i = 0; i < tobeUploaded.length; i++) {
                const file = tobeUploaded[i];
                let uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${taskId}-${file.md5}-${uuidv1()}.${getExtension(
                    file.mimetype
                  )}`,
                  fileContent: file.data,
                  realName: file.name,
                  contentType: file.mimetype
                };
                uploadParamsArray.push(uploadParams);
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                dataAttachments.push({
                  userId,
                  url: filePath,
                  name: file.name ? file.name : 'untitled',
                  mimeType: file.mimetype,
                  attachmentTableId: taskId,
                  attachmentTableType: ATTACHMENT_TYPE.TASK
                });
              }

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                attachments = await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }
              return resolve(attachments);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeAttachment({ attachmentId }) {
    let attachment = await Attachment.findOne({
      where: {
        id: attachmentId,
        attachmentTableType: ATTACHMENT_TYPE.TASK
      }
    });
    if (!attachment) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const arrPath = attachment.url.split('/');

              await Attachment.destroy({
                where: {
                  id: attachmentId,
                  attachmentTableType: ATTACHMENT_TYPE.TASK
                },
                transaction: t
              });

              const removeS3 = await S3Service.removeFileS3({
                bucket: S3Service.DATA.S3_BUCKET,
                fileName: `${arrPath[1]}/${arrPath[2]}`
              });

              if (!removeS3.success) return reject(removeS3);
              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          return resolve(jsonSuccess(attachment));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async getUserOfTask({ taskId }) {
    const task = await Task.findByPk(taskId, {
      include: [
        { model: User, as: 'assigneeAs', attributes: ['id', 'name', 'avatar'] },
        { model: User, as: 'creatorAs', attributes: ['id', 'name', 'avatar'] }
      ]
    });
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);
    let members = [task.assigneeAs];
    let listIds = [task.assignee];
    if (task.creator !== task.assignee) {
      members.push(task.creatorAs);
      listIds.push(task.creator);
    }
    let listTaskStates = await TaskState.findAll({
      where: { taskId: taskId },
      attributes: ['editor'],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    listTaskStates.map(item => {
      if (!listIds.includes(item.editor)) {
        listIds.push(item.editor);
        members.push(item.User);
      }
    });
    return jsonSuccess(members);
  }

  static async updateTotalCommentsOfTask({ taskId, type = 'add' }) {
    let task = await Task.findByPk(taskId);
    if (!task) return jsonError(errors.NOT_FOUND_ERROR);
    let totalComments = type === 'sub' ? task.totalComments - 1 : task.totalComments + 1;
    if (!task.totalComments) {
      totalComments = await Comment.count({
        where: {
          commentTableId: taskId,
          commentTableType: COMMENT_TYPE.TASK
        }
      });
    }
    await task.update({ totalComments });
    return jsonSuccess(task);
  }

  static async getTaskPointOfUser({ userId, sprintId }) {
    // calculate total time estimated and completed tasks
    const totalTimeNormalTask = await TaskService.getTotalTimeCompleteTask({
      sprintId,
      userId,
      type: TASK_TYPE.NORMAL
    });

    const totalTimeEstimateTask = totalTimeNormalTask.totalTimeEstimated;
    const totalTimeCompleteTask = totalTimeNormalTask.totalTimeCompleted;

    // miss deadline
    const totalTimeMissDeadline =
      totalTimeCompleteTask > totalTimeEstimateTask ? totalTimeCompleteTask - totalTimeEstimateTask : 0;

    // calculate
    const totalTimeForErrorRelate = await TaskService.getTotalTimeCompleteTask({
      sprintId,
      userId,
      type: TASK_TYPE.BUG
    });
    const totalTimeFixBug = totalTimeForErrorRelate.totalTimeCompleted;

    // get working day rate
    // const time = new Date();
    // const totalWorkingDayInMonth = getTotalWorkingDayInMonth({
    //   year: time.getFullYear(),
    //   month: time.getMonth() + 1,
    //   endOfMonth: moment(time).endOf('month').format('DD'),
    // });
    // const workingDayRate = totalWorkingDayInMonth * 8 / 100;

    // workforce of user in sprint
    const allocations = await Allocation.findAll({
      where: { userId, sprintId }
    });
    const workingDayRate = workloadAllocations(allocations);
    const taskComplete = workingDayRate
      ? ((totalTimeEstimateTask - (Math.abs(totalTimeMissDeadline) + totalTimeFixBug)) / workingDayRate) * 100
      : 0;
    return {
      taskComplete,
      missDeadline: totalTimeMissDeadline,
      errorRelate: totalTimeFixBug
    };
  }

  /**
   * calculate total time estimated and total time completed tasks in sprint of user
   * @param userId
   * @param sprintId
   * @param type
   * @returns {Promise.<{totalTimeCompleted: number, totalTimeEstimated: number}>}
   */
  static async getTotalTimeCompleteTask({ userId, sprintId, type }) {
    let totalTimeCompleted = 0;
    let totalTimeEstimated = 0;

    // get all tasks in sprint
    let listAllTasksTerminated = await Task.findAll({
      where: { sprintId, type },
      include: [{ model: WorkflowState, where: { effortType: EFFORT_TYPES.TERMINATED } }]
    });
    for (let i = 0; i < listAllTasksTerminated.length; i += 1) {
      // get all task state by editor (userId)
      let allTaskState = await TaskState.findAll({
        where: { taskId: listAllTasksTerminated[i].id, editor: userId },
        include: [{ model: WorkflowState, as: 'fromStates' }, { model: WorkflowState, as: 'toStates' }]
      });

      // add time estimated
      if (allTaskState.length > 0) {
        totalTimeEstimated += listAllTasksTerminated[i].estimate;
      }

      // add time complete all tasks in sprint
      for (let j = 0; j < allTaskState.length - 1; j += 1) {
        if (
          allTaskState[j] &&
          allTaskState[j].toState === allTaskState[j + 1].fromState &&
          allTaskState[j].toStates &&
          allTaskState[j].toStates.effortType === EFFORT_TYPES.PROGRESS
        ) {
          totalTimeCompleted += durationHoursWork(allTaskState[j].createdAt, allTaskState[j + 1].createdAt);
        }
      }
    }
    return { totalTimeCompleted, totalTimeEstimated };
  }

  static async closeAllIssue({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let tasks = await Task.findAll({
      where: { projectId, issueId: { [Op.ne]: null } },
      include: [
        {
          model: WorkflowState,
          attributes: ['effortType'],
          where: { effortType: EFFORT_TYPES.TERMINATED }
        }
      ]
    });

    const idTasks = await tasks.map(item => item.issueId);
    await Issue.update({ status: ISSUE_STATUS.CLOSED }, { where: { id: idTasks } });
    return jsonSuccess();
  }
}

export { TaskService };
