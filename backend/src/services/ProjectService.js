import { Op } from 'sequelize';
import randomstring from 'randomstring';
import _ from 'lodash';
import uuidv1 from 'uuid/v1';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Group } from '../models/schema/Group';
import { Project, ACTIVE_STATUS } from '../models/schema/Project';
import { Environment } from '../models/schema/Environment';
import { ProjectRole } from '../models/schema/ProjectRole';
import { AlertSubscription } from '../models/schema/AlertSubscription';
import { Document } from '../models/schema/Document';
import { UserRole } from '../models/schema/UserRole';
import { Configuration } from '../models/schema/Configuration';
import { Sprint, STATUS } from '../models/schema/Sprint';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { Workflow } from '../models/schema/Workflow';
import { PermissionScheme } from '../models/schema/PermissionScheme';
import { User } from '../models/schema/User';
import { Role } from '../models/schema/Role';
import { WorkflowState } from '../models/schema/WorkflowState';
import { TaskState } from '../models/schema/TaskState';
import { Task } from '../models/schema/Task';
import { Allocation } from '../models/schema/Allocation';
import { combineRolesOfUser, getExtension, paginate } from '../utils/common';
import { KPISprint } from '../models/schema/KPISprint';
import { getKPICoefficient } from '../utils/common';
import { KPI_TYPE } from '../models/schema/KPICoefficient';
import { sequelize } from '../core/boot';
import { S3Service } from './S3Service';

class ProjectService {
  static async boot() {
    ProjectService.DATA = {
      S3_BUCKET: getEnv('S3_BUCKET'),
      IDENTITY_URL: getEnv('IDENTITY_URL'),
      SUPER_ADMIN_ROLE: getEnv('SUPER_ADMIN_ROLE')
    };

    // update created at for projects
    // const projects = await Project.findAll({
    //   include: [
    //     {
    //       model: Sprint, as: 'sprints', attributes: ['startDate'], order: [['id', 'ASC']]
    //     }
    //   ]
    // });
    // for (let i = 0; i < projects.length; i++) {
    //   let project = projects[i];
    //   if (!project.createdAt) {
    //     let sprints = project.sprints;
    //     if (sprints.length) {
    //       await project.update({
    //         createdAt: sprints[0].startDate
    //       });
    //     }
    //   }
    // }

    return jsonSuccess();
  }

  static async listMyGroups({ userId }) {
    //-- get all projects of current user
    const projectRoles = await UserProjectRole.findAll({
      where: { userId, deletedAt: { [Op.eq]: null } }
    });
    const projectIds = projectRoles.map(r => r.projectId);
    const projects = await Project.findAll({
      where: { id: { [Op.in]: projectIds } }
    });
    const projectGroupIds = projects.map(p => p.groupId);
    const groups = await Group.findAll({
      where: { id: { [Op.in]: projectGroupIds }, isActive: true }
    });
    return jsonSuccess(groups);
  }

  static async listGroups() {
    let groups = await Group.findAll();
    return jsonSuccess(groups);
  }

  static async getGroup({ groupId }) {
    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.NOT_FOUND_ERROR);
    group.dataValues.projects = await Project.findAll({
      where: { groupId }
    });
    return jsonSuccess(group);
  }

  static async createGroup({ name, isActive }) {
    let group = await Group.findOne({ where: { name } });
    if (group) return jsonError(errors.DUPLICATED_ERROR);
    group = await Group.create({ name, isActive });
    return jsonSuccess(group);
  }

  static async updateGroup({ groupId, name, isActive }) {
    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.GROUP_NOT_FOUND);
    let updatedGroup = await Group.findOne({
      where: { name, id: { [Op.ne]: groupId } }
    });
    if (updatedGroup) return jsonError(errors.DUPLICATED_ERROR);
    const data = {};
    if (name) data.name = name;
    if (isActive === true || isActive === false) data.isActive = isActive;
    updatedGroup = await group.update(data);
    return jsonSuccess(updatedGroup);
  }

  static async deleteGroup({ groupId }) {
    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.GROUP_NOT_FOUND);
    let projects = await Project.findAll({ where: { groupId } });
    if (projects.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);
    await group.destroy();
    return jsonSuccess();
  }

  static async toggleIsActiveGroup({ groupId }) {
    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.GROUP_NOT_FOUND);
    let isActivated = Boolean(group.isActive);
    // let listProjectsByGroupId = await Project.findAll({
    //   where: { groupId, isActive: isActivated }
    // });
    // listProjectsByGroupId.map( async (project) => {
    //   await project.update({isActive: !isActivated})
    // });
    group = await group.update({ isActive: !isActivated });
    return jsonSuccess(group);
  }

  static async getDetailProject({ projectId }) {
    let query = {
      where: { id: projectId },
      include: [
        { model: Group, required: false },
        {
          model: Sprint,
          as: 'sprints',
          attributes: ['id', 'name', 'status', 'startDate', 'endDate'],
          required: false
        },
        { model: Workflow, required: false },
        { model: PermissionScheme, required: false },
        { model: Environment, as: 'environment', required: false },
        {
          model: UserProjectRole,
          as: 'users',
          where: { deletedAt: { [Op.eq]: null } },
          attributes: ['userId'],
          include: [{ model: ProjectRole }, { model: User, attributes: ['id', 'name', 'avatar', 'isActive'] }],
          required: false
        }
      ]
    };

    let project = await Project.findOne(query);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    return jsonSuccess(project);
  }

  static async listProjects({ userId }) {
    let query = {
      include: [
        { model: Group },
        {
          model: Sprint,
          as: 'sprints',
          attributes: ['id', 'name', 'status', 'startDate', 'endDate']
        },
        { model: Workflow },
        { model: PermissionScheme }
      ]
    };

    if (userId) {
      const role = await UserRole.findOne({
        where: { userId },
        include: [{ model: Role }]
      });

      if (role && role.Role) {
        const myProjects = await UserProjectRole.findAll({
          where: { userId, deletedAt: { [Op.eq]: null } }
        }).map(projectRole => projectRole.projectId);
        const queryUser = {
          where: {
            id: { [Op.in]: myProjects },
            isActive: ACTIVE_STATUS.active
          }
        };
        query = Object.assign(queryUser, query);
      }
    }

    let projects = await Project.findAll(query).map(async project => {
      let environments = await Environment.findAll({
        where: { projectId: project.id }
      });
      let users = await UserProjectRole.findAll({
        where: { projectId: project.id, deletedAt: { [Op.eq]: null } },
        attributes: ['userId'],
        include: [{ model: ProjectRole }, { model: User, attributes: ['id', 'name', 'avatar'] }]
      });
      project.dataValues.environments = environments;
      project.dataValues.users = users;

      const leaders = users.map(user => {
        if (user && user.ProjectRole && user.ProjectRole.name && user.ProjectRole.name.toLowerCase() === 'leader') {
          return {
            name: user && user.User.name,
            avatar: user && user.User.avatar
          };
        }
      });
      project.dataValues.leaders = _.compact(leaders);
      return project;
    });
    return jsonSuccess(projects);
  }

  static async getProject({ projectId }) {
    let project = await Project.findByPk(projectId, {
      include: [{ model: Group }, { model: Workflow }, { model: PermissionScheme }]
    });
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    project.dataValues.userProjectRoles = await UserProjectRole.findAll({
      where: {
        projectId: project.id,
        deletedAt: { [Op.eq]: null }
      },
      include: [{ model: ProjectRole }, { model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    return jsonSuccess(project);
  }

  static async createProject({ name, groupId, description, permissionSchemeId, workflowSchemeId }) {
    let project = await Project.findOne({ where: { name } });
    if (project) return jsonError(errors.DUPLICATED_ERROR);

    let group = await Group.findByPk(groupId);
    if (!group) return jsonError(errors.NOT_FOUND_ERROR);

    let workflow = await Workflow.findByPk(workflowSchemeId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);

    let permissionScheme = await PermissionScheme.findByPk(permissionSchemeId);
    if (!permissionScheme) return jsonError(errors.NOT_FOUND_ERROR);

    project = await Project.create({
      name,
      description,
      permissionSchemeId,
      workflowSchemeId,
      groupId
    });

    return jsonSuccess(project);
  }

  static async updateProject({ projectId, name, isActive, groupId, projectRate }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    const data = {};
    if (name) {
      let checkIfExist = await Project.findOne({
        where: { name, id: { [Op.ne]: projectId } }
      });
      if (checkIfExist) return jsonError(errors.DUPLICATED_ERROR);

      data.name = name;
    }
    if (isActive || isActive === 0) {
      // const activeStatusValid = Object.values(ACTIVE_STATUS);
      // if (!activeStatusValid.includes(isActive))
      //   return jsonError(errors.PARAM_INVALID);
      data.isActive = isActive;
      if (data.isActive === ACTIVE_STATUS.finished) {
        data.finishedAt = new Date();
      }
    }

    if (groupId) {
      let group = await Group.findByPk(groupId);
      if (!group) return jsonError(errors.GROUP_NOT_FOUND);
      data.groupId = groupId;
    }

    if (projectRate && !isNaN(projectRate)) {
      // if (projectRate < 0) return jsonError(errors.PARAM_INVALID);
      data.projectRate = projectRate;
    }

    const projectUpdated = await project.update(data);
    if (projectUpdated && projectRate !== project.projectRate) {
      ProjectService.updateKpiSprintPointByProject({ projectRate, projectId });
    }
    return jsonSuccess();
  }

  static async updateKpiSprintPointByProject({ projectRate, projectId }) {
    let dataProjectServiceDATA = await getKPICoefficient(KPI_TYPE.KPI_Leader);
    // note case get coefficient failed
    if (dataProjectServiceDATA) {
      let kpiSprints = await KPISprint.findAll({ where: { projectId } });
      for (let i = 0; i < kpiSprints.length; i++) {
        const kpiSprint = kpiSprints[i];
        const point =
          (projectRate *
            (kpiSprint.workManagementPoint * dataProjectServiceDATA.percentWorkManagement +
              kpiSprint.peopleManagementPoint * dataProjectServiceDATA.percentPeopleManagement +
              kpiSprint.processPoint * dataProjectServiceDATA.percentDoProcess)) /
          100;
        await kpiSprint.update({
          point: +point.toFixed(5)
        });
      }
    }
    return jsonSuccess();
  }

  static async deleteProject({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    // await UserProjectRole.destroy({ where: { projectId: projectId } });
    // let environments = await Environment.findAll({where: {projectId: projectId}});
    // environments.forEach(async environment => {
    //   this.deleteEnvironment({envId: environment.id});
    // });

    // references
    let sprints = await Sprint.findAll({ where: { projectId } });
    if (sprints.length > 0) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            UserProjectRole.destroy({ where: { projectId }, transaction: t }),
            project.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // await project.destroy();
    // return jsonSuccess();
  }

  // static async listProjectsByUserId({userId}) {
  //   let user = await User.findOne({
  //     where: {id: userId},
  //     attributes: ['id'],
  //   });
  //   if (!user)
  //     return jsonError(errors.NOT_FOUND_ERROR);
  //   let listProjectByUserId = await UserProjectRole.findAll({
  //     where: {userId: userId},
  //     attributes: ['id'],
  //     include: [
  //       {model: Project},
  //       {model: ProjectRole}
  //     ]
  //   });
  //   return jsonSuccess(listProjectByUserId || []);
  // }

  static async getUsersOfProject({ projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let users = await UserProjectRole.findAll({
      where: { projectId, deletedAt: { [Op.eq]: null } },
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    let members = [];
    let listIds = [];
    users.map(user => {
      if (!listIds.includes(user.userId)) {
        listIds.push(user.userId);
        members.push(user);
      }
    });
    return jsonSuccess(members);
  }

  static async getUsersOfAllProject({ isActive }) {
    const query = {};
    if (isActive && Number.isInteger(isActive)) {
      // const activeStatusValid = Object.values(ACTIVE_STATUS);
      // if (!activeStatusValid.includes(isActive))
      //   return jsonError(errors.PARAM_INVALID);
      query.isActive = isActive;
    }
    let projects = await Project.findAll({
      where: query,
      include: [{ model: Sprint, as: 'sprints' }]
    });
    for (let i = 0; i < projects.length; i++) {
      let project = projects[i];
      let projectId = project.id;
      project.dataValues.sprintActive = project.sprints.length
        ? project.sprints.find(s => s.status === STATUS.ACTIVE)
        : null;
      let members = await UserProjectRole.findAll({
        where: { projectId, deletedAt: { [Op.eq]: null } },
        include: [
          {
            model: User,
            attributes: ['id', 'name', 'avatar'],
            where: { id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') } }
          },
          { model: ProjectRole }
        ]
      });

      members = combineRolesOfUser(members);

      for (let k = 0; k < members.length; k++) {
        let mem = members[k];
        let allocations = await Allocation.findOne({
          where: { userId: mem.id },
          include: [
            {
              model: Sprint,
              where: { projectId, status: STATUS.ACTIVE }
            }
          ]
        });
        mem.effort = allocations ? allocations.effortPercent : 0;
      }
      project.dataValues.members = members;
    }
    return jsonSuccess(projects);
  }

  static async workflowMigrate({ projectId, workflowId, workflowStateIds }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let workflow = await Workflow.findByPk(workflowId);
    if (!workflow) return jsonError(errors.NOT_FOUND_ERROR);

    if (project.workflowSchemeId === workflowId) return jsonError(errors.DUPLICATED_ERROR);

    let workflowStates = await WorkflowState.findAll({
      where: { workflowId: project.workflowSchemeId }
    });
    if (workflowStateIds.length !== workflowStates.length) {
      return jsonError(errors.PARAM_INVALID);
    }

    let replaceVal = [];
    for (let i = 0; i < workflowStateIds.length; i++) {
      const workflowStateId = workflowStateIds[i];
      const workflowState = await WorkflowState.findByPk(workflowStateId);
      if (!workflowState) return jsonError(errors.NOT_FOUND_ERROR);
      replaceVal[workflowStates[i].id] = workflowStateId;
    }

    let tasks = await Task.findAll({ where: { projectId } });

    tasks.forEach(async task => {
      task.update({ state: replaceVal[task.state] });
      let taskStates = await TaskState.findAll({ where: { taskId: task.id } });
      taskStates.forEach(taskState => {
        taskState.update({
          fromState: replaceVal[taskState.fromState],
          toState: replaceVal[taskState.toState]
        });
      });
    });
    project = await project.update({ workflowSchemeId: workflowId });
    return jsonSuccess(project);
  }

  // Alert system
  static async listEnvironments({ projectId }) {
    let condition = {};
    if (projectId) {
      let project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.NOT_FOUND_ERROR);
      condition.where = { projectId };
    }

    let environments = await Environment.findAll(condition);
    return jsonSuccess(environments);
  }

  static async getEnvironment({ envId, secretKey }) {
    let environment = await Environment.findOne({
      where: { id: envId, secretKey }
    });
    if (!environment) return jsonError(errors.NOT_FOUND_ERROR);
    environment.dataValues.configs = await Configuration.findAll({
      where: { environmentId: envId }
    });
    // environment.dataValues.healthCheckData = serverHCVar.find(x => x.id === envId);
    return jsonSuccess(environment);
  }

  static async createEnvironment({ name, serverHC, hcThresholdDown, hcThresholdUp, projectId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    let environment = await Environment.findOne({ where: { projectId, name } });

    if (environment) return jsonError(errors.DUPLICATED_ERROR);

    environment = await Environment.create({
      name,
      // serverHealthCheck: serverHC,
      // HcThresholdDown: hcThresholdDown,
      // HcThresholdUp: hcThresholdUp,
      secretKey: randomstring.generate(32),
      projectId: projectId
    });
    return jsonSuccess(environment);
  }

  static async updateEnvironment({ envId, name, serverHC, hcThresholdDown, hcThresholdUp }) {
    let environment = await Environment.findByPk(envId);
    if (!environment) jsonError(errors.NOT_FOUND_ERROR);

    let updateValues = {};
    if (serverHC !== null && serverHC !== undefined) {
      updateValues.serverHealthCheck = serverHC;
    }

    environment = await environment.update(updateValues);
    return jsonSuccess(environment);
  }

  static async deleteEnvironment({ envId }) {
    let environment = await Environment.findByPk(envId);
    if (!environment) return jsonError(errors.NOT_FOUND_ERROR);
    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            Configuration.destroy({
              where: { environmentId: envId },
              transaction: t
            }),
            environment.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // await Configuration.destroy({ where: { environmentId: envId } });
    // await environment.destroy();
    // return jsonSuccess();
  }

  static async listConfigurations({ envId }) {
    let environment = await Environment.findByPk(envId);
    if (!environment) return jsonError(errors.NOT_FOUND_ERROR);
    let configurations = await Configuration.findAll({
      where: { environmentId: envId }
    });
    return jsonSuccess(configurations);
  }

  static async createConfiguration({ envId, key, value }) {
    let environment = await Environment.findByPk(envId);
    if (!environment) return jsonError(errors.NOT_FOUND_ERROR);
    let configuration = await Configuration.findOne({
      where: { key, environmentId: envId }
    });
    if (configuration) return jsonError(errors.DUPLICATED_ERROR);
    configuration = await Configuration.create({
      key,
      value,
      environmentId: envId
    });
    return jsonSuccess(configuration);
  }

  static async updateConfiguration({ configId, key, value, envId }) {
    let configuration = await Configuration.findByPk(configId);
    if (!configuration) return jsonError(errors.NOT_FOUND_ERROR);
    let environment = await Environment.findByPk(envId);
    if (!environment) return jsonError(errors.NOT_FOUND_ERROR);
    let updatedConfiguration = await Configuration.findOne({
      where: {
        environmentId: envId,
        key,
        id: { [Op.ne]: configId }
      }
    });
    if (updatedConfiguration) return jsonError(errors.DUPLICATED_ERROR);

    configuration = await configuration.update({
      key,
      value,
      environmentId: envId
    });
    return jsonSuccess(configuration);
  }

  static async deleteConfiguration({ configId }) {
    let configuration = await Configuration.findByPk(configId);
    if (!configuration) return jsonError(errors.NOT_FOUND_ERROR);
    await configuration.destroy();
    return jsonSuccess();
  }

  static async listProjectEnvironments({ projectId }) {
    let environments = await Environment.findAll({
      where: { projectId }
    });
    return jsonSuccess(environments);
  }

  static async getUserAlertSubscriptions({ userId, projectId }) {
    let subscription = await AlertSubscription.findOne({
      where: { userId, projectId }
    });
    if (subscription) {
      subscription.environmentIds = JSON.parse(subscription.environmentIds);
      subscription.methodIds = JSON.parse(subscription.methodIds);
    }
    return jsonSuccess(subscription);
  }

  static async updateUserAlertSubscriptions({ userId, projectId, environmentIds, methodIds }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.PROJECT_NOT_FOUND);

    let user = await User.findByPk(userId);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    let subscription = await AlertSubscription.findOne({
      where: { userId, projectId }
    });

    let update = {};
    if (environmentIds !== null && environmentIds !== undefined) {
      update['environmentIds'] = JSON.stringify(environmentIds);
    }
    if (methodIds !== null && methodIds !== undefined) {
      update['methodIds'] = JSON.stringify(methodIds);
    }

    if (!subscription) {
      await AlertSubscription.create({
        userId,
        projectId,
        ...update
      });
    } else {
      await subscription.update(update);
    }

    return ProjectService.getUserAlertSubscriptions({ userId, projectId });
  }

  static async addAttachment({ projectId, files, userId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let attachments = [];
              const tobeUploaded = Array.isArray(files) ? files : [files];

              for (let i = 0; i < tobeUploaded.length; i++) {
                const file = tobeUploaded[i];
                if (!file) return jsonError(errors.PARAM_INVALID);
                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${projectId}-${file.md5}-${uuidv1()}.${getExtension(
                    file.mimetype
                  )}`,
                  fileContent: file.data,
                  realName: file.name,
                  contentType: file.mimetype
                };
                uploadParamsArray.push(uploadParams);
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                dataAttachments.push({
                  userId,
                  url: filePath,
                  name: file.name ? file.name : 'untitled',
                  mimeType: file.mimetype,
                  attachmentTableId: projectId,
                  attachmentTableType: ATTACHMENT_TYPE.PROJECT
                });
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                attachments = await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }
              return resolve(attachments);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeAttachment({ attachmentId }) {
    let attachment = await Attachment.findOne({
      where: {
        id: attachmentId,
        attachmentTableType: ATTACHMENT_TYPE.PROJECT
      }
    });
    if (!attachment) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const arrPath = attachment.url.split('/');

              await Attachment.destroy({
                where: {
                  id: attachmentId,
                  attachmentTableType: ATTACHMENT_TYPE.PROJECT
                },
                transaction: t
              });

              const removeS3 = await S3Service.removeFileS3({
                bucket: S3Service.DATA.S3_BUCKET,
                fileName: `${arrPath[1]}/${arrPath[2]}`
              });

              if (!removeS3.success) return reject(removeS3);
              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          return resolve(jsonSuccess({ attachment }));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async listAttachmentByProject({ projectId, name, limit, offset }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let query = {
      attachmentTableId: projectId,
      attachmentTableType: ATTACHMENT_TYPE.PROJECT
    };

    if (name) {
      query = { ...query, name: { [Op.like]: `%${name}%` } };
    }

    const attachmentQuery = {
      where: query,
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    };

    if (limit && (offset || offset === 0)) {
      attachmentQuery.limit = limit;
      attachmentQuery.offset = offset;
    }

    const attachments = await Attachment.findAndCountAll(attachmentQuery);
    return jsonSuccess(attachments);
  }

  // Documents
  static async createDocumentWiki({ projectId, userId, subject, content, parentId }) {
    let project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let document = await Document.findOne({ where: { projectId, subject } });
    if (document) return jsonError(errors.DUPLICATED_ERROR);

    if (parentId) {
      let parentDocument = await Document.findByPk(parentId);
      if (!parentDocument) return jsonError(errors.NOT_FOUND_ERROR);
      if (parentDocument.projectId !== projectId) return jsonError(errors.PARAM_INVALID);
    }

    document = await Document.create({
      projectId,
      userId,
      subject,
      content,
      parentId
    });

    return jsonSuccess(document);
  }

  static async updateDocumentWiki({ documentId, subject, content, parentId }) {
    const updateData = {};
    let document = await Document.findByPk(documentId);
    if (!document) return jsonError(errors.NOT_FOUND_ERROR);

    let updatedDocument = await Document.findOne({
      where: {
        projectId: document.projectId,
        subject,
        id: { [Op.ne]: documentId }
      }
    });
    if (updatedDocument) return jsonError(errors.DUPLICATED_ERROR);

    if (subject) updateData.subject = subject;
    if (content) updateData.content = content;

    if (parentId && document.parentId !== parentId) {
      let parentDocumentId = parentId;
      // Check parentId not cycle
      do {
        if (parentDocumentId === documentId) return jsonError(errors.DEPENDENCIES_ERROR);

        const parentDocument = await Document.findByPk(parentDocumentId);
        if (!parentDocument) return jsonError(errors.NOT_FOUND_ERROR);

        if (parentDocument.projectId !== document.projectId) return jsonError(errors.PARAM_INVALID);

        parentDocumentId = parentDocument.parentId;
      } while (parentDocumentId);

      updateData.parentId = parentId;
    } else if (parentId === null) {
      updateData.parentId = parentId;
    }

    document = await document.update(updateData);
    return jsonSuccess(document);
  }

  static async removeDocumentWiki({ documentId }) {
    let document = await Document.findByPk(documentId);
    if (!document) return jsonError(errors.NOT_FOUND_ERROR);

    const isChild = await Document.findOne({ where: { parentId: documentId } });
    if (isChild) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    await document.destroy();
    return jsonSuccess(document);
  }

  static async getDetailDocumentWiki({ documentId }) {
    const document = await Document.findByPk(documentId, {
      include: [
        { model: User, attributes: ['id', 'name', 'avatar'] },
        {
          model: Document,
          attributes: ['id', 'subject', 'content'],
          as: 'DocumentParent'
        }
      ]
    });

    if (!document) return jsonError(errors.NOT_FOUND_ERROR);

    return jsonSuccess(document);
  }

  static async listDocumentsByProject({ projectId, subject, limit, offset }) {
    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let query = { projectId };

    if (subject) {
      query = { ...query, subject: { [Op.like]: `%${subject}%` } };
    }

    const documentQuery = {
      where: query,
      order: [['createdAt', 'DESC']],
      include: [
        { model: User, attributes: ['id', 'name', 'avatar'] },
        {
          model: Document,
          attributes: ['id', 'subject', 'content'],
          as: 'DocumentParent'
        }
      ]
    };

    if (limit && (offset || offset === 0)) {
      documentQuery.limit = limit;
      documentQuery.offset = offset;
    }

    let listDocuments = await Document.findAndCountAll(documentQuery);
    return jsonSuccess(listDocuments);
  }

  static async getUsersAlertSubscriptionOfProject({ projectId }) {
    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    const users = await AlertSubscription.findAll({
      where: { projectId },
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    const members = [];
    const listIds = [];
    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      if (!listIds.includes(user.userId)) {
        listIds.push(user.userId);
        const environmentIds = JSON.parse(user.environmentIds);
        if (environmentIds && Array.isArray(environmentIds)) {
          user.environmentIds = await Environment.findAll({
            where: { id: environmentIds }
          });
        } else {
          user.environmentIds = [];
        }
        user.methodIds = JSON.parse(user.methodIds);
        members.push(user);
      }
    }
    return jsonSuccess(members);
  }

  static async findInfoEnvironmentIds(result) {
    return await Promise.all(
      result.map(async item => {
        let env = JSON.parse(item.environmentIds);
        let environments = await Environment.findAll({
          where: { id: { [Op.in]: env } }
        });
        return { ...item.dataValues, environments };
      })
    );
  }

  static async getListAlertSubscriptions({ page, limit, projectId, userId }) {
    let query = {};

    const include = [
      {
        model: Project,
        attributes: ['name']
      },
      {
        model: User,
        attributes: ['name']
      }
    ];

    if (projectId) {
      const project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.PROJECT_NOT_FOUND);
      query.projectId = projectId;
    }
    if (userId) {
      const user = await User.findByPk(userId);
      if (!user) return jsonError(errors.USER_NOT_FOUND);
      query.userId = userId;
    }
    if (page && limit) {
      const result = await paginate({
        model: AlertSubscription,
        query: { where: query, include },
        page,
        limit
      });
      if (!result.success) {
        return result;
      }

      const data = await ProjectService.findInfoEnvironmentIds(result.result.data);
      return jsonSuccess({ ...result.result, data });
    }

    let result = await AlertSubscription.findAll({
      where: query,
      include
    });

    const data = await ProjectService.findInfoEnvironmentIds(result);
    return jsonSuccess(data);
  }
}

export { ProjectService };
