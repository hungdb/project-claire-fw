import { errors, jsonError, jsonSuccess } from '../utils/system';
import { SaleCategory } from '../models/schema/SaleCategory';
import { Op } from 'sequelize';

class SaleCategoryService {
  static async boot() {}

  static async createSaleCategory({ name }) {
    let category = await SaleCategory.findOne({ where: { name: name.trim() } });
    if (category) return jsonError(errors.DUPLICATED_ERROR);

    category = await SaleCategory.create({ name: name.trim() });
    return jsonSuccess(category);
  }

  static async updateSaleCategory({ saleId, name }) {
    let category = await SaleCategory.findByPk(saleId);
    if (!category) return jsonError(errors.NOT_FOUND_ERROR);

    let checkIfExist = await SaleCategory.findOne({
      where: { name: name.trim(), id: { [Op.ne]: saleId } }
    });
    if (checkIfExist) return jsonError(errors.DUPLICATED_ERROR);

    category = await category.update({
      name: name.trim(),
      updatedAt: new Date()
    });
    return jsonSuccess(category);
  }

  static async removeSaleCategory({ saleId }) {
    let category = await SaleCategory.findByPk(saleId);
    if (!category) return jsonError(errors.NOT_FOUND_ERROR);

    await category.destroy();
    return jsonSuccess();
  }

  static async getListSaleCategory() {
    let saleCategories = await SaleCategory.findAll();
    return jsonSuccess(saleCategories);
  }
}

export { SaleCategoryService };
