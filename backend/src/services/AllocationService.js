import moment from 'moment';
import _ from 'lodash';
import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess } from '../utils/system';
import { Allocation } from '../models/schema/Allocation';
import { User } from '../models/schema/User';
import { Sprint } from '../models/schema/Sprint';
import { Project } from '../models/schema/Project';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { RedisService } from './RedisService';
import { UserSkill } from '../models/schema/UserSkill';
import { Skill } from '../models/schema/Skill';
import { customDataAllocation, getOverloadEffortPercentUsers, calculateLength } from '../utils/common';
import { UserRole } from '../models/schema/UserRole';
import { Role } from '../models/schema/Role';
import { inValidStartDateInSprint, inValidEndDateInSprint, subTimeDateString } from '../utils/common';
import { TYPE_USER_FILTER_ALLOCATION } from '../utils/type';
import { AllocationLabel } from '../models/schema/AllocationLabel';

class AllocationService {
  static async boot() {
    let data = await RedisService.get('ALLOCATION_DATA');

    AllocationService.DATA = data
      ? JSON.parse(data)
      : {
          allocationData: null
        };
    await AllocationService.storeAllAllocationToRedis();
    return jsonSuccess();
  }

  static async createAllocation({ sprintId, data }) {
    const allocationData = [];
    const sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    const userIds = data.map(item => item.userId);
    const listEffortOfUser = await Allocation.findAll({ where: { userId: userIds } });
    const overloadUsers = getOverloadEffortPercentUsers(listEffortOfUser, data);
    if (calculateLength(overloadUsers)) {
      const overloadUserIds = overloadUsers.map(item => item.userId);
      let users = await User.findAll({ where: { id: overloadUserIds }, attributes: ['id', 'name'] });
      if (!calculateLength(users) || users.length !== overloadUsers.length) return jsonError(errors.NOT_FOUND_ERROR);

      users = users.map(user => {
        const overloadUser = overloadUsers.find(item => item.userId === user.id);
        return `${user.name}(${overloadUser ? overloadUser.currentEffort : 0}%)`;
      });
      const message = `${users.join(', ')} ${users.length === 1 ? 'has' : 'have'} effort overload`;
      return jsonError(errors.EFFORT_OVERLOAD_DETECTED, { message });
    }

    for (let i = 0; i < data.length; i++) {
      const { userId, effortPercent, startDate, endDate, label } = data[i];
      const allocationItem = {
        userId,
        sprintId,
        effortPercent,
        startDate,
        endDate
      };

      if (label) {
        const labelExists = await AllocationLabel.findByPk(label);
        if (!labelExists) return jsonError(errors.ALLOCATION_LABEL_NOT_FOUND);
        allocationItem.label = label;
      }

      // if (effortPercent > 100)
      //   return jsonError(errors.PARAM_INVALID);

      const promises = await Promise.all([
        User.findByPk(userId),
        UserProjectRole.findOne({
          where: {
            userId,
            projectId: sprint.projectId,
            deletedAt: { [Op.eq]: null }
          }
        })
      ]);

      if (!promises[0]) return jsonError(errors.NOT_FOUND_ERROR);
      if (!promises[1]) return jsonError(errors.USER_NOT_EXISTS_IN_PROJECT);
      // let allocationExists = await Allocation.findOne({where: {userId: userId, sprintId: sprintId}});
      // if (allocationExists)
      //   return jsonError(errors.DUPLICATED_ERROR);

      // check time
      const inValidStartDate = inValidStartDateInSprint({
        startDate,
        startDateSprint: sprint.startDate,
        endDateSprint: sprint.endDate
      });
      if (inValidStartDate) return jsonError(errors.TIME_ERROR);
      const inValidEndDate = inValidEndDateInSprint({
        endDate,
        startDateSprint: sprint.startDate,
        endDateSprint: sprint.endDate
      });
      if (inValidEndDate) return jsonError(errors.TIME_ERROR);
      allocationData.push(allocationItem);
    }

    const allocations = await Allocation.bulkCreate(allocationData);
    return jsonSuccess(allocations);
  }

  static async updateAllocation({ allocationId, effortPercent, startDate, endDate, label }) {
    const allocation = await Allocation.findByPk(allocationId);
    if (!allocation) return jsonError(errors.ALLOCATION_NOT_FOUND);

    let sprint = await Sprint.findByPk(allocation.sprintId);
    if (!sprint) return jsonError(errors.SPRINT_NOT_FOUND);

    const allocationUpdateData = {};
    if (label) {
      const labelExists = await AllocationLabel.findByPk(label);
      if (!labelExists) return jsonError(errors.ALLOCATION_LABEL_NOT_FOUND);
      if (label !== allocation.label) allocationUpdateData.label = label;
    } else {
      allocationUpdateData.label = null;
    }

    if (startDate) {
      const inValidStartDate = inValidStartDateInSprint({
        startDate,
        startDateSprint: sprint.startDate,
        endDateSprint: sprint.endDate
      });
      if (inValidStartDate) return jsonError(errors.TIME_ERROR);
      if (subTimeDateString(startDate, allocation.startDate) !== 0) allocationUpdateData.startDate = startDate;
    }

    if (endDate) {
      const inValidEndDate = inValidEndDateInSprint({
        endDate,
        startDateSprint: sprint.startDate,
        endDateSprint: sprint.endDate
      });
      if (inValidEndDate) return jsonError(errors.TIME_ERROR);
      if (subTimeDateString(endDate, allocation.endDate) !== 0) allocationUpdateData.endDate = endDate;
    }
    if (startDate || endDate || effortPercent || effortPercent === 0) {
      //check effort percent
      let listEffortOfUser = await Allocation.findAll({
        where: { userId: allocation.userId, id: { [Op.ne]: allocationId } }
      });
      const newEffort = {
        userId: allocation.userId,
        effortPercent: effortPercent ? effortPercent : allocation.effortPercent,
        startDate: startDate ? startDate : allocation.startDate,
        endDate: endDate ? endDate : allocation.endDate
      };
      const overloadUsers = getOverloadEffortPercentUsers(listEffortOfUser, newEffort);
      if (calculateLength(overloadUsers)) {
        let user = await User.findByPk(allocation.userId, { attributes: ['id', 'name'] });
        if (!user) return jsonError(errors.NOT_FOUND_ERROR);

        const overloadUser = overloadUsers.find(item => item.userId === user.id);
        user = `${user.name}(${overloadUser ? overloadUser.currentEffort : 0}%)`;
        const message = `${user} has effort overload`;
        return jsonError(errors.EFFORT_OVERLOAD_DETECTED, { message });
      }

      if (effortPercent !== allocation.effortPercent)
        allocationUpdateData.effortPercent = effortPercent ? effortPercent : allocation.effortPercent;
    }

    if (Object.keys(allocationUpdateData).length !== 0) {
      await allocation.update(allocationUpdateData);
    }

    const allocationUpdated = await Allocation.findByPk(allocationId, {
      include: [
        { model: User, attributes: ['id', 'name', 'avatar', 'isActive'] },
        { model: AllocationLabel, as: 'labels', attributes: ['id', 'name'] }
      ]
    });
    return jsonSuccess(allocationUpdated);
  }

  static async deleteAllocation({ allocationId }) {
    let allocation = await Allocation.findByPk(allocationId);
    if (!allocation) return jsonError(errors.NOT_FOUND_ERROR);

    // let sprint = await Sprint.findByPk(allocation.sprintId);
    // if (!sprint)
    //   return jsonError(errors.NOT_FOUND_ERROR);

    await allocation.destroy();
    return jsonSuccess();
  }

  static async getListAllocationOfUser({ userId }) {
    let user = await User.findOne({
      where: { id: userId },
      attributes: ['id', 'name', 'avatar'],
      include: [{ model: User, as: 'infoMentor', attributes: ['id', 'name', 'avatar'] }]
    });
    if (!user) return jsonError(errors.NOT_FOUND_ERROR);
    let listAllocation = await Allocation.findAll({
      where: { userId },
      include: [
        {
          model: Sprint,
          include: [{ model: Project }]
        }
      ]
    });
    user.dataValues.allocations = customDataAllocation(listAllocation);
    // user.dataChart = this.processDataAllocation(customData);
    return jsonSuccess(user);
  }

  static async getListAllocationOfSprint({ sprintId }) {
    let sprint = await Sprint.findByPk(sprintId);
    if (!sprint) return jsonError(errors.NOT_FOUND_ERROR);

    sprint.dataValues.allocations = await Allocation.findAll({
      where: { sprintId: sprintId }
    });
    return jsonSuccess(sprint);
  }

  static async storeAllAllocationToRedis() {
    if (!AllocationService.DATA.allocationData) {
      AllocationService.DATA.allocationData = {};
    }
    let users = await User.findAll();
    for (let i = 0; i < users.length; i++) {
      let listAllocation = await Allocation.findAll({
        where: { userId: users[i].id },
        include: [
          {
            model: Sprint,
            include: [{ model: Project }]
          }
        ]
      });
      let timestamps = [];
      for (let j = 0; j < listAllocation.length; j++) {
        timestamps.push(listAllocation[j].startDate);
        timestamps.push(listAllocation[j].endDate);
      }
      timestamps.sort((a, b) => {
        return moment(a) < moment(b) ? -1 : 1;
      });
      let efforts = [];
      //-- calculate effort
      for (let j = 0; j < timestamps.length - 1; j++) {
        efforts.push(0);
      }
      for (let j = 0; j < timestamps.length - 1; j++) {
        //-- check effort between i and i+1
        listAllocation.forEach(effort => {
          if (
            moment(timestamps[j]) >= moment(effort.startDate) &&
            moment(timestamps[j + 1]) <= moment(effort.endDate)
          ) {
            efforts[j] += effort.effortPercent;
          }
        });
      }

      AllocationService.DATA.allocationData[users[i].id] = {
        timestamps: timestamps,
        efforts: efforts
      };
    }
    await RedisService.stringsSetNoExpires('ALLOCATION_DATA', JSON.stringify(AllocationService.DATA));
    return jsonSuccess(AllocationService.DATA.allocationData);
  }

  static async filterAllocation({
    startDate,
    endDate,
    effortPercent,
    skillId,
    limit,
    page,
    name,
    actor,
    projectId,
    type
  }) {
    let query = {};
    let queryRole = {};
    if (type === TYPE_USER_FILTER_ALLOCATION.DEFAULT) {
      query = Object.assign(query, {
        id: { [Op.ne]: getEnv('SUPER_ADMIN_ID') },
        isActive: true
      });
      queryRole = Object.assign(queryRole, {
        name: { [Op.ne]: getEnv('CUSTOMER_ROLE') }
      });
    }
    if (name) query = Object.assign(query, { name: { [Op.like]: `%${name}%` } });

    let usersData = await User.findAll({
      where: query,
      include: [
        {
          model: UserRole,
          as: 'roles',
          include: [
            {
              model: Role,
              where: queryRole
            }
          ],
          required: true
        }
      ],
      attributes: ['id', 'name', 'avatar']
    });

    let users;
    // check is super admin
    const check = await UserRole.findOne({
      where: { userId: actor },
      include: [{ model: Role, where: { name: getEnv('SUPER_ADMIN_ROLE') } }]
    });
    if (check) {
      users = usersData;
    } else {
      users = [];
      let projects = [];
      let data = await UserProjectRole.findAll({
        where: {
          userId: actor,
          deletedAt: { [Op.eq]: null }
        },
        attributes: ['projectId']
      });
      for (let i = 0; i < data.length; i++) {
        if (!projects.includes(data[i].projectId)) {
          projects.push(data[i].projectId);
        }
      }
      let usersOfProject = await UserProjectRole.findAll({
        where: {
          projectId: { [Op.in]: projects },
          deletedAt: { [Op.eq]: null }
        }
      });
      usersOfProject = usersOfProject.map(user => user.userId);
      // let x = (usersOfProject) => usersOfProject.filter((v, i) => usersOfProject.indexOf(v) === i);
      // const tmp = x(usersOfProject);
      const tmp = _.uniq(usersOfProject);
      for (let j = 0; j < tmp.length; j++) {
        const user = usersData.find(user => user.id === tmp[j]);
        if (user) users.push(user);
      }
    }

    const tmp = users;
    if (projectId) {
      users = [];
      const project = await Project.findByPk(projectId);
      if (!project) return jsonError(errors.NOT_FOUND_ERROR);
      let usersOfProject = await UserProjectRole.findAll({
        where: {
          projectId,
          deletedAt: { [Op.eq]: null }
        }
      });
      usersOfProject = usersOfProject.map(user => user.userId);
      const allUsers = _.uniq(usersOfProject);
      for (let m = 0; m < allUsers.length; m++) {
        const user = tmp.find(user => user.id === allUsers[m]);
        if (user) users.push(user);
      }
    }

    const tmpUsers = users;
    if (skillId) {
      users = [];
      const skill = await Skill.findByPk(skillId);
      if (!skill) return jsonError(errors.NOT_FOUND_ERROR);
      let usersHaveSkill = await UserSkill.findAll({
        where: { skillId },
        include: [{ model: User, as: 'user', attributes: ['id', 'name', 'avatar'] }]
      });
      usersHaveSkill = usersHaveSkill.map(user => user.userId);
      const tmp = _.uniq(usersHaveSkill);
      for (let n = 0; n < tmp.length; n++) {
        const user = tmpUsers.find(user => user.id === tmp[n]);
        if (user) users.push(user);
      }
    }

    if (
      (startDate && endDate && moment(startDate) >= moment(endDate)) ||
      (effortPercent && (effortPercent < 0 || effortPercent > 100))
    )
      return jsonError(errors.PARAM_INVALID);

    let listUsersByEffortPercent = [];
    if (effortPercent) {
      for (let i = 0; i < users.length; i++) {
        const dataAllocation = AllocationService.DATA.allocationData[users[i].id];
        const timestamps = dataAllocation.timestamps;
        const efforts = dataAllocation.efforts;
        if (!timestamps.length) {
          listUsersByEffortPercent.push(users[i]);
        } else {
          let newEfforts = [];
          if (moment(endDate).isSame(moment(timestamps[0]), 'day')) {
            newEfforts.push(0);
            newEfforts.push(efforts[0]);
          } else if (moment(startDate).isSame(moment(timestamps[timestamps.length - 1]), 'day')) {
            newEfforts.push(efforts[timestamps.length - 2]);
            newEfforts.push(0);
          } else if (
            moment(startDate).isAfter(moment(timestamps[timestamps.length - 1]), 'day') ||
            moment(endDate).isBefore(moment(timestamps[0]), 'day')
          ) {
            newEfforts.push(0);
          } else {
            let checkBegin = false;
            for (let k = 0; k < timestamps.length - 1; k++) {
              if (
                moment(startDate) < moment(timestamps[0]) &&
                moment(endDate) > moment(timestamps[timestamps.length - 1])
              ) {
                newEfforts = [0, ...efforts, 0];
                break;
              } else {
                if (moment(endDate).utc() === moment(timestamps[k])) {
                  newEfforts.push(efforts[k]);
                  break;
                }
                if (moment(startDate) < moment(timestamps[0]) && !checkBegin && moment(endDate) < timestamps[1]) {
                  newEfforts.push(0);
                  newEfforts.push(efforts[0]);
                  checkBegin = true;
                }
                if (moment(timestamps[k]) <= moment(startDate) && moment(startDate) <= moment(timestamps[k + 1])) {
                  newEfforts.push(efforts[k]);
                }
                if (moment(endDate) > moment(timestamps[k + 1]) && moment(startDate) < moment(timestamps[k])) {
                  newEfforts.push(efforts[k]);
                }
                if (k + 1 === timestamps.length - 1 && moment(endDate) > moment(timestamps[k + 1])) {
                  newEfforts.push(0);
                }
                if (moment(endDate) <= moment(timestamps[k + 1])) break;
              }
            }
          }
          for (let j = 0; j < timestamps.length - 1; j++) {
            let effortData = 100 - Math.max(...newEfforts);
            if (effortData >= effortPercent) {
              listUsersByEffortPercent.push(users[i]);
              break;
            }
          }
        }
      }
    } else {
      listUsersByEffortPercent = [...users];
    }
    // listUsersByEffortPercent.sort((a, b) => (a.id < b.id ? -1 : 1));
    listUsersByEffortPercent = _.orderBy(listUsersByEffortPercent, ['name'], 'asc');
    const offset = (page - 1) * limit;
    return jsonSuccess({
      users: listUsersByEffortPercent.slice(offset, limit + offset),
      total: listUsersByEffortPercent.length
    });
  }
}

export { AllocationService };
