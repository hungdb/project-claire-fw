import uuidv1 from 'uuid/v1';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Issue, ISSUE_STATUS } from '../models/schema/Issue';
import { Project } from '../models/schema/Project';
import { WorkflowState, EFFORT_TYPES } from '../models/schema/WorkflowState';
import { Task, TASK_TYPE } from '../models/schema/Task';
import { User } from '../models/schema/User';
import { Op } from 'sequelize';
import { paginate, getExtension, transferToArrayValue, calculateLength } from '../utils/common';
import { Comment, COMMENT_TYPE } from '../models/schema/Comment';
import { S3Service } from './S3Service';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { sequelize } from '../core/boot';

class IssueService {
  static async createIssue({ subject, content, userRelate, projectId, creator, estimate, assignee, files, typeTask }) {
    let number = 1;

    // if (estimate && (estimate < 0 || isNaN(estimate)))
    //   return jsonError(errors.PARAM_INVALID);

    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    const isIssueExist = await Issue.findOne({
      where: { subject, content, projectId }
    });
    if (isIssueExist) return jsonError(errors.DUPLICATED_ERROR);

    if (parseInt(typeTask) === TASK_TYPE.BUG) {
      for (let i = 0; i < userRelate.length; i += 1) {
        const userRelateExist = await User.findByPk(userRelate[i]);
        if (!userRelateExist) return jsonError(errors.NOT_FOUND_ERROR);
      }
      userRelate = JSON.stringify(userRelate);
    } else {
      userRelate = JSON.stringify([]);
    }

    if (assignee) {
      let isAssigneeExist = await User.findByPk(assignee);
      if (!isAssigneeExist) assignee = creator;
    } else {
      assignee = creator;
    }

    const issueLast = await Issue.findOne({
      where: { projectId },
      order: [['number', 'DESC']]
    });
    if (issueLast) number = issueLast.number + 1;

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const issue = await Issue.create(
                {
                  number,
                  subject,
                  content,
                  status: ISSUE_STATUS.OPEN,
                  userRelate,
                  projectId,
                  creator,
                  estimate,
                  assignee,
                  typeTask: typeTask ? typeTask : TASK_TYPE.NORMAL
                },
                { transaction: t }
              );

              const result = await Issue.findByPk(issue.id, {
                include: [
                  {
                    model: User,
                    as: 'user',
                    attributes: ['id', 'name', 'avatar']
                  }
                ],
                transaction: t
              });

              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];

              if (files) {
                const tobeUploaded = Array.isArray(files) ? files : [files];

                for (let i = 0; i < tobeUploaded.length; i++) {
                  const file = tobeUploaded[i];
                  if (!file) return jsonError(errors.PARAM_INVALID);
                  const uploadParams = {
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${issue.id}-${
                      file.md5
                    }-${uuidv1()}.${getExtension(file.mimetype)}`,
                    fileContent: file.data,
                    realName: file.name,
                    contentType: file.mimetype
                  };
                  uploadParamsArray.push(uploadParams);
                  let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                  dataAttachments.push({
                    userId: creator,
                    url: filePath,
                    name: file.name ? file.name : 'untitled',
                    mimeType: file.mimetype,
                    attachmentTableId: issue.id,
                    attachmentTableType: ATTACHMENT_TYPE.ISSUE
                  });
                }
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let resultUpload = await S3Service.uploadFileToS3(uploadParams);
                  if (!resultUpload.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(resultUpload);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }

              return resolve(result);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async checkRemoveIssue({ issueId }) {
    const issue = await Issue.findByPk(issueId, {
      include: [{ model: Task, as: 'task', attributes: ['id'] }]
    });
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    if (issue.task) return jsonError(errors.REMOVE_REFERENCED_RECORDS);
    return jsonSuccess();
  }

  static async updateIssue({ issueId, subject, content, status, userRelate, estimate, assignee, typeTask }) {
    const data = {};
    const include = [
      {
        model: User,
        as: 'user',
        attributes: ['id', 'name', 'avatar'],
        required: false
      },
      {
        model: Task,
        as: 'task',
        attributes: ['id', 'issueId'],
        include: [{ model: WorkflowState, attributes: ['effortType'] }],
        required: false
      },
      {
        model: User,
        as: 'assigneeAs',
        attributes: ['id', 'name', 'avatar'],
        required: false
      }
    ];
    let issue = await Issue.findByPk(issueId, { include });
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    const effortType = issue.task && issue.task.WorkflowState && issue.task.WorkflowState.effortType;
    if (status === ISSUE_STATUS.CLOSED && effortType && effortType !== EFFORT_TYPES.TERMINATED)
      return jsonError(errors.TASK_RELATE_MUST_TERMINATED);
    if (status === ISSUE_STATUS.OPEN && issue.task) return jsonError(errors.TASK_CREATED_FROM_ISSUE);

    if (estimate || estimate === 0) {
      // if (estimate < 0 || isNaN(estimate))
      //   return jsonError(errors.PARAM_INVALID);
      data.estimate = estimate;
    }

    if (assignee) {
      let isAssigneeExist = await User.findByPk(assignee);
      if (isAssigneeExist) {
        data.assignee = assignee;
      }
    }

    if (subject) data.subject = subject;
    if (content !== undefined) data.content = content;
    if (status) {
      // if (status !== ISSUE_STATUS.OPEN && status !== ISSUE_STATUS.CLOSED && status !== ISSUE_STATUS.SOLVING)
      //   return jsonError(errors.STATUS_ERROR);
      data.status = status;
    }

    if (typeTask) {
      data.typeTask = typeTask;
      if (typeTask === TASK_TYPE.NORMAL) {
        data.userRelate = JSON.stringify([]);
      }
    }
    const isUpdateBugTask = (typeTask && typeTask === TASK_TYPE.BUG) || (!typeTask && issue.typeTask === TASK_TYPE.BUG);
    if (isUpdateBugTask && userRelate) {
      for (let i = 0; i < userRelate.length; i += 1) {
        const userRelateExist = await User.findByPk(userRelate[i]);
        if (!userRelateExist) return jsonError(errors.NOT_FOUND_ERROR);
      }
      data.userRelate = JSON.stringify(userRelate);
    }
    await issue.update(data);
    issue = await Issue.findByPk(issueId, { include });
    return jsonSuccess(issue);
  }

  static async deleteIssue({ issueId }) {
    const issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAll({
      where: {
        attachmentTableId: issueId,
        attachmentTableType: ATTACHMENT_TYPE.ISSUE
      }
    });

    const attachmentIds = transferToArrayValue(attachments);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            Comment.destroy({
              where: {
                commentTableType: COMMENT_TYPE.ISSUE,
                commentTableId: issueId
              },
              transaction: t
            }),
            Attachment.destroy({
              where: {
                id: attachmentIds,
                attachmentTableType: ATTACHMENT_TYPE.ISSUE
              },
              transaction: t
            }),
            issue.destroy({ transaction: t })
          ]);
        })
        .then(() => {
          S3Service.removeAttachmentsS3({ attachments });
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async getIssue({ issueId }) {
    const issue = await Issue.findByPk(issueId, {
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['id', 'name', 'avatar'],
          required: false
        },
        {
          model: Task,
          as: 'task',
          attributes: ['state'],
          required: false
        },
        {
          model: User,
          as: 'assigneeAs',
          attributes: ['id', 'name', 'avatar'],
          required: false
        }
      ]
    });
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    return jsonSuccess(issue);
  }

  static async getListIssue({ limit, page, projectId, status, subject, creator }) {
    const include = [
      {
        model: User,
        as: 'user',
        attributes: ['id', 'name', 'avatar'],
        required: false
      },
      {
        model: Task,
        as: 'task',
        attributes: ['state'],
        include: [{ model: WorkflowState }],
        required: false
      },
      {
        model: User,
        as: 'assigneeAs',
        attributes: ['id', 'name', 'avatar'],
        required: false
      }
    ];
    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);

    let query = { projectId };

    const validStatus = Object.values(ISSUE_STATUS);
    if (status && validStatus.includes(status)) {
      query = Object.assign(query, { status });
    }

    if (subject) {
      query = Object.assign(query, { subject: { [Op.like]: `%${subject}%` } });
    }

    if (creator) {
      query = Object.assign(query, { creator });
    }

    return await paginate({
      model: Issue,
      query: { where: query, include },
      limit,
      page
    });
  }

  static async statisticalIssues({ projectId }) {
    let promises = await Promise.all([
      Issue.count({ where: { projectId, status: ISSUE_STATUS.OPEN } }),
      Issue.count({ where: { projectId, status: ISSUE_STATUS.CLOSED } }),
      Issue.count({ where: { projectId, status: ISSUE_STATUS.SOLVING } })
    ]);
    return jsonSuccess({
      open: promises[0],
      close: promises[1],
      solving: promises[2],
      all: promises[0] + promises[1] + promises[2]
    });
  }

  static async createCommentIssue({ issueId, content, userId }) {
    let issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    let issueComment = await Comment.create({
      content,
      userId,
      commentTableId: issueId,
      commentTableType: COMMENT_TYPE.ISSUE
    });

    const data = await Comment.findByPk(issueComment.id, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    return jsonSuccess(data);
  }

  static async listCommentIssueByIssueId({ issueId, limit, page }) {
    let issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    const offset = (page - 1) * limit;
    let comments = await Comment.findAndCountAll({
      where: { commentTableId: issueId, commentTableType: COMMENT_TYPE.ISSUE },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(comments);
  }

  static async listMyIssue({ userId, limit, page }) {
    const offset = (page - 1) * limit;
    let comments = await Comment.findAndCountAll({
      where: { userId, commentTableType: COMMENT_TYPE.ISSUE },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(comments);
  }

  static async updateCommentIssue({ commentIssueId, content }) {
    let comment = await Comment.findByPk(commentIssueId, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    if (!comment) return jsonError(errors.NOT_FOUND_ERROR);
    if (content) comment = await comment.update({ content });
    return jsonSuccess(comment);
  }

  static async deleteCommentIssue({ commentIssueId }) {
    let comment = await Comment.findByPk(commentIssueId, {
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });
    if (!comment) return jsonError(errors.NOT_FOUND_ERROR);

    const remove = await comment.destroy();
    if (!remove) return jsonError(errors.SYSTEM_ERROR);
    return jsonSuccess(comment);
  }

  static async deleteIssueAttachment({ issueId }) {
    const issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAll({
      where: {
        attachmentTableId: issueId,
        attachmentTableType: ATTACHMENT_TYPE.ISSUE
      }
    });
    for (let i = 0; i < attachments.length; i += 1) {
      const attachment = attachments[i];
      const arrPath = attachment.url.split('/');
      const remove = await Attachment.destroy({
        where: { id: attachment.id, attachmentTableType: ATTACHMENT_TYPE.ISSUE }
      });

      if (!remove) return jsonError(errors.SYSTEM_ERROR);
      const result = await S3Service.removeFileS3({
        bucket: getEnv('S3_BUCKET'),
        fileName: `${arrPath[1]}/${arrPath[2]}`
      });
      if (!result.success) return result;
    }
    return jsonSuccess();
  }

  static async updateTotalCommentsOfIssue({ issueId, type = 'add' }) {
    let issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);
    let totalComments = type === 'sub' ? issue.totalComments - 1 : issue.totalComments + 1;
    await issue.update({ totalComments });
    return jsonSuccess(issue);
  }

  static async deleteIssueComments({ issueId }) {
    const issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);

    const count = await Comment.count({
      where: { commentTableType: COMMENT_TYPE.ISSUE, commentTableId: issueId }
    });
    if (!count) return jsonSuccess();

    const remove = await Comment.destroy({
      where: { commentTableType: COMMENT_TYPE.ISSUE, commentTableId: issueId }
    });
    if (!remove) return jsonError(errors.SYSTEM_ERROR);

    return jsonSuccess();
  }

  static async createMultipleIssue({ data, projectId, creator }) {
    const issuesData = [];

    const project = await Project.findByPk(projectId);
    if (!project) return jsonError(errors.NOT_FOUND_ERROR);
    const issueLast = await Issue.findOne({
      where: { projectId },
      order: [['number', 'DESC']]
    });
    let number = issueLast ? issueLast.number + 1 : 1;

    for (let i = 0; i < data.length; i += 1) {
      const { subject, content = null, type, estimate, assignee, userRelate } = data[i];
      const issueItem = {
        subject,
        content,
        status: ISSUE_STATUS.OPEN,
        userRelate: JSON.stringify([]),
        projectId,
        creator
      };

      if (assignee) {
        let isAssigneeExist = await User.findOne({ where: { name: assignee } });
        if (!isAssigneeExist) {
          issueItem.assignee = creator;
        } else {
          issueItem.assignee = isAssigneeExist.id;
        }
      } else {
        issueItem.assignee = creator;
      }

      if (estimate || estimate === 0) {
        // if (estimate < 0 || isNaN(estimate))
        //   return jsonError(errors.PARAM_INVALID);
        issueItem.estimate = estimate;
      }

      if (type) {
        // if (type !== TASK_TYPE.NORMAL && type !== TASK_TYPE.BUG)
        //   return jsonError(errors.STATUS_ERROR);
        if (parseInt(type) === TASK_TYPE.BUG && calculateLength(userRelate)) {
          for (let i = 0; i < userRelate.length; i += 1) {
            const userRelateExist = await User.findByPk(userRelate[i]);
            if (!userRelateExist) return jsonError(errors.NOT_FOUND_ERROR);
          }
          issueItem.userRelate = JSON.stringify(userRelate);
        }
        issueItem.typeTask = type;
      }

      const isIssueExist = await Issue.findOne({
        where: { subject, content, projectId }
      });
      if (isIssueExist) return jsonError(errors.DUPLICATED_ERROR);

      const isIssueExistCurrent = issuesData.some(
        issue => issue && issue.subject === subject && issue.content === content
      );
      if (isIssueExistCurrent) return jsonError(errors.DUPLICATED_ERROR);

      issueItem.number = number;
      number += 1;

      issuesData.push(issueItem);
    }

    const issues = await Issue.bulkCreate(issuesData);
    return jsonSuccess(issues);
  }

  static async addAttachment({ issueId, files, userId }) {
    const issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let attachments = [];
              const tobeUploaded = Array.isArray(files) ? files : [files];

              for (let i = 0; i < tobeUploaded.length; i++) {
                const file = tobeUploaded[i];
                if (!file) return jsonError(errors.PARAM_INVALID);
                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${issueId}-${file.md5}-${uuidv1()}.${getExtension(
                    file.mimetype
                  )}`,
                  fileContent: file.data,
                  realName: file.name,
                  contentType: file.mimetype
                };
                uploadParamsArray.push(uploadParams);
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                dataAttachments.push({
                  userId,
                  url: filePath,
                  name: file.name ? file.name : 'untitled',
                  mimeType: file.mimetype,
                  attachmentTableId: issueId,
                  attachmentTableType: ATTACHMENT_TYPE.ISSUE
                });
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                attachments = await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }
              return resolve(attachments);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeAttachment({ attachmentId }) {
    const attachment = await Attachment.findOne({
      where: {
        id: attachmentId,
        attachmentTableType: ATTACHMENT_TYPE.ISSUE
      }
    });
    if (!attachment) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const arrPath = attachment.url.split('/');

              await Attachment.destroy({
                where: {
                  id: attachmentId,
                  attachmentTableType: ATTACHMENT_TYPE.ISSUE
                },
                transaction: t
              });

              const removeS3 = await S3Service.removeFileS3({
                bucket: S3Service.DATA.S3_BUCKET,
                fileName: `${arrPath[1]}/${arrPath[2]}`
              });

              if (!removeS3.success) return reject(removeS3);
              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          return resolve(jsonSuccess(attachment));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async listAttachmentByIssue({ issueId, limit, offset }) {
    let issue = await Issue.findByPk(issueId);
    if (!issue) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAndCountAll({
      where: {
        attachmentTableId: issueId,
        attachmentTableType: ATTACHMENT_TYPE.ISSUE
      },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }],
      limit,
      offset
    });
    return jsonSuccess(attachments);
  }
}

export { IssueService };
