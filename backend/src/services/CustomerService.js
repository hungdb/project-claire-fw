import uuidv1 from 'uuid/v1';
import { Op } from 'sequelize';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Sale } from '../models/schema/Sale';
import { Customer } from '../models/schema/Customer';
import { Market } from '../models/schema/Market';
import { Attachment, ATTACHMENT_TYPE } from '../models/schema/Attachment';
import { S3Service } from './S3Service';
import { sequelize } from '../core/boot';
import { getExtension } from '../utils/common';
import { User } from '../models/schema/User';
import { paginate, getS3FileName } from '../utils/common';

class CustomerService {
  static async createCustomer({ name, marketId, note, company, status, files, avatar }) {
    const isMarketExist = await Market.findByPk(marketId);
    if (!isMarketExist) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let avatarUrl = null;

              if (avatar) {
                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${'avatar-customer'}-${
                    avatar.md5
                  }-${uuidv1()}.${getExtension(avatar.mimetype)}`,
                  fileContent: avatar.data,
                  realName: avatar.name,
                  contentType: avatar.mimetype,
                  ACL: 'public-read'
                };
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                avatarUrl = filePath;
                let resultUpload = await S3Service.uploadFileToS3(uploadParams);
                if (!resultUpload.success) {
                  S3Service.removeFileS3({
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: filePath
                  });
                  return reject(resultUpload);
                }
              }

              let customer = await Customer.create(
                {
                  name,
                  marketId,
                  note,
                  company,
                  status,
                  avatar: avatarUrl
                },
                { transaction: t }
              );

              customer = await Customer.findByPk(customer.id, {
                include: [{ model: Market, as: 'market' }],
                transaction: t
              });

              if (files) {
                const tobeUploaded = Array.isArray(files) ? files : [files];

                for (let i = 0; i < tobeUploaded.length; i++) {
                  const file = tobeUploaded[i];
                  if (!file) return jsonError(errors.PARAM_INVALID);
                  const uploadParams = {
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${customer.id}-${
                      file.md5
                    }-${uuidv1()}.${getExtension(file.mimetype)}`,
                    fileContent: file.data,
                    realName: file.name,
                    contentType: file.mimetype,
                    ACL: 'public-read'
                  };
                  uploadParamsArray.push(uploadParams);
                  let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                  dataAttachments.push({
                    url: filePath,
                    name: file.name ? file.name : 'untitled',
                    mimeType: file.mimetype,
                    attachmentTableId: customer.id,
                    attachmentTableType: ATTACHMENT_TYPE.CUSTOMER
                  });
                }
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let resultUpload = await S3Service.uploadFileToS3(uploadParams);
                  if (!resultUpload.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(resultUpload);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }

              return resolve(customer);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async updateCustomer({ id, name, marketId, note, company, status, avatar }) {
    let customer = await Customer.findByPk(id);
    if (!customer) return jsonError(errors.NOT_FOUND_ERROR);

    if (marketId) {
      const isMarketExist = await Market.findByPk(marketId);
      if (!isMarketExist) return jsonError(errors.NOT_FOUND_ERROR);
    }

    let avatarOldUrl = customer.avatar;

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              let avatarNewUrl = null;

              if (avatar) {
                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${'avatar-customer'}-${
                    avatar.md5
                  }-${uuidv1()}.${getExtension(avatar.mimetype)}`,
                  fileContent: avatar.data,
                  realName: avatar.name,
                  contentType: avatar.mimetype,
                  ACL: 'public-read'
                };
                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                avatarNewUrl = filePath;
                let resultUpload = await S3Service.uploadFileToS3(uploadParams);
                if (!resultUpload.success) {
                  S3Service.removeFileS3({
                    bucket: S3Service.DATA.S3_BUCKET,
                    fileName: filePath
                  });
                  return reject(resultUpload);
                }
              }
              const dataUpdate = {
                name: name ? name : customer.name,
                marketId: marketId ? marketId : customer.marketId,
                note: note || note === null ? note : customer.note,
                company: company || company === null ? company : customer.company,
                status: status ? status : customer.status,
                avatar: avatar ? avatarNewUrl : avatarOldUrl
              };

              await customer.update(dataUpdate, { transaction: t });

              customer = await Customer.findByPk(id, {
                include: [
                  {
                    model: Market,
                    as: 'market'
                  }
                ],
                transaction: t
              });

              return resolve(customer);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          if (avatar) {
            S3Service.removeFileS3({
              bucket: S3Service.DATA.S3_BUCKET,
              fileName: getS3FileName(avatarOldUrl)
            });
          }
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeCustomer({ id }) {
    let customer = await Customer.findByPk(id);
    if (!customer) return jsonError(errors.NOT_FOUND_ERROR);

    const isCustomerInSale = await Sale.findOne({ where: { customerId: id } });
    if (isCustomerInSale) return jsonError(errors.ERROR_RESOURCE_DEPENDENCY);

    await customer.destroy();
    return jsonSuccess();
  }

  static async getListCustomer({ page, limit, marketId, status, name, company }) {
    let condition = [];

    if (marketId) {
      const market = await Market.findByPk(marketId);
      if (!market) return jsonError(errors.NOT_FOUND_ERROR);
      condition.push({ marketId });
    }

    if (status) condition.push({ status });

    if (name) condition.push({ name: { [Op.like]: `%${name}%` } });

    if (company) condition.push({ company: { [Op.like]: `%${company}%` } });

    const query = {
      where: condition,
      include: [
        {
          model: Market,
          as: 'market'
        }
      ]
    };

    if (page && limit) {
      return await paginate({ model: Customer, query, page, limit });
    }
    const customers = await Customer.findAll(query);
    return jsonSuccess(customers);
  }

  static async addAttachment({ customerId, files, userId }) {
    let customer = await Customer.findByPk(customerId);
    if (!customer) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const uploadParamsArray = [];
              const dataAttachments = [];
              const tmp = [];
              let attachments = [];
              const tobeUploaded = Array.isArray(files) ? files : [files];

              for (let i = 0; i < tobeUploaded.length; i++) {
                const file = tobeUploaded[i];

                const uploadParams = {
                  bucket: S3Service.DATA.S3_BUCKET,
                  fileName: `${S3Service.DATA.S3_ATTACHMENT_FOLDER}/${customerId}-${
                    file.md5
                  }-${uuidv1()}.${getExtension(file.mimetype)}`,
                  fileContent: file.data,
                  realName: file.name,
                  contentType: file.mimetype,
                  ACL: 'public-read'
                };

                uploadParamsArray.push(uploadParams);

                let filePath = `${uploadParams.bucket}/${uploadParams.fileName}`;
                dataAttachments.push({
                  userId,
                  url: filePath,
                  name: file.name ? file.name : 'untitled',
                  mimeType: file.mimetype,
                  attachmentTableId: customerId,
                  attachmentTableType: ATTACHMENT_TYPE.CUSTOMER
                });
              }

              if (dataAttachments.length !== uploadParamsArray.length) return reject(jsonError(errors.SYSTEM_ERROR));

              if (dataAttachments.length !== 0 && uploadParamsArray.length !== 0) {
                attachments = await Attachment.bulkCreate(dataAttachments, {
                  transaction: t
                });

                for (let j = 0; j < uploadParamsArray.length; j++) {
                  let uploadParams = uploadParamsArray[j];
                  let result = await S3Service.uploadFileToS3(uploadParams);
                  if (!result.success) {
                    S3Service.removeArrayFileS3({
                      bucket: S3Service.DATA.S3_BUCKET,
                      arrayFile: tmp
                    });
                    return reject(result);
                  }
                  tmp.push(uploadParams.fileName);
                }
              }

              return resolve(attachments);
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(data => {
          return resolve(jsonSuccess(data));
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async removeAttachment({ attachmentId }) {
    const attachment = await Attachment.findOne({
      where: {
        id: attachmentId,
        attachmentTableType: ATTACHMENT_TYPE.CUSTOMER
      }
    });
    if (!attachment) return jsonError(errors.NOT_FOUND_ERROR);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return new Promise(async (resolve, reject) => {
            try {
              const arrPath = attachment.url.split('/');

              await Attachment.destroy({
                where: {
                  id: attachmentId,
                  attachmentTableType: ATTACHMENT_TYPE.CUSTOMER
                },
                transaction: t
              });

              const removeS3 = await S3Service.removeFileS3({
                bucket: S3Service.DATA.S3_BUCKET,
                fileName: `${arrPath[1]}/${arrPath[2]}`
              });

              if (!removeS3.success) return reject(removeS3);

              return resolve();
            } catch (e) {
              return reject(e);
            }
          });
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });
  }

  static async getListAttachment({ customerId }) {
    let customer = await Customer.findByPk(customerId);
    if (!customer) return jsonError(errors.NOT_FOUND_ERROR);

    const attachments = await Attachment.findAll({
      where: {
        attachmentTableId: customerId,
        attachmentTableType: ATTACHMENT_TYPE.CUSTOMER
      },
      order: [['createdAt', 'DESC']],
      include: [{ model: User, attributes: ['id', 'name', 'avatar'] }]
    });

    return jsonSuccess(attachments);
  }
}

export { CustomerService };
