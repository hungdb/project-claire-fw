import { Op } from 'sequelize';
import moment from 'moment';
import { errors, jsonError, jsonSuccess, logger } from '../utils/system';
import { Behavior, TYPE_APPLY, TYPE_ACTION, TYPE_SUB_APPLY } from '../models/schema/Behavior';
import { Sprint, STATUS as SPRINT_STATUS } from '../models/schema/Sprint';
import { Allocation } from '../models/schema/Allocation';
import { LogBehaviorSprint, TYPE_LOG_BEHAVIOR } from '../models/schema/LogBehaviorSprint';
import { User } from '../models/schema/User';
import { KPIBehavior } from '../models/schema/KPIBehavior';
import { LogDetailBehavior, LOG_DETAIL_BEHAVIOR_TABLE_TYPE } from '../models/schema/LogDetailBehavior';
import { paginate, calcPointBehavior } from '../utils/common';
import { sequelize } from '../core/boot';
import { TYPE_GET_BEHAVIOR } from '../utils/type';

class BehaviorService {
  static async boot() {}

  static async listBehavior({ type, isApply }) {
    const query = {};
    if (type === TYPE_GET_BEHAVIOR.SPRINT)
      query.typeApply = [TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION, TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY];
    if (isApply === true || isApply === false) query.isApply = isApply;
    const behaviors = await Behavior.findAll(query);
    return jsonSuccess(behaviors);
  }

  static async addBehavior({
    activity,
    point,
    typeApply,
    typeAction,
    decider,
    note,
    isApply,
    startDate,
    endDate,
    description,
    typeSubApply
  }) {
    // const typeApplyValid = Object.values(TYPE_APPLY);
    // const typeActionValid = Object.values(TYPE_ACTION);
    // if (!typeApplyValid.includes(typeApply) || !typeActionValid.includes(typeAction))
    //   return jsonError(errors.PARAM_INVALID);

    const validTypeSubApply = Object.values(TYPE_SUB_APPLY.BEHAVIOR_GENERAL);
    if (typeApply === TYPE_APPLY.BEHAVIOR_GENERAL && !validTypeSubApply.includes(typeSubApply))
      return jsonError(errors.PARAM_INVALID);

    if (startDate && endDate) {
      if (new Date(startDate).getTime() >= new Date(endDate).getTime()) return jsonError(errors.PARAM_INVALID);
    }

    const behavior = await Behavior.create({
      activity,
      point,
      typeApply,
      typeAction,
      decider,
      note,
      isApply,
      startDate,
      endDate,
      description,
      typeSubApply
    });
    return jsonSuccess(behavior);
  }

  static async updateBehavior({
    behaviorId,
    activity,
    point,
    typeApply,
    typeAction,
    decider,
    note,
    isApply,
    startDate,
    endDate,
    description,
    typeSubApply
  }) {
    let behavior = await Behavior.findByPk(behaviorId);
    if (!behavior) return jsonError(errors.NOT_FOUND_ERROR);

    const validTypeSubApply = Object.values(TYPE_SUB_APPLY.BEHAVIOR_GENERAL);
    if (typeApply === TYPE_APPLY.BEHAVIOR_GENERAL && !validTypeSubApply.includes(typeSubApply))
      return jsonError(errors.PARAM_INVALID);

    if (startDate && endDate) {
      if (new Date(startDate).getTime() >= new Date(endDate).getTime()) return jsonError(errors.PARAM_INVALID);
    }

    const data = {
      decider: decider || decider === null ? decider : behavior.decider,
      note: note || note === null ? note : behavior.note,
      activity: activity ? activity : behavior.activity,
      typeApply: typeApply ? typeApply : behavior.typeApply,
      isApply: isApply === true || isApply === false ? isApply : behavior.isApply,
      description: description || description === null ? description : behavior.description,
      typeSubApply: typeSubApply ? typeSubApply : behavior.typeSubApply,
      startDate: startDate ? startDate : behavior.startDate,
      endDate: endDate ? endDate : behavior.endDate
    };

    if (
      (!isNaN(point) && point !== behavior.point && point !== behavior.point) ||
      (typeAction && typeAction !== behavior.typeAction)
    ) {
      const references = await LogDetailBehavior.findOne({
        where: { behaviorId }
      });
      if (references) return jsonError(errors.BEHAVIOR_USED_TO_EVALUATE);
      if (point) data.point = point;
      if (typeAction) data.typeAction = typeAction;
    }

    behavior = await behavior.update(data);
    return jsonSuccess(behavior);
  }

  static async deleteBehavior({ behaviorId }) {
    const behavior = await Behavior.findByPk(behaviorId);
    if (!behavior) return jsonError(errors.NOT_FOUND_ERROR);
    const references = await LogDetailBehavior.findOne({
      where: { behaviorId }
    });
    if (references) return jsonError(errors.REMOVE_REFERENCED_RECORDS);

    await behavior.destroy();
    return jsonSuccess(behavior);
  }

  static async addLogBehaviorSprint({ actor, userId, sprintId, behaviors }) {
    const logBehaviorSprint = [];

    const sprint = await Sprint.findOne({
      where: { id: sprintId, status: SPRINT_STATUS.ACTIVE }
    });
    if (!sprint) return jsonError(errors.SPRINT_NOT_FOUND_OR_NOT_ACTIVE);

    const allocation = await Allocation.findOne({
      where: { userId, sprintId }
    });

    if (!allocation) return jsonError(errors.USER_NOT_EXISTS_IN_SPRINT);

    // Calc point from list behaviors
    const behaviorData = await Behavior.findAll({
      where: {
        id: behaviors,
        isApply: true,
        typeApply: [TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION, TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY]
      }
    });
    if (behaviorData.length !== behaviors.length) return jsonError(errors.NOT_VALID_ID);

    const behaviorDedication = behaviorData.filter(item => item.typeApply === TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION);
    const behaviorResponsibility = behaviorData.filter(
      item => item.typeApply === TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY
    );

    if (behaviorDedication.length > 0) {
      const point = calcPointBehavior(behaviorDedication);
      let logDedication = await LogBehaviorSprint.create({
        actor,
        userId,
        sprintId,
        pointAdd: point.add,
        pointSub: point.sub,
        type: TYPE_LOG_BEHAVIOR.DEDICATION
      });
      logBehaviorSprint.push(logDedication);
    }

    if (behaviorResponsibility.length > 0) {
      const point = calcPointBehavior(behaviorResponsibility);
      let logResponsibility = await LogBehaviorSprint.create({
        actor,
        userId,
        sprintId,
        pointAdd: point.add,
        pointSub: point.sub,
        type: TYPE_LOG_BEHAVIOR.RESPONSIBILITY
      });
      logBehaviorSprint.push(logResponsibility);
    }

    return jsonSuccess(logBehaviorSprint);
  }

  static async createKpiBehavior({ actor, userId, behaviors, evaluatedAt }) {
    const user = await User.findByPk(userId);
    if (!user) return jsonError(errors.USER_NOT_FOUND);

    // Calc point from list behaviors
    const behaviorData = await Behavior.findAll({
      where: {
        id: behaviors,
        isApply: true,
        typeApply: TYPE_APPLY.BEHAVIOR_GENERAL
      }
    });
    if (behaviorData.length !== behaviors.length) return jsonError(errors.NOT_VALID_ID);

    const pointCalc = calcPointBehavior(behaviorData);
    const point = pointCalc.add - pointCalc.sub;

    const kpiBehavior = await KPIBehavior.create({
      actor,
      userId,
      point: point.toFixed(5),
      totalPointAdd: pointCalc.add,
      totalPointSub: pointCalc.sub,
      evaluatedAt
    });

    return jsonSuccess(kpiBehavior);
  }

  static async removeKpiBehavior({ kpiBehaviorId }) {
    const kpiBehavior = await KPIBehavior.findByPk(kpiBehaviorId);
    if (!kpiBehavior) return jsonError(errors.KPI_BEHAVIOR_NOT_FOUND);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            kpiBehavior.destroy({ transaction: t }),
            LogDetailBehavior.destroy({
              where: {
                logDetailBehaviorTableId: kpiBehaviorId,
                logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
              },
              transaction: t
            })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // const isRemovedKpiBehavior = await KPIBehavior.destroy({
    //   where: { id: kpiBehaviorId },
    // });
    //
    // if (!isRemovedKpiBehavior)
    //   return jsonError(errors.SYSTEM_ERROR);
    //
    // LogDetailBehavior.destroy({ where: {
    //     logDetailBehaviorTableId: kpiBehaviorId,
    //     logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
    //   }
    // });

    // return jsonSuccess(isRemovedKpiBehavior);
  }

  static async getListKpiBehavior({ userId, date, page, limit }) {
    const query = {};
    if (userId) {
      const user = await User.findByPk(userId);
      if (!user) return jsonError(errors.USER_NOT_FOUND);
      query.userId = userId;
    }

    if (date) {
      const from = moment(date)
        .startOf('date')
        .format('YYYY-MM-DD HH:mm:ss');
      const to = moment(date)
        .endOf('date')
        .format('YYYY-MM-DD HH:mm:ss');
      query.evaluatedAt = {
        [Op.gte]: from,
        [Op.lte]: to
      };
    }

    const include = [
      {
        model: User,
        as: 'user',
        attributes: ['id', 'name', 'avatar']
      },
      {
        model: User,
        as: 'infoActor',
        attributes: ['id', 'name', 'avatar']
      },
      {
        model: LogDetailBehavior,
        as: 'logDetailBehaviors',
        where: {
          logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
        },
        include: [
          {
            model: Behavior,
            as: 'behavior',
            attributes: ['id', 'activity', 'point', 'typeApply', 'typeAction']
          }
        ],
        required: false
      }
    ];
    if (limit && page) {
      const data = await paginate({
        model: KPIBehavior,
        query: { where: query, include },
        page,
        limit
      });
      return data.success ? jsonSuccess(data.result) : data;
    }

    const kpiBehaviors = await KPIBehavior.findAll({
      where: query,
      include
    });

    return jsonSuccess({
      data: kpiBehaviors,
      total: kpiBehaviors.length
    });
  }

  // Total point responsibility and dedication of user in sprint
  static async getPointOfUserInSprint({ userId, sprintId, type }) {
    const userLogBehaviorSprints = await LogBehaviorSprint.findAll({
      where: { userId, sprintId, type }
    });
    const points = userLogBehaviorSprints.map(
      logBehaviorSprint => logBehaviorSprint.pointAdd + logBehaviorSprint.pointSub
    );
    return points.length > 0 ? points.reduce((total, point) => total + point) : 0;
  }

  // Get list behaviors by sprint
  static async getListLogBehaviorInSprint({ sprintId }) {
    let checkSprint = await Sprint.findByPk(sprintId);
    if (!checkSprint) return jsonError(errors.NOT_FOUND_ERROR);

    const getListLogBehaviorBySprint = await LogBehaviorSprint.findAll({
      where: { sprintId },
      include: [
        {
          model: User,
          attributes: ['id', 'name', 'avatar', 'isActive']
        },
        {
          model: LogDetailBehavior,
          as: 'logDetailBehaviors',
          where: {
            logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
          },
          include: [
            {
              model: Behavior,
              as: 'behavior',
              attributes: ['id', 'activity', 'point', 'typeApply', 'typeAction']
            }
          ],
          required: false
        }
      ]
    });
    return jsonSuccess(getListLogBehaviorBySprint);
  }

  //Remove all logs of behaviors by sprint
  static async removeLogBehaviorInSprint({ owner, logBehaviorId }) {
    let behaviorSprint = await LogBehaviorSprint.findByPk(logBehaviorId);
    if (!behaviorSprint) return jsonError(errors.NOT_FOUND_ERROR);

    if (behaviorSprint.dataValues.actor !== owner) return jsonError(errors.ACCESS_DENIED);

    return new Promise(resolve => {
      sequelize
        .transaction(t => {
          return Promise.all([
            behaviorSprint.destroy({ transaction: t }),
            LogDetailBehavior.destroy({
              where: {
                logDetailBehaviorTableId: logBehaviorId,
                logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
              },
              transaction: t
            })
          ]);
        })
        .then(() => {
          return resolve(jsonSuccess());
        })
        .catch(err => {
          logger.error(err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        });
    });

    // const remove = await behaviorSprint.destroy();
    // if (!remove)
    //   return jsonError(errors.SYSTEM_ERROR);

    // LogDetailBehavior.destroy({ where: {
    //     logDetailBehaviorTableId: logBehaviorId,
    //     logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
    //   }
    // });

    // return jsonSuccess();
  }

  static async createLogDetailBehaviorWithKpiBehavior({ behaviors, kpiBehaviorId }) {
    const behaviorData = await Behavior.findAll({
      where: {
        id: behaviors,
        isApply: true,
        typeApply: TYPE_APPLY.BEHAVIOR_GENERAL
      }
    });
    if (behaviorData.length !== behaviors.length) return jsonError(errors.NOT_VALID_ID);

    const kpiBehavior = await KPIBehavior.findByPk(kpiBehaviorId);
    if (!kpiBehavior) return jsonError(errors.NOT_VALID_ID);

    const logDetailData = behaviors.map(id => ({
      behaviorId: id,
      logDetailBehaviorTableId: kpiBehaviorId,
      logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
    }));
    await LogDetailBehavior.bulkCreate(logDetailData);
    return jsonSuccess();
  }

  static async createLogDetailBehaviorWithLogBehaviorSprint({ behaviors, logBehaviorSprints }) {
    let logDetailData = [];
    const behaviorData = await Behavior.findAll({
      where: {
        id: behaviors,
        isApply: true,
        typeApply: [TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION, TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY]
      }
    });
    if (behaviorData.length !== behaviors.length) return jsonError(errors.NOT_VALID_ID);

    const ids = logBehaviorSprints.map(item => item.id);
    const logBehaviorSprintsData = await LogBehaviorSprint.findAll({
      where: { id: ids }
    });
    if (logBehaviorSprintsData.length !== logBehaviorSprints.length) return jsonError(errors.NOT_VALID_ID);

    const behaviorDedication = behaviorData.filter(item => item.typeApply === TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION);
    const behaviorResponsibility = behaviorData.filter(
      item => item.typeApply === TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY
    );

    for (let i = 0; i < logBehaviorSprints.length; i++) {
      const logBehaviorSprint = logBehaviorSprints[i];
      if (logBehaviorSprint.type === TYPE_LOG_BEHAVIOR.DEDICATION && behaviorDedication.length > 0) {
        logDetailData = logDetailData.concat(
          behaviorDedication.map(item => ({
            behaviorId: item.id,
            logDetailBehaviorTableId: logBehaviorSprint.id,
            logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
          }))
        );
      } else if (logBehaviorSprint.type === TYPE_LOG_BEHAVIOR.RESPONSIBILITY && behaviorResponsibility.length > 0) {
        logDetailData = logDetailData.concat(
          behaviorResponsibility.map(item => ({
            behaviorId: item.id,
            logDetailBehaviorTableId: logBehaviorSprint.id,
            logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT
          }))
        );
      }
    }
    await LogDetailBehavior.bulkCreate(logDetailData);
    return jsonSuccess();
  }

  static async createMultipleKpiBehavior({ data, actor }) {
    let codes = [];
    let usernames = [];
    let isValidCode = true;
    data.forEach(item => {
      codes = codes.concat(item.codes);
      usernames = usernames.concat(item.username);
      isValidCode = isValidCode && Array.isArray(item.codes) && item.codes.length > 0;
    });
    if (!isValidCode) return jsonError(errors.PARAM_INVALID);
    // remove duplicate value
    codes = codes.filter((v, i) => codes.indexOf(v) == i);
    usernames = usernames.filter((v, i) => usernames.indexOf(v) == i);

    let behaviors = await Behavior.findAll({
      where: {
        isApply: true,
        typeApply: TYPE_APPLY.BEHAVIOR_GENERAL
      }
    });
    behaviors = behaviors.filter(behavior => codes.includes(behavior.code));
    if (behaviors.length !== codes.length) return jsonError(errors.PARAM_INVALID);

    const users = await User.findAll({
      where: {
        name: usernames,
        isActive: true
      }
    });
    if (users.length !== usernames.length) return jsonError(errors.USER_NOT_FOUND);

    const kpiBehaviorsData = data.map(item => {
      const behaviorData = behaviors.filter(behavior => item.codes.includes(behavior.code));
      const pointCalc = calcPointBehavior(behaviorData);
      const point = pointCalc.add - pointCalc.sub;
      const user = users.find(user => user.name === item.username);

      return {
        actor,
        userId: user.id,
        point: point.toFixed(5),
        totalPointAdd: pointCalc.add,
        totalPointSub: pointCalc.sub,
        evaluatedAt: item.evaluatedAt || new Date()
      };
    });
    const kpiBehaviors = await KPIBehavior.bulkCreate(kpiBehaviorsData);

    return jsonSuccess(kpiBehaviors);
  }

  static async createLogDetailBehaviorWithKpiBehaviors({ data }) {
    let codes = [];
    let kpiBehaviorIds = [];
    data.forEach(item => {
      codes = codes.concat(item.codes);
      kpiBehaviorIds = kpiBehaviorIds.concat(item.kpiBehaviorId);
    });
    // remove duplicate value
    codes = codes.filter((v, i) => codes.indexOf(v) == i);

    let behaviors = await Behavior.findAll({
      where: {
        isApply: true,
        typeApply: TYPE_APPLY.BEHAVIOR_GENERAL
      }
    });
    behaviors = behaviors.filter(behavior => codes.includes(behavior.code));
    if (behaviors.length !== codes.length) return jsonError(errors.PARAM_INVALID);

    const kpiBehaviors = await KPIBehavior.findAll({
      where: {
        id: kpiBehaviorIds
      }
    });
    if (kpiBehaviors.length !== data.length) return jsonError(errors.NOT_VALID_ID);

    let logDetailData = [];
    data.forEach(item => {
      const behaviorData = behaviors.filter(behavior => item.codes.includes(behavior.code));
      const logDetailItem = behaviorData.map(behavior => ({
        behaviorId: behavior.id,
        logDetailBehaviorTableId: item.kpiBehaviorId,
        logDetailBehaviorTableType: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR
      }));
      logDetailData = logDetailData.concat(logDetailItem);
    });

    await LogDetailBehavior.bulkCreate(logDetailData);
    return jsonSuccess(logDetailData);
  }

  static async getListKpiBehaviorWithLog({ ids, logDetailBehaviorTableType }) {
    const include = [
      {
        model: User,
        as: 'user',
        attributes: ['id', 'name', 'avatar']
      },
      {
        model: User,
        as: 'infoActor',
        attributes: ['id', 'name', 'avatar']
      },
      {
        model: LogDetailBehavior,
        as: 'logDetailBehaviors',
        where: {
          logDetailBehaviorTableType
        },
        include: [
          {
            model: Behavior,
            as: 'behavior',
            attributes: ['id', 'activity', 'point', 'typeApply', 'typeAction']
          }
        ],
        required: false
      }
    ];

    const result = await KPIBehavior.findAll({
      where: {
        id: ids
      },
      include
    });
    return jsonSuccess(result);
  }
}

export { BehaviorService };
