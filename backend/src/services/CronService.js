import axios from 'axios';
import { Op } from 'sequelize';
import moment from 'moment';
import emailTemplate from 'email-templates';
import { jsonError, jsonSuccess, logger, errors } from '../utils/system';
import { Environment } from '../models/schema/Environment';
import { User } from '../models/schema/User';
import { AlertSubscription, SUBSCRIPTION_METHODS } from '../models/schema/AlertSubscription';
import { NotificationService } from './NotificationService';
import { Project } from '../models/schema/Project';
import { RedisService } from './RedisService';
import {
  getPermissionsForRole,
  getPermissionsForProjectRole,
  getPermissionsForSpecificUser,
  getListUncalculatedIds,
  getListUserIds,
  parseJSONIsArray
} from '../utils/common';
import path from 'path';
import * as logrotate from 'logrotator';
import { UserService } from './UserService';
import { SprintService } from './SprintService';
import { BehaviorService } from './BehaviorService';
import { TYPE_LOG_BEHAVIOR } from '../models/schema/LogBehaviorSprint';
import { TaskService } from './TaskService';

const setLoop = (fnName, fn, timeout, failureContinue) => {
  logger.verbose(`cronjob started: ${fnName}`);
  fn()
    .then(result => {
      logger.verbose(`cronjob finished: ${fnName}`);
      // logger.verbose(result);
      setTimeout(setLoop, timeout, fnName, fn, timeout, failureContinue);
    })
    .catch(err => {
      logger.error(`cron job failed: ${fnName}`);
      logger.error(err);
      if (failureContinue) setTimeout(setLoop, timeout, fnName, fn, timeout, failureContinue);
    });
};

const HEALTH_CHECK_SERVER_THRESHOLD = 3;
const MAX_MULTICAST_RECEIVERS = 50;

class CronService {
  static async boot() {
    CronService.CONST = {
      SCHEDULE_CHECK_LOCK_ROTATE: getEnv('SCHEDULE_CHECK_LOCK_ROTATE'),
      MAX_SIZE_LOG_ROTATE_FILE: getEnv('MAX_SIZE_LOG_ROTATE_FILE'),
      MAX_FILES_LOG_ROTATED: getEnv('MAX_FILES_LOG_ROTATED'),
      LOG_FILE_PATH: path.join(__dirname, '../../', getEnv('LOG_FILE_PATH'))
    };

    let data = await RedisService.get('CRON_DATA');
    data = data ? JSON.parse(data) : {};

    CronService.DATA = Object.assign(
      {
        healthCheckEnvironments: [],
        healthCheckSubscriptions: null,
        healthCheckStatus: null,
        userData: null,
        isActiveAlert: true,
        unauthenticatedPermissions: [],
        appPermissions: [],
        userPermissions: {},
        userRolePermissions: {},
        projectRolePermissions: {},
        kpiSprintCacheExpired: 60 * 60, // 1 hours - unit seconds
        twilioBalance: null
      },
      data
    );

    CronService.backupToRedis();
    CronService.userPermissionCron();
    CronService.syncIdentityUserDataCron();
    CronService.projectHealthCheckCron();
    CronService.logRotate();
    // kpi project
    CronService.calculateKpiSprint();
    CronService.checkTwilioBalance();

    return jsonSuccess();
  }

  static async getHealthCheckData({ envId }) {
    return jsonSuccess(CronService.DATA.healthCheckStatus && CronService.DATA.healthCheckStatus[envId]);
  }

  static async backupToRedis() {
    setLoop(
      'backupCronDataToRedis',
      async () => {
        await RedisService.stringsSetNoExpires(
          'CRON_DATA',
          JSON.stringify({
            healthCheckStatus: CronService.DATA.healthCheckStatus
          })
        );
      },
      parseInt(getEnv('SYNC_USER_DATA_INTERVAL_MS')),
      true
    );
  }

  static async syncIdentityUserDataCron() {
    setLoop(
      'syncIdentityUserDataCron',
      async () => {
        if (!CronService.DATA.userData) {
          CronService.DATA.userData = {};
        }
        let users = await User.findAll();
        let usersInfo = await UserService.getUsers({
          ids: users.map(u => u.id)
        });
        if (!usersInfo.success) {
          //-- reload access token
          logger.warn('Fail to fetch user info from Identity after reloading token');
          logger.error(usersInfo);
        } else {
          usersInfo = usersInfo.result;
          for (let i = 0; i < users.length; i++) {
            let userInfo = usersInfo.find(u => u.id === users[i].id);
            if (userInfo) {
              CronService.DATA.userData[users[i].id] = userInfo;
              //-- update db
              await users[i].update({
                name: userInfo.username,
                avatar: userInfo.profilePhoto,
                isActive: !!userInfo.isActive,
                branch: userInfo.branch && userInfo.branch.name
              });
            }
          }
        }

        return CronService.DATA.userData;
      },
      parseInt(getEnv('SYNC_USER_DATA_INTERVAL_MS')),
      true
    );
  }

  static async projectHealthCheckCron() {
    // this cron regularly requests all the health check server
    // to get the current statuses of each project environment
    const interval = parseInt(getEnv('PROJECT_ALERT_CRON_INTERVAL_MS'));

    const sendNotification = async (envId, isServiceDown, subject, messages) => {
      logger.info('Sending notification' + JSON.stringify(messages));
      try {
        //-- get all subscriptions to this env
        let subs = CronService.DATA.healthCheckSubscriptions[envId];

        //-- skip if there is no subscription
        if (!subs) {
          logger.warn('no subscription for ' + envId);
          return jsonSuccess();
        }

        let environment = await Environment.findOne({
          where: { id: envId },
          include: [Project]
        });

        //-- skip if notification for this project is disabled
        if (!environment.Project.isNotificationEnable) {
          logger.warn('notification for this project is disabled');
          return jsonSuccess();
        }

        let sms =
          'B.A.P project alert\n' +
          `- Project: ${environment.Project.name}\n` +
          `- Env: ${environment.name}\n` +
          '- Reason:';
        messages.forEach(m => {
          sms += `\n  + ${m}`;
        });
        let dataWriteLog = `\n-> ${moment().format('DD/MM/YYYY HH:mm:ss Z')} \n` + sms;
        sms += `\n- Time: ${moment().format('DD/MM/YYYY HH:mm:ss Z')}`;

        let template = await new Promise(resolve => {
          new emailTemplate()
            .render('../assets/email_templates/project_server_alert/index', {
              projectName: environment.Project.name,
              envName: environment.name,
              timestamp: moment().format('DD/MM/YYYY HH:mm:ss Z'),
              messages
            })
            .then(result => {
              resolve(jsonSuccess(result));
            })
            .catch(err => {
              logger.error(err);
              resolve(jsonError(errors.SYSTEM_ERROR));
            });
        });

        if (!template.success) {
          logger.warn('building email template failed', template);
        } else {
          //-- send emails
          for (let i = 0; i < subs[SUBSCRIPTION_METHODS.EMAIL].length; i += MAX_MULTICAST_RECEIVERS) {
            //-- get email addresses of all subscribers
            let userIds = subs[SUBSCRIPTION_METHODS.EMAIL].slice(i, i + MAX_MULTICAST_RECEIVERS);
            let emails = userIds
              .filter(id => !!CronService.DATA.userData[id] && !!CronService.DATA.userData[id].email)
              .map(id => CronService.DATA.userData[id].email);
            if (emails && emails.length) {
              let listEmail = JSON.stringify(emails);
              logger.verbose(`sending emails to ${listEmail}`);
              NotificationService.sendEmail(getEnv('SYSTEM_EMAIL_ADDRESS'), [], emails, subject, template.result);
              dataWriteLog += `\n- EMAIL to users:${userIds.map(id => {
                return ` ${CronService.DATA.userData[id].username}`;
              })}, listEmail: ${listEmail}`;
            }
          }
        }

        //-- send sms
        if (subs[SUBSCRIPTION_METHODS.SMS]) {
          for (let i = 0; i < subs[SUBSCRIPTION_METHODS.SMS].length; i++) {
            //-- get phoneNumber of user
            let userId = subs[SUBSCRIPTION_METHODS.SMS][i];
            let phoneNumber = !!CronService.DATA.userData[userId] && CronService.DATA.userData[userId].phoneNumber;
            if (phoneNumber) {
              logger.verbose(`sending sms to ${phoneNumber}`);
              NotificationService.sendSMS(sms, phoneNumber);
              dataWriteLog += `\n- SMS to ${CronService.DATA.userData[userId].username} - ${phoneNumber}`;
            }
          }
        }

        //-- only make phone call when service is down
        if (subs[SUBSCRIPTION_METHODS.CALL] && isServiceDown) {
          for (let i = 0; i < subs[SUBSCRIPTION_METHODS.CALL].length; i++) {
            //-- get phoneNumber of user
            let userId = subs[SUBSCRIPTION_METHODS.CALL][i];
            let phoneNumber = !!CronService.DATA.userData[userId] && CronService.DATA.userData[userId].phoneNumber;
            if (phoneNumber) {
              logger.verbose(`calling ${phoneNumber}`);
              NotificationService.makePhoneCall('', phoneNumber);
              dataWriteLog += `\n- CALL to ${CronService.DATA.userData[userId].username} - ${phoneNumber}`;
            }
          }
        }

        let projectId = environment.Project.id.toString();
        let data = {
          projectId,
          value: dataWriteLog
        };

        let createLogResult = await NotificationService.createLog(JSON.stringify(data));
        if (!createLogResult.success) {
          logger.error(createLogResult);
        }
        return jsonSuccess();
      } catch (err) {
        logger.error(err);
        return jsonError(errors.SYSTEM_ERROR);
      }
    };

    setLoop(
      'projectHealthCheckDataReloadCron',
      async () => {
        if (CronService.DATA.isActiveAlert) {
          logger.verbose('Active alert system');
          let environments = await Environment.findAll({
            where: {
              [Op.not]: {
                [Op.or]: [{ serverHealthCheck: null }, { serverHealthCheck: '' }]
              }
            }
          });
          CronService.DATA.healthCheckEnvironments = environments.map(e => ({
            id: e.id,
            projectId: e.projectId,
            serverHealthCheck: e.serverHealthCheck,
            HcThresholdDown: e.HcThresholdDown,
            HcThresholdUp: e.HcThresholdUp
          }));

          let subscriptions = await AlertSubscription.findAll();
          CronService.DATA.healthCheckSubscriptions = {};
          subscriptions.forEach(sub => {
            let environmentIds = JSON.parse(sub.environmentIds);
            let methodIds = JSON.parse(sub.methodIds);
            if (methodIds.length) {
              environmentIds.forEach(id => {
                if (!CronService.DATA.healthCheckSubscriptions[id]) {
                  CronService.DATA.healthCheckSubscriptions[id] = {};
                  Object.keys(SUBSCRIPTION_METHODS).forEach(method => {
                    CronService.DATA.healthCheckSubscriptions[id][SUBSCRIPTION_METHODS[method]] = [];
                  });
                }
                methodIds.forEach(mId => {
                  CronService.DATA.healthCheckSubscriptions[id][mId].push(sub.userId);
                });
              });
            }
          });

          return {
            environments: CronService.DATA.healthCheckEnvironments,
            subscriptions: CronService.DATA.healthCheckSubscriptions
          };
        } else {
          logger.verbose('Inactive alert system');
        }
      },
      interval * 2,
      true
    );

    setLoop(
      'projectHealthCheckCron',
      async () => {
        if (CronService.DATA.isActiveAlert) {
          logger.verbose('Active alert system');
          //-- loop over the environments
          for (let i = 0; i < CronService.DATA.healthCheckEnvironments.length; i++) {
            let env = CronService.DATA.healthCheckEnvironments[i];

            if (!CronService.DATA.healthCheckStatus) CronService.DATA.healthCheckStatus = {};

            if (!CronService.DATA.healthCheckStatus[env.id]) {
              CronService.DATA.healthCheckStatus[env.id] = {
                healthCheckServer: {
                  threshold: 0,
                  isAlive: false,
                  alertSent: true
                },
                healthCheckData: {
                  serviceDown: true,
                  servers: []
                }
              };
            }
            //-- request to get data
            let response = await new Promise(resolve => {
              logger.verbose('requesting to health check: ' + env.serverHealthCheck);
              axios
                .get(env.serverHealthCheck, { timeout: 3000 })
                .then(response => {
                  logger.verbose('response for: ' + env.serverHealthCheck);
                  logger.verbose(response.data);
                  resolve(jsonSuccess(response.data));
                })
                .catch(err => {
                  logger.warn('error request health check: ' + env.serverHealthCheck);
                  logger.warn(err);
                  resolve(jsonError(errors.SYSTEM_ERROR));
                });
            });

            if (!response.success) {
              //-- health check server is down
              if (CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive) {
                CronService.DATA.healthCheckStatus[env.id].healthCheckServer.threshold--;
                if (CronService.DATA.healthCheckStatus[env.id].healthCheckServer.threshold <= 0) {
                  CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive = false;
                  CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent = false;
                }
              }
              if (
                !CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive &&
                !CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent
              ) {
                // console.log('notify Health check server DOWN for env', env.id);
                let notificationResult = await sendNotification(env.id, true, 'B.A.P project alert', [
                  'Health check server is OFF'
                ]);
                CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent = notificationResult.success;
              }
              continue;
            }

            //-- health check server is up
            if (!CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive) {
              CronService.DATA.healthCheckStatus[env.id].healthCheckServer.threshold++;
              if (
                CronService.DATA.healthCheckStatus[env.id].healthCheckServer.threshold >= HEALTH_CHECK_SERVER_THRESHOLD
              ) {
                CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive = true;
                CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent = false;
              }
            }
            if (
              CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive &&
              !CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent
            ) {
              // console.log('notify Health check server UP');
              let notificationResult = await sendNotification(env.id, false, 'B.A.P project alert', [
                'Health check server is ON'
              ]);
              CronService.DATA.healthCheckStatus[env.id].healthCheckServer.alertSent = notificationResult.success;
            }

            //-- check response data for service down
            response = response.result;
            if (!response.success) continue;

            if (CronService.DATA.healthCheckStatus[env.id].healthCheckServer.isAlive) {
              CronService.DATA.healthCheckStatus[env.id].healthCheckData.servers = response.result
                ? response.result.servers
                : [];
              let isServiceDown =
                !CronService.DATA.healthCheckStatus[env.id].healthCheckData ||
                !CronService.DATA.healthCheckStatus[env.id].healthCheckData.servers ||
                CronService.DATA.healthCheckStatus[env.id].healthCheckData.servers.some(s => {
                  return s && s.services && s.services.some(ser => !ser.isAlive);
                });

              if (isServiceDown !== CronService.DATA.healthCheckStatus[env.id].healthCheckData.serviceDown) {
                if (isServiceDown) {
                  let messages = [];
                  CronService.DATA.healthCheckStatus[env.id].healthCheckData.servers.forEach(server => {
                    server.services.forEach(service => {
                      if (!service.isAlive) {
                        messages.push(`${server.serverName}/${service.serviceName} DOWN`);
                      }
                    });
                  });
                  await sendNotification(env.id, true, 'B.A.P project alert', messages);
                } else {
                  await sendNotification(env.id, false, 'B.A.P project alert', ['All services GOOD']);
                }
                CronService.DATA.healthCheckStatus[env.id].healthCheckData.serviceDown = isServiceDown;
              }
            }
          }
          return CronService.DATA.healthCheckStatus;
        } else {
          logger.verbose('Inactive alert system');
        }
      },
      interval,
      true
    );
  }

  static async userPermissionCron() {
    const interval = parseInt(getEnv('USER_PERMISSION_CRON_INTERVAL_MS'));

    setLoop(
      'userPermissionCron',
      async () => {
        //-- get permission for roles
        await getPermissionsForRole();

        //-- get permission for projectRoles
        await getPermissionsForProjectRole();

        let users = await User.findAll();

        for (let i = 0; i < users.length; i++) {
          await getPermissionsForSpecificUser(users[i].id);
        }
      },
      interval,
      true
    );
  }

  static async logRotate() {
    const rotator = logrotate.rotator;
    const logFile = CronService.CONST.LOG_FILE_PATH;
    // check file rotation every SCHEDULE_CHECK_LOCK_ROTATE,
    // and rotate the file if its size exceeds MAX_SIZE_LOG_ROTATE_FILE.
    // keep only MAX_FILES_LOG_ROTATED rotated files and compress (gzip) them.
    rotator.register(logFile, {
      schedule: CronService.CONST.SCHEDULE_CHECK_LOCK_ROTATE,
      size: CronService.CONST.MAX_SIZE_LOG_ROTATE_FILE,
      compress: true,
      count: CronService.CONST.MAX_FILES_LOG_ROTATED
    });
    rotator.on('error', err => {
      logger.verbose('oops, an error occured on log rotate!');
    });
    rotator.on('rotate', file => {
      logger.verbose('file ' + file + ' was rotated!');
    });
  }

  static async calculateKpiSprint() {
    const interval = parseInt(getEnv('CALCULATE_KPI_SPRINT_CRON_INTERVAL'));
    setLoop(
      'calculateKpiSprintForMember',
      async () => {
        try {
          const isCalculatingKpiSprintForMember = await RedisService.get('isCalculatingKpiSprintForMember');
          if (+isCalculatingKpiSprintForMember) {
            return;
          }
          // set isCalculatingKpiSprintForMember = 1 for 1 hour
          await RedisService.stringsSet('isCalculatingKpiSprintForMember', '1', CronService.DATA.kpiSprintCacheExpired);

          // get list finished sprints
          const sprints = await SprintService.getListFinishedSprint({
            calculatedKpiSprintState: false
          });

          for (let i = 0; i < sprints.length; i += 1) {
            const sprint = sprints[i];
            const allocationUserIds = getListUserIds(sprint.allocations);
            const kpiSprintUserIds = getListUserIds(sprint.kpiSprints);
            const userIds = getListUncalculatedIds(allocationUserIds, kpiSprintUserIds);
            if (userIds && Array.isArray(userIds) && userIds.length === 0) {
              // All members were calculated kpi sprint
              // but the sprint was not set flag calculatedKpiSprint is true
              await SprintService.updateCalculatedKpiSprint({
                sprintId: sprint.id,
                state: true
              });
              continue;
            }

            const projectRate = sprint.Project.projectRate;
            // calculate kpi sprint for members
            for (let j = 0; j < userIds.length; j += 1) {
              const userId = userIds[j];
              // responsibility point of user
              const userResponsibilityPoint = await BehaviorService.getPointOfUserInSprint({
                userId,
                sprintId: sprint.id,
                type: TYPE_LOG_BEHAVIOR.RESPONSIBILITY
              });
              // dedication point of user
              const userDedicationPoint = await BehaviorService.getPointOfUserInSprint({
                userId,
                sprintId: sprint.id,
                type: TYPE_LOG_BEHAVIOR.DEDICATION
              });

              const { taskComplete, missDeadline, errorRelate } = await TaskService.getTaskPointOfUser({
                userId,
                sprintId: sprint.id
              });

              // store
              await SprintService.createKpiSprintForMember({
                userId,
                sprintId: sprint.id,
                projectId: sprint.projectId,
                taskComplete,
                userResponsibilityPoint,
                userDedicationPoint,
                missDeadline,
                errorRelate,
                projectRate
              });
            }
          }
          // set flag = 0
          await RedisService.stringsSet('isCalculatingKpiSprintForMember', '0', CronService.DATA.kpiSprintCacheExpired);
        } catch (e) {
          logger.error(e);
          return jsonError(errors.SYSTEM_ERROR);
        }
      },
      interval,
      true
    );
  }

  static async checkTwilioBalance() {
    const interval = parseInt(getEnv('TWILIO_CHECK_BALANCE_CRON_INTERVAL_MS'));
    setLoop(
      'checkTwilioBalance',
      async () => {
        try {
          const resultAccount = await NotificationService.getAccountDetail();
          const account = resultAccount.result;
          const balanceUrl = account && account.subresourceUris && account.subresourceUris.balance;

          if (!resultAccount.success || !balanceUrl) {
            logger.warn('Fail to fetch account resource from Twilio');
            logger.error(resultAccount);
            return jsonError(errors.SYSTEM_ERROR);
          }

          const resultBalance = await NotificationService.getTwilioFile(balanceUrl);
          if (!resultBalance.success) {
            logger.warn('Fail to fetch balance file from Twilio');
            logger.error(resultBalance);
            return jsonError(errors.SYSTEM_ERROR);
          }

          const currentBalance = parseFloat(resultBalance.result && resultBalance.result.balance);
          const minBalance = parseFloat(getEnv('TWILIO_MIN_BALANCE'));
          CronService.DATA.twilioBalance = currentBalance;
          if (!isNaN(currentBalance) && currentBalance < minBalance) {
            const listEmailString = getEnv('TWILIO_EMAIL_ALERT_BALANCE');
            const emails = parseJSONIsArray(listEmailString);
            const subject = 'B.A.P project alert';
            const messages = [
              `Twilio account with sId ${getEnv('TWILIO_ACCOUNT_SID')} has low balance: ${currentBalance}`
            ];
            const template = await new Promise(resolve => {
              new emailTemplate()
                .render('../assets/email_templates/server_alert/index', {
                  timestamp: moment().format('DD/MM/YYYY HH:mm:ss Z'),
                  messages
                })
                .then(result => {
                  resolve(jsonSuccess(result));
                })
                .catch(err => {
                  logger.error(err);
                  resolve(jsonError(errors.SYSTEM_ERROR));
                });
            });

            if (!template.success) {
              logger.warn('building email template failed', template);
              return jsonError(errors.SYSTEM_ERROR);
            }
            logger.verbose(`sending emails to ${listEmailString}`);
            await NotificationService.sendEmail(getEnv('SYSTEM_EMAIL_ADDRESS'), [], emails, subject, template.result);
            return jsonSuccess();
          }
        } catch (err) {
          logger.error(err);
          return jsonError(errors.SYSTEM_ERROR);
        }
      },
      interval,
      true
    );
  }
}

export { CronService };
