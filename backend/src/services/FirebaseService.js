import admin from 'firebase-admin';
import { jsonSuccess, jsonError, errors } from '../utils/system';

class FirebaseService {
  static async boot() {
    admin.initializeApp({
      credential: admin.credential.cert({
        type: getEnv('FIREBASE_TYPE'),
        project_id: getEnv('FIREBASE_PROJECT_ID'),
        private_key_id: getEnv('FIREBASE_PRIVATE_KEY_ID'),
        private_key: getEnv('FIREBASE_PRIVATE_KEY').replace(/\\n/g, '\n'),
        client_email: getEnv('FIREBASE_CLIENT_EMAIL'),
        client_id: getEnv('FIREBASE_CLIENT_ID'),
        auth_uri: getEnv('FIREBASE_AUTH_URI'),
        token_uri: getEnv('FIREBASE_TOKEN_URI'),
        auth_provider_x509_cert_url: getEnv('FIREBASE_AUTH_PROVIDER_X509_CERT_URL'),
        client_x509_cert_url: getEnv('FIREBASE_CLIENT_X509_CERT_URL')
      })
    });
  }

  static async sendToDevice({ data, registrationToken }) {
    return new Promise(resolve => {
      if (!registrationToken || !data) return resolve(jsonError(errors.PARAM_INVALID));

      const payload = {
        data,
        notification: {
          title: `${data.title}`,
          icon: `${data.icon}`,
          body: `${data.body}`,
          sound: 'default',
          click_action: `${data.click_action}`
        }
      };

      // Send a message to the device corresponding to the provided
      // registration token.
      admin
        .messaging()
        .sendToDevice(registrationToken, payload)
        .then(response => {
          // See the MessagingDevicesResponse reference documentation for
          // the contents of response.
          return resolve(jsonSuccess(response));
        })
        .catch(error => {
          return resolve(jsonError(error));
        });
    });
  }

  static async subscribeToTopic(registrationToken, topic) {
    return new Promise(resolve => {
      // Subscribe the device corresponding to the registration token to the
      // topic.
      if (!registrationToken || !topic) return resolve(jsonError(errors.PARAM_INVALID));
      admin
        .messaging()
        .subscribeToTopic(registrationToken, topic)
        .then(response => {
          return resolve(jsonSuccess(response));
        })
        .catch(error => {
          return resolve(jsonError(error));
        });
    });
  }

  static async unsubscribeFromTopic(registrationToken, topic) {
    return new Promise(resolve => {
      // Subscribe the device corresponding to the registration token to the
      // topic.
      if (!registrationToken || !topic) return resolve(jsonError(errors.PARAM_INVALID));

      admin
        .messaging()
        .unsubscribeFromTopic(registrationToken, topic)
        .then(response => {
          return resolve(jsonSuccess(response));
        })
        .catch(error => {
          return resolve(jsonError(error));
        });
    });
  }

  static async sendToTopic({ data, topic }) {
    return new Promise(resolve => {
      if (!data || !topic) return resolve(jsonError(errors.PARAM_INVALID));

      const payload = {
        data,
        notification: {
          title: `${data.title}`,
          icon: `${data.icon}`,
          body: `${data.body}`,
          sound: 'default',
          click_action: `${data.click_action}`
        }
      };
      // Send a message to devices subscribed to the provided topic.
      admin
        .messaging()
        .sendToTopic(topic, payload)
        .then(response => {
          // See the MessagingTopicResponse reference documentation for the
          // contents of response.
          return resolve(jsonSuccess(response));
        })
        .catch(error => {
          return resolve(jsonError(error));
        });
    });
  }

  static async sendMessageToTopic({ data, topic, token }) {
    if (!data || !topic || !token) return jsonError(errors.PARAM_INVALID);
    await Promise.all([
      this.unsubscribeFromTopic(token, topic),
      this.sendToTopic({ data, topic }),
      this.subscribeToTopic(token, topic)
    ]);
    return jsonSuccess();
  }
}

export { FirebaseService };
