import { errors, jsonError, jsonSuccess } from '../utils/system';
import { SaleStatus } from '../models/schema/SaleStatus';

class SaleStatusService {
  static async boot() {}

  static async createStatus({ name }) {
    let status = await SaleStatus.findOne({ where: { name } });
    if (status) return jsonError(errors.DUPLICATED_ERROR);

    status = await SaleStatus.create({ name });
    return jsonSuccess(status);
  }

  static async updateStatus({ statusId, name }) {
    let status = await SaleStatus.findByPk(statusId);
    if (!status) return jsonError(errors.NOT_FOUND_ERROR);

    status = await status.update({
      name,
      updatedAt: new Date()
    });
    return jsonSuccess(status);
  }

  static async getListStatus() {
    let result = await SaleStatus.findAll();
    return jsonSuccess(result);
  }

  static async removeStatus({ statusId }) {
    let status = await SaleStatus.findByPk(statusId);
    if (!status) return jsonError(errors.NOT_FOUND_ERROR);

    status = await status.destroy();
    if (!status) return jsonError(errors.SYSTEM_ERROR);
    return jsonSuccess(status);
  }
}

export { SaleStatusService };
