export const regexStringValue = (fields) => {
  return {
    fields,
    valueType: 'string',
    permit: (conditions) => {
      return {
        name: 'regexStringValue', value: !!conditions && conditions.map(c => {
          return {name: c.name, value: c.value}
        })
      }
    },
    evaluate: async (params, conditions) => {

    },
  };
};

export const editableFields = (fields) => {
  return {
    fields,
    valueType: 'boolean',
    permit: (conditions) => {
      return {name: 'editableFields', value: conditions || null}
    },
    evaluate: async (params, conditions) => {

    },
  };
};

export const subset = (fields) => {
  return {
    fields,
    valueType: 'string',
    permit: (conditions) => {
      return {name: 'subset', value: conditions || null}
    },
    evaluate: async (params, conditions) => {
      return !fields.some(f => {
        return !(!!params[f] && !!conditions && conditions.some(c => {
          return (c.name === f) && params[f].every(field => c.value.indexOf(field) >= 0);
        }));
      });
    },
  }
};

export const projectDepend = (projectIdProvider) => {
  return {
    valueType: 'array',
    permit: (conditions) => {
      return {name: 'project_depend', value: conditions || null}
    },
    evaluate: async (params, conditions) => {
      if (!projectIdProvider) {
        return false;
      }
      let projectId = await projectIdProvider(params);
      return conditions.includes(Number(projectId));
    },
  }
};

