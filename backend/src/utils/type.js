export const KPI_TYPES = {
  behavior: 1,
  skill: 2,
  projectMemberSprint: 3,
  projectLeaderSprint: 4,
};

export const TYPE_USER_FILTER_ALLOCATION = {
  DEFAULT: 1, // user active, not role customer, not user admin (user init system)
  ALL: 2,
};

export const TYPE_LIST_USER = {
  DEFAULT: 1, // not user admin (user init system)
  ALL: 2
};

export const TYPE_GET_BEHAVIOR = {
  DEFAULT: 0,
  SPRINT: 1 // get behaviors for sprint (dedication, responsibility)
};
