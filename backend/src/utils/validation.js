const isValidObjectId = (id) => {
  return id.match(/^[1-9][0-9]*$/) ? id : null;
};

export {isValidObjectId};
