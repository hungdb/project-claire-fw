import _ from 'lodash';

const mimeTypes = [
  {
    mimeType: {
      name: 'AAC audio',
      extension: 'aac',
      mime: 'audio/aac'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'AbiWord document',
      extension: 'abw',
      mime: 'application/x-abiword'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Archive document',
      extension: 'arc',
      mime: 'application/x-freearc'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'AVI: Audio Video Interleave',
      extension: 'avi',
      mime: 'video/x-msvideo'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Amazon Kindle eBook format',
      extension: 'azw',
      mime: 'application/vnd.amazon.ebook'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Any kind of binary data',
      extension: 'bin',
      mime: 'application/octet-stream'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Windows OS/2 Bitmap Graphics',
      extension: 'bmp',
      mime: 'image/bmp'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'BZip archive',
      extension: 'bz',
      mime: 'application/x-bzip'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Cascading Style Sheets (CSS)',
      extension: 'text/css',
      mime: 'css'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'BZip2 archive',
      extension: 'bz2',
      mime: 'application/x-bzip2'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'C-Shell script',
      extension: 'csh',
      mime: 'application/x-csh'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Comma-separated values (CSV)',
      extension: 'csv',
      mime: 'text/csv'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Word',
      extension: 'doc',
      mime: 'application/msword'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'MS Embedded OpenType fonts',
      extension: 'eot',
      mime: 'application/vnd.ms-fontobject'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Word (OpenXML)',
      extension: 'docx',
      mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Graphics Interchange Format (GIF)',
      extension: 'gif',
      mime: 'image/gif'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Electronic publication (EPUB)',
      extension: 'epub',
      mime: 'application/epub+zip'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'HyperText Markup Language (HTML)',
      extension: 'html',
      mime: 'text/html'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Icon format',
      extension: 'ico',
      mime: 'image/vnd.microsoft.icon'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'iCalendar format',
      extension: 'ics',
      mime: 'text/calendar'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Java Archive (JAR)',
      extension: 'jar',
      mime: 'application/java-archive'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JPEG images',
      extension: 'jpeg',
      mime: 'image/jpeg'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JPG images',
      extension: 'jpg',
      mime: 'image/jpg'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JavaScript',
      extension: 'js',
      mime: 'text/javascript'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'JSON format',
      extension: 'json',
      mime: 'application/json'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Musical Instrument Digital Interface (MIDI)',
      extension: 'midi',
      mime: 'audio/midi audio/x-midi'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'JavaScript module',
      extension: 'mjs',
      mime: 'application/javascript'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'MP3 audio',
      extension: 'mp3',
      mime: 'audio/mpeg'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'MPEG Video',
      extension: 'mpeg',
      mime: 'video/mpeg'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Apple Installer Package',
      extension: 'mpkg',
      mime: 'application/vnd.apple.installer+xml'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument presentation document',
      extension: 'odp',
      mime: 'application/vnd.oasis.opendocument.presentation'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument spreadsheet document',
      extension: 'ods',
      mime: 'application/vnd.oasis.opendocument.spreadsheet'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument text document',
      extension: 'odt',
      mime: 'application/vnd.oasis.opendocument.text'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OGG audio',
      extension: 'oga',
      mime: 'audio/ogg'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OGG video',
      extension: 'ogv',
      mime: 'video/ogg'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OGG',
      extension: 'ogx',
      mime: 'application/ogg'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OpenType font',
      extension: 'otf',
      mime: 'font/otf'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Portable Network Graphics',
      extension: 'png',
      mime: 'image/png'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Adobe Portable Document Format (PDF)',
      extension: 'pdf',
      mime: 'application/pdf'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft PowerPoint',
      extension: 'ppt',
      mime: 'application/vnd.ms-powerpoint'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft PowerPoint (OpenXML)',
      extension: 'pptx',
      mime: 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'RAR archive',
      extension: 'rar',
      mime: 'application/x-rar-compressed'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Rich Text Format (RTF)',
      extension: 'rtf',
      mime: 'application/rtf'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Bourne shell script',
      extension: 'sh',
      mime: 'application/x-sh'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Scalable Vector Graphics (SVG)',
      extension: 'svg',
      mime: 'image/svg+xml'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Small web format (SWF) or Adobe Flash document',
      extension: 'swf',
      mime: 'application/x-shockwave-flash'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Tape Archive (TAR)',
      extension: 'tar',
      mime: 'application/x-tar'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Tagged Image File Format (TIFF)',
      extension: 'tiff',
      mime: 'image/tiff'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'TrueType Font',
      extension: 'ttf',
      mime: 'font/ttf'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Text, (generally ASCII or ISO 8859-n)',
      extension: 'txt',
      mime: 'text/plain'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Visio',
      extension: 'vsd',
      mime: 'application/vnd.visio'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Waveform Audio Format',
      extension: 'wav',
      mime: 'audio/wav'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBM audio',
      extension: 'weba',
      mime: 'audio/webm'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBM video',
      extension: 'webm',
      mime: 'video/webm'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBP image',
      extension: 'webp',
      mime: 'image/webp'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Web Open Font Format (WOFF)',
      extension: 'woff',
      mime: 'font/woff'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Web Open Font Format (WOFF)',
      extension: 'woff2',
      mime: 'font/woff2'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XHTML',
      extension: 'xhtml',
      mime: 'application/xhtml+xml'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Excel',
      extension: 'xls',
      mime: 'application/vnd.ms-excel'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Excel (OpenXML)',
      extension: 'xlsx',
      mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XML application/xml if not readable from casual users (RFC 3023, section 3)',
      extension: 'xml',
      mime: 'text/xml'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XML application/xml if readable from casual users (RFC 3023, section 3)',
      extension: 'xml',
      mime: 'text/xml'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XUL',
      extension: 'xul',
      mime: 'application/vnd.mozilla.xul+xml'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'ZIP archive',
      extension: 'zip',
      mime: 'application/zip'
    },
    allow: 1
  },
  {
    mimeType: {
      name: '3GPP audio/video container',
      extension: '3gp',
      mime: 'video/3gpp'
    },
    allow: 0
  },
  {
    mimeType: {
      name: "3GPP audio/video container if it doesn't contain video",
      extension: '3gp',
      mime: 'audio/3gpp'
    },
    allow: 0
  },
  {
    mimeType: {
      name: '3GPP2 audio/video container',
      extension: '3g2',
      mime: 'video/3gpp2'
    },
    allow: 0
  },
  {
    mimeType: {
      name: "3GPP2 audio/video container if it doesn't contain video",
      extension: '3g2',
      mime: 'audio/3gpp2'
    },
    allow: 0
  },
  {
    mimeType: {
      name: '7-zip archive',
      extension: '7z',
      mime: 'application/x-7z-compressed'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'SKETCH',
      extension: 'sketch',
      mime: 'application/octet-stream'
    },
    allow: 1
  }
];

const allows = mimeTypes.filter(m => m.allow);
const allowMimeTypes = _.uniq(_.compact(allows.map(a => a.mimeType.mime)));
const allowMimeTypesByType = type => {
  return allowMimeTypes.filter(item => {
    const arr = item.split('/');
    return Array.isArray(arr) && arr.length > 0 && arr[0] === type;
  });
};

export { mimeTypes, allowMimeTypes, allowMimeTypesByType };
