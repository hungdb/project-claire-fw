import moment from 'moment-timezone';
import { jsonSuccess, jsonError, errors, logger } from './system';
import { Dependency } from '../models/schema/TaskDependency';
import { Task } from '../models/schema/Task';
import { Role } from '../models/schema/Role';
import { RolePermission } from '../models/schema/RolePermission';
import { PermissionScheme } from '../models/schema/PermissionScheme';
import { ProjectRole } from '../models/schema/ProjectRole';
import { PermissionSchemeDetail } from '../models/schema/PermissionSchemeDetail';
import { UserRole } from '../models/schema/UserRole';
import { UserProjectRole } from '../models/schema/UserProjectRole';
import { Project } from '../models/schema/Project';
import { projectDepend } from './rpc_conditions';
import { CronService } from '../services/CronService';
import { UserService } from '../services/UserService';
import { Allocation } from '../models/schema/Allocation';
import { TaskState } from '../models/schema/TaskState';
import { User } from '../models/schema/User';
import { EFFORT_TYPES, WorkflowState } from '../models/schema/WorkflowState';
import { mimeTypes } from './mime_types';
import { TYPE_ACTION } from '../models/schema/Behavior';
import { KPICoefficient } from '../models/schema/KPICoefficient';
import { Op } from 'sequelize';

moment.tz.setDefault('Asia/Ho_Chi_Minh');

export const parseJSON = (data, value = []) => {
  try {
    if (!data) return value;
    return JSON.parse(data);
  } catch (e) {
    return value;
  }
};

export const parseJSONIsArray = (data, value = []) => {
  try {
    if (!data) return value;
    const parse = JSON.parse(data);
    return Array.isArray(parse) ? parse : value;
  } catch (e) {
    return value;
  }
};

export const getDurationHours = (startDate, endDate) => {
  if (!startDate || !moment.isMoment(moment(startDate) || !endDate || !moment.isMoment(moment(endDate)))) return 0;

  const end = moment(endDate);
  const start = moment(startDate);
  const duration = moment.duration(end.diff(start));
  return duration.asHours();
};

export const getExtension = typeFile => {
  const mimeType = mimeTypes.find(m => m.mimeType.mime === typeFile);
  if (mimeType) return mimeType.mimeType.extension;

  const type = typeFile.split('/');
  return type[1];
  // switch (extension) {
  //   case 'vnd.openxmlformats-officedocument.presentationml.presentation':
  //     return 'pptx';
  //   case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
  //     return 'docx';
  //   case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
  //     return 'xlsx';
  //   case 'text/plain':
  //     return 'txt';
  //   case 'msword':
  //     return 'doc';
  //   case 'vnd.oasis.opendocument.spreadsheet':
  //     return 'ods';
  //   case 'vnd.oasis.opendocument.presentation':
  //     return 'odp';
  //   default:
  //     return extension;
  // }
};

export const paginate = ({ model, query = {}, page = 1, order = [['id', 'DESC']], limit = 10 }) => {
  return new Promise(async resolve => {
    try {
      const offset = (page - 1) * limit;
      if (page && page < 1) page = 1;
      let data = await model.findAndCountAll({
        ...query,
        order,
        limit,
        offset
      });
      const total = data.count;
      data = data.rows;
      const pages = Math.ceil(Number(total) / Number(limit));
      const result = {
        data,
        total,
        limit,
        pages,
        page
      };
      return resolve(jsonSuccess(result));
    } catch (e) {
      logger.error(e);
      return resolve(jsonError(errors.SYSTEM_ERROR));
    }
  });
};

const checkTimeString = time => {
  const array = time.split(':');
  if (!Array.isArray(array) || (Array.isArray(array) && array.length !== 2) || isNaN(array[0]) || isNaN(array[1])) {
    return null;
  }
  const num1 = parseInt(array[0], 10);
  const num2 = parseInt(array[1], 10);

  if (num1 < 0 || num1 > 23 || num2 < 0 || num2 > 60) {
    return null;
  }
  return {
    hours: num1,
    minutes: num2
  };
};

const convertToHours = time => {
  const result = checkTimeString(time);
  if (!result) return 0;
  return result.hours + result.minutes / 60;
};

const convertTotalHours = (hours = 0, minutes = 0, seconds = 0, milliseconds = 0) => {
  if (isNaN(hours) || isNaN(minutes) || isNaN(seconds) || isNaN(milliseconds)) return 0;
  return hours + minutes / 60 + seconds / (60 * 60) + milliseconds / (60 * 60 * 60);
};

export const convertMonthString = month => {
  if (month.toString().length === 1) return `0${month}`;
  if (month < 1) return 1;
  if (month > 12) return 12;
  return month;
};

const convertDayString = day => {
  if (day.toString().length === 1) return `0${day}`;
  return day;
};

const daysInMonth = (year, month) => {
  return moment(`${year}-${convertMonthString(month)}`, 'YYYY-MM').daysInMonth();
};

const isDayOff = (year, month, day) => {
  const date = moment(`${year}-${convertMonthString(month)}-${convertDayString(day)}`);
  const dayOfWeek = date.weekday(); // ISO
  return dayOfWeek === 6 || dayOfWeek === 0;
};

const totalTimeWorkInDay = ({
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type,
  startYear,
  startMonth,
  startDay
}) => {
  let total = 0;
  if (type === 1 && !isDayOff(startYear, startMonth, startDay) && startTotalHours < endTotalHours) {
    if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      START_TIME_HOUR_MORNING < endTotalHours &&
      endTotalHours < END_TIME_HOUR_MORNING
    ) {
      total = total + (endTotalHours - START_TIME_HOUR_MORNING);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      END_TIME_HOUR_MORNING <= endTotalHours &&
      endTotalHours <= START_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total =
        total +
        (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
        (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (START_TIME_HOUR_MORNING < startTotalHours && endTotalHours < END_TIME_HOUR_MORNING) {
      total = total + (endTotalHours - startTotalHours);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      END_TIME_HOUR_MORNING <= endTotalHours &&
      endTotalHours <= START_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours) + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours) + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (
      END_TIME_HOUR_MORNING <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_AFTERNOON &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      END_TIME_HOUR_MORNING <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_AFTERNOON &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (START_TIME_HOUR_AFTERNOON < startTotalHours && endTotalHours < END_TIME_HOUR_AFTERNOON) {
      total = total + (endTotalHours - startTotalHours);
    } else if (
      START_TIME_HOUR_AFTERNOON < startTotalHours &&
      startTotalHours < END_TIME_HOUR_AFTERNOON &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_AFTERNOON - startTotalHours);
    }
  }
  return total;
};

const totalTimeWorksDaysInMonthOfYear = ({
  startDay,
  endDay,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type,
  year,
  month
}) => {
  let total = 0;
  let i = startDay;
  while (i <= endDay) {
    if (type === 1 && !isDayOff(year, month, i)) {
      if (i === startDay) {
        if (0 <= startTotalHours && startTotalHours <= START_TIME_HOUR_MORNING) {
          total =
            total +
            (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
            (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (START_TIME_HOUR_MORNING < startTotalHours && startTotalHours < END_TIME_HOUR_MORNING) {
          total =
            total + (END_TIME_HOUR_MORNING - startTotalHours) + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (END_TIME_HOUR_MORNING <= startTotalHours && startTotalHours <= START_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (START_TIME_HOUR_AFTERNOON < startTotalHours && startTotalHours < END_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_AFTERNOON - startTotalHours);
        } else {
          total = total + 0;
        }
      } else if (startDay < i && i < endDay) {
        total =
          total +
          (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
          (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
      } else if (i === endDay) {
        if (0 <= endTotalHours && endTotalHours <= START_TIME_HOUR_MORNING) {
          total = total + 0;
        } else if (START_TIME_HOUR_MORNING < endTotalHours && endTotalHours < END_TIME_HOUR_MORNING) {
          total = total + (endTotalHours - START_TIME_HOUR_MORNING);
        } else if (END_TIME_HOUR_MORNING <= endTotalHours && endTotalHours <= START_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        } else if (START_TIME_HOUR_AFTERNOON < endTotalHours && endTotalHours < END_TIME_HOUR_AFTERNOON) {
          total =
            total + (endTotalHours - START_TIME_HOUR_AFTERNOON) + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        } else {
          total =
            total +
            (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON) +
            (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        }
      }
    }
    i += 1;
  }
  return total;
};

const totalTimeWorksDaysInMonthsOfYear = ({
  startDay,
  endDay,
  startMonth,
  endMonth,
  year,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type
}) => {
  let initMonth = startMonth;
  let initStartDay, initStartTotalHours, initEndDay, initEndTotalHours;
  let total = 0;
  while (initMonth <= endMonth) {
    if (initMonth === startMonth) {
      initStartDay = startDay;
      initStartTotalHours = startTotalHours;
      const initEndDateMonth = moment(
        `${year}-${convertMonthString(initMonth)}-${daysInMonth(year, initMonth)}T23:59:59.000`
      );
      initEndDay = initEndDateMonth.date();
      initEndTotalHours = convertTotalHours(
        initEndDateMonth.hours(),
        initEndDateMonth.minutes(),
        initEndDateMonth.seconds(),
        initEndDateMonth.milliseconds()
      );
    } else if (initMonth === endMonth) {
      const initStartDateMonth = moment(`${year}-${convertMonthString(initMonth)}-01T00:00:00.000`);
      initStartDay = initStartDateMonth.date();
      initStartTotalHours = convertTotalHours(
        initStartDateMonth.hours(),
        initStartDateMonth.minutes(),
        initStartDateMonth.seconds(),
        initStartDateMonth.milliseconds()
      );
      initEndDay = endDay;
      initEndTotalHours = endTotalHours;
    } else if (initMonth < endMonth) {
      const initStartDateMonth = moment(`${year}-${convertMonthString(initMonth)}-01T00:00:00.000`);
      initStartDay = initStartDateMonth.date();
      initStartTotalHours = convertTotalHours(
        initStartDateMonth.hours(),
        initStartDateMonth.minutes(),
        initStartDateMonth.seconds(),
        initStartDateMonth.milliseconds()
      );
      const initEndDateMonth = moment(
        `${year}-${convertMonthString(initMonth)}-${daysInMonth(year, initMonth)}T23:59:59.000`
      );
      initEndDay = initEndDateMonth.date();
      initEndTotalHours = convertTotalHours(
        initEndDateMonth.hours(),
        initEndDateMonth.minutes(),
        initEndDateMonth.seconds(),
        initEndDateMonth.milliseconds()
      );
    }

    total =
      total +
      totalTimeWorksDaysInMonthOfYear({
        startDay: initStartDay,
        endDay: initEndDay,
        startTotalHours: initStartTotalHours,
        endTotalHours: initEndTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        year,
        month: initMonth
      });
    initMonth += 1;
  }
  return total;
};

const totalTimeWorksDaysInYears = ({
  startYear,
  endYear,
  startDay,
  endDay,
  startMonth,
  endMonth,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type
}) => {
  let initYear = startYear;
  let initStartDay, initStartTotalHours, initEndDay, initEndTotalHours, initStartMonth, initEndMonth;
  let total = 0;
  while (initYear <= endYear) {
    /*
     *    startDay
     *    endDay
     *    startTotalHours
     *    endTotalHours
     */
    if (initYear === startYear) {
      initStartDay = startDay;
      initStartTotalHours = startTotalHours;
      const initEndDateYear = moment(`${initYear}-12-31T23:59:59.000`);
      initStartMonth = startMonth;
      initEndMonth = 12;
      initEndDay = initEndDateYear.date();
      initEndTotalHours = convertTotalHours(
        initEndDateYear.hours(),
        initEndDateYear.minutes(),
        initEndDateYear.seconds(),
        initEndDateYear.milliseconds()
      );
    } else if (initYear === endYear) {
      const initStartDateYear = moment(`${initYear}-01-01T00:00:00.000`);
      initStartMonth = 1;
      initEndMonth = endMonth;
      initStartDay = initStartDateYear.date();
      initStartTotalHours = convertTotalHours(
        initStartDateYear.hours(),
        initStartDateYear.minutes(),
        initStartDateYear.seconds(),
        initStartDateYear.milliseconds()
      );
      initEndDay = endDay;
      initEndTotalHours = endTotalHours;
    } else if (initYear < endYear) {
      const initStartDateYear = moment(`${initYear}-01-01T00:00:00.000`);
      initStartDay = initStartDateYear.date();
      initStartTotalHours = convertTotalHours(
        initStartDateYear.hours(),
        initStartDateYear.minutes(),
        initStartDateYear.seconds(),
        initStartDateYear.milliseconds()
      );
      const initEndDateYear = moment(`${initYear}-12-31T23:59:59.000`);
      initEndDay = initEndDateYear.date();
      initStartMonth = 1;
      initEndMonth = 12;
      initEndTotalHours = convertTotalHours(
        initEndDateYear.hours(),
        initEndDateYear.minutes(),
        initEndDateYear.seconds(),
        initEndDateYear.milliseconds()
      );
    }

    if (initStartMonth === initEndMonth) {
      total =
        total +
        totalTimeWorksDaysInMonthOfYear({
          startDay: initStartDay,
          endDay: initEndDay,
          startTotalHours: initStartTotalHours,
          endTotalHours: initEndTotalHours,
          START_TIME_HOUR_MORNING,
          END_TIME_HOUR_MORNING,
          START_TIME_HOUR_AFTERNOON,
          END_TIME_HOUR_AFTERNOON,
          type,
          year: initYear,
          month: initStartMonth // or initEndMonth
        });
    } else {
      total =
        total +
        totalTimeWorksDaysInMonthsOfYear({
          startDay: initStartDay,
          endDay: initEndDay,
          startMonth: initStartMonth,
          endMonth: initEndMonth,
          year: initYear,
          startTotalHours: initStartTotalHours,
          endTotalHours: initEndTotalHours,
          START_TIME_HOUR_MORNING,
          END_TIME_HOUR_MORNING,
          START_TIME_HOUR_AFTERNOON,
          END_TIME_HOUR_AFTERNOON,
          type
        });
    }
    initYear += 1;
  }
  return total;
};

/**
 * type = 1: no count t7, cn
 * type = 2: count t7, cn
 */
export const durationHoursWork = (startDate, endDate, type = 1) => {
  if (!startDate || !moment.isMoment(moment(startDate)) || !endDate || !moment.isMoment(moment(endDate))) return 0;

  const START_TIME_HOUR_MORNING = convertToHours(getEnv('START_TIME_MORNING'));
  const END_TIME_HOUR_MORNING = convertToHours(getEnv('END_TIME_MORNING'));
  const START_TIME_HOUR_AFTERNOON = convertToHours(getEnv('START_TIME_AFTERNOON'));
  const END_TIME_HOUR_AFTERNOON = convertToHours(getEnv('END_TIME_AFTERNOON'));

  const end = moment(endDate);

  const start = moment(startDate);

  const startYear = start.year();
  const startDay = start.date();
  const startMonth = start.month() + 1;
  const startHours = start.hours();
  const startMinutes = start.minutes();
  const startSeconds = start.seconds();
  const startMilliseconds = start.milliseconds();
  const startTotalHours = convertTotalHours(startHours, startMinutes, startSeconds, startMilliseconds);

  const endYear = end.year();
  const endDay = end.date();
  const endMonth = end.month() + 1;
  const endHours = end.hours();
  const endMinutes = end.minutes();
  const endSeconds = end.seconds();
  const endMilliseconds = end.milliseconds();
  const endTotalHours = convertTotalHours(endHours, endMinutes, endSeconds, endMilliseconds);

  if (START_TIME_HOUR_MORNING < END_TIME_HOUR_MORNING < START_TIME_HOUR_AFTERNOON < END_TIME_HOUR_AFTERNOON) {
    if (startDay === endDay && startTotalHours < endTotalHours && startMonth === endMonth && startYear === endYear) {
      // console.log('totalTimeWorkInDay');
      return totalTimeWorkInDay({
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        startYear,
        startMonth,
        startDay
      });
    } else if (startDay < endDay && startMonth === endMonth && startYear === endYear) {
      // console.log('totalTimeWorksDaysInMonthOfYear');
      return totalTimeWorksDaysInMonthOfYear({
        startDay,
        endDay,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        year: startYear,
        month: startMonth
      });
    } else if (startMonth < endMonth && startYear === endYear) {
      // console.log('totalTimeWorksDaysInMonthsOfYear');
      return totalTimeWorksDaysInMonthsOfYear({
        startDay,
        endDay,
        startMonth,
        endMonth,
        year: startYear,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type
      });
    } else if (startYear < endYear) {
      // console.log('totalTimeWorksDaysInYears');
      return totalTimeWorksDaysInYears({
        startYear,
        endYear,
        startDay,
        endDay,
        startMonth,
        endMonth,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type
      });
    }
  }
  return 0;
};

export const transferToArrayValue = (arrayObject, property = 'id') => {
  const outputArray = [];
  if (!arrayObject || !Array.isArray(arrayObject)) {
    return outputArray;
  }
  arrayObject.forEach(object => {
    if (object[property] !== undefined) outputArray.push(object[property]);
  });
  return outputArray;
};

// truy van den cac task cha cua task dang xet -> bo sung graph
export const getParentTasks = async ({ graph, taskId }) => {
  let newGraph = { ...graph };
  const graphNodes = Object.keys(newGraph);
  if (!graphNodes.includes(`${taskId}`)) {
    let parentsOfDependency = await Dependency.findAll({
      where: { taskId: taskId },
      attributes: ['parentId']
    });
    newGraph[taskId] = [];
    if (parentsOfDependency.length) {
      for (let j = 0; j < parentsOfDependency.length; j++) {
        newGraph[taskId].push(`${parentsOfDependency[j].parentId}`);
      }
      for (let i = 0; i < parentsOfDependency.length; i++) {
        newGraph = await getParentTasks({
          graph: newGraph,
          taskId: parentsOfDependency[i].parentId
        });
      }
    } else {
      newGraph = Object.assign(newGraph, newGraph[taskId]);
    }
  }
  return newGraph;
};

// init graph relationship dependency between tasks
export const getGraphTaskDependencies = async ({ taskId, dependencies }) => {
  let graph = {};

  graph[taskId] = [];
  for (let i = 0; i < dependencies.length; i++) {
    const dependency = dependencies[i];
    const parent = await Task.findByPk(dependency);
    if (!parent) return jsonError(errors.NOT_FOUND_ERROR);
    graph[taskId].push(`${dependency}`);
  }
  for (let j = 0; j < dependencies.length; j += 1) {
    const dependency = dependencies[j];
    const parent = await Task.findByPk(dependency);
    if (!parent) return jsonError(errors.NOT_FOUND_ERROR);
    graph = await getParentTasks({ graph: graph, taskId: dependency });
  }
  return graph;
};

// detect cycle in graph
export const detectCycle = ({ graph }) => {
  const graphNodes = Object.keys(graph);
  const visited = {};
  const recStack = {};
  for (let i = 0; i < graphNodes.length; i++) {
    const node = graphNodes[i];
    if (detectCycleUtil(node, visited, recStack, graph)) {
      return false;
    }
  }
  return true;
};

export const detectCycleUtil = (vertex, visited, recStack, graph) => {
  if (!visited[vertex]) {
    visited[vertex] = true;
    recStack[vertex] = true;
    const nodeNeighbors = graph[vertex];
    for (let i = 0; i < nodeNeighbors.length; i++) {
      const currentNode = nodeNeighbors[i];
      if (!visited[currentNode] && detectCycleUtil(currentNode, visited, recStack, graph)) {
        return true;
      } else if (recStack[currentNode]) {
        return true;
      }
    }
  }
  recStack[vertex] = false;
  return false;
};

export const customDataAllocation = listAllocation => {
  let result = [];
  listAllocation.forEach(allocation => {
    let newAllo = Object();
    newAllo.id = allocation.id;
    newAllo.effortPercent = allocation.effortPercent;
    newAllo.startDate = allocation.startDate;
    newAllo.endDate = allocation.endDate;
    newAllo.project = allocation.Sprint && allocation.Sprint.Project && allocation.Sprint.Project.name;
    result.push(newAllo);
  });
  return result;
};

export const getPermissionsForRole = async () => {
  try {
    //-- get permissions for roles
    let allRoles = await Role.findAll({ where: {} });
    for (let iRole = 0; iRole < allRoles.length; iRole++) {
      // console.log("checking for role", allRoles[iRole].id, allRoles[iRole].name);
      CronService.DATA.userRolePermissions[allRoles[iRole].id] = [];
      let permissionForRole = await RolePermission.findAll({
        where: {
          roleId: allRoles[iRole].id
        }
      });
      permissionForRole.forEach(per => {
        CronService.DATA.userRolePermissions[allRoles[iRole].id].push({
          permission: per.permission,
          permissionCondition: JSON.parse(per.permissionCondition)
        });
      });

      // console.log("comparing", allRoles[iRole].name, UserService.DATA.UNAUTHENTICATED_ROLE);
      if (allRoles[iRole].name === UserService.DATA.UNAUTHENTICATED_ROLE) {
        // console.log('unauth role');
        CronService.DATA.unauthenticatedPermissions = CronService.DATA.userRolePermissions[allRoles[iRole].id];
      } else if (allRoles[iRole].name === UserService.DATA.APP_ROLE) {
        CronService.DATA.appPermissions = CronService.DATA.userRolePermissions[allRoles[iRole].id];
      }
    }
    return 1;
  } catch (e) {
    logger.error(e);
    return 0;
  }
};

export const getPermissionsForProjectRole = async () => {
  try {
    //-- get permissions for project roles
    let allSchemes = await PermissionScheme.findAll({ where: {} });
    let allProjectRoles = await ProjectRole.findAll({ where: {} });
    for (let iScheme = 0; iScheme < allSchemes.length; iScheme++) {
      CronService.DATA.projectRolePermissions[allSchemes[iScheme].id] = {};
      for (let iProjectRole = 0; iProjectRole < allProjectRoles.length; iProjectRole++) {
        CronService.DATA.projectRolePermissions[allSchemes[iScheme].id][allProjectRoles[iProjectRole].id] = [];
        //-- get permission setting for this
        let permissionForProjectRoles = await PermissionSchemeDetail.findAll({
          where: {
            permissionSchemeId: allSchemes[iScheme].id,
            projectRoleId: allProjectRoles[iProjectRole].id
          }
        });
        permissionForProjectRoles.forEach(per => {
          CronService.DATA.projectRolePermissions[allSchemes[iScheme].id][allProjectRoles[iProjectRole].id].push({
            permission: per.permission,
            permissionCondition: JSON.parse(per.permissionCondition)
          });
        });
      }
    }
    return 1;
  } catch (e) {
    logger.error(e);
    return 0;
  }
};

export const getPermissionsForSpecificUser = async userId => {
  try {
    CronService.DATA.userPermissions[userId] = [];
    //-- get all roles
    let userRoles = await UserRole.findAll({ where: { userId } });
    userRoles.forEach(userRole => {
      CronService.DATA.userPermissions[userId] = CronService.DATA.userPermissions[userId].concat(
        CronService.DATA.userRolePermissions[userRole.roleId]
      );
    });
    //-- get all project roles
    let projectRoles = await UserProjectRole.findAll({
      where: {
        userId,
        deletedAt: { [Op.eq]: null }
      }
    });
    for (let j = 0; j < projectRoles.length; j++) {
      let project = await Project.findByPk(projectRoles[j].projectId);
      CronService.DATA.projectRolePermissions[project.permissionSchemeId][projectRoles[j].projectRoleId].forEach(
        per => {
          CronService.DATA.userPermissions[userId].push({
            ...per,
            permissionCondition: [...per.permissionCondition, projectDepend().permit([project.id])]
          });
        }
      );
    }
    return 1;
  } catch (e) {
    logger.error(e);
    return 0;
  }
};

// update user permission when permission scheme detail changed
export const updateUserPermission = async permissionSchemeDetail => {
  try {
    await getPermissionsForProjectRole();
    let dataUserProjectRole = await UserProjectRole.findAll({
      where: { projectRoleId: permissionSchemeDetail.projectRoleId }
    });
    let listUsersChangePermission = [];
    for (let i = 0; i < dataUserProjectRole.length; i++) {
      if (!listUsersChangePermission.includes(dataUserProjectRole[i].userId)) {
        listUsersChangePermission.push(dataUserProjectRole[i].userId);
      }
    }
    for (let k = 0; k < listUsersChangePermission.length; k++) {
      getPermissionsForSpecificUser(listUsersChangePermission[k]);
    }
    return 1;
  } catch (e) {
    logger.error(e);
    return 0;
  }
};

// count weekend date in date range
export const countDayWorking = (startDate, endDate) => {
  if (!startDate || !moment.isMoment(moment(startDate)) || !endDate || !moment.isMoment(moment(endDate))) return 0;
  let totalDay = 0;
  let start = moment(startDate).utc();
  const end = moment(endDate).utc();
  while (start <= end) {
    const dayOfWeek = start.weekday(); // ISO
    if (dayOfWeek >= 1 && dayOfWeek <= 5) {
      totalDay = totalDay + 1;
    }
    start = start.add(1, 'days');
  }
  if (end.format().slice(10, end.format().length) === 'T00:00:00Z') totalDay = totalDay - 1;
  return totalDay;
};

// handle allocation data of a user in the sprint
export const handleAllocationDataOfUser = async (userId, sprintId) => {
  let listAllocation = await Allocation.findAll({
    where: { userId, sprintId }
  });
  let timestamps = [];
  for (let j = 0; j < listAllocation.length; j++) {
    timestamps.push(listAllocation[j].startDate);
    timestamps.push(listAllocation[j].endDate);
  }
  timestamps.sort((a, b) => {
    return moment(a) < moment(b) ? -1 : 1;
  });
  let efforts = [];
  //-- calculate effort
  for (let j = 0; j < timestamps.length - 1; j++) {
    efforts.push(0);
  }
  for (let j = 0; j < timestamps.length - 1; j++) {
    //-- check effort between i and i+1
    listAllocation.forEach(effort => {
      if (moment(timestamps[j]) >= moment(effort.startDate) && moment(timestamps[j + 1]) <= moment(effort.endDate)) {
        efforts[j] += effort.effortPercent;
      }
    });
  }

  return { timestamps, efforts };
};

export const getAbbreviationProjectName = projectName => {
  if (projectName.length <= 3) return projectName;
  let result = '';
  for (let i = 0; i < projectName.length; i++) {
    if (
      projectName.charAt(i) === projectName.charAt(i).toUpperCase() &&
      projectName[i] !== ' ' &&
      projectName[i].match(/^[a-zA-Z0-9]*$/)
    )
      result += projectName[i];
  }
  if (result.length < 2) return projectName.slice(0, 3);
  return result;
};

export const getRelateUsersOfTask = async taskId => {
  let users = [];
  let userIds = [];
  const taskState = await TaskState.findAll({
    where: { taskId },
    include: [
      { model: User, attributes: ['id', 'name', 'avatar'] },
      { model: WorkflowState, as: 'fromStates' },
      { model: WorkflowState, as: 'toStates' }
    ]
  });
  await taskState.map(st => {
    if (!userIds.includes(st.editor)) {
      userIds.push(st.editor);
      users.push(st.User);
    }
  });
  return { users, userIds, taskState };
};

export const getRelateUsersOfTaskTerminated = async taskId => {
  let relateUsers = await getRelateUsersOfTask(taskId);
  let { users, userIds, taskState } = relateUsers;
  let totalTimeCompletedTask = 0;
  let totalTimeOfUser = new Array(userIds.length).fill(0);
  for (let j = 0; j < taskState.length - 1; j++) {
    if (taskState[j].toState === taskState[j + 1].fromState) {
      if (taskState[j] && taskState[j].toStates && taskState[j].toStates.effortType === EFFORT_TYPES.PROGRESS) {
        let timeProgress = durationHoursWork(taskState[j].createdAt, taskState[j + 1].createdAt);
        totalTimeCompletedTask += timeProgress;
        let indexOfUser = userIds.indexOf(taskState[j].editor);
        totalTimeOfUser[indexOfUser] += timeProgress;
      }
    }
  }
  for (let k = 0; k < users.length; k++) {
    users[k].dataValues.percentEffort = (totalTimeOfUser[k] / totalTimeCompletedTask) * 100;
  }
  return users;
};

export const calculateLength = array => {
  if (!array || !array.hasOwnProperty('length')) return 0;
  return array.length;
};

export const combineRolesOfUser = members => {
  let data = [];
  const tmp = [];
  for (let j = 0; j < members.length; j++) {
    const member = members[j];
    const userId = member.User && member.User.id;
    if (!tmp.includes(userId)) {
      tmp.push(userId);
      data.push({
        id: member.User && member.User.id,
        name: member.User && member.User.name,
        avatar: member.User && member.User.avatar,
        roles: [
          {
            id: member.ProjectRole && member.ProjectRole.id,
            name: member.ProjectRole && member.ProjectRole.name,
            priorityLevel: member.ProjectRole && member.ProjectRole.priorityLevel
          }
        ]
      });
    } else {
      const index = data.findIndex(d => d.id === userId);
      data = [
        ...data.slice(0, index),
        {
          ...data[index],
          roles: [
            ...data[index].roles,
            {
              id: member.ProjectRole && member.ProjectRole.id,
              name: member.ProjectRole && member.ProjectRole.name
            }
          ]
        },
        ...data.slice(index + 1)
      ];
    }
  }
  return data;
};

export const getTotalWorkingDayInMonth = ({ year, month, endOfMonth }) => {
  let totalWorkingDay = 0;
  for (let i = 1; i <= endOfMonth; i += 1) {
    if (!isDayOff(year, month, i)) {
      totalWorkingDay += 1;
    }
  }
  return totalWorkingDay;
};

export const getListUncalculatedIds = (allocateUsers, calculatedUsers) =>
  allocateUsers.filter(userId => !calculatedUsers.includes(userId));

export const getListUserIds = array => {
  const userIds = [];
  array.forEach(item => {
    if (!userIds.includes(item.userId)) userIds.push(item.userId);
  });
  return userIds;
};

export const getTokenFirebase = async userId => {
  const user = await User.findByPk(userId);
  return (user && user.tokenFirebase) || null;
};

export const truncateString = (str, num) => {
  if (str.length > num) {
    return str.slice(0, num) + '...';
  } else {
    return str;
  }
};

export const inValidStartDateInSprint = ({ startDate, startDateSprint, endDateSprint }) => {
  const isLTStartDateSprint = new Date(startDate).getTime() < new Date(startDateSprint).getTime();
  const isGTEEndDateSprint = new Date(startDate).getTime() >= new Date(endDateSprint).getTime();
  return isLTStartDateSprint || isGTEEndDateSprint;
};

export const inValidEndDateInSprint = ({ endDate, startDateSprint, endDateSprint }) => {
  const isLTEStartDateSprint = new Date(endDate).getTime() <= new Date(startDateSprint).getTime();
  const isGTEndDateSprint = new Date(endDate).getTime() > new Date(endDateSprint).getTime();
  return isLTEStartDateSprint || isGTEndDateSprint;
};

export const subTimeDateString = (dateString, anoteDateString) => {
  return new Date(dateString).getTime() - new Date(anoteDateString).getTime();
};

export const groupBy = ({ array, key }) =>
  array.reduce((objectsByKeyValue, obj) => {
    const value = obj[key];
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
    return objectsByKeyValue;
  }, {});

export const getOverloadEffortPercentUsers = (currentArray, newAllocation = []) => {
  const arrayData = currentArray.concat(newAllocation);
  const allocationsGroupUser = groupBy({ array: arrayData, key: 'userId' });
  const overloadUsers = [];
  for (const userId in allocationsGroupUser) {
    let allocations = [];
    let timestamps = [];

    const allocationsOfUser = allocationsGroupUser[userId];
    allocationsOfUser.forEach(item => {
      allocations.push({
        startDate: item.startDate,
        endDate: item.endDate,
        effortPercent: item.effortPercent,
        id: item.id
      });
      timestamps.push(item.startDate);
      timestamps.push(item.endDate);
    });

    timestamps.sort((a, b) => {
      return moment(a) < moment(b) ? -1 : 1;
    });

    //-- calculate effort
    let currentEffort = 0;
    const efforts = new Array(timestamps.length).fill(0);
    const overload = timestamps.some((time, i) => {
      currentEffort = 0;
      const isLastItem = i + 1 === timestamps.length;
      const nextTime = isLastItem ? time : timestamps[i + 1];
      return allocations.some(effort => {
        if (moment(time) >= moment(effort.startDate) && moment(nextTime) <= moment(effort.endDate)) {
          efforts[i] += effort.effortPercent;
          if (effort.id) {
            currentEffort += effort.effortPercent;
          }
          return efforts[i] > 100;
        }
      });
    });

    if (overload) {
      overloadUsers.push({ userId: parseInt(userId), currentEffort });
    }
  }
  return overloadUsers;
};

export const calcPointBehavior = behaviors => {
  const point = {
    add: 0,
    sub: 0
  };
  for (let i = 0; i < behaviors.length; i++) {
    const behavior = behaviors[i];
    if (behavior.typeAction === TYPE_ACTION.ADD) {
      point.add += behavior.point;
    } else if (behavior.typeAction === TYPE_ACTION.SUB) {
      point.sub += behavior.point;
    }
  }
  return point;
};

export const parseJSONSafe = data => {
  try {
    return JSON.parse(data);
  } catch (err) {
    return null;
  }
};

export const getKPICoefficient = async type => {
  let skills = await KPICoefficient.findOne({ where: { type } });
  let coefficient = skills && skills.dataValues && skills.dataValues.data;
  return parseJSONSafe(coefficient);
};

export const withExtra = (error, action, message) => {
  return Object.assign({}, error, {
    extra: {
      action: action || '',
      message: message || ''
    }
  });
};

export const validPermissionCondition = scope => {
  let allowDataName = ['subset', 'editableFields', 'regexStringValue'];
  let permissions = [];
  try {
    permissions = JSON.parse(scope);
    return permissions.every(p => {
      let keys = Object.keys(p);
      if (!(keys.length === 2 && keys[0] === 'name' && keys[1] === 'value')) return false;
      if (!allowDataName.includes(p.name)) return false;
      return [p.value].constructor === Array;
    });
  } catch (err) {
    return false;
  }
};

export const workloadAllocation = (startDate, endDate, effortPercent) => {
  if (
    !startDate ||
    !moment.isMoment(moment(startDate)) ||
    !endDate ||
    !moment.isMoment(moment(endDate)) ||
    !effortPercent
  )
    return 0;
  let totalDay = 0;
  let start = moment(startDate).utc();
  const end = moment(endDate).utc();
  while (start <= end) {
    const dayOfWeek = start.weekday(); // ISO
    if (dayOfWeek >= 1 && dayOfWeek <= 5) {
      totalDay = totalDay + 1;
    }
    start = start.add(1, 'days');
  }
  return (8 * totalDay * effortPercent) / 100;
};

export const workloadAllocations = allocations => {
  let total = 0;
  if (!allocations || !allocations.hasOwnProperty('length')) return total;
  allocations.map(
    allocation => (total += workloadAllocation(allocation.startDate, allocation.endDate, allocation.effortPercent))
  );
  return total;
};

export const getS3FileName = filePath => {
  try {
    const arrPath = filePath.split('/');
    return `${arrPath[1]}/${arrPath[2]}`;
  } catch (e) {
    return '';
  }
};

export const formatDateTime = (dateTime, format = 'YYYY-MM-DD HH:mm:ss.SSS', type = 0) => {
  if (!dateTime) return '';
  if (type) return moment(dateTime).format(format);
  return moment(dateTime)
    .utc()
    .format(format);
  // return dateTime;
};
