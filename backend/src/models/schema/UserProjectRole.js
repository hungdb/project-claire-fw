import {Project} from "./Project";

let UserProjectRole;

const s = (builder, Sequelize) => {
  UserProjectRole = builder.define('UserProjectRole', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    projectRoleId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    deletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: null
    }
  }, {
    tableName: 'user_project_roles'
  });
  UserProjectRole.associate = (models) => {
    UserProjectRole.belongsTo(models['User'], { foreignKey: 'userId'});
    UserProjectRole.belongsTo(models['Project'], { foreignKey: 'projectId'});
    UserProjectRole.belongsTo(models['ProjectRole'], {foreignKey: 'projectRoleId'});
  };
  return UserProjectRole;
};

export {UserProjectRole, s};