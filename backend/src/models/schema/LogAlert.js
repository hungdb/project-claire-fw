let LogAlert;

const s = (builder, Sequelize) => {
  LogAlert = builder.define('LogAlert', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    environmentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    userIds: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: '[]',
    },
    methodId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ''
    },
    createdAt:
    {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  }, {
    tableName: 'log_alerts'
  });
  LogAlert.associate = (models) => {
    LogAlert.belongsTo(models['Environment'], {foreignKey: 'environmentId'});
  };
  return LogAlert;
};

export {LogAlert, s};
