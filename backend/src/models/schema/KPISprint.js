let KPISprint;

const WORK_MANAGEMENT_POINTS = {
  veryBad: 60,
  bad: 80,
  normal: 100,
  good: 120,
  veryGood: 140,
};

const PEOPLE_MANAGEMENT_POINTS = {
  veryBad: 60,
  bad: 80,
  normal: 100,
  good: 120,
  veryGood: 140,
};

const PROCESS_POINTS = {
  veryBad: 60,
  bad: 80,
  normal: 100,
  good: 120,
  veryGood: 140,
};

const TYPES = {
  member: 1,
  leader: 2,
};

const s = (builder, Sequelize) => {
  KPISprint = builder.define('KPISprint', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      point: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      sprintId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      projectId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      workManagementPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      peopleManagementPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      processPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      taskComplete: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      type: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
      missDeadline: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      errorRelate: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      }
    },
    {
      tableName: 'kpi_sprints'
    }
  );

  KPISprint.associate = (models) => {
    KPISprint.belongsTo(models['User'], { as: 'user', foreignKey: 'userId' });
    KPISprint.belongsTo(models['User'], { as: 'infoActor', foreignKey: 'actor' });
    KPISprint.belongsTo(models['Sprint'], { as: 'sprint', foreignKey: 'sprintId' });
    KPISprint.belongsTo(models['Project'], { as: 'project', foreignKey: 'projectId' });
  };
  return KPISprint;
};

export { KPISprint, s, WORK_MANAGEMENT_POINTS, PEOPLE_MANAGEMENT_POINTS, PROCESS_POINTS, TYPES };