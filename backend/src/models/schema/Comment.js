let Comment;
const COMMENT_TYPE = {
  TASK: 1,
  ISSUE: 2
};
const s = (builder, Sequelize) => {
  Comment = builder.define('Comment', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    commentTableId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    commentTableType: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  },
  {
    tableName: 'comments'
  });
  Comment.associate = (models) => {
    Comment.belongsTo(models['User'], {foreignKey: 'userId'});
  };
  return Comment;
};

export {Comment, COMMENT_TYPE, s};