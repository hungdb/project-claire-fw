let Environment;

const s = (builder, Sequelize) => {
  Environment = builder.define('Environment', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    serverHealthCheck: {
      type: Sequelize.STRING,
    },
    HcThresholdDown: {
      type: Sequelize.INTEGER
    },
    HcThresholdUp: {
      type: Sequelize.INTEGER
    },
    secretKey: {
      type: Sequelize.STRING
    }
  }, {
    tableName: 'environments'
  });
  Environment.associate = (models) => {
      Environment.belongsTo(models['Project'], {foreignKey: 'projectId'});
  };
  return Environment;
};

export {Environment, s};
