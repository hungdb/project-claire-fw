const ATTACHMENT_TYPE = {
  PROJECT: 1,
  TASK: 2,
  ISSUE: 3,
  SALE: 4,
  CUSTOMER: 5,
};

let Attachment;

const s = (builder, Sequelize) => {
  Attachment = builder.define('Attachment', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type:Sequelize.INTEGER,
        allowNull: false
      },
      url: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      mimeType: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      attachmentTableId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      attachmentTableType: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'attachments'
    });
  Attachment.associate = (models) => {
    Attachment.belongsTo(models['User'], { foreignKey: 'userId' });
  };
  return Attachment;
};

export {Attachment, s, ATTACHMENT_TYPE};
