let ProjectRole;

const s = (builder, Sequelize) => {
  ProjectRole = builder.define('ProjectRole', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      priorityLevel: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: 'project_roles'
    }
  );
  ProjectRole.associate = (models) => {
  };
  return ProjectRole;
};

export {ProjectRole, s};