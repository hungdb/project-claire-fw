let UserSkill;

const SKILL_TYPES = {
  normal: 1,
  project: 2,
  yourSelf: 3,
};

const s = (builder, Sequelize) => {
  UserSkill = builder.define('UserSkill', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      skillId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      evaluationPoint: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      isStandard: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      projectId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      sprintId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      type: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      note: {
        type: Sequelize.TEXT,
        defaultValue: '',
        allowNull: true,
      },
    },
    {
      tableName: 'user_skills'
    }
  );
  UserSkill.associate = (models) => {
    UserSkill.belongsTo(models['User'], {as: 'user', foreignKey: 'userId'});
    UserSkill.belongsTo(models['Skill'], {as: 'skill', foreignKey: 'skillId'});
    UserSkill.belongsTo(models['Project'], {as: 'project', foreignKey: 'projectId'});
    UserSkill.belongsTo(models['Sprint'], {as: 'sprint', foreignKey: 'sprintId'});
    UserSkill.belongsTo(models['User'], { as: 'actorAs', foreignKey: 'actor' });
  };
  return UserSkill;
};

export {UserSkill, s, SKILL_TYPES};