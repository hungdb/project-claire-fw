let Market;

const s = (builder, Sequelize) => {
    Market = builder.define('Market', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
      },
      {
        tableName: 'markets'
      }
    );

    Market.associate = (models) => {
        Market.hasMany(models['Customer'], {as: 'customer', foreignKey: 'marketId', sourceKey: 'id'});
    };

    return Market;
};

export { Market, s }
