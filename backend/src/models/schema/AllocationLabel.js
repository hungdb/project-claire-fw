let AllocationLabel;

const s = (builder, Sequelize) => {
  AllocationLabel = builder.define('AllocationLabel', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: new Date()
    }
  }, {
      tableName: 'allocation_labels'
    });
  AllocationLabel.associate = (models) => {
  };
  return AllocationLabel;
};

export { AllocationLabel, s };