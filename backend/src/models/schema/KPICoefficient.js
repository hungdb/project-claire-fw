const KPI_TYPE = {
  KPI_Skill: 1,
  KPI_Leader: 2,
  KPI_Project: 3
};

let KPICoefficient;

const s = (builder, Sequelize) => {
  KPICoefficient = builder.define('kpi_coefficients', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    data: {
      type: Sequelize.STRING,
      allowNull: false
    },
    type: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
  });

  KPICoefficient.associate = (models) => {

  };

  return KPICoefficient;
};

export { KPICoefficient, s, KPI_TYPE };
