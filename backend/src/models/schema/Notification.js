let Notification;

const NOTIFICATION_TYPE = {
  TASK: 'task',
  PROJECT: 'project',
  ISSUE: 'issue'
};
const s = (builder, Sequelize) => {
  Notification = builder.define('Notification', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      notificationableId: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      notificationableType: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      isRead: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      type: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      data: {
        type: Sequelize.TEXT,
        allowNull: true
      }
    },
    {
      tableName: 'notifications'
    });
  Notification.associate = (models) => {
    Notification.belongsTo(models['User'], { as: 'userAs', foreignKey: 'userId' });
    Notification.belongsTo(models['User'], { as: 'actorAs', foreignKey: 'actor' });
    Notification.belongsTo(models['Project'], { foreignKey: 'notificationableId', constraints: false, as: NOTIFICATION_TYPE.PROJECT });
    Notification.belongsTo(models['Task'], { foreignKey: 'notificationableId', constraints: false, as: NOTIFICATION_TYPE.TASK });
  };
  return Notification;
};

export {Notification, s, NOTIFICATION_TYPE};
