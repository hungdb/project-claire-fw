let Sprint;

const STATUS = {
  ACTIVE: 'active',
  UNACTIVE: 'unactive',
  COMPLETED:'completed'
};

const s = (builder, Sequelize) => {
  Sprint = builder.define('Sprint', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    calculatedKpiSprint: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, 
  {
    tableName: 'sprints'
  });
  Sprint.associate = (models) => {
    Sprint.belongsTo(models['Project'], { foreignKey: 'projectId' });
    Sprint.hasMany(models['Allocation'], { as: 'allocations', foreignKey: 'sprintId' });
    Sprint.hasMany(models['KPISprint'], { as: 'kpiSprints', foreignKey: 'sprintId' });
  };
  return Sprint;
};

export { Sprint, s, STATUS };