let RolePermission;

const s = (builder, Sequelize) => {
  RolePermission = builder.define('RolePermission', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      roleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      permission: {
        type: Sequelize.STRING,
        allowNull: false
      },
      permissionCondition: {
        type: Sequelize.TEXT,
        defaultValue: JSON.stringify([])
      }
    },
    {
      tableName: 'role_permissions'
    }
  );
  RolePermission.associate = (models) => {
    RolePermission.belongsTo(models['Role'], {foreignKey: 'roleId'});
  };
  return RolePermission;
};

export {RolePermission, s};