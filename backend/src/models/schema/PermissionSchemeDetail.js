let PermissionSchemeDetail;

const s = (builder, Sequelize) => {
  PermissionSchemeDetail = builder.define('PermissionSchemeDetail', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      permissionSchemeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      projectRoleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      permission: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      permissionCondition: {
        type: Sequelize.TEXT,
        allowNull: true
      }
    },
    {
      tableName: 'permission_scheme_details'
    }
  );
  PermissionSchemeDetail.associate = (models) => {
    PermissionSchemeDetail.belongsTo(models['PermissionScheme'], {foreignKey: 'permissionSchemeId'});
    PermissionSchemeDetail.belongsTo(models['ProjectRole'], {foreignKey: 'projectRoleId'});
  };
  return PermissionSchemeDetail;
};

export {PermissionSchemeDetail, s};