'use strict';

let KPIBehavior;

const s = (builder, Sequelize) => {
  KPIBehavior = builder.define(
    'KPIBehavior',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      point: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      totalPointAdd: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      totalPointSub: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      evaluatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
    },
    {
      tableName: 'kpi_behaviors',
    },
  );

  KPIBehavior.associate = (models) => {
    KPIBehavior.belongsTo(models['User'], { as: 'user', foreignKey: 'userId' });
    KPIBehavior.belongsTo(models['User'], { as: 'infoActor', foreignKey: 'actor' });
    KPIBehavior.hasMany(
      models['LogDetailBehavior'],
      {
        as: 'logDetailBehaviors',
        foreignKey: 'logDetailBehaviorTableId',
        sourceKey: 'id'
      }
    );
  };

  return KPIBehavior;
};

export { KPIBehavior, s };
