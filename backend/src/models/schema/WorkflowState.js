let WorkflowState;

const EFFORT_TYPES = {
  UNTOUCHED: 'untouched',
  HOLDING: 'holding',
  PROGRESS: 'progress',
  TERMINATED: 'terminated'
};

const s = (builder, Sequelize) => {
  WorkflowState = builder.define('WorkflowState', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      effortType: {
        type: Sequelize.STRING,
        allowNull: false
      },
      workflowId: {
        type:Sequelize.INTEGER,
        allowNull: false
      },
      ordinalNumber: {
        type: Sequelize.INTEGER,
        allowNull: false
      }
    },
    {
      tableName: 'workflow_states'
    }
  );
  WorkflowState.associate = (models) => {
  };
  return WorkflowState;
};

export {WorkflowState, s, EFFORT_TYPES};