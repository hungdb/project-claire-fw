const TYPE_APPLY = {
  BEHAVIOR_GENERAL: 1,
  BEHAVIOR_SPRINT_RESPONSIBILITY: 2,
  BEHAVIOR_SPRINT_DEDICATION: 3,
};

const TYPE_ACTION = {
  ADD: 1,
  SUB: 2,
};

const TYPE_SUB_APPLY = {
  BEHAVIOR_GENERAL: {
    STANDARD: 1,
    BONUS: 2
  }
};

let Behavior;

const s = (builder, Sequelize) => {
  Behavior = builder.define('Behavior', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    activity: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    point: {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0,
    },
    typeApply: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
    typeAction: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    decider: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    note: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    isApply: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: null,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: null,
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    typeSubApply: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null,
    },
    code: {
      type: Sequelize.VIRTUAL(Sequelize.STRING, ["typeApply", "id"]),
      get() {
        let prefix = '';
        switch(this.typeApply) {
          case TYPE_APPLY.BEHAVIOR_GENERAL:
            prefix = 'BG';
            break;
          case TYPE_APPLY.BEHAVIOR_SPRINT_RESPONSIBILITY:
            prefix = 'BSR';
            break;
          case TYPE_APPLY.BEHAVIOR_SPRINT_DEDICATION:
            prefix = 'BSD';
            break;
          default:
            prefix = 'BG';
        }
        return prefix + this.id;
      }
    }
  }, {
    tableName: 'behaviors'
  });
  Behavior.associate = (models) => {};
  return Behavior;
};

export { Behavior, s, TYPE_APPLY, TYPE_ACTION, TYPE_SUB_APPLY };
