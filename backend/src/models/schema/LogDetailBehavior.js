let LogDetailBehavior;

const LOG_DETAIL_BEHAVIOR_TABLE_TYPE = {
  KPI_BEHAVIOR: 'KPIBehavior',
  LOG_BEHAVIOR_SPRINT: 'LogBehaviorSprint',
};
const s = (builder, Sequelize) => {
  LogDetailBehavior = builder.define('LogDetailBehavior', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      behaviorId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      logDetailBehaviorTableId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      logDetailBehaviorTableType: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
    },
    {
      tableName: 'log_detail_behaviors'
    });
    LogDetailBehavior.associate = (models) => {
    LogDetailBehavior.belongsTo(models['Behavior'], { as: 'behavior', foreignKey: 'behaviorId' });
    LogDetailBehavior.belongsTo(
      models['KPIBehavior'],
      { foreignKey: 'logDetailBehaviorTableId', constraints: false, as: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.KPI_BEHAVIOR }
    );
    LogDetailBehavior.belongsTo(
      models['LogBehaviorSprint'],
      { foreignKey: 'logDetailBehaviorTableId', constraints: false, as: LOG_DETAIL_BEHAVIOR_TABLE_TYPE.LOG_BEHAVIOR_SPRINT }
    );
  };
  return LogDetailBehavior;
};

export {LogDetailBehavior, s, LOG_DETAIL_BEHAVIOR_TABLE_TYPE};
