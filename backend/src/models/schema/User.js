let User;
const s = (builder, Sequelize) => {
  User = builder.define('User', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      token: {
        type: Sequelize.TEXT,
      },
      tokenFirebase: {
        type: Sequelize.TEXT,
      },
      name: {
        type: Sequelize.STRING
      },
      avatar: {
        type: Sequelize.TEXT,
      },
      mentor: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      branch: {
        type: Sequelize.STRING,
        allowNull: true
      },
    },
    {
      tableName: 'users'
    });
  User.associate = (models) => {
    User.belongsTo(models['User'], { as: 'infoMentor', foreignKey: 'mentor' });
    User.belongsToMany(models['Skill'], { as: 'skills', through: 'UserSkill', foreignKey: 'userId' });
    User.hasMany(models['Allocation'], { as: 'allocationsAs', foreignKey: 'userId', sourceKey: 'id' });
    User.hasMany(models['AllocationPlan'], { as: 'allocationsPlanAs', foreignKey: 'userId', sourceKey: 'id' });
    User.hasMany(models['UserRole'], { as: 'roles', foreignKey: 'userId', sourceKey: 'id' });
    User.hasMany(models['UserSkill'], { as: 'userSkills', foreignKey: 'userId', sourceKey: 'id' });
  };
  return User;
};

export {User, s};
