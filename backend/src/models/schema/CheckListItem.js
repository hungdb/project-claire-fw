let CheckListItem;

const s = (builder, Sequelize) => {
  CheckListItem = builder.define('CheckListItem', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'check_list_items'
    });
  CheckListItem.associate = (models) => {};
  return CheckListItem;
};

export {CheckListItem, s};