let TaskState;
const s = (builder, Sequelize) => {
  TaskState = builder.define('TaskState', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    taskId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    fromState: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    toState: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    editor: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  }, {
    tableName: 'task_states'
  });
  TaskState.associate = (models) => {
      TaskState.belongsTo(models['Task'], { foreignKey: 'taskId'});
      TaskState.belongsTo(models['User'], {foreignKey: 'editor'});
      TaskState.belongsTo(models['WorkflowState'], {as: 'fromStates',  foreignKey: 'fromState'});
      TaskState.belongsTo(models['WorkflowState'], {as: 'toStates', foreignKey:'toState'});
  };
  return TaskState;
};
export {TaskState, s};