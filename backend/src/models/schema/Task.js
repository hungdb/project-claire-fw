let Task;

const TASK_PRIORITIES = {
  HIGH: 3,
  MEDIUM: 2,
  LOW: 1,
};

const TASK_TYPE = {
  NORMAL: 1,
  BUG: 2
};

const s = (builder, Sequelize) => {
  Task = builder.define('Task', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    creator: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    estimate: {
      type: Sequelize.DOUBLE,
      allowNull: true,
      defaultValue: 0
    },
    sprintId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    issueId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    tag: {
      type: Sequelize.TEXT
    },
    taskCode: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
    state: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    assignee: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    endDate: {
      type: Sequelize.DATE
    },
    totalTime: {
      type: Sequelize.FLOAT
    },
    priority: {
      type: Sequelize.INTEGER
    },
    milestone: {
      type: Sequelize.TEXT
    },
    dueDate: {
      type: Sequelize.DATE,
      allowNull: true
    },
    type: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    totalComments: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    userRelate: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: JSON.stringify([]),
    },
  }, {
    tableName: 'tasks'
  });
  Task.associate = (models) => {
    Task.belongsTo(models['Sprint'], { foreignKey: 'sprintId' });
    Task.belongsTo(models['User'], { as: 'creatorAs', foreignKey: 'creator' });
    Task.belongsTo(models['Project'], { foreignKey: 'projectId' });
    Task.belongsTo(models['Issue'], { foreignKey: 'issueId' });
    Task.belongsTo(models['WorkflowState'], { foreignKey: 'state' });
    Task.belongsTo(models['User'], { as: 'assigneeAs', foreignKey: 'assignee' });
    Task.hasMany(models['TaskDependency'], {as: 'dependencies', foreignKey: 'taskId', sourceKey: 'id'});
  };
  return Task;
};
export {Task, s, TASK_PRIORITIES, TASK_TYPE};