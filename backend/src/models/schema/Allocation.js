let Allocation;

const s = (builder, Sequelize) => {
  Allocation = builder.define('Allocation', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    sprintId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    effortPercent: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: false
    },
    label: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
  }, {
    tableName: 'allocations'
  });
  Allocation.associate = (models) => {
      Allocation.belongsTo(models['User'], {foreignKey: 'userId'});
      Allocation.belongsTo(models['Sprint'], {foreignKey: 'sprintId'});
      Allocation.belongsTo(models['AllocationLabel'], { as: 'labels', foreignKey: 'label'});
  };
  return Allocation;
};

export {Allocation, s};
