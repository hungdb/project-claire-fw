let UserRole;

const s = (builder, Sequelize) => {
  UserRole = builder.define('UserRole', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      roleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      }
    },
    {
      tableName: 'user_roles'
    }
  );
  UserRole.associate = (models) => {
    UserRole.belongsTo(models['User'], {foreignKey: 'userId'});
    UserRole.belongsTo(models['Role'], {foreignKey: 'roleId'});
  };
  return UserRole;
};

export {UserRole, s};