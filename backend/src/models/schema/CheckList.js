let CheckList;

const s = (builder, Sequelize) => {
  CheckList = builder.define('CheckList', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    checkItemId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    isChecked: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
  },
  {
    tableName: 'check_lists'
  });
  CheckList.associate = (models) => {
    CheckList.belongsTo(models['Project'], {foreignKey: 'projectId'});
    CheckList.belongsTo(models['CheckListItem'], {foreignKey: 'checkItemId'});
  };
  return CheckList;
};

export {CheckList, s};