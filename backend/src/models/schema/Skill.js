let Skill;
const s = (builder, Sequelize) => {
  Skill = builder.define('Skill', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      linkIcon: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'skills'
    });
  Skill.associate = (models) => {
    Skill.hasMany(models['UserSkill'], {as: 'userSkill', foreignKey: 'skillId', sourceKey: 'id'});
  };
  return Skill;
};

export {Skill, s};
