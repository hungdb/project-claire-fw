'use strict';

let SaleCategory;

const s = (builder, Sequelize) => {
  SaleCategory = builder.define('SaleCategory', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
    },
    {
      tableName: 'sale_categories'
    }
  );

  SaleCategory.associate = (models) => {
    SaleCategory.hasMany(models['Sale'], { as: 'sales', foreignKey: 'categoryId', sourceKey: 'id' });
  };

  return SaleCategory;
};

export {SaleCategory, s};