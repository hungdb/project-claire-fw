let WorkflowDetail;

const s = (builder, Sequelize) => {
  WorkflowDetail = builder.define('WorkflowDetail', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      workflowId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      fromState: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      toState: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true
      }
    },
    {
      tableName: 'workflow_details'
    }
  );
  WorkflowDetail.associate = (models) => {
    WorkflowDetail.belongsTo(models['Workflow'], {foreignKey: 'workflowId'});
    WorkflowDetail.belongsTo(models['WorkflowState'], {as: 'fromStates', foreignKey: 'fromState'});
    WorkflowDetail.belongsTo(models['WorkflowState'], {as: 'toStates', foreignKey: 'toState'});
  };
  return WorkflowDetail;
};

export {WorkflowDetail, s};