let LogBehaviorSprint;

const TYPE_LOG_BEHAVIOR = {
  RESPONSIBILITY: 1,
  DEDICATION: 2
};

const s = (builder, Sequelize) => {
  LogBehaviorSprint = builder.define('LogBehaviorSprint', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    actor: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    sprintId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    pointAdd: {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0,
    },
    pointSub: {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0,
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW,
    },
    type: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
  }, {
    tableName: 'log_behavior_sprints'
  });
  LogBehaviorSprint.associate = (models) => {
    LogBehaviorSprint.belongsTo(models['User'], { as: 'actorAs', foreignKey: 'actor' });
    LogBehaviorSprint.belongsTo(models['User'], { foreignKey: 'userId' });
    LogBehaviorSprint.belongsTo(models['Sprint'], { foreignKey: 'sprintId' });
    LogBehaviorSprint.hasMany(
      models['LogDetailBehavior'],
      {
        as: 'logDetailBehaviors',
        foreignKey: 'logDetailBehaviorTableId',
        sourceKey: 'id'
      }
    );
  };
  return LogBehaviorSprint;
};

export { LogBehaviorSprint, s, TYPE_LOG_BEHAVIOR };
