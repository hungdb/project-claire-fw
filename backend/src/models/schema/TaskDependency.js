let Dependency;

const s = (builder, Sequelize) => {
  Dependency = builder.define('TaskDependency', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      parentId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      taskId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'task_dependencies'
    });
  Dependency.associate = (models) => {
    Dependency.belongsTo(models['Task'], {as: 'parentTask', foreignKey: 'parentId'});
    Dependency.belongsTo(models['Task'], {as: 'currentTask', foreignKey: 'taskId'});
  };
  return Dependency;
};

export {Dependency, s};