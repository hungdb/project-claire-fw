let Customer;

const CUSTOMER_STATUS = {
    ACTIVE: 1,
    INACTIVE: 2,
};

const s = (builder, Sequelize) => {
    Customer = builder.define('Customer', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        company: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        status: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 1
        },
        marketId: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        note: {
          type: Sequelize.TEXT,
          allowNull: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
        avatar: {
          type: Sequelize.TEXT,
          allowNull: true,
        }
      },
      {
        tableName: 'customers'
      }
    );

    Customer.associate = (models) => {
      Customer.belongsTo(models['Market'], {as: 'market', foreignKey: 'marketId'});
    };

    return Customer;
};

export { Customer, s , CUSTOMER_STATUS }
