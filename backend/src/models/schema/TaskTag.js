let TaskTag;

const s = (builder, Sequelize) => {
  TaskTag = builder.define('TaskTag', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      projectId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      tags: {
        type: Sequelize.TEXT,
        allowNull: true
      }
    },
    {
      tableName: 'task_tags'
    });
  TaskTag.associate = (models) => {
    TaskTag.belongsTo(models['Project'], { foreignKey: 'projectId' });
  };
  return TaskTag;
};

export {TaskTag, s};