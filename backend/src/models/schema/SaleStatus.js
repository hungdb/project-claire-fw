let SaleStatus;

const s = (builder, Sequelize) => {
  SaleStatus = builder.define('SaleStatus', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date()
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'sale_statuses'
    });

    SaleStatus.associate = (models) => {
      SaleStatus.hasMany(models['Sale'], { as: 'sales', foreignKey: 'statusId', sourceKey: 'id' });
    };

    return SaleStatus;
};

export { SaleStatus, s };
