const SUBSCRIPTION_METHODS = {
  EMAIL: 1,
  SMS: 2,
  CALL: 3,
};

let AlertSubscription;

const s = (builder, Sequelize) => {
  AlertSubscription = builder.define('AlertSubscription', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    environmentIds: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: '[]',
    },
    methodIds: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: '[]',
    },
  }, {
    tableName: 'alert_subscriptions'
  });
  AlertSubscription.associate = (models) => {
    AlertSubscription.belongsTo(models['User'], {foreignKey: 'userId'});
    AlertSubscription.belongsTo(models['Project'], {foreignKey: 'projectId'});
  };
  return AlertSubscription;
};

export {AlertSubscription, s, SUBSCRIPTION_METHODS};
