let AllocationPlan;

const s = (builder, Sequelize) => {
  AllocationPlan = builder.define('AllocationPlan', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    actor: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    effortPercent: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  }, {
      tableName: 'allocation_plans'
    });
  AllocationPlan.associate = (models) => {
    AllocationPlan.belongsTo(models['User'], {foreignKey: 'userId'});
  };
  return AllocationPlan;
};

export { AllocationPlan, s };
