let Role;

const s = (builder, Sequelize) => {
  Role = builder.define('Role', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      isSuperUser: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      isEditable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
      },
      isSystemRole: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
      },
    },
    {
      tableName: 'roles'
    }
  );
  Role.associate = (models) => {
  };
  return Role;
};

export {Role, s};