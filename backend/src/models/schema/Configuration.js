let Configuration;

const s = (builder, Sequelize) => {
  Configuration = builder.define('Configuration', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    environmentId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    key: {
      type: Sequelize.STRING,
    },
    value: {
      type: Sequelize.TEXT
    }
  }, {
    tableName: 'configurations'
  });
  Configuration.associate = (models) => {
      Configuration.belongsTo(models['Environment'], {foreignKey: 'environmentId'});
  };
  return Configuration;
};

export {Configuration, s};