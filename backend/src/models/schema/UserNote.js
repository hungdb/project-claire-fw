let UserNote;

const s = (builder, Sequelize) => {
  UserNote = builder.define('UserNote', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'user_notes'
    });
  UserNote.associate = (models) => {
    UserNote.belongsTo(models['User'], { foreignKey: 'userId' });
  };
  return UserNote;
};

export {UserNote, s};