let Workflow;

const s = (builder, Sequelize) => {
  Workflow = builder.define('Workflow', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      allowParallelTask: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      tableName: 'workflows'
    }
  );
  Workflow.associate = (models) => {
  };
  return Workflow;
};

export {Workflow, s};