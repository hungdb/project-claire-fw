'use strict';

let KPISkill;

const s = (builder, Sequelize) => {
  KPISkill = builder.define('KPISkill', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    actor: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    point: {
      type: Sequelize.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    reviewPoint: {
      type: Sequelize.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    testPoint: {
      type: Sequelize.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    evaluatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
  },
  {
    tableName: 'kpi_skills',
  });

  KPISkill.associate = (models) => {
    KPISkill.belongsTo(models['User'], { as: 'user', foreignKey: 'userId' });
    KPISkill.belongsTo(models['User'], { as: 'infoActor', foreignKey: 'actor' });
  };

  return KPISkill;
};

export { KPISkill, s };

