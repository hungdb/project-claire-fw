"use strict";

let Sale;

const s = (builder, Sequelize) => {
  Sale = builder.define('Sale',
    {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true
      },
      eventId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      categoryId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      discussionContent: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      company: {
        type: Sequelize.STRING,
        allowNull: true
      },
      contact: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      priorityId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      statusId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      memoProgress: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      pic: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      memoMeeting: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      appointmentStartDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      appointmentEndDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      note: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      appointmentPlace: {
        type: Sequelize.STRING,
        allowNull: false
      },
      otherMemo: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      customerId: {
        type:Sequelize.INTEGER,
        allowNull: true,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    },
    {
      tableName: 'sales'
    }
  );

  Sale.associate = function(models) {
    // associations can be defined here
    Sale.belongsTo(models['SaleCategory'],  { as: 'saleCategory', foreignKey: 'categoryId'});
    Sale.belongsTo(models['SalePriority'],  { as: 'salePriority', foreignKey: 'priorityId'});
    Sale.belongsTo(models['SaleStatus'],  { as: 'saleStatus', foreignKey: 'statusId'});
    Sale.belongsTo(models['User'],  { as: 'picAs', foreignKey: 'pic' });
    Sale.belongsTo(models['User'],  { as: 'actorAs', foreignKey: 'actor' });
    Sale.belongsTo(models['Customer'], { as: 'customer', foreignKey: 'customerId'});
  };

  return Sale;
};

export { Sale, s }
