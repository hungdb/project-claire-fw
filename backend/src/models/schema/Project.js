let Project;

const ACTIVE_STATUS = {
  inactive: 0,
  active: 1,
  finished: 2,
};

const s = (builder, Sequelize) => {
  Project = builder.define('Project', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      groupId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
      },
      isActive: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      },
      isNotificationEnable: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      permissionSchemeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      workflowSchemeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      indexOfTask: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      projectRate: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 1,
      },
      finishedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: Sequelize.NOW
      }
    },
    {
      tableName: 'projects'
    }
  );
  Project.associate = (models) => {
    Project.belongsTo(models['Group'], {foreignKey: 'groupId'});
    Project.belongsTo(models['PermissionScheme'], {foreignKey: 'permissionSchemeId'});
    Project.belongsTo(models['Workflow'], {foreignKey: 'workflowSchemeId'});
    Project.hasMany(models['UserProjectRole'], {as: 'users', foreignKey: 'projectId', sourceKey: 'id'});
    Project.hasMany(models['Sprint'], {as: 'sprints', foreignKey: 'projectId', sourceKey: 'id'});
    Project.hasMany(models['Environment'], {as: 'environment', foreignKey: 'projectId', sourceKey: 'id'})
  };
  return Project;
};

export {Project, s, ACTIVE_STATUS};