let Issue;

const ISSUE_STATUS = {
  OPEN: 1,
  CLOSED: 2,
  SOLVING: 3
};

const s = (builder, Sequelize) => {
  Issue = builder.define('Issue', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    number: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
    subject: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
      allowNull: false
    },
    userRelate: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    projectId: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    creator: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    totalComments: {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
    typeTask: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    estimate: {
      type: Sequelize.DOUBLE,
      allowNull: true,
      defaultValue: 0
    },
    assignee: {
      type: Sequelize.INTEGER,
      allowNull: true,
    }
  },
  {
    tableName: 'issues'
  }
  );
  Issue.associate = (models) => {
    Issue.belongsTo(models['User'], { as: 'user', foreignKey: 'creator' });
    Issue.belongsTo(models['User'], { as: 'assigneeAs', foreignKey: 'assignee' });
    Issue.belongsTo(models['Project'], { as: 'project', foreignKey: 'projectId' });
    Issue.hasOne(models['Task'], {as: 'task', foreignKey: 'issueId', sourceKey: 'id'});
  };
  return Issue;
};

export { Issue, s, ISSUE_STATUS };
