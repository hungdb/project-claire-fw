let PermissionScheme;

const s = (builder, Sequelize) => {
  PermissionScheme = builder.define('PermissionScheme', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      }
    },
    {
      tableName: 'permission_schemes'
    }
  );
  PermissionScheme.associate = (models) => {
  };
  return PermissionScheme;
};

export {PermissionScheme, s};