let Document;

const s = (builder, Sequelize) => {
  Document = builder.define('Document', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      projectId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      subject: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      parentId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
    },
    {
      tableName: 'documents'
    });
  Document.associate = (models) => {
    Document.belongsTo(models['Project'], { foreignKey: 'projectId' });
    Document.belongsTo(models['User'], { foreignKey: 'userId' });
    Document.belongsTo(models['Document'], { as: 'DocumentParent', foreignKey: 'parentId' });
  };
  return Document;
};

export {Document, s};