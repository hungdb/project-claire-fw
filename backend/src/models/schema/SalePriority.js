let SalePriority;

const s = (builder, Sequelize) => {
    SalePriority = builder.define('SalePriority', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        ordinalNumber: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
      },
      {
        tableName: 'sale_priorities'
      }
    );

    SalePriority.associate = (models) => {
      SalePriority.hasMany(models['Sale'], { as: 'sales', foreignKey: 'priorityId', sourceKey: 'id' });
    };

    return SalePriority;
};

export { SalePriority, s }
