'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_project_roles',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        projectRoleId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'project_roles',
            key: 'id'
          }
        },
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_project_roles', {})
  }
};
