'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('issues',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        number: {
          type: Sequelize.DOUBLE,
          allowNull: false,
        },
        subject: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        content: {
          type: Sequelize.TEXT,
          allowNull: true
        },
        status: {
          type: Sequelize.INTEGER,
          defaultValue: 1,
          allowNull: false,
        },
        taskRelate: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        creator: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        totalComments: {
          type: Sequelize.DOUBLE,
          allowNull: false,
          defaultValue: 0
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('issues', {});
  }
};
