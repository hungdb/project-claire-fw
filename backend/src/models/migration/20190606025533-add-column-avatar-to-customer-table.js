'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'customers',
      'avatar',
      {
        type: Sequelize.TEXT,
        allowNull: true,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'customers',
      'avatar'
    );
  }
};
