'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
        token: {
          type: Sequelize.TEXT,
        },
        // name: {
        //   type: Sequelize.STRING
        // }
      },
      {
        charset: 'utf8'
      });
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users', {});
  }
};
