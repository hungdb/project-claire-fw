'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tasks',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        content: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        creator: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        estimate: {
          type: Sequelize.DOUBLE,
          defaultValue: 0,
          allowNull: true,
        },
        sprintId: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'sprints',
            key: 'id'
          }
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        tag: {
          type: Sequelize.TEXT,
        },
        description: {
          type: Sequelize.TEXT,
          allowNull: true
        }
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tasks', {});
  }
};
