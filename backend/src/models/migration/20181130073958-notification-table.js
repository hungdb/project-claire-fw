'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('notifications',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        taskId: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'tasks',
            key: 'id'
          }
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        isRead: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        content: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('notifications', {});
  }
};
