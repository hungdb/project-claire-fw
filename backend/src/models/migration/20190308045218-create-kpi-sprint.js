'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('kpi_sprints', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      point: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      sprintId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'sprints',
          key: 'id',
        },
      },
      projectId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'projects',
          key: 'id',
        },
      },
      workManagementPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      peopleManagementPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      processPoint: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      taskComplete: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      type: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('kpi_sprints', {});
  }
};