'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('comments', 'comments_ibfk_2');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('comments', ['taskId'], {
      type: 'foreign key',
      name: 'comments_ibfk_2',
      references: {
        table: 'tasks',
        field: 'id'
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  }
};
