'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const columns = [
      queryInterface.addColumn(
        'user_skills',
        'projectId',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'projects',
            key: 'id',
          },
        }
      ),
      queryInterface.addColumn(
        'user_skills',
        'type',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 1,
        }
      ),
      queryInterface.addColumn(
        'user_skills',
        'sprintId',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'sprints',
            key: 'id',
          },
        }
      ),
    ];
    return Promise.all(columns);
  },

  down: (queryInterface, Sequelize) => {
    const removedColumns = [
      queryInterface.removeColumn('user_skills','sprintId'),
      queryInterface.removeColumn('user_skills','type'),
      queryInterface.removeColumn('user_skills','projectId'),
    ];
    return Promise.all(removedColumns);
  }
};
