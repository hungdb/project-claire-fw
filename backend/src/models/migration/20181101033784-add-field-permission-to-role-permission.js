'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'role_permissions',
      'permission',
      {
        type: Sequelize.STRING,
        allowNull: false
      }

    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'role_permissions',
      'permission'
    );
  }
};
