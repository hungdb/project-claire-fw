'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'log_behavior_sprints',
      'type',
      {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 1,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'log_behavior_sprints',
      'type',
    );
  }
};
