'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'projects',
      'projectRate',
      {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 1,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'projects',
      'projectRate',
    );
  }
};
