'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const addColumns = [
      queryInterface.addColumn(
        'user_skills',
        'actor',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          references:{
            model: 'users',
            key: 'id'
          }
        },
      ),
      queryInterface.addColumn(
        'user_skills',
        'note',
        {
          type: Sequelize.TEXT,
          defaultValue: '',
          allowNull: true,
        },
      ),
    ];
    return Promise.all(addColumns);
  },

  down: (queryInterface, Sequelize) => {
    const removedColumns = [
      queryInterface.removeColumn('user_skills', 'actor'),
      queryInterface.removeColumn('user_skills', 'note'),
    ];
    return Promise.all(removedColumns);
  },
};
