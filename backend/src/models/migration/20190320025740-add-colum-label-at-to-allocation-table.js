'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'allocations',
      'label',
      {
        type:Sequelize.INTEGER,
        allowNull: true,
      }
    )
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.removeColumn(
    'allocations',
    'label',
   )
  }
};

