'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
      'behaviors',
      'startDate',
      {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }),
      queryInterface.addColumn(
        'behaviors',
        'endDate',
        {
          type: Sequelize.DATE,
          allowNull: true,
          defaultValue: null
        }
      ),
      queryInterface.addColumn(
        'behaviors',
        'description',
        {
          type: Sequelize.TEXT,
          allowNull: true,
          defaultValue: null
        }
      ),
      queryInterface.addColumn(
        'behaviors',
        'typeSubApply',
        {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        'behaviors',
        'startDate',
      ),
      queryInterface.removeColumn(
        'behaviors',
        'endDate',
      ),
      queryInterface.removeColumn(
        'behaviors',
        'description',
      ),
      queryInterface.removeColumn(
        'behaviors',
        'typeSubApply',
      ),
    ]);
  }
};
