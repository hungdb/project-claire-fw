'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('log_detail_behaviors',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        behaviorId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'behaviors',
            key: 'id',
          },
        },
        logDetailBehaviorTableId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        logDetailBehaviorTableType: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('log_detail_behaviors', {});
  }
};
