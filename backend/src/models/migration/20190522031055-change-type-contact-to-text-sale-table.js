'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn("sales", 'contact', {
      type: Sequelize.TEXT,
      allowNull: true
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn("sales", 'contact', {
      type: Sequelize.STRING,
      allowNull: true
    });
  }
};
