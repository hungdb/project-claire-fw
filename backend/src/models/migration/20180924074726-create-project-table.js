'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('projects', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        groupId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'project_groups',
            key: 'id'
          }
        },
        name: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
        },
        description: {
          type: Sequelize.TEXT,
        },
        isActive: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        isNotificationEnable: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        permissionSchemeId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'permission_schemes',
            key: 'id'
          }
        },
        workflowSchemeId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'workflows',
            key: 'id'
          }
        }
      },
      {
        charset: 'utf8'
      });
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('projects', {});
  }
};
