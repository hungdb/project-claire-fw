'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('workflow_details',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        workflowId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references:{
            model: 'workflows',
            key: 'id'
          }
        },
        fromState: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'workflow_states',
            key: 'id'
          }
        },
        toState: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'workflow_states',
            key: 'id'
          }
        },
        description: {
          type: Sequelize.TEXT,
          allowNull: true
        }
      },
      {
      
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('workflow_details', {});
  }
};
