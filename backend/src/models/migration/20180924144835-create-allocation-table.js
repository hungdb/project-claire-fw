'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('allocations',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references:{
            model: 'users',
            key: 'id'
          }
        },
        sprintId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'sprints',
            key: 'id'
          }
        },
        effortPercent: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        startDate: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        endDate: {
          type: Sequelize.DATE,
          allowNull: false
        }
      },
      {
        charset: 'utf8'
      })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('allocations', {});
  }
};
