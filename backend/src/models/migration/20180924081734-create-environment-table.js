'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('environments',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        serverHealthCheck: {
          type: Sequelize.STRING,
        },
        HcThresholdDown: {
          type: Sequelize.INTEGER
        },
        HcThresholdUp: {
          type: Sequelize.INTEGER
        },
        secretKey: {
          type: Sequelize.STRING
        }
      },
      {
        charset: 'utf8'
      });
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('environments', {});
  }
};
