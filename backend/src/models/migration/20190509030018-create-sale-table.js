'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sales', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true
      },
      eventId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      categoryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'sale_categories',
          key: 'id'
        }
      },
      company: {
        type: Sequelize.STRING,
        allowNull: true
      },
      discussionContent: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      contact: {
        type: Sequelize.STRING,
        allowNull: true
      },
      priorityId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'sale_priorities',
          key: 'id'
        }
      },
      statusId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'sale_statuses',
          key: 'id'
        }
      },
      memoProgress: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      pic: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      memoMeeting: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      appointmentDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      appointmentPlace: {
        type: Sequelize.STRING,
        allowNull: false
      },
      otherMemo: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    },
    {
      charset: 'utf8'
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('sales', {});
  }
};