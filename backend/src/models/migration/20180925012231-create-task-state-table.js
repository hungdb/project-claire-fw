'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('task_states',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        taskId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'tasks',
            key: 'id'
          }
        },
        fromState: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'workflow_states',
            key: 'id'
          }
        },
        toState: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'workflow_states',
            key: 'id'
          }
        },
        editor: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('task_states', {})
  }
};
