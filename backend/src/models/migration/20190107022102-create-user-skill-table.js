'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_skills', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        skillId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'skills',
            key: 'id'
          }
        },
        evaluationPoint: {
          type: Sequelize.INTEGER,
          allowNull: false
        }
      },
      {
        charset: 'utf8'
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_skills', {});
  }
};
