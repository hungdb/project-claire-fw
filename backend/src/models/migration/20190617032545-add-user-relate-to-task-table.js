'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tasks',
      'userRelate',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: JSON.stringify([])
      }
    )
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.removeColumn(
    'tasks',
    'userRelate',
   )
  }
};
