'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('role_permissions',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        roleId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'roles',
            key: 'id'
          }
        },
        // permission: {
        //   type: Sequelize.STRING,
        //   allowNull: false,
        // }
      },
      {
        charset: 'utf8'
      })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('role_permissions', {});
  }
};
