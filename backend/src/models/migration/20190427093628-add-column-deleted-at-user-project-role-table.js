'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'user_project_roles',
      'deletedAt',
      {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'user_project_roles',
      'deletedAt',
    )
  }
};
