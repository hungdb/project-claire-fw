'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('notifications',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        notificationableId: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        notificationableType: {
          type: Sequelize.TEXT,
          allowNull: true
        },
        isRead: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        content: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        type: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('notifications', {});
  }
};
