'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('comments', 'taskId', 'commentTableId');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('comments', 'commentTableId', 'taskId');
  }
};
