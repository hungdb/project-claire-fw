'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('allocation_plans',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references:{
            model: 'users',
            key: 'id'
          }
        },
        actor: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references:{
            model: 'users',
            key: 'id'
          }
        },
        effortPercent: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        startDate: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        endDate: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.NOW
        }
      },
      {
        charset: 'utf8'
      })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('allocation_plans', {});
  }
};
