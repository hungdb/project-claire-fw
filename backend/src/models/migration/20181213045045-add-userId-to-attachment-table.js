'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'attachments',
      'userId',
      {
        type: Sequelize.INTEGER,
        references:{
          model: 'users',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'attachments',
      'userId'
    );
  }
};
