'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('behaviors', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      activity: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      point: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0,
      },
      typeApply: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      typeAction: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      decider: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      note: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      isApply: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
    }, {
      charset: 'utf8'
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('behaviors', {});
  }
};
