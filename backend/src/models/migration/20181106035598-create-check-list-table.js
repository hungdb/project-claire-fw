'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('check_lists',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        checkItemId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'check_list_items',
            key: 'id'
          }
        },
        isChecked: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
      },
      {
        charset: 'utf8'
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('check_lists', {});
  }
};
