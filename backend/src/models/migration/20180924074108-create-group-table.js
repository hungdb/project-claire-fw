'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('project_groups', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
        },
      },
      {
        charset: 'utf8'
      });
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('project_groups', {});
  }
};
