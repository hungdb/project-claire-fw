'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'skills',
      'linkIcon',
      {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'skills',
      'linkIcon'
    );
  }
};
