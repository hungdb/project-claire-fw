'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('alert_subscriptions',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        projectId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'projects',
            key: 'id'
          }
        },
        environmentIds: {
          type: Sequelize.STRING,
          defaultValue: '[]',
          allowNull: false,
        },
        methodIds: {
          type: Sequelize.STRING,
          defaultValue: '[]',
          allowNull: false,
        },
      },
      {
        charset: 'utf8'
      }
    );
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('alert_subscriptions', {})
  }
};
