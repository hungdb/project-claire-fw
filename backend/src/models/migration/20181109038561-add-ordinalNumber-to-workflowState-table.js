'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn(
        'workflow_states',
        'ordinalNumber',
        {
          type: Sequelize.INTEGER,
          allowNull: false
        }
      ),
    ];
  },

  down: (queryInterface, Sequelize) => {
    return [
      queryInterface.removeColumn(
        'workflow_states',
        'ordinalNumber'
      )
    ];
  }
};
