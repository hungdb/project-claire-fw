'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn(
        'workflow_states',
        'workflowId',
        {
          type: Sequelize.INTEGER,
          references:{
            model: 'workflows',
            key: 'id'
          }
        }
      )
    ];
  },

  down: (queryInterface, Sequelize) => {
    return [
      queryInterface.removeColumn(
        'workflow_states',
        'workflowId'
      )
    ];
  }
};
