'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('environments', 'name');
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('environments', ['name'], {type: 'unique', name: 'name'});
  }
};
