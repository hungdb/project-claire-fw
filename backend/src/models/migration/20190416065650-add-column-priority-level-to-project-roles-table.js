'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'project_roles',
      'priorityLevel',
      {
        type:Sequelize.INTEGER,
        allowNull: true,
      }
    )
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.removeColumn(
    'project_roles',
    'priorityLevel',
   )
  }
};
