'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('roles',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
        },
        isSuperUser: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false,
        },
        isEditable: {
          type: Sequelize.BOOLEAN,
          defaultValue: true,
          allowNull: false
        },
      },
      {
        charset: 'utf8'
      })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('roles', {});
  }
};
