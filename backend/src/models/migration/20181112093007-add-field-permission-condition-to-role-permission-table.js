'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'role_permissions',
      'permissionCondition',
      {
        type: Sequelize.TEXT,
        defaultValue: JSON.stringify([])
      }

    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'role_permissions',
      'permissionCondition'
    );
  }
};
