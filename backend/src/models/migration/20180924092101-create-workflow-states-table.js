'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('workflow_states',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
        },
        effortType: {
          type: Sequelize.STRING,
          allowNull: false
        }
      },
      {
        charset: 'utf8'
      })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('workflow_states', {});
  }
};
