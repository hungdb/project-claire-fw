'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'sales',
      'appointmentDate',
      'appointmentStartDate'
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'sales',
      'appointmentStartDate',
      'appointmentDate'
    )}
};
