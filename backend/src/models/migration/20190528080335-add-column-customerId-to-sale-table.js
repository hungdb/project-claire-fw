'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'sales',
      'customerId',
      {
        type:Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'customers',
          key: 'id',
        }
      }
    )
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.removeColumn(
    'sales',
    'customerId',
   )
  }
};
