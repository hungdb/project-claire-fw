'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
   return queryInterface.addColumn(
     'issues',
     'assignee',
     {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'users',
          key: 'id'
        }
      }
   )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'issues',
      'assignee'
    )
  }
};
