'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tasks',
      'issueId',
      {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'issues',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'tasks',
      'issueId'
    );
  }
};
