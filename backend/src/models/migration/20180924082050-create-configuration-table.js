'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('configurations',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        environmentId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'environments',
            key: 'id'
          }
        },
        key: {
          type: Sequelize.STRING,
        },
        value: {
          type: Sequelize.TEXT
        }
      },
      {
        charset: 'utf8'
      });
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('configurations', {});
  }
};
