'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('kpi_skills', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      actor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      point: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      reviewPoint: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      testPoint: {
        type: Sequelize.FLOAT,
        defaultValue: 0,
        allowNull: false,
      },
      evaluatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('kpi_skills', {});
  }
};