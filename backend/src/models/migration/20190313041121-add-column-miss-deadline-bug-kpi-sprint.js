'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const addColumns = [
      queryInterface.addColumn(
        'kpi_sprints',
        'missDeadline',
        {
          type: Sequelize.FLOAT,
          allowNull: false,
          defaultValue: 0,
        },
      ),
      queryInterface.addColumn(
        'kpi_sprints',
        'errorRelate',
        {
          type: Sequelize.FLOAT,
          allowNull: false,
          defaultValue: 0,
        },
      ),
    ];
    return Promise.all(addColumns);
  },

  down: (queryInterface, Sequelize) => {
    const removedColumns = [
      queryInterface.removeColumn('kpi_sprints', 'missDeadline'),
      queryInterface.removeColumn('kpi_sprints', 'errorRelate'),
    ];
    return Promise.all(removedColumns);
  },
};
