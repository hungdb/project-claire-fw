'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('permission_scheme_details',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        permissionSchemeId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'permission_schemes',
            key: 'id'
          }
        },
        projectRoleId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'project_roles',
            key: 'id'
          }
        },
        permission: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        permissionCondition: {
          type: Sequelize.TEXT,
          allowNull: true
        }
      },
      {
        charset: 'utf8'
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('permission_scheme_details', {});
  }
};
