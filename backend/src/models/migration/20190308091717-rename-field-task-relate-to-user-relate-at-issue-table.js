'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('issues', 'taskRelate', 'userRelate');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('issues', 'userRelate', 'taskRelate');
  }
};
