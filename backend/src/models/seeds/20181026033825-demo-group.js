'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('project_groups', 
      [
        { id: 1, name: "ARM" },
        { id: 2, name: "Internal" },
        { id: 3, name: "Research" },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data project_groups: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('project_groups', null, {});
  }
};
