'use strict';
const date = new Date();
const mockData = [
  { name: "Sprint 1",
    status: "Closed",
    projectId: "1",
    startDate: new Date((new Date()).setMonth(date.getMonth() - 2)),
    endDate: new Date((new Date()).setMonth(date.getMonth() - 1))
  },
  { name: "Sprint 2",
    status: "Closed",
    projectId: "1",
    startDate: new Date((new Date()).setDate(date.getDate() - 10)),
    endDate: new Date((new Date()).setDate(date.getDate() - 3))
  },
  { name: "Sprint 3",
    status: "Active",
    projectId: "1",
    startDate: date,
    endDate: new Date((new Date()).setDate(date.getDate() + 7))
  },        
  { name: "Sprint 1-2",
    status: "Closed",
    projectId: "2",
    startDate: new Date((new Date()).setMonth(date.getMonth() - 2)),
    endDate: new Date((new Date()).setMonth(date.getMonth() - 1))
  },
  { name: "Sprint 2-2",
    status: "Closed",
    projectId: "2",
    startDate: new Date((new Date()).setDate(date.getDate() - 10)),
    endDate: new Date((new Date()).setDate(date.getDate() - 3))
  },
  { name: "Sprint 3-2",
    status: "Active",
    projectId: "2",
    startDate: date,
    endDate: new Date((new Date()).setDate(date.getDate() + 7))
  }, 
]

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('sprints', mockData, {})
      .catch(
        (err) => {
          console.log("Error seed data sprints: ", err);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('sprints', null, {});
  }
};
