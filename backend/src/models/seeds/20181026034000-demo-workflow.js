'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('workflows', 
      [
        { name: "Default workflow" },
        { name: "Double testing" },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data workflows: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('workflows', null, {});
  }
};
