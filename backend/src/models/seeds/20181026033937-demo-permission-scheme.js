'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('permission_schemes',
      [
        { name: "Default permision scheme" },
        { name: "Leader with full permission" },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data permission_schemes: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('permission_schemes', null, {});
  }
};
