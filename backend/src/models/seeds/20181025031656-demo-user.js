'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let users = [];
    for (let i = 1; i < 80; i++) {
      let u = { id: i, name: "user"+i };
      users.push(u);
    }
    return queryInterface.bulkInsert('users', users, {})
      .catch(
        (err) => {
          console.log("Error seed data user: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all(
      [
        queryInterface.bulkDelete('user_roles', null, {}),
        queryInterface.bulkDelete('users', null, {})
      ]
    );
  }
};
