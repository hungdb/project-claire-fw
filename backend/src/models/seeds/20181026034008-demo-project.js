'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('projects',
      [
        { groupId: 1,
          name: "TRIAM Network",
          description: "Lorem",
          isActive: true,
          isNotificationEnable: false,
          permissionSchemeId: 1,
          workflowSchemeId: 1,
        },
        { groupId: 1,
          name: "Greenbox wallet",
          description: "Lorem paerm",
          isActive: true,
          isNotificationEnable: false,
          permissionSchemeId: 1,
          workflowSchemeId: 1,
        },
        { groupId: 2,
          name: "Project Management System",
          description: "Lorem paerm lomkr",
          isActive: true,
          isNotificationEnable: false,
          permissionSchemeId: 1,
          workflowSchemeId: 1,
        }
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data projects: ", err);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('projects', null, {});
  }
};
