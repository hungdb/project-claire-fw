'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_roles', [{id: 1, userId: 1, roleId: 1}], {})
      .catch(
        (err) => {
          console.log("Error seed data user: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all(
      [
        queryInterface.bulkDelete('user_roles', null, {}),
      ]
    );
  }
};
