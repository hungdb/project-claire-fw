'use strict';
const date = new Date();

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('task_states', 
      [
        { taskId: 1,
          fromState: 1,
          toState: 2,
          editor: 1,
          createdAt: new Date((new Date()).setDate(date.getDate() - 2)) 
        },
        { taskId: 1,
          fromState: 2,
          toState: 3,
          editor: 1,
          createdAt: new Date((new Date()).setDate(date.getDate() - 1)) 
        },
        { taskId: 1,
          fromState: 3,
          toState: 4,
          editor: 1,
          createdAt: date 
        },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data task_states: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('task_states', null, {});
  }
};
