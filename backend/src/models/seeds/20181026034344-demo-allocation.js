'use strict';
const date = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('allocations', 
      [
        { userId: 1,
        sprintId: 1,
        effortPercent: 20,
        startDate: new Date((new Date()).setDate(date.getDate() - 2)),
        endDate: new Date((new Date()).setDate(date.getDate() + 5))
        },
        { userId: 1,
          sprintId: 1,
          effortPercent: 50,
          startDate: new Date((new Date()).setDate(date.getDate() + 6)),
          endDate: new Date((new Date()).setDate(date.getDate() + 12))
        },
        { userId: 1,
          sprintId: 6,
          effortPercent: 60,
          startDate: new Date((new Date()).setDate(date.getDate() - 6)),
          endDate: new Date((new Date()).setDate(date.getDate() + 2))
        },
        { userId: 1,
          sprintId: 4,
          effortPercent: 50,
          startDate: new Date((new Date()).setDate(date.getDate() + 6)),
          endDate: new Date((new Date()).setDate(date.getDate() + 10))
        },
        { userId: 2,
          sprintId: 1,
          effortPercent: 50,
          startDate: new Date((new Date()).setDate(date.getDate() - 3)),
          endDate: new Date((new Date()).setDate(date.getDate() + 4))
        },
        { userId: 2,
          sprintId: 5,
          effortPercent: 50,
          startDate: new Date((new Date()).setDate(date.getDate() + 2)),
          endDate: new Date((new Date()).setDate(date.getDate() + 10))
        },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data allocations: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('allocations', null, {});
  }
};
