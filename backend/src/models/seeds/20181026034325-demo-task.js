'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('tasks', 
      [
        {
          content: "Login function",
          creator: 1,
          estimate: 10,
          sprintId: 1,
          projectId: 1,
          tag: "[\"bug\", \"urgent\"]",
          description: "Login with identity...",
          createdAt: new Date()
        },
        {
          content: "[FRONT END] Login function",
          creator: 1,
          estimate: 10,
          sprintId: 1,
          projectId: 1,
          tag: "[\"feature\"]",
          description: "Frontend login with identity...",
          createdAt: new Date()
        },
        {
          content: "Get list users",
          creator: 1,
          estimate: 10,
          sprintId: 1,
          projectId: 1,
          tag: "[\"backend\"]",
          description: "List users from identity...",
          createdAt: new Date()
        },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data tasks: ", err);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('tasks', null, {});
  }
};
