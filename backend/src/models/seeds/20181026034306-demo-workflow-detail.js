'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('workflow_details', 
      [
        { workflowId: 1,
          fromState: 1,
          toState: 2,
          description: "Assignment"
        },
        { workflowId: 1,
          fromState: 2,
          toState: 3,
          description: "To test"
        },
        { workflowId: 1,
          fromState: 3,
          toState: 4,
          description: "Close"
        },
        { workflowId: 1,
          fromState: 3,
          toState: 5,
          description: "Bug, reopen"
        },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data workflow_details: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('workflow_details', null, {});
  }
};
