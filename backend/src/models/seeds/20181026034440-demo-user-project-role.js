'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_project_roles', 
      [
        { userId: 1,
          projectId: 1,
          projectRoleId: 1
        },
        { userId: 2,
          projectId: 1,
          projectRoleId: 2
        },
        { userId: 3,
          projectId: 1,
          projectRoleId: 3
        },
        { userId: 4,
          projectId: 1,
          projectRoleId: 4
        },
        { userId: 1,
          projectId: 2,
          projectRoleId: 2
        },
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data user_project_roles: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_project_roles', null, {});
  }
};
