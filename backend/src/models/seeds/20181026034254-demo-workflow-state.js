'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('workflow_states', 
      [
        { name: "To do",
          effortType: "untouched"
        },
        { name: "Inprogress",
          effortType: "holding"
        },
        { name: "Testing",
          effortType: "progress"
        },
        { name: "Done",
          effortType: "terminated"
        },
        { name: "Reopen",
          effortType: "untouched"
        },
      
      ], {})
      .catch(
        (err) => {
          console.log("Error seed data workflow_states: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('workflow_states', null, {});
  }
};
