'use strict';

const permission_scheme_details_mockData = [
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "get_project",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "list_projects",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "list_groups",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "list_roles",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "list_workflow",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "list_users",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "create_sprint",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
  { permissionSchemeId: 1,
    projectRoleId: 1,
    permission: "get_sprint",
    permissionCondition: JSON.stringify([{
      name: "subset",
      value: ["id", "name"]
    },
    {
      name: "regexStringValue",
      value: ["id", "name"]
    },
    {
      name: "editableFields",
      value: ["id", "name"]
    }])
  },
]

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('permission_scheme_details', permission_scheme_details_mockData, {})
      .catch(
        (err) => {
          console.log("Error seed data permission_scheme_details: ", err.errors);
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('permission_scheme_details', null, {});
  }
};
