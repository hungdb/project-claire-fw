/*
 *=================================================================================================
 * Notification Constants
 *=================================================================================================
 */
export const GET_NOTIFICATIONS_PROGRESS = 'GET_NOTIFICATIONS_PROGRESS';
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS';
export const GET_NOTIFICATIONS_FAIL = 'GET_NOTIFICATIONS_FAIL';

export const LOAD_MORE_NOTIFICATIONS_PROGRESS = 'LOAD_MORE_NOTIFICATIONS_PROGRESS';
export const LOAD_MORE_NOTIFICATIONS_SUCCESS = 'LOAD_MORE_NOTIFICATIONS_SUCCESS';
export const LOAD_MORE_NOTIFICATIONS_FAILED = 'LOAD_MORE_NOTIFICATIONS_FAILED';

export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';

export const TYPE_TASK_STATE_CHANGE = 'task_state_change';
export const TYPE_PROJECT_ROLE_CHANGE = 'change_role_project';
export const TYPE_NEW_COMMENT = 'new_comment';
export const TYPE_NEW_TASK = 'new_task';
export const TYPE_REMOVE_TASK = 'remove_task';
export const TYPE_TIME_ACTIVE_SPRINT_CHANGE = 'time_active_sprint_change';
export const TYPE_ASSIGN_TASK_PROJECT = 'assign_task_project';

export const UPDATE_NOTIFICATION = 'UPDATE_NOTIFICATION';
export const UPDATE_NOTIFICATION_SUCCESS = 'UPDATE_NOTIFICATION_SUCCESS';
export const UPDATE_NOTIFICATION_FAILED = 'UPDATE_NOTIFICATION_FAILED';
export const READ_ALL_NOTIFICATIONS = 'READ_ALL_NOTIFICATIONS';
