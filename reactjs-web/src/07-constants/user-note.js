/*
 *=================================================================================================
 * User Note Constants
 *=================================================================================================
 */
export const CREATE_USER_NOTE = 'CREATE_USER_NOTE';
export const CREATE_USER_NOTE_SUCCESS = 'CREATE_USER_NOTE_SUCCESS';
export const CREATE_USER_NOTE_FAILED = 'CREATE_USER_NOTE_FAILED';
export const GET_USER_NOTES = 'GET_USER_NOTES';
export const GET_USER_NOTES_SUCCESS = 'GET_USER_NOTES_SUCCESS';
export const GET_USER_NOTES_FAILED = 'GET_USER_NOTES_FAILED';
export const UPDATE_USER_NOTE = 'UPDATE_USER_NOTE';
export const UPDATE_USER_NOTE_SUCCESS = 'UPDATE_USER_NOTE_SUCCESS';
export const UPDATE_USER_NOTE_FAILED = 'UPDATE_USER_NOTE_FAILED';
export const DELETE_USER_NOTE = 'DELETE_USER_NOTE';
export const DELETE_USER_NOTE_SUCCESS = 'DELETE_USER_NOTE_SUCCESS';
export const DELETE_USER_NOTE_FAILED = 'DELETE_USER_NOTE_FAILED';
