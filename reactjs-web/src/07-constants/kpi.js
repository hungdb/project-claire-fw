/*
 *=================================================================================================
 * KPI Constants
 *=================================================================================================
 */
export const LIST_USERS_PROGRESS = 'LIST_KPI_USERS_PROGRESS';
export const LIST_USERS_SUCCESS = 'LIST_KPI_USERS_SUCCESS';
export const LIST_USERS_FAILED = 'LIST_KPI_USERS_FAILED';
export const CLEAR_LIST_USERS = 'CLEAR_KPI_LIST_USERS';
export const GET_LIST_KPI_TO_CHART_PROGRESS = 'GET_LIST_KPI_TO_CHART_PROGRESS';
export const GET_LIST_KPI_TO_CHART_SUCCESS = 'GET_LIST_KPI_TO_CHART_SUCCESS';
export const GET_LIST_KPI_TO_CHART_FAILED = 'GET_LIST_KPI_TO_CHART_FAILED';
export const GET_MY_LIST_KPI_TO_CHART_PROGRESS = 'GET_MY_LIST_KPI_TO_CHART_PROGRESS';
export const GET_MY_LIST_KPI_TO_CHART_SUCCESS = 'GET_MY_LIST_KPI_TO_CHART_SUCCESS';
export const GET_MY_LIST_KPI_TO_CHART_FAILED = 'GET_MY_LIST_KPI_TO_CHART_FAILED';
export const CLEAR_KPI_CHART = 'CLEAR_KPI_CHART';
export const FILTER_BY_START_DATE = 'FILTER_KPI_BY_START_DATE';
export const FILTER_BY_END_DATE = 'FILTER_KPI_BY_END_DATE';
export const FILTER_BY_PROJECT = 'FILTER_KPI_BY_PROJECT';
export const FILTER_BY_USER = 'FILTER_KPI_BY_USER';
export const FILTER_BY_TYPE = 'FILTER_KPI_BY_TYPE';
export const FILTER_BY_TYPE_TIME = 'FILTER_KPI_BY_TYPE_TIME';
export const FILTER_BY_YEAR = 'FILTER_KPI_BY_YEAR';
