/*
 *=================================================================================================
 * Market Constants
 *=================================================================================================
 */
export const GET_LIST_MARKET_PROGRESS = 'GET_LIST_MARKET_PROGRESS';
export const GET_LIST_MARKET_SUCCESS = 'GET_LIST_MARKET_SUCCESS';
export const GET_LIST_MARKET_FAIL = 'GET_LIST_MARKET_FAIL';

export const CREATE_MARKET_PROGRESS = 'CREATE_MARKET_PROGRESS';
export const CREATE_MARKET_SUCCESS = 'CREATE_MARKET_SUCCESS';
export const CREATE_MARKET_FAIL = 'CREATE_MARKET_FAIL';

export const UPDATE_MARKET_PROGRESS = 'UPDATE_MARKET_PROGRESS';
export const UPDATE_MARKET_SUCCESS = 'UPDATE_MARKET_SUCCESS';
export const UPDATE_MARKET_FAIL = 'UPDATE_MARKET_FAIL';

export const REMOVE_MARKET_PROGRESS = 'REMOVE_MARKET_PROGRESS';
export const REMOVE_MARKET_SUCCESS = 'REMOVE_MARKET_SUCCESS';
export const REMOVE_MARKET_FAIL = 'REMOVE_MARKET_FAIL';
