/*
 *=================================================================================================
 * Common Constants
 *=================================================================================================
 */
export const CHECK_ERROR = 'CHECK_ERROR';
export const USER_ACCESS_TOKEN = 'USER_ACCESS_TOKEN';
export const USER_INFO = 'USER_INFO';
export const USER_LOGIN_TOKEN = 'USER_LOGIN_TOKEN';
export const LIGHT_BACKGROUND_TOGGLE = 'LIGHT_BACKGROUND_TOGGLE';
export const FULL_BOARD_TOGGLE = 'FULL_BOARD_TOGGLE';
