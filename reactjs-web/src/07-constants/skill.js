/*
 *=================================================================================================
 * Skill Constants
 *=================================================================================================
 */
export const LIST_SKILL_PROGRESS = 'LIST_SKILL_PROGRESS';
export const LIST_SKILL_SUCCESS = 'LIST_SKILL_SUCCESS';
export const LIST_SKILL_FAILED = 'LIST_SKILL_FAILED';
export const REMOVE_SKILL_PROGRESS = 'REMOVE_SKILL_PROGRESS';
export const REMOVE_SKILL_SUCCESS = 'REMOVE_SKILL_SUCCESS';
export const REMOVE_SKILL_FAILED = 'REMOVE_SKILL_FAILED';
export const CREATE_SKILL_PROGRESS = 'CREATE_SKILL_PROGRESS';
export const CREATE_SKILL_SUCCESS = 'CREATE_SKILL_SUCCESS';
export const CREATE_SKILL_FAILED = 'CREATE_SKILL_FAILED';
export const GET_SKILL_PROGRESS = 'GET_SKILL_PROGRESS';
export const GET_SKILL_SUCCESS = 'GET_SKILL_SUCCESS';
export const GET_SKILL_FAILED = 'GET_SKILL_FAILED';
export const UPDATE_SKILL_PROGRESS = 'UPDATE_SKILL_PROGRESS';
export const UPDATE_SKILL_SUCCESS = 'UPDATE_SKILL_SUCCESS';
export const UPDATE_SKILL_FAILED = 'UPDATE_SKILL_FAILED';
export const HANDLE_CHANGE_ROWS_PER_PAGE = 'HANDLE_CHANGE_ROWS_PER_PAGE';
export const HANDLE_CHANGE_PAGE = 'HANDLE_CHANGE_PAGE';
export const LIST_ALL_SKILL_PROGRESS = 'LIST_ALL_SKILL_PROGRESS';
export const LIST_ALL_SKILL_SUCCESS = 'LIST_ALL_SKILL_SUCCESS';
export const LIST_ALL_SKILL_FAILED = 'LIST_ALL_SKILL_FAILED';
export const FILTER_BY_USER_ID_SKILL = 'FILTER_BY_USER_ID_SKILL';

/*
 *=================================================================================================
 * User Skill Constants
 *=================================================================================================
 */
export const LIST_SKILL_BY_USER_PROGRESS = 'LIST_SKILL_BY_USER_PROGRESS';
export const LIST_SKILL_BY_USER_SUCCESS = 'LIST_SKILL_BY_USER_SUCCESS';
export const LIST_SKILL_BY_USER_FAILED = 'LIST_SKILL_BY_USER_FAILED';
export const LIST_USER_BY_SKILL_PROGRESS = 'LIST_USER_BY_SKILL_PROGRESS';
export const LIST_USER_BY_SKILL_SUCCESS = 'LIST_USER_BY_SKILL_SUCCESS';
export const LIST_USER_BY_SKILL_FAILED = 'LIST_USER_BY_SKILL_FAILED';
export const GET_USER_SKILL_PROGRESS = 'GET_USER_SKILL_PROGRESS';
export const GET_USER_SKILL_SUCCESS = 'GET_USER_SKILL_SUCCESS';
export const GET_USER_SKILL_FAILED = 'GET_USER_SKILL_FAILED';
export const REMOVE_USER_SKILL_PROGRESS = 'REMOVE_USER_SKILL_PROGRESS';
export const REMOVE_USER_SKILL_SUCCESS = 'REMOVE_USER_SKILL_SUCCESS';
export const REMOVE_USER_SKILL_FAILED = 'REMOVE_USER_SKILL_FAILED';
export const CREATE_USER_SKILL_PROGRESS = 'CREATE_USER_SKILL_PROGRESS';
export const CREATE_USER_SKILL_SUCCESS = 'CREATE_USER_SKILL_SUCCESS';
export const CREATE_USER_SKILL_FAILED = 'CREATE_USER_SKILL_FAILED';
export const UPDATE_USER_SKILL_PROGRESS = 'UPDATE_USER_SKILL_PROGRESS';
export const UPDATE_USER_SKILL_SUCCESS = 'UPDATE_USER_SKILL_SUCCESS';
export const UPDATE_USER_SKILL_FAILED = 'UPDATE_USER_SKILL_FAILED';
export const CLEAR_USER_SKILL = 'CLEAR_USER_SKILL';
export const FILTER_BY_USER_ID_KPI = 'FILTER_BY_USER_ID_KPI';

/*
 *=================================================================================================
 * Get Skills Of All Users
 *=================================================================================================
 */
export const GET_SKILLS_OF_ALL_USERS_PROGRESS = 'GET_SKILLS_OF_ALL_USERS_PROGRESS';
export const GET_SKILLS_OF_ALL_USERS_SUCCESS = 'GET_SKILLS_OF_ALL_USERS_SUCCESS';
export const GET_SKILLS_OF_ALL_USERS_FAILED = 'GET_SKILLS_OF_ALL_USERS_FAILED';

export const CREATE_KPI_SKILL_PROGRESS = 'CREATE_KPI_SKILL_PROGRESS';
export const CREATE_KPI_SKILL_SUCCESS = 'CREATE_KPI_SKILL_SUCCESS';
export const CREATE_KPI_SKILL_FAILED = 'CREATE_KPI_SKILL_FAILED';

export const GET_LIST_KPI_SKILL_OF_USER_PROGRESS = 'GET_LIST_KPI_SKILL_OF_USER_PROGRESS';
export const GET_LIST_KPI_SKILL_OF_USER_SUCCESS = 'GET_LIST_KPI_SKILL_OF_USER_SUCCESS';
export const GET_LIST_KPI_SKILL_OF_USER_FAILED = 'GET_LIST_KPI_SKILL_OF_USER_FAILED';

export const REMOVE_KPI_SKILL_PROGRESS = 'REMOVE_KPI_SKILL_PROGRESS';
export const REMOVE_KPI_SKILL_SUCCESS = 'REMOVE_KPI_SKILL_SUCCESS';
export const REMOVE_KPI_SKILL_FAILED = 'REMOVE_KPI_SKILL_FAILED';

/*
 *=================================================================================================
 * Log Skill Profile
 *=================================================================================================
 */
export const LIST_MY_USER_SKILLS_PROGRESS = 'LIST_MY_USER_SKILLS_PROGRESS';
export const LIST_MY_USER_SKILLS_SUCCESS = 'LIST_MY_USER_SKILLS_SUCCESS';
export const LIST_MY_USER_SKILLS_FAILED = 'LIST_MY_USER_SKILLS_FAILED';
