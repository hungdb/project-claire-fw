/*
 *=================================================================================================
 * Socket Constants
 *=================================================================================================
 */
export const EVENT_RELOAD_BOARD = 'reload_board';
export const EVENT_NOTIFICATION = 'notification';

export const EVENT_NEW_COMMENT = 'new_comment';
export const EVENT_UPDATE_COMMENT = 'update_comment';
export const EVENT_DELETE_COMMENT = 'delete_comment';

export const EVENT_NEW_COMMENT_ISSUE = 'new_comment_issue';
export const EVENT_UPDATE_COMMENT_ISSUE = 'update_comment_issue';
export const EVENT_DELETE_COMMENT_ISSUE = 'delete_comment_issue';
