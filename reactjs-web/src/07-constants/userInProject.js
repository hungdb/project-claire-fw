/*
 *=================================================================================================
 * User In Project Constants
 *=================================================================================================
 */

export const UIP_PROJECTS_GET = 'UIP_PROJECTS_GET';
export const UIP_PROJECTS_GET_SUCCESS = 'UIP_PROJECTS_GET_SUCCESS';
export const UIP_PROJECTS_GET_FAILED = 'UIP_PROJECTS_GET_FAILED';

export const UIP_USERS_GET = 'UIP_USERS_GET';
export const UIP_USERS_GET_SUCCESS = 'UIP_USERS_GET_SUCCESS';
export const UIP_USERS_GET_FAILED = 'UIP_USERS_GET_FAILED';

export const UIP_USER_CREATE = 'UIP_USER_CREATE';
export const UIP_USER_CREATE_SUCCESS = 'UIP_USER_CREATE_SUCCESS';
export const UIP_USER_CREATE_FAILED = 'UIP_USER_CREATE_FAILED';

export const UIP_USER_UPDATE = 'UIP_USER_UPDATE';
export const UIP_USER_UPDATE_SUCCESS = 'UIP_USER_UPDATE_SUCCESS';
export const UIP_USER_UPDATE_FAILED = 'UIP_USER_UPDATE_FAILED';

export const UIP_USER_DELETE = 'UIP_USER_DELETE';
export const UIP_USER_DELETE_SUCCESS = 'UIP_USER_DELETE_SUCCESS';
export const UIP_USER_DELETE_FAILED = 'UIP_USER_DELETE_FAILED';

export const UIP_LIST_USER = 'UIP_LIST_USER';
export const UIP_LIST_USER_SUCCESS = 'UIP_LIST_USER_SUCCESS';
export const UIP_LIST_USER_FAILED = 'UIP_LIST_USER_FAILED';

export const UIP_LIST_ROLE = 'UIP_LIST_ROLE';
export const UIP_LIST_ROLE_SUCCESS = 'UIP_LIST_ROLE_SUCCESS';
export const UIP_LIST_ROLE_FAILED = 'UIP_LIST_ROLE_FAILED';

export const CLEAR_USER_IN_PROJECT = 'CLEAR_USER_IN_PROJECT';

export const GET_USERS_OF_ALL_PROJECT = 'GET_USERS_OF_ALL_PROJECT';
export const GET_USERS_OF_ALL_PROJECT_SUCCESS = 'GET_USERS_OF_ALL_PROJECT_SUCCESS';
export const GET_USERS_OF_ALL_PROJECT_FAILED = 'GET_USERS_OF_ALL_PROJECT_FAILED';
