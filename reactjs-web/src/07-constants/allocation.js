/*
 *=================================================================================================
 * Allocation Constants
 *=================================================================================================
 */
export const LIST_ALLOCATIONS_OF_USER_PROGRESS = 'LIST_ALLOCATIONS_OF_USER_PROGRESS';
export const LIST_ALLOCATIONS_OF_USER_SUCCESS = 'LIST_ALLOCATIONS_OF_USER_SUCCESS';
export const LIST_ALLOCATIONS_OF_USER_FAILED = 'LIST_ALLOCATIONS_OF_USER_FAILED';
export const LIST_USERS_PROGRESS = 'LIST_USERS_PROGRESS';
export const LIST_USERS_SUCCESS = 'LIST_USERS_SUCCESS';
export const LIST_USERS_FAILED = 'LIST_USERS_FAILED';
export const LIST_SKILLS_BY_USER_PROGRESS = 'LIST_SKILLS_BY_USER_PROGRESS';
export const LIST_SKILLS_BY_USER_SUCCESS = 'LIST_SKILLS_BY_USER_SUCCESS';
export const LIST_SKILLS_BY_USER_FAILED = 'LIST_SKILLS_BY_USER_FAILED';
export const CLEAR_SKILLS = 'CLEAR_SKILLS';
export const FILTER_BY_START_DATE = 'FILTER_BY_START_DATE';
export const FILTER_BY_END_DATE = 'FILTER_BY_END_DATE';
export const FILTER_BY_SKILL = 'FILTER_BY_SKILL';
export const FILTER_BY_EFFORT_PERCENT = 'FILTER_BY_EFFORT_PERCENT';
export const FILTER_BY_SEARCH_NAME = 'FILTER_BY_SEARCH_NAME';
export const FILTER_BY_PROJECT = 'FILTER_BY_PROJECT';
export const FILTER_BY_PAGE = 'FILTER_BY_PAGE';
export const FILTER_ALLOCATION_PROGRESS = 'FILTER_ALLOCATION_PROGRESS';
export const FILTER_ALLOCATION_SUCCESS = 'FILTER_ALLOCATION_SUCCESS';
export const FILTER_ALLOCATION_FAILED = 'FILTER_ALLOCATION_FAILED';
export const SWITCH_USER_ALLOCATION = 'SWITCH_USER_ALLOCATION';
export const CLEAR_ALLOCATION_OF_USER = 'CLEAR_ALLOCATION_OF_USER';
export const CLEAR_SKILL_OF_USER = 'CLEAR_SKILL_OF_USER';
