/*
 *=================================================================================================
 * Auth Constants
 *=================================================================================================
 */
export const LOGIN_PROGRESS = 'LOGIN_PROGRESS';
export const REDIRECT_PROGRESS = 'REDIRECT_PROGRESS';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const PRE_LOGIN_SUCCESS = 'PRE_LOGIN_SUCCESS';
export const REDIRECT_SUCCESS = 'REDIRECT_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const PRE_LOGIN_FAILED = 'PRE_LOGIN_FAILED';
export const REDIRECT_FAILED = 'REDIRECT_FAILED';
export const REGISTER_PROGRESS = 'REGISTER_PROGRESS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';
export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';
export const GET_MY_ROLE_AND_PERMISSION_PROGRESS = 'GET_MY_ROLE_AND_PERMISSION_PROGRESS';
export const GET_MY_ROLE_AND_PERMISSION_SUCCESS = 'GET_MY_ROLE_AND_PERMISSION_SUCCESS';
export const GET_MY_ROLE_AND_PERMISSION_FAILED = 'GET_MY_ROLE_AND_PERMISSION_FAILED';
export const STORE_TOKEN_FIREBASE = 'STORE_TOKEN_FIREBASE';
export const LOGOUT_PROGRESS = 'LOGOUT_PROGRESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILED = 'LOGOUT_FAILED';
