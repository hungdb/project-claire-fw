/*
 *=================================================================================================
 * PermissionScheme Constants
 *=================================================================================================
 */
export const GET_PERMISSION_SCHEMES_PROGRESS = 'GET_PERMISSION_SCHEMES_PROGRESS';
export const GET_PERMISSION_SCHEMES_SUCCESS = 'GET_PERMISSION_SCHEMES_SUCCESS';
export const GET_PERMISSION_SCHEMES_FAILED = 'GET_PERMISSION_SCHEMES_FAILED';
export const GET_PROJECT_ROLES_PROGRESS = 'GET_PROJECT_ROLES_PROGRESS';
export const GET_PROJECT_ROLES_SUCCESS = 'GET_PROJECT_ROLES_SUCCESS';
export const GET_PROJECT_ROLES_FAILED = 'GET_PROJECT_ROLES_FAILED';
export const GET_PERMISSION_SCHEME_DETAIL_PROGRESS = 'GET_PERMISSION_SCHEME_DETAIL_PROGRESS';
export const GET_PERMISSION_SCHEME_DETAIL_SUCCESS = 'GET_PERMISSION_SCHEME_DETAIL_SUCCESS';
export const GET_PERMISSION_SCHEME_DETAIL_FAILED = 'GET_PERMISSION_SCHEME_DETAIL_FAILED';
export const GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS = 'GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS';
export const GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS = 'GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS';
export const GET_PERMISSION_OF_PROJECT_ROLE_FAILED = 'GET_PERMISSION_OF_PROJECT_ROLE_FAILED';
export const CREATE_PERMISSION_SCHEME_PROGRESS = 'CREATE_PERMISSION_SCHEME_PROGRESS';
export const CREATE_PERMISSION_SCHEME_SUCCESS = 'CREATE_PERMISSION_SCHEME_SUCCESS';
export const CREATE_PERMISSION_SCHEME_FAILED = 'CREATE_PERMISSION_SCHEME_FAILED';
export const UPDATE_PERMISSION_SCHEME_PROGRESS = 'UPDATE_PERMISSION_SCHEME_PROGRESS';
export const UPDATE_PERMISSION_SCHEME_SUCCESS = 'UPDATE_PERMISSION_SCHEME_SUCCESS';
export const UPDATE_PERMISSION_SCHEME_FAILED = 'UPDATE_PERMISSION_SCHEME_FAILED';
export const CREATE_PROJECT_ROLE_PROGRESS = 'CREATE_PROJECT_ROLE_PROGRESS';
export const CREATE_PROJECT_ROLE_SUCCESS = 'CREATE_PROJECT_ROLE_SUCCESS';
export const CREATE_PROJECT_ROLE_FAILED = 'CREATE_PROJECT_ROLE_FAILED';
export const UPDATE_PROJECT_ROLE_PROGRESS = 'UPDATE_PROJECT_ROLE_PROGRESS';
export const UPDATE_PROJECT_ROLE_SUCCESS = 'UPDATE_PROJECT_ROLE_SUCCESS';
export const UPDATE_PROJECT_ROLE_FAILED = 'UPDATE_PROJECT_ROLE_FAILED';
export const GET_ALL_PERMISSION_PROGRESS = 'GET_ALL_PERMISSION_PROGRESS';
export const GET_ALL_PERMISSION_SUCCESS = 'GET_ALL_PERMISSION_SUCCESS';
export const GET_ALL_PERMISSION_FAILED = 'GET_ALL_PERMISSION_FAILED';
export const CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS = 'CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS';
export const CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS = 'CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS';
export const CREATE_PERMISSION_SCHEME_DETAIL_FAILED = 'CREATE_PERMISSION_SCHEME_DETAIL_FAILED';
export const REMOVE_PERMISSION_SCHEME_PROGRESS = 'REMOVE_PERMISSION_SCHEME_PROGRESS';
export const REMOVE_PERMISSION_SCHEME_SUCCESS = 'REMOVE_PERMISSION_SCHEME_SUCCESS';
export const REMOVE_PERMISSION_SCHEME_FAILED = 'REMOVE_PERMISSION_SCHEME_FAILED';
export const REMOVE_PROJECT_ROLE_PROGRESS = 'REMOVE_PROJECT_ROLE_PROGRESS';
export const REMOVE_PROJECT_ROLE_SUCCESS = 'REMOVE_PROJECT_ROLE_SUCCESS';
export const REMOVE_PROJECT_ROLE_FAILED = 'REMOVE_PROJECT_ROLE_FAILED';
export const UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS = 'UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS';
export const UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS = 'UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS';
export const UPDATE_PERMISSION_SCHEME_DETAIL_FAILED = 'UPDATE_PERMISSION_SCHEME_DETAIL_FAILED';
export const REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS = 'REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS';
export const REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS = 'REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS';
export const REMOVE_PERMISSION_SCHEME_DETAIL_FAILED = 'REMOVE_PERMISSION_SCHEME_DETAIL_FAILED';
export const CLEAR_PERMISSION_SCHEME = 'CLEAR_PERMISSION_SCHEME';
