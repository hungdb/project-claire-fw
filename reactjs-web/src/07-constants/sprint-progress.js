/*
 *=================================================================================================
 * Sprint Progress Constants
 *=================================================================================================
 */
export const RESET_SPRINT_PROGRESS_MEMBERS = 'RESET_SPRINT_PROGRESS_MEMBERS';
export const GET_SPRINT_PROGRESS_MEMBERS = 'GET_SPRINT_PROGRESS_MEMBERS';
export const GET_SPRINT_PROGRESS_MEMBERS_SUCCESS = 'GET_SPRINT_PROGRESS_MEMBERS_SUCCESS';
export const GET_SPRINT_PROGRESS_MEMBERS_FAILED = 'GET_SPRINT_PROGRESS_MEMBERS_SUCCESS';
