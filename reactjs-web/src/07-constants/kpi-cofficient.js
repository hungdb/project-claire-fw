/*
 *=================================================================================================
 * KPI Coefficient Constants
 *=================================================================================================
 */

export const UPDATE_KPI_COEFFICIENT_PROGRESS = 'UPDATE_KPI_COEFFICIENT_PROGRESS';
export const UPDATE_KPI_COEFFICIENT_SUCCESS = 'UPDATE_KPI_COEFFICIENT_SUCCESS';
export const UPDATE_KPI_COEFFICIENT_FAILED = 'UPDATE_KPI_COEFFICIENT_FAILED';
export const GET_KPI_COEFFICIENT_PROGRESS = 'GET_KPI_COEFFICIENT_PROGRESS';
export const GET_KPI_COEFFICIENT_SUCCESS = 'GET_KPI_COEFFICIENT_SUCCESS';
export const GET_KPI_COEFFICIENT_FAILED = 'GET_KPI_COEFFICIENT_FAILED';
