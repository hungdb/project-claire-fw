/*
 *=================================================================================================
 * Task Constants
 *=================================================================================================
 */
export const TASKOWN_PROJECTS = 'TASKOWN_PROJECTS';
export const TASKOWN_PROJECTS_SUCCESS = 'TASKOWN_PROJECTS_SUCCESS';
export const TASKOWN_PROJECTS_FAILED = 'TASKOWN_PROJECTS_FAILED';
export const TASKOWN_TASKS = 'TASKOWN_TASKS';
export const TASKOWN_TASKS_SUCCESS = 'TASKOWN_TASKS_SUCCESS';
export const TASKOWN_TASKS_FAILED = 'TASKOWN_TASKS_FAILED';
export const GET_TASKS_OF_USERS_PROGRESS = 'GET_TASKS_OF_USERS_PROGRESS';
export const GET_TASKS_OF_USERS_SUCCESS = 'GET_TASKS_OF_USERS_SUCCESS';
export const GET_TASKS_OF_USERS_FAILED = 'GET_TASKS_OF_USERS_FAILED';
