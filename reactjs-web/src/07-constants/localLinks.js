/*
 *=================================================================================================
 * Local link
 *=================================================================================================
 */
import { getEnv } from '../env';
import { PERM_REDIRECT_CHECK_IN, PERM_REDIRECT_IDENTITY, PERM_REDIRECT_ASSET } from './app-permission';

export const CHECKIN = {
  title: 'checkin',
  link: getEnv('LINK_CHECKIN'),
  color: '#016FB9',
  permission: PERM_REDIRECT_CHECK_IN
};
export const IDENTITY = {
  title: 'identity',
  link: getEnv('LINK_IDENTITY'),
  color: '#0083EF',
  permission: PERM_REDIRECT_IDENTITY
};
export const ASSET = {
  title: 'asset',
  link: getEnv('LINK_ASSET'),
  color: '#1DA2E0',
  permission: PERM_REDIRECT_ASSET
};
