/*
 *=================================================================================================
 * Setting constants Constants
 *=================================================================================================
 */
export const ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS = 'ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS';
export const ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS = 'ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS';
export const ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED = 'ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED';

export const GET_STATUS_OF_ALERT_SYSTEM_PROGRESS = 'GET_STATUS_OF_ALERT_SYSTEM_PROGRESS';
export const GET_STATUS_OF_ALERT_SYSTEM_SUCCESS = 'GET_STATUS_OF_ALERT_SYSTEM_SUCCESS';
export const GET_STATUS_OF_ALERT_SYSTEM_FAILED = 'GET_STATUS_OF_ALERT_SYSTEM_FAILED';
