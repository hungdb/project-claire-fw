/*
 *=================================================================================================
 * Admin Allocation Constants
 *=================================================================================================
 */
export const GET_ALLOCATIONS_USERS = 'GET_ALLOCATIONS_USERS';
export const GET_ALLOCATIONS_USERS_SUCCESS = 'GET_ALLOCATIONS_USERS_SUCCESS';
export const GET_ALLOCATIONS_USERS_FAILED = 'GET_ALLOCATIONS_USERS_FAILED';
