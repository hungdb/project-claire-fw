/*
 *==============================================================================
 * Alert subscription constants
 *==============================================================================
 */
export const LIST_ALERT_SUBSCRIPTION_PROGRESS = 'LIST_ALERT_SUBSCRIPTION_PROGRESS';
export const LIST_ALERT_SUBSCRIPTION_SUCCESS = 'LIST_ALERT_SUBSCRIPTION_SUCCESS';
export const LIST_ALERT_SUBSCRIPTION_FAILED = 'LIST_ALERT_SUBSCRIPTION_FAILED';
export const LIST_USERS_PROGRESS = 'LIST_USERS_PROGRESS';
export const LIST_USERS_SUCCESS = 'LIST_USERS_SUCCESS';
export const LIST_USERS_FAILED = 'LIST_USERS_FAILED';
export const LIST_PROJECT_PROGRESS = 'LIST_PROJECT_PROGRESS';
export const LIST_PROJECT_SUCCESS = 'LIST_PROJECT_SUCCESS';
export const LIST_PROJECT_FAILED = 'LIST_PROJECT_FAILED';
