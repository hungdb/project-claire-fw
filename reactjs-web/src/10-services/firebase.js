/*
 *=================================================================================================
 * Firebase service
 *   - Cloud Messaging
 *=================================================================================================
 */

import firebase from 'firebase/app';
import 'firebase/messaging';
import { getEnv } from '../env';

export const initializeFirebase = () => {
  firebase.initializeApp({
    messagingSenderId: getEnv('MESSAGING_SENDER_ID')
  });

  let messaging = null;
  if (firebase.messaging.isSupported()) {
    messaging = firebase.messaging();
    messaging.onMessage(function(payload) {
      // ...
      const src = `${getEnv('LINK_PROJECT')}/seasons.mp3`;
      const c = document.createElement('audio');
      c.src = src;
      c.play();
      //
    });
  }

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register(`${getEnv('LINK_PROJECT')}/firebase-messaging-sw.js`).then(registration => {
      firebase.messaging().useServiceWorker(registration);
    });
  }
};

export const askForPermissionToReceiveNotifications = () => {
  try {
    return new Promise(function(resolve, reject) {
      const permissionResult = Notification.requestPermission(result => {
        // Handling deprecated version with callback.
        return resolve(result);
      });
      if (permissionResult) {
        permissionResult.then(resolve, reject);
      }
    }).then(async permissionResult => {
      if (permissionResult !== 'granted') {
        console.error('Permission not granted.');
      } else {
        if (firebase.messaging.isSupported()) {
          const messaging = firebase.messaging();
          await messaging.requestPermission();
          return await messaging.getToken();
        }
      }
    });
  } catch (error) {
    console.error(error);
  }
};
