const ENV = {
  LCL: {
    REACT_APP_API_SERVER: 'http://localhost:1992',
    REACT_APP_API_IDENTITY: 'https://api.dev-identity.bap.jp',
    IDENTITY_OAUTH: 'https://dev-identity.bap.jp/login?redirectService=oauth',
    OAUTH_APP_ID: 6,
    OAUTH_APP_CALLBACK_URL: 'http://localhost:3000/login?provider=identity',
    S3_BUCKET: 'https://s3-ap-southeast-1.amazonaws.com',
    START_TIME_MORNING: '8:15',
    END_TIME_MORNING: '12:00',
    START_TIME_AFTERNOON: '13:15',
    END_TIME_AFTERNOON: '17:30',
    MESSAGING_SENDER_ID: '728657623035',
    LINK_CHECKIN: 'https://checkin.bap.jp',
    LINK_IDENTITY: 'https://identity.bap.jp',
    LINK_ASSET: 'https://asset.bap.jp',
    LINK_PROJECT: 'http://localhost:3000',
    KPI_REVIEW_POINT_PERCENT: 0.7,
    KPI_TEST_POINT_PERCENT: 0.3,
    DEFAULT_BEHAVIOR_POINT: 50,
    KPI_MAXIMUM_POINT: 100,
    KPI_PROJECT_MAXIMUM_COEFFICIENT: 1.4,
    LIMIT_ROW_ISSUE_IMPORT_FROM_FILE: 500,
    LINK_FORMAT_FILE_ISSUE:
      'https://docs.google.com/spreadsheets/d/1WVva9Lq-KFHNHyyPkkkVRkkcVDJztz4fHzeR6ncxz48/edit?usp=sharing',
    LINK_FORMAT_FILE_KPI_BEHAVIOR:
      'https://docs.google.com/spreadsheets/d/1knvmED11HKokCskJ9uUDuEc9qvsjDhqLrGt2KpOtaYw/edit?usp=sharing',
    LINK_DOMAIN_PROJECT: 'http://localhost:3000',
    LINK_FORMAT_FILE_PERMISSION:
      'https://docs.google.com/spreadsheets/d/1wEoFSCqwO5CkeDeyEcjMCkAGx81VesyxdbyVS7L9axM/edit?usp=sharing'
  },

  DEV: {
    REACT_APP_API_SERVER: 'https://api.dev-project.bap.jp',
    REACT_APP_API_IDENTITY: 'https://api.dev-identity.bap.jp',
    IDENTITY_OAUTH: 'https://dev-identity.bap.jp/login?redirectService=oauth',
    OAUTH_APP_ID: 6,
    OAUTH_APP_CALLBACK_URL: 'https://dev-project.bap.jp/login?provider=identity',
    S3_BUCKET: 'https://s3-ap-southeast-1.amazonaws.com',
    START_TIME_MORNING: '8:00',
    END_TIME_MORNING: '12:00',
    START_TIME_AFTERNOON: '13:15',
    END_TIME_AFTERNOON: '17:15',
    MESSAGING_SENDER_ID: '402136633344',
    LINK_CHECKIN: 'https://checkin.bap.jp',
    LINK_IDENTITY: 'https://identity.bap.jp',
    LINK_ASSET: 'https://asset.bap.jp',
    LINK_PROJECT: 'https://dev-project.bap.jp',
    KPI_REVIEW_POINT_PERCENT: 0.7,
    KPI_TEST_POINT_PERCENT: 0.3,
    DEFAULT_BEHAVIOR_POINT: 50,
    KPI_MAXIMUM_POINT: 100,
    KPI_PROJECT_MAXIMUM_COEFFICIENT: 1.4,
    LIMIT_ROW_ISSUE_IMPORT_FROM_FILE: 500,
    LINK_FORMAT_FILE_ISSUE:
      'https://docs.google.com/spreadsheets/d/1WVva9Lq-KFHNHyyPkkkVRkkcVDJztz4fHzeR6ncxz48/edit?usp=sharing',
    LINK_FORMAT_FILE_KPI_BEHAVIOR:
      'https://docs.google.com/spreadsheets/d/1knvmED11HKokCskJ9uUDuEc9qvsjDhqLrGt2KpOtaYw/edit?usp=sharing',
    LINK_DOMAIN_PROJECT: 'https://dev-project.bap.jp',
    LINK_FORMAT_FILE_PERMISSION:
      'https://docs.google.com/spreadsheets/d/1wEoFSCqwO5CkeDeyEcjMCkAGx81VesyxdbyVS7L9axM/edit?usp=sharing'
  },

  STG: {
    REACT_APP_API_SERVER: ''
  },

  PRO: {
    REACT_APP_API_SERVER: 'https://api.project.bap.jp',
    REACT_APP_API_IDENTITY: 'https://api.identity.bap.jp',
    IDENTITY_OAUTH: 'https://identity.bap.jp/login?redirectService=oauth',
    OAUTH_APP_ID: 15,
    OAUTH_APP_CALLBACK_URL: 'https://project.bap.jp/login?provider=identity',
    S3_BUCKET: 'https://s3-ap-southeast-1.amazonaws.com',
    START_TIME_MORNING: '8:00',
    END_TIME_MORNING: '12:00',
    START_TIME_AFTERNOON: '13:15',
    END_TIME_AFTERNOON: '17:15',
    MESSAGING_SENDER_ID: '934126702978',
    LINK_CHECKIN: 'https://checkin.bap.jp',
    LINK_IDENTITY: 'https://identity.bap.jp',
    LINK_ASSET: 'https://asset.bap.jp',
    LINK_PROJECT: 'https://project.bap.jp',
    KPI_REVIEW_POINT_PERCENT: 0.7,
    KPI_TEST_POINT_PERCENT: 0.3,
    DEFAULT_BEHAVIOR_POINT: 50,
    KPI_MAXIMUM_POINT: 100,
    KPI_PROJECT_MAXIMUM_COEFFICIENT: 1.4,
    LIMIT_ROW_ISSUE_IMPORT_FROM_FILE: 500,
    LINK_FORMAT_FILE_ISSUE:
      'https://docs.google.com/spreadsheets/d/1WVva9Lq-KFHNHyyPkkkVRkkcVDJztz4fHzeR6ncxz48/edit?usp=sharing',
    LINK_FORMAT_FILE_KPI_BEHAVIOR:
      'https://docs.google.com/spreadsheets/d/1knvmED11HKokCskJ9uUDuEc9qvsjDhqLrGt2KpOtaYw/edit?usp=sharing',
    LINK_DOMAIN_PROJECT: 'https://project.bap.jp',
    LINK_FORMAT_FILE_PERMISSION:
      'https://docs.google.com/spreadsheets/d/1wEoFSCqwO5CkeDeyEcjMCkAGx81VesyxdbyVS7L9axM/edit?usp=sharing'
  }
};

const config = ENV[process.env.REACT_APP_STAGE || 'LCL'];

const getEnv = (name, defaultValue) => {
  return process.env[name] || config[name] || defaultValue;
};

export { getEnv };
