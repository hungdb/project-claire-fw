/*
 *=================================================================================================
 * Group Reducer
 *=================================================================================================
 */
import {
  GET_GROUP_PROGRESS,
  GET_GROUP_SUCCESS,
  GET_GROUP_FAILED,
  CREATE_GROUP_PROGRESS,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_FAILED,
  UPDATE_GROUP_PROGRESS,
  UPDATE_GROUP_SUCCESS,
  UPDATE_GROUP_FAILED,
  REMOVE_GROUP_PROGRESS,
  REMOVE_GROUP_SUCCESS,
  REMOVE_GROUP_FAILED,
  TOGGLE_NOTIFICATION_GROUP_PROGRESS,
  TOGGLE_NOTIFICATION_GROUP_SUCCESS,
  TOGGLE_NOTIFICATION_GROUP_FAILED,
  SWITCH_PROJECT_GROUP
} from '../07-constants/group';

const initialState = {
  status: null,
  projectGroups: [],
  selectedProjectGroup: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_GROUP_PROGRESS:
    case GET_GROUP_PROGRESS:
    case UPDATE_GROUP_PROGRESS:
    case REMOVE_GROUP_PROGRESS:
    case TOGGLE_NOTIFICATION_GROUP_PROGRESS:
      return state;
    case CREATE_GROUP_SUCCESS:
      return {
        ...state,
        status: action.type,
        projectGroups: state.projectGroups.concat([action.data.group])
      };
    case UPDATE_GROUP_SUCCESS: {
      const index = state.projectGroups.findIndex(g => g.id === action.data.group.id);
      const newGroup = {
        ...state.projectGroups[index],
        name: action.data.group.name,
        isActive: action.data.group.isActive
      };
      const newValue = [...state.projectGroups.slice(0, index), newGroup, ...state.projectGroups.slice(index + 1)];
      return {
        ...state,
        status: action.type,
        selectedProjectGroup:
          state.selectedProjectGroup && state.selectedProjectGroup.id === newGroup.id
            ? newGroup
            : state.selectedProjectGroup,
        projectGroups: newValue
      };
    }
    case TOGGLE_NOTIFICATION_GROUP_SUCCESS: {
      const groupIndex = state.projectGroups.findIndex(g => g.id === action.data.groupId);
      const newGroupValue = {
        ...state.projectGroups[groupIndex],
        isNotificationEnable: action.data.isEnabled
      };
      const newValue = [
        ...state.projectGroups.slice(0, groupIndex),
        newGroupValue,
        ...state.projectGroups.slice(groupIndex + 1)
      ];
      return {
        ...state,
        status: action.type,
        projectGroups: newValue,
        selectedProjectGroup: newGroupValue
      };
    }
    case REMOVE_GROUP_SUCCESS: {
      const removedGroup = action.data && action.data.removedGroup;
      if (removedGroup) {
        const newArray = state.projectGroups.filter(g => g.id !== removedGroup.id);
        return {
          ...state,
          status: action.type,
          projectGroups: [...newArray]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case GET_GROUP_FAILED:
    case CREATE_GROUP_FAILED:
    case UPDATE_GROUP_FAILED:
    case REMOVE_GROUP_FAILED:
    case TOGGLE_NOTIFICATION_GROUP_FAILED:
      return {
        ...state,
        status: action.type,
        groupError: (action.data && action.data.error) || initialState.groupError
      };
    case GET_GROUP_SUCCESS:
      return {
        ...state,
        status: action.type,
        projectGroups: action.data.groups,
        selectedProjectGroup: state.selectedProjectGroup
          ? action.data.groups.find(g => g.id === state.selectedProjectGroup.id)
          : null
      };

    case SWITCH_PROJECT_GROUP:
      return {
        ...state,
        selectedProjectGroup: action.data.selectedProjectGroup
      };
    default:
      return state;
  }
};
