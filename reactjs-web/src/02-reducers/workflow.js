/*
 *=================================================================================================
 * Workflow Reducer
 *=================================================================================================
 */
import {
  GET_WORKFLOW_PROGRESS,
  GET_WORKFLOW_SUCCESS,
  GET_WORKFLOW_FAILED,
  CREATE_WORKFLOW_PROGRESS,
  CREATE_WORKFLOW_SUCCESS,
  CREATE_WORKFLOW_FAILED,
  UPDATE_WORKFLOW_PROGRESS,
  UPDATE_WORKFLOW_SUCCESS,
  UPDATE_WORKFLOW_FAILED,
  REMOVE_WORKFLOW_PROGRESS,
  REMOVE_WORKFLOW_SUCCESS,
  REMOVE_WORKFLOW_FAILED,
  GET_WORKFLOW_DETAIL_SUCCESS,
  GET_WORKFLOW_DETAIL_FAILED,
  GET_WORKFLOW_DETAIL_PROGRESS,
  GET_WORKFLOW_STATE_PROGRESS,
  GET_WORKFLOW_STATE_SUCCESS,
  GET_WORKFLOW_STATE_FAILED,
  REMOVE_WORKFLOW_DETAIL_PROGRESS,
  REMOVE_WORKFLOW_DETAIL_SUCCESS,
  REMOVE_WORKFLOW_DETAIL_FAILED,
  ADD_WORKFLOW_DETAIL_PROGRESS,
  ADD_WORKFLOW_DETAIL_SUCCESS,
  ADD_WORKFLOW_DETAIL_FAILED,
  ADD_WORKFLOW_STATE_PROGRESS,
  ADD_WORKFLOW_STATE_SUCCESS,
  ADD_WORKFLOW_STATE_FAILED,
  WORKFLOW_STATE_UPDATE_PROGRESS,
  WORKFLOW_STATE_UPDATE_SUCCESS,
  WORKFLOW_STATE_UPDATE_FAILED,
  WORKFLOW_STATE_DELETE_PROGRESS,
  WORKFLOW_STATE_DELETE_SUCCESS,
  WORKFLOW_STATE_DELETE_FAILED,
  WORKFLOW_DETAIL_UPDATE,
  WORKFLOW_DETAIL_UPDATE_SUCCESS,
  WORKFLOW_DETAIL_UPDATE_FAILED,
  GET_WORKFLOW_FOR_MIGRATE_PROGRESS,
  GET_WORKFLOW_FOR_MIGRATE_SUCCESS,
  GET_WORKFLOW_FOR_MIGRATE_FAILED,
  GET_WORKFLOW_STATE_FOR_MIGRATE_PROGRESS,
  GET_WORKFLOW_STATE_FOR_MIGRATE_SUCCESS,
  GET_WORKFLOW_STATE_FOR_MIGRATE_FAILED,
  WORKFLOW_MIGRATE_PROGRESS,
  WORKFLOW_MIGRATE_SUCCESS,
  WORKFLOW_MIGRATE_FAILED,
  CLEAR_WORKFLOW_STATES_FOR_MIGRATE
} from '../07-constants/workflow';

const initialState = {
  status: null,
  workflowError: null,
  workflows: [],
  listState: [],
  workflowDetail: [],
  workflowsForMigrate: [],
  listFromStateForMigrate: [],
  listToStateForMigrate: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_WORKFLOW_PROGRESS:
    case GET_WORKFLOW_PROGRESS:
    case UPDATE_WORKFLOW_PROGRESS:
    case REMOVE_WORKFLOW_PROGRESS:
    case GET_WORKFLOW_DETAIL_PROGRESS:
    case GET_WORKFLOW_STATE_PROGRESS:
    case REMOVE_WORKFLOW_DETAIL_PROGRESS:
    case ADD_WORKFLOW_DETAIL_PROGRESS:
    case ADD_WORKFLOW_STATE_PROGRESS:
    case WORKFLOW_STATE_UPDATE_PROGRESS:
    case WORKFLOW_STATE_DELETE_PROGRESS:
    case WORKFLOW_DETAIL_UPDATE:
    case GET_WORKFLOW_FOR_MIGRATE_PROGRESS:
    case GET_WORKFLOW_STATE_FOR_MIGRATE_PROGRESS:
    case WORKFLOW_MIGRATE_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_WORKFLOW_FAILED:
    case CREATE_WORKFLOW_FAILED:
    case UPDATE_WORKFLOW_FAILED:
    case REMOVE_WORKFLOW_FAILED:
    case GET_WORKFLOW_DETAIL_FAILED:
    case GET_WORKFLOW_STATE_FAILED:
    case REMOVE_WORKFLOW_DETAIL_FAILED:
    case ADD_WORKFLOW_DETAIL_FAILED:
    case ADD_WORKFLOW_STATE_FAILED:
    case WORKFLOW_STATE_UPDATE_FAILED:
    case WORKFLOW_STATE_DELETE_FAILED:
    case WORKFLOW_DETAIL_UPDATE_FAILED:
    case GET_WORKFLOW_FOR_MIGRATE_FAILED:
    case GET_WORKFLOW_STATE_FOR_MIGRATE_FAILED:
    case WORKFLOW_MIGRATE_FAILED:
      return {
        ...state,
        status: action.type,
        workflowError: action.data && action.data.error
      };

    case GET_WORKFLOW_SUCCESS:
      return {
        ...state,
        status: action.type,
        workflows: action.data.workflows
      };

    case CREATE_WORKFLOW_SUCCESS:
      return {
        ...state,
        status: action.type,
        workflows: [...state.workflows, action.data.workflow]
      };

    case UPDATE_WORKFLOW_SUCCESS: {
      const updateIndex = state.workflows.findIndex(w => w.id === action.data.updateWorkflow.id);
      return {
        ...state,
        status: action.type,
        workflows: [
          ...state.workflows.slice(0, updateIndex),
          { ...state.workflows[updateIndex], ...action.data.updateWorkflow },
          ...state.workflows.slice(updateIndex + 1)
        ]
      };
    }

    case REMOVE_WORKFLOW_SUCCESS:
      var pushArray = [];
      pushArray = state.workflows.filter(workflow => workflow.id !== action.data.removeWorkflow.id);
      return {
        ...state,
        status: action.type,
        workflows: pushArray
      };

    case GET_WORKFLOW_DETAIL_SUCCESS:
      return {
        ...state,
        status: action.type,
        workflowDetail:
          (action.data && action.data.workflowDetail && action.data.workflowDetail.detail) ||
          initialState.workflowDetail
      };

    case GET_WORKFLOW_STATE_SUCCESS:
      return {
        ...state,
        status: action.type,
        listState: (action.data && action.data.listState) || initialState.listState
      };

    case REMOVE_WORKFLOW_DETAIL_SUCCESS: {
      let dataArray = [];
      dataArray = state.workflowDetail.filter(
        workflowDetail => workflowDetail.id !== action.data.removeWorkflowDetailData.id
      );
      return {
        ...state,
        status: action.type,
        workflowDetail: dataArray
      };
    }

    case ADD_WORKFLOW_DETAIL_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          workflowDetail: [...state.workflowDetail, action.data]
        };
      }
      return { ...state, status: action.type };
    }

    case ADD_WORKFLOW_STATE_SUCCESS:
      return {
        ...state,
        status: action.type,
        listState: [...state.listState, ...[action.data.createWorkflowStateData]]
      };
    case WORKFLOW_STATE_UPDATE_SUCCESS: {
      const updateIndex = state.listState.findIndex(ws => ws.id === action.data.updateWorkflowStateData.id);
      const data = {
        id: action.data.updateWorkflowStateData.id,
        name: action.data.updateWorkflowStateData.name,
        effortType: action.data.updateWorkflowStateData.type,
        ordinalNumber: action.data.updateWorkflowStateData.ordinal
      };
      return {
        ...state,
        status: action.type,
        listState: [
          ...state.listState.slice(0, updateIndex),
          { ...state.listState[updateIndex], ...data },
          ...state.listState.slice(updateIndex + 1)
        ]
      };
    }

    // var workflowsStateAfterUpdate = state.listState.map((item) => {
    //   if (item.id === action.data.updateWorkflowStateData.id) {
    //     return ({
    //       id: action.data.updateWorkflowStateData.id,
    //       name: action.data.updateWorkflowStateData.name,
    //       effortType: action.data.updateWorkflowStateData.type,
    //       ordinalNumber: action.data.updateWorkflowStateData.ordinal,
    //     });
    //   }
    //   return item;
    // });
    // return {
    //   ...state,
    //   status: action.type,
    //   listState: workflowsStateAfterUpdate,
    // };
    case WORKFLOW_STATE_DELETE_SUCCESS: {
      const workflowsStateAfterDelete = state.listState.filter(
        item => item.id !== action.data.deleteWorkflowStateData.id
      );
      return {
        ...state,
        status: action.type,
        listState: workflowsStateAfterDelete
      };
    }
    case WORKFLOW_DETAIL_UPDATE_SUCCESS: {
      if (action.data) {
        const workflowDetail = [...state.workflowDetail];
        const updateIndex = workflowDetail.findIndex(item => item.id === action.data.id);
        if (updateIndex > -1) {
          workflowDetail[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            workflowDetail
          };
        }
      }
      return { ...state, status: action.type };
    }
    case GET_WORKFLOW_FOR_MIGRATE_SUCCESS: {
      return {
        ...state,
        status: action.type,
        workflowsForMigrate: (action.data && action.data.workflowsForMigrate) || initialState.workflowsForMigrate
      };
    }

    case GET_WORKFLOW_STATE_FOR_MIGRATE_SUCCESS: {
      const listStateForMigrate = {
        listFromStateForMigrate: state.listFromStateForMigrate || initialState.listFromStateForMigrate,
        listToStateForMigrate: state.listToStateForMigrate || initialState.listToStateForMigrate
      };
      if (action.data && action.data.type === 'from')
        listStateForMigrate.listFromStateForMigrate =
          (action.data && action.data.listStateForMigrate) || initialState.listFromStateForMigrate;
      if (action.data && action.data.type === 'to')
        listStateForMigrate.listToStateForMigrate =
          (action.data && action.data.listStateForMigrate) || initialState.listToStateForMigrate;
      return {
        ...state,
        status: action.type,
        ...listStateForMigrate
      };
    }

    case WORKFLOW_MIGRATE_SUCCESS: {
      return {
        ...state,
        status: action.type,
        listFromStateForMigrate: initialState.listFromStateForMigrate
      };
    }

    case CLEAR_WORKFLOW_STATES_FOR_MIGRATE: {
      return {
        ...state,
        status: action.type,
        listFromStateForMigrate: initialState.listFromStateForMigrate,
        listToStateForMigrate: initialState.listToStateForMigrate
      };
    }

    default:
      return state;
  }
};
