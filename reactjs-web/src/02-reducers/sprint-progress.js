/*
 *=================================================================================================
 * Sprint Progress Reducer
 *=================================================================================================
 */
import {
  RESET_SPRINT_PROGRESS_MEMBERS,
  GET_SPRINT_PROGRESS_MEMBERS,
  GET_SPRINT_PROGRESS_MEMBERS_SUCCESS,
  GET_SPRINT_PROGRESS_MEMBERS_FAILED
} from '../07-constants/sprint-progress';

const initialState = {
  type: null,
  error: null,
  sprintProgressMembers: []
};

const sprintProgress = (state = initialState, action) => {
  switch (action.type) {
    // PLAIN
    case RESET_SPRINT_PROGRESS_MEMBERS:
      return {
        ...state,
        type: action.type,
        sprintProgressMembers: []
      };

    // REQUEST
    case GET_SPRINT_PROGRESS_MEMBERS:
      return {
        ...state,
        type: action.type
      };

    case GET_SPRINT_PROGRESS_MEMBERS_SUCCESS:
      return {
        ...state,
        type: action.type,
        sprintProgressMembers: action.sprintProgressMembers
      };
    case GET_SPRINT_PROGRESS_MEMBERS_FAILED:
      return {
        ...state,
        type: action.type,
        error: action.error,
        sprintProgressMembers: []
      };

    default:
      return state;
  }
};

export default sprintProgress;
