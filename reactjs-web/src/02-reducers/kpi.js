/*
 *=================================================================================================
 * KPI Reducer
 *=================================================================================================
 */
import {
  GET_LIST_KPI_TO_CHART_PROGRESS,
  GET_LIST_KPI_TO_CHART_SUCCESS,
  GET_LIST_KPI_TO_CHART_FAILED,
  GET_MY_LIST_KPI_TO_CHART_PROGRESS,
  GET_MY_LIST_KPI_TO_CHART_SUCCESS,
  GET_MY_LIST_KPI_TO_CHART_FAILED,
  CLEAR_KPI_CHART,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  CLEAR_LIST_USERS,
  FILTER_BY_START_DATE,
  FILTER_BY_END_DATE,
  FILTER_BY_PROJECT,
  FILTER_BY_USER,
  FILTER_BY_TYPE,
  FILTER_BY_TYPE_TIME,
  FILTER_BY_YEAR
} from '../07-constants/kpi';
import { getYear, sortObjectInArray } from '../08-helpers/common';
import { KPI_TYPE } from '../05-utils/commonData';

const initialState = {
  status: null,
  kpiChartError: null,
  filter: {
    startDate: null,
    endDate: null,
    type: null,
    typeTime: null,
    userId: null,
    projectId: null,
    year: null
  },
  listUsers: {
    users: []
  },
  kpiDataChart: {
    data: [],
    total: 0
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_KPI_TO_CHART_PROGRESS:
    case GET_MY_LIST_KPI_TO_CHART_PROGRESS:
      return {
        ...state,
        status: action.type,
        kpiDataChart: initialState.kpiDataChart
      };
    case LIST_USERS_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_LIST_KPI_TO_CHART_FAILED:
    case GET_MY_LIST_KPI_TO_CHART_FAILED:
    case LIST_USERS_FAILED:
      return {
        ...state,
        status: action.type,
        kpiChartError: (action.data && action.data.error) || initialState.kpiChartError
      };
    case LIST_USERS_SUCCESS: {
      let userId = state.filter.userId;
      const listUsers = action.data && action.data.listUsers;
      const isProjectTypeInit =
        (state.filter.type === KPI_TYPE.projectMember.value || state.filter.type === KPI_TYPE.projectLeader.value) &&
        !userId;
      const isExistUsers = listUsers && Array.isArray(listUsers.users) && listUsers.users.length > 0;

      if (isProjectTypeInit && isExistUsers) {
        userId = listUsers.users[0].id;
      }
      return {
        ...state,
        status: action.type,
        listUsers: listUsers || initialState.listUsers,
        filter: {
          ...state.filter,
          userId
        }
      };
    }
    case GET_LIST_KPI_TO_CHART_SUCCESS:
    case GET_MY_LIST_KPI_TO_CHART_SUCCESS: {
      const yearOptions = [];
      let yearFilter = state.filter.year;
      const kpiDataChart = action.data && action.data.kpiDataChart;
      if (kpiDataChart && kpiDataChart.data) {
        const kpiData = action.data.kpiDataChart.data;
        const yearOption = {};
        kpiData.some(element => {
          const year = getYear(element.evaluatedAt);
          yearOption[year] = year;
          return false;
        });
        for (const year in yearOption) {
          if (yearOption.hasOwnProperty(year)) yearOptions.push({ label: year, value: year });
        }
        if (yearOptions.length > 0) {
          sortObjectInArray(yearOptions, ['value'], false);
          if (!state.filter.year) {
            yearFilter = yearOptions[0].value;
          }
        }
      }
      return {
        ...state,
        status: action.type,
        kpiDataChart: kpiDataChart || initialState.kpiDataChart,
        yearOptions,
        filter: {
          ...state.filter,
          year: yearFilter
        }
      };
    }
    case FILTER_BY_START_DATE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          startDate: action.data.startDate
        }
      };
    case FILTER_BY_END_DATE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          endDate: action.data.endDate
        }
      };
    case FILTER_BY_PROJECT:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          projectId: action.data.projectId
        }
      };
    case FILTER_BY_USER:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          userId: action.data.userId
        }
      };
    case FILTER_BY_TYPE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          type: action.data.type
        }
      };
    case FILTER_BY_TYPE_TIME:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          typeTime: action.data.typeTime
        }
      };
    case FILTER_BY_YEAR:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          year: action.data.year
        }
      };
    case CLEAR_KPI_CHART:
      return {
        ...state,
        kpiChart: initialState.kpiChart,
        filter: initialState.filter
      };
    case CLEAR_LIST_USERS:
      return {
        ...state,
        listUsers: initialState.listUsers
      };
    default:
      return state;
  }
};
