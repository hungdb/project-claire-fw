/*
 *=================================================================================================
 * Notification Reducer
 *=================================================================================================
 */
import {
  GET_NOTIFICATIONS_PROGRESS,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAIL,
  LOAD_MORE_NOTIFICATIONS_PROGRESS,
  LOAD_MORE_NOTIFICATIONS_SUCCESS,
  LOAD_MORE_NOTIFICATIONS_FAILED,
  ADD_NOTIFICATION,
  UPDATE_NOTIFICATION,
  UPDATE_NOTIFICATION_SUCCESS,
  UPDATE_NOTIFICATION_FAILED,
  READ_ALL_NOTIFICATIONS
} from '../07-constants/notification';

const initialState = {
  notifications: [],
  total: 0,
  status: null,
  notifyError: null,
  totalNewNotification: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATIONS_PROGRESS:
    case LOAD_MORE_NOTIFICATIONS_PROGRESS:
    case UPDATE_NOTIFICATION:
      return {
        ...state,
        status: action.type
      };
    case GET_NOTIFICATIONS_FAIL:
    case LOAD_MORE_NOTIFICATIONS_FAILED:
    case UPDATE_NOTIFICATION_FAILED:
      return {
        ...state,
        status: action.type,
        notifyError: (action.data && action.data.error) || initialState.notifyError
      };
    case GET_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        status: action.type,
        notifications: (action.data && action.data.notifications) || initialState.notifications,
        total: (action.data && action.data.total) || initialState.total,
        totalNewNotification: (action.data && action.data.totalNotRead) || initialState.totalNewNotification
      };
    case LOAD_MORE_NOTIFICATIONS_SUCCESS: {
      const notifications = (action.data && action.data.notifications) || initialState.notifications;
      return {
        ...state,
        status: action.type,
        notifications: [...state.notifications, ...notifications],
        total: (action.data && action.data.total) || initialState.total
      };
    }
    case ADD_NOTIFICATION: {
      return {
        ...state,
        totalNewNotification: state.totalNewNotification + 1
      };
    }
    case UPDATE_NOTIFICATION_SUCCESS: {
      return {
        ...state,
        status: action.type,
        totalNewNotification: state.totalNewNotification - 1,
        notifications: state.notifications.map(n => (n.id === action.notifyId ? { ...n, id: action.notifyId } : n))
      };
    }
    case READ_ALL_NOTIFICATIONS: {
      const notifications = state.notifications.map(notification => {
        return {
          ...notification,
          isRead: true
        };
      });
      return {
        ...state,
        totalNewNotification: initialState.totalNewNotification,
        notifications
      };
    }
    default:
      return state;
  }
};
