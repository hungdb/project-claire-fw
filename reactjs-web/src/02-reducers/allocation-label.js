/*
 *=================================================================================================
 * Allocation Label Reducer
 *=================================================================================================
 */
import {
  GET_ALL_ALLOCATION_LABELS_PROGRESS,
  GET_ALL_ALLOCATION_LABELS_FAILED,
  GET_ALL_ALLOCATION_LABELS_SUCCESS,
  ADD_ALLOCATION_LABEL_PROGRESS,
  ADD_ALLOCATION_LABEL_SUCCESS,
  ADD_ALLOCATION_LABEL_FAILED,
  UPDATE_ALLOCATION_LABEL_PROGRESS,
  UPDATE_ALLOCATION_LABEL_SUCCESS,
  UPDATE_ALLOCATION_LABEL_FAILED,
  REMOVE_ALLOCATION_LABEL_PROGRESS,
  REMOVE_ALLOCATION_LABEL_SUCCESS,
  REMOVE_ALLOCATION_LABEL_FAILED,
  CLEAR_ALLOCATION_LABELS
} from '../07-constants/allocation-label';

const initialState = {
  status: null,
  checkListError: null,
  allocationLabels: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_ALLOCATION_LABELS_PROGRESS:
    case ADD_ALLOCATION_LABEL_PROGRESS:
    case UPDATE_ALLOCATION_LABEL_PROGRESS:
    case REMOVE_ALLOCATION_LABEL_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_ALL_ALLOCATION_LABELS_FAILED:
    case ADD_ALLOCATION_LABEL_FAILED:
    case UPDATE_ALLOCATION_LABEL_FAILED:
    case REMOVE_ALLOCATION_LABEL_FAILED:
      return {
        ...state,
        status: action.type,
        checkListError: (action.data && action.data.error) || initialState.checkListError
      };
    case GET_ALL_ALLOCATION_LABELS_SUCCESS:
      return {
        ...state,
        status: action.type,
        allocationLabels: (action.data && action.data.allocationLabels) || initialState.allocationLabels
      };
    case ADD_ALLOCATION_LABEL_SUCCESS:
      return {
        ...state,
        status: action.type,
        allocationLabels: [...state.allocationLabels, action.data.allocationLabel]
      };
    case UPDATE_ALLOCATION_LABEL_SUCCESS: {
      const { id, name } = action.data.allocationLabel;
      const allocationLabels = state.allocationLabels.map(item => {
        if (item.id === id) {
          return { id, name };
        }
        return item;
      });
      return {
        ...state,
        status: action.type,
        allocationLabels
      };
    }
    case REMOVE_ALLOCATION_LABEL_SUCCESS: {
      const { id } = action.data.allocationLabel;
      const allocationLabels = state.allocationLabels.filter(item => item.id !== id);
      return {
        ...state,
        status: action.type,
        allocationLabels
      };
    }
    case CLEAR_ALLOCATION_LABELS:
      return {
        ...state,
        status: action.type,
        allocationLabels: initialState.allocationLabels
      };
    default:
      return state;
  }
};
