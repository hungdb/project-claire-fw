/*
 *=================================================================================================
 * Permission Schema Reducer
 *=================================================================================================
 */
import {
  GET_PERMISSION_SCHEMES_PROGRESS,
  GET_PERMISSION_SCHEMES_SUCCESS,
  GET_PERMISSION_SCHEMES_FAILED,
  GET_PROJECT_ROLES_PROGRESS,
  GET_PROJECT_ROLES_SUCCESS,
  GET_PROJECT_ROLES_FAILED,
  GET_PERMISSION_SCHEME_DETAIL_PROGRESS,
  GET_PERMISSION_SCHEME_DETAIL_SUCCESS,
  GET_PERMISSION_SCHEME_DETAIL_FAILED,
  GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS,
  GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS,
  GET_PERMISSION_OF_PROJECT_ROLE_FAILED,
  CREATE_PERMISSION_SCHEME_PROGRESS,
  CREATE_PERMISSION_SCHEME_SUCCESS,
  CREATE_PERMISSION_SCHEME_FAILED,
  UPDATE_PERMISSION_SCHEME_PROGRESS,
  UPDATE_PERMISSION_SCHEME_SUCCESS,
  UPDATE_PERMISSION_SCHEME_FAILED,
  CREATE_PROJECT_ROLE_PROGRESS,
  CREATE_PROJECT_ROLE_SUCCESS,
  CREATE_PROJECT_ROLE_FAILED,
  UPDATE_PROJECT_ROLE_PROGRESS,
  UPDATE_PROJECT_ROLE_SUCCESS,
  UPDATE_PROJECT_ROLE_FAILED,
  GET_ALL_PERMISSION_PROGRESS,
  GET_ALL_PERMISSION_SUCCESS,
  GET_ALL_PERMISSION_FAILED,
  CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  CREATE_PERMISSION_SCHEME_DETAIL_FAILED,
  REMOVE_PERMISSION_SCHEME_PROGRESS,
  REMOVE_PERMISSION_SCHEME_SUCCESS,
  REMOVE_PERMISSION_SCHEME_FAILED,
  REMOVE_PROJECT_ROLE_PROGRESS,
  REMOVE_PROJECT_ROLE_SUCCESS,
  REMOVE_PROJECT_ROLE_FAILED,
  REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  REMOVE_PERMISSION_SCHEME_DETAIL_FAILED,
  UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  UPDATE_PERMISSION_SCHEME_DETAIL_FAILED,
  CLEAR_PERMISSION_SCHEME
} from '../07-constants/permission-scheme';
import { sortObjectInArray } from '../08-helpers/common';

const initialState = {
  status: null,
  permissionSchemeError: null,
  permissionSchemes: [],
  permissionSchemeDetail: {},
  projectRoles: [],
  permissionsOfProjectRole: [],
  allPermissions: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PERMISSION_SCHEMES_PROGRESS:
    case GET_PROJECT_ROLES_PROGRESS:
    case GET_PERMISSION_SCHEME_DETAIL_PROGRESS:
    case GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS:
    case CREATE_PERMISSION_SCHEME_PROGRESS:
    case UPDATE_PERMISSION_SCHEME_PROGRESS:
    case CREATE_PROJECT_ROLE_PROGRESS:
    case UPDATE_PROJECT_ROLE_PROGRESS:
    case GET_ALL_PERMISSION_PROGRESS:
    case CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS:
    case REMOVE_PERMISSION_SCHEME_PROGRESS:
    case REMOVE_PROJECT_ROLE_PROGRESS:
    case UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS:
    case REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_PERMISSION_SCHEMES_FAILED:
    case GET_PROJECT_ROLES_FAILED:
    case GET_PERMISSION_SCHEME_DETAIL_FAILED:
    case GET_PERMISSION_OF_PROJECT_ROLE_FAILED:
    case CREATE_PERMISSION_SCHEME_FAILED:
    case UPDATE_PERMISSION_SCHEME_FAILED:
    case CREATE_PROJECT_ROLE_FAILED:
    case UPDATE_PROJECT_ROLE_FAILED:
    case GET_ALL_PERMISSION_FAILED:
    case CREATE_PERMISSION_SCHEME_DETAIL_FAILED:
    case REMOVE_PERMISSION_SCHEME_FAILED:
    case REMOVE_PROJECT_ROLE_FAILED:
    case UPDATE_PERMISSION_SCHEME_DETAIL_FAILED:
    case REMOVE_PERMISSION_SCHEME_DETAIL_FAILED:
      return {
        ...state,
        status: action.type,
        permissionSchemeError: action.data && action.data.error
      };
    case GET_PERMISSION_SCHEMES_SUCCESS:
      return {
        ...state,
        status: action.type,
        permissionSchemes: action.data.schemes
      };
    case GET_PROJECT_ROLES_SUCCESS: {
      const projectRoles = (action.data && action.data.projectRoles) || initialState.projectRoles;
      sortObjectInArray(projectRoles, ['priorityLevel', 'name']);
      return {
        ...state,
        status: action.type,
        projectRoles
      };
    }
    case GET_PERMISSION_SCHEME_DETAIL_SUCCESS:
      return {
        ...state,
        status: action.type,
        permissionSchemeDetail:
          (action.data && action.data.permissionSchemeDetail) || initialState.permissionSchemeDetail
      };
    case GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS:
      return {
        ...state,
        status: action.type,
        permissionsOfProjectRole:
          (action.data && action.data.permissionsOfProjectRole) || initialState.permissionsOfProjectRole
      };
    case CREATE_PERMISSION_SCHEME_SUCCESS:
      return {
        ...state,
        status: action.type,
        permissionSchemes: [...state.permissionSchemes, action.data]
      };
    case UPDATE_PERMISSION_SCHEME_SUCCESS: {
      const showAgainPS = state.permissionSchemes.map(list => {
        if (list.id === action.data.updatePermissionSchemeData.id) {
          return {
            id: action.data.updatePermissionSchemeData.id,
            name: action.data.updatePermissionSchemeData.name
          };
        } else {
          return {
            ...list,
            id: list.id,
            name: list.name
          };
        }
      });
      return {
        ...state,
        status: action.type,
        permissionSchemes: showAgainPS
      };
    }
    case CREATE_PROJECT_ROLE_SUCCESS: {
      const projectRoles = [...state.projectRoles, action.data.createProjectRoleData];
      sortObjectInArray(projectRoles, ['priorityLevel', 'name']);
      return {
        ...state,
        status: action.type,
        projectRoles
      };
    }
    case UPDATE_PROJECT_ROLE_SUCCESS: {
      const updateItem = action.data.updateProjectRoleData;
      const projectRoles = state.projectRoles.map(item => (item.id === updateItem.id ? updateItem : item));
      sortObjectInArray(projectRoles, ['priorityLevel', 'name']);
      return {
        ...state,
        status: action.type,
        projectRoles
      };
    }
    case GET_ALL_PERMISSION_SUCCESS:
      return {
        ...state,
        status: action.type,
        allPermissions: (action.data && action.data.allPermissions) || initialState.allPermissions
      };
    case CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS:
      return {
        ...state,
        status: action.type,
        permissionsOfProjectRole: [...state.permissionsOfProjectRole, action.data.createPermissionSchemeDetailData]
      };
    case REMOVE_PERMISSION_SCHEME_SUCCESS: {
      let newListPS = [];
      newListPS = state.permissionSchemes.filter(
        permissionScheme => permissionScheme.id !== action.data.removePermissionSchemeData.id
      );
      return {
        ...state,
        status: action.type,
        permissionSchemes: newListPS,
        permissionsOfProjectRole: initialState.permissionsOfProjectRole
      };
    }
    case REMOVE_PROJECT_ROLE_SUCCESS: {
      let newListPR = [];
      newListPR = state.projectRoles.filter(role => role.id !== action.data.removeProjectRoleData.id);
      return {
        ...state,
        status: action.type,
        projectRoles: newListPR
      };
    }
    case UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS: {
      const updateItem = action.data.updatePermissionSchemeDetailData;
      const permissionsOfProjectRole = state.permissionsOfProjectRole.map(item => {
        return item.id === updateItem.id ? updateItem : item;
      });
      return {
        ...state,
        permissionsOfProjectRole,
        status: action.type
      };
      // const arrayPSD = state.permissionsOfProjectRole.map((per) => {
      //   if (per.id === action.data.updatePermissionSchemeDetailData.permissionSchemeDetailId) {
      //     return ({
      //       id: action.data.updatePermissionSchemeDetailData.permissionSchemeDetailId,
      //       permissionSchemeId: action.data.updatePermissionSchemeDetailData.permissionSchemeId,
      //       projectRoleId: action.data.updatePermissionSchemeDetailData.projectRoleId,
      //       permission: action.data.updatePermissionSchemeDetailData.permission,
      //       permissionCondition: action.data.updatePermissionSchemeDetailData.permissionCondition
      //     });
      //   } else {
      //     return {
      //       ...per,
      //       id: per.id,
      //       permissionSchemeId: per.permissionSchemeId,
      //       projectRoleId: per.projectRoleId,
      //       permission: per.permission,
      //       permissionCondition: per.permissionCondition
      //     };
      //   }
      // });
      // return {
      //   ...state,
      //   status: action.type,
      //   permissionsOfProjectRole: arrayPSD
      // };
    }
    case REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS: {
      let newListPSD = [];
      newListPSD = state.permissionsOfProjectRole.filter(
        per => per.id !== action.data.removePermissionSchemeDetailData.permissionSchemeDetailId
      );
      return {
        ...state,
        status: action.type,
        permissionsOfProjectRole: newListPSD
      };
    }
    case CLEAR_PERMISSION_SCHEME:
      return {
        ...state,
        status: action.type,
        permissionsOfProjectRole: initialState.permissionsOfProjectRole,
        permissionSchemeDetail: initialState.permissionSchemeDetail
      };
    default:
      return state;
  }
};
