/*
 *=================================================================================================
 * KPI Leader Reducer
 *=================================================================================================
 */
import {
  GET_LIST_KPI_SPRINT_FAILED,
  GET_LIST_KPI_SPRINT_INPROGRESS,
  GET_LIST_KPI_SPRINT_SUCCESS,
  GET_LIST_PROJECT_FAILED,
  GET_LIST_PROJECT_INPROGRESS,
  GET_LIST_PROJECT_SUCCESS,
  CREATE_KPI_SPRINT_FAILED,
  CREATE_KPI_SPRINT_INPROGRESS,
  CREATE_KPI_SPRINT_SUCCESS,
  REMOVE_KPI_SPRINT_FAILED,
  REMOVE_KPI_SPRINT_INPROGRESS,
  REMOVE_KPI_SPRINT_SUCCESS,
  UPDATE_KPI_SPRINT_FAILED,
  UPDATE_KPI_SPRINT_INPROGRESS,
  UPDATE_KPI_SPRINT_SUCCESS
} from '../07-constants/kpiLeader';
import * as R from 'ramda';
import { PROJECT_MANAGER_NAME } from '../05-utils/commonData';

const initialState = {
  status: null,
  kpiLeaderError: null,
  listKpiSprint: [],
  managerOptions: [],
  total: 0,
  listProjects: [],
  projectOptions: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_PROJECT_INPROGRESS:
    case GET_LIST_KPI_SPRINT_INPROGRESS:
    case CREATE_KPI_SPRINT_INPROGRESS:
    case REMOVE_KPI_SPRINT_INPROGRESS:
    case UPDATE_KPI_SPRINT_INPROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_LIST_PROJECT_FAILED:
    case GET_LIST_KPI_SPRINT_FAILED:
    case CREATE_KPI_SPRINT_FAILED:
    case REMOVE_KPI_SPRINT_FAILED:
    case UPDATE_KPI_SPRINT_FAILED:
      return {
        ...state,
        status: action.type,
        kpiLeaderError: action.data && action.data.error
      };
    case GET_LIST_KPI_SPRINT_SUCCESS: {
      return {
        ...state,
        listKpiSprint: (action.data && action.data.listKpiSprint) || initialState.listKpiSprint,
        managerOptions: action.data.managerOptions,
        total: (action.data && action.data.total) || initialState.total
      };
    }
    case CREATE_KPI_SPRINT_SUCCESS: {
      let newManagerOptions = state.managerOptions;
      let newListKpiSprint = state.listKpiSprint;
      if (action.data && action.data.kpiCreation) {
        newListKpiSprint = [...state.listKpiSprint, action.data.kpiCreation];
      }
      if (action.data && action.data.managerOptions && action.data.managerOptions.length > 0) {
        action.data.managerOptions.forEach(userManage => {
          !R.includes(userManage, newManagerOptions) && (newManagerOptions = [...newManagerOptions, userManage]);
        });
      }
      return {
        ...state,
        listKpiSprint: newListKpiSprint,
        total: state.total + 1,
        managerOptions: newManagerOptions
      };
    }
    case REMOVE_KPI_SPRINT_SUCCESS: {
      let managerOptions = [];
      let newListKpiSprint = state.listKpiSprint;
      if (action.data.kpiRemove) {
        const indexRemove = newListKpiSprint.findIndex(a => a.id === action.data.kpiRemove);
        if (indexRemove !== -1) {
          newListKpiSprint = [...newListKpiSprint.slice(0, indexRemove), ...newListKpiSprint.slice(indexRemove + 1)];
          // eslint-disable-next-line
          newListKpiSprint.map(sprint => {
            sprint.project.users.forEach(user => {
              if (
                (user && user.ProjectRole && user.ProjectRole.name && user.ProjectRole.name.toUpperCase()) ===
                PROJECT_MANAGER_NAME
              ) {
                const manager = { value: user.User.id, label: user.User.name };
                !R.includes(manager, managerOptions) && (managerOptions = [...managerOptions, manager]);
              }
            });
          });
        }
      }
      return {
        ...state,
        listKpiSprint: newListKpiSprint,
        total: state.total - 1,
        managerOptions
      };
    }
    case UPDATE_KPI_SPRINT_SUCCESS: {
      let newListKpiSprint = state.listKpiSprint;
      if (action.data.kpiUpdate) {
        const indexUpdate = newListKpiSprint.findIndex(
          a => a.id === (action.data.kpiUpdate && action.data.kpiUpdate.id)
        );
        if (indexUpdate !== -1) {
          newListKpiSprint = [
            ...newListKpiSprint.slice(0, indexUpdate),
            action.data.kpiUpdate,
            ...newListKpiSprint.slice(indexUpdate + 1)
          ];
        }
      }
      return {
        ...state,
        listKpiSprint: newListKpiSprint
      };
    }
    case GET_LIST_PROJECT_SUCCESS:
      return {
        ...state,
        listProjects: (action.data && action.data.listProjects) || initialState.listProjects,
        projectOptions: action.data.projectOptions
      };
    default:
      return state;
  }
};
