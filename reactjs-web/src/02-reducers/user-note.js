/*
 *=================================================================================================
 * User Note Reducer
 *=================================================================================================
 */
import {
  CREATE_USER_NOTE,
  CREATE_USER_NOTE_SUCCESS,
  CREATE_USER_NOTE_FAILED,
  GET_USER_NOTES,
  GET_USER_NOTES_SUCCESS,
  GET_USER_NOTES_FAILED,
  UPDATE_USER_NOTE,
  UPDATE_USER_NOTE_SUCCESS,
  UPDATE_USER_NOTE_FAILED,
  DELETE_USER_NOTE,
  DELETE_USER_NOTE_SUCCESS,
  DELETE_USER_NOTE_FAILED
} from '../07-constants/user-note';

const initialState = {
  type: null,
  error: null,
  userNotes: []
};

const userNote = (state = initialState, action) => {
  switch (action.type) {
    // REQUEST
    case CREATE_USER_NOTE:
    case UPDATE_USER_NOTE:
    case DELETE_USER_NOTE:
      return {
        ...state,
        type: action.type
      };

    case GET_USER_NOTES:
      return {
        ...state,
        type: action.type,
        userNotes: []
      };

    // SUCCESS
    case CREATE_USER_NOTE_SUCCESS:
      return {
        ...state,
        type: action.type
      };

    case GET_USER_NOTES_SUCCESS:
      return {
        ...state,
        type: action.type,
        userNotes: action.userNotes
      };

    case UPDATE_USER_NOTE_SUCCESS:
      var userNotesUpdated = state.userNotes.map(n => {
        if (n.id === action.userNoteUpdated.id) return action.userNoteUpdated;
        return n;
      });

      return {
        ...state,
        type: action.type,
        userNotes: userNotesUpdated
      };

    case DELETE_USER_NOTE_SUCCESS:
      var userNotesDeleted = state.userNotes.filter(n => n.id !== action.userNoteIdDeleted);

      return {
        ...state,
        type: action.type,
        userNotes: userNotesDeleted
      };

    // FAILED
    case CREATE_USER_NOTE_FAILED:
    case GET_USER_NOTES_FAILED:
    case UPDATE_USER_NOTE_FAILED:
    case DELETE_USER_NOTE_FAILED:
      return {
        ...state,
        type: action.type,
        error: action.error
      };

    default:
      return state;
  }
};

export default userNote;
