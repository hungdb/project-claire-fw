/*
 *=================================================================================================
 * Allocation Reducer
 *=================================================================================================
 */
import {
  LIST_ALLOCATIONS_OF_USER_PROGRESS,
  LIST_ALLOCATIONS_OF_USER_SUCCESS,
  LIST_ALLOCATIONS_OF_USER_FAILED,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  LIST_SKILLS_BY_USER_PROGRESS,
  LIST_SKILLS_BY_USER_SUCCESS,
  LIST_SKILLS_BY_USER_FAILED,
  CLEAR_SKILLS,
  FILTER_BY_START_DATE,
  FILTER_BY_EFFORT_PERCENT,
  FILTER_BY_SEARCH_NAME,
  FILTER_BY_PROJECT,
  FILTER_BY_END_DATE,
  FILTER_BY_SKILL,
  FILTER_BY_PAGE,
  FILTER_ALLOCATION_SUCCESS,
  FILTER_ALLOCATION_PROGRESS,
  FILTER_ALLOCATION_FAILED,
  SWITCH_USER_ALLOCATION,
  CLEAR_ALLOCATION_OF_USER,
  CLEAR_SKILL_OF_USER
} from '../07-constants/allocation';

const initialState = {
  status: null,
  allocationError: null,
  filter: {
    startDate: null,
    endDate: null,
    effortPercent: null,
    skillId: null,
    projectId: null,
    name: null,
    perPage: 10,
    page: 1
  },
  listUsers: {
    users: [],
    total: 0
  },
  allocationsOfUser: [],
  skillsOfUser: [],
  selectedUser: null,
  userInfor: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_ALLOCATIONS_OF_USER_PROGRESS:
    case LIST_USERS_PROGRESS:
    case LIST_SKILLS_BY_USER_PROGRESS:
    case FILTER_ALLOCATION_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case LIST_ALLOCATIONS_OF_USER_FAILED:
    case LIST_USERS_FAILED:
    case LIST_SKILLS_BY_USER_FAILED:
    case FILTER_ALLOCATION_FAILED:
      return {
        ...state,
        status: action.type,
        allocationError: (action.data && action.data.error) || initialState.allocationError
      };
    case LIST_USERS_SUCCESS:
      return {
        ...state,
        status: action.type,
        listUsers: (action.data && action.data.listUsers) || initialState.listUsers
      };
    case LIST_ALLOCATIONS_OF_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        allocationsOfUser: (action.data && action.data.allocationsOfUser) || initialState.allocationsOfUser,
        userInfor: (action.data && action.data.userInfor) || initialState.userInfor
      };
    case LIST_SKILLS_BY_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        skillsOfUser: (action.data && action.data.skills) || initialState.skillsOfUser
      };
    case CLEAR_SKILLS:
      return {
        ...state,
        status: action.type,
        skillsOfUser: initialState.skillsOfUser
      };
    case FILTER_BY_START_DATE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          startDate: action.data.startDate
        }
      };
    case FILTER_BY_END_DATE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          endDate: action.data.endDate
        }
      };
    case FILTER_BY_EFFORT_PERCENT:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          effortPercent: action.data.effortPercent
        }
      };
    case FILTER_BY_SEARCH_NAME:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          name: action.data.name
        }
      };
    case FILTER_BY_PROJECT:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          projectId: action.data.projectId
        }
      };
    case FILTER_BY_SKILL:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          skillId: action.data.skillId
        }
      };
    case FILTER_BY_PAGE:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          page: action.page
        }
      };
    case FILTER_ALLOCATION_SUCCESS:
      return {
        ...state,
        status: action.type,
        listUsers: (action.data && action.data.listUsers) || initialState.listUsers
      };
    case SWITCH_USER_ALLOCATION:
      return {
        ...state,
        selectedUser: (action.data && action.data.user) || initialState.selectedUser
      };
    case CLEAR_ALLOCATION_OF_USER:
      return {
        ...state,
        allocationsOfUser: initialState.allocationsOfUser
      };
    case CLEAR_SKILL_OF_USER:
      return {
        ...state,
        skillsOfUser: initialState.skillsOfUser
      };
    default:
      return state;
  }
};
