/*
 *=================================================================================================
 * User In Project Reducer
 *=================================================================================================
 */
import {
  UIP_PROJECTS_GET,
  UIP_PROJECTS_GET_SUCCESS,
  UIP_PROJECTS_GET_FAILED,
  UIP_USERS_GET,
  UIP_USERS_GET_SUCCESS,
  UIP_USERS_GET_FAILED,
  UIP_USER_CREATE,
  UIP_USER_CREATE_SUCCESS,
  UIP_USER_CREATE_FAILED,
  UIP_USER_UPDATE,
  UIP_USER_UPDATE_SUCCESS,
  UIP_USER_UPDATE_FAILED,
  UIP_USER_DELETE,
  UIP_USER_DELETE_SUCCESS,
  UIP_USER_DELETE_FAILED,
  UIP_LIST_USER,
  UIP_LIST_USER_SUCCESS,
  UIP_LIST_USER_FAILED,
  UIP_LIST_ROLE,
  UIP_LIST_ROLE_SUCCESS,
  UIP_LIST_ROLE_FAILED,
  CLEAR_USER_IN_PROJECT,
  GET_USERS_OF_ALL_PROJECT,
  GET_USERS_OF_ALL_PROJECT_SUCCESS,
  GET_USERS_OF_ALL_PROJECT_FAILED
  // FILTER_USER_PROJECT
} from '../07-constants/userInProject';

const initialState = {
  status: null,
  error: null,
  projects: [],
  users: [],
  usersInProject: [],
  listRole: [],
  usersInProjects: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UIP_PROJECTS_GET:
    case UIP_USERS_GET:
    case UIP_USER_CREATE:
    case UIP_USER_UPDATE:
    case UIP_USER_DELETE:
    case UIP_LIST_USER:
    case UIP_LIST_ROLE:
    case GET_USERS_OF_ALL_PROJECT:
    case UIP_USER_CREATE_SUCCESS:
    case UIP_USER_UPDATE_SUCCESS:
    case UIP_USER_DELETE_SUCCESS:
      return {
        ...state,
        status: action.type
      };
    case UIP_PROJECTS_GET_FAILED:
    case UIP_USERS_GET_FAILED:
    case UIP_USER_CREATE_FAILED:
    case UIP_USER_UPDATE_FAILED:
    case UIP_USER_DELETE_FAILED:
    case UIP_LIST_USER_FAILED:
    case UIP_LIST_ROLE_FAILED:
    case GET_USERS_OF_ALL_PROJECT_FAILED:
      return {
        ...state,
        status: action.type,
        error: action.data
      };
    case UIP_PROJECTS_GET_SUCCESS:
      return {
        ...state,
        status: action.type,
        projects: action.data || initialState.projects
      };
    case UIP_USERS_GET_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersInProject: action.data || initialState.usersInProject
      };
    case UIP_LIST_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        users: (action.data && action.data.users) || initialState.users
      };
    case UIP_LIST_ROLE_SUCCESS:
      return {
        ...state,
        status: action.type,
        listRole: action.data || initialState.listRole
      };
    case CLEAR_USER_IN_PROJECT:
      return {
        ...state,
        status: action.type,
        usersInProject: initialState.usersInProject
      };
    case GET_USERS_OF_ALL_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersInProjects: action.data || initialState.usersInProjects
      };
    default:
      return state;
  }
};
