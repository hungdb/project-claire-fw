/*
 *=================================================================================================
 * Alert subscription reducer
 *=================================================================================================
 */
import {
  LIST_ALERT_SUBSCRIPTION_PROGRESS,
  LIST_ALERT_SUBSCRIPTION_SUCCESS,
  LIST_ALERT_SUBSCRIPTION_FAILED,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  LIST_PROJECT_PROGRESS,
  LIST_PROJECT_SUCCESS,
  LIST_PROJECT_FAILED
} from '../07-constants/alert-system';

const initialState = {
  status: null,
  listAlertSystem: [],
  total: 0,
  listUser: [],
  listProject: [],
  alertSystemError: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_ALERT_SUBSCRIPTION_PROGRESS:
    case LIST_USERS_PROGRESS:
    case LIST_PROJECT_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case LIST_ALERT_SUBSCRIPTION_FAILED:
    case LIST_USERS_FAILED:
    case LIST_PROJECT_FAILED:
      return {
        ...state,
        status: action.type,
        alertSystemError: (action.data && action.data.error) || initialState.alertSystemError
      };
    case LIST_ALERT_SUBSCRIPTION_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          listAlertSystem: action.data.listAlertSystem || initialState.listAlertSystem,
          total: action.data.total || initialState.total
        };
      }
      return { ...state, status: action.type };
    }
    case LIST_USERS_SUCCESS:
      return {
        ...state,
        status: action.type,
        listUser: (action.data && action.data.listUser) || initialState.listUser
      };
    case LIST_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listProject: (action.data && action.data.listProject) || initialState.listProject
      };
    default:
      return state;
  }
};
