/*
 *=================================================================================================
 * Wiki Reducer
 *=================================================================================================
 */
import {
  SELECT_DOCUMENT,
  WIKI_DOCUMENT_UI_STATUS,
  WIKI_FILE_ATTACHMENT_UI_STATUS,
  GET_LIST_DOCUMENT_PROGRESS,
  GET_LIST_DOCUMENT_SUCCESS,
  GET_LIST_DOCUMENT_FAILED,
  LOAD_MORE_DOCUMENT_PROGRESS,
  LOAD_MORE_DOCUMENT_SUCCESS,
  LOAD_MORE_DOCUMENT_FAILED,
  SEARCH_DOCUMENT_PROGRESS,
  SEARCH_DOCUMENT_SUCCESS,
  SEARCH_DOCUMENT_FAILED,
  ADD_DOCUMENT_PROGRESS,
  ADD_DOCUMENT_SUCCESS,
  ADD_DOCUMENT_FAILED,
  UPDATE_DOCUMENT_PROGRESS,
  UPDATE_DOCUMENT_SUCCESS,
  UPDATE_DOCUMENT_FAILED,
  // UPDATE_DOCUMENT_PARENT_REQUEST,
  // UPDATE_DOCUMENT_PARENT_SUCCESS,
  // UPDATE_DOCUMENT_PARENT_FAILURE,
  REMOVE_DOCUMENT_PROGRESS,
  REMOVE_DOCUMENT_SUCCESS,
  REMOVE_DOCUMENT_FAILED,
  GET_LIST_ATTACHMENT_PROGRESS,
  GET_LIST_ATTACHMENT_SUCCESS,
  GET_LIST_ATTACHMENT_FAILED,
  LOAD_MORE_ATTACHMENT_PROGRESS,
  LOAD_MORE_ATTAHMENT_SUCCESS,
  LOAD_MORE_ATTACHMENT_FAILED,
  SEARCH_ATTACHMENT_PROGRESS,
  SEARCH_ATTACHMENT_SUCCESS,
  SEARCH_ATTACHMENT_FAILED,
  REMOVE_ATTACHMENT_PROGRESS,
  REMOVE_ATTACHMENT_SUCCESS,
  REMOVE_ATTACHMENT_FAILED,
  GET_PRE_SIGNED_URL_ATTACHMENT_PROGRESS,
  GET_PRE_SIGNED_URL_ATTACHMENT_FAILED,
  GET_PRE_SIGNED_URL_ATTACHMENT_SUCCESS,
  UPLOAD_ATTACHMENT_PROGRESS,
  UPLOAD_ATTACHMENT_SUCCESS,
  UPLOAD_ATTACHMENT_FAILED,
  GET_LIST_ATTACHMENT_OF_TASK_PROGRESS,
  GET_LIST_ATTACHMENT_OF_TASK_FAILED,
  GET_LIST_ATTACHMENT_OF_TASK_SUCCESS,
  UPLOAD_ATTACHMENT_TO_TASK_PROGRESS,
  UPLOAD_ATTACHMENT_TO_TASK_FAILED,
  UPLOAD_ATTACHMENT_TO_TASK_SUCCESS,
  LOAD_MORE_ATTACHMENT_OF_TASK_FAILED,
  LOAD_MORE_ATTACHMENT_OF_TASK_PROGRESS,
  LOAD_MORE_ATTACHMENT_OF_TASK_SUCCESS,
  REMOVE_ATTACHMENT_OF_TASK_FAILED,
  REMOVE_ATTACHMENT_OF_TASK_PROGRESS,
  REMOVE_ATTACHMENT_OF_TASK_SUCCESS
} from '../07-constants/wiki';
import { SWITCH_SPRINT_PROJECT } from '../07-constants/sprint';
import { PAGINATE_DOCUMENT_LIMIT, PAGINATE_ATTACHMENT_LIMIT } from '../05-utils/commonData';
import { calculateLength, parseJSON } from '../08-helpers/common';
import { getKey } from '../08-helpers/storage';
import { USER_INFO } from '../07-constants/common';

const initialState = {
  status: null,
  error: null,
  statusDocumentUI: 0,
  statusFileAttachmentUI: 0,
  documents: [],
  documentTotal: 0,
  attachments: [],
  attachmentTotal: 0,
  attachmentsOfTask: [],
  attachmentsOfTaskTotal: 0,
  selectedDocument: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SWITCH_SPRINT_PROJECT:
      return {
        ...state,
        status: action.type,
        selectedDocument: null
      };

    case SELECT_DOCUMENT:
      return {
        ...state,
        status: action.type,
        selectedDocument: action.selectedDocument
      };

    case WIKI_DOCUMENT_UI_STATUS:
      return {
        ...state,
        status: action.type,
        statusDocumentUI: action.statusDocumentUI
      };
    case WIKI_FILE_ATTACHMENT_UI_STATUS:
      return {
        ...state,
        status: action.type,
        statusFileAttachmentUI: action.statusFileAttachmentUI
      };
    case LOAD_MORE_DOCUMENT_PROGRESS:
    case SEARCH_DOCUMENT_PROGRESS:
    case ADD_DOCUMENT_PROGRESS:
    case UPDATE_DOCUMENT_PROGRESS:
    case REMOVE_DOCUMENT_PROGRESS:
    case GET_LIST_ATTACHMENT_PROGRESS:
    case LOAD_MORE_ATTACHMENT_PROGRESS:
    case SEARCH_ATTACHMENT_PROGRESS:
    case REMOVE_ATTACHMENT_PROGRESS:
    case GET_PRE_SIGNED_URL_ATTACHMENT_PROGRESS:
    case GET_PRE_SIGNED_URL_ATTACHMENT_SUCCESS:
    case UPLOAD_ATTACHMENT_PROGRESS:
    case GET_LIST_ATTACHMENT_OF_TASK_PROGRESS:
    case UPLOAD_ATTACHMENT_TO_TASK_PROGRESS:
    case LOAD_MORE_ATTACHMENT_OF_TASK_PROGRESS:
    case REMOVE_ATTACHMENT_OF_TASK_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_LIST_DOCUMENT_FAILED:
    case LOAD_MORE_DOCUMENT_FAILED:
    case SEARCH_DOCUMENT_FAILED:
    case ADD_DOCUMENT_FAILED:
    case UPDATE_DOCUMENT_FAILED:
    case REMOVE_DOCUMENT_FAILED:
    case GET_LIST_ATTACHMENT_FAILED:
    case LOAD_MORE_ATTACHMENT_FAILED:
    case SEARCH_ATTACHMENT_FAILED:
    case REMOVE_ATTACHMENT_FAILED:
    case GET_PRE_SIGNED_URL_ATTACHMENT_FAILED:
    case UPLOAD_ATTACHMENT_FAILED:
    case GET_LIST_ATTACHMENT_OF_TASK_FAILED:
    case UPLOAD_ATTACHMENT_TO_TASK_FAILED:
    case LOAD_MORE_ATTACHMENT_OF_TASK_FAILED:
    case REMOVE_ATTACHMENT_OF_TASK_FAILED:
      return {
        ...state,
        status: action.type,
        error: action.data && action.data.error
      };
    case GET_LIST_DOCUMENT_PROGRESS:
      return {
        ...state,
        status: action.type,
        documents: [],
        documentTotal: 0
      };
    case GET_LIST_DOCUMENT_SUCCESS:
    case SEARCH_DOCUMENT_SUCCESS:
      return {
        ...state,
        status: action.type,
        documents: (action.data && action.data.documents) || initialState.documents,
        documentTotal: (action.data && action.data.total) || initialState.documentTotal
      };
    case LOAD_MORE_DOCUMENT_SUCCESS: {
      const newDocuments = (action.data && action.data.documents) || initialState.documents;
      return {
        ...state,
        status: action.type,
        documents: [...state.documents, ...newDocuments],
        documentTotal: (action.data && action.data.total) || initialState.documentTotal
      };
    }
    case ADD_DOCUMENT_SUCCESS: {
      const newDocument = action.data && action.data.document;
      const user = parseJSON(getKey(USER_INFO));
      newDocument.User = {
        id: newDocument && newDocument.userId,
        name: user && user.username,
        avatar: user && user.profilePhoto
      };
      if (newDocument) {
        if (calculateLength(state.documents) < PAGINATE_DOCUMENT_LIMIT) {
          return {
            ...state,
            status: action.type,
            documents: [newDocument, ...state.documents],
            documentTotal: state.documentTotal + 1
          };
        } else {
          state.documents.pop();
          return {
            ...state,
            status: action.type,
            documents: [newDocument, ...state.documents],
            documentTotal: state.documentTotal + 1
          };
        }
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_DOCUMENT_SUCCESS: {
      // const newDocument = action.data && action.data.document;
      // if (newDocument) {
      //   const index = state.documents.findIndex(document => document.id === newDocument.id);
      //   if (index > -1) {
      //     state.documents[ index ].subject = newDocument.subject;
      //     state.documents[ index ].content = newDocument.content;
      //     state.documents[ index ].parentId = newDocument.parentId;
      //   }
      //   return {
      //     ...state,
      //     status: action.type,
      //     documents: [ ...state.documents ],
      //     selectedDocument: { ...state.selectedDocument, subject: action.data.document.subject, content: action.data.document.content }
      //   };
      // }
      // return {
      //   ...state,
      //   status: action.type
      // };
      const newDocuments = state.documents.map(document => {
        if (document.id === action.data.document.id)
          return {
            ...document,
            subject: action.data.document.subject,
            content: action.data.document.content,
            parentId: Number(action.data.document.parentId)
          };
        return document;
      });
      // console.log('newDocuments', newDocuments);

      return {
        ...state,
        status: action.type,
        documents: newDocuments,
        selectedDocument: {
          ...state.selectedDocument,
          subject: action.data.document.subject,
          content: action.data.document.content
        }
      };
    }
    case REMOVE_DOCUMENT_SUCCESS: {
      const newDocument = action.data && action.data.document;
      if (newDocument) {
        const documents = state.documents.filter(document => document.id !== newDocument.id);
        return {
          ...state,
          status: action.type,
          documents,
          documentTotal: state.documentTotal - 1,
          selectedDocument: null
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case REMOVE_ATTACHMENT_OF_TASK_SUCCESS: {
      const newAttachment = action.data && action.data.attachmentOfTask;
      if (newAttachment) {
        const attachmentsOfTask = state.attachmentsOfTask.filter(attachment => attachment.id !== newAttachment.id);
        return {
          ...state,
          status: action.type,
          attachmentsOfTask,
          attachmentsOfTaskTotal: state.attachmentsOfTaskTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case GET_LIST_ATTACHMENT_OF_TASK_SUCCESS:
      return {
        ...state,
        status: action.type,
        attachmentsOfTask: (action.data && action.data.attachmentsOfTask) || initialState.attachmentsOfTask,
        attachmentsOfTaskTotal: (action.data && action.data.total) || initialState.attachmentsOfTaskTotal
      };
    case GET_LIST_ATTACHMENT_SUCCESS:
    case SEARCH_ATTACHMENT_SUCCESS:
      return {
        ...state,
        status: action.type,
        attachments: (action.data && action.data.attachments) || initialState.attachments,
        attachmentTotal: (action.data && action.data.total) || initialState.attachmentTotal
      };
    case LOAD_MORE_ATTAHMENT_SUCCESS: {
      const newAttachments = (action.data && action.data.attachments) || initialState.attachments;
      return {
        ...state,
        status: action.type,
        attachments: [...state.attachments, ...newAttachments],
        attachmentTotal: (action.data && action.data.total) || initialState.attachmentTotal
      };
    }
    case LOAD_MORE_ATTACHMENT_OF_TASK_SUCCESS:
      const newAttachments = (action.data && action.data.attachmentsOfTask) || initialState.attachmentsOfTask;
      return {
        ...state,
        status: action.type,
        attachmentsOfTask: [...state.attachmentsOfTask, ...newAttachments],
        attachmentsOfTaskTotal: (action.data && action.data.total) || initialState.attachmentsOfTaskTotal
      };
    case REMOVE_ATTACHMENT_SUCCESS: {
      const newAttachment = action.data && action.data.attachment;
      if (newAttachment) {
        const attachments = state.attachments.filter(attachment => attachment.id !== newAttachment.id);
        return {
          ...state,
          status: action.type,
          attachments,
          attachmentTotal: state.attachmentTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPLOAD_ATTACHMENT_SUCCESS: {
      const newAttachments = action.data && action.data.attachments;
      if (newAttachments) {
        const newAttachmentsLength = calculateLength(newAttachments);
        let attachments = [...newAttachments, ...state.attachments];
        if (calculateLength(state.attachments) + newAttachmentsLength > PAGINATE_ATTACHMENT_LIMIT) {
          attachments = attachments.slice(0, PAGINATE_ATTACHMENT_LIMIT);
        }
        return {
          ...state,
          status: action.type,
          attachments,
          attachmentTotal: state.attachmentTotal + newAttachmentsLength
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPLOAD_ATTACHMENT_TO_TASK_SUCCESS: {
      const newAttachments = action.data && action.data.attachmentsOfTask;
      if (newAttachments) {
        const newAttachmentsLength = calculateLength(newAttachments);
        if (calculateLength(state.attachmentsOfTask) + newAttachmentsLength < PAGINATE_ATTACHMENT_LIMIT) {
          return {
            ...state,
            status: action.type,
            attachmentsOfTask: [...newAttachments, ...state.attachmentsOfTask],
            attachmentsOfTaskTotal: state.attachmentsOfTaskTotal + newAttachmentsLength
          };
        } else {
          const attachmentsOfTask = [...newAttachments, ...state.attachmentsOfTask];
          return {
            ...state,
            status: action.type,
            attachmentsOfTask: attachmentsOfTask.slice(0, PAGINATE_ATTACHMENT_LIMIT),
            attachmentsOfTaskTotal: PAGINATE_ATTACHMENT_LIMIT
          };
        }
      }
      return {
        ...state,
        status: action.type
      };
    }
    default:
      return state;
  }
};
