/*
 *=================================================================================================
 * Sprint Reducer
 *=================================================================================================
 */
import {
  SPRINTS_PAGE_URL_CACHE,
  GET_TASK_BACKLOG_PROGRESS,
  GET_TASK_BACKLOG_PROJECT_SUCCESS,
  GET_TASK_BACKLOG_PROJECT_FAILED,
  CLEAR_TASK_BACKLOG,
  GET_SUGGEST_TAG_PROGRESS,
  GET_SUGGEST_TAG_FAILED,
  GET_SUGGEST_TAG_SUCCESS,
  CHANGE_ESTIMATE_PROGRESS,
  CHANGE_ESTIMATE_SUCCESS,
  CHANGE_ESTIMATE_FAILED,
  CREATE_TASK_PROGRESS,
  CREATE_TASK_SUCCESS,
  CREATE_TASK_FAILED,
  UPDATE_TASK_PROGRESS,
  UPDATE_TASK_SUCCESS,
  UPDATE_TASK_FAILED,
  GET_DETAIL_TASK_PROGRESS,
  GET_DETAIL_TASK_FAILED,
  GET_DETAIL_TASK_SUCCESS,
  DELETE_TASK_PROGRESS,
  DELETE_TASK_SUCCESS,
  DELETE_TASK_FAILED,
  GET_SPRINTS_PROGRESS,
  GET_SPRINTS_FAILED,
  GET_SPRINTS_SUCCESS,
  UPDATE_SPRINT_PROGRESS,
  UPDATE_SPRINT_SUCCESS,
  UPDATE_SPRINT_FAILED,
  DELETE_SPRINT_PROGRESS,
  DELETE_SPRINT_SUCCESS,
  DELETE_SPRINT_FAILED,
  CREATE_SPRINT_PROGRESS,
  CREATE_SPRINT_SUCCESS,
  CREATE_SPRINT_FAILED,
  GET_SPRINTS_DIFF_COMPLETED_PROGRESS,
  GET_SPRINTS_DIFF_COMPLETED_SUCCESS,
  GET_SPRINTS_DIFF_COMPLETED_FAILED,
  CLEAR_SPRINTS_DIFF_COMPLETED,
  GET_DETAIL_SPRINT_PROGRESS,
  GET_DETAIL_SPRINT_SUCCESS,
  GET_DETAIL_SPRINT_FAILED,
  CLEAR_DETAIL_SPRINT,
  SEARCH_BACKLOG_TASK_PROGRESS,
  SEARCH_BACKLOG_TASK_SUCCESS,
  SEARCH_BACKLOG_TASK_FAILED,
  REMOVE_TASK_TO_BACKLOG_PROGRESS,
  REMOVE_TASK_TO_BACKLOG_SUCCESS,
  REMOVE_TASK_TO_BACKLOG_FAILED,
  ADD_TASK_TO_SPRINT_PROGRESS,
  ADD_TASK_TO_SPRINT_SUCCESS,
  ADD_TASK_TO_SPRINT_FAILED,
  REMOVE_ALLOCATION_PROGRESS,
  REMOVE_ALLOCATION_SUCCESS,
  REMOVE_ALLOCATION_FAILED,
  ADD_ALLOCATION_PROGRESS,
  ADD_ALLOCATION_SUCCESS,
  ADD_ALLOCATION_FAILED,
  GET_STATISTICAL_ACTIVE_SPRINT_PROGRESS,
  GET_STATISTICAL_ACTIVE_SPRINT_SUCCESS,
  GET_STATISTICAL_ACTIVE_SPRINT_FAILED,
  GET_WORKFLOW_BY_PROJECT_PROGRESS,
  GET_WORKFLOW_BY_PROJECT_SUCCESS,
  GET_WORKFLOW_BY_PROJECT_FAILED,
  MOVE_TASK_PROGRESS,
  MOVE_TASK_SUCCESS,
  MOVE_TASK_FAILED,
  SWITCH_SPRINT_PROJECT,
  CLEAR_CURRENT_TASK,
  GET_USERS_BY_PROJECT_PROGRESS,
  GET_USERS_BY_PROJECT_FAILED,
  GET_USERS_BY_PROJECT_SUCCESS,
  SUGGEST_MILESTONES_SUCCESS,
  SUGGEST_MILESTONES_FAILED,
  SUGGEST_MILESTONES_PROGRESS,
  GET_ALL_TASKS_PROGRESS,
  GET_ALL_TASKS_SUCCESS,
  GET_ALL_TASKS_FAILED,
  GET_USERS_OF_TASK_FAILED,
  GET_USERS_OF_TASK_PROGRESS,
  GET_USERS_OF_TASK_SUCCESS,
  CLEAR_ALL_BOARD,
  CLEAR_SPRINTS_LIST,
  SET_TYPE_GET_TASK,
  SET_ACTIVE_TAB_BOARD,
  SET_KEY_SEARCH_TASK,
  ASSIGN_TASK_TO_USER_PROGRESS,
  ASSIGN_TASK_TO_USER_SUCCESS,
  ASSIGN_TASK_TO_USER_FAILED,
  ADD_USER_SKILL_IN_SPRINT_PROGRESS,
  ADD_USER_SKILL_IN_SPRINT_SUCCESS,
  ADD_USER_SKILL_IN_SPRINT_FAILED,
  LIST_BEHAVIORS_PROGRESS,
  LIST_BEHAVIORS_SUCCESS,
  LIST_BEHAVIORS_FAILED,
  LOG_BEHAVIOR_SPRINT_PROGRESS,
  LOG_BEHAVIOR_SPRINT_SUCCESS,
  LOG_BEHAVIOR_SPRINT_FAILED,
  GET_LIST_ALLOCATION_LABELS_REQUEST,
  GET_LIST_ALLOCATION_LABELS_SUCCESS,
  GET_LIST_ALLOCATION_LABELS_FAILED,
  LIST_USER_SKILLS_PROGRESS,
  LIST_USER_SKILLS_SUCCESS,
  LIST_USER_SKILLS_FAILED,
  REMOVE_USER_SKILL_PROGRESS,
  REMOVE_USER_SKILL_SUCCESS,
  REMOVE_USER_SKILL_FAILED,
  LIST_USER_SKILLS_BY_SPRINT_PROGRESS,
  LIST_USER_SKILLS_BY_SPRINT_SUCCESS,
  LIST_USER_SKILLS_BY_SPRINT_FAILED,
  REMOVE_USER_SKILLS_IN_SPRINT_PROGRESS,
  REMOVE_USER_SKILLS_IN_SPRINT_SUCCESS,
  REMOVE_USER_SKILLS_IN_SPRINT_FAILED,
  ADD_MY_SKILL_IN_SPRINT_SUCCESS,
  ADD_MY_SKILL_IN_SPRINT_FAILED,
  ADD_MY_SKILL_IN_SPRINT_PROGRESS,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_PROGRESS,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_SUCCESS,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_FAILED,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_PROGRESS,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_SUCCESS,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_FAILED,
  UPDATE_ALLOCATION_REQUEST,
  UPDATE_ALLOCATION_SUCCESS,
  UPDATE_ALLOCATION_FAILED,
  FILTER_USER_ON_BOARD,
  CLEAR_FILTER_USER_ON_BOARD
} from '../07-constants/sprint';
import { parseJSON, calculateLength, totalByPriority } from '../08-helpers/common';
import { PROJECT_GET_BY_USER_SUCCESS } from '../07-constants/project';
import { EFFORT_TYPES } from '../05-utils/commonData';
import _ from 'lodash';

const initialState = {
  selectedProject: null,
  status: null,
  sprintError: null,
  backlog: [],
  tags: [],
  currentTask: null,
  sprints: [],
  sprintsDiffCompleted: [],
  sprint: null,
  searchBacklog: [],
  activeSprint: null,
  workflowProject: [],
  usersIntoProject: [],
  milestones: [],
  suggestTasks: [],
  usersOfTask: [],
  typeGetTask: 2,
  activeTabBoard: 1,
  keySearchTask: null,
  allMySkillByUser: [],
  allocationLabels: [],
  allUsersBehaviorSprint: [],
  selectedUserBoard: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SPRINTS_PAGE_URL_CACHE:
      return {
        ...state,
        status: action.type,
        urlCache: action.urlCache
      };
    case GET_TASK_BACKLOG_PROGRESS:
    case GET_SUGGEST_TAG_PROGRESS:
    case CHANGE_ESTIMATE_PROGRESS:
    case CREATE_TASK_PROGRESS:
    case CREATE_TASK_SUCCESS:
    case UPDATE_TASK_PROGRESS:
    case GET_DETAIL_TASK_PROGRESS:
    case DELETE_TASK_PROGRESS:
    case GET_SPRINTS_PROGRESS:
    case UPDATE_SPRINT_PROGRESS:
    case UPDATE_SPRINT_SUCCESS:
    case DELETE_SPRINT_PROGRESS:
    case DELETE_SPRINT_SUCCESS:
    case CREATE_SPRINT_PROGRESS:
    case CREATE_SPRINT_SUCCESS:
    case GET_SPRINTS_DIFF_COMPLETED_PROGRESS:
    case GET_DETAIL_SPRINT_PROGRESS:
    case SEARCH_BACKLOG_TASK_PROGRESS:
    case REMOVE_TASK_TO_BACKLOG_PROGRESS:
    case REMOVE_TASK_TO_BACKLOG_SUCCESS:
    case ADD_TASK_TO_SPRINT_PROGRESS:
    case ADD_TASK_TO_SPRINT_SUCCESS:
    case REMOVE_ALLOCATION_PROGRESS:
    case REMOVE_ALLOCATION_SUCCESS:
    case ADD_ALLOCATION_PROGRESS:
    case ADD_ALLOCATION_SUCCESS:
    case GET_STATISTICAL_ACTIVE_SPRINT_PROGRESS:
    case GET_WORKFLOW_BY_PROJECT_PROGRESS:
    case MOVE_TASK_PROGRESS:
    case MOVE_TASK_SUCCESS:
    case GET_USERS_BY_PROJECT_PROGRESS:
    case SUGGEST_MILESTONES_PROGRESS:
    case GET_ALL_TASKS_PROGRESS:
    case GET_USERS_OF_TASK_PROGRESS:
    case ASSIGN_TASK_TO_USER_PROGRESS:
    case ADD_USER_SKILL_IN_SPRINT_PROGRESS:
    case LOG_BEHAVIOR_SPRINT_PROGRESS:
    case LIST_BEHAVIORS_PROGRESS:
    case GET_LIST_ALLOCATION_LABELS_REQUEST:
    case LIST_USER_SKILLS_PROGRESS:
    case REMOVE_USER_SKILL_PROGRESS:
    case LIST_USER_SKILLS_BY_SPRINT_PROGRESS:
    case REMOVE_USER_SKILLS_IN_SPRINT_PROGRESS:
    case ADD_MY_SKILL_IN_SPRINT_PROGRESS:
    case REMOVE_LOG_BEHAVIOR_IN_SPRINT_PROGRESS:
    case UPDATE_ALLOCATION_REQUEST:
      return {
        ...state,
        status: action.type
      };
    case GET_TASK_BACKLOG_PROJECT_FAILED:
    case GET_SUGGEST_TAG_FAILED:
    case CHANGE_ESTIMATE_FAILED:
    case CREATE_TASK_FAILED:
    case UPDATE_TASK_FAILED:
    case GET_DETAIL_TASK_FAILED:
    case DELETE_TASK_FAILED:
    case GET_SPRINTS_FAILED:
    case UPDATE_SPRINT_FAILED:
    case DELETE_SPRINT_FAILED:
    case CREATE_SPRINT_FAILED:
    case GET_SPRINTS_DIFF_COMPLETED_FAILED:
    case GET_DETAIL_SPRINT_FAILED:
    case SEARCH_BACKLOG_TASK_FAILED:
    case REMOVE_TASK_TO_BACKLOG_FAILED:
    case ADD_TASK_TO_SPRINT_FAILED:
    case REMOVE_ALLOCATION_FAILED:
    case ADD_ALLOCATION_FAILED:
    case GET_STATISTICAL_ACTIVE_SPRINT_FAILED:
    case GET_WORKFLOW_BY_PROJECT_FAILED:
    case MOVE_TASK_FAILED:
    case GET_USERS_BY_PROJECT_FAILED:
    case SUGGEST_MILESTONES_FAILED:
    case LOG_BEHAVIOR_SPRINT_FAILED:
    case GET_ALL_TASKS_FAILED:
    case GET_USERS_OF_TASK_FAILED:
    case ASSIGN_TASK_TO_USER_FAILED:
    case ADD_USER_SKILL_IN_SPRINT_FAILED:
    case LIST_BEHAVIORS_FAILED:
    case GET_LIST_ALLOCATION_LABELS_FAILED:
    case LIST_USER_SKILLS_FAILED:
    case REMOVE_USER_SKILL_FAILED:
    case LIST_USER_SKILLS_BY_SPRINT_FAILED:
    case REMOVE_USER_SKILLS_IN_SPRINT_FAILED:
    case ADD_MY_SKILL_IN_SPRINT_FAILED:
    case GET_LIST_LOG_BEHAVIOR_IN_SPRINT_FAILED:
    case REMOVE_LOG_BEHAVIOR_IN_SPRINT_FAILED:
      return {
        ...state,
        status: action.type,
        sprintError: action.data && action.data.error
      };
    case ADD_MY_SKILL_IN_SPRINT_SUCCESS: {
      const skillIndex = state.allMySkillByUser.findIndex(s => s.skillId === (action.data && action.data.skillId));
      const skillUpdated = state.allMySkillByUser.map(s => {
        if (s.skillId === action.data.skillId) {
          return {
            ...s,
            createdAt: action.data && action.data.createdAt,
            note: action.data && action.data.note
          };
        }
        return s;
      });
      if (skillIndex === -1) {
        return {
          ...state,
          allMySkillByUser: [action.data, ...state.allMySkillByUser]
        };
      }
      return {
        ...state,
        allMySkillByUser: skillUpdated
      };
    }
    case REMOVE_USER_SKILL_SUCCESS: {
      const removedUserSkill = state.allMySkillByUser.filter(s => s.id !== (action.data && action.data.id));
      return {
        ...state,
        allMySkillByUser: removedUserSkill
      };
    }
    case LIST_USER_SKILLS_SUCCESS:
      return {
        ...state,
        status: action.type,
        allMySkillByUser: action.data || initialState.allMySkillByUser
      };
    case GET_TASK_BACKLOG_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        backlog: (action.data && action.data.backlog) || initialState.backlog
      };
    case CLEAR_TASK_BACKLOG:
      return {
        ...state,
        status: action.type,
        backlog: initialState.backlog
      };
    case GET_SUGGEST_TAG_SUCCESS:
      return {
        ...state,
        status: action.type,
        tags: (action.data && action.data.tags) || initialState.tags
      };
    case GET_DETAIL_TASK_SUCCESS:
    case CHANGE_ESTIMATE_SUCCESS:
      return {
        ...state,
        status: action.type,
        currentTask: (action.data && action.data.info) || initialState.currentTask
      };
    case GET_SPRINTS_SUCCESS:
      return {
        ...state,
        status: action.type,
        sprints: (action.data && action.data.sprints) || initialState.sprints
      };
    case GET_SPRINTS_DIFF_COMPLETED_SUCCESS:
      return {
        ...state,
        status: action.type,
        sprintsDiffCompleted: (action.data && action.data.sprintsDiffCompleted) || initialState.sprintsDiffCompleted
      };
    case GET_DETAIL_SPRINT_SUCCESS:
      return {
        ...state,
        status: action.type,
        sprint: (action.data && action.data.info) || initialState.sprint
      };
    case CLEAR_DETAIL_SPRINT:
      return {
        ...state,
        status: action.type,
        sprint: initialState.sprint
      };
    case CLEAR_SPRINTS_DIFF_COMPLETED:
      return {
        ...state,
        status: action.type,
        sprintsDiffCompleted: initialState.sprintsDiffCompleted
      };
    case SEARCH_BACKLOG_TASK_SUCCESS:
      return {
        ...state,
        status: action.type,
        searchBacklog: (action.data && action.data.backlog) || initialState.searchBacklog
      };
    case GET_STATISTICAL_ACTIVE_SPRINT_SUCCESS:
      return {
        ...state,
        status: action.type,
        activeSprint: (action.data && action.data.statistical) || initialState.activeSprint
      };
    case GET_WORKFLOW_BY_PROJECT_SUCCESS: {
      const workflowProject = (action.data && action.data.workflow) || initialState.workflowProject;
      if (workflowProject.length > 0) {
        const lanes = workflowProject.map(lane => {
          const cards =
            (lane.tasks &&
              lane.tasks.map(task => {
                return {
                  id: task.id.toString(),
                  description: task.description,
                  title: task.content,
                  time: `${task.estimate}h`,
                  status: parseJSON(task.tag),
                  createdAt: task.createdAt,
                  effortType: task.WorkflowState && task.WorkflowState.effortType,
                  estimate: task.estimate,
                  startDate: task.startDate,
                  endDate: task.endDate,
                  totalTime: task.totalTime,
                  projectId: task.projectId,
                  dueDate: task.dueDate,
                  priority: task.priority,
                  assignee: task.assigneeAs,
                  taskCode: task.taskCode,
                  relateUsers: task.relateUsers || [],
                  type: task.type,
                  totalComments: task.totalComments
                };
              })) ||
            [];
          return {
            id: lane.id.toString(),
            title: lane.name && lane.name.toUpperCase(),
            total: `(${calculateLength(cards)} - ${totalByPriority(cards, 'estimate')}h)`,
            effortType: lane.effortType,
            cards: lane.effortType === EFFORT_TYPES.UNTOUCHED ? _.orderBy(cards, ['priority'], ['desc']) : cards,
            className: 'custom-lane'
          };
        });
        return {
          ...state,
          status: action.type,
          workflowProject: lanes
        };
      }
      return {
        ...state,
        status: action.type,
        workflowProject: initialState.workflowProject
      };
    }

    case FILTER_USER_ON_BOARD:
      return {
        ...state,
        selectedUserBoard: action.data.assignee
      };

    case CLEAR_FILTER_USER_ON_BOARD:
      return {
        ...state,
        selectedUserBoard: initialState.selectedUserBoard
      };

    case SWITCH_SPRINT_PROJECT:
      return {
        ...state,
        selectedProject: action.data.project
      };

    case PROJECT_GET_BY_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        selectedProject:
          state.selectedProject && action.data.listProject
            ? action.data.listProject.find(p => p.id === state.selectedProject.id)
            : null
      };
    case CLEAR_CURRENT_TASK:
      return {
        ...state,
        status: action.type,
        currentTask: null
      };
    case GET_USERS_BY_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersIntoProject: (action.data && action.data.users) || initialState.usersIntoProject
      };
    case GET_USERS_OF_TASK_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersOfTask: (action.data && action.data.users) || initialState.usersOfTask
      };
    case SUGGEST_MILESTONES_SUCCESS:
      return {
        ...state,
        status: action.type,
        milestones: (action.data && action.data.milestones) || initialState.milestones
      };

    case GET_ALL_TASKS_SUCCESS:
      return {
        ...state,
        status: action.type,
        suggestTasks: (action.data && action.data.tasks) || initialState.suggestTasks
      };
    case CLEAR_ALL_BOARD:
      return {
        ...state,
        activeSprint: initialState.activeSprint,
        workflowProject: initialState.workflowProject
      };
    case CLEAR_SPRINTS_LIST:
      return {
        ...state,
        sprints: initialState.sprints
      };
    case SET_TYPE_GET_TASK:
      return {
        ...state,
        typeGetTask: (action.data && action.data.type) || initialState.typeGetTask
      };
    case SET_ACTIVE_TAB_BOARD:
      return {
        ...state,
        activeTabBoard: (action.data && action.data.type) || initialState.activeTabBoard
      };
    case SET_KEY_SEARCH_TASK:
      return {
        ...state,
        keySearchTask: (action.data && action.data.key) || initialState.keySearchTask
      };
    case ASSIGN_TASK_TO_USER_SUCCESS: {
      const indexLane = state.workflowProject.findIndex(w => Number(w.id) === action.data.WorkflowState.id);
      const indexCard = state.workflowProject[indexLane].cards.findIndex(c => Number(c.id) === action.data.id);

      return {
        ...state,
        status: action.type,
        currentTask: {
          ...state.currentTask,
          assignee: action.data && action.data.assignee,
          assigneeAs: action.data && action.data.assigneeAs
        },
        workflowProject: [
          ...state.workflowProject.slice(0, indexLane),
          {
            ...state.workflowProject[indexLane],
            cards: [
              ...state.workflowProject[indexLane].cards.slice(0, indexCard),
              {
                ...state.workflowProject[indexLane].cards[indexCard],
                assignee: action.data && action.data.assigneeAs
              },
              ...state.workflowProject[indexLane].cards.slice(indexCard + 1)
            ]
          },
          ...state.workflowProject.slice(indexLane + 1)
        ]
      };
    }
    case LIST_BEHAVIORS_SUCCESS:
      return {
        ...state,
        allBehaviorsInSprint: (action.data && action.data) || initialState.allBehaviorsInSprint
      };
    case LOG_BEHAVIOR_SPRINT_SUCCESS:
      return {
        ...state,
        status: action.status,
        allUsersBehaviorSprint: [...action.data, ...state.allUsersBehaviorSprint]
      };
    case DELETE_TASK_SUCCESS:
    case UPDATE_TASK_SUCCESS:
      return {
        ...state,
        status: action.type,
        currentTask: null
      };
    case GET_LIST_ALLOCATION_LABELS_SUCCESS:
      return {
        ...state,
        status: action.type,
        allocationLabels: action.allocationLabels
      };
    case GET_LIST_LOG_BEHAVIOR_IN_SPRINT_PROGRESS:
      return {
        ...state,
        status: action.type,
        allUsersBehaviorSprint: []
      };
    case GET_LIST_LOG_BEHAVIOR_IN_SPRINT_SUCCESS:
      return {
        ...state,
        status: action.type,
        allUsersBehaviorSprint: action.data || initialState.allUsersBehaviorSprint
      };
    case REMOVE_LOG_BEHAVIOR_IN_SPRINT_SUCCESS: {
      const updateItem = state.allUsersBehaviorSprint.filter(u => u.id !== action.data);
      return {
        ...state,
        status: action.type,
        allUsersBehaviorSprint: updateItem
      };
    }
    case LIST_USER_SKILLS_BY_SPRINT_SUCCESS: {
      const newListUserSkillSprint = (action.data && action.data.listUserSkillsSprint) || state.listUserSkillsSprint;
      return {
        ...state,
        listUserSkillsSprint: newListUserSkillSprint
      };
    }
    case ADD_USER_SKILL_IN_SPRINT_SUCCESS: {
      const skillIndex = state.listUserSkillsSprint.findIndex(
        s => s.skillId === (action.data && action.data.skillId) && s.userId === (action.data && action.data.userId)
      );
      const skillUpdated = state.listUserSkillsSprint.map(s => {
        if (s.skillId === action.data.skillId) {
          return {
            ...s,
            createdAt: action.data && action.data.createdAt,
            note: action.data && action.data.note
          };
        }
        return s;
      });
      if (skillIndex === -1) {
        return {
          ...state,
          listUserSkillsSprint: [action.data, ...state.listUserSkillsSprint]
        };
      }
      return {
        ...state,
        listUserSkillsSprint: skillUpdated
      };
    }
    case REMOVE_USER_SKILLS_IN_SPRINT_SUCCESS: {
      const removedUserSkill = state.listUserSkillsSprint.filter(s => s.id !== (action.data && action.data.id));
      return {
        ...state,
        listUserSkillsSprint: removedUserSkill
      };
    }

    case UPDATE_ALLOCATION_SUCCESS:
      return {
        ...state,
        status: action.type,
        sprint: {
          ...state.sprint,
          allocations: state.sprint.allocations.map(allocation => {
            if (allocation.id === action.allocation.id) {
              return action.allocation;
            }
            return allocation;
          })
        }
      };
    case UPDATE_ALLOCATION_FAILED:
      return {
        ...state,
        status: action.type
      };

    default:
      return state;
  }
};
