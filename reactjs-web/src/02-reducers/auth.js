/*
 *=================================================================================================
 * Auth Reducer
 *=================================================================================================
 */
import {
  LOGIN_PROGRESS,
  REDIRECT_PROGRESS,
  LOGIN_SUCCESS,
  LOGOUT_PROGRESS,
  LOGOUT_FAILED,
  REDIRECT_SUCCESS,
  LOGIN_FAILED,
  REDIRECT_FAILED,
  REGISTER_PROGRESS,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  LOGOUT_SUCCESS,
  GET_USER_INFO_SUCCESS,
  GET_MY_ROLE_AND_PERMISSION_PROGRESS,
  GET_MY_ROLE_AND_PERMISSION_SUCCESS,
  GET_MY_ROLE_AND_PERMISSION_FAILED,
  PRE_LOGIN_SUCCESS,
  PRE_LOGIN_FAILED
} from '../07-constants/auth';
import {
  getKey,
  // removeKey,
  setKey
} from '../08-helpers/storage';
import {
  // USER_ACCESS_TOKEN,
  // USER_ACCESS_TOKEN,
  USER_INFO,
  USER_LOGIN_TOKEN
} from '../07-constants/common';

const initialState = {
  loginToken: JSON.parse(getKey(USER_LOGIN_TOKEN)),
  // accessToken: JSON.parse(getKey(USER_ACCESS_TOKEN)),
  status: null,
  user: Object.assign(
    {
      username: null,
      profilePhoto: null
    },
    JSON.parse(getKey(USER_INFO))
  ),
  roles: null,
  permissions: null,
  authError: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_PROGRESS:
    case REDIRECT_PROGRESS:
    case REGISTER_PROGRESS:
    case REGISTER_SUCCESS:
    case REDIRECT_SUCCESS:
    case REDIRECT_FAILED:
    case LOGOUT_PROGRESS:
    case GET_MY_ROLE_AND_PERMISSION_PROGRESS:
    case LOGIN_SUCCESS:
      return {
        ...state,
        status: action.type
      };

    case PRE_LOGIN_SUCCESS:
      setKey(USER_LOGIN_TOKEN, JSON.stringify(action.data.tokenData));
      return {
        ...state,
        status: action.type,
        loginToken: action.data.tokenData
      };

    case GET_USER_INFO_SUCCESS:
      setKey(
        USER_INFO,
        JSON.stringify({
          username: action.data.user.name,
          profilePhoto: action.data.user.avatar
        })
      );
      return {
        ...state,
        status: action.type,
        user: {
          ...state.user,
          id: action.data && action.data.user && action.data.user.id,
          username: action.data && action.data.user && action.data.user.name,
          profilePhoto: action.data && action.data.user && action.data.user.avatar
        }
      };

    case PRE_LOGIN_FAILED:
    case LOGIN_FAILED:
    case REGISTER_FAILED:
    case GET_MY_ROLE_AND_PERMISSION_FAILED:
      return {
        ...state,
        status: action.type,
        authError: (action.data && action.data.error) || initialState.authError
      };
    case LOGOUT_FAILED:
    case LOGOUT_SUCCESS:
      // removeKey(USER_LOGIN_TOKEN);
      // removeKey(USER_ACCESS_TOKEN);
      // removeKey(USER_INFO);
      // // clear cache
      // removeKey('sprintsBoardCaches');
      // removeKey('sprintsIssueCaches');
      // removeKey('sprintsProjectCache');
      // removeKey('sprintsProjectCaches');
      // removeKey('sprintsUrlCache');
      return {
        ...state,
        status: action.type,
        loginToken: null,
        accessToken: null,
        roles: null
      };
    case GET_MY_ROLE_AND_PERMISSION_SUCCESS:
      return {
        ...state,
        status: action.type,
        roles: (action.data && action.data.user && action.data.user.roles) || initialState.roles,
        permissions: (action.data && action.data.user && action.data.user.permissions) || initialState.permissions
      };
    default:
      return state;
  }
};
