/*
 *=================================================================================================
 * Issue Reducer
 *=================================================================================================
 */
import {
  CREATE_ISSUE_FAILED,
  CREATE_ISSUE_PROGRESS,
  CREATE_ISSUE_SUCCESS,
  LIST_ISSUES_FAILED,
  LIST_ISSUES_PROGRESS,
  LIST_ISSUES_SUCCESS,
  UPDATE_ISSUE_FAILED,
  UPDATE_ISSUE_PROGRESS,
  UPDATE_ISSUE_SUCCESS,
  REMOVE_ISSUE_FAILED,
  REMOVE_ISSUE_PROGRESS,
  REMOVE_ISSUE_SUCCESS,
  GET_ISSUE_FAILED,
  GET_ISSUE_PROGRESS,
  GET_ISSUE_SUCCESS,
  STATISTICAL_ISSUES_OF_PROJECT_PROGRESS,
  STATISTICAL_ISSUES_OF_PROJECT_FAILED,
  STATISTICAL_ISSUES_OF_PROJECT_SUCCESS,
  FILTER_BY_SEARCH_SUBJECT,
  CREATE_MULTIPLE_ISSUE_PROGRESS,
  CREATE_MULTIPLE_ISSUE_SUCCESS,
  CREATE_MULTIPLE_ISSUE_FAILED,
  CLOSE_ALL_ISSUES_PROGRESS,
  CLOSE_ALL_ISSUES_SUCCESS,
  CLOSE_ALL_ISSUES_FAILED,
  GET_LIST_ATTACHMENT_OF_ISSUE_PROGRESS,
  GET_LIST_ATTACHMENT_OF_ISSUE_SUCCESS,
  GET_LIST_ATTACHMENT_OF_ISSUE_FAILED,
  UPLOAD_ATTACHMENT_TO_ISSUE_PROGRESS,
  UPLOAD_ATTACHMENT_TO_ISSUE_SUCCESS,
  UPLOAD_ATTACHMENT_TO_ISSUE_FAILED,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_PROGRESS,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_SUCCESS,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_FAILED,
  REMOVE_ATTACHMENT_OF_ISSUE_PROGRESS,
  REMOVE_ATTACHMENT_OF_ISSUE_SUCCESS,
  REMOVE_ATTACHMENT_OF_ISSUE_FAILED,
  CLEAR_ISSUE,
  CLEAR_CURRENT_ISSUE,
  FILTER_BY_STATUS,
  FILTER_BY_CREATOR,
  FILTER_BY_LIMIT,
  FILTER_BY_ISSUE_PAGE,
  CLEAR_FILTER_CREATOR,
  CLEAR_FILTER_LIMIT,
  CLEAR_FILTER_PAGE,
  CLEAR_FILTER_SEARCH,
  CLEAR_FILTER_STATUS
} from '../07-constants/issue';
import { ADD_COMMENT_ISSUE_SUCCESS, DELETE_COMMENT_ISSUE_SUCCESS } from '../07-constants/comment';
import { ISSUE_STATUS, PAGINATE_ATTACHMENT_LIMIT } from '../05-utils/commonData';
import { calculateLength } from '../08-helpers/common';

const initialState = {
  status: null,
  issues: {
    data: [],
    total: 0,
    limit: 10,
    pages: 0,
    page: 1
  },
  currentIssue: {},
  issueError: null,
  statisticalIssues: {
    open: 0,
    close: 0,
    solving: 0,
    all: 0
  },
  filter: {
    subject: '',
    status: ISSUE_STATUS.OPEN,
    creator: 0
  },
  activeTab: ISSUE_STATUS.OPEN,
  issueActionType: 0,
  attachmentsOfIssue: [],
  attachmentsOfIssueTotal: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_ISSUES_PROGRESS:
    case GET_ISSUE_PROGRESS:
    case REMOVE_ISSUE_PROGRESS:
    case CREATE_ISSUE_PROGRESS:
    case UPDATE_ISSUE_PROGRESS:
    case STATISTICAL_ISSUES_OF_PROJECT_PROGRESS:
    case CREATE_MULTIPLE_ISSUE_PROGRESS:
    case CLOSE_ALL_ISSUES_PROGRESS:
    case GET_LIST_ATTACHMENT_OF_ISSUE_PROGRESS:
    case UPLOAD_ATTACHMENT_TO_ISSUE_PROGRESS:
    case LOAD_MORE_ATTACHMENT_OF_ISSUE_PROGRESS:
    case REMOVE_ATTACHMENT_OF_ISSUE_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case LIST_ISSUES_FAILED:
    case GET_ISSUE_FAILED:
    case REMOVE_ISSUE_FAILED:
    case CREATE_ISSUE_FAILED:
    case UPDATE_ISSUE_FAILED:
    case STATISTICAL_ISSUES_OF_PROJECT_FAILED:
    case CREATE_MULTIPLE_ISSUE_FAILED:
    case CLOSE_ALL_ISSUES_FAILED:
    case GET_LIST_ATTACHMENT_OF_ISSUE_FAILED:
    case UPLOAD_ATTACHMENT_TO_ISSUE_FAILED:
    case LOAD_MORE_ATTACHMENT_OF_ISSUE_FAILED:
    case REMOVE_ATTACHMENT_OF_ISSUE_FAILED:
      return {
        ...state,
        status: action.type,
        issueError: (action.data && action.data.error) || initialState.issueError
      };
    case LIST_ISSUES_SUCCESS:
      return {
        ...state,
        status: action.type,
        issues: (action.data && action.data.issues) || initialState.issues
      };
    case GET_ISSUE_SUCCESS:
      return {
        ...state,
        status: action.type,
        currentIssue: (action.data && action.data.currentIssue) || initialState.currentIssue
      };
    case CREATE_ISSUE_SUCCESS: {
      const newIssue = action.data && action.data.createdIssue;
      if (newIssue) {
        return {
          ...state,
          status: action.type,
          statisticalIssues: {
            ...state.statisticalIssues,
            open: state.statisticalIssues.open + 1,
            all: state.statisticalIssues.all + 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_ISSUE_SUCCESS: {
      let issuesData = [...state.issues.data];
      const { updatedIssue, newStatus } = action.data;
      const issueId = updatedIssue && updatedIssue.id;
      const updateIndex = issuesData.findIndex(issue => issue.id === issueId);
      const oldUpdatedIssue = issuesData.find(issue => issue.id === issueId);

      if (updateIndex > -1) {
        issuesData[updateIndex] = updatedIssue;
      }
      if (newStatus && oldUpdatedIssue.status !== newStatus) {
        let { open, close, solving } = state.statisticalIssues;
        let total = state.issues.total;
        if (state.filter.status !== ISSUE_STATUS.ALL) {
          issuesData = issuesData.filter(issue => issue.id !== issueId);
          total = total - 1;
        }
        switch (oldUpdatedIssue.status) {
          case ISSUE_STATUS.OPEN:
            open -= 1;
            break;
          case ISSUE_STATUS.CLOSE:
            close -= 1;
            break;
          case ISSUE_STATUS.SOLVING:
            solving -= 1;
            break;
          default:
        }
        switch (newStatus) {
          case ISSUE_STATUS.OPEN:
            open += 1;
            break;
          case ISSUE_STATUS.CLOSE:
            close += 1;
            break;
          case ISSUE_STATUS.SOLVING:
            solving += 1;
            break;
          default:
        }
        return {
          ...state,
          status: action.type,
          issues: {
            ...state.issues,
            data: issuesData,
            total
          },
          statisticalIssues: {
            ...state.statisticalIssues,
            open,
            close,
            solving
          }
        };
      }
      return {
        ...state,
        status: action.type,
        issues: {
          ...state.issues,
          data: issuesData
        }
      };
    }
    case REMOVE_ISSUE_SUCCESS: {
      const removedIssue = action.data && action.data.removedIssue;
      const removeItem = removedIssue && state.issues.data.find(issue => issue.id === removedIssue.id);

      if (removeItem) {
        const newArray = state.issues.data.filter(issue => issue.id !== removedIssue.id);
        let { open, close, solving, all } = state.statisticalIssues;
        all -= 1;
        switch (removeItem.status) {
          case ISSUE_STATUS.OPEN:
            open -= 1;
            break;
          case ISSUE_STATUS.CLOSE:
            close -= 1;
            break;
          case ISSUE_STATUS.SOLVING:
            solving -= 1;
            break;
          default:
        }
        return {
          ...state,
          status: action.type,
          issues: {
            ...state.issues,
            data: [...newArray],
            total: state.issues.total - 1
          },
          statisticalIssues: {
            open,
            close,
            solving,
            all
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case ADD_COMMENT_ISSUE_SUCCESS: {
      const newComment = action.data && action.data.comment;
      const issues = state.issues;
      if (newComment && newComment.commentTableId && issues && Array.isArray(issues.data)) {
        const dataIssues = issues.data;
        const updateItem = dataIssues.find(issue => issue.id === newComment.commentTableId);
        updateItem.totalComments += 1;
        return {
          ...state,
          status: action.type,
          issues: {
            ...issues,
            data: dataIssues
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case DELETE_COMMENT_ISSUE_SUCCESS: {
      const newComment = action.data && action.data.comment;
      const issues = state.issues;
      if (newComment && newComment.commentTableId && issues && Array.isArray(issues.data)) {
        const dataIssues = issues.data;
        const updateItem = dataIssues.find(issue => issue.id === newComment.commentTableId);
        updateItem.totalComments -= 1;
        return {
          ...state,
          status: action.type,
          issues: {
            ...issues,
            data: dataIssues
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case STATISTICAL_ISSUES_OF_PROJECT_SUCCESS: {
      return {
        ...state,
        status: action.type,
        statisticalIssues: (action.data && action.data.result) || initialState.statisticalIssues
      };
    }
    case FILTER_BY_SEARCH_SUBJECT:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          subject: action.data.subject
        }
      };
    case FILTER_BY_STATUS:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          status: action.data.status
        }
      };
    case FILTER_BY_CREATOR:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          creator: action.data.creator
        }
      };
    case FILTER_BY_LIMIT:
      return {
        ...state,
        status: action.type,
        issues: {
          ...state.issues,
          limit: action.data.limit
        }
      };
    case FILTER_BY_ISSUE_PAGE:
      return {
        ...state,
        status: action.type,
        issues: {
          ...state.issues,
          page: action.data.page
        }
      };
    case CLEAR_FILTER_SEARCH:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          subject: initialState.filter.subject
        }
      };
    case CLEAR_FILTER_STATUS:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          status: initialState.filter.status
        }
      };
    case CLEAR_FILTER_CREATOR:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          creator: initialState.filter.creator
        }
      };
    case CLEAR_FILTER_LIMIT:
      return {
        ...state,
        status: action.type,
        issues: {
          ...state.issues,
          limit: initialState.issues.limit
        }
      };
    case CLEAR_FILTER_PAGE:
      return {
        ...state,
        status: action.type,
        issues: {
          ...state.issues,
          page: initialState.issues.page
        }
      };
    case CREATE_MULTIPLE_ISSUE_SUCCESS: {
      const newIssues = action.data && action.data.multipleIssues;
      if (newIssues && newIssues.length > 0) {
        return {
          ...state,
          status: action.type,
          statisticalIssues: {
            ...state.statisticalIssues,
            all: state.statisticalIssues.all + newIssues.length,
            open: state.statisticalIssues.open + newIssues.length
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case CLOSE_ALL_ISSUES_SUCCESS: {
      return {
        ...state,
        status: action.type
      };
    }
    case REMOVE_ATTACHMENT_OF_ISSUE_SUCCESS: {
      const newAttachment = action.data && action.data.attachmentOfIssue;
      if (newAttachment) {
        const attachmentsOfIssue = state.attachmentsOfIssue.filter(attachment => attachment.id !== newAttachment.id);
        return {
          ...state,
          status: action.type,
          attachmentsOfIssue,
          attachmentsOfIssueTotal: state.attachmentsOfIssueTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPLOAD_ATTACHMENT_TO_ISSUE_SUCCESS: {
      const newAttachments = action.data && action.data.attachmentsOfIssue;
      if (newAttachments) {
        const newAttachmentsLength = calculateLength(newAttachments);
        if (calculateLength(state.attachmentsOfIssue) + newAttachmentsLength < PAGINATE_ATTACHMENT_LIMIT) {
          return {
            ...state,
            status: action.type,
            attachmentsOfIssue: [...newAttachments, ...state.attachmentsOfIssue],
            attachmentsOfIssueTotal: state.attachmentsOfIssueTotal + newAttachmentsLength
          };
        } else {
          const attachmentsOfIssue = [...newAttachments, ...state.attachmentsOfIssue];
          return {
            ...state,
            status: action.type,
            attachmentsOfIssue: attachmentsOfIssue.slice(0, PAGINATE_ATTACHMENT_LIMIT),
            attachmentsOfIssueTotal: PAGINATE_ATTACHMENT_LIMIT
          };
        }
      }
      return {
        ...state,
        status: action.type
      };
    }
    case LOAD_MORE_ATTACHMENT_OF_ISSUE_SUCCESS: {
      const newAttachments = (action.data && action.data.attachmentsOfIssue) || initialState.attachmentsOfIssue;
      return {
        ...state,
        status: action.type,
        attachmentsOfIssue: [...state.attachmentsOfIssue, ...newAttachments],
        attachmentsOfIssueTotal: (action.data && action.data.total) || initialState.attachmentsOfIssueTotal
      };
    }
    case GET_LIST_ATTACHMENT_OF_ISSUE_SUCCESS:
      return {
        ...state,
        status: action.type,
        attachmentsOfIssue: (action.data && action.data.attachmentsOfIssue) || initialState.attachmentsOfIssue,
        attachmentsOfIssueTotal: (action.data && action.data.total) || initialState.attachmentsOfIssueTotal
      };
    case CLEAR_ISSUE:
      return {
        ...initialState,
        status: action.type
      };
    case CLEAR_CURRENT_ISSUE:
      return {
        ...state,
        status: action.type,
        currentIssue: {}
      };
    default:
      return state;
  }
};
