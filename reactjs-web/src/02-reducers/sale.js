/*
 *=================================================================================================
 * Sale Reducer
 *=================================================================================================
 */
import {
  LIST_SALE_CATEGORY_PROGRESS,
  LIST_SALE_CATEGORY_SUCCESS,
  LIST_SALE_CATEGORY_FAILED,
  REMOVE_SALE_CATEGORY_PROGRESS,
  REMOVE_SALE_CATEGORY_SUCCESS,
  REMOVE_SALE_CATEGORY_FAILED,
  CREATE_SALE_CATEGORY_PROGRESS,
  CREATE_SALE_CATEGORY_SUCCESS,
  CREATE_SALE_CATEGORY_FAILED,
  UPDATE_SALE_CATEGORY_PROGRESS,
  UPDATE_SALE_CATEGORY_SUCCESS,
  UPDATE_SALE_CATEGORY_FAILED,
  CLEAR_SALE_SETTING,
  LIST_SALE_STATUS_PROGRESS,
  LIST_SALE_STATUS_SUCCESS,
  LIST_SALE_STATUS_FAILED,
  REMOVE_SALE_STATUS_PROGRESS,
  REMOVE_SALE_STATUS_SUCCESS,
  REMOVE_SALE_STATUS_FAILED,
  CREATE_SALE_STATUS_PROGRESS,
  CREATE_SALE_STATUS_SUCCESS,
  CREATE_SALE_STATUS_FAILED,
  UPDATE_SALE_STATUS_PROGRESS,
  UPDATE_SALE_STATUS_SUCCESS,
  UPDATE_SALE_STATUS_FAILED,
  CREATE_SALE_PRIORITY_FAILED,
  CREATE_SALE_PRIORITY_PROGRESS,
  CREATE_SALE_PRIORITY_SUCCESS,
  UPDATE_SALE_PRIORITY_FAILED,
  UPDATE_SALE_PRIORITY_PROGRESS,
  UPDATE_SALE_PRIORITY_SUCCESS,
  GET_LIST_SALE_PRIORITY_FAILED,
  GET_LIST_SALE_PRIORITY_PROGRESS,
  GET_LIST_SALE_PRIORITY_SUCCESS,
  REMOVE_SALE_PRIORITY_FAILED,
  REMOVE_SALE_PRIORITY_PROGRESS,
  REMOVE_SALE_PRIORITY_SUCCESS,
  LIST_SALE_PROGRESS,
  LIST_SALE_SUCCESS,
  LIST_SALE_FAILED,
  REMOVE_SALE_PROGRESS,
  REMOVE_SALE_SUCCESS,
  REMOVE_SALE_FAILED,
  CREATE_SALE_PROGRESS,
  CREATE_SALE_SUCCESS,
  CREATE_SALE_FAILED,
  UPDATE_SALE_PROGRESS,
  UPDATE_SALE_SUCCESS,
  UPDATE_SALE_FAILED,
  ADD_ATTACHMENT_BY_SALE_PROGRESS,
  ADD_ATTACHMENT_BY_SALE_SUCCESS,
  ADD_ATTACHMENT_BY_SALE_FAILED,
  REMOVE_ATTACHMENT_BY_SALE_PROGRESS,
  REMOVE_ATTACHMENT_BY_SALE_SUCCESS,
  REMOVE_ATTACHMENT_BY_SALE_FAILED,
  LIST_ATTACHMENT_BY_SALE_PROGRESS,
  LIST_ATTACHMENT_BY_SALE_SUCCESS,
  LIST_ATTACHMENT_BY_SALE_FAILED,
  GET_SIGNED_URL_SALE_PROGRESS,
  GET_SIGNED_URL_SALE_SUCCESS,
  GET_SIGNED_URL_SALE_FAILED
} from '../07-constants/sale';

const initialState = {
  status: null,
  saleCategories: [],
  saleStatuses: [],
  salePriorities: [],
  sales: [],
  saleAttachments: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_SALE_CATEGORY_PROGRESS:
    case REMOVE_SALE_CATEGORY_PROGRESS:
    case CREATE_SALE_CATEGORY_PROGRESS:
    case UPDATE_SALE_CATEGORY_PROGRESS:
    case LIST_SALE_STATUS_PROGRESS:
    case REMOVE_SALE_STATUS_PROGRESS:
    case CREATE_SALE_STATUS_PROGRESS:
    case UPDATE_SALE_STATUS_PROGRESS:
    case GET_LIST_SALE_PRIORITY_PROGRESS:
    case CREATE_SALE_PRIORITY_PROGRESS:
    case UPDATE_SALE_PRIORITY_PROGRESS:
    case REMOVE_SALE_PRIORITY_PROGRESS:
    case LIST_SALE_PROGRESS:
    case REMOVE_SALE_PROGRESS:
    case CREATE_SALE_PROGRESS:
    case UPDATE_SALE_PROGRESS:
    case LIST_ATTACHMENT_BY_SALE_PROGRESS:
    case ADD_ATTACHMENT_BY_SALE_PROGRESS:
    case REMOVE_ATTACHMENT_BY_SALE_PROGRESS:
    case GET_SIGNED_URL_SALE_PROGRESS:
    case GET_SIGNED_URL_SALE_SUCCESS:
      return { ...state, status: action.type };
    case LIST_SALE_CATEGORY_FAILED:
    case REMOVE_SALE_CATEGORY_FAILED:
    case CREATE_SALE_CATEGORY_FAILED:
    case UPDATE_SALE_CATEGORY_FAILED:
    case LIST_SALE_STATUS_FAILED:
    case REMOVE_SALE_STATUS_FAILED:
    case CREATE_SALE_STATUS_FAILED:
    case UPDATE_SALE_STATUS_FAILED:
    case GET_LIST_SALE_PRIORITY_FAILED:
    case CREATE_SALE_PRIORITY_FAILED:
    case UPDATE_SALE_PRIORITY_FAILED:
    case REMOVE_SALE_PRIORITY_FAILED:
    case LIST_SALE_FAILED:
    case REMOVE_SALE_FAILED:
    case CREATE_SALE_FAILED:
    case UPDATE_SALE_FAILED:
    case ADD_ATTACHMENT_BY_SALE_FAILED:
    case REMOVE_ATTACHMENT_BY_SALE_FAILED:
    case LIST_ATTACHMENT_BY_SALE_FAILED:
    case GET_SIGNED_URL_SALE_FAILED:
      return {
        ...state,
        status: action.type,
        saleError: action.data && action.data.error
      };
    case LIST_SALE_CATEGORY_SUCCESS:
      return {
        ...state,
        status: action.type,
        saleCategories: action.data || initialState.saleCategories
      };
    case REMOVE_SALE_CATEGORY_SUCCESS: {
      const saleCategories = state.saleCategories.filter(item => item.id !== action.data.saleCategoryId);
      return {
        ...state,
        status: action.type,
        saleCategories
      };
    }
    case CREATE_SALE_CATEGORY_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          saleCategories: [...state.saleCategories, action.data]
        };
      }
      return { ...state, status: action.type };
    }
    case UPDATE_SALE_CATEGORY_SUCCESS: {
      if (action.data) {
        const saleCategories = [...state.saleCategories];
        const updateIndex = saleCategories.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          saleCategories[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            saleCategories
          };
        }
      }
      return { ...state, status: action.type };
    }
    case CLEAR_SALE_SETTING:
      return {
        ...state,
        status: action.type,
        saleCategories: initialState.saleCategories,
        saleStatuses: initialState.saleStatuses,
        salePriorities: initialState.salePriorities,
        sales: initialState.sales
      };
    case LIST_SALE_STATUS_SUCCESS:
      return {
        ...state,
        status: action.type,
        saleStatuses: action.data || initialState.saleStatuses
      };
    case REMOVE_SALE_STATUS_SUCCESS: {
      const saleStatuses = state.saleStatuses.filter(item => item.id !== action.data.saleStatusId);
      return {
        ...state,
        status: action.type,
        saleStatuses
      };
    }
    case CREATE_SALE_STATUS_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          saleStatuses: [...state.saleStatuses, action.data]
        };
      }
      return { ...state, status: action.type };
    }
    case UPDATE_SALE_STATUS_SUCCESS: {
      if (action.data) {
        const saleStatuses = [...state.saleStatuses];
        const updateIndex = saleStatuses.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          saleStatuses[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            saleStatuses
          };
        }
      }
      return { ...state, status: action.type };
    }
    case CREATE_SALE_PRIORITY_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          salePriorities: [...state.salePriorities, action.data]
        };
      }
      return { ...state, status: action.type };
    }
    case GET_LIST_SALE_PRIORITY_SUCCESS: {
      return {
        ...state,
        status: action.type,
        salePriorities: action.data || initialState.salePriorities
      };
    }
    case REMOVE_SALE_PRIORITY_SUCCESS: {
      const priortiyRemoved = state.salePriorities.filter(item => item.id !== action.data.salePriorityId);
      return {
        ...state,
        status: action.type,
        salePriorities: priortiyRemoved
      };
    }
    case UPDATE_SALE_PRIORITY_SUCCESS: {
      if (action.data) {
        const salePriorities = [...state.salePriorities];
        const updateIndex = salePriorities.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          salePriorities[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            salePriorities
          };
        }
      }
      return { ...state, status: action.type };
    }
    case LIST_SALE_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          sales: action.data.sales || initialState.sales,
          total: action.data.total || initialState.total
        };
      }
      return { ...state, status: action.type };
    }
    case REMOVE_SALE_SUCCESS: {
      if (action.data) {
        const sales = state.sales.filter(item => item.id !== action.data.saleId);
        return {
          ...state,
          status: action.type,
          sales,
          total: state.total - 1
        };
      }
      return { ...state, status: action.type };
    }
    case CREATE_SALE_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          sales: [...state.sales, action.data],
          total: state.total + 1
        };
      }
      return { ...state, status: action.type };
    }
    case UPDATE_SALE_SUCCESS: {
      if (action.data) {
        const sales = [...state.sales];
        const updateIndex = sales.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          sales[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            sales
          };
        }
      }
      return { ...state, status: action.type };
    }
    case LIST_ATTACHMENT_BY_SALE_SUCCESS: {
      return {
        ...state,
        status: action.type,
        saleAttachments: (action.data && action.data.rows) || initialState.saleAttachments,
        totalAttachments: action.data && action.data.count
      };
    }
    case REMOVE_ATTACHMENT_BY_SALE_SUCCESS: {
      if (action.data) {
        const saleAttachments = state.saleAttachments.filter(item => item.id !== action.data.id);
        return {
          ...state,
          status: action.type,
          saleAttachments,
          totalAttachments: state.totalAttachments - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case ADD_ATTACHMENT_BY_SALE_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.status,
          saleAttachments: [...state.saleAttachments, ...action.data],
          totalAttachments: state.totalAttachments + 1
        };
      }
      return {
        ...state,
        status: action.status
      };
    }
    default:
      return state;
  }
};
