/*
 *=================================================================================================
 * Admin Allocation Reducer
 *=================================================================================================
 */
import {
  GET_ALLOCATIONS_USERS,
  GET_ALLOCATIONS_USERS_SUCCESS,
  GET_ALLOCATIONS_USERS_FAILED
} from '../07-constants/admin-allocations';

const initialState = {
  type: null,
  error: null,
  allocations: [],
  skills: [],
  projects: []
};

const adminAllocations = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALLOCATIONS_USERS:
      return {
        ...state,
        type: action.type
      };
    case GET_ALLOCATIONS_USERS_SUCCESS:
      return {
        ...state,
        type: action.type,
        allocations: action.allocations,
        skills: action.skills,
        projects: action.projects
      };
    case GET_ALLOCATIONS_USERS_FAILED:
      return {
        ...state,
        type: action.type,
        error: action.error
      };

    default:
      return state;
  }
};

export default adminAllocations;
