/*
 *=================================================================================================
 * Common Reducer
 *=================================================================================================
 */
import React from 'react';
import i18n from '../i18n';
import Toast from '../03-components/Toast';
import { CHECK_ERROR } from '../07-constants/common';
import { toast } from 'react-toastify';
import { TOAST_ERROR_STYLE } from '../05-utils/commonData';
import { LIGHT_BACKGROUND_TOGGLE, FULL_BOARD_TOGGLE } from '../07-constants/common';
import { getStatusToggleLight, getStatusToggleFullBoard } from '../08-helpers/common';

const initialState = {
  status: null,
  error: null,
  toggleLight: getStatusToggleLight(),
  toggleFullBoard: getStatusToggleFullBoard()
};

export default (state = initialState, action) => {
  const env = process.env.REACT_APP_STAGE || 'LCL';

  switch (action.type) {
    case CHECK_ERROR: {
      const code =
        action.data && action.data.error && action.data.error.code
          ? i18n.t(action.data && action.data.error && action.data.error.code)
          : i18n.t('undefined_error');
      let extraAction = '';
      let message = '';
      if (action.data && action.data.error && action.data.error.extra && action.data.error.extra.action) {
        extraAction = action.data.error.extra && action.data.error.extra.action;
      }
      if (action.data && action.data.error && action.data.error.extra && action.data.error.extra.message)
        message = action.data.error.extra && action.data.error.extra.message;

      toast.error(<Toast code={code} env={env} extraAction={extraAction} message={message} />, TOAST_ERROR_STYLE);
      return {
        ...state,
        status: action.type,
        error: (action.data && action.data.error && action.data.error.code) || initialState.error
      };
    }
    case LIGHT_BACKGROUND_TOGGLE:
      return {
        ...state,
        status: action.type,
        toggleLight: (action.data && action.data.status) || initialState.toggleLight
      };
    case FULL_BOARD_TOGGLE:
      return {
        ...state,
        status: action.type,
        toggleFullBoard: (action.data && action.data.status) || initialState.toggleFullBoard
      };
    default:
      return state;
  }
};
