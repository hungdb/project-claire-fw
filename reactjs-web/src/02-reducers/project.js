/*
 *=================================================================================================
 * Project Reducer
 *=================================================================================================
 */
import {
  PROJECT_PROGRESS,
  PROJECT_GET_SUCCESS,
  PROJECT_FAILED,
  PROJECT_CREATE_PROGRESS,
  PROJECT_CREATE_SUCCESS,
  PROJECT_CREATE_FAILED,
  PROJECT_UPDATE_PROGRESS,
  PROJECT_UPDATE_SUCCESS,
  PROJECT_UPDATE_FAILED,
  PROJECT_REMOVE_PROGRESS,
  PROJECT_REMOVE_SUCCESS,
  PROJECT_REMOVE_FAILED,
  PROJECT_GET_EVN_PROGRESS,
  PROJECT_GET_EVN_SUCCESS,
  PROJECT_GET_EVN_FAILED,
  PROJECT_CREATE_EVN_PROGRESS,
  PROJECT_CREATE_EVN_SUCCESS,
  PROJECT_CREATE_EVN_FAILED,
  PROJECT_UPDATE_EVN_PROGRESS,
  PROJECT_UPDATE_EVN_SUCCESS,
  PROJECT_UPDATE_EVN_FAILED,
  PROJECT_REMOVE_EVN_PROGRESS,
  PROJECT_REMOVE_EVN_SUCCESS,
  PROJECT_REMOVE_EVN_FAILED,
  PROJECT_GET_LIST_ROLE_PROGRESS,
  PROJECT_GET_LIST_ROLE_SUCCESS,
  PROJECT_GET_HEATH_CHECK_PROGRESS,
  PROJECT_GET_HEATH_CHECK_SUCCESS,
  PROJECT_GET_HEATH_CHECK_FAILED,
  PROJECT_CREATE_CONFIG_PROGRESS,
  PROJECT_CREATE_CONFIG_SUCCESS,
  PROJECT_CREATE_CONFIG_FAILED,
  PROJECT_UPDATE_CONFIG_PROGRESS,
  PROJECT_UPDATE_CONFIG_SUCCESS,
  PROJECT_UPDATE_CONFIG_FAILED,
  PROJECT_REMOVE_CONFIG_PROGRESS,
  // PROJECT_REMOVE_CONFIG_SUCCESS,
  PROJECT_REMOVE_CONFIG_FAILED,
  GET_ALERT_SUBSCRIPTION_SUCCESS,
  UPDATE_ALERT_SUBSCRIPTION_SUCCESS,
  GET_ALERT_SUBSCRIPTION_FAILED,
  PROJECT_GET_ROLE_FAILED,
  PROJECT_GET_BY_USER_PROGRESS,
  PROJECT_GET_BY_USER_FAILED,
  PROJECT_GET_BY_USER_SUCCESS,
  // GET_HEALTH_CHECK_PROGRESS,
  GET_HEALTH_CHECK_SUCCESS,
  // GET_HEALTH_CHECK_FAILED,
  TOGGLE_HEALTH_CHECK_SERVER_DETAIL,
  CLEAR_HEALTH_CHECK_DATA,
  GET_USERS_OF_PROJECT_PROGRESS,
  GET_USERS_OF_PROJECT_SUCCESS,
  GET_USERS_OF_PROJECT_FAILED,
  SWITCH_PROJECT,
  UPDATE_PROJECT_ALERT_ENABLE_SUCCESS,
  GET_LOGS_OF_PROJECT_PROGRESS,
  GET_LOGS_OF_PROJECT_SUCCESS,
  GET_LOGS_OF_PROJECT_FAILED,
  CLEAR_LOG_HISTORY,
  CLEAR_SWITCH_PROJECT,
  CLEAR_SWITCH_GROUP_PROJECT,
  SET_SELECTED_SETTING_TAB,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_SUCCESS,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_FAILED,
  GET_DETAIL_PROJECT_PROGRESS,
  GET_DETAIL_PROJECT_SUCCESS,
  GET_DETAIL_PROJECT_FAILED,
  CLEAR_LIST_PROJECTS,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_PROGRESS,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_FAILED,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_SUCCESS,
  SET_ITEM_ALERT_SUBSCRIPTION_BY_USER
} from '../07-constants/project';
import { SWITCH_PROJECT_GROUP } from '../07-constants/group';

const initialState = {
  status: null,
  projectError: null,
  projects: [],
  selectedProject: null,
  selectedProjectEnvironments: [],
  listRoleProject: [],
  // listHeathCheckData: [],
  projectAlertSubscription: null,
  healthCheck: {
    loading: true
  },
  usersOfProject: [],
  logsOfProject: [],
  usersSubscriptionAlert: [],
  selectedSettingTab: 5,
  infoProject: null,
  itemAlertSubByUser: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_ALERT_SUBSCRIPTION_BY_USER_PROGRESS:
    case PROJECT_GET_BY_USER_PROGRESS:
    case PROJECT_PROGRESS:
    case PROJECT_CREATE_PROGRESS:
    case PROJECT_UPDATE_PROGRESS:
    case PROJECT_REMOVE_PROGRESS:
    case PROJECT_GET_EVN_PROGRESS:
    case PROJECT_CREATE_EVN_PROGRESS:
    case PROJECT_UPDATE_EVN_PROGRESS:
    case PROJECT_REMOVE_EVN_PROGRESS:
    case PROJECT_GET_LIST_ROLE_PROGRESS:
    case PROJECT_GET_HEATH_CHECK_PROGRESS:
    case PROJECT_CREATE_CONFIG_PROGRESS:
    case PROJECT_UPDATE_CONFIG_PROGRESS:
    case PROJECT_REMOVE_CONFIG_PROGRESS:
    case GET_USERS_OF_PROJECT_PROGRESS:
    case GET_LOGS_OF_PROJECT_PROGRESS:
    case GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT:
    case GET_DETAIL_PROJECT_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersSubscriptionAlert: action.data || initialState.usersSubscriptionAlert
      };
    case PROJECT_CREATE_SUCCESS:
      return {
        ...state,
        status: action.type,
        projects: state.projects.concat([action.data.project])
      };
    case PROJECT_UPDATE_SUCCESS: {
      const index = state.projects.findIndex(p => p.id === action.data.project.id);
      //-- check for selected project
      const newProject = {
        ...state.projects[index],
        name: action.data.project.name,
        isActive: action.data.project.isActive,
        groupId: action.data.project.groupId,
        projectRate: action.data.project.projectRate
      };
      return {
        ...state,
        status: action.type,
        selectedProject: state.selectedProject
          ? state.selectedProject.groupId === newProject.groupId
            ? state.selectedProject.id === newProject.id
              ? newProject
              : state.selectedProject
            : null
          : state.selectedProject,
        projects: [...state.projects.slice(0, index), newProject, ...state.projects.slice(index + 1)]
      };
    }
    case PROJECT_REMOVE_SUCCESS:
      return {
        ...state,
        status: action.type,
        selectedProject: null,
        projects: state.projects.filter(p => p.id !== action.data.removeProjectData.id)
      };
    case PROJECT_UPDATE_EVN_SUCCESS: {
      const index = state.selectedProjectEnvironments.findIndex(e => e.id === action.data.env.id);
      return {
        ...state,
        status: action.type,
        selectedProjectEnvironments: [
          ...state.selectedProjectEnvironments.slice(0, index),
          { ...state.selectedProjectEnvironments[index], ...action.data.env },
          ...state.selectedProjectEnvironments.slice(index + 1)
        ]
      };
    }
    case PROJECT_CREATE_EVN_SUCCESS:
      // case PROJECT_REMOVE_EVN_SUCCESS:
      return {
        ...state,
        status: action.type,
        selectedProjectEnvironments: [...state.selectedProjectEnvironments, action.data.environment]
      };
    case UPDATE_ALERT_SUBSCRIPTION_BY_USER_FAILED:
    case PROJECT_FAILED:
    case PROJECT_CREATE_FAILED:
    case PROJECT_GET_ROLE_FAILED:
    case PROJECT_UPDATE_FAILED:
    case PROJECT_REMOVE_FAILED:
    case PROJECT_GET_EVN_FAILED:
    case PROJECT_CREATE_EVN_FAILED:
    case PROJECT_UPDATE_EVN_FAILED:
    case PROJECT_REMOVE_EVN_FAILED:
    case PROJECT_GET_HEATH_CHECK_FAILED:
    case PROJECT_CREATE_CONFIG_FAILED:
    case PROJECT_UPDATE_CONFIG_FAILED:
    case PROJECT_REMOVE_CONFIG_FAILED:
    case GET_USERS_OF_PROJECT_FAILED:
    case GET_LOGS_OF_PROJECT_FAILED:
    case GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_FAILED:
    case GET_DETAIL_PROJECT_FAILED:
      return {
        ...state,
        status: action.type,
        projectError: (action.data && action.data.error) || initialState.projectError
      };
    case PROJECT_GET_SUCCESS:
      return {
        ...state,
        status: action.type,
        projects: action.data.projects,
        selectedProject: state.selectedProject
          ? action.data.projects.find(p => p.id === state.selectedProject.id)
          : null
      };
    case PROJECT_GET_EVN_SUCCESS:
      return {
        ...state,
        status: action.type,
        selectedProjectEnvironments: action.data.environments
      };
    case PROJECT_GET_LIST_ROLE_SUCCESS: {
      const changeRole = [];
      changeRole.push(action.data.listRoleProjects);
      return {
        ...state,
        status: action.type,
        listRoleProject: changeRole
      };
    }
    case PROJECT_REMOVE_EVN_SUCCESS:
    case PROJECT_GET_BY_USER_FAILED:
      return {
        ...state,
        status: action.type
      };
    case PROJECT_GET_BY_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        projects: action.data.listProject
      };
    case PROJECT_GET_HEATH_CHECK_SUCCESS: {
      const changeHeathCheck = [];
      changeHeathCheck.push(action.data.listHeathCheck);
      return {
        ...state,
        status: action.type,
        listHeathCheckData: changeHeathCheck
      };
    }
    case PROJECT_CREATE_CONFIG_SUCCESS:
      state.listHeathCheckData.map(listConfig => {
        listConfig.configs.push(action.data.createConfig);
        return listConfig;
      });
      return {
        ...state,
        status: action.type,
        listHeathCheckData: state.listHeathCheckData
      };
    case PROJECT_UPDATE_CONFIG_SUCCESS:
      state.listHeathCheckData.map(listConfig => {
        if (listConfig.id === action.data.updateConfig.envId) {
          const index = listConfig.configs.findIndex(id => id.id === action.data.updateConfig.configId);
          if (index > -1) {
            listConfig.configs[index].envId = action.data.updateConfig.envId;
            listConfig.configs[index].configId = action.data.updateConfig.configId;
            listConfig.configs[index].key = action.data.updateConfig.key;
            listConfig.configs[index].value = action.data.updateConfig.value;
          }
        }
        return listConfig;
      });
      break;
    // case PROJECT_GET_HEATH_CHECK_SUCCESS:
    //   let changeHeathCheck = [];
    //   changeHeathCheck.push(action.data.listHeathCheck);
    //   return {
    //     ...state,
    //     status: action.type,
    //     listHeathCheckData: changeHeathCheck
    //   };
    // case PROJECT_CREATE_CONFIG_SUCCESS:
    //   state.listHeathCheckData.map((listConfig) => {
    //     listConfig.configs.push(action.data.createConfig);
    //     return listConfig;
    //   });
    //   return {
    //     ...state,
    //     status: action.type,
    //     listHeathCheckData: state.listHeathCheckData,
    //   };
    // case PROJECT_UPDATE_CONFIG_SUCCESS:
    //   state.listHeathCheckData.map((listConfig) => {
    //     if (listConfig.id === action.data.updateConfig.envId) {
    //       let index = listConfig.configs.findIndex(id => id.id === action.data.updateConfig.configId);
    //       if (index > -1) {
    //         listConfig.configs[index].envId = action.data.updateConfig.envId;
    //         listConfig.configs[index].configId = action.data.updateConfig.configId;
    //         listConfig.configs[index].key = action.data.updateConfig.key;
    //         listConfig.configs[index].value = action.data.updateConfig.value;
    //       }
    //     }
    //     return listConfig;
    //   });
    //   return {
    //     ...state,
    //     status: action.type,
    //     listHeathCheckData: state.listHeathCheckData,
    //   };
    // case PROJECT_REMOVE_CONFIG_SUCCESS:
    //   state.listHeathCheckData.map((listConfig) => {
    //     let index = listConfig.configs.findIndex(id => id.id === action.data.removeConfig.configId);
    //     if (index > -1) {
    //       listConfig.configs.splice(index, 1);
    //     }
    //     return listConfig;
    //   });
    //   return {
    //     ...state,
    //     status: action.type,
    //     listHeathCheckData: state.listHeathCheckData,
    //   };
    case GET_ALERT_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        status: action.type,
        projectAlertSubscription: action.data.subscription
      };
    case GET_ALERT_SUBSCRIPTION_FAILED:
      return {
        ...state,
        status: action.type,
        projectAlertSubscription: null
      };
    case UPDATE_ALERT_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        status: action.type,
        projectAlertSubscription: action.data.subscription
      };
    case GET_HEALTH_CHECK_SUCCESS:
      return {
        ...state,
        status: action.type,
        healthCheck:
          state.healthCheck && !state.healthCheck.loading
            ? {
                ...state.healthCheck,
                ...action.data.healthCheck,
                loading: false,
                healthCheckData: {
                  ...state.healthCheck.healthCheckData,
                  ...(action.data.healthCheck ? action.data.healthCheck.healthCheckData : {}),
                  servers:
                    action.data.healthCheck && action.data.healthCheck.healthCheckData
                      ? action.data.healthCheck.healthCheckData.servers.map(server => {
                          const oldServer = state.healthCheck.healthCheckData.servers.find(
                            s => s.serverName === server.serverName
                          );
                          return { ...oldServer, ...server };
                        })
                      : []
                }
              }
            : action.data.healthCheck || { loading: false }
      };
    case TOGGLE_HEALTH_CHECK_SERVER_DETAIL: {
      const index = state.healthCheck.healthCheckData.servers.findIndex(
        server => server.serverName === action.data.serverId
      );
      return {
        ...state,
        status: action.type,
        healthCheck: {
          ...state.healthCheck,
          healthCheckData: {
            ...state.healthCheck.healthCheckData,
            servers: [
              ...state.healthCheck.healthCheckData.servers.slice(0, index),
              {
                ...state.healthCheck.healthCheckData.servers[index],
                isShowDetail: !state.healthCheck.healthCheckData.servers[index].isShowDetail
              },
              ...state.healthCheck.healthCheckData.servers.slice(index + 1)
            ]
          }
        }
      };
    }
    case CLEAR_HEALTH_CHECK_DATA:
      return {
        ...state,
        status: action.type,
        healthCheck: {
          loading: true
        }
      };
    case GET_USERS_OF_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        usersOfProject: (action.data && action.data.users) || initialState.usersOfProject
      };

    case SWITCH_PROJECT_GROUP: {
      return {
        ...state,
        selectedProject: null
      };
    }

    case SWITCH_PROJECT:
      return {
        ...state,
        status: action.type,
        selectedProject: action.data.selectedProject
      };

    case UPDATE_PROJECT_ALERT_ENABLE_SUCCESS: {
      //-- update projects and selectedProject
      const projectToReplaceIndex = state.projects.findIndex(p => p.id === action.data.projectId);
      const projectToReplace = {
        ...state.projects[projectToReplaceIndex],
        isNotificationEnable: action.data.isEnabled
      };
      return {
        ...state,
        selectedProject: projectToReplace,
        projects: [
          ...state.projects.slice(0, projectToReplaceIndex),
          projectToReplace,
          ...state.projects.slice(projectToReplaceIndex + 1)
        ]
      };
    }

    case GET_LOGS_OF_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        logsOfProject: (action.data && action.data.logs) || initialState.logsOfProject
      };

    case CLEAR_LOG_HISTORY:
      return {
        ...state,
        status: action.type,
        logsOfProject: initialState.logsOfProject
      };

    case CLEAR_SWITCH_PROJECT:
      return {
        ...state,
        status: action.type,
        usersOfProject: initialState.usersOfProject,
        logsOfProject: initialState.logsOfProject,
        selectedProjectEnvironments: initialState.selectedProjectEnvironments,
        projectAlertSubscription: initialState.projectAlertSubscription,
        healthCheck: initialState.healthCheck
      };

    case CLEAR_SWITCH_GROUP_PROJECT:
      return {
        ...state,
        status: action.type,
        usersOfProject: initialState.usersOfProject,
        logsOfProject: initialState.logsOfProject,
        selectedProjectEnvironments: initialState.selectedProjectEnvironments,
        projectAlertSubscription: initialState.projectAlertSubscription,
        healthCheck: initialState.healthCheck,
        selectedProject: initialState.selectedProject
      };

    case SET_SELECTED_SETTING_TAB:
      return {
        ...state,
        status: action.type,
        selectedSettingTab: (action.data && action.data.value) || initialState.selectedSettingTab
      };
    case GET_DETAIL_PROJECT_SUCCESS: {
      return {
        ...state,
        status: action.type,
        infoProject: action.data.detailProject
      };
    }
    case CLEAR_LIST_PROJECTS: {
      return {
        ...state,
        projects: initialState.projects
      };
    }
    case UPDATE_ALERT_SUBSCRIPTION_BY_USER_SUCCESS: {
      const indexUpdate = state.usersSubscriptionAlert.findIndex(a => a.id === action.data.subscription.id);
      const environmentIds = state.selectedProjectEnvironments.filter(e =>
        action.data.subscription.environmentIds.includes(e.id)
      );
      const data = {
        ...state.usersSubscriptionAlert[indexUpdate],
        environmentIds,
        methodIds: (action.data.subscription && action.data.subscription.methodIds) || []
      };
      if (indexUpdate !== -1) {
        return {
          ...state,
          usersSubscriptionAlert: [
            ...state.usersSubscriptionAlert.slice(0, indexUpdate),
            data,
            ...state.usersSubscriptionAlert.slice(indexUpdate + 1)
          ],
          itemAlertSubByUser: data,
          status: action.type
        };
      }
      return { ...state, status: action.type };
    }
    case SET_ITEM_ALERT_SUBSCRIPTION_BY_USER:
      return {
        ...state,
        status: action.type,
        itemAlertSubByUser: (action.data && action.data.item) || initialState.itemAlertSubByUser
      };
    default:
      return state;
  }
};
