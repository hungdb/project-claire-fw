/*
 *=================================================================================================
 * KPI Coefficient Reducer
 *=================================================================================================
 */
import {
  UPDATE_KPI_COEFFICIENT_PROGRESS,
  UPDATE_KPI_COEFFICIENT_SUCCESS,
  UPDATE_KPI_COEFFICIENT_FAILED,
  GET_KPI_COEFFICIENT_PROGRESS,
  GET_KPI_COEFFICIENT_SUCCESS,
  GET_KPI_COEFFICIENT_FAILED
} from '../07-constants/kpi-cofficient';

const initialState = {
  status: null,
  kpiCoefficientError: null,
  listKpiCoefficient: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_KPI_COEFFICIENT_PROGRESS:
    case GET_KPI_COEFFICIENT_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case UPDATE_KPI_COEFFICIENT_FAILED:
    case GET_KPI_COEFFICIENT_FAILED:
      return {
        ...state,
        status: action.type,
        kpiCoefficientError: action.data && action.data.error
      };
    case GET_KPI_COEFFICIENT_SUCCESS:
      return {
        ...state,
        listKpiCoefficient: action.data && action.data.listKpiCoefficient
      };
    case UPDATE_KPI_COEFFICIENT_SUCCESS: {
      let newArrayKpiCoefficientUpdate = state.listKpiCoefficient;
      if (action.data && action.data.data) {
        const indexUpdate = newArrayKpiCoefficientUpdate.findIndex(a => a.id === action.data.id);
        if (indexUpdate !== -1) {
          const itemUpdate = newArrayKpiCoefficientUpdate[indexUpdate];
          itemUpdate.data = action.data.data;
          newArrayKpiCoefficientUpdate = [
            ...newArrayKpiCoefficientUpdate.slice(0, indexUpdate),
            itemUpdate,
            ...newArrayKpiCoefficientUpdate.slice(indexUpdate + 1)
          ];
        }
      }
      return {
        ...state,
        listKpiCoefficient: newArrayKpiCoefficientUpdate
      };
    }
    default:
      return state;
  }
};
