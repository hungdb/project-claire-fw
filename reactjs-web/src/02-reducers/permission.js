/*
 *=================================================================================================
 * Permission Reducer
 *=================================================================================================
 */
import {
  GET_PERMISSION_PROGRESS,
  GET_PERMISSION_SUCCESS,
  GET_PERMISSION_FAILED,
  GET_ALL_PERMISSION_PROGRESS,
  GET_ALL_PERMISSION_SUCCESS,
  GET_ALL_PERMISSION_FAILED,
  ADD_ROLE_PERMISSION_PROGRESS,
  ADD_ROLE_PERMISSION_SUCCESS,
  ADD_ROLE_PERMISSION_FAILED,
  REMOVE_PERMISSION_SUCCESS,
  REMOVE_PERMISSION_FAILED,
  REMOVE_PERMISSION_PROGRESS,
  ADD_PERMISSION_PROGRESS,
  ADD_PERMISSION_SUCCESS,
  ADD_PERMISSION_FAILED,
  PROJECT_GET_ROLE_PROGRESS,
  PROJECT_GET_ROLE_SUCCESS,
  PROJECT_GET_ROLE_FAILED,
  CREATE_ROLE_PROGRESS,
  CREATE_ROLE_SUCCESS,
  CREATE_ROLE_FAILED,
  UPDATE_ROLE_PROGRESS,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAILED,
  REMOVE_ROLE_PROGRESS,
  REMOVE_ROLE_SUCCESS,
  REMOVE_ROLE_FAILED
} from '../07-constants/permission';

const initialState = {
  status: null,
  permissionError: null,
  listPerms: [],
  allPerms: [],
  listRoles: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PERMISSION_PROGRESS:
    case GET_ALL_PERMISSION_PROGRESS:
    case ADD_ROLE_PERMISSION_PROGRESS:
    case REMOVE_PERMISSION_PROGRESS:
    case ADD_PERMISSION_PROGRESS:
    case CREATE_ROLE_PROGRESS:
    case UPDATE_ROLE_PROGRESS:
    case REMOVE_ROLE_PROGRESS:
    case PROJECT_GET_ROLE_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_PERMISSION_FAILED:
    case GET_ALL_PERMISSION_FAILED:
    case ADD_ROLE_PERMISSION_FAILED:
    case REMOVE_PERMISSION_FAILED:
    case ADD_PERMISSION_FAILED:
    case CREATE_ROLE_FAILED:
    case UPDATE_ROLE_FAILED:
    case REMOVE_ROLE_FAILED:
    case PROJECT_GET_ROLE_FAILED:
      return {
        ...state,
        status: action.type,
        permissionError: (action.data && action.data.error) || initialState.permissionError
      };

    case ADD_ROLE_PERMISSION_SUCCESS:
      //-- add the new role permission to current role permissions
      return {
        ...state,
        status: action.type,
        listPerms: [...state.listPerms, action.data]
      };

    case PROJECT_GET_ROLE_SUCCESS:
      return {
        ...state,
        status: action.type,
        listRoles: (action.data && action.data.listRoles) || initialState.listRoles
      };
    case GET_PERMISSION_SUCCESS:
      return {
        ...state,
        status: action.type,
        listPerms: (action.data && action.data.listPerms && action.data.listPerms.permissions) || initialState.listPerms
      };
    case GET_ALL_PERMISSION_SUCCESS:
    case ADD_PERMISSION_SUCCESS:
      return {
        ...state,
        status: action.type,
        allPerms: (action.data && action.data.allPerms) || initialState.allPerms
      };
    case REMOVE_PERMISSION_SUCCESS:
      let newList = state.listPerms;
      let index = state.listPerms.findIndex(p => p.id === action.data.rolePermissionId);
      if (index >= 0) {
        newList = [...newList.slice(0, index), ...newList.slice(index + 1)];
      }
      return {
        ...state,
        status: action.type,
        listPerms: newList
      };
    case CREATE_ROLE_SUCCESS:
      return {
        ...state,
        status: action.type,
        listRoles: [...state.listRoles, action.data]
      };
    case UPDATE_ROLE_SUCCESS: {
      let newListRoles = state.listRoles;
      let index = state.listRoles.findIndex(r => r.id === action.data.id);
      if (index >= 0) {
        newListRoles = [
          ...state.listRoles.slice(0, index),
          Object.assign({}, state.listRoles[index], action.data),
          ...state.listRoles.slice(index + 1)
        ];
      }
      // const showAgainRole = state.listRoles.map((list)=> {
      //   if (list.id === action.data.updateRole.id) {
      //     return ({
      //       id: action.data.updateRole.id,
      //       name: action.data.updateRole.name,
      //       super_user: list.super_user || false,
      //       editable: list.editable || false,
      //     });
      //   }
      //   else {
      //     return {
      //       ...list,
      //       id: list.id,
      //       name: list.name,
      //       super_user: list.super_user || false,
      //       editable: list.editable || false,
      //     }
      //   }
      // });
      return {
        ...state,
        status: action.type,
        listRoles: newListRoles
      };
    }

    case REMOVE_ROLE_SUCCESS: {
      let newListRoles = state.listRoles;
      let index = state.listRoles.findIndex(r => r.id === action.data.id);
      if (index >= 0) {
        newListRoles = [...state.listRoles.slice(0, index), ...state.listRoles.slice(index + 1)];
      }
      return {
        ...state,
        status: action.type,
        listRoles: newListRoles
      };
    }
    default:
      return state;
  }
};
