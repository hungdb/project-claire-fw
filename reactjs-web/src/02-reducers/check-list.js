/*
 *=================================================================================================
 * Checklist Reducer
 *=================================================================================================
 */
import {
  GET_ALL_ITEMS_CHECK_PROGRESS,
  GET_ALL_ITEMS_CHECK_FAILED,
  GET_ALL_ITEMS_CHECK_SUCCESS,
  ADD_ITEM_CHECK_PROGRESS,
  ADD_ITEM_CHECK_SUCCESS,
  ADD_ITEM_CHECK_FAILED,
  UPDATE_ITEM_CHECK_PROGRESS,
  UPDATE_ITEM_CHECK_SUCCESS,
  UPDATE_ITEM_CHECK_FAILED,
  REMOVE_ITEM_CHECK_PROGRESS,
  REMOVE_ITEM_CHECK_SUCCESS,
  REMOVE_ITEM_CHECK_FAILED,
  CHECK_UNCHECK_ITEM_PROGRESS,
  CHECK_UNCHECK_ITEM_FAILED,
  CHECK_UNCHECK_ITEM_SUCCESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_PROGRESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_SUCCESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_FAILED,
  CLEAR_ITEMS_CHECKED,
  ADD_ITEM_CHECK_TO_PROJECT_PROGRESS,
  ADD_ITEM_CHECK_TO_PROJECT_SUCCESS,
  ADD_ITEM_CHECK_TO_PROJECT_FAILED,
  REMOVE_ITEM_CHECK_OF_PROJECT_PROGRESS,
  REMOVE_ITEM_CHECK_OF_PROJECT_SUCCESS,
  REMOVE_ITEM_CHECK_OF_PROJECT_FAILED
} from '../07-constants/check-list';

const initialState = {
  status: null,
  checkListError: null,
  listItemsCheck: [],
  listItemsCheckOfProject: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_ITEMS_CHECK_PROGRESS:
    case ADD_ITEM_CHECK_PROGRESS:
    case UPDATE_ITEM_CHECK_PROGRESS:
    case REMOVE_ITEM_CHECK_PROGRESS:
    case CHECK_UNCHECK_ITEM_PROGRESS:
    case GET_LIST_ITEM_CHECK_OF_PROJECT_PROGRESS:
    case ADD_ITEM_CHECK_TO_PROJECT_PROGRESS:
    case REMOVE_ITEM_CHECK_OF_PROJECT_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_ALL_ITEMS_CHECK_FAILED:
    case ADD_ITEM_CHECK_FAILED:
    case UPDATE_ITEM_CHECK_FAILED:
    case REMOVE_ITEM_CHECK_FAILED:
    case CHECK_UNCHECK_ITEM_FAILED:
    case GET_LIST_ITEM_CHECK_OF_PROJECT_FAILED:
    case ADD_ITEM_CHECK_TO_PROJECT_FAILED:
    case REMOVE_ITEM_CHECK_OF_PROJECT_FAILED:
      return {
        ...state,
        status: action.type,
        checkListError: (action.data && action.data.error) || initialState.checkListError
      };
    case GET_ALL_ITEMS_CHECK_SUCCESS:
      return {
        ...state,
        status: action.type,
        listItemsCheck: (action.data && action.data.listItemsCheck) || initialState.listItemsCheck
      };
    case ADD_ITEM_CHECK_SUCCESS:
      return {
        ...state,
        status: action.type,
        listItemsCheck: [...state.listItemsCheck, action.data.createItemCheckData]
      };
    case UPDATE_ITEM_CHECK_SUCCESS: {
      const newArray = state.listItemsCheck.map(item => {
        if (item.id === action.data.updateItemCheckData.id) {
          return {
            id: action.data.updateItemCheckData.id,
            name: action.data.updateItemCheckData.name,
            description: action.data.updateItemCheckData.description
          };
        } else {
          return {
            ...item,
            id: item.id,
            name: item.name,
            description: item.description
          };
        }
      });
      return {
        ...state,
        status: action.type,
        listItemsCheck: newArray
      };
    }
    case REMOVE_ITEM_CHECK_SUCCESS: {
      let newItems = [];
      newItems = state.listItemsCheck.filter(item => item.id !== action.data.removeItemCheckData.id);
      return {
        ...state,
        status: action.type,
        listItemsCheck: newItems
      };
    }
    case CHECK_UNCHECK_ITEM_SUCCESS: {
      const index = state.listItemsCheckOfProject.findIndex(
        c => c.projectId === action.data.data.projectId && c.checkItemId === action.data.data.checkItemId
      );
      return {
        ...state,
        status: action.type,
        listItemsCheckOfProject: [
          ...state.listItemsCheckOfProject.slice(0, index),
          {
            ...state.listItemsCheckOfProject[index],
            isChecked: action.data.data.isChecked
          },
          ...state.listItemsCheckOfProject.slice(index + 1)
        ]
      };
    }
    case GET_LIST_ITEM_CHECK_OF_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listItemsCheckOfProject:
          (action.data && action.data.listItemsCheckOfProject) || initialState.listItemsCheckOfProject
      };
    case CLEAR_ITEMS_CHECKED:
      return {
        ...state,
        status: action.type,
        listCheckedItemsOfProject: initialState.listCheckedItemsOfProject
      };
    case ADD_ITEM_CHECK_TO_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listItemsCheckOfProject: [...state.listItemsCheckOfProject, action.data.addItemCheckOfProjectData]
      };

    case REMOVE_ITEM_CHECK_OF_PROJECT_SUCCESS: {
      const newList = state.listItemsCheckOfProject.filter(
        item => item.id !== action.data.removeItemCheckOfProjectData.itemId
      );
      return {
        ...state,
        status: action.type,
        listItemsCheckOfProject: newList
      };
    }
    default:
      return state;
  }
};
