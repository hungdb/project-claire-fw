/*
 *=================================================================================================
 * Customer Reducer
 *=================================================================================================
 */
import {
  CREATE_CUSTOMER_PROGRESS,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_FAIL,
  UPDATE_CUSTOMER_PROGRESS,
  UPDATE_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_FAIL,
  REMOVE_CUSTOMER_PROGRESS,
  REMOVE_CUSTOMER_SUCCESS,
  REMOVE_CUSTOMER_FAIL,
  GET_LIST_CUSTOMER_PROGRESS,
  GET_LIST_CUSTOMER_SUCCESS,
  GET_LIST_CUSTOMER_FAIL,
  ADD_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  ADD_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  ADD_ATTACHMENT_BY_CUSTOMER_FAIL,
  REMOVE_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  REMOVE_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  REMOVE_ATTACHMENT_BY_CUSTOMER_FAIL,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_FAIL,
  GET_SIGNED_URL_CUSTOMER_PROGRESS,
  GET_SIGNED_URL_CUSTOMER_SUCCESS,
  GET_SIGNED_URL_CUSTOMER_FAIL,
  FORMAT_DATA_URL_CUSTOMER_PROGRESS,
  FORMAT_DATA_URL_CUSTOMER_SUCCESS,
  FORMAT_DATA_URL_CUSTOMER_FAIL,
  CLEAR_FORMAT_DATA_URL_CUSTOMER
} from '../07-constants/customer';

const initialState = {
  status: null,
  allCustomers: [],
  customerError: null,
  customerAttachments: [],
  totalCustomer: 0,
  attachmentFormated: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_CUSTOMER_PROGRESS:
    case UPDATE_CUSTOMER_PROGRESS:
    case REMOVE_CUSTOMER_PROGRESS:
    case GET_LIST_CUSTOMER_PROGRESS:
    case ADD_ATTACHMENT_BY_CUSTOMER_PROGRESS:
    case REMOVE_ATTACHMENT_BY_CUSTOMER_PROGRESS:
    case GET_LIST_ATTACHMENT_BY_CUSTOMER_PROGRESS:
    case GET_SIGNED_URL_CUSTOMER_PROGRESS:
    case FORMAT_DATA_URL_CUSTOMER_PROGRESS:
      return { ...state, status: action.type };
    case CREATE_CUSTOMER_FAIL:
    case UPDATE_CUSTOMER_FAIL:
    case REMOVE_CUSTOMER_FAIL:
    case GET_LIST_CUSTOMER_FAIL:
    case ADD_ATTACHMENT_BY_CUSTOMER_FAIL:
    case REMOVE_ATTACHMENT_BY_CUSTOMER_FAIL:
    case GET_LIST_ATTACHMENT_BY_CUSTOMER_FAIL:
    case GET_SIGNED_URL_CUSTOMER_FAIL:
    case FORMAT_DATA_URL_CUSTOMER_FAIL:
      return {
        ...state,
        status: action.type,
        customerError: action.data
      };
    case CREATE_CUSTOMER_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.status,
          allCustomers: [...state.allCustomers, action.data],
          totalCustomer: state.totalCustomer + 1
        };
      }
      return {
        ...state,
        status: action.status
      };
    }
    case UPDATE_CUSTOMER_SUCCESS: {
      if (action.data) {
        const allCustomers = [...state.allCustomers];
        const updateIndex = allCustomers.findIndex(c => c.id === action.data.id);
        if (updateIndex > -1) {
          allCustomers[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            allCustomers
          };
        }
      }
      return { ...state, status: action.type };
    }
    case REMOVE_CUSTOMER_SUCCESS: {
      if (action.data) {
        const allCustomers = state.allCustomers.filter(item => item.id !== action.data);
        return {
          ...state,
          status: action.type,
          allCustomers,
          totalCustomer: state.totalCustomer - 1
        };
      }
      return { ...state, status: action.type };
    }
    case GET_LIST_CUSTOMER_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          allCustomers: action.data.data || action.data,
          totalCustomer: action.data.total
        };
      }
      return {
        ...state,
        status: action.type,
        allCustomers: initialState.allCustomers
      };
    }
    case GET_LIST_ATTACHMENT_BY_CUSTOMER_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          customerAttachments: action.data
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case REMOVE_ATTACHMENT_BY_CUSTOMER_SUCCESS: {
      const customerAttachments = state.customerAttachments.filter(item => item.id !== action.data);
      if (action.data) {
        return {
          ...state,
          status: action.type,
          customerAttachments
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case ADD_ATTACHMENT_BY_CUSTOMER_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          customerAttachments: [...state.customerAttachments, ...action.data]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case GET_SIGNED_URL_CUSTOMER_SUCCESS: {
      return {
        ...state,
        status: action.type
      };
    }
    case FORMAT_DATA_URL_CUSTOMER_SUCCESS: {
      return {
        ...state,
        status: action.type,
        attachmentFormated: action.data
      };
    }
    case CLEAR_FORMAT_DATA_URL_CUSTOMER: {
      return {
        ...state,
        status: action.type,
        attachmentFormated: initialState.attachmentFormated
      };
    }
    default:
      return state;
  }
};
