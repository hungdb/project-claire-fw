/*
 *=================================================================================================
 * Role Permission Project Reducer
 *=================================================================================================
 */
import {
  GET_PERMISSION_PROJECT_PROGRESS,
  GET_PERMISSION_PROJECT_SUCCESS,
  GET_PERMISSION_PROJECT_FAILED,
  GET_ALL_PERMISSION_PROJECT_PROGRESS,
  GET_ALL_PERMISSION_PROJECT_SUCCESS,
  GET_ALL_PERMISSION_PROJECT_FAILED,
  ADD_ROLE_PERMISSION_PROJECT_PROGRESS,
  ADD_ROLE_PERMISSION_PROJECT_SUCCESS,
  ADD_ROLE_PERMISSION_PROJECT_FAILED,
  REMOVE_PERMISSION_PROJECT_SUCCESS,
  REMOVE_PERMISSION_PROJECT_FAILED,
  REMOVE_PERMISSION_PROJECT_PROGRESS,
  ADD_PERMISSION_PROJECT_PROGRESS,
  ADD_PERMISSION_PROJECT_SUCCESS,
  ADD_PERMISSION_PROJECT_FAILED,
  PROJECT_GET_ROLE_PROJECT_PROGRESS,
  PROJECT_GET_ROLE_PROJECT_SUCCESS,
  PROJECT_GET_ROLE_PROJECT_FAILED,
  CREATE_ROLE_PROJECT_PROGRESS,
  CREATE_ROLE_PROJECT_SUCCESS,
  CREATE_ROLE_PROJECT_FAILED,
  UPDATE_ROLE_PROJECT_PROGRESS,
  UPDATE_ROLE_PROJECT_SUCCESS,
  UPDATE_ROLE_PROJECT_FAILED,
  REMOVE_ROLE_PROJECT_PROGRESS,
  REMOVE_ROLE_PROJECT_SUCCESS,
  REMOVE_ROLE_PROJECT_FAILED
} from '../07-constants/rolePermissionProject';

const initialState = {
  status: null,
  permissionError: null,
  listPerms: [],
  allPerms: [],
  listRoles: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PERMISSION_PROJECT_PROGRESS:
    case GET_ALL_PERMISSION_PROJECT_PROGRESS:
    case ADD_ROLE_PERMISSION_PROJECT_PROGRESS:
    case ADD_ROLE_PERMISSION_PROJECT_SUCCESS:
    case REMOVE_PERMISSION_PROJECT_PROGRESS:
    case ADD_PERMISSION_PROJECT_PROGRESS:
    case CREATE_ROLE_PROJECT_PROGRESS:
    case UPDATE_ROLE_PROJECT_PROGRESS:
    case REMOVE_ROLE_PROJECT_PROGRESS:
    case PROJECT_GET_ROLE_PROJECT_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_PERMISSION_PROJECT_FAILED:
    case GET_ALL_PERMISSION_PROJECT_FAILED:
    case ADD_ROLE_PERMISSION_PROJECT_FAILED:
    case REMOVE_PERMISSION_PROJECT_FAILED:
    case ADD_PERMISSION_PROJECT_FAILED:
    case CREATE_ROLE_PROJECT_FAILED:
    case UPDATE_ROLE_PROJECT_FAILED:
    case REMOVE_ROLE_PROJECT_FAILED:
    case PROJECT_GET_ROLE_PROJECT_FAILED:
      return {
        ...state,
        status: action.type,
        permissionError: action.data && action.data.error
      };
    case PROJECT_GET_ROLE_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listRoles: (action.data && action.data.listRoles) || initialState.listRoles
      };
    case GET_PERMISSION_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listPerms: (action.data && action.data.listPerms && action.data.listPerms.permissions) || initialState.listPerms
      };
    case GET_ALL_PERMISSION_PROJECT_SUCCESS:
    case ADD_PERMISSION_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        allPerms: (action.data && action.data.allPerms) || initialState.allPerms
      };
    case REMOVE_PERMISSION_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type
      };
    case CREATE_ROLE_PROJECT_SUCCESS:
      return {
        ...state,
        status: action.type,
        listRoles: [...state.listRoles, action.data.createRoleData]
      };
    case UPDATE_ROLE_PROJECT_SUCCESS:
      const showAgainRole = state.listRoles.map(list => {
        if (list.id === action.data.updateRole.id) {
          return {
            id: action.data.updateRole.id,
            name: action.data.updateRole.name,
            super_user: list.super_user || false,
            editable: list.editable || false
          };
        } else {
          return {
            ...list,
            id: list.id,
            name: list.name,
            super_user: list.super_user || false,
            editable: list.editable || false
          };
        }
      });
      return {
        ...state,
        status: action.type,
        listRoles: showAgainRole
      };
    case REMOVE_ROLE_PROJECT_SUCCESS:
      let pushArray = [];
      pushArray = state.listRoles.filter(groupPJ => groupPJ.id !== action.data.removeRole.id);
      return {
        ...state,
        status: action.type,
        listRoles: pushArray
      };
    default:
      return state;
  }
};
