/*
 *=================================================================================================
 * Market Reducer
 *=================================================================================================
 */
import {
  GET_LIST_MARKET_PROGRESS,
  GET_LIST_MARKET_SUCCESS,
  GET_LIST_MARKET_FAIL,
  CREATE_MARKET_PROGRESS,
  CREATE_MARKET_SUCCESS,
  CREATE_MARKET_FAIL,
  UPDATE_MARKET_PROGRESS,
  UPDATE_MARKET_SUCCESS,
  UPDATE_MARKET_FAIL,
  REMOVE_MARKET_PROGRESS,
  REMOVE_MARKET_SUCCESS,
  REMOVE_MARKET_FAIL
} from '../07-constants/market';

const initialState = {
  allMarkets: [],
  status: null,
  marketError: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_MARKET_PROGRESS:
    case CREATE_MARKET_PROGRESS:
    case UPDATE_MARKET_PROGRESS:
    case REMOVE_MARKET_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_LIST_MARKET_FAIL:
    case CREATE_MARKET_FAIL:
    case UPDATE_MARKET_FAIL:
    case REMOVE_MARKET_FAIL:
      return {
        ...state,
        status: action.type,
        marketError: (action.data && action.data.error) || initialState.marketError
      };
    case GET_LIST_MARKET_SUCCESS: {
      return {
        ...state,
        status: action.type,
        allMarkets: action.data || initialState.allMarkets
      };
    }
    case CREATE_MARKET_SUCCESS: {
      if (action.data) {
        return {
          ...state,
          status: action.type,
          allMarkets: [...state.allMarkets, action.data]
        };
      } else {
        return {
          ...state,
          status: action.type,
          allMarkets: initialState.allMarkets
        };
      }
    }
    case REMOVE_MARKET_SUCCESS: {
      if (action.data) {
        const allMarkets = state.allMarkets.filter(item => item.id !== action.data);
        return {
          ...state,
          status: action.type,
          allMarkets
        };
      }
      return {
        ...state,
        status: action.type,
        allMarkets: initialState.allMarkets
      };
    }
    case UPDATE_MARKET_SUCCESS: {
      if (action.data) {
        const allMarkets = [...state.allMarkets];
        const updateIndex = allMarkets.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          allMarkets[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            allMarkets
          };
        }
      }
      return {
        ...state,
        status: action.type,
        allMarkets: initialState.allMarkets
      };
    }
    default:
      return state;
  }
};
