/*
 *=================================================================================================
 * Project Task View Reducer
 *=================================================================================================
 */
import {
  PTV_FILTER_QUERY_URL,
  PTV_FILTER_SPRINT,
  PTV_FILTER_PRIORITY,
  PTV_FILTER_ASSIGNEE,
  PTV_FILTER_STATUS,
  PTV_FILTER_START_DATE,
  PTV_FILTER_SEARCH,
  PTV_FILTER_ROWS_PER_PAGE,
  PTV_FILTER_PAGE,
  PTV_USERS_IN_PROJECT_GET,
  PTV_USERS_IN_PROJECT_GET_SUCCESS,
  PTV_USERS_IN_PROJECT_GET_FAILED,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET_SUCCESS,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET_FAILED,
  PTV_PROGRESS_BAR_GET,
  PTV_PROGRESS_BAR_GET_SUCCESS,
  PTV_PROGRESS_BAR_GET_FAILED,
  PTV_TASKS_IN_PROJECT_GET,
  PTV_TASKS_IN_PROJECT_GET_SUCCESS,
  PTV_TASKS_IN_PROJECT_GET_FAILED,
  PTV_FILTER_VIEW_TYPE,
  PTV_CLEAR_FILTER_ASSIGNEE,
  PTV_CLEAR_FILTER_PRIORITY,
  PTV_CLEAR_FILTER_SEARCH,
  PTV_CLEAR_FILTER_SPRINT,
  PTV_CLEAR_FILTER_STATUS,
  PTV_CLEAR_FILTER_START_DATE,
  PTV_CLEAR_ALL_FILTER
} from '../07-constants/project-task-view';
import { UPDATE_TASK_SUCCESS } from '../07-constants/sprint';
import {
  PRIORITY_OBJECT,
  PRIORITY_OPTIONS,
  TASK_TYPE_OBJECT,
  TASK_TYPE_OPTIONS,
  ROWS_PER_PAGE_TASK_OPTIONS
} from '../05-utils/commonData';
import { parseJSON } from '../08-helpers/common';
import { cleanUndefinedProperties } from '../05-utils/commonUtils';
import { NOT_CLOSED_STATUS_TASK } from '../05-utils/commonData';

const initialState = {
  type: null,
  error: null,
  progressBar: {
    untouched: 0,
    progress: 0,
    holding: 0,
    terminated: 0
  },
  filter: {
    sprint: null,
    priority: null,
    assignee: null,
    status: null,
    startDate: null,
    search: '',
    rowsPerPage: ROWS_PER_PAGE_TASK_OPTIONS[0],
    page: 1,
    viewType: 1
  },
  optionsUser: [],
  optionsStatus: [],
  tasks: [],
  dependenTasks: [],
  totalTask: 0
};

const taskModifier = task => {
  return {
    id: task.id,
    text: `#${task.taskCode ? task.taskCode : task.id} ${task.content}`,
    assignee: task.assigneeAs && task.assigneeAs.name ? task.assigneeAs.name : null,
    sprint: task.Sprint && task.Sprint.name,
    priority:
      task.priority === PRIORITY_OBJECT.HIGH
        ? PRIORITY_OPTIONS[0].label
        : task.priority === PRIORITY_OBJECT.MEDIUM
        ? PRIORITY_OPTIONS[1].label
        : PRIORITY_OPTIONS[2].label,
    status: task.WorkflowState && task.WorkflowState.effortType,
    tag: parseJSON(task.tag),
    create_date: task.createdAt,
    start_date: task.startDate ? new Date(task.startDate) : null,
    due_date: task.dueDate ? new Date(task.dueDate) : null,
    end_date: task.endDate ? new Date(task.endDate) : null,
    estimate: task.estimate,
    progress: task.WorkflowState && task.WorkflowState.id === 4 ? 1 : 0,
    taskCode: task.taskCode,
    typeCode: task.type,
    typeText:
      task.type === TASK_TYPE_OBJECT.BUG
        ? TASK_TYPE_OPTIONS[TASK_TYPE_OBJECT.BUG - 1].label
        : TASK_TYPE_OPTIONS[TASK_TYPE_OBJECT.NORMAL - 1].label,
    dependencies: task.dependencies || []
  };
};

export default (state = initialState, action) => {
  switch (action.type) {
    // PLAIN
    case PTV_FILTER_QUERY_URL:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          sprint: action.sprint,
          priority: action.priority,
          assignee: action.assignee,
          status: action.status,
          startDate: action.startDate,
          search: action.search,
          rowsPerPage: action.limit,
          page: action.page,
          viewType: action.viewType
        }
      };

    case UPDATE_TASK_SUCCESS:
      const index = state.tasks.findIndex(t => t.id === action.data.info.id);
      const newTask = Object.assign({}, state.tasks[index], cleanUndefinedProperties(taskModifier(action.data.info)));
      return {
        ...state,
        status: action.type,
        tasks: [...state.tasks.slice(0, index), newTask, ...state.tasks.slice(index + 1)]
        // currentTask: (action.data && action.data.info) || initialState.currentTask
      };

    case PTV_FILTER_VIEW_TYPE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          viewType: action.viewType
        }
      };

    case PTV_CLEAR_ALL_FILTER:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          priority: action.priority,
          sprint: initialState.filter.sprint,
          assignee: initialState.filter.assignee,
          status: initialState.filter.status,
          startDate: initialState.filter.startDate,
          search: initialState.filter.search
        }
      };

    case PTV_FILTER_SPRINT:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          sprint: action.sprint
        }
      };

    case PTV_CLEAR_FILTER_SPRINT:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          sprint: initialState.filter.sprint
        }
      };

    case PTV_FILTER_PRIORITY:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          priority: action.priority
        }
      };

    case PTV_CLEAR_FILTER_PRIORITY:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          priority: initialState.filter.priority
        }
      };

    case PTV_FILTER_ASSIGNEE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          assignee: action.assignee
        }
      };

    case PTV_CLEAR_FILTER_ASSIGNEE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          assignee: initialState.filter.assignee
        }
      };

    case PTV_FILTER_STATUS:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          status: action.status
        }
      };

    case PTV_CLEAR_FILTER_STATUS:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          status: initialState.filter.status
        }
      };

    case PTV_FILTER_START_DATE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          startDate: action.startDate
        }
      };

    case PTV_CLEAR_FILTER_START_DATE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          startDate: initialState.filter.startDate
        }
      };

    case PTV_FILTER_SEARCH:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          search: action.search
        }
      };

    case PTV_CLEAR_FILTER_SEARCH:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          search: initialState.filter.search
        }
      };

    case PTV_FILTER_ROWS_PER_PAGE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          rowsPerPage: action.rowsPerPage
        }
      };

    case PTV_FILTER_PAGE:
      return {
        ...state,
        type: action.type,
        filter: {
          ...state.filter,
          page: action.page
        }
      };

    // REQUEST
    case PTV_PROGRESS_BAR_GET:
    case PTV_USERS_IN_PROJECT_GET:
    case PTV_WORKFLOW_STATE_IN_PROJECT_GET:
    case PTV_TASKS_IN_PROJECT_GET:
      return {
        ...state,
        type: action.type
      };

    // FAILED
    case PTV_PROGRESS_BAR_GET_FAILED:
    case PTV_USERS_IN_PROJECT_GET_FAILED:
    case PTV_WORKFLOW_STATE_IN_PROJECT_GET_FAILED:
    case PTV_TASKS_IN_PROJECT_GET_FAILED:
      return {
        ...state,
        type: action.type,
        error: action.error
      };

    // SUCCESS
    case PTV_USERS_IN_PROJECT_GET_SUCCESS:
      return {
        ...state,
        type: action.type,
        // optionsUser: R.equals(state.optionsUser, action.optionsUser) ? state.optionsUser : action.optionsUser,
        optionsUser: action.optionsUser
      };

    case PTV_WORKFLOW_STATE_IN_PROJECT_GET_SUCCESS:
      return {
        ...state,
        type: action.type,
        // optionsStatus: R.equals(state.optionsStatus, action.optionsStatus) ? state.optionsStatus : action.optionsStatus,
        optionsStatus: Array.isArray(action.optionsStatus)
          ? [NOT_CLOSED_STATUS_TASK, ...action.optionsStatus]
          : initialState.optionsStatus
      };

    case PTV_PROGRESS_BAR_GET_SUCCESS:
      return {
        ...state,
        type: action.type,
        // progressBar: R.equals(state.progressBar, action.progressBar) ? state.progressBar : action.progressBar,
        progressBar: action.progressBar
      };

    case PTV_TASKS_IN_PROJECT_GET_SUCCESS:
      const modifiedTasks = action.data.tasks.map(t => taskModifier(t));
      const dependenTasks = [];
      modifiedTasks.forEach(e => {
        e.dependencies &&
          e.dependencies.forEach(dependency =>
            dependenTasks.push({
              source: dependency.parentId,
              target: dependency.taskId,
              type: '0'
            })
          );
      });

      return {
        ...state,
        type: action.type,
        tasks: modifiedTasks,
        dependenTasks: dependenTasks,
        totalTask: action.data.totalTask
      };

    // DEFAULT
    default:
      return state;
  }
};
