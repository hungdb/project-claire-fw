/*
 *=================================================================================================
 * Task Reducer
 *=================================================================================================
 */
import {
  TASKOWN_PROJECTS,
  TASKOWN_PROJECTS_SUCCESS,
  TASKOWN_PROJECTS_FAILED,
  TASKOWN_TASKS,
  TASKOWN_TASKS_SUCCESS,
  TASKOWN_TASKS_FAILED,
  GET_TASKS_OF_USERS_PROGRESS,
  GET_TASKS_OF_USERS_SUCCESS,
  GET_TASKS_OF_USERS_FAILED
} from '../07-constants/task';

const initState = {
  status: null,
  error: null,
  projects: [],
  tasks: [],
  taskOfUsers: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case TASKOWN_PROJECTS:
    case TASKOWN_TASKS:
    case GET_TASKS_OF_USERS_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case TASKOWN_PROJECTS_FAILED:
    case TASKOWN_TASKS_FAILED:
    case GET_TASKS_OF_USERS_FAILED:
      return {
        ...state,
        status: action.type,
        error: (action.data && action.data.error) || initState.error
      };
    case TASKOWN_PROJECTS_SUCCESS:
      return {
        ...state,
        status: action.type,
        projects: action.data
      };
    case TASKOWN_TASKS_SUCCESS:
      return {
        ...state,
        status: action.type,
        tasks: action.data
      };
    case GET_TASKS_OF_USERS_SUCCESS:
      return {
        ...state,
        status: action.type,
        taskOfUsers: (action.data && action.data.taskOfUsers) || initState.taskOfUsers
      };
    default:
      return state;
  }
};
