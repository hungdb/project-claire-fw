/*
 *=================================================================================================
 * Behaviour Reducer
 *=================================================================================================
 */
import {
  GET_LIST_OF_BEHAVIOURS_INPROGRESS,
  GET_LIST_OF_BEHAVIOURS_SUCCESS,
  GET_LIST_OF_BEHAVIOURS_FAILED,
  CREATE_BEHAVIOURS_INPROGRESS,
  CREATE_BEHAVIOURS_SUCCESS,
  CREATE_BEHAVIOURS_FAILED,
  UPDATE_BEHAVIOURS_INPROGRESS,
  UPDATE_BEHAVIOURS_SUCCESS,
  UPDATE_BEHAVIOURS_FAILED,
  DELETE_BEHAVIOURS_INPROGRESS,
  DELETE_BEHAVIOURS_SUCCESS,
  DELETE_BEHAVIOURS_FAILED,
  GET_LIST_KPI_BEHAVIOUR_INPROGRESS,
  GET_LIST_KPI_BEHAVIOUR_SUCCESS,
  GET_LIST_KPI_BEHAVIOUR_FAILED,
  CREATE_KPI_BEHAVIOUR_INPROGRESS,
  CREATE_KPI_BEHAVIOUR_SUCCESS,
  CREATE_KPI_BEHAVIOUR_FAILED,
  REMOVE_KPI_BEHAVIOUR_INPROGRESS,
  REMOVE_KPI_BEHAVIOUR_SUCCESS,
  REMOVE_KPI_BEHAVIOUR_FAILED,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_INPROGRESS,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_SUCCESS,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_FAILED,
  CLEAR_BEHAVIOURS,
  CLEAR_KPI_BEHAVIOUR
} from '../07-constants/behaviours';

const initialState = {
  listKpiBehaviour: [],
  listBehaviour: [],
  status: null,
  behaviourError: null,
  kpiBehaviorId: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_OF_BEHAVIOURS_INPROGRESS:
    case CREATE_BEHAVIOURS_INPROGRESS:
    case DELETE_BEHAVIOURS_INPROGRESS:
    case UPDATE_BEHAVIOURS_INPROGRESS:
    case GET_LIST_KPI_BEHAVIOUR_INPROGRESS:
    case CREATE_KPI_BEHAVIOUR_INPROGRESS:
    case REMOVE_KPI_BEHAVIOUR_INPROGRESS:
    case CREATE_MULTIPLE_KPI_BEHAVIOUR_INPROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_LIST_OF_BEHAVIOURS_FAILED:
    case CREATE_BEHAVIOURS_FAILED:
    case UPDATE_BEHAVIOURS_FAILED:
    case DELETE_BEHAVIOURS_FAILED:
    case GET_LIST_KPI_BEHAVIOUR_FAILED:
    case CREATE_KPI_BEHAVIOUR_FAILED:
    case REMOVE_KPI_BEHAVIOUR_FAILED:
    case CREATE_MULTIPLE_KPI_BEHAVIOUR_FAILED:
      return {
        ...state,
        status: action.type,
        behaviourError: action.data && action.data.error
      };
    case GET_LIST_OF_BEHAVIOURS_SUCCESS: {
      return {
        ...state,
        listBehaviour: action.data && action.data.listBehaviour,
        status: action.type
      };
    }
    case CREATE_BEHAVIOURS_SUCCESS: {
      const newListBehaviour = [...state.listBehaviour, action.data.behaviour];
      return {
        ...state,
        listBehaviour: newListBehaviour,
        status: action.type
      };
    }
    case UPDATE_BEHAVIOURS_SUCCESS: {
      const listBehaviour = state.listBehaviour;
      const indexUpdate = listBehaviour.findIndex(a => a.id === action.data.behaviourUpdate.id);
      if (indexUpdate !== -1) {
        return {
          ...state,
          listBehaviour: [
            ...state.listBehaviour.slice(0, indexUpdate),
            action.data.behaviourUpdate,
            ...state.listBehaviour.slice(indexUpdate + 1)
          ],
          status: action.type
        };
      }
      return { ...state, status: action.type };
    }
    case DELETE_BEHAVIOURS_SUCCESS: {
      const listBehaviour = state.listBehaviour;
      const indexRemove = listBehaviour.findIndex(a => a.id === action.data.behaviourRemove);
      if (indexRemove !== -1) {
        return {
          ...state,
          listBehaviour: [...state.listBehaviour.slice(0, indexRemove), ...state.listBehaviour.slice(indexRemove + 1)],
          status: action.type
        };
      }
      return { ...state, status: action.type };
    }
    case GET_LIST_KPI_BEHAVIOUR_SUCCESS:
      return {
        ...state,
        listKpiBehaviour: action.data && action.data.listKpiBehaviour.data,
        status: action.type
      };
    case CREATE_KPI_BEHAVIOUR_SUCCESS:
      if (action.data && action.data.kpiBehaviour) {
        const newListKpiBehaviour = [...state.listKpiBehaviour, action.data.kpiBehaviour];
        return {
          ...state,
          listKpiBehaviour: newListKpiBehaviour,
          status: action.type
        };
      }
      return { ...state, status: action.type };
    case REMOVE_KPI_BEHAVIOUR_SUCCESS: {
      const listKpiBehaviour = state.listKpiBehaviour.filter(item => item.id !== action.data.kpiBehaviorId);
      return {
        ...state,
        listKpiBehaviour,
        kpiBehaviorId: action.data.kpiBehaviorId,
        status: action.type
      };
    }
    case CREATE_MULTIPLE_KPI_BEHAVIOUR_SUCCESS:
      if (action.data && action.data.multipleKpiBehaviours) {
        const newListKpiBehaviour = [...state.listKpiBehaviour, ...action.data.multipleKpiBehaviours];
        return {
          ...state,
          listKpiBehaviour: newListKpiBehaviour,
          status: action.type
        };
      }
      return { ...state, status: action.type };
    case CLEAR_BEHAVIOURS:
      return {
        ...state,
        listBehaviour: initialState.listBehaviour,
        status: action.type
      };
    case CLEAR_KPI_BEHAVIOUR:
      return {
        ...state,
        listKpiBehaviour: initialState.listKpiBehaviour,
        kpiBehaviorId: initialState.kpiBehaviorId,
        status: action.type
      };
    default:
      return state;
  }
};
