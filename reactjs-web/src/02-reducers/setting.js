/*
 *=================================================================================================
 * Setting Reducer
 *=================================================================================================
 */
import {
  ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS,
  ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED,
  ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS,
  GET_STATUS_OF_ALERT_SYSTEM_FAILED,
  GET_STATUS_OF_ALERT_SYSTEM_SUCCESS,
  GET_STATUS_OF_ALERT_SYSTEM_PROGRESS
} from '../07-constants/setting';

const initialState = {
  status: null,
  isActive: null,
  settingError: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS:
    case GET_STATUS_OF_ALERT_SYSTEM_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED:
    case GET_STATUS_OF_ALERT_SYSTEM_FAILED:
      return {
        ...state,
        status: action.type,
        settingError: action.data && action.data.error
      };
    case ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS:
      return {
        ...state,
        status: action.type,
        isActive: (action.data && action.data.state) || initialState.isActive
      };
    case GET_STATUS_OF_ALERT_SYSTEM_SUCCESS:
      return {
        ...state,
        status: action.type,
        isActive: (action.data && action.data.state) || initialState.isActive
      };
    default:
      return state;
  }
};
