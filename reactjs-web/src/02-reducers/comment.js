/*
 *=================================================================================================
 * Comment Reducer
 *=================================================================================================
 */
import {
  GET_COMMENTS_OF_TASK_PROGRESS,
  GET_COMMENTS_OF_TASK_SUCCESS,
  GET_COMMENTS_OF_TASK_FAILED,
  GET_COMMENTS_OF_USER_FAILED,
  GET_COMMENTS_OF_USER_PROGRESS,
  GET_COMMENTS_OF_USER_SUCCESS,
  UPDATE_COMMENT_FAILED,
  UPDATE_COMMENT_PROGRESS,
  UPDATE_COMMENT_SUCCESS,
  ADD_COMMENT_FAILED,
  ADD_COMMENT_PROGRESS,
  ADD_COMMENT_SUCCESS,
  DELETE_COMMENT_FAILED,
  DELETE_COMMENT_PROGRESS,
  DELETE_COMMENT_SUCCESS,
  LOAD_MORE_COMMENTS_OF_TASK_FAILED,
  LOAD_MORE_COMMENTS_OF_TASK_PROGRESS,
  LOAD_MORE_COMMENTS_OF_TASK_SUCCESS,
  FIRE_SOCKET_ADD_COMMENT,
  FIRE_SOCKET_DELETE_COMMENT,
  FIRE_SOCKET_UPDATE_COMMENT,
  GET_COMMENTS_OF_ISSUE_PROGRESS,
  GET_COMMENTS_OF_ISSUE_SUCCESS,
  GET_COMMENTS_OF_ISSUE_FAILED,
  ADD_COMMENT_ISSUE_FAILED,
  ADD_COMMENT_ISSUE_PROGRESS,
  ADD_COMMENT_ISSUE_SUCCESS,
  UPDATE_COMMENT_ISSUE_FAILED,
  UPDATE_COMMENT_ISSUE_PROGRESS,
  UPDATE_COMMENT_ISSUE_SUCCESS,
  DELETE_COMMENT_ISSUE_FAILED,
  DELETE_COMMENT_ISSUE_PROGRESS,
  DELETE_COMMENT_ISSUE_SUCCESS,
  LOAD_MORE_COMMENTS_OF_ISSUE_FAILED,
  LOAD_MORE_COMMENTS_OF_ISSUE_SUCCESS,
  LOAD_MORE_COMMENTS_OF_ISSUE_PROGRESS,
  FIRE_SOCKET_ADD_COMMENT_ISSUE,
  FIRE_SOCKET_DELETE_COMMENT_ISSUE,
  FIRE_SOCKET_UPDATE_COMMENT_ISSUE
} from '../07-constants/comment';

const initialState = {
  status: null,
  commentError: null,
  commentsOfTask: [],
  commentsOfTaskTotal: 0,
  commentsOfIssue: [],
  commentsOfIssueTotal: 0,
  commentsOfUser: [],
  commentsOfUserTotal: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_COMMENTS_OF_TASK_PROGRESS:
    case GET_COMMENTS_OF_USER_PROGRESS:
    case ADD_COMMENT_PROGRESS:
    case UPDATE_COMMENT_PROGRESS:
    case DELETE_COMMENT_PROGRESS:
    case GET_COMMENTS_OF_ISSUE_PROGRESS:
    case ADD_COMMENT_ISSUE_PROGRESS:
    case UPDATE_COMMENT_ISSUE_PROGRESS:
    case DELETE_COMMENT_ISSUE_PROGRESS:
    case LOAD_MORE_COMMENTS_OF_TASK_PROGRESS:
    case LOAD_MORE_COMMENTS_OF_ISSUE_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case GET_COMMENTS_OF_TASK_FAILED:
    case GET_COMMENTS_OF_USER_FAILED:
    case ADD_COMMENT_FAILED:
    case UPDATE_COMMENT_FAILED:
    case DELETE_COMMENT_FAILED:
    case GET_COMMENTS_OF_ISSUE_FAILED:
    case ADD_COMMENT_ISSUE_FAILED:
    case UPDATE_COMMENT_ISSUE_FAILED:
    case DELETE_COMMENT_ISSUE_FAILED:
    case LOAD_MORE_COMMENTS_OF_TASK_FAILED:
    case LOAD_MORE_COMMENTS_OF_ISSUE_FAILED:
      return {
        ...state,
        status: action.type,
        commentError: action.data && action.data.error
      };
    case GET_COMMENTS_OF_TASK_SUCCESS:
      return {
        ...state,
        status: action.type,
        commentsOfTask:
          (action.data && action.data.commentsOfTask && action.data.commentsOfTask.reverse()) ||
          initialState.commentsOfTask,
        commentsOfTaskTotal: (action.data && action.data.commentsOfTaskTotal) || initialState.commentsOfTaskTotal
      };
    case GET_COMMENTS_OF_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        commentsOfUser: (action.data && action.data.commentsOfUser) || initialState.commentsOfUser,
        commentsOfUserTotal: (action.data && action.data.commentsOfUserTotal) || initialState.commentsOfUserTotal
      };
    case ADD_COMMENT_SUCCESS: {
      const newComment = action.data && action.data.comment;
      if (newComment) {
        return {
          ...state,
          status: action.type,
          commentsOfTask: [...state.commentsOfTask, newComment],
          commentsOfTaskTotal: state.commentsOfTaskTotal + 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_COMMENT_SUCCESS: {
      const updatedComment = action.data && action.data.comment;
      if (updatedComment) {
        const index = state.commentsOfTask.findIndex(comment => comment.id === updatedComment.id);
        if (index > -1) {
          state.commentsOfTask[index] = updatedComment;
        }
        return {
          ...state,
          status: action.type,
          commentsOfTask: [...state.commentsOfTask]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case DELETE_COMMENT_SUCCESS: {
      const removeComment = action.data && action.data.comment;
      if (removeComment) {
        const comments = state.commentsOfTask.filter(comment => comment.id !== removeComment.id);
        return {
          ...state,
          status: action.type,
          commentsOfTask: comments,
          commentsOfTaskTotal: state.commentsOfTaskTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case LOAD_MORE_COMMENTS_OF_TASK_SUCCESS: {
      const newComment =
        (action.data && action.data.commentsOfTask && action.data.commentsOfTask.reverse()) ||
        initialState.commentsOfTask;
      return {
        ...state,
        status: action.type,
        commentsOfTask: [...newComment, ...state.commentsOfTask],
        commentsOfTaskTotal: (action.data && action.data.commentsOfTaskTotal) || initialState.commentsOfTaskTotal
      };
    }
    case FIRE_SOCKET_ADD_COMMENT: {
      const newComment = action.data && action.data.newComment;
      const data = {
        ...newComment,
        Task: newComment.task,
        User: newComment.actor
      };
      if (newComment) {
        return {
          ...state,
          status: action.type,
          commentsOfTask: [...state.commentsOfTask, data],
          commentsOfTaskTotal: state.commentsOfTaskTotal + 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case FIRE_SOCKET_UPDATE_COMMENT: {
      const updatedComment = action.data && action.data.updatedComment;
      const data = {
        ...updatedComment,
        Task: updatedComment.task,
        User: updatedComment.actor
      };
      if (updatedComment) {
        const index = state.commentsOfTask.findIndex(comment => comment.id === updatedComment.id);
        if (index > -1) {
          state.commentsOfTask[index] = data;
        }
        return {
          ...state,
          status: action.type,
          commentsOfTask: [...state.commentsOfTask]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case FIRE_SOCKET_DELETE_COMMENT: {
      const removeComment = action.data && action.data.deletedComment;
      if (removeComment) {
        const comments = state.commentsOfTask.filter(comment => comment.id !== removeComment.id);
        return {
          ...state,
          status: action.type,
          commentsOfTask: comments,
          commentsOfTaskTotal: state.commentsOfTaskTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case GET_COMMENTS_OF_ISSUE_SUCCESS:
      return {
        ...state,
        status: action.type,
        commentsOfIssue:
          (action.data && action.data.commentsOfIssue && action.data.commentsOfIssue.reverse()) ||
          initialState.commentsOfIssue,
        commentsOfIssueTotal: (action.data && action.data.commentsOfIssueTotal) || initialState.commentsOfIssueTotal
      };
    case ADD_COMMENT_ISSUE_SUCCESS: {
      const newComment = action.data && action.data.comment;
      if (newComment) {
        return {
          ...state,
          status: action.type,
          commentsOfIssue: [...state.commentsOfIssue, newComment],
          commentsOfIssueTotal: state.commentsOfIssueTotal + 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_COMMENT_ISSUE_SUCCESS: {
      const updatedComment = action.data && action.data.comment;
      if (updatedComment) {
        const index = state.commentsOfIssue.findIndex(comment => comment.id === updatedComment.id);
        if (index > -1) {
          state.commentsOfIssue[index] = updatedComment;
        }
        return {
          ...state,
          status: action.type,
          commentsOfIssue: [...state.commentsOfIssue]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case DELETE_COMMENT_ISSUE_SUCCESS: {
      const removeComment = action.data && action.data.comment;
      if (removeComment) {
        const comments = state.commentsOfIssue.filter(comment => comment.id !== removeComment.id);
        return {
          ...state,
          status: action.type,
          commentsOfIssue: comments,
          commentsOfIssueTotal: state.commentsOfIssueTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case LOAD_MORE_COMMENTS_OF_ISSUE_SUCCESS: {
      const newComment =
        (action.data && action.data.commentsOfIssue && action.data.commentsOfIssue.reverse()) ||
        initialState.commentsOfIssue;
      return {
        ...state,
        status: action.type,
        commentsOfIssue: [...newComment, ...state.commentsOfIssue],
        commentsOfIssueTotal: (action.data && action.data.commentsOfIssueTotal) || initialState.commentsOfIssueTotal
      };
    }
    case FIRE_SOCKET_ADD_COMMENT_ISSUE: {
      const newComment = action.data && action.data.newComment;
      const data = {
        ...newComment,
        Issue: newComment.issue,
        User: newComment.actor
      };
      if (newComment) {
        return {
          ...state,
          status: action.type,
          commentsOfIssue: [...state.commentsOfIssue, data],
          commentsOfIssueTotal: state.commentsOfIssueTotal + 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case FIRE_SOCKET_UPDATE_COMMENT_ISSUE: {
      const updatedComment = action.data && action.data.updatedComment;
      const data = {
        ...updatedComment,
        Issue: updatedComment.issue,
        User: updatedComment.actor
      };
      if (updatedComment) {
        const index = state.commentsOfIssue.findIndex(comment => comment.id === updatedComment.id);
        if (index > -1) {
          state.commentsOfIssue[index] = data;
        }
        return {
          ...state,
          status: action.type,
          commentsOfIssue: [...state.commentsOfIssue]
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case FIRE_SOCKET_DELETE_COMMENT_ISSUE: {
      const removeComment = action.data && action.data.deletedComment;
      if (removeComment) {
        const comments = state.commentsOfIssue.filter(comment => comment.id !== removeComment.id);
        return {
          ...state,
          status: action.type,
          commentsOfIssue: comments,
          commentsOfIssueTotal: state.commentsOfIssueTotal - 1
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    default:
      return state;
  }
};
