/*
 *=================================================================================================
 * Skill Reducer
 *=================================================================================================
 */
import {
  LIST_SKILL_PROGRESS,
  LIST_SKILL_SUCCESS,
  LIST_SKILL_FAILED,
  GET_SKILL_PROGRESS,
  GET_SKILL_SUCCESS,
  GET_SKILL_FAILED,
  REMOVE_SKILL_PROGRESS,
  REMOVE_SKILL_SUCCESS,
  REMOVE_SKILL_FAILED,
  CREATE_SKILL_PROGRESS,
  CREATE_SKILL_SUCCESS,
  CREATE_SKILL_FAILED,
  UPDATE_SKILL_PROGRESS,
  UPDATE_SKILL_SUCCESS,
  UPDATE_SKILL_FAILED,
  LIST_SKILL_BY_USER_PROGRESS,
  LIST_SKILL_BY_USER_SUCCESS,
  LIST_SKILL_BY_USER_FAILED,
  LIST_USER_BY_SKILL_PROGRESS,
  LIST_USER_BY_SKILL_SUCCESS,
  LIST_USER_BY_SKILL_FAILED,
  GET_USER_SKILL_PROGRESS,
  GET_USER_SKILL_SUCCESS,
  GET_USER_SKILL_FAILED,
  REMOVE_USER_SKILL_PROGRESS,
  REMOVE_USER_SKILL_SUCCESS,
  REMOVE_USER_SKILL_FAILED,
  CREATE_USER_SKILL_PROGRESS,
  CREATE_USER_SKILL_SUCCESS,
  CREATE_USER_SKILL_FAILED,
  UPDATE_USER_SKILL_PROGRESS,
  UPDATE_USER_SKILL_SUCCESS,
  UPDATE_USER_SKILL_FAILED,
  HANDLE_CHANGE_PAGE,
  HANDLE_CHANGE_ROWS_PER_PAGE,
  LIST_ALL_SKILL_SUCCESS,
  LIST_ALL_SKILL_PROGRESS,
  LIST_ALL_SKILL_FAILED,
  CLEAR_USER_SKILL,
  GET_SKILLS_OF_ALL_USERS_PROGRESS,
  GET_SKILLS_OF_ALL_USERS_SUCCESS,
  GET_SKILLS_OF_ALL_USERS_FAILED,
  CREATE_KPI_SKILL_SUCCESS,
  CREATE_KPI_SKILL_PROGRESS,
  CREATE_KPI_SKILL_FAILED,
  GET_LIST_KPI_SKILL_OF_USER_PROGRESS,
  GET_LIST_KPI_SKILL_OF_USER_SUCCESS,
  GET_LIST_KPI_SKILL_OF_USER_FAILED,
  REMOVE_KPI_SKILL_PROGRESS,
  REMOVE_KPI_SKILL_SUCCESS,
  REMOVE_KPI_SKILL_FAILED,
  LIST_MY_USER_SKILLS_PROGRESS,
  LIST_MY_USER_SKILLS_FAILED,
  LIST_MY_USER_SKILLS_SUCCESS,
  FILTER_BY_USER_ID_KPI,
  FILTER_BY_USER_ID_SKILL
} from '../07-constants/skill';
import { getRankOfSkill } from '../08-helpers/common';

const initialState = {
  status: null,
  skills: {
    data: [],
    total: 0,
    limit: 10,
    pages: 0,
    page: 1
  },
  currentSkill: {},
  userSkills: {
    data: []
    // total: 0,
    // limit: 10,
    // pages: 0,
    // page: 1
  },
  skillError: null,
  allSkills: [],
  allSkillByUser: [],
  skillsKPIOfUser: [],
  logMySkill: {
    data: [],
    total: 0,
    limit: 10,
    pages: 0,
    page: 1
  },
  filter: {
    userIdKPI: null,
    userIdSkill: null
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_SKILL_PROGRESS:
    case GET_SKILL_PROGRESS:
    case REMOVE_SKILL_PROGRESS:
    case CREATE_SKILL_PROGRESS:
    case UPDATE_SKILL_PROGRESS:
    case LIST_SKILL_BY_USER_PROGRESS:
    case LIST_USER_BY_SKILL_PROGRESS:
    case GET_USER_SKILL_PROGRESS:
    case REMOVE_USER_SKILL_PROGRESS:
    case CREATE_USER_SKILL_PROGRESS:
    case UPDATE_USER_SKILL_PROGRESS:
    case LIST_ALL_SKILL_PROGRESS:
    case GET_SKILLS_OF_ALL_USERS_PROGRESS:
    case CREATE_KPI_SKILL_PROGRESS:
    case GET_LIST_KPI_SKILL_OF_USER_PROGRESS:
    case REMOVE_KPI_SKILL_PROGRESS:
    case LIST_MY_USER_SKILLS_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case LIST_SKILL_FAILED:
    case GET_SKILL_FAILED:
    case REMOVE_SKILL_FAILED:
    case CREATE_SKILL_FAILED:
    case UPDATE_SKILL_FAILED:
    case LIST_SKILL_BY_USER_FAILED:
    case LIST_USER_BY_SKILL_FAILED:
    case GET_USER_SKILL_FAILED:
    case REMOVE_USER_SKILL_FAILED:
    case CREATE_USER_SKILL_FAILED:
    case UPDATE_USER_SKILL_FAILED:
    case LIST_ALL_SKILL_FAILED:
    case GET_SKILLS_OF_ALL_USERS_FAILED:
    case CREATE_KPI_SKILL_FAILED:
    case GET_LIST_KPI_SKILL_OF_USER_FAILED:
    case REMOVE_KPI_SKILL_FAILED:
    case LIST_MY_USER_SKILLS_FAILED:
      return {
        ...state,
        status: action.type,
        skillError: action.data && action.data.error
      };
    case GET_SKILL_SUCCESS:
      return {
        ...state,
        status: action.type,
        currentSkill: action.data.currentSkill
      };
    case LIST_SKILL_SUCCESS:
      return {
        ...state,
        status: action.type,
        skills: (action.data && action.data.skills) || initialState.skills
      };
    case HANDLE_CHANGE_ROWS_PER_PAGE:
      return {
        ...state,
        type: action.type,
        skills: {
          ...state.skills,
          limit: action.rowsPerPage
        }
      };
    case HANDLE_CHANGE_PAGE:
      return {
        ...state,
        type: action.type,
        skills: {
          ...state.skills,
          page: action.page
        }
      };
    case REMOVE_SKILL_SUCCESS: {
      const removedSkill = action.data && action.data.removedSkill;
      if (removedSkill) {
        const newArray = state.skills.data.filter(skill => skill.id !== removedSkill.id);
        return {
          ...state,
          status: action.type,
          skills: {
            ...state.skills,
            data: [...newArray],
            total: state.skills.total - 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case CREATE_SKILL_SUCCESS: {
      const newSkill = action.data && action.data.createdSkill;
      if (newSkill) {
        return {
          ...state,
          status: action.type,
          skills: {
            ...state.skills,
            data: [newSkill, ...state.skills.data],
            total: state.skills.total + 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_SKILL_SUCCESS: {
      const updateIndex = state.skills.data.findIndex(s => s.id === action.data.updatedSkill.id);
      if (updateIndex > -1) {
        state.skills.data[updateIndex] = action.data.updatedSkill;
      }
      return {
        ...state,
        status: action.type,
        skills: { ...state.skills }
      };
    }
    case GET_USER_SKILL_SUCCESS:
      return {
        ...state,
        status: action.type,
        userskills: action.data.userskills
      };
    case LIST_SKILL_BY_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        userSkills: {
          ...state.userSkills,
          data: (action.data && action.data.skillsByUser) || initialState.userSkills.data
        }
      };
    case LIST_USER_BY_SKILL_SUCCESS:
      return {
        ...state,
        status: action.type,
        userBySkill: (action.data && action.data.userBySkill) || initialState.userBySkill
      };
    case REMOVE_USER_SKILL_SUCCESS: {
      const removedUserSkill = action.data && action.data.removedUserSkill;
      if (removedUserSkill) {
        const newArray = state.userSkills.data.filter(item => item.id !== removedUserSkill.id);
        return {
          ...state,
          status: action.type,
          userSkills: {
            ...state.userSkills,
            data: [...newArray],
            total: state.userSkills.total - 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case CREATE_USER_SKILL_SUCCESS: {
      const newUserSkill = action.data && action.data.userSkill;
      if (newUserSkill) {
        return {
          ...state,
          status: action.type,
          userSkills: {
            ...state.userSkills,
            data: [newUserSkill, ...state.userSkills.data],
            total: state.userSkills.total + 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case UPDATE_USER_SKILL_SUCCESS: {
      const userSkills = { ...state.userSkills };
      const updateIndex = userSkills.data.findIndex(s => s.id === action.data.updateUserSkill.id);
      if (updateIndex > -1) {
        userSkills.data[updateIndex] = action.data.updateUserSkill;
      }
      return {
        ...state,
        status: action.type,
        userSkills
      };
    }
    case LIST_ALL_SKILL_SUCCESS:
      return {
        ...state,
        status: action.type,
        allSkills: (action.data && action.data.skills.data) || initialState.allSkills
      };
    case CLEAR_USER_SKILL:
      return {
        ...state,
        status: action.type,
        userSkills: initialState.userSkills,
        allSkills: initialState.allSkills,
        filter: initialState.filter,
        allSkillByUser: initialState.allSkillByUser,
        skillsKPIOfUser: initialState.skillsKPIOfUser,
        logMySkill: initialState.logMySkill,
        skills: initialState.skills,
        currentSkill: initialState.currentSkill
      };
    case GET_SKILLS_OF_ALL_USERS_SUCCESS: {
      const allSkillByUser = action.data.map(user => {
        const userSkills = user.userSkills.map(item => {
          const rank = getRankOfSkill(item.evaluationPoint, item.skill && item.skill.name);
          return {
            ...item,
            rank
          };
        });
        return {
          ...user,
          userSkills
        };
      });
      return {
        ...state,
        status: action.type,
        allSkillByUser: allSkillByUser || initialState.allSkillByUser
      };
    }
    case GET_LIST_KPI_SKILL_OF_USER_SUCCESS:
      return {
        ...state,
        status: action.type,
        skillsKPIOfUser: action.data && action.data
      };
    case CREATE_KPI_SKILL_SUCCESS: {
      const newKPISkill = action.data && action.data.data;
      if (newKPISkill) {
        return {
          ...state,
          status: action.type,
          skillsKPIOfUser: {
            ...state.skillsKPIOfUser,
            data: [newKPISkill, ...state.skillsKPIOfUser.data],
            total: state.skillsKPIOfUser.total + 1
          }
        };
      }
      return {
        ...state,
        status: action.type
      };
    }
    case REMOVE_KPI_SKILL_SUCCESS: {
      return {
        ...state,
        status: action.type,
        skillsKPIOfUser: {
          ...state.skillsKPIOfUser,
          data: [...state.skillsKPIOfUser.data.filter(item => item.id !== action.data)],
          total: state.skillsKPIOfUser.total - 1
        }
      };
    }
    case LIST_MY_USER_SKILLS_SUCCESS: {
      const newListLogSkill = action.data || state.logMySkill;
      return {
        ...state,
        logMySkill: newListLogSkill
      };
    }
    case FILTER_BY_USER_ID_KPI:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          userIdKPI: action.data.userIdKPI
        }
      };
    case FILTER_BY_USER_ID_SKILL:
      return {
        ...state,
        status: action.type,
        filter: {
          ...state.filter,
          userIdSkill: action.data.userIdSkill
        }
      };
    default:
      return state;
  }
};
