/*
 *=================================================================================================
 * Allocation Plan Reducer
 *=================================================================================================
 */
import {
  LIST_ALLOCATION_PLANS_PROGRESS,
  LIST_ALLOCATION_PLANS_SUCCESS,
  LIST_ALLOCATION_PLANS_FAILED,
  LIST_ALLOCATION_PLANS_OF_USERS_PROGRESS,
  LIST_ALLOCATION_PLANS_OF_USERS_SUCCESS,
  LIST_ALLOCATION_PLANS_OF_USERS_FAILED,
  REMOVE_ALLOCATION_PLANS_PROGRESS,
  REMOVE_ALLOCATION_PLANS_SUCCESS,
  REMOVE_ALLOCATION_PLANS_FAILED,
  CREATE_ALLOCATION_PLANS_PROGRESS,
  CREATE_ALLOCATION_PLANS_SUCCESS,
  CREATE_ALLOCATION_PLANS_FAILED,
  UPDATE_ALLOCATION_PLANS_PROGRESS,
  UPDATE_ALLOCATION_PLANS_SUCCESS,
  UPDATE_ALLOCATION_PLANS_FAILED,
  CLEAR_ALLOCATION_PLANS,
  CLEAR_ALLOCATION_PLAN_OF_USER
} from '../07-constants/allocation-plan';

const initialState = {
  status: null,
  allocationPlanError: null,
  allocationPlanUsers: [],
  allocationPlans: [],
  mentors: [],
  skills: []
};

const allocationPlan = (state = initialState, action) => {
  switch (action.type) {
    case LIST_ALLOCATION_PLANS_OF_USERS_PROGRESS:
    case REMOVE_ALLOCATION_PLANS_PROGRESS:
    case CREATE_ALLOCATION_PLANS_PROGRESS:
    case UPDATE_ALLOCATION_PLANS_PROGRESS:
    case LIST_ALLOCATION_PLANS_PROGRESS:
      return { ...state, status: action.type };
    case LIST_ALLOCATION_PLANS_OF_USERS_FAILED:
    case REMOVE_ALLOCATION_PLANS_FAILED:
    case CREATE_ALLOCATION_PLANS_FAILED:
    case UPDATE_ALLOCATION_PLANS_FAILED:
    case LIST_ALLOCATION_PLANS_FAILED:
      return {
        ...state,
        status: action.type,
        allocationPlanError: action.data && action.data.error
      };
    case LIST_ALLOCATION_PLANS_SUCCESS: {
      return {
        ...state,
        status: action.type,
        allocationPlans: action.data ? action.data : initialState.allocationPlans
      };
    }
    case LIST_ALLOCATION_PLANS_OF_USERS_SUCCESS: {
      const data = action.data;
      let allocationPlanUsers = initialState.allocationPlanUsers;
      let mentors = initialState.mentors;
      let skills = initialState.skills;
      if (data) {
        allocationPlanUsers = data.allocationPlanUsers;
        mentors = data.mentors;
        skills = data.skills;
      }
      return {
        ...state,
        status: action.type,
        allocationPlanUsers,
        mentors,
        skills
      };
    }
    case REMOVE_ALLOCATION_PLANS_SUCCESS: {
      const allocationPlans = state.allocationPlans.filter(item => item.id !== action.data.allocationPlanId);
      return {
        ...state,
        status: action.type,
        allocationPlans
      };
    }
    case CREATE_ALLOCATION_PLANS_SUCCESS: {
      if (Array.isArray(action.data)) {
        return {
          ...state,
          status: action.type,
          allocationPlans: action.data.concat(state.allocationPlans)
        };
      }
      return { ...state, status: action.type };
    }
    case UPDATE_ALLOCATION_PLANS_SUCCESS: {
      if (action.data) {
        const allocationPlans = [...state.allocationPlans];
        const updateIndex = allocationPlans.findIndex(s => s.id === action.data.id);
        if (updateIndex > -1) {
          allocationPlans[updateIndex] = action.data;
          return {
            ...state,
            status: action.type,
            allocationPlans
          };
        }
      }
      return { ...state, status: action.type };
    }
    case CLEAR_ALLOCATION_PLANS:
      return {
        ...state,
        status: action.type,
        allocationPlans: initialState.allocationPlans,
        allocationPlanError: initialState.allocationPlanError
      };
    case CLEAR_ALLOCATION_PLAN_OF_USER:
      return {
        ...state,
        status: action.type,
        allocationPlanUsers: initialState.allocationPlanUsers,
        mentors: initialState.mentors,
        skills: initialState.skills,
        allocationPlanError: initialState.allocationPlanError
      };
    default:
      return state;
  }
};

export default allocationPlan;
