/*
 *=================================================================================================
 * Admin Project Reducer
 *=================================================================================================
 */
import {
  PROJECT_GET_ADMIN_PROGRESS,
  PROJECT_GET_ADMIN_SUCCESS,
  PROJECT_GET_ADMIN_FAILED,
  PROJECT_UPDATE_USER_PROGRESS,
  PROJECT_UPDATE_USER_SUCCESS,
  PROJECT_UPDATE_USER_FAILED,
  PROJECT_GET_CURRENT_USERS_PROGRESS,
  PROJECT_GET_CURRENT_USERS_SUCCESS,
  PROJECT_GET_CURRENT_USERS_FAILED,
  ADD_USER_PROGRESS,
  ADD_USER_SUCCESS,
  ADD_USER_FAILED,
  UPDATE_MENTOR_USER_PROGRESS,
  UPDATE_MENTOR_USER_SUCCESS,
  UPDATE_MENTOR_USER_FAILED
} from '../07-constants/adminproject';
import { getOptionsBranchFromListUser } from '../08-helpers/common';

const initialState = {
  status: null,
  adminError: null,
  listAdmins: [],
  listUsers: [],
  branches: [],
  success: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_GET_ADMIN_PROGRESS:
    case PROJECT_UPDATE_USER_PROGRESS:
    case PROJECT_GET_CURRENT_USERS_PROGRESS:
    case ADD_USER_PROGRESS:
    case UPDATE_MENTOR_USER_PROGRESS:
      return {
        ...state,
        status: action.type
      };
    case PROJECT_GET_ADMIN_FAILED:
    case PROJECT_UPDATE_USER_FAILED:
    case PROJECT_GET_CURRENT_USERS_FAILED:
    case ADD_USER_FAILED:
    case UPDATE_MENTOR_USER_FAILED:
      return {
        ...state,
        status: action.type,
        adminError: (action.data && action.data.error) || initialState.adminError
      };

    case ADD_USER_SUCCESS: {
      let newList = state.listAdmins;
      let newCurrentList = state.listUsers;
      let branches = state.branches;
      const userIndex = state.listAdmins.findIndex(u => u.id === (action.data && action.data.id));
      if (userIndex) {
        const user = {
          ...state.listAdmins[userIndex],
          roles: [...state.listAdmins[userIndex].roles, action.data.role]
        };
        newList = [...state.listAdmins.slice(0, userIndex), user, ...state.listAdmins.slice(userIndex + 1)];
        branches = getOptionsBranchFromListUser(newList);

        newCurrentList = [...state.listUsers, user];
      }
      return {
        ...state,
        status: action.type,
        listAdmins: newList,
        listUsers: newCurrentList,
        branches
      };
    }

    case PROJECT_UPDATE_USER_SUCCESS: {
      let newListUsers = state.listAdmins;
      let branches = state.branches;
      const userIndex = state.listAdmins.findIndex(u => u.id === (action.data && action.data.userId));
      if (userIndex >= 0) {
        let newRoles = state.listAdmins[userIndex].roles;
        const roleIndex = state.listAdmins[userIndex].roles.findIndex(ur => ur.id === action.data.id);
        if (roleIndex < 0) {
          newRoles = [...state.listAdmins[userIndex].roles, action.data];
        } else {
          newRoles = [
            ...state.listAdmins[userIndex].roles.slice(0, roleIndex),
            { ...state.listAdmins[userIndex].roles[roleIndex], ...action.data },
            ...state.listAdmins[userIndex].roles.slice(roleIndex + 1)
          ];
        }
        newListUsers = [
          ...state.listAdmins.slice(0, userIndex),
          {
            ...state.listAdmins[userIndex],
            roles: newRoles
          },
          ...state.listAdmins.slice(userIndex + 1)
        ];
        branches = getOptionsBranchFromListUser(newListUsers);
      }
      return {
        ...state,
        status: action.type,
        listAdmins: newListUsers,
        branches
      };
    }
    case PROJECT_GET_ADMIN_SUCCESS: {
      const listAdmins = (action.data && action.data.listAdmins) || initialState.listAdmins;
      const branches = getOptionsBranchFromListUser(listAdmins);
      return {
        ...state,
        status: action.type,
        listAdmins,
        branches
      };
    }
    case PROJECT_GET_CURRENT_USERS_SUCCESS:
      return {
        ...state,
        status: action.type,
        listUsers: (action.data && action.data.listUsers && action.data.listUsers.users) || initialState.listAdmins
      };
    case UPDATE_MENTOR_USER_SUCCESS: {
      const userIndex = state.listAdmins.findIndex(u => Number(u.id) === (action.data && action.data.id));
      const listAdmins = [
        ...state.listAdmins.slice(0, userIndex),
        {
          ...state.listAdmins[userIndex],
          mentor: (action.data && action.data.infoMentor && action.data.infoMentor.id) || null,
          infoMentor: (action.data && action.data.infoMentor && action.data.infoMentor) || null
        },
        ...state.listAdmins.slice(userIndex + 1)
      ];
      const branches = getOptionsBranchFromListUser(listAdmins);
      return {
        ...state,
        status: action.type,
        listAdmins,
        branches
      };
    }
    default:
      return state;
  }
};
