import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import createRootReducer from './reducers';

const history = createBrowserHistory();
const initialState = {};

const middleware = () => {
  const env = process.env.REACT_APP_STAGE || 'LCL';
  if (env === 'LCL') {
    return composeWithDevTools(applyMiddleware(thunkMiddleware, promise(), routerMiddleware(history)));
  } else {
    return applyMiddleware(thunkMiddleware, promise(), routerMiddleware(history));
  }
};

const store = createStore(createRootReducer(history), initialState, middleware());

export { history, store };
