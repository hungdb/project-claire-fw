import React, { lazy } from 'react';
import { promiseDelayImport } from '../08-helpers/common';
import LazyComponent from '../03-components/LazyComponent';
import SpinLoader from '../03-components/SpinLoader';

const App = lazy(() => import('../04-containers/App'));
const Login = lazy(() => import('../04-containers/_pages/Auth/Login'));
const HomePage = lazy(() => import('../04-containers/_pages/Home'));
const Tasks = lazy(() => import('../04-containers/_pages/Home/Tasks'));
const Sprint = lazy(() => import('../04-containers/_pages/Home/Sprints'));
const Allocation = lazy(() => import('../04-containers/_pages/Home/Allocations'));
const Workflow = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Workflows'));
const ProjectManagement = lazy(() => import('../04-containers/_pages/Home/Projects'));
const Users = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Users'));
const Skills = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Skills'));
const PermissionDenied = lazy(() => import('../04-containers/_pages/Error/403/Error403Page'));
const NotFound = lazy(() => import('../04-containers/_pages/Error/404/Error404Page'));
const TabBoard = lazy(() => import('../04-containers/_pages/Home/Sprints/TabBoard'));
const TabBackLog = lazy(() => import('../04-containers/_pages/Home/Sprints/TabBackLog'));
const TabIssue = lazy(() => import('../04-containers/_pages/Home/Sprints/TabIssue'));
const TabSprint = lazy(() => import('../04-containers/_pages/Home/Sprints/TabSprint'));
const Wiki = lazy(() => import('../04-containers/_pages/Home/Sprints/Wiki/Wiki.jsx'));
const TabProgress = lazy(() => import('../04-containers/_pages/Home/Sprints/TabProgress'));
const Setting = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Setting'));
const AdminProject = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Projects'));
const AdminUserTasks = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Tasks'));
const AdminAllocations = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Allocations'));
const Profile = lazy(() => import('../04-containers/_pages/Home/Profile'));
const Behaviours = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Behaviours'));
const KPI = lazy(() => import('../04-containers/_pages/Home/AdminPanel/KPI'));
const KpiLeader = lazy(() => import('../04-containers/_pages/Home/AdminPanel/KPILeader'));
const TabLog = lazy(() => import('../04-containers/_pages/Home/Sprints/TabLog'));
const SaleSetting = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Sale'));
const AlertSystem = lazy(() => import('../04-containers/_pages/Home/AdminPanel/AlertSystem'));

const AdminAllocationPlan = lazy(() => import('../04-containers/_pages/Home/AdminPanel/AllocationPlan'));
const CustomerPage = lazy(() => import('../04-containers/_pages/Home/AdminPanel/Customer'));

const ProjectCheckListTab = LazyComponent(
  promiseDelayImport(import('../04-containers/_pages/Home/Projects/ProjectSettingTabs/CheckList'), 1200),
  <SpinLoader type="grow" color="primary" />
);
const ProjectInfoTab = LazyComponent(
  promiseDelayImport(import('../04-containers/_pages/Home/Projects/ProjectSettingTabs/InforProject'), 1200),
  <SpinLoader type="grow" color="primary" />
);
const ProjectUserAndRoleTab = LazyComponent(
  promiseDelayImport(import('../04-containers/_pages/Home/Projects/ProjectSettingTabs/UserInProject'), 1200),
  <SpinLoader type="grow" color="primary" />
);
const ProjectAlertTab = LazyComponent(
  promiseDelayImport(import('../04-containers/_pages/Home/Projects/ProjectSettingTabs/AlertTab'), 1200),
  <SpinLoader type="grow" color="primary" />
);
const ProjectDeploymentTab = LazyComponent(
  promiseDelayImport(import('../04-containers/_pages/Home/Projects/ProjectSettingTabs/DeploymentTab'), 1200),
  <SpinLoader type="grow" color="primary" />
);

const routes = [
  {
    path: '/login',
    component: Login
  },
  {
    component: App,
    protected: '/login',
    routes: [
      {
        path: '/',
        component: HomePage,
        routes: [
          // Tasks
          {
            path: '/tasks',
            component: Tasks
          },
          // Sprint
          {
            path: '/sprints/:projectId?',
            component: Sprint,
            routes: [
              {
                path: '/sprints/:projectId/board/:filterType(1|2)?/:taskId?',
                component: TabBoard
              },
              {
                // path: '/sprints/:projectId/backlog/:viewType(1|2)/:limit/:page/:taskId?/:actionType(1|2)?',
                path: '/sprints/:projectId/backlog/:taskId?',
                component: TabBackLog
              },
              {
                path: '/sprints/:projectId/sprint/:sprintId?',
                component: TabSprint
              },
              {
                path: '/sprints/:projectId/wiki/:documentId?',
                component: Wiki
              },
              {
                path: '/sprints/:projectId/progress',
                component: TabProgress
              },
              {
                path: '/sprints/:projectId/issues/:issueId?',
                component: TabIssue
              },
              {
                path: '/sprints/:projectId/log',
                component: TabLog
              }
            ]
          },
          // Allocation
          {
            path: '/allocations',
            component: Allocation
          },
          // ProjectManagement
          {
            path: '/projects/:groupId?/:projectId?',
            component: ProjectManagement,
            routes: [
              {
                path: '/projects/:groupId?/:projectId?/checklist',
                component: ProjectCheckListTab
              },
              {
                path: '/projects/:groupId?/:projectId?/users-roles',
                component: ProjectUserAndRoleTab
              },
              {
                path: '/projects/:groupId?/:projectId?/alert-notifi',
                component: ProjectAlertTab
              },
              {
                path: '/projects/:groupId?/:projectId?/deployment/:envId?',
                component: ProjectDeploymentTab
              },
              {
                path: '/projects/:groupId?/:projectId?/:info',
                component: ProjectInfoTab
              }
            ]
          },
          // Users
          {
            path: '/users',
            component: Users
          },
          // User Tasks
          {
            path: '/user-tasks',
            component: AdminUserTasks
          },
          // Admin allocation
          {
            path: '/admin-allocations',
            component: AdminAllocations
          },
          // Admin allocation
          {
            path: '/admin-allocation-plans',
            component: AdminAllocationPlan
          },
          // Skill
          {
            path: '/skills',
            component: Skills
          },
          //Behaviours
          {
            path: '/behaviours',
            component: Behaviours
          },
          // KPI
          {
            path: '/kpi',
            component: KPI
          },
          // Admin projects
          {
            path: '/admin-projects',
            component: AdminProject
          },
          //KPI-Leader
          {
            path: '/kpi-leader',
            component: KpiLeader
          },
          // Workflow
          {
            path: '/workflows',
            component: Workflow
          },
          // Alert system
          {
            path: '/alert-system',
            component: AlertSystem
          },
          // Setting
          {
            path: '/settings',
            component: Setting
          },
          // Profile
          {
            path: '/profile/:userId?',
            component: Profile
          },
          // Admin-sale
          {
            path: '/sales',
            component: SaleSetting
          },
          {
            path: '/customer',
            component: CustomerPage
          },
          // PermissionDenied
          {
            path: '/error/403',
            component: PermissionDenied
          },
          // NotFound
          {
            component: NotFound
          }
        ]
      }
    ]
  }
];

export default routes;
