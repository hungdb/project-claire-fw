import React, { Fragment, Suspense } from 'react';
import { array, bool, object } from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { isTokenExpired } from '../08-helpers/jwt';

const RenderRoutes = ({ routes, isAuthState, location }) => {
  // Check routes empty
  if (!routes) {
    return null;
  }

  // Apply protected
  routes.forEach(route => {
    if (!route.protected || !route.routes) {
      return;
    }
    return route.routes.forEach(routeChildren => {
      if (routeChildren.path === route.protected) {
        return;
      }
      return (routeChildren.protected = route.protected);
    });
  });

  return (
    <Suspense fallback={<div />}>
      <Switch>
        {routes.map((route, i) => (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            strict={route.strict}
            render={props => (
              <Fragment>
                {// Not logged
                route.protected && !isAuthState ? (
                  <Redirect
                    to={{
                      pathname: route.protected,
                      state: { from: location.pathname + location.search }
                    }}
                  />
                ) : (
                  <route.component {...props} route={route} />
                )}
              </Fragment>
            )}
          />
        ))}
      </Switch>
    </Suspense>
  );
};

RenderRoutes.propTypes = {
  routes: array,
  isAuthState: bool,
  location: object
};

const mapStateToProps = store => ({
  isAuthState: !isTokenExpired(store.auth.loginToken)
});

export default withRouter(connect(mapStateToProps)(RenderRoutes));
