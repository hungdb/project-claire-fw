/*
 *=================================================================================================
 * Wiki Actions
 *=================================================================================================
 */
import {
  SELECT_DOCUMENT,
  WIKI_DOCUMENT_UI_STATUS,
  WIKI_FILE_ATTACHMENT_UI_STATUS,
  GET_LIST_DOCUMENT_PROGRESS,
  GET_LIST_DOCUMENT_SUCCESS,
  GET_LIST_DOCUMENT_FAILED,
  LOAD_MORE_DOCUMENT_PROGRESS,
  LOAD_MORE_DOCUMENT_SUCCESS,
  LOAD_MORE_DOCUMENT_FAILED,
  SEARCH_DOCUMENT_PROGRESS,
  SEARCH_DOCUMENT_SUCCESS,
  SEARCH_DOCUMENT_FAILED,
  ADD_DOCUMENT_PROGRESS,
  ADD_DOCUMENT_SUCCESS,
  ADD_DOCUMENT_FAILED,
  UPDATE_DOCUMENT_PROGRESS,
  UPDATE_DOCUMENT_SUCCESS,
  UPDATE_DOCUMENT_FAILED,
  REMOVE_DOCUMENT_PROGRESS,
  REMOVE_DOCUMENT_SUCCESS,
  REMOVE_DOCUMENT_FAILED,
  GET_LIST_ATTACHMENT_PROGRESS,
  GET_LIST_ATTACHMENT_SUCCESS,
  GET_LIST_ATTACHMENT_FAILED,
  LOAD_MORE_ATTACHMENT_PROGRESS,
  LOAD_MORE_ATTAHMENT_SUCCESS,
  LOAD_MORE_ATTACHMENT_FAILED,
  SEARCH_ATTACHMENT_PROGRESS,
  SEARCH_ATTACHMENT_SUCCESS,
  SEARCH_ATTACHMENT_FAILED,
  REMOVE_ATTACHMENT_PROGRESS,
  REMOVE_ATTACHMENT_SUCCESS,
  REMOVE_ATTACHMENT_FAILED,
  GET_PRE_SIGNED_URL_ATTACHMENT_PROGRESS,
  GET_PRE_SIGNED_URL_ATTACHMENT_SUCCESS,
  GET_PRE_SIGNED_URL_ATTACHMENT_FAILED,
  UPLOAD_ATTACHMENT_PROGRESS,
  UPLOAD_ATTACHMENT_SUCCESS,
  UPLOAD_ATTACHMENT_FAILED,
  GET_LIST_ATTACHMENT_OF_TASK_PROGRESS,
  GET_LIST_ATTACHMENT_OF_TASK_FAILED,
  GET_LIST_ATTACHMENT_OF_TASK_SUCCESS,
  UPLOAD_ATTACHMENT_TO_TASK_PROGRESS,
  UPLOAD_ATTACHMENT_TO_TASK_FAILED,
  UPLOAD_ATTACHMENT_TO_TASK_SUCCESS,
  LOAD_MORE_ATTACHMENT_OF_TASK_FAILED,
  LOAD_MORE_ATTACHMENT_OF_TASK_PROGRESS,
  LOAD_MORE_ATTACHMENT_OF_TASK_SUCCESS,
  REMOVE_ATTACHMENT_OF_TASK_FAILED,
  REMOVE_ATTACHMENT_OF_TASK_PROGRESS,
  REMOVE_ATTACHMENT_OF_TASK_SUCCESS
} from '../07-constants/wiki';
import { getTokenBearer, updateStatus } from '../08-helpers/common';
import { callApi } from '../05-utils/commonUtils';
import { getEnv } from '../env';
import { CHECK_ERROR } from '../07-constants/common';

export const selectDocument = (selectedDocument = null) => ({
  type: SELECT_DOCUMENT,
  selectedDocument
});

export const setStatusDocumentUI = (status = 0) => ({
  type: WIKI_DOCUMENT_UI_STATUS,
  statusDocumentUI: status
});

export const setStatusFileAttachmentUI = (status = 0) => ({
  type: WIKI_FILE_ATTACHMENT_UI_STATUS,
  statusFileAttachmentUI: status
});

export const getDocuments = ({ projectId, subject }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_documents_by_project',
      projectId,
      subject
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_DOCUMENT_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_LIST_DOCUMENT_SUCCESS, {
            documents: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreDocuments = ({ projectId, subject, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_documents_by_project',
      projectId,
      subject,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_DOCUMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_DOCUMENT_SUCCESS, {
            documents: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const searchDocuments = ({ projectId, subject, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_documents_by_project',
      projectId,
      subject,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(SEARCH_DOCUMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(SEARCH_DOCUMENT_SUCCESS, {
            documents: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(SEARCH_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addDocument = ({ projectId, subject, content, parentId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_document_wiki',
      projectId,
      subject,
      content,
      parentId
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_DOCUMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_DOCUMENT_SUCCESS, { document: data }));
      },
      err => {
        dispatch(updateStatus(ADD_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateDocument = ({ documentId, projectId, subject, content, parentId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_document_wiki',
      documentId,
      projectId,
      subject,
      content,
      parentId
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_DOCUMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_DOCUMENT_SUCCESS, { document: data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeDocument = ({ documentId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_document_wiki',
      documentId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_DOCUMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(REMOVE_DOCUMENT_SUCCESS, { document: data }));
      },
      err => {
        dispatch(updateStatus(REMOVE_DOCUMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getAttachments = ({ projectId, name, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_project',
      projectId,
      name,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_LIST_ATTACHMENT_SUCCESS, {
            attachments: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreAttachments = ({ projectId, name, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_project',
      projectId,
      name,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_ATTAHMENT_SUCCESS, {
            attachments: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const searchAttachments = ({ projectId, name, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_project',
      projectId,
      name,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(SEARCH_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(SEARCH_ATTACHMENT_SUCCESS, {
            attachments: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(SEARCH_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAttachment = ({ attachmentId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_attachment_by_project',
      attachmentId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(REMOVE_ATTACHMENT_SUCCESS, {
            attachment: data.attachment
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const downloadFile = ({ filePath, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_signed_url',
      filePath,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PRE_SIGNED_URL_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_PRE_SIGNED_URL_ATTACHMENT_SUCCESS, { url: data }));
        // window.location.href = data;
        window.open(data, '_blank');
      },
      err => {
        dispatch(updateStatus(GET_PRE_SIGNED_URL_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const uploadFile = ({ files, projectId }) => {
  const formData = new FormData();
  formData.append('action', 'add_attachment_by_project');
  formData.append('projectId', projectId);
  for (const file of files) {
    formData.append('files', file, file.name);
  }
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      // 'Content-Type': 'multipart/form-data',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(UPLOAD_ATTACHMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPLOAD_ATTACHMENT_SUCCESS, { attachments: data }));
      },
      err => {
        dispatch(updateStatus(UPLOAD_ATTACHMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getAttachmentsOfTask = ({ taskId, projectId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_task',
      projectId,
      taskId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_ATTACHMENT_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_LIST_ATTACHMENT_OF_TASK_SUCCESS, {
            attachmentsOfTask: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_ATTACHMENT_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const uploadFileToTask = ({ files, taskId, projectId }) => {
  const formData = new FormData();
  formData.append('action', 'add_attachment_by_task');
  formData.append('projectId', projectId);
  formData.append('taskId', taskId);
  for (const file of files) {
    formData.append('files', file, file.name);
  }
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      // 'Content-Type': 'multipart/form-data',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(UPLOAD_ATTACHMENT_TO_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPLOAD_ATTACHMENT_TO_TASK_SUCCESS, {
            attachmentsOfTask: data
          })
        );
      },
      err => {
        dispatch(updateStatus(UPLOAD_ATTACHMENT_TO_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreAttachmentsOfTask = ({ projectId, taskId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_task',
      projectId,
      taskId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_ATTACHMENT_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_ATTACHMENT_OF_TASK_SUCCESS, {
            attachmentsOfTask: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_ATTACHMENT_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAttachmentOfTask = ({ attachmentId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_attachment_by_task',
      attachmentId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ATTACHMENT_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(REMOVE_ATTACHMENT_OF_TASK_SUCCESS, {
            attachmentOfTask: data
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
