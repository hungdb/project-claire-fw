/*
 *=================================================================================================
 * Project Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, checkAccessToken, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  PROJECT_PROGRESS,
  PROJECT_GET_SUCCESS,
  PROJECT_FAILED,
  PROJECT_CREATE_PROGRESS,
  PROJECT_CREATE_SUCCESS,
  PROJECT_CREATE_FAILED,
  PROJECT_UPDATE_PROGRESS,
  PROJECT_UPDATE_SUCCESS,
  PROJECT_UPDATE_FAILED,
  PROJECT_REMOVE_PROGRESS,
  PROJECT_REMOVE_SUCCESS,
  PROJECT_REMOVE_FAILED,
  PROJECT_GET_EVN_PROGRESS,
  PROJECT_GET_EVN_SUCCESS,
  PROJECT_GET_EVN_FAILED,
  PROJECT_CREATE_EVN_PROGRESS,
  PROJECT_CREATE_EVN_SUCCESS,
  PROJECT_CREATE_EVN_FAILED,
  PROJECT_UPDATE_EVN_PROGRESS,
  PROJECT_UPDATE_EVN_SUCCESS,
  PROJECT_UPDATE_EVN_FAILED,
  // PROJECT_REMOVE_EVN_PROGRESS,
  // PROJECT_REMOVE_EVN_SUCCESS,
  // PROJECT_REMOVE_EVN_FAILED,
  // PROJECT_GET_LIST_ROLE_PROGRESS,
  // PROJECT_GET_LIST_ROLE_SUCCESS,
  // PROJECT_GET_LIST_ROLE_FAILED,
  // PROJECT_GET_HEATH_CHECK_PROGRESS,
  // PROJECT_GET_HEATH_CHECK_SUCCESS,
  // PROJECT_GET_HEATH_CHECK_FAILED,
  // PROJECT_CREATE_CONFIG_PROGRESS,
  // PROJECT_CREATE_CONFIG_SUCCESS,
  // PROJECT_CREATE_CONFIG_FAILED,
  // PROJECT_UPDATE_CONFIG_PROGRESS,
  // PROJECT_UPDATE_CONFIG_SUCCESS,
  // PROJECT_UPDATE_CONFIG_FAILED,
  // PROJECT_REMOVE_CONFIG_PROGRESS,
  // PROJECT_REMOVE_CONFIG_SUCCESS,
  // PROJECT_REMOVE_CONFIG_FAILED,
  GET_ALERT_SUBSCRIPTION_PROGRESS,
  GET_ALERT_SUBSCRIPTION_SUCCESS,
  GET_ALERT_SUBSCRIPTION_FAILED,
  UPDATE_ALERT_SUBSCRIPTION_PROGRESS,
  UPDATE_ALERT_SUBSCRIPTION_SUCCESS,
  UPDATE_ALERT_SUBSCRIPTION_FAILED,
  PROJECT_GET_BY_USER_PROGRESS,
  PROJECT_GET_BY_USER_SUCCESS,
  PROJECT_GET_BY_USER_FAILED,
  GET_HEALTH_CHECK_PROGRESS,
  GET_HEALTH_CHECK_SUCCESS,
  GET_HEALTH_CHECK_FAILED,
  TOGGLE_HEALTH_CHECK_SERVER_DETAIL,
  CLEAR_HEALTH_CHECK_DATA,
  GET_USERS_OF_PROJECT_PROGRESS,
  GET_USERS_OF_PROJECT_SUCCESS,
  GET_USERS_OF_PROJECT_FAILED,
  SWITCH_PROJECT,
  UPDATE_PROJECT_ALERT_ENABLE_FAILED,
  UPDATE_PROJECT_ALERT_ENABLE_SUCCESS,
  GET_LOGS_OF_PROJECT_PROGRESS,
  GET_LOGS_OF_PROJECT_SUCCESS,
  GET_LOGS_OF_PROJECT_FAILED,
  CLEAR_LOG_HISTORY,
  CLEAR_SWITCH_PROJECT,
  CLEAR_SWITCH_GROUP_PROJECT,
  SET_SELECTED_SETTING_TAB,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_SUCCESS,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_FAILED,
  GET_DETAIL_PROJECT_PROGRESS,
  GET_DETAIL_PROJECT_SUCCESS,
  GET_DETAIL_PROJECT_FAILED,
  CLEAR_LIST_PROJECTS,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_SUCCESS,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_PROGRESS,
  UPDATE_ALERT_SUBSCRIPTION_BY_USER_FAILED,
  SET_ITEM_ALERT_SUBSCRIPTION_BY_USER
} from '../07-constants/project';

export const getProjects = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_projects'
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(PROJECT_GET_SUCCESS, { projects: data }));
      },
      err => {
        dispatch(updateStatus(PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getMyProjects = () => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_BY_USER_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        return callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_my_projects'
          }),
          null,
          data => {
            dispatch(updateStatus(PROJECT_GET_BY_USER_SUCCESS, { listProject: data }));
          },
          err => {
            dispatch(updateStatus(PROJECT_GET_BY_USER_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const createProject = data => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_CREATE_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_project',
            name: data.projectName,
            description: data.description,
            groupId: data.projectGroupId,
            workflowSchemeId: data.projectWorkflowId,
            permissionSchemeId: data.projectPermissionSchemeId
          }),
          null,
          data => {
            dispatch(updateStatus(PROJECT_CREATE_SUCCESS, { project: data }));
          },
          err => {
            dispatch(updateStatus(PROJECT_CREATE_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const updateProject = updateProjectData => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_UPDATE_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'update_project',
            projectId: updateProjectData.id,
            name: updateProjectData.name,
            isActive: updateProjectData.isActive,
            groupId: updateProjectData.groupId,
            projectRate: updateProjectData.projectRate
          }),
          null,
          data => {
            dispatch(
              updateStatus(PROJECT_UPDATE_SUCCESS, {
                project: updateProjectData
              })
            );
          },
          err => {
            dispatch(updateStatus(PROJECT_UPDATE_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const removeProjects = removeProjectData => {
  const config = {
    method: 'post',
    headers: {
      'Content-type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_project',
      projectId: removeProjectData.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_REMOVE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(PROJECT_REMOVE_SUCCESS, { removeProjectData }));
      },
      err => {
        dispatch(updateStatus(PROJECT_REMOVE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getProjectEnvironments = data => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_EVN_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        return callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_project_environments',
            projectId: data.projectId
          }),
          null,
          data => {
            dispatch(updateStatus(PROJECT_GET_EVN_SUCCESS, { environments: data }));
          },
          err => {
            dispatch(updateStatus(PROJECT_GET_EVN_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const addProjectEnvironment = data => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_CREATE_EVN_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_environment',
            projectId: data.projectId,
            name: data.name
          }),
          null,
          data => {
            dispatch(updateStatus(PROJECT_CREATE_EVN_SUCCESS, { environment: data }));
          },
          err => {
            dispatch(updateStatus(PROJECT_CREATE_EVN_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const updateProjectEnvironment = data => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_UPDATE_EVN_PROGRESS));
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'update_environment',
            ...data
          }),
          null,
          data => {
            dispatch(updateStatus(PROJECT_UPDATE_EVN_SUCCESS, { env: data }));
          },
          err => {
            dispatch(updateStatus(PROJECT_UPDATE_EVN_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};
//
// export const removeEVN = (removeEVNdata) => {
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_REMOVE_EVN_PROGRESS));
//     checkAccessToken()
//       .then(result => {
//         if (result.success || (result.error && result.error.code === "TOKEN_NOT_EXPIRED")) {
//           callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//             defaultPostConfig({
//               action: 'remove_environment',
//               envId: removeEVNdata.idEVN,
//             }),
//             null,
//             (data) => {
//               dispatch(updateStatus(PROJECT_REMOVE_EVN_SUCCESS, {removeEVNs: removeEVNdata}));
//             },
//             (err) => {
//               dispatch(updateStatus(PROJECT_REMOVE_EVN_FAILED, {error: err}));
//               dispatch(updateStatus(CHECK_ERROR, {error: err}));
//             }
//           );
//         }
//       });
//   };
// };

// export const getRoleProject = (getaRoleProjectdata) => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': getTokenBearer(),
//     },
//     body: JSON.stringify({
//
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_GET_LIST_ROLE_PROGRESS));
//     checkAccessToken()
//       .then(result => {
//         if (result.success || (result.error && result.error.code === "TOKEN_NOT_EXPIRED")) {
//           callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//             defaultPostConfig({
//               action: 'get_project',
//               projectId: getaRoleProjectdata.projectId,
//             }),
//             null,
//             (data) => {
//               dispatch(updateStatus(PROJECT_GET_LIST_ROLE_SUCCESS, {listRoleProjects: data}));
//             },
//             (err) => {
//               dispatch(updateStatus(PROJECT_GET_LIST_ROLE_FAILED, {error: err}));
//               dispatch(updateStatus(CHECK_ERROR, {error: err}));
//             }
//           );
//         }
//       });
//   };
// };
//
// export const getHeathCheck = (getHeathCheckData) => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': getTokenBearer(),
//     },
//     body: JSON.stringify({
//       'action': 'get_environment',
//       'envId': getHeathCheckData.evnId,
//       'secretKey': getHeathCheckData.secretKey,
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_GET_HEATH_CHECK_PROGRESS));
//     callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//       config,
//       null,
//       (data) => {
//         dispatch(updateStatus(PROJECT_GET_HEATH_CHECK_SUCCESS, {listHeathCheck: data}));
//       },
//       (err) => {
//         dispatch(updateStatus(PROJECT_GET_HEATH_CHECK_FAILED, {error: err}));
//         dispatch(updateStatus(CHECK_ERROR, {error: err}));
//       }
//     );
//   };
// };

// export const createConfig = (createConfigData) => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': getTokenBearer(),
//     },
//     body: JSON.stringify({
//       'action': 'create_configuration',
//       'envId': createConfigData.evnId,
//       'key': createConfigData.key,
//       'value': createConfigData.value,
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_CREATE_CONFIG_PROGRESS));
//     callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//       config,
//       null,
//       (data) => {
//         dispatch(updateStatus(PROJECT_CREATE_CONFIG_SUCCESS, {createConfig: data}));
//       },
//       (err) => {
//         dispatch(updateStatus(PROJECT_CREATE_CONFIG_FAILED, {error: err}));
//         dispatch(updateStatus(CHECK_ERROR, {error: err}));
//       }
//     );
//   };
// };

// export const updateConfig = (updateConfigData) => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': getTokenBearer(),
//     },
//     body: JSON.stringify({
//       'action': 'update_configuration',
//       'envId': updateConfigData.envId,
//       'configId': updateConfigData.configId,
//       'key': updateConfigData.key,
//       'value': updateConfigData.value,
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_UPDATE_CONFIG_PROGRESS));
//     callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//       config,
//       null,
//       (data) => {
//         dispatch(updateStatus(PROJECT_UPDATE_CONFIG_SUCCESS, {updateConfig: updateConfigData}));
//       },
//       (err) => {
//         dispatch(updateStatus(PROJECT_UPDATE_CONFIG_FAILED, {error: err}));
//         dispatch(updateStatus(CHECK_ERROR, {error: err}));
//       }
//     );
//   };
// };

// export const removeConfig = (removeConfigData) => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': getTokenBearer(),
//     },
//     body: JSON.stringify({
//       'action': 'remove_configuration',
//       'configId': removeConfigData.configId,
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(PROJECT_REMOVE_CONFIG_PROGRESS));
//     callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//       config,
//       null,
//       (data) => {
//         dispatch(updateStatus(PROJECT_REMOVE_CONFIG_SUCCESS, {removeConfig: removeConfigData}));
//       },
//       (err) => {
//         dispatch(updateStatus(PROJECT_REMOVE_CONFIG_FAILED, {error: err}));
//         dispatch(updateStatus(CHECK_ERROR, {error: err}));
//       }
//     );
//   };
// };

export const getUserAlertSubscription = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_project_alert_subscription',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ALERT_SUBSCRIPTION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_ALERT_SUBSCRIPTION_SUCCESS, { subscription: data }));
      },
      err => {
        dispatch(updateStatus(GET_ALERT_SUBSCRIPTION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateProjectAlertSubscription = ({ projectId, environmentIds, methodIds }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_project_alert_subscription',
      projectId,
      environmentIds,
      methodIds
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ALERT_SUBSCRIPTION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPDATE_ALERT_SUBSCRIPTION_SUCCESS, {
            subscription: data
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_ALERT_SUBSCRIPTION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateProjectAlertSubscriptionByUser = ({ userId, projectId, environmentIds, methodIds }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_project_alert_subscription_by_user',
      userId,
      projectId,
      environmentIds,
      methodIds
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ALERT_SUBSCRIPTION_BY_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPDATE_ALERT_SUBSCRIPTION_BY_USER_SUCCESS, {
            subscription: data
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_ALERT_SUBSCRIPTION_BY_USER_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadHealthCheckData = ({ envId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_health_check_data',
      envId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_HEALTH_CHECK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_HEALTH_CHECK_SUCCESS, { healthCheck: data }));
      },
      err => {
        dispatch(updateStatus(GET_HEALTH_CHECK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const toggleServerDetail = serverId => {
  return dispatch => {
    dispatch(updateStatus(TOGGLE_HEALTH_CHECK_SERVER_DETAIL, { serverId }));
  };
};

export const clearHealthCheckData = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_HEALTH_CHECK_DATA));
  };
};

export const setItemAlertSubscriptionByUser = item => {
  return dispatch => {
    dispatch(updateStatus(SET_ITEM_ALERT_SUBSCRIPTION_BY_USER, { item }));
  };
};

export const getUsersOfProject = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_users_of_project',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_USERS_OF_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_USERS_OF_PROJECT_SUCCESS, { users: data }));
      },
      err => {
        dispatch(updateStatus(GET_USERS_OF_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const switchProject = project => {
  return dispatch => {
    dispatch(updateStatus(SWITCH_PROJECT, { selectedProject: project }));
  };
};

export const updateProjectAlertEnable = ({ projectId, isEnabled }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'toggle_notification_enable',
      projectId,
      isEnabled
    })
  };
  return dispatch => {
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(UPDATE_PROJECT_ALERT_ENABLE_SUCCESS, {
            projectId,
            isEnabled
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_PROJECT_ALERT_ENABLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getLogOfProject = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_logs_of_project',
      projectId
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LOGS_OF_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_LOGS_OF_PROJECT_SUCCESS, { logs: data }));
      },
      err => {
        dispatch(updateStatus(GET_LOGS_OF_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearLogHistory = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_LOG_HISTORY));
  };
};

export const clearSwitchProject = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SWITCH_PROJECT));
  };
};

export const clearSwitchGroupProject = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SWITCH_GROUP_PROJECT));
  };
};

export const setSelectedSettingTab = value => {
  return dispatch => {
    dispatch(updateStatus(SET_SELECTED_SETTING_TAB, { value }));
  };
};

export const getDetailProject = projectId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_detail_project',
      projectId
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_DETAIL_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_DETAIL_PROJECT_SUCCESS, { detailProject: data }));
      },
      err => {
        dispatch(updateStatus(GET_DETAIL_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getUsersSubscriptionProjectsAlert = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_users_alert_subscription_of_project',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_SUCCESS, data));
      },
      err => {
        dispatch(
          updateStatus(GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT_FAILED, {
            error: err
          })
        );
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearListProjects = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_LIST_PROJECTS));
  };
};
