/*
 *=================================================================================================
 * Auth Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { toast } from 'react-toastify';
import i18n from '../i18n';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  LOGIN_PROGRESS,
  REDIRECT_PROGRESS,
  REDIRECT_SUCCESS,
  REDIRECT_FAILED,
  GET_USER_INFO_SUCCESS,
  GET_MY_ROLE_AND_PERMISSION_PROGRESS,
  GET_MY_ROLE_AND_PERMISSION_SUCCESS,
  GET_MY_ROLE_AND_PERMISSION_FAILED,
  PRE_LOGIN_SUCCESS,
  PRE_LOGIN_FAILED,
  STORE_TOKEN_FIREBASE,
  LOGOUT_PROGRESS,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED
} from '../07-constants/auth';
import { TOAST_ERROR_STYLE } from '../05-utils/commonData';

export const getOAuthLink = () => {
  return dispatch => {
    try {
      dispatch(updateStatus(REDIRECT_PROGRESS));
      const permissions = JSON.stringify([
        {
          permissionId: 'read_owned_info',
          permissionSettings: [
            {
              name: 'subset',
              value: ['id', 'username', 'profilePhoto', 'email', 'phoneNumber']
            }
          ]
        }
      ]);
      window.location.href = `${getEnv('IDENTITY_OAUTH')}&appId=${getEnv(
        'OAUTH_APP_ID'
      )}&callbackUrl=${encodeURIComponent(getEnv('OAUTH_APP_CALLBACK_URL'))}&permissions=${permissions}`;
      dispatch(updateStatus(REDIRECT_SUCCESS));
    } catch (e) {
      dispatch(updateStatus(REDIRECT_FAILED, { error: 'REDIRECT_FAILED' }));
      toast.error(i18n.t('REDIRECT_FAILED'), TOAST_ERROR_STYLE);
    }
  };
};

export const showRejectedReason = reason => {
  // toast.error(i18n.t(reason));
  return dispatch => {
    dispatch(updateStatus(CHECK_ERROR, { error: { code: reason } }));
  };
};

export const getUserInfo = () => {
  return dispatch => {
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'read_my_info'
      }),
      null,
      data => {
        dispatch(updateStatus(GET_USER_INFO_SUCCESS, { user: data }));
      },
      err => {
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const login = ({ provider, code }) => {
  return dispatch => {
    dispatch(updateStatus(LOGIN_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig(
        {
          action: 'login',
          grantType: 'oauth',
          grantData: [provider, code, getEnv('OAUTH_APP_CALLBACK_URL')]
        },
        false
      ),
      null,
      data => {
        dispatch(updateStatus(PRE_LOGIN_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(PRE_LOGIN_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const logout = () => {
  return dispatch => {
    dispatch(updateStatus(LOGOUT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'logout'
      }),
      null,
      () => {
        dispatch(updateStatus(LOGOUT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(LOGOUT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getMyRolePermission = () => {
  return dispatch => {
    dispatch(updateStatus(GET_MY_ROLE_AND_PERMISSION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_my_role_and_permission'
      }),
      null,
      data => {
        dispatch(updateStatus(GET_MY_ROLE_AND_PERMISSION_SUCCESS, { user: data }));
      },
      err => {
        dispatch(updateStatus(GET_MY_ROLE_AND_PERMISSION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const storeTokenFirebase = ({ tokenFirebase }) => {
  return dispatch => {
    dispatch({ type: STORE_TOKEN_FIREBASE });
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'store_token_firebase',
        tokenFirebase
      }),
      null,
      result => {},
      error => {
        // dispatch(updateStatus(CHECK_ERROR, { error }));
      }
    );
  };
};
