/*
 *=================================================================================================
 * Comment Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_COMMENTS_OF_TASK_PROGRESS,
  GET_COMMENTS_OF_TASK_SUCCESS,
  GET_COMMENTS_OF_TASK_FAILED,
  GET_COMMENTS_OF_USER_FAILED,
  GET_COMMENTS_OF_USER_PROGRESS,
  GET_COMMENTS_OF_USER_SUCCESS,
  UPDATE_COMMENT_FAILED,
  UPDATE_COMMENT_PROGRESS,
  UPDATE_COMMENT_SUCCESS,
  ADD_COMMENT_FAILED,
  ADD_COMMENT_PROGRESS,
  ADD_COMMENT_SUCCESS,
  DELETE_COMMENT_FAILED,
  DELETE_COMMENT_PROGRESS,
  DELETE_COMMENT_SUCCESS,
  LOAD_MORE_COMMENTS_OF_TASK_FAILED,
  LOAD_MORE_COMMENTS_OF_TASK_PROGRESS,
  LOAD_MORE_COMMENTS_OF_TASK_SUCCESS,
  FIRE_SOCKET_ADD_COMMENT,
  FIRE_SOCKET_DELETE_COMMENT,
  FIRE_SOCKET_UPDATE_COMMENT,
  GET_COMMENTS_OF_ISSUE_PROGRESS,
  GET_COMMENTS_OF_ISSUE_SUCCESS,
  GET_COMMENTS_OF_ISSUE_FAILED,
  ADD_COMMENT_ISSUE_FAILED,
  ADD_COMMENT_ISSUE_PROGRESS,
  ADD_COMMENT_ISSUE_SUCCESS,
  UPDATE_COMMENT_ISSUE_FAILED,
  UPDATE_COMMENT_ISSUE_PROGRESS,
  UPDATE_COMMENT_ISSUE_SUCCESS,
  DELETE_COMMENT_ISSUE_FAILED,
  DELETE_COMMENT_ISSUE_PROGRESS,
  DELETE_COMMENT_ISSUE_SUCCESS,
  LOAD_MORE_COMMENTS_OF_ISSUE_FAILED,
  LOAD_MORE_COMMENTS_OF_ISSUE_PROGRESS,
  LOAD_MORE_COMMENTS_OF_ISSUE_SUCCESS,
  FIRE_SOCKET_ADD_COMMENT_ISSUE,
  FIRE_SOCKET_DELETE_COMMENT_ISSUE,
  FIRE_SOCKET_UPDATE_COMMENT_ISSUE
} from '../07-constants/comment';

export const getCommentsOfTask = ({ taskId, projectId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_comments_by_taskId',
      projectId,
      taskId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_COMMENTS_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_COMMENTS_OF_TASK_SUCCESS, {
            commentsOfTask: data.rows,
            commentsOfTaskTotal: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_COMMENTS_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getCommentsOfUser = ({ projectId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_comments_by_userId',
      projectId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_COMMENTS_OF_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_COMMENTS_OF_USER_SUCCESS, {
            commentsOfUser: data.row,
            commentsOfUserTotal: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_COMMENTS_OF_USER_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreCommentsOfTask = ({ projectId, taskId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_comments_by_taskId',
      projectId,
      taskId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_COMMENTS_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_COMMENTS_OF_TASK_SUCCESS, {
            commentsOfTask: data.rows,
            commentsOfTaskTotal: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_COMMENTS_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addComment = ({ taskId, projectId, content }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_comment',
      projectId,
      taskId,
      content
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_COMMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_COMMENT_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(ADD_COMMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const deleteComment = ({ commentId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'delete_comment',
      projectId,
      commentId
    })
  };

  return dispatch => {
    dispatch(updateStatus(DELETE_COMMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(DELETE_COMMENT_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(DELETE_COMMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateComment = ({ commentId, projectId, content }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_comment',
      projectId,
      commentId,
      content
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_COMMENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_COMMENT_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_COMMENT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const fireSocketAddComment = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_ADD_COMMENT, { newComment: data }));
  };
};

export const fireSocketUpdateComment = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_UPDATE_COMMENT, { updatedComment: data }));
  };
};

export const fireSocketDeleteComment = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_DELETE_COMMENT, { deletedComment: data }));
  };
};

export const getCommentsOfIssue = ({ issueId, limit, page }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_comments_issue_by_issueId',
      issueId,
      limit,
      page
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_COMMENTS_OF_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_COMMENTS_OF_ISSUE_SUCCESS, {
            commentsOfIssue: data.rows,
            commentsOfIssueTotal: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_COMMENTS_OF_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addCommentIssue = ({ issueId, content }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_comment_issue',
      issueId,
      content
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_COMMENT_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_COMMENT_ISSUE_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(ADD_COMMENT_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const deleteCommentIssue = ({ commentIssueId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'delete_comment_issue',
      commentIssueId
    })
  };

  return dispatch => {
    dispatch(updateStatus(DELETE_COMMENT_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(DELETE_COMMENT_ISSUE_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(DELETE_COMMENT_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateCommentIssue = ({ commentIssueId, content }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_comment_issue',
      commentIssueId,
      content
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_COMMENT_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_COMMENT_ISSUE_SUCCESS, { comment: data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_COMMENT_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreCommentsOfIssue = ({ issueId, limit, page }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_comments_issue_by_issueId',
      issueId,
      limit,
      page
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_COMMENTS_OF_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_COMMENTS_OF_ISSUE_SUCCESS, {
            commentsOfIssue: data.rows,
            commentsOfIssueTotal: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_COMMENTS_OF_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const fireSocketAddCommentIssue = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_ADD_COMMENT_ISSUE, { newComment: data }));
  };
};

export const fireSocketUpdateCommentIssue = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_UPDATE_COMMENT_ISSUE, { updatedComment: data }));
  };
};

export const fireSocketDeleteCommentIssue = ({ data }) => {
  return dispatch => {
    dispatch(updateStatus(FIRE_SOCKET_DELETE_COMMENT_ISSUE, { deletedComment: data }));
  };
};
