/*
 *=================================================================================================
 * Task Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, checkAccessToken, defaultPostConfig } from '../05-utils/commonUtils';
import { getTokenBearer, updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  TASKOWN_PROJECTS,
  TASKOWN_PROJECTS_SUCCESS,
  TASKOWN_PROJECTS_FAILED,
  TASKOWN_TASKS,
  TASKOWN_TASKS_SUCCESS,
  TASKOWN_TASKS_FAILED,
  GET_TASKS_OF_USERS_PROGRESS,
  GET_TASKS_OF_USERS_SUCCESS,
  GET_TASKS_OF_USERS_FAILED
} from '../07-constants/task';

export const getProjects = () => {
  return dispatch => {
    dispatch(updateStatus(TASKOWN_PROJECTS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_current_project_by_userId'
          }),
          null,
          res => {
            dispatch(updateStatus(TASKOWN_PROJECTS_SUCCESS, res));
          },
          err => {
            dispatch(updateStatus(TASKOWN_PROJECTS_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const getTasks = () => {
  return dispatch => {
    dispatch(updateStatus(TASKOWN_TASKS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_my_current_tasks'
          }),
          null,
          res => {
            dispatch(updateStatus(TASKOWN_TASKS_SUCCESS, res));
          },
          err => {
            dispatch(updateStatus(TASKOWN_TASKS_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const getTaskOfUsers = ({ typeComparison, estimate, date, name, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_task_of_all_users',
      typeComparison,
      estimate,
      date,
      name,
      projectId
    })
  };

  return dispatch => {
    dispatch({ type: GET_TASKS_OF_USERS_PROGRESS });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_TASKS_OF_USERS_SUCCESS, { taskOfUsers: result }));
      },
      error => {
        dispatch(updateStatus(GET_TASKS_OF_USERS_FAILED, { error }));
        dispatch(updateStatus(CHECK_ERROR, { error }));
      }
    );
  };
};
