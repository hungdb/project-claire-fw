/*
 *=================================================================================================
 * User In Project Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  // UIP_PROJECTS_GET,
  // UIP_PROJECTS_GET_SUCCESS,
  // UIP_PROJECTS_GET_FAILED,
  UIP_USERS_GET,
  UIP_USERS_GET_SUCCESS,
  UIP_USERS_GET_FAILED,
  UIP_USER_CREATE,
  UIP_USER_CREATE_SUCCESS,
  UIP_USER_CREATE_FAILED,
  UIP_USER_UPDATE,
  UIP_USER_UPDATE_SUCCESS,
  UIP_USER_UPDATE_FAILED,
  UIP_USER_DELETE,
  UIP_USER_DELETE_SUCCESS,
  UIP_USER_DELETE_FAILED,
  UIP_LIST_USER,
  UIP_LIST_USER_SUCCESS,
  UIP_LIST_USER_FAILED,
  UIP_LIST_ROLE,
  UIP_LIST_ROLE_SUCCESS,
  UIP_LIST_ROLE_FAILED,
  CLEAR_USER_IN_PROJECT,
  GET_USERS_OF_ALL_PROJECT,
  GET_USERS_OF_ALL_PROJECT_SUCCESS,
  GET_USERS_OF_ALL_PROJECT_FAILED
} from '../07-constants/userInProject';

// export const getProjects = () => {
//   const config = {
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': 'api_call_will_replace_this',
//     },
//     body: JSON.stringify({
//       'action': 'list_projects',
//     })
//   };
//
//   return (dispatch) => {
//     dispatch(updateStatus(UIP_PROJECTS_GET));
//     callApi(getEnv('REACT_APP_API_SERVER') + '/api/rpc',
//       config,
//       null,
//       (result) => {
//         dispatch(updateStatus(UIP_PROJECTS_GET_SUCCESS, result));
//       },
//       (err) => {
//         dispatch(updateStatus(UIP_PROJECTS_GET_FAILED, err));
//         dispatch(updateStatus(CHECK_ERROR, { error: err }));
//       }
//     )
//   };
// };

export const getUsers = data => {
  return dispatch => {
    dispatch(updateStatus(UIP_USERS_GET));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_users_by_project',
        projectId: data.projectId
      }),
      null,
      result => {
        dispatch(updateStatus(UIP_USERS_GET_SUCCESS, result));
      },
      err => {
        dispatch(updateStatus(UIP_USERS_GET_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addUserIntoProject = data => {
  return dispatch => {
    dispatch(updateStatus(UIP_USER_CREATE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'add_user_into_project',
        projectId: data.projectId,
        userId: data.userId,
        projectRoleIds: data.projectRoleIds
      }),
      null,
      () => {
        dispatch(updateStatus(UIP_USER_CREATE_SUCCESS));
      },
      err => {
        dispatch(updateStatus(UIP_USER_CREATE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addAllUserIntoProject = data => {
  return dispatch => {
    dispatch(updateStatus(UIP_USER_CREATE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'add_all_user_into_project',
        projectId: data.projectId,
        projectRoleIds: data.projectRoleIds
      }),
      null,
      () => {
        dispatch(updateStatus(UIP_USER_CREATE_SUCCESS));
      },
      err => {
        dispatch(updateStatus(UIP_USER_CREATE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateUserRoleInProject = data => {
  return dispatch => {
    dispatch(updateStatus(UIP_USER_UPDATE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_role_user_in_project',
        projectId: data.projectId,
        userId: data.userId,
        projectRoleIds: data.roleIds
      }),
      null,
      () => {
        dispatch(updateStatus(UIP_USER_UPDATE_SUCCESS));
      },
      err => {
        dispatch(updateStatus(UIP_USER_UPDATE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeUserFromProject = data => {
  return dispatch => {
    dispatch(updateStatus(UIP_USER_DELETE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_user_from_project',
        userId: data.userId,
        projectId: data.projectId
      }),
      null,
      () => {
        dispatch(updateStatus(UIP_USER_DELETE_SUCCESS));
      },
      err => {
        dispatch(updateStatus(UIP_USER_DELETE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getAllUsers = () => {
  return dispatch => {
    dispatch(updateStatus(UIP_LIST_USER));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_current_users',
        isActive: true
      }),
      null,
      result => {
        dispatch(updateStatus(UIP_LIST_USER_SUCCESS, { users: result.users }));
      },
      err => {
        dispatch(updateStatus(UIP_LIST_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listRole = () => {
  return dispatch => {
    dispatch(updateStatus(UIP_LIST_ROLE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_project_role'
      }),
      null,
      result => {
        dispatch(updateStatus(UIP_LIST_ROLE_SUCCESS, result));
      },
      err => {
        dispatch(updateStatus(UIP_LIST_ROLE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearUserInProject = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_USER_IN_PROJECT));
  };
};

export const getUsersOfAllProject = () => {
  return dispatch => {
    dispatch(updateStatus(GET_USERS_OF_ALL_PROJECT));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_users_of_all_project'
      }),
      null,
      result => {
        dispatch(updateStatus(GET_USERS_OF_ALL_PROJECT_SUCCESS, result));
      },
      err => {
        dispatch(updateStatus(GET_USERS_OF_ALL_PROJECT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
