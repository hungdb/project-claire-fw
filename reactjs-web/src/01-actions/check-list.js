/*
 *=================================================================================================
 * Checklist Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_ALL_ITEMS_CHECK_PROGRESS,
  GET_ALL_ITEMS_CHECK_FAILED,
  GET_ALL_ITEMS_CHECK_SUCCESS,
  ADD_ITEM_CHECK_PROGRESS,
  ADD_ITEM_CHECK_SUCCESS,
  ADD_ITEM_CHECK_FAILED,
  UPDATE_ITEM_CHECK_PROGRESS,
  UPDATE_ITEM_CHECK_SUCCESS,
  UPDATE_ITEM_CHECK_FAILED,
  REMOVE_ITEM_CHECK_PROGRESS,
  REMOVE_ITEM_CHECK_SUCCESS,
  REMOVE_ITEM_CHECK_FAILED,
  CHECK_UNCHECK_ITEM_SUCCESS,
  CHECK_UNCHECK_ITEM_FAILED,
  CHECK_UNCHECK_ITEM_PROGRESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_PROGRESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_SUCCESS,
  GET_LIST_ITEM_CHECK_OF_PROJECT_FAILED,
  CLEAR_ITEMS_CHECKED,
  ADD_ITEM_CHECK_TO_PROJECT_PROGRESS,
  ADD_ITEM_CHECK_TO_PROJECT_SUCCESS,
  ADD_ITEM_CHECK_TO_PROJECT_FAILED,
  REMOVE_ITEM_CHECK_OF_PROJECT_PROGRESS,
  REMOVE_ITEM_CHECK_OF_PROJECT_SUCCESS,
  REMOVE_ITEM_CHECK_OF_PROJECT_FAILED
} from '../07-constants/check-list';

export const listAllItemCheck = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_all_item_check'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ALL_ITEMS_CHECK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_ALL_ITEMS_CHECK_SUCCESS, { listItemsCheck: data }));
      },
      err => {
        dispatch(updateStatus(GET_ALL_ITEMS_CHECK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createItemCheck = createItemCheckData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_item_check',
      name: createItemCheckData.name,
      description: createItemCheckData.description
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ITEM_CHECK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_ITEM_CHECK_SUCCESS, { createItemCheckData: data }));
      },
      err => {
        dispatch(updateStatus(ADD_ITEM_CHECK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateItemCheck = updateItemCheckData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_item_check',
      itemCheckId: updateItemCheckData.id,
      name: updateItemCheckData.name,
      description: updateItemCheckData.description
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ITEM_CHECK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(UPDATE_ITEM_CHECK_SUCCESS, {
            updateItemCheckData: updateItemCheckData
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_ITEM_CHECK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeItemCheck = removeItemCheckData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_item_check',
      itemCheckId: removeItemCheckData.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ITEM_CHECK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_ITEM_CHECK_SUCCESS, {
            removeItemCheckData: removeItemCheckData
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ITEM_CHECK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const checkUncheckItem = checkUncheckItemData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'check_uncheck_item',
      checkItemId: checkUncheckItemData.checkItemId,
      projectId: checkUncheckItemData.projectId,
      isChecked: checkUncheckItemData.isChecked
    })
  };

  return dispatch => {
    dispatch(updateStatus(CHECK_UNCHECK_ITEM_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(CHECK_UNCHECK_ITEM_SUCCESS, {
            data: checkUncheckItemData
          })
        );
      },
      err => {
        dispatch(updateStatus(CHECK_UNCHECK_ITEM_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listItemCheckOfProject = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_item_check_of_project',
      projectId: params.projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_ITEM_CHECK_OF_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_LIST_ITEM_CHECK_OF_PROJECT_SUCCESS, {
            listItemsCheckOfProject: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_ITEM_CHECK_OF_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearItemChecked = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ITEMS_CHECKED));
  };
};

export const addItemCheckToProject = addItemCheckToProjectData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'add_item_check_to_project',
      projectId: addItemCheckToProjectData.projectId,
      checkItemId: addItemCheckToProjectData.checkItemId
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ITEM_CHECK_TO_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(ADD_ITEM_CHECK_TO_PROJECT_SUCCESS, {
            addItemCheckOfProjectData: data
          })
        );
      },
      err => {
        dispatch(updateStatus(ADD_ITEM_CHECK_TO_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeItemCheckOfProject = removeItemCheckOfProjectData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_item_check_of_project',
      itemId: removeItemCheckOfProjectData.itemId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ITEM_CHECK_OF_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_ITEM_CHECK_OF_PROJECT_SUCCESS, {
            removeItemCheckOfProjectData: removeItemCheckOfProjectData
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ITEM_CHECK_OF_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
