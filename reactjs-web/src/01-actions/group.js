/*
 *=================================================================================================
 * Group Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_GROUP_PROGRESS,
  GET_GROUP_SUCCESS,
  GET_GROUP_FAILED,
  CREATE_GROUP_PROGRESS,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_FAILED,
  UPDATE_GROUP_PROGRESS,
  UPDATE_GROUP_SUCCESS,
  UPDATE_GROUP_FAILED,
  REMOVE_GROUP_PROGRESS,
  REMOVE_GROUP_SUCCESS,
  REMOVE_GROUP_FAILED,
  SWITCH_PROJECT_GROUP,
  TOGGLE_NOTIFICATION_GROUP_PROGRESS,
  TOGGLE_NOTIFICATION_GROUP_SUCCESS,
  TOGGLE_NOTIFICATION_GROUP_FAILED
} from '../07-constants/group';

export const getGroups = () => {
  return dispatch => {
    dispatch(updateStatus(GET_GROUP_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_groups'
      }),
      null,
      data => {
        dispatch(updateStatus(GET_GROUP_SUCCESS, { groups: data }));
      },
      err => {
        dispatch(updateStatus(GET_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getMyGroups = () => {
  return dispatch => {
    dispatch(updateStatus(GET_GROUP_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_my_project_groups'
      }),
      null,
      data => {
        dispatch(updateStatus(GET_GROUP_SUCCESS, { groups: data }));
      },
      err => {
        dispatch(updateStatus(GET_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addGroup = groupData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_group',
      name: groupData.name,
      isActive: groupData.isActive
    })
  };
  return dispatch => {
    dispatch(updateStatus(CREATE_GROUP_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_GROUP_SUCCESS, { group: data }));
      },
      err => {
        dispatch(updateStatus(CREATE_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateGroup = updateGroupData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_group',
      groupId: updateGroupData.id,
      name: updateGroupData.name,
      isActive: updateGroupData.isActive
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_GROUP_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(UPDATE_GROUP_SUCCESS, { group: updateGroupData }));
      },
      err => {
        dispatch(updateStatus(UPDATE_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const deleteGroup = deleteGroupData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_group',
      groupId: deleteGroupData.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_GROUP_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_GROUP_SUCCESS, { removedGroup: deleteGroupData }));
      },
      err => {
        dispatch(updateStatus(REMOVE_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const switchProjectGroup = projectGroup => {
  return dispatch => {
    dispatch(updateStatus(SWITCH_PROJECT_GROUP, { selectedProjectGroup: projectGroup }));
  };
};

export const toggleNotificationGroup = (groupId, isEnabled) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'toggle_notification_enable_group',
      groupId,
      isEnabled
    })
  };

  return dispatch => {
    dispatch(updateStatus(TOGGLE_NOTIFICATION_GROUP_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        const data = { groupId, isEnabled };
        dispatch(updateStatus(TOGGLE_NOTIFICATION_GROUP_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(TOGGLE_NOTIFICATION_GROUP_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
