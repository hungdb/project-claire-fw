/*
 *=================================================================================================
 * Skill Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  LIST_SKILL_PROGRESS,
  LIST_SKILL_SUCCESS,
  LIST_SKILL_FAILED,
  GET_SKILL_PROGRESS,
  GET_SKILL_SUCCESS,
  GET_SKILL_FAILED,
  REMOVE_SKILL_PROGRESS,
  REMOVE_SKILL_SUCCESS,
  REMOVE_SKILL_FAILED,
  CREATE_SKILL_PROGRESS,
  CREATE_SKILL_SUCCESS,
  CREATE_SKILL_FAILED,
  UPDATE_SKILL_PROGRESS,
  UPDATE_SKILL_SUCCESS,
  UPDATE_SKILL_FAILED,
  LIST_SKILL_BY_USER_PROGRESS,
  LIST_SKILL_BY_USER_SUCCESS,
  LIST_SKILL_BY_USER_FAILED,
  LIST_USER_BY_SKILL_PROGRESS,
  LIST_USER_BY_SKILL_SUCCESS,
  LIST_USER_BY_SKILL_FAILED,
  GET_USER_SKILL_PROGRESS,
  GET_USER_SKILL_SUCCESS,
  GET_USER_SKILL_FAILED,
  REMOVE_USER_SKILL_PROGRESS,
  REMOVE_USER_SKILL_SUCCESS,
  REMOVE_USER_SKILL_FAILED,
  CREATE_USER_SKILL_PROGRESS,
  CREATE_USER_SKILL_SUCCESS,
  CREATE_USER_SKILL_FAILED,
  UPDATE_USER_SKILL_PROGRESS,
  UPDATE_USER_SKILL_SUCCESS,
  UPDATE_USER_SKILL_FAILED,
  HANDLE_CHANGE_PAGE,
  HANDLE_CHANGE_ROWS_PER_PAGE,
  LIST_ALL_SKILL_FAILED,
  LIST_ALL_SKILL_PROGRESS,
  LIST_ALL_SKILL_SUCCESS,
  CLEAR_USER_SKILL,
  GET_SKILLS_OF_ALL_USERS_PROGRESS,
  GET_SKILLS_OF_ALL_USERS_SUCCESS,
  GET_SKILLS_OF_ALL_USERS_FAILED,
  CREATE_KPI_SKILL_PROGRESS,
  CREATE_KPI_SKILL_SUCCESS,
  CREATE_KPI_SKILL_FAILED,
  GET_LIST_KPI_SKILL_OF_USER_PROGRESS,
  GET_LIST_KPI_SKILL_OF_USER_SUCCESS,
  GET_LIST_KPI_SKILL_OF_USER_FAILED,
  REMOVE_KPI_SKILL_PROGRESS,
  REMOVE_KPI_SKILL_SUCCESS,
  REMOVE_KPI_SKILL_FAILED,
  LIST_MY_USER_SKILLS_FAILED,
  LIST_MY_USER_SKILLS_PROGRESS,
  LIST_MY_USER_SKILLS_SUCCESS,
  FILTER_BY_USER_ID_KPI,
  FILTER_BY_USER_ID_SKILL
} from '../07-constants/skill';

export const getSkill = skillId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_skill',
      skillId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_SKILL_SUCCESS, { currentSkill: result }));
      },
      err => {
        dispatch(updateStatus(GET_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listSkills = ({ limit, page }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_skills',
      limit,
      page
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_SKILL_SUCCESS, { skills: result }));
      },
      err => {
        dispatch(updateStatus(LIST_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSkill = ({ name, description, linkIcon }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_skill',
      name,
      description,
      linkIcon
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_SKILL_SUCCESS, { createdSkill: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSkill = updateSkilldata => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_skill',
      skillId: updateSkilldata.id,
      name: updateSkilldata.name,
      description: updateSkilldata.description,
      linkIcon: updateSkilldata.linkIcon
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_SKILL_SUCCESS, { updatedSkill: data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeSkill = removeSkilldata => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_skill',
      skillId: removeSkilldata.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(REMOVE_SKILL_SUCCESS, { removedSkill: result }));
      },
      err => {
        dispatch(updateStatus(REMOVE_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const handleChangeRowsPerPage = e => ({
  type: HANDLE_CHANGE_ROWS_PER_PAGE,
  rowsPerPage: e.target.value
});

export const handleChangePage = (e, page) => ({
  type: HANDLE_CHANGE_PAGE,
  page
});

export const getUserSkill = userskillId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_user_skill',
      userskillId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_USER_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_USER_SKILL_SUCCESS, { userskills: result }));
      },
      err => {
        dispatch(updateStatus(GET_USER_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listSkillsByUser = ({ userId, type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_skills_by_user',
      userId,
      type
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_SKILL_BY_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_SKILL_BY_USER_SUCCESS, { skillsByUser: result }));
      },
      err => {
        dispatch(updateStatus(LIST_SKILL_BY_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listUsersBySkill = ({ skillId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_users_by_skill',
      skillId
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_USER_BY_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_USER_BY_SKILL_SUCCESS, { skillbyuser: result }));
      },
      err => {
        dispatch(updateStatus(LIST_USER_BY_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createUserSkill = ({ userId, skillId, evaluationPoint, isStandard }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_user_skill',
      userId,
      skillId,
      evaluationPoint,
      isStandard
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_USER_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_USER_SKILL_SUCCESS, { userSkill: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_USER_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateUserSkill = updateUserSkilldata => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_user_skill',
      userSkillId: updateUserSkilldata.id,
      evaluationPoint: updateUserSkilldata.evaluationPoint,
      isStandard: updateUserSkilldata.isStandard
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_USER_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(UPDATE_USER_SKILL_SUCCESS, { updateUserSkill: result }));
      },
      err => {
        dispatch(updateStatus(UPDATE_USER_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listAllSkills = isStandard => {
  return dispatch => {
    dispatch(updateStatus(LIST_ALL_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_skills',
        isStandard
      }),
      null,
      result => {
        dispatch(updateStatus(LIST_ALL_SKILL_SUCCESS, { skills: result }));
      },
      err => {
        dispatch(updateStatus(LIST_ALL_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeUserSkill = removedUserSkill => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_user_skill',
      userSkillId: removedUserSkill.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_USER_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(REMOVE_USER_SKILL_SUCCESS, { removedUserSkill: result }));
      },
      err => {
        dispatch(updateStatus(REMOVE_USER_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearUserSkill = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_USER_SKILL));
  };
};

export const getSkillsOfAllUsers = () => {
  return dispatch => {
    dispatch(updateStatus(GET_SKILLS_OF_ALL_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_skills_of_all_users'
      }),
      null,
      data => {
        dispatch(updateStatus(GET_SKILLS_OF_ALL_USERS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_SKILLS_OF_ALL_USERS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createKpiSkill = (userId, reviewPoint, testPoint, evaluatedAt) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_kpi_skill',
      userId,
      reviewPoint,
      testPoint,
      evaluatedAt
    })
  };
  return dispatch => {
    dispatch(updateStatus(CREATE_KPI_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_KPI_SKILL_SUCCESS, { data: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_KPI_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListKPISkillOfUser = (userId, page, limit) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_kpi_skill',
      userId,
      page,
      limit
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_KPI_SKILL_OF_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_LIST_KPI_SKILL_OF_USER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_LIST_KPI_SKILL_OF_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeKpiSkill = kpiSkillId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_kpi_skill',
      kpiSkillId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_KPI_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_KPI_SKILL_SUCCESS, kpiSkillId));
      },
      err => {
        dispatch(updateStatus(REMOVE_KPI_SKILL_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listMyUserSkills = (limit, page) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_my_user_skills',
      limit,
      page
    })
  };
  return dispatch => {
    dispatch(updateStatus(LIST_MY_USER_SKILLS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_MY_USER_SKILLS_SUCCESS, { data: result }));
      },
      err => {
        dispatch(updateStatus(LIST_MY_USER_SKILLS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const handleFilterByUserIdKPI = userIdKPI => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_USER_ID_KPI, { userIdKPI }));
  };
};

export const handleFilterByUserIdSkill = userIdSkill => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_USER_ID_SKILL, { userIdSkill }));
  };
};
