/*
 *=================================================================================================
 * Project Task View Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';

import {
  PTV_FILTER_QUERY_URL,
  PTV_FILTER_SPRINT,
  PTV_FILTER_PRIORITY,
  PTV_FILTER_ASSIGNEE,
  PTV_FILTER_STATUS,
  PTV_FILTER_START_DATE,
  PTV_FILTER_SEARCH,
  PTV_FILTER_ROWS_PER_PAGE,
  PTV_FILTER_PAGE,
  PTV_USERS_IN_PROJECT_GET,
  PTV_USERS_IN_PROJECT_GET_SUCCESS,
  PTV_USERS_IN_PROJECT_GET_FAILED,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET_SUCCESS,
  PTV_WORKFLOW_STATE_IN_PROJECT_GET_FAILED,
  PTV_PROGRESS_BAR_GET,
  PTV_PROGRESS_BAR_GET_SUCCESS,
  PTV_PROGRESS_BAR_GET_FAILED,
  PTV_TASKS_IN_PROJECT_GET,
  PTV_TASKS_IN_PROJECT_GET_SUCCESS,
  PTV_TASKS_IN_PROJECT_GET_FAILED,
  PTV_FILTER_VIEW_TYPE,
  PTV_CLEAR_FILTER_ASSIGNEE,
  PTV_CLEAR_FILTER_PRIORITY,
  PTV_CLEAR_FILTER_SEARCH,
  PTV_CLEAR_FILTER_SPRINT,
  PTV_CLEAR_FILTER_STATUS,
  PTV_CLEAR_FILTER_START_DATE,
  PTV_CLEAR_ALL_FILTER
} from '../07-constants/project-task-view';

export const handleFilterQueryUrl = (sprint, priority, assignee, status, startDate, search, limit, page, viewType) => ({
  type: PTV_FILTER_QUERY_URL,
  sprint,
  priority,
  assignee,
  status,
  startDate,
  search,
  limit,
  page,
  viewType
});

export const handleFilterViewType = type => ({
  type: PTV_FILTER_VIEW_TYPE,
  viewType: [1, 2].includes(type) ? type : 1
});

export const handleFilterSprint = sprint => ({
  type: PTV_FILTER_SPRINT,
  sprint: sprint && sprint.value
});

export const handleFilterPriority = priority => ({
  type: PTV_FILTER_PRIORITY,
  priority: priority && priority.value
});

export const handleFilterAssignee = assignee => ({
  type: PTV_FILTER_ASSIGNEE,
  assignee: assignee && assignee.value
});

export const handleFilterStatus = status => ({
  type: PTV_FILTER_STATUS,
  status: status && status.value
});

export const handleFilterStartDate = startDate => ({
  type: PTV_FILTER_START_DATE,
  startDate
});

export const handleFilterSearch = search => ({
  type: PTV_FILTER_SEARCH,
  search
});

export const handleFilterRowsPerPage = limit => ({
  type: PTV_FILTER_ROWS_PER_PAGE,
  rowsPerPage: limit
});

export const handleFilterPage = page => ({
  type: PTV_FILTER_PAGE,
  page
});

export const handleClearAllFilter = () => ({
  type: PTV_CLEAR_ALL_FILTER
});

export const handleClearFilterAssignee = () => ({
  type: PTV_CLEAR_FILTER_ASSIGNEE
});

export const handleClearFilterPriority = () => ({
  type: PTV_CLEAR_FILTER_PRIORITY
});

export const handleClearFilterSearch = () => ({
  type: PTV_CLEAR_FILTER_SEARCH
});

export const handleClearFilterSprint = () => ({
  type: PTV_CLEAR_FILTER_SPRINT
});

export const handleClearFilterStatus = () => ({
  type: PTV_CLEAR_FILTER_STATUS
});

export const handleClearFilterStartDate = () => ({
  type: PTV_CLEAR_FILTER_START_DATE
});

export const getStatistical = projectId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'progress_statistical_all_tasks_by_project',
      projectId: projectId
    })
  };

  return dispatch => {
    dispatch({ type: PTV_PROGRESS_BAR_GET });
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        const progressBar = {};
        result.forEach(e => {
          progressBar[e.status] = e.total;
        });
        dispatch({
          type: PTV_PROGRESS_BAR_GET_SUCCESS,
          progressBar
        });
      },
      error => {
        dispatch({
          type: PTV_PROGRESS_BAR_GET_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

export const getUsersInProject = projectId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_users_of_project',
      projectId
    })
  };

  return dispatch => {
    dispatch({ type: PTV_USERS_IN_PROJECT_GET });
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        const optionsUser = result.map(e => ({
          value: e.User.id,
          label: e.User.name ? e.User.name : 'ID: ' + e.User.id
        }));
        dispatch({
          type: PTV_USERS_IN_PROJECT_GET_SUCCESS,
          optionsUser
        });
      },
      error => {
        dispatch({
          type: PTV_USERS_IN_PROJECT_GET_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

export const getWorkFlowStateInProject = projectId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow_state_by_projectId',
      projectId
    })
  };

  return dispatch => {
    dispatch({ type: PTV_WORKFLOW_STATE_IN_PROJECT_GET });
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        const optionsStatus = result.map(e => ({ value: e.id, label: e.name }));
        dispatch({
          type: PTV_WORKFLOW_STATE_IN_PROJECT_GET_SUCCESS,
          optionsStatus
        });
      },
      error => {
        dispatch({
          type: PTV_WORKFLOW_STATE_IN_PROJECT_GET_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

export const getTasksInProject = (
  rowsPerPage = null,
  page = null,
  projectId = null,
  sprintId = null,
  userId = null,
  priorityId = null,
  statusId = null,
  startDate = null,
  search = null
) => {
  return dispatch => {
    dispatch({ type: PTV_TASKS_IN_PROJECT_GET });
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'filter_tasks_of_project',
        limit: rowsPerPage,
        page,
        projectId,
        sprintId,
        assignee: userId,
        priority: priorityId,
        status: statusId,
        startDate,
        key: search
      }),
      null,
      result => {
        dispatch(
          updateStatus(PTV_TASKS_IN_PROJECT_GET_SUCCESS, {
            tasks: result.data,
            totalTask: result.total
          })
        );
      },
      error => {
        dispatch({
          type: PTV_TASKS_IN_PROJECT_GET_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};
