/*
 *=================================================================================================
 * Allocation Plan Actions
 *=================================================================================================
 */
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import {
  LIST_ALLOCATION_PLANS_PROGRESS,
  LIST_ALLOCATION_PLANS_SUCCESS,
  LIST_ALLOCATION_PLANS_FAILED,
  LIST_ALLOCATION_PLANS_OF_USERS_PROGRESS,
  LIST_ALLOCATION_PLANS_OF_USERS_SUCCESS,
  LIST_ALLOCATION_PLANS_OF_USERS_FAILED,
  REMOVE_ALLOCATION_PLANS_PROGRESS,
  REMOVE_ALLOCATION_PLANS_SUCCESS,
  REMOVE_ALLOCATION_PLANS_FAILED,
  CREATE_ALLOCATION_PLANS_PROGRESS,
  CREATE_ALLOCATION_PLANS_SUCCESS,
  CREATE_ALLOCATION_PLANS_FAILED,
  UPDATE_ALLOCATION_PLANS_PROGRESS,
  UPDATE_ALLOCATION_PLANS_SUCCESS,
  UPDATE_ALLOCATION_PLANS_FAILED,
  CLEAR_ALLOCATION_PLANS,
  CLEAR_ALLOCATION_PLAN_OF_USER
} from '../07-constants/allocation-plan';
import { CHECK_ERROR } from '../07-constants/common';
import { updateStatus, removeDuplicateObjInArray } from '../08-helpers/common';
import { getEnv } from '../env';

export const listAllocationPlans = () => {
  return dispatch => {
    dispatch(updateStatus(LIST_ALLOCATION_PLANS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_allocation_plans'
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_ALLOCATION_PLANS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_ALLOCATION_PLANS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listAllocationsPlanOfUsers = () => {
  return dispatch => {
    dispatch(updateStatus(LIST_ALLOCATION_PLANS_OF_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_allocations_plan_of_users'
      }),
      null,
      result => {
        let skills = [];
        let mentors = [];
        result.forEach(user => {
          skills = skills.concat(user.skills);
          if (user.infoMentor) {
            mentors = mentors.concat(user.infoMentor);
          }
        });

        skills = removeDuplicateObjInArray({ list: skills, key: 'id' });
        mentors = removeDuplicateObjInArray({ list: mentors, key: 'id' });
        skills = skills.map(item => ({ value: item.id, label: item.name }));
        mentors = mentors.map(item => ({ value: item.id, label: item.name }));

        dispatch(
          updateStatus(LIST_ALLOCATION_PLANS_OF_USERS_SUCCESS, {
            allocationPlanUsers: result,
            skills,
            mentors
          })
        );
      },
      err => {
        dispatch(updateStatus(LIST_ALLOCATION_PLANS_OF_USERS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createAllocationPlan = data => {
  return dispatch => {
    dispatch(updateStatus(CREATE_ALLOCATION_PLANS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'create_allocation_plan',
        data
      }),
      null,
      result => {
        dispatch(updateStatus(CREATE_ALLOCATION_PLANS_SUCCESS, result));
      },
      err => {
        dispatch(updateStatus(CREATE_ALLOCATION_PLANS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateAllocationPlan = ({ id, effortPercent, startDate, endDate }) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_ALLOCATION_PLANS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_allocation_plan',
        allocationPlanId: id,
        effortPercent,
        startDate,
        endDate
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_ALLOCATION_PLANS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_ALLOCATION_PLANS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAllocationPlan = allocationPlanId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_ALLOCATION_PLANS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_allocation_plan',
        allocationPlanId
      }),
      null,
      () => {
        dispatch(updateStatus(REMOVE_ALLOCATION_PLANS_SUCCESS, { allocationPlanId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_ALLOCATION_PLANS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearAllocationPlans = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ALLOCATION_PLANS));
  };
};

export const clearAllocationPlanOfUser = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ALLOCATION_PLAN_OF_USER));
  };
};
