/*
 *=================================================================================================
 * KPI Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_LIST_KPI_TO_CHART_PROGRESS,
  GET_LIST_KPI_TO_CHART_SUCCESS,
  GET_LIST_KPI_TO_CHART_FAILED,
  GET_MY_LIST_KPI_TO_CHART_PROGRESS,
  GET_MY_LIST_KPI_TO_CHART_SUCCESS,
  GET_MY_LIST_KPI_TO_CHART_FAILED,
  CLEAR_KPI_CHART,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  CLEAR_LIST_USERS,
  FILTER_BY_START_DATE,
  FILTER_BY_END_DATE,
  FILTER_BY_PROJECT,
  FILTER_BY_USER,
  FILTER_BY_TYPE,
  FILTER_BY_TYPE_TIME,
  FILTER_BY_YEAR
} from '../07-constants/kpi';

export const listUsers = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_current_users',
      isActive: true
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_USERS_SUCCESS, { listUsers: result }));
      },
      err => {
        dispatch(updateStatus(LIST_USERS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListKpiToChart = ({ type, userId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_kpi_to_chart',
      type: parseInt(type),
      userId: userId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_KPI_TO_CHART_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_LIST_KPI_TO_CHART_SUCCESS, { kpiDataChart: result }));
      },
      err => {
        dispatch(updateStatus(GET_LIST_KPI_TO_CHART_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getMyListKpiToChart = ({ type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_my_list_kpi_to_chart',
      type: parseInt(type)
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_MY_LIST_KPI_TO_CHART_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(GET_MY_LIST_KPI_TO_CHART_SUCCESS, {
            kpiDataChart: result
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_MY_LIST_KPI_TO_CHART_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const handleFilterByStartDate = startDate => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_START_DATE, { startDate }));
  };
};

export const handleFilterByEndDate = endDate => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_END_DATE, { endDate }));
  };
};

export const handleFilterByType = type => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_TYPE, { type }));
  };
};

export const handleFilterByTypeTime = typeTime => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_TYPE_TIME, { typeTime }));
  };
};

export const handleFilterByProject = projectId => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_PROJECT, { projectId }));
  };
};

export const handleFilterByUser = userId => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_USER, { userId }));
  };
};

export const handleFilterByYear = year => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_YEAR, { year }));
  };
};

export const clearListUser = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_LIST_USERS));
  };
};

export const clearListKpiToChart = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_KPI_CHART));
  };
};
