/*
 *=================================================================================================
 * Issue Actions
 *=================================================================================================
 */
import { getTokenBearer, updateStatus } from '../08-helpers/common';
import {
  CREATE_ISSUE_FAILED,
  CREATE_ISSUE_PROGRESS,
  CREATE_ISSUE_SUCCESS,
  LIST_ISSUES_FAILED,
  LIST_ISSUES_PROGRESS,
  LIST_ISSUES_SUCCESS,
  UPDATE_ISSUE_FAILED,
  UPDATE_ISSUE_PROGRESS,
  UPDATE_ISSUE_SUCCESS,
  REMOVE_ISSUE_FAILED,
  REMOVE_ISSUE_PROGRESS,
  REMOVE_ISSUE_SUCCESS,
  GET_ISSUE_FAILED,
  GET_ISSUE_PROGRESS,
  GET_ISSUE_SUCCESS,
  STATISTICAL_ISSUES_OF_PROJECT_PROGRESS,
  STATISTICAL_ISSUES_OF_PROJECT_FAILED,
  STATISTICAL_ISSUES_OF_PROJECT_SUCCESS,
  FILTER_BY_SEARCH_SUBJECT,
  CREATE_MULTIPLE_ISSUE_PROGRESS,
  CREATE_MULTIPLE_ISSUE_SUCCESS,
  CREATE_MULTIPLE_ISSUE_FAILED,
  CLOSE_ALL_ISSUES_PROGRESS,
  CLOSE_ALL_ISSUES_SUCCESS,
  CLOSE_ALL_ISSUES_FAILED,
  GET_LIST_ATTACHMENT_OF_ISSUE_PROGRESS,
  GET_LIST_ATTACHMENT_OF_ISSUE_SUCCESS,
  GET_LIST_ATTACHMENT_OF_ISSUE_FAILED,
  UPLOAD_ATTACHMENT_TO_ISSUE_PROGRESS,
  UPLOAD_ATTACHMENT_TO_ISSUE_SUCCESS,
  UPLOAD_ATTACHMENT_TO_ISSUE_FAILED,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_PROGRESS,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_SUCCESS,
  LOAD_MORE_ATTACHMENT_OF_ISSUE_FAILED,
  REMOVE_ATTACHMENT_OF_ISSUE_PROGRESS,
  REMOVE_ATTACHMENT_OF_ISSUE_SUCCESS,
  REMOVE_ATTACHMENT_OF_ISSUE_FAILED,
  CLEAR_ISSUE,
  CLEAR_CURRENT_ISSUE,
  FILTER_BY_STATUS,
  FILTER_BY_CREATOR,
  FILTER_BY_LIMIT,
  FILTER_BY_ISSUE_PAGE,
  CLEAR_FILTER_CREATOR,
  CLEAR_FILTER_LIMIT,
  CLEAR_FILTER_PAGE,
  CLEAR_FILTER_SEARCH,
  CLEAR_FILTER_STATUS
} from '../07-constants/issue';
import { callApi, defaultFormDataConfig } from '../05-utils/commonUtils';
import { getEnv } from '../env';
import { CHECK_ERROR } from '../07-constants/common';

export const getIssue = issueId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_issue',
      issueId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_ISSUE_SUCCESS, { currentIssue: result }));
      },
      err => {
        dispatch(updateStatus(GET_ISSUE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listIssues = ({ limit, page, subject, projectId, status, creator }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_issue',
      limit,
      page,
      subject,
      projectId,
      status,
      creator
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_ISSUES_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_ISSUES_SUCCESS, { issues: result }));
      },
      err => {
        dispatch(updateStatus(LIST_ISSUES_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createIssue = ({ subject, projectId, typeTask, userRelate, content, estimate, files, assignee }) => {
  const formData = new FormData();
  formData.append('action', 'create_issue');
  formData.append('subject', subject);
  formData.append('projectId', projectId);
  formData.append('typeTask', typeTask);
  userRelate && Array.isArray(userRelate) && formData.append('userRelate', JSON.stringify(userRelate));
  assignee && formData.append('assignee', assignee);
  content && formData.append('content', content);
  estimate && formData.append('estimate', estimate);
  if (files) {
    for (const file of files) {
      formData.append('files', file, file.name);
    }
  }

  return dispatch => {
    dispatch(updateStatus(CREATE_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultFormDataConfig(formData),
      null,
      result => {
        dispatch(updateStatus(CREATE_ISSUE_SUCCESS, { createdIssue: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_ISSUE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateIssue = updateIssueData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_issue',
      issueId: updateIssueData.id,
      subject: updateIssueData.subject,
      content: updateIssueData.content,
      userRelate: updateIssueData.userRelate,
      status: updateIssueData.status,
      estimate: updateIssueData.estimate,
      assignee: updateIssueData.assignee,
      typeTask: updateIssueData.typeTask
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPDATE_ISSUE_SUCCESS, {
            updatedIssue: data,
            newStatus: updateIssueData.status || null
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeIssue = removeIssueData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_issue',
      issueId: removeIssueData.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_ISSUE_SUCCESS, { removedIssue: removeIssueData }));
      },
      err => {
        dispatch(updateStatus(REMOVE_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const statisticalIssueOfProject = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'statical_status_of_issue',
      projectId: params.projectId
    })
  };
  return dispatch => {
    dispatch(updateStatus(STATISTICAL_ISSUES_OF_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(STATISTICAL_ISSUES_OF_PROJECT_SUCCESS, { result: data }));
      },
      err => {
        dispatch(updateStatus(STATISTICAL_ISSUES_OF_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const handleFilterBySearch = subject => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_SEARCH_SUBJECT, { subject }));
  };
};

export const handleFilterByStatus = status => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_STATUS, { status: status && status.value }));
  };
};

export const handleFilterByCreator = creator => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_CREATOR, { creator: creator && creator.value }));
  };
};

export const handleFilterByLimit = limit => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_LIMIT, { limit }));
  };
};

export const clearFilterSearch = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FILTER_SEARCH));
  };
};

export const clearFilterStatus = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FILTER_STATUS));
  };
};

export const clearFilterCreator = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FILTER_CREATOR));
  };
};

export const clearFilterLimit = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FILTER_LIMIT));
  };
};

export const clearFilterPage = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FILTER_PAGE));
  };
};

export const handleFilterByPage = page => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_ISSUE_PAGE, { page }));
  };
};

export const createMultipleIssue = ({ data, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_multiple_issue',
      data,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_MULTIPLE_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(CREATE_MULTIPLE_ISSUE_SUCCESS, {
            multipleIssues: result
          })
        );
      },
      err => {
        dispatch(updateStatus(CREATE_MULTIPLE_ISSUE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const closeAllIssues = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'close_all_issue',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(CLOSE_ALL_ISSUES_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(CLOSE_ALL_ISSUES_SUCCESS));
      },
      err => {
        dispatch(updateStatus(CLOSE_ALL_ISSUES_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getAttachmentsOfIssue = ({ issueId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_attachment_by_issue',
      issueId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_ATTACHMENT_OF_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_LIST_ATTACHMENT_OF_ISSUE_SUCCESS, {
            attachmentsOfIssue: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_ATTACHMENT_OF_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const uploadFileToIssue = ({ files, issueId }) => {
  const formData = new FormData();
  formData.append('action', 'add_attachment_by_issue');
  formData.append('issueId', issueId);
  for (const file of files) {
    formData.append('files', file, file.name);
  }
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      // 'Content-Type': 'multipart/form-data',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(UPLOAD_ATTACHMENT_TO_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPLOAD_ATTACHMENT_TO_ISSUE_SUCCESS, {
            attachmentsOfIssue: data
          })
        );
      },
      err => {
        dispatch(updateStatus(UPLOAD_ATTACHMENT_TO_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreAttachmentsOfIssue = ({ issueId, limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_attachment_by_issue',
      issueId,
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_ATTACHMENT_OF_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_ATTACHMENT_OF_ISSUE_SUCCESS, {
            attachmentsOfIssue: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_ATTACHMENT_OF_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAttachmentOfIssue = ({ attachmentId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_attachment_by_issue',
      attachmentId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_ATTACHMENT_OF_ISSUE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(REMOVE_ATTACHMENT_OF_ISSUE_SUCCESS, {
            attachmentOfIssue: data
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_OF_ISSUE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearIssue = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ISSUE));
  };
};

export const clearCurrentIssue = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_CURRENT_ISSUE));
  };
};
