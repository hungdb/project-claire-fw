/*
 *=================================================================================================
 * Sale Actions
 *=================================================================================================
 */
import {
  CREATE_SALE_PRIORITY_FAILED,
  CREATE_SALE_PRIORITY_PROGRESS,
  CREATE_SALE_PRIORITY_SUCCESS,
  UPDATE_SALE_PRIORITY_FAILED,
  UPDATE_SALE_PRIORITY_PROGRESS,
  UPDATE_SALE_PRIORITY_SUCCESS,
  GET_LIST_SALE_PRIORITY_FAILED,
  GET_LIST_SALE_PRIORITY_PROGRESS,
  GET_LIST_SALE_PRIORITY_SUCCESS,
  REMOVE_SALE_PRIORITY_FAILED,
  REMOVE_SALE_PRIORITY_PROGRESS,
  REMOVE_SALE_PRIORITY_SUCCESS,
  LIST_SALE_CATEGORY_PROGRESS,
  LIST_SALE_CATEGORY_SUCCESS,
  LIST_SALE_CATEGORY_FAILED,
  REMOVE_SALE_CATEGORY_PROGRESS,
  REMOVE_SALE_CATEGORY_SUCCESS,
  REMOVE_SALE_CATEGORY_FAILED,
  CREATE_SALE_CATEGORY_PROGRESS,
  CREATE_SALE_CATEGORY_SUCCESS,
  CREATE_SALE_CATEGORY_FAILED,
  UPDATE_SALE_CATEGORY_PROGRESS,
  UPDATE_SALE_CATEGORY_SUCCESS,
  UPDATE_SALE_CATEGORY_FAILED,
  CLEAR_SALE_SETTING,
  LIST_SALE_STATUS_PROGRESS,
  LIST_SALE_STATUS_SUCCESS,
  LIST_SALE_STATUS_FAILED,
  REMOVE_SALE_STATUS_PROGRESS,
  REMOVE_SALE_STATUS_SUCCESS,
  REMOVE_SALE_STATUS_FAILED,
  CREATE_SALE_STATUS_PROGRESS,
  CREATE_SALE_STATUS_SUCCESS,
  CREATE_SALE_STATUS_FAILED,
  UPDATE_SALE_STATUS_PROGRESS,
  UPDATE_SALE_STATUS_SUCCESS,
  UPDATE_SALE_STATUS_FAILED,
  LIST_SALE_PROGRESS,
  LIST_SALE_SUCCESS,
  LIST_SALE_FAILED,
  REMOVE_SALE_PROGRESS,
  REMOVE_SALE_SUCCESS,
  REMOVE_SALE_FAILED,
  CREATE_SALE_PROGRESS,
  CREATE_SALE_SUCCESS,
  CREATE_SALE_FAILED,
  UPDATE_SALE_PROGRESS,
  UPDATE_SALE_SUCCESS,
  UPDATE_SALE_FAILED,
  ADD_ATTACHMENT_BY_SALE_PROGRESS,
  ADD_ATTACHMENT_BY_SALE_SUCCESS,
  ADD_ATTACHMENT_BY_SALE_FAILED,
  REMOVE_ATTACHMENT_BY_SALE_PROGRESS,
  REMOVE_ATTACHMENT_BY_SALE_SUCCESS,
  REMOVE_ATTACHMENT_BY_SALE_FAILED,
  LIST_ATTACHMENT_BY_SALE_PROGRESS,
  LIST_ATTACHMENT_BY_SALE_SUCCESS,
  LIST_ATTACHMENT_BY_SALE_FAILED,
  GET_SIGNED_URL_SALE_PROGRESS,
  GET_SIGNED_URL_SALE_SUCCESS,
  GET_SIGNED_URL_SALE_FAILED
} from '../07-constants/sale';
import { updateStatus, getTokenBearer, calculateLength } from '../08-helpers/common';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { getEnv } from '../env';
import { CHECK_ERROR } from '../07-constants/common';

export const getListSalePriority = () => {
  return dispatch => {
    dispatch(updateStatus(GET_LIST_SALE_PRIORITY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_sale_priority'
      }),
      null,
      result => {
        const data = calculateLength(result) ? result.sort((a, b) => a.ordinalNumber - b.ordinalNumber) : [];
        dispatch(updateStatus(GET_LIST_SALE_PRIORITY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_LIST_SALE_PRIORITY_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSalePriority = (name, ordinalNumber) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_sale_priority',
      name,
      ordinalNumber
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_SALE_PRIORITY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_SALE_PRIORITY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_SALE_PRIORITY_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeSalePriority = salePriorityId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_SALE_PRIORITY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_sale_priority',
        salePriorityId
      }),
      null,
      data => {
        dispatch(updateStatus(REMOVE_SALE_PRIORITY_SUCCESS, { salePriorityId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_SALE_PRIORITY_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSalePriority = (salePriorityId, name, ordinalNumber) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_SALE_PRIORITY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_sale_priority',
        salePriorityId,
        name,
        ordinalNumber
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_SALE_PRIORITY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_SALE_PRIORITY_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

/*
 *==============================================================================
 * Sale Category
 *==============================================================================
 */
export const listSaleCategory = () => {
  return dispatch => {
    dispatch(updateStatus(LIST_SALE_CATEGORY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_sale_category'
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_SALE_CATEGORY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_SALE_CATEGORY_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSaleCategory = ({ name }) => {
  return dispatch => {
    dispatch(updateStatus(CREATE_SALE_CATEGORY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'create_sale_category',
        name
      }),
      null,
      data => {
        dispatch(updateStatus(CREATE_SALE_CATEGORY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_SALE_CATEGORY_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSaleCategory = ({ id, name }) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_SALE_CATEGORY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_sale_category',
        saleId: id,
        name
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_SALE_CATEGORY_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_SALE_CATEGORY_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeSaleCategory = saleCategoryId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_SALE_CATEGORY_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_sale_category',
        saleId: saleCategoryId
      }),
      null,
      () => {
        dispatch(updateStatus(REMOVE_SALE_CATEGORY_SUCCESS, { saleCategoryId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_SALE_CATEGORY_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearSaleSetting = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SALE_SETTING, null));
  };
};

/*
 *==============================================================================
 * Sale status
 *==============================================================================
 */
export const listSaleStatus = () => {
  return dispatch => {
    dispatch(updateStatus(LIST_SALE_STATUS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_sale_status'
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_SALE_STATUS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_SALE_STATUS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSaleStatus = ({ name }) => {
  return dispatch => {
    dispatch(updateStatus(CREATE_SALE_STATUS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'create_sale_status',
        name
      }),
      null,
      data => {
        dispatch(updateStatus(CREATE_SALE_STATUS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_SALE_STATUS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSaleStatus = ({ id, name }) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_SALE_STATUS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_sale_status',
        statusId: id,
        name
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_SALE_STATUS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_SALE_STATUS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeSaleStatus = saleStatusId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_SALE_STATUS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_sale_status',
        statusId: saleStatusId
      }),
      null,
      () => {
        dispatch(updateStatus(REMOVE_SALE_STATUS_SUCCESS, { saleStatusId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_SALE_STATUS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
/*
 *==============================================================================
 * Sale
 *==============================================================================
 */
export const listSale = ({ limit, page, categoryId, priorityId, statusId, pic, start, end, keywords, customerId }) => {
  return dispatch => {
    dispatch(updateStatus(LIST_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_sale',
        limit,
        page,
        categoryId,
        priorityId,
        statusId,
        pic,
        start,
        end,
        keywords,
        customerId
      }),
      null,
      (result = {}) => {
        dispatch(
          updateStatus(LIST_SALE_SUCCESS, {
            sales: result.data,
            total: result.total
          })
        );
      },
      err => {
        dispatch(updateStatus(LIST_SALE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSale = payload => {
  const formData = new FormData();
  formData.append('action', 'create_sale');
  const key = [
    'categoryId',
    'priorityId',
    'statusId',
    'pic',
    'company',
    'appointmentStartDate',
    'appointmentPlace',
    'discussionContent',
    'memoMeeting',
    'memoProgress',
    'otherMemo',
    'contact',
    'appointmentEndDate',
    'note',
    'customerId'
  ];
  key.forEach(item => {
    formData.append(item, payload[item]);
  });
  payload.files.forEach(file => {
    formData.append('files', file, file.name);
  });
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_SALE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_SALE_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSale = payload => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_sale',
        saleId: payload.id,
        categoryId: payload.categoryId,
        priorityId: payload.priorityId,
        statusId: payload.statusId,
        pic: payload.pic,
        company: payload.company,
        appointmentStartDate: payload.appointmentStartDate,
        appointmentPlace: payload.appointmentPlace,
        discussionContent: payload.discussionContent,
        memoMeeting: payload.memoMeeting,
        memoProgress: payload.memoProgress,
        otherMemo: payload.otherMemo,
        contact: payload.contact,
        appointmentEndDate: payload.appointmentEndDate,
        note: payload.note,
        customerId: payload.customerId
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_SALE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeSale = saleId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_sale',
        saleId
      }),
      null,
      () => {
        dispatch(updateStatus(REMOVE_SALE_SUCCESS, { saleId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addAttachmentBySale = (saleId, files, userId) => {
  const formData = new FormData();
  formData.append('action', 'add_attachment_by_sale');
  formData.append('saleId', saleId);
  files.forEach(file => {
    formData.append('files', file, file.name);
  });
  formData.append('userId', userId);
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };
  return dispatch => {
    dispatch(updateStatus(ADD_ATTACHMENT_BY_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_ATTACHMENT_BY_SALE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_ATTACHMENT_BY_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listAttachmentBySale = (saleId, offset, limit) => {
  return dispatch => {
    dispatch(updateStatus(LIST_ATTACHMENT_BY_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_attachment_by_sale',
        saleId,
        offset,
        limit
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_ATTACHMENT_BY_SALE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_ATTACHMENT_BY_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAttachmentBySale = attachmentId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_ATTACHMENT_BY_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_attachment_by_sale',
        attachmentId
      }),
      null,
      data => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_BY_SALE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_BY_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const downloadSaleAttachments = filePath => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_signed_url_sale',
      filePath
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_SIGNED_URL_SALE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_SIGNED_URL_SALE_SUCCESS, { url: data }));
        window.open(data, '_blank');
      },
      err => {
        dispatch(updateStatus(GET_SIGNED_URL_SALE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
