/*
 *=================================================================================================
 * Admin Allocation Actions
 *=================================================================================================
 */
import {
  GET_ALLOCATIONS_USERS,
  GET_ALLOCATIONS_USERS_SUCCESS,
  GET_ALLOCATIONS_USERS_FAILED
} from '../07-constants/admin-allocations';
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';

export const getAllocations = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_allocations_of_users'
    })
  };

  const fixDataAllocations = skillGroups =>
    skillGroups.map(skillGroup => ({
      ...skillGroup,
      users: skillGroup.users.map(user => ({
        ...user,
        allocations: user.allocations.map(allocation => ({
          ...allocation,
          startDate: new Date(
            Date.UTC(
              new Date(allocation.startDate).getUTCFullYear(),
              new Date(allocation.startDate).getUTCMonth(),
              new Date(allocation.startDate).getUTCDate(),
              0,
              0,
              0
            )
          ),
          endDate: new Date(
            Date.UTC(
              new Date(allocation.endDate).getUTCFullYear(),
              new Date(allocation.endDate).getUTCMonth(),
              new Date(allocation.endDate).getUTCDate(),
              23,
              59,
              59
            )
          )
        }))
      }))
    }));

  return dispatch => {
    dispatch(updateStatus(GET_ALLOCATIONS_USERS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      // callApi('https://api.project.bap.jp/api/rpc', config, null,
      result => {
        const skills = [];
        const projects = [];

        result.forEach(r => {
          skills.push({ value: r.skill.name, label: r.skill.name });
          r.users.forEach(u =>
            u.allocations.forEach(a => {
              if (!projects.some(p => p.value === a.project)) {
                projects.push({ value: a.project, label: a.project });
              }
            })
          );
        });
        dispatch({
          type: GET_ALLOCATIONS_USERS_SUCCESS,
          allocations: fixDataAllocations(result),
          skills,
          projects
        });
      },
      error => {
        dispatch({ type: GET_ALLOCATIONS_USERS_FAILED, error });
        dispatch(updateStatus(CHECK_ERROR, { error }));
      }
    );
  };
};
