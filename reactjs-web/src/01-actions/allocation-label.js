/*
 *=================================================================================================
 * Allocation Label Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_ALL_ALLOCATION_LABELS_PROGRESS,
  GET_ALL_ALLOCATION_LABELS_FAILED,
  GET_ALL_ALLOCATION_LABELS_SUCCESS,
  ADD_ALLOCATION_LABEL_PROGRESS,
  ADD_ALLOCATION_LABEL_SUCCESS,
  ADD_ALLOCATION_LABEL_FAILED,
  UPDATE_ALLOCATION_LABEL_PROGRESS,
  UPDATE_ALLOCATION_LABEL_SUCCESS,
  UPDATE_ALLOCATION_LABEL_FAILED,
  REMOVE_ALLOCATION_LABEL_PROGRESS,
  REMOVE_ALLOCATION_LABEL_SUCCESS,
  REMOVE_ALLOCATION_LABEL_FAILED,
  CLEAR_ALLOCATION_LABELS
} from '../07-constants/allocation-label';

export const getAllocationLabels = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_allocation_labels'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ALL_ALLOCATION_LABELS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_ALL_ALLOCATION_LABELS_SUCCESS, {
            allocationLabels: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_ALL_ALLOCATION_LABELS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createAllocationLabel = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_allocation_label',
      name: data.name
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ALLOCATION_LABEL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_ALLOCATION_LABEL_SUCCESS, { allocationLabel: data }));
      },
      err => {
        dispatch(updateStatus(ADD_ALLOCATION_LABEL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateAllocationLabel = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_allocation_label',
      labelId: data.id,
      name: data.name
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ALLOCATION_LABEL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(UPDATE_ALLOCATION_LABEL_SUCCESS, {
            allocationLabel: data
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_ALLOCATION_LABEL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAllocationLabel = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_allocation_label',
      labelId: data.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ALLOCATION_LABEL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_ALLOCATION_LABEL_SUCCESS, {
            allocationLabel: data
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_ALLOCATION_LABEL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearAllocationLabels = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ALLOCATION_LABELS));
  };
};
