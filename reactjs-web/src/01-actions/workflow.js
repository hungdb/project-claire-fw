/*
 *=================================================================================================
 * Workflow Actions
 *=================================================================================================
 */
import i18n from '../i18n';
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import { toast } from 'react-toastify';
import { TOAST_ERROR_STYLE, TOAST_SUCCESS_STYLE } from '../05-utils/commonData';
import {
  GET_WORKFLOW_PROGRESS,
  GET_WORKFLOW_SUCCESS,
  GET_WORKFLOW_FAILED,
  CREATE_WORKFLOW_PROGRESS,
  CREATE_WORKFLOW_SUCCESS,
  CREATE_WORKFLOW_FAILED,
  UPDATE_WORKFLOW_PROGRESS,
  UPDATE_WORKFLOW_SUCCESS,
  UPDATE_WORKFLOW_FAILED,
  REMOVE_WORKFLOW_PROGRESS,
  REMOVE_WORKFLOW_SUCCESS,
  REMOVE_WORKFLOW_FAILED,
  GET_WORKFLOW_DETAIL_PROGRESS,
  GET_WORKFLOW_DETAIL_SUCCESS,
  GET_WORKFLOW_DETAIL_FAILED,
  GET_WORKFLOW_STATE_PROGRESS,
  GET_WORKFLOW_STATE_SUCCESS,
  GET_WORKFLOW_STATE_FAILED,
  REMOVE_WORKFLOW_DETAIL_PROGRESS,
  REMOVE_WORKFLOW_DETAIL_SUCCESS,
  REMOVE_WORKFLOW_DETAIL_FAILED,
  ADD_WORKFLOW_DETAIL_PROGRESS,
  ADD_WORKFLOW_DETAIL_SUCCESS,
  ADD_WORKFLOW_DETAIL_FAILED,
  ADD_WORKFLOW_STATE_PROGRESS,
  ADD_WORKFLOW_STATE_SUCCESS,
  ADD_WORKFLOW_STATE_FAILED,
  WORKFLOW_STATE_UPDATE_PROGRESS,
  WORKFLOW_STATE_UPDATE_SUCCESS,
  WORKFLOW_STATE_UPDATE_FAILED,
  WORKFLOW_STATE_DELETE_PROGRESS,
  WORKFLOW_STATE_DELETE_SUCCESS,
  WORKFLOW_STATE_DELETE_FAILED,
  WORKFLOW_DETAIL_UPDATE,
  WORKFLOW_DETAIL_UPDATE_SUCCESS,
  WORKFLOW_DETAIL_UPDATE_FAILED,
  WORKFLOW_MIGRATE_PROGRESS,
  WORKFLOW_MIGRATE_SUCCESS,
  WORKFLOW_MIGRATE_FAILED,
  GET_WORKFLOW_FOR_MIGRATE_PROGRESS,
  GET_WORKFLOW_FOR_MIGRATE_SUCCESS,
  GET_WORKFLOW_FOR_MIGRATE_FAILED,
  GET_WORKFLOW_STATE_FOR_MIGRATE_PROGRESS,
  GET_WORKFLOW_STATE_FOR_MIGRATE_SUCCESS,
  GET_WORKFLOW_STATE_FOR_MIGRATE_FAILED,
  CLEAR_WORKFLOW_STATES_FOR_MIGRATE
} from '../07-constants/workflow';

export const getWorkflows = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_WORKFLOW_SUCCESS, { workflows: data }));
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createWorkflow = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_workflow',
      name: data.name,
      allowParallelTask: data.allowParallelTask
    })
  };
  return dispatch => {
    dispatch(updateStatus(CREATE_WORKFLOW_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_WORKFLOW_SUCCESS, { workflow: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_WORKFLOW_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateWorkflow = updateWorkflowData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_workflow',
      workflowId: updateWorkflowData.id,
      name: updateWorkflowData.name,
      allowParallelTask: updateWorkflowData.allowParallelTask
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_WORKFLOW_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(UPDATE_WORKFLOW_SUCCESS, {
            updateWorkflow: updateWorkflowData
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_WORKFLOW_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeWorkflow = removeWorkflowData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_workflow',
      workflowId: removeWorkflowData.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_WORKFLOW_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_WORKFLOW_SUCCESS, {
            removeWorkflow: removeWorkflowData
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_WORKFLOW_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getWorkflow = workflowId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_workflow',
      workflowId: workflowId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_DETAIL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_WORKFLOW_DETAIL_SUCCESS, { workflowDetail: data }));
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_DETAIL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getWorkflowStates = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow_state_by_workflowId',
      workflowId: params.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_STATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_WORKFLOW_STATE_SUCCESS, { listState: data }));
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_STATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeWorkflowDetail = removeWorkflowDetailData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_workflow_detail',
      workflowDetailId: removeWorkflowDetailData.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_WORKFLOW_DETAIL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_WORKFLOW_DETAIL_SUCCESS, {
            removeWorkflowDetailData: removeWorkflowDetailData
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_WORKFLOW_DETAIL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createWorkflowDetail = createWorkflowDetailData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_workflow_detail',
      workflowId: createWorkflowDetailData.workflowId,
      fromState: createWorkflowDetailData.fromState,
      toState: createWorkflowDetailData.toState,
      description: createWorkflowDetailData.description ? createWorkflowDetailData.description : ''
    })
  };
  return dispatch => {
    dispatch(updateStatus(ADD_WORKFLOW_DETAIL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_WORKFLOW_DETAIL_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_WORKFLOW_DETAIL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateWorkflowDetail = updateWorkflowDetailData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_workflow_detail',
      workflowDetailId: updateWorkflowDetailData.currentWorkflowTransitionId,
      workflowId: updateWorkflowDetailData.workflowId,
      fromState: updateWorkflowDetailData.fromState,
      toState: updateWorkflowDetailData.toState,
      description: updateWorkflowDetailData.description ? updateWorkflowDetailData.description : 'test'
    })
  };
  return dispatch => {
    dispatch(updateStatus(WORKFLOW_DETAIL_UPDATE));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(WORKFLOW_DETAIL_UPDATE_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(WORKFLOW_DETAIL_UPDATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createWorkflowState = createWorkflowStateData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_workflow_state',
      workflowId: createWorkflowStateData.workflowId,
      name: createWorkflowStateData.name,
      effortType: createWorkflowStateData.type,
      ordinalNumber: createWorkflowStateData.ordinal
    })
  };
  return dispatch => {
    dispatch(updateStatus(ADD_WORKFLOW_STATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(ADD_WORKFLOW_STATE_SUCCESS, {
            createWorkflowStateData: data
          })
        );
      },
      err => {
        dispatch(updateStatus(ADD_WORKFLOW_STATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateWorkflowState = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_workflow_state',
      workflowId: data.workflowId,
      workflowStateId: data.id,
      name: data.name,
      effortType: data.type,
      ordinalNumber: data.ordinal
    })
  };
  return dispatch => {
    dispatch(updateStatus(WORKFLOW_STATE_UPDATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(WORKFLOW_STATE_UPDATE_SUCCESS, {
            updateWorkflowStateData: data
          })
        );
      },
      err => {
        dispatch(updateStatus(WORKFLOW_STATE_UPDATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const deleteWorkflowState = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_workflow_state',
      workflowStateId: data.id
    })
  };
  return dispatch => {
    dispatch(updateStatus(WORKFLOW_STATE_DELETE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(WORKFLOW_STATE_DELETE_SUCCESS, {
            deleteWorkflowStateData: data
          })
        );
      },
      err => {
        dispatch(updateStatus(WORKFLOW_STATE_DELETE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const workflowMigrate = data => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'workflow_migrate',
      projectId: data.projectId,
      workflowId: data.workflowId,
      workflowStateIds: data.workflowStateIds
    })
  };
  return dispatch => {
    dispatch(updateStatus(WORKFLOW_MIGRATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(WORKFLOW_MIGRATE_SUCCESS, { result: result }));
        toast.success(i18n.t('Workflow migration had been successfully'), TOAST_SUCCESS_STYLE);
      },
      err => {
        dispatch(updateStatus(WORKFLOW_MIGRATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
        toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
      }
    );
  };
};

export const getWorkflowsForMigrate = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_FOR_MIGRATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_WORKFLOW_FOR_MIGRATE_SUCCESS, {
            workflowsForMigrate: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_FOR_MIGRATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getWorkflowStatesForMigrate = ({ id, type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow_state_by_workflowId',
      workflowId: id
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_STATE_FOR_MIGRATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_WORKFLOW_STATE_FOR_MIGRATE_SUCCESS, {
            listStateForMigrate: data,
            type
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_STATE_FOR_MIGRATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearWorkflowStatesForMigrate = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_WORKFLOW_STATES_FOR_MIGRATE));
  };
};
