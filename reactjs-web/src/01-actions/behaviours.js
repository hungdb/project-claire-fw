/*
 *=================================================================================================
 * Behaviour Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_LIST_OF_BEHAVIOURS_INPROGRESS,
  GET_LIST_OF_BEHAVIOURS_SUCCESS,
  GET_LIST_OF_BEHAVIOURS_FAILED,
  CREATE_BEHAVIOURS_INPROGRESS,
  CREATE_BEHAVIOURS_SUCCESS,
  CREATE_BEHAVIOURS_FAILED,
  UPDATE_BEHAVIOURS_INPROGRESS,
  UPDATE_BEHAVIOURS_SUCCESS,
  UPDATE_BEHAVIOURS_FAILED,
  DELETE_BEHAVIOURS_INPROGRESS,
  DELETE_BEHAVIOURS_SUCCESS,
  DELETE_BEHAVIOURS_FAILED,
  GET_LIST_KPI_BEHAVIOUR_INPROGRESS,
  GET_LIST_KPI_BEHAVIOUR_SUCCESS,
  GET_LIST_KPI_BEHAVIOUR_FAILED,
  CREATE_KPI_BEHAVIOUR_INPROGRESS,
  CREATE_KPI_BEHAVIOUR_SUCCESS,
  CREATE_KPI_BEHAVIOUR_FAILED,
  REMOVE_KPI_BEHAVIOUR_INPROGRESS,
  REMOVE_KPI_BEHAVIOUR_SUCCESS,
  REMOVE_KPI_BEHAVIOUR_FAILED,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_INPROGRESS,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_SUCCESS,
  CREATE_MULTIPLE_KPI_BEHAVIOUR_FAILED,
  CLEAR_BEHAVIOURS,
  CLEAR_KPI_BEHAVIOUR
} from '../07-constants/behaviours';

export const getBehaviours = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_behaviors'
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_OF_BEHAVIOURS_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(GET_LIST_OF_BEHAVIOURS_SUCCESS, {
            listBehaviour: result
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_OF_BEHAVIOURS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createBehaviour = ({
  activity,
  point,
  typeApply,
  typeAction,
  isApply,
  startDate,
  endDate,
  description,
  typeSubApply
}) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_behavior',
      activity,
      point,
      typeApply,
      typeAction,
      isApply,
      startDate,
      endDate,
      description,
      typeSubApply
    })
  };
  return dispatch => {
    dispatch(updateStatus(CREATE_BEHAVIOURS_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_BEHAVIOURS_SUCCESS, { behaviour: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_BEHAVIOURS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateBehaviour = ({
  behaviorId,
  activity,
  point,
  typeApply,
  typeAction,
  isApply,
  startDate,
  endDate,
  description,
  typeSubApply
}) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_behavior',
      behaviorId,
      activity,
      point,
      typeApply,
      typeAction,
      isApply,
      startDate,
      endDate,
      description,
      typeSubApply
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_BEHAVIOURS_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(UPDATE_BEHAVIOURS_SUCCESS, { behaviourUpdate: result }));
      },
      err => {
        dispatch(updateStatus(UPDATE_BEHAVIOURS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeBehaviour = ({ behaviorId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_behavior',
      behaviorId
    })
  };
  return dispatch => {
    dispatch(updateStatus(DELETE_BEHAVIOURS_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(DELETE_BEHAVIOURS_SUCCESS, {
            behaviourRemove: behaviorId
          })
        );
      },
      err => {
        dispatch(updateStatus(DELETE_BEHAVIOURS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListKpiBehavior = ({ userId, date, limit, page }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_kpi_behavior',
      userId,
      date,
      limit,
      page
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_KPI_BEHAVIOUR_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(GET_LIST_KPI_BEHAVIOUR_SUCCESS, {
            listKpiBehaviour: result
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_KPI_BEHAVIOUR_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createKPIBehaviour = ({ userId, behaviors, evaluatedAt }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_kpi_behavior',
      userId,
      behaviors,
      evaluatedAt
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_KPI_BEHAVIOUR_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(CREATE_KPI_BEHAVIOUR_SUCCESS, { kpiBehaviour: result }));
      },
      err => {
        dispatch(updateStatus(CREATE_KPI_BEHAVIOUR_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeKPIBehaviour = ({ kpiBehaviorId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_kpi_behavior',
      kpiBehaviorId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_KPI_BEHAVIOUR_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_KPI_BEHAVIOUR_SUCCESS, { kpiBehaviorId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_KPI_BEHAVIOUR_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createMultipleKPIBehaviour = ({ data }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_multiple_kpi_behavior',
      data
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_MULTIPLE_KPI_BEHAVIOUR_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(CREATE_MULTIPLE_KPI_BEHAVIOUR_SUCCESS, {
            multipleKpiBehaviours: result.result
          })
        );
      },
      err => {
        dispatch(updateStatus(CREATE_MULTIPLE_KPI_BEHAVIOUR_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearBehaviours = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_BEHAVIOURS));
  };
};

export const clearKPIBehaviour = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_KPI_BEHAVIOUR));
  };
};
