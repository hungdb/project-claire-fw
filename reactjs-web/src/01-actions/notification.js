/*
 *=================================================================================================
 * Notification Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_NOTIFICATIONS_PROGRESS,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAIL,
  LOAD_MORE_NOTIFICATIONS_PROGRESS,
  LOAD_MORE_NOTIFICATIONS_SUCCESS,
  LOAD_MORE_NOTIFICATIONS_FAILED,
  ADD_NOTIFICATION,
  UPDATE_NOTIFICATION,
  UPDATE_NOTIFICATION_SUCCESS,
  UPDATE_NOTIFICATION_FAILED,
  READ_ALL_NOTIFICATIONS
} from '../07-constants/notification';

export const getMyNotifications = ({ limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_my_notifications',
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_NOTIFICATIONS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_NOTIFICATIONS_SUCCESS, {
            notifications: data.rows,
            total: data.count,
            totalNotRead: data.totalNotRead
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_NOTIFICATIONS_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const loadMoreMyNotifications = ({ limit, offset }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_my_notifications',
      limit,
      offset
    })
  };

  return dispatch => {
    dispatch(updateStatus(LOAD_MORE_NOTIFICATIONS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(LOAD_MORE_NOTIFICATIONS_SUCCESS, {
            notifications: data.rows,
            total: data.count
          })
        );
      },
      err => {
        dispatch(updateStatus(LOAD_MORE_NOTIFICATIONS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addNotification = ({ type, data }) => {
  return dispatch => {
    dispatch(updateStatus(ADD_NOTIFICATION, { type, data }));
  };
};

export const readAllNotifications = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'read_all_notifications'
    })
  };
  return dispatch => {
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(READ_ALL_NOTIFICATIONS));
      },
      err => {
        dispatch(updateStatus(UPDATE_NOTIFICATION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const readNotification = notifyId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'read_notification',
      notificationId: notifyId
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_NOTIFICATION));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch({ type: UPDATE_NOTIFICATION_SUCCESS, notifyId });
      },
      err => {
        dispatch(updateStatus(UPDATE_NOTIFICATION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
