/*
 *=================================================================================================
 * Setting Actions
 *=================================================================================================
 */
import {
  ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED,
  ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS,
  ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS,
  GET_STATUS_OF_ALERT_SYSTEM_FAILED,
  GET_STATUS_OF_ALERT_SYSTEM_PROGRESS,
  GET_STATUS_OF_ALERT_SYSTEM_SUCCESS
} from '../07-constants/setting';
import { getTokenBearer, updateStatus } from '../08-helpers/common';
import { callApi } from '../05-utils/commonUtils';
import { getEnv } from '../env';
import { CHECK_ERROR } from '../07-constants/common';

export const activeInactiveAlertSystem = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'active_inactive_alert_system'
    })
  };

  return dispatch => {
    dispatch(updateStatus(ACTIVE_INACTIVE_ALERT_SYSTEM_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(ACTIVE_INACTIVE_ALERT_SYSTEM_SUCCESS, {
            state: result.status
          })
        );
      },
      err => {
        dispatch(updateStatus(ACTIVE_INACTIVE_ALERT_SYSTEM_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getStatusOfAlertSystem = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_state_of_alert_system'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_STATUS_OF_ALERT_SYSTEM_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(GET_STATUS_OF_ALERT_SYSTEM_SUCCESS, {
            state: result.status
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_STATUS_OF_ALERT_SYSTEM_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
