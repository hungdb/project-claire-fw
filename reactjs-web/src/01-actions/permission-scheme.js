/*
 *=================================================================================================
 * Permission Schema Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, checkAccessToken, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_PERMISSION_SCHEMES_PROGRESS,
  GET_PERMISSION_SCHEMES_SUCCESS,
  GET_PERMISSION_SCHEMES_FAILED,
  GET_PROJECT_ROLES_PROGRESS,
  GET_PROJECT_ROLES_SUCCESS,
  GET_PROJECT_ROLES_FAILED,
  GET_PERMISSION_SCHEME_DETAIL_PROGRESS,
  GET_PERMISSION_SCHEME_DETAIL_SUCCESS,
  GET_PERMISSION_SCHEME_DETAIL_FAILED,
  GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS,
  GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS,
  GET_PERMISSION_OF_PROJECT_ROLE_FAILED,
  CREATE_PERMISSION_SCHEME_PROGRESS,
  CREATE_PERMISSION_SCHEME_SUCCESS,
  CREATE_PERMISSION_SCHEME_FAILED,
  UPDATE_PERMISSION_SCHEME_PROGRESS,
  UPDATE_PERMISSION_SCHEME_SUCCESS,
  UPDATE_PERMISSION_SCHEME_FAILED,
  CREATE_PROJECT_ROLE_PROGRESS,
  CREATE_PROJECT_ROLE_SUCCESS,
  CREATE_PROJECT_ROLE_FAILED,
  UPDATE_PROJECT_ROLE_PROGRESS,
  UPDATE_PROJECT_ROLE_SUCCESS,
  UPDATE_PROJECT_ROLE_FAILED,
  // GET_ALL_PERMISSION_PROGRESS,
  // GET_ALL_PERMISSION_SUCCESS,
  // GET_ALL_PERMISSION_FAILED,
  CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  CREATE_PERMISSION_SCHEME_DETAIL_FAILED,
  REMOVE_PERMISSION_SCHEME_PROGRESS,
  REMOVE_PERMISSION_SCHEME_SUCCESS,
  REMOVE_PERMISSION_SCHEME_FAILED,
  REMOVE_PROJECT_ROLE_PROGRESS,
  REMOVE_PROJECT_ROLE_SUCCESS,
  REMOVE_PROJECT_ROLE_FAILED,
  REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  REMOVE_PERMISSION_SCHEME_DETAIL_FAILED,
  UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS,
  UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS,
  UPDATE_PERMISSION_SCHEME_DETAIL_FAILED,
  CLEAR_PERMISSION_SCHEME
} from '../07-constants/permission-scheme';

export const getPermissionSchemes = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_permission_scheme'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PERMISSION_SCHEMES_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_PERMISSION_SCHEMES_SUCCESS, { schemes: data }));
      },
      err => {
        dispatch(updateStatus(GET_PERMISSION_SCHEMES_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createPermissionScheme = data => {
  return dispatch => {
    dispatch(updateStatus(CREATE_PERMISSION_SCHEME_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_permission_scheme',
            name: data.name
          }),
          null,
          res => {
            dispatch(updateStatus(CREATE_PERMISSION_SCHEME_SUCCESS, res));
          },
          err => {
            dispatch(updateStatus(CREATE_PERMISSION_SCHEME_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const getPermissionDetail = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_permission_scheme',
      permissionSchemeId: params.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PERMISSION_SCHEME_DETAIL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_PERMISSION_SCHEME_DETAIL_SUCCESS, {
            permissionSchemeDetail: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_PERMISSION_SCHEME_DETAIL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getProjectRoles = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_project_role'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PROJECT_ROLES_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_PROJECT_ROLES_SUCCESS, { projectRoles: data }));
      },
      err => {
        dispatch(updateStatus(GET_PROJECT_ROLES_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getPermissionsOfProjectRole = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_permission_by_permissionScheme_and_projectRole',
      permissionSchemeId: params.permissionSchemeId,
      projectRoleId: params.projectRoleId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PERMISSION_OF_PROJECT_ROLE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_PERMISSION_OF_PROJECT_ROLE_SUCCESS, {
            permissionsOfProjectRole: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_PERMISSION_OF_PROJECT_ROLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updatePermissionScheme = params => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_PERMISSION_SCHEME_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'update_permission_scheme',
            permissionSchemeId: params.id,
            name: params.name
          }),
          null,
          () => {
            dispatch(
              updateStatus(UPDATE_PERMISSION_SCHEME_SUCCESS, {
                updatePermissionSchemeData: params
              })
            );
          },
          err => {
            dispatch(updateStatus(UPDATE_PERMISSION_SCHEME_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const createProjectRole = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_project_role',
      name: params.name,
      priorityLevel: params.priorityLevel
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_PROJECT_ROLE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(CREATE_PROJECT_ROLE_SUCCESS, {
            createProjectRoleData: data
          })
        );
      },
      err => {
        dispatch(updateStatus(CREATE_PROJECT_ROLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateProjectRole = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_project_role',
      name: params.name,
      projectRoleId: params.id,
      priorityLevel: params.priorityLevel
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_PROJECT_ROLE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(UPDATE_PROJECT_ROLE_SUCCESS, {
            updateProjectRoleData: params
          })
        );
      },
      err => {
        dispatch(updateStatus(UPDATE_PROJECT_ROLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removePermissionScheme = params => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_PERMISSION_SCHEME_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'remove_permission_scheme',
            permissionSchemeId: params.id
          }),
          null,
          () => {
            dispatch(
              updateStatus(REMOVE_PERMISSION_SCHEME_SUCCESS, {
                removePermissionSchemeData: params
              })
            );
          },
          err => {
            dispatch(updateStatus(REMOVE_PERMISSION_SCHEME_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const removeProjectRole = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_project_role',
      projectRoleId: params.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_PROJECT_ROLE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(
          updateStatus(REMOVE_PROJECT_ROLE_SUCCESS, {
            removeProjectRoleData: params
          })
        );
      },
      err => {
        dispatch(updateStatus(REMOVE_PROJECT_ROLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createPermissionSchemeDetail = params => {
  return dispatch => {
    dispatch(updateStatus(CREATE_PERMISSION_SCHEME_DETAIL_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_permission_scheme_detail',
            permissionSchemeId: params.permissionSchemeId,
            projectRoleId: params.projectRoleId,
            permission: params.permission,
            permissionConditions: JSON.stringify(params.permissionConditions || [])
          }),
          null,
          res => {
            dispatch(
              updateStatus(CREATE_PERMISSION_SCHEME_DETAIL_SUCCESS, {
                createPermissionSchemeDetailData: res
              })
            );
          },
          err => {
            dispatch(
              updateStatus(CREATE_PERMISSION_SCHEME_DETAIL_FAILED, {
                error: err
              })
            );
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const updatePermissionSchemeDetail = params => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_PERMISSION_SCHEME_DETAIL_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'update_permission_scheme_detail',
            permissionSchemeDetailId: params.permissionSchemeDetailId,
            permissionSchemeId: params.permissionSchemeId,
            projectRoleId: params.projectRoleId,
            permission: params.permission,
            permissionCondition: JSON.stringify(params.permissionConditions)
          }),
          null,
          res => {
            dispatch(
              updateStatus(UPDATE_PERMISSION_SCHEME_DETAIL_SUCCESS, {
                updatePermissionSchemeDetailData: params
              })
            );
          },
          err => {
            dispatch(
              updateStatus(UPDATE_PERMISSION_SCHEME_DETAIL_FAILED, {
                error: err
              })
            );
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const removePermissionSchemeDetail = params => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_PERMISSION_SCHEME_DETAIL_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'remove_permission_scheme_detail',
            permissionSchemeDetailId: params.permissionSchemeDetailId
          }),
          null,
          res => {
            dispatch(
              updateStatus(REMOVE_PERMISSION_SCHEME_DETAIL_SUCCESS, {
                removePermissionSchemeDetailData: params
              })
            );
          },
          err => {
            dispatch(
              updateStatus(REMOVE_PERMISSION_SCHEME_DETAIL_FAILED, {
                error: err
              })
            );
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const clearPermissionScheme = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_PERMISSION_SCHEME));
  };
};
