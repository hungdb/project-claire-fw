/*
 *=================================================================================================
 * Common Actions
 *=================================================================================================
 */
import { LIGHT_BACKGROUND_TOGGLE, FULL_BOARD_TOGGLE } from '../07-constants/common';
import { updateStatus } from '../08-helpers/common';

export const toggleLight = status => {
  return dispatch => {
    dispatch(updateStatus(LIGHT_BACKGROUND_TOGGLE, { status }));
  };
};

export const toggleFullBoard = status => {
  return dispatch => {
    dispatch(updateStatus(FULL_BOARD_TOGGLE, { status }));
  };
};
