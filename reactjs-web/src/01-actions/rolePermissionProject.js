/*
 *=================================================================================================
 * Role Permission Project Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
// import { TOAST_SUCCESS_STYLE } from '../05-utils/commonData';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
// import { toast } from 'react-toastify';
// import i18n from '../i18n';
import {
  GET_PERMISSION_PROJECT_PROGRESS,
  GET_PERMISSION_PROJECT_SUCCESS,
  GET_PERMISSION_PROJECT_FAILED,
  GET_ALL_PERMISSION_PROJECT_PROGRESS,
  GET_ALL_PERMISSION_PROJECT_SUCCESS,
  GET_ALL_PERMISSION_PROJECT_FAILED,
  ADD_ROLE_PERMISSION_PROJECT_PROGRESS,
  ADD_ROLE_PERMISSION_PROJECT_SUCCESS,
  ADD_ROLE_PERMISSION_PROJECT_FAILED,
  REMOVE_PERMISSION_PROJECT_SUCCESS,
  REMOVE_PERMISSION_PROJECT_FAILED,
  ADD_PERMISSION_PROJECT_PROGRESS,
  ADD_PERMISSION_PROJECT_SUCCESS,
  ADD_PERMISSION_PROJECT_FAILED,
  CREATE_ROLE_PROJECT_PROGRESS,
  CREATE_ROLE_PROJECT_SUCCESS,
  CREATE_ROLE_PROJECT_FAILED,
  UPDATE_ROLE_PROJECT_PROGRESS,
  UPDATE_ROLE_PROJECT_SUCCESS,
  UPDATE_ROLE_PROJECT_FAILED,
  REMOVE_ROLE_PROJECT_PROGRESS,
  REMOVE_ROLE_PROJECT_SUCCESS,
  PROJECT_GET_ROLE_PROJECT_PROGRESS,
  PROJECT_GET_ROLE_PROJECT_SUCCESS,
  PROJECT_GET_ROLE_PROJECT_FAILED
} from '../07-constants/rolePermissionProject';

export const listPermissions = listPerdata => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_permission_of_role_project',
      projectRoleId: listPerdata.roleId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_PERMISSION_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_PERMISSION_PROJECT_SUCCESS, { listPerms: data }));
      },
      err => {
        dispatch(updateStatus(GET_PERMISSION_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listAllPermissions = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_permission_scheme'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ALL_PERMISSION_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_ALL_PERMISSION_PROJECT_SUCCESS, { allPerms: data }));
      },
      err => {
        dispatch(updateStatus(GET_ALL_PERMISSION_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addRolePermission = addPermissionData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_permission_scheme_detail',
      projectRoleId: addPermissionData.roleId,
      permissionSchemeId: addPermissionData.permissionId
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ROLE_PERMISSION_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_ROLE_PERMISSION_PROJECT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(ADD_ROLE_PERMISSION_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeRolePermission = removePermissionData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_permission_scheme_detail',
      permissionSchemeDetailId: removePermissionData.rolePermissionId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_PERMISSION_PROJECT_SUCCESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(REMOVE_PERMISSION_PROJECT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(REMOVE_PERMISSION_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addPermission = name => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_permission_scheme',
      name: name
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_PERMISSION_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_PERMISSION_PROJECT_SUCCESS, { allPerms: data }));
      },
      err => {
        dispatch(updateStatus(ADD_PERMISSION_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createRole = createRoleData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_project_role',
      name: createRoleData.name
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_ROLE_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_ROLE_PROJECT_SUCCESS, { createRoleData: data }));
        // toast.success(i18n.t('Create role had been successfully'), TOAST_SUCCESS_STYLE);
      },
      err => {
        dispatch(updateStatus(CREATE_ROLE_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateRole = updateRoleData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_project_role',
      name: updateRoleData.name,
      projectRoleId: updateRoleData.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ROLE_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(UPDATE_ROLE_PROJECT_SUCCESS, {
            updateRole: updateRoleData
          })
        );
        // toast.success(i18n.t('Update role had been successfully'), TOAST_SUCCESS_STYLE);
      },
      err => {
        dispatch(updateStatus(UPDATE_ROLE_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeRole = removeRoleData => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_project_role',
      projectRoleId: removeRoleData.id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ROLE_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(REMOVE_ROLE_PROJECT_SUCCESS, {
            removeRole: removeRoleData
          })
        );
        // toast.success(i18n.t('Delete role had been successfully'), TOAST_SUCCESS_STYLE);
      },
      err => {
        dispatch(updateStatus(CREATE_ROLE_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getRoles = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_project_role'
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_ROLE_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(PROJECT_GET_ROLE_PROJECT_SUCCESS, { listRoles: data }));
      },
      err => {
        dispatch(updateStatus(PROJECT_GET_ROLE_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
