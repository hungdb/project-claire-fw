/*
 *=================================================================================================
 * Customer Actions
 *=================================================================================================
 */
import {
  CREATE_CUSTOMER_PROGRESS,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_FAIL,
  UPDATE_CUSTOMER_PROGRESS,
  UPDATE_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_FAIL,
  REMOVE_CUSTOMER_PROGRESS,
  REMOVE_CUSTOMER_SUCCESS,
  REMOVE_CUSTOMER_FAIL,
  GET_LIST_CUSTOMER_PROGRESS,
  GET_LIST_CUSTOMER_SUCCESS,
  GET_LIST_CUSTOMER_FAIL,
  ADD_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  ADD_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  ADD_ATTACHMENT_BY_CUSTOMER_FAIL,
  REMOVE_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  REMOVE_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  REMOVE_ATTACHMENT_BY_CUSTOMER_FAIL,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_PROGRESS,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_SUCCESS,
  GET_LIST_ATTACHMENT_BY_CUSTOMER_FAIL,
  GET_SIGNED_URL_CUSTOMER_PROGRESS,
  GET_SIGNED_URL_CUSTOMER_SUCCESS,
  GET_SIGNED_URL_CUSTOMER_FAIL,
  FORMAT_DATA_URL_CUSTOMER_PROGRESS,
  FORMAT_DATA_URL_CUSTOMER_SUCCESS,
  FORMAT_DATA_URL_CUSTOMER_FAIL,
  CLEAR_FORMAT_DATA_URL_CUSTOMER
} from '../07-constants/customer';
import { updateStatus, calculateLength, linkImage } from '../08-helpers/common';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { getEnv } from '../env';
import { CHECK_ERROR } from '../07-constants/common';

export const createCustomer = payload => {
  const formData = new FormData();
  formData.append('action', 'create_customer');
  const key = ['name', 'marketId', 'note', 'company', 'status'];
  key.forEach(item => {
    formData.append(item, payload[item]);
  });

  formData.append('avatar', payload.avatar, payload.avatar.name);

  payload.files.forEach(file => {
    formData.append('files', file, file.name);
  });

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateCustomer = (id, payload) => {
  const formData = new FormData();
  formData.append('action', 'update_customer');
  formData.append('id', id);
  const key = ['name', 'marketId', 'note', 'company', 'status'];
  key.forEach(item => {
    formData.append(item, payload[item]);
  });

  formData.append('avatar', payload.avatar, payload.avatar.name);

  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_CUSTOMER_FAIL, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeCustomer = id => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_customer',
        id
      }),
      null,
      data => {
        dispatch(updateStatus(REMOVE_CUSTOMER_SUCCESS, id));
      },
      err => {
        dispatch(updateStatus(REMOVE_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListCustomer = (page, limit, name, marketId, company, status) => {
  return dispatch => {
    dispatch(updateStatus(GET_LIST_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_customer',
        page,
        limit,
        name,
        marketId,
        company,
        status
      }),
      null,
      data => {
        dispatch(updateStatus(GET_LIST_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_LIST_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAttachmentsByCustomer = attachmentId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_ATTACHMENT_BY_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_attachment_by_customer',
        attachmentId
      }),
      null,
      data => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_BY_CUSTOMER_SUCCESS, attachmentId));
      },
      err => {
        dispatch(updateStatus(REMOVE_ATTACHMENT_BY_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addAttachmentsByCustomer = (customerId, files) => {
  const formData = new FormData();
  formData.append('action', 'add_attachment_by_customer');
  formData.append('customerId', customerId);
  files.forEach(file => {
    formData.append('files', file, file.name);
  });
  const config = {
    method: 'post',
    headers: {
      Accept: 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: formData
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ATTACHMENT_BY_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_ATTACHMENT_BY_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_ATTACHMENT_BY_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListAttachmentByCustomer = customerId => {
  return dispatch => {
    dispatch(updateStatus(GET_LIST_ATTACHMENT_BY_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_list_attachment_by_customer',
        customerId
      }),
      null,
      data => {
        dispatch(updateStatus(GET_LIST_ATTACHMENT_BY_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_LIST_ATTACHMENT_BY_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const downloadCustomerAttachments = filePath => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_signed_url_customer',
      filePath
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_SIGNED_URL_CUSTOMER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_SIGNED_URL_CUSTOMER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_SIGNED_URL_CUSTOMER_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

const onGetImageSize = url => {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve(img);
    img.src = url;
  });
};

export const onFormatData = data => {
  const result =
    calculateLength(data) &&
    data.map(async item => {
      const data = await onGetImageSize(linkImage(item.url));
      return {
        ...item,
        src: linkImage(item.url),
        width: data.width,
        height: data.height
      };
    });
  if (result) {
    return dispatch => {
      dispatch(updateStatus(FORMAT_DATA_URL_CUSTOMER_PROGRESS));
      Promise.all(result)
        .then(data => {
          if (data) {
            dispatch(updateStatus(FORMAT_DATA_URL_CUSTOMER_SUCCESS, data));
          }
        })
        .catch(err => {
          dispatch(updateStatus(FORMAT_DATA_URL_CUSTOMER_FAIL, { error: err }));
          dispatch(updateStatus(CHECK_ERROR, { error: err }));
        });
    };
  }
};

export const clearAllDataFormated = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_FORMAT_DATA_URL_CUSTOMER));
  };
};
