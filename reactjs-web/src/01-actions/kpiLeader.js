/*
 *=================================================================================================
 * KPI Leader Actions
 *=================================================================================================
 */
import * as R from 'ramda';
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import { PROJECT_MANAGER_NAME } from '../05-utils/commonData';
import {
  GET_LIST_KPI_SPRINT_FAILED,
  GET_LIST_KPI_SPRINT_INPROGRESS,
  GET_LIST_KPI_SPRINT_SUCCESS,
  GET_LIST_PROJECT_FAILED,
  GET_LIST_PROJECT_INPROGRESS,
  GET_LIST_PROJECT_SUCCESS,
  CREATE_KPI_SPRINT_FAILED,
  CREATE_KPI_SPRINT_INPROGRESS,
  CREATE_KPI_SPRINT_SUCCESS,
  REMOVE_KPI_SPRINT_FAILED,
  REMOVE_KPI_SPRINT_INPROGRESS,
  REMOVE_KPI_SPRINT_SUCCESS,
  UPDATE_KPI_SPRINT_FAILED,
  UPDATE_KPI_SPRINT_INPROGRESS,
  UPDATE_KPI_SPRINT_SUCCESS
} from '../07-constants/kpiLeader';

export const getKpiSprint = ({ userId, projectId, type, page, limit }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_kpi_sprint',
      userId,
      projectId,
      type,
      page,
      limit
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_KPI_SPRINT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        let managerOptions = [];
        const listKpiSprint = result.data.map(sprint => {
          let managers = [];

          sprint.project.users.forEach(user => {
            if (
              (user && user.ProjectRole && user.ProjectRole.name && user.ProjectRole.name.toUpperCase()) ===
              PROJECT_MANAGER_NAME
            ) {
              managers = [...managers, user.User.name];

              const manager = { value: user.User.id, label: user.User.name };
              !R.includes(manager, managerOptions) && (managerOptions = [...managerOptions, manager]);
            }
          });
          return { ...sprint, managers };
        });

        dispatch(
          updateStatus(GET_LIST_KPI_SPRINT_SUCCESS, {
            listKpiSprint,
            managerOptions,
            total: result.total
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_KPI_SPRINT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListProject = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_projects'
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_PROJECT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        const projectOptions = result.map(project => ({
          value: project.id,
          label: project.name
        }));

        dispatch(
          updateStatus(GET_LIST_PROJECT_SUCCESS, {
            listProjects: result,
            projectOptions
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_LIST_PROJECT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListMyProjects = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_my_projects'
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_LIST_PROJECT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(GET_LIST_PROJECT_SUCCESS, { listProjects: result }));
      },
      err => {
        dispatch(updateStatus(GET_LIST_PROJECT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createKpiSprint = ({
  userId,
  sprintId,
  projectId,
  workManagementPoint,
  peopleManagementPoint,
  processPoint,
  project,
  sprint,
  user
}) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_kpi_sprint',
      userId,
      sprintId,
      projectId,
      workManagementPoint,
      peopleManagementPoint,
      processPoint
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_KPI_SPRINT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        if (result) {
          let managerOptions = [];
          let managers = [];
          project.users.forEach(user => {
            if (
              (user && user.ProjectRole && user.ProjectRole.name && user.ProjectRole.name.toUpperCase()) ===
              PROJECT_MANAGER_NAME
            ) {
              managers = [...managers, user.User.name];

              const manager = { value: user.User.id, label: user.User.name };
              !R.includes(manager, managerOptions) && (managerOptions = [...managerOptions, manager]);
            }
          });

          dispatch(
            updateStatus(CREATE_KPI_SPRINT_SUCCESS, {
              kpiCreation: { project, sprint, user, ...result, managers },
              managerOptions
            })
          );
        }
      },
      err => {
        dispatch(updateStatus(CREATE_KPI_SPRINT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeKpiSprint = ({ kpiSprintId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_kpi_sprint',
      kpiSprintId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_KPI_SPRINT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(REMOVE_KPI_SPRINT_SUCCESS, { kpiRemove: kpiSprintId }));
      },
      err => {
        dispatch(updateStatus(REMOVE_KPI_SPRINT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateKpiSprint = ({ kpiSprintId, workManagementPoint, peopleManagementPoint, processPoint, user }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_kpi_sprint',
      kpiSprintId,
      workManagementPoint,
      peopleManagementPoint,
      processPoint
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_KPI_SPRINT_INPROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        if (result) {
          dispatch(
            updateStatus(UPDATE_KPI_SPRINT_SUCCESS, {
              kpiUpdate: { user, ...result }
            })
          );
        }
      },
      err => {
        dispatch(updateStatus(UPDATE_KPI_SPRINT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
