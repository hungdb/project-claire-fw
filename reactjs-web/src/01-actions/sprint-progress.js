/*
 *=================================================================================================
 * Sprint Progress Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  RESET_SPRINT_PROGRESS_MEMBERS,
  GET_SPRINT_PROGRESS_MEMBERS,
  GET_SPRINT_PROGRESS_MEMBERS_SUCCESS,
  GET_SPRINT_PROGRESS_MEMBERS_FAILED
} from '../07-constants/sprint-progress';

export const resetProgressMembersInActiveSprint = () => ({
  type: RESET_SPRINT_PROGRESS_MEMBERS
});

export const getProgressMembersInActiveSprint = sprintId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'sprint_progress_of_member',
      sprintId
    })
  };

  return dispatch => {
    dispatch({ type: GET_SPRINT_PROGRESS_MEMBERS });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch({
          type: GET_SPRINT_PROGRESS_MEMBERS_SUCCESS,
          sprintProgressMembers: result
        });
      },
      error => {
        dispatch({
          type: GET_SPRINT_PROGRESS_MEMBERS_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};
