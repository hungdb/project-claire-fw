/*
 *=================================================================================================
 * Allocation Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer, getDateAt235959ZeroGMT, getDateAt000000ZeroGMT } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  LIST_ALLOCATIONS_OF_USER_PROGRESS,
  LIST_ALLOCATIONS_OF_USER_SUCCESS,
  LIST_ALLOCATIONS_OF_USER_FAILED,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  LIST_SKILLS_BY_USER_PROGRESS,
  LIST_SKILLS_BY_USER_SUCCESS,
  LIST_SKILLS_BY_USER_FAILED,
  CLEAR_SKILLS,
  FILTER_BY_START_DATE,
  FILTER_BY_EFFORT_PERCENT,
  FILTER_BY_END_DATE,
  FILTER_BY_SKILL,
  FILTER_BY_SEARCH_NAME,
  FILTER_BY_PROJECT,
  FILTER_BY_PAGE,
  FILTER_ALLOCATION_FAILED,
  FILTER_ALLOCATION_PROGRESS,
  FILTER_ALLOCATION_SUCCESS,
  SWITCH_USER_ALLOCATION,
  CLEAR_ALLOCATION_OF_USER,
  CLEAR_SKILL_OF_USER
} from '../07-constants/allocation';

export const listUsers = (params = { limit: 10, offset: 0 }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_current_users',
      limit: params.limit,
      offset: params.offset,
      isActive: true
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_USERS_SUCCESS, { listUsers: result }));
      },
      err => {
        dispatch(updateStatus(LIST_USERS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listAllocationsOfUser = userId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_allocation_of_user',
      userId: userId
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_ALLOCATIONS_OF_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        const allocationsOfUser = [...result.allocations];
        delete result.allocations;

        dispatch(
          updateStatus(LIST_ALLOCATIONS_OF_USER_SUCCESS, {
            allocationsOfUser,
            userInfor: result
          })
        );
      },
      err => {
        dispatch(updateStatus(LIST_ALLOCATIONS_OF_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listSkillsByUser = userId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_skills_by_user_with_latest_evaluation',
      userId
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_SKILLS_BY_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_SKILLS_BY_USER_SUCCESS, { skills: result }));
      },
      err => {
        dispatch(updateStatus(LIST_SKILLS_BY_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearSkills = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SKILLS));
  };
};

export const handleFilterByStartDate = startDate => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_START_DATE, { startDate }));
  };
};

export const handleFilterByEndDate = endDate => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_END_DATE, { endDate }));
  };
};

export const handleFilterBySkill = skillId => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_SKILL, { skillId }));
  };
};

export const handleFilterByProject = projectId => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_PROJECT, { projectId }));
  };
};

export const handleFilterByEffortPercent = effortPercent => {
  return dispatch => {
    dispatch(
      updateStatus(FILTER_BY_EFFORT_PERCENT, {
        effortPercent: Number(effortPercent)
      })
    );
  };
};

export const handleFilterBySearchName = name => {
  return dispatch => {
    dispatch(updateStatus(FILTER_BY_SEARCH_NAME, { name: name }));
  };
};

export const handleFilterByPage = page => ({ type: FILTER_BY_PAGE, page });

export const filterAllocation = ({ startDate, endDate, effortPercent, skillId, name, limit, page, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'filter_allocation',
      startDate: startDate ? getDateAt000000ZeroGMT(startDate) : null,
      endDate: endDate ? getDateAt235959ZeroGMT(endDate) : null,
      effortPercent: effortPercent,
      skillId: skillId,
      name: name,
      limit: limit,
      page: page,
      projectId: projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(FILTER_ALLOCATION_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(FILTER_ALLOCATION_SUCCESS, { listUsers: result }));
      },
      err => {
        dispatch(updateStatus(FILTER_ALLOCATION_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const switchUserAllocation = user => {
  return dispatch => {
    dispatch(updateStatus(SWITCH_USER_ALLOCATION, { user }));
  };
};

export const clearAllocationOfUser = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ALLOCATION_OF_USER));
  };
};

export const clearSkillOfUser = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SKILL_OF_USER));
  };
};
