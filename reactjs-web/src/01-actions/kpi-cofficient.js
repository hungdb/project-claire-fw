/*
 *=================================================================================================
 * KPI Coefficient Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  UPDATE_KPI_COEFFICIENT_PROGRESS,
  UPDATE_KPI_COEFFICIENT_SUCCESS,
  UPDATE_KPI_COEFFICIENT_FAILED,
  GET_KPI_COEFFICIENT_PROGRESS,
  GET_KPI_COEFFICIENT_SUCCESS,
  GET_KPI_COEFFICIENT_FAILED
} from '../07-constants/kpi-cofficient';

export const getKpiCoefficient = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_kpi_coefficients'
    })
  };
  return dispatch => {
    dispatch(updateStatus(GET_KPI_COEFFICIENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(GET_KPI_COEFFICIENT_SUCCESS, {
            listKpiCoefficient: result
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_KPI_COEFFICIENT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateKpiCoefficient = (id, data) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_kpi_coefficient',
      id,
      data
    })
  };
  return dispatch => {
    dispatch(updateStatus(UPDATE_KPI_COEFFICIENT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(UPDATE_KPI_COEFFICIENT_SUCCESS, { id, data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_KPI_COEFFICIENT_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
