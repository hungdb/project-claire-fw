/*
 *=================================================================================================
 * Market Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_LIST_MARKET_PROGRESS,
  GET_LIST_MARKET_SUCCESS,
  GET_LIST_MARKET_FAIL,
  CREATE_MARKET_PROGRESS,
  CREATE_MARKET_SUCCESS,
  CREATE_MARKET_FAIL,
  UPDATE_MARKET_PROGRESS,
  UPDATE_MARKET_SUCCESS,
  UPDATE_MARKET_FAIL,
  REMOVE_MARKET_PROGRESS,
  REMOVE_MARKET_SUCCESS,
  REMOVE_MARKET_FAIL
} from '../07-constants/market';

export const getListMarket = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_list_market'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_MARKET_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_LIST_MARKET_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(GET_LIST_MARKET_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createMarket = name => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_market',
      name
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_MARKET_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(CREATE_MARKET_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(CREATE_MARKET_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateMarket = (id, name) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_market',
      id,
      name
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_MARKET_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_MARKET_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_MARKET_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeMarket = id => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_market',
      id
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_MARKET_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(REMOVE_MARKET_SUCCESS, id));
      },
      err => {
        dispatch(updateStatus(REMOVE_MARKET_FAIL, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
