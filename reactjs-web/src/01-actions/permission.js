/*
 *=================================================================================================
 * Permission Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, checkAccessToken, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  GET_PERMISSION_PROGRESS,
  GET_PERMISSION_SUCCESS,
  GET_PERMISSION_FAILED,
  GET_ALL_PERMISSION_PROGRESS,
  GET_ALL_PERMISSION_SUCCESS,
  GET_ALL_PERMISSION_FAILED,
  ADD_ROLE_PERMISSION_SUCCESS,
  ADD_ROLE_PERMISSION_FAILED,
  REMOVE_PERMISSION_SUCCESS,
  REMOVE_PERMISSION_FAILED,
  CREATE_ROLE_PROGRESS,
  CREATE_ROLE_SUCCESS,
  CREATE_ROLE_FAILED,
  UPDATE_ROLE_PROGRESS,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAILED,
  REMOVE_ROLE_PROGRESS,
  REMOVE_ROLE_SUCCESS,
  PROJECT_GET_ROLE_PROGRESS,
  PROJECT_GET_ROLE_SUCCESS,
  PROJECT_GET_ROLE_FAILED,
  REMOVE_PERMISSION_PROGRESS,
  REMOVE_ROLE_FAILED
} from '../07-constants/permission';

export const listPermissions = listPerdata => {
  return dispatch => {
    dispatch(updateStatus(GET_PERMISSION_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_permission_of_role',
            roleId: listPerdata.roleId
          }),
          null,
          res => {
            dispatch(updateStatus(GET_PERMISSION_SUCCESS, { listPerms: res }));
          },
          err => {
            dispatch(updateStatus(GET_PERMISSION_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const listAllPermissions = () => {
  return dispatch => {
    dispatch(updateStatus(GET_ALL_PERMISSION_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'list_all_permissions'
          }),
          null,
          res => {
            dispatch(updateStatus(GET_ALL_PERMISSION_SUCCESS, { allPerms: res }));
          },
          err => {
            dispatch(updateStatus(GET_ALL_PERMISSION_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const addRolePermission = addPermissionData => {
  return dispatch => {
    dispatch(updateStatus(GET_ALL_PERMISSION_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_role_permission',
            roleId: addPermissionData.roleId,
            permission: addPermissionData.permission,
            permissionConditions: addPermissionData.permissionConditions
          }),
          null,
          res => {
            dispatch(updateStatus(ADD_ROLE_PERMISSION_SUCCESS, res));
          },
          err => {
            dispatch(updateStatus(ADD_ROLE_PERMISSION_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const removeRolePermission = removePermissionData => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_PERMISSION_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'remove_role_permission',
            rolePermissionId: removePermissionData.rolePermissionId
          }),
          null,
          () => {
            dispatch(updateStatus(REMOVE_PERMISSION_SUCCESS, removePermissionData));
          },
          err => {
            dispatch(updateStatus(REMOVE_PERMISSION_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const createRole = createRoleData => {
  return dispatch => {
    dispatch(updateStatus(CREATE_ROLE_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'create_role',
            name: createRoleData.name
          }),
          null,
          res => {
            dispatch(updateStatus(CREATE_ROLE_SUCCESS, res));
          },
          err => {
            dispatch(updateStatus(CREATE_ROLE_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const updateRole = updateRoleData => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_ROLE_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'update_role',
            roleId: updateRoleData.id,
            name: updateRoleData.name
          }),
          null,
          res => {
            dispatch(updateStatus(UPDATE_ROLE_SUCCESS, updateRoleData));
          },
          err => {
            dispatch(updateStatus(UPDATE_ROLE_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const removeRole = removeRoleData => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_ROLE_PROGRESS));

    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        callApi(
          getEnv('REACT_APP_API_SERVER') + '/api/rpc',
          defaultPostConfig({
            action: 'remove_role',
            roleId: removeRoleData.id
          }),
          null,
          () => {
            dispatch(updateStatus(REMOVE_ROLE_SUCCESS, removeRoleData));
          },
          err => {
            dispatch(updateStatus(REMOVE_ROLE_FAILED, { error: err }));
            dispatch(updateStatus(CHECK_ERROR, { error: err }));
          }
        );
      }
    });
  };
};

export const getRoles = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_roles'
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_ROLE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(PROJECT_GET_ROLE_SUCCESS, { listRoles: data }));
      },
      err => {
        dispatch(updateStatus(PROJECT_GET_ROLE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
