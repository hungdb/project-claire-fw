/*
 *=================================================================================================
 * Sprint Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultFormDataConfig, defaultPostConfig } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  // SPRINTS_PAGE_URL_CACHE,
  GET_TASK_BACKLOG_PROGRESS,
  GET_TASK_BACKLOG_PROJECT_SUCCESS,
  GET_TASK_BACKLOG_PROJECT_FAILED,
  CLEAR_TASK_BACKLOG,
  GET_SUGGEST_TAG_PROGRESS,
  GET_SUGGEST_TAG_SUCCESS,
  GET_SUGGEST_TAG_FAILED,
  CHANGE_ESTIMATE_PROGRESS,
  CHANGE_ESTIMATE_SUCCESS,
  CHANGE_ESTIMATE_FAILED,
  CREATE_TASK_PROGRESS,
  CREATE_TASK_SUCCESS,
  CREATE_TASK_FAILED,
  UPDATE_TASK_PROGRESS,
  UPDATE_TASK_SUCCESS,
  UPDATE_TASK_FAILED,
  GET_DETAIL_TASK_PROGRESS,
  GET_DETAIL_TASK_SUCCESS,
  GET_DETAIL_TASK_FAILED,
  DELETE_TASK_PROGRESS,
  DELETE_TASK_SUCCESS,
  DELETE_TASK_FAILED,
  GET_SPRINTS_PROGRESS,
  GET_SPRINTS_SUCCESS,
  GET_SPRINTS_FAILED,
  UPDATE_SPRINT_PROGRESS,
  UPDATE_SPRINT_SUCCESS,
  UPDATE_SPRINT_FAILED,
  DELETE_SPRINT_PROGRESS,
  DELETE_SPRINT_SUCCESS,
  DELETE_SPRINT_FAILED,
  CREATE_SPRINT_PROGRESS,
  CREATE_SPRINT_SUCCESS,
  CREATE_SPRINT_FAILED,
  GET_SPRINTS_DIFF_COMPLETED_PROGRESS,
  GET_SPRINTS_DIFF_COMPLETED_SUCCESS,
  GET_SPRINTS_DIFF_COMPLETED_FAILED,
  CLEAR_SPRINTS_DIFF_COMPLETED,
  GET_DETAIL_SPRINT_PROGRESS,
  GET_DETAIL_SPRINT_SUCCESS,
  GET_DETAIL_SPRINT_FAILED,
  CLEAR_DETAIL_SPRINT,
  SEARCH_BACKLOG_TASK_PROGRESS,
  SEARCH_BACKLOG_TASK_SUCCESS,
  SEARCH_BACKLOG_TASK_FAILED,
  REMOVE_TASK_TO_BACKLOG_PROGRESS,
  REMOVE_TASK_TO_BACKLOG_SUCCESS,
  REMOVE_TASK_TO_BACKLOG_FAILED,
  ADD_TASK_TO_SPRINT_PROGRESS,
  ADD_TASK_TO_SPRINT_SUCCESS,
  ADD_TASK_TO_SPRINT_FAILED,
  REMOVE_ALLOCATION_PROGRESS,
  REMOVE_ALLOCATION_SUCCESS,
  REMOVE_ALLOCATION_FAILED,
  ADD_ALLOCATION_PROGRESS,
  ADD_ALLOCATION_SUCCESS,
  ADD_ALLOCATION_FAILED,
  GET_STATISTICAL_ACTIVE_SPRINT_PROGRESS,
  GET_STATISTICAL_ACTIVE_SPRINT_SUCCESS,
  GET_STATISTICAL_ACTIVE_SPRINT_FAILED,
  GET_WORKFLOW_BY_PROJECT_FAILED,
  GET_WORKFLOW_BY_PROJECT_PROGRESS,
  GET_WORKFLOW_BY_PROJECT_SUCCESS,
  MOVE_TASK_PROGRESS,
  MOVE_TASK_SUCCESS,
  MOVE_TASK_FAILED,
  SWITCH_SPRINT_PROJECT,
  CLEAR_CURRENT_TASK,
  GET_USERS_BY_PROJECT_PROGRESS,
  GET_USERS_BY_PROJECT_FAILED,
  GET_USERS_BY_PROJECT_SUCCESS,
  SUGGEST_MILESTONES_FAILED,
  SUGGEST_MILESTONES_PROGRESS,
  SUGGEST_MILESTONES_SUCCESS,
  GET_ALL_TASKS_PROGRESS,
  GET_ALL_TASKS_FAILED,
  GET_ALL_TASKS_SUCCESS,
  GET_USERS_OF_TASK_FAILED,
  GET_USERS_OF_TASK_PROGRESS,
  GET_USERS_OF_TASK_SUCCESS,
  CLEAR_ALL_BOARD,
  CLEAR_SPRINTS_LIST,
  SET_TYPE_GET_TASK,
  SET_ACTIVE_TAB_BOARD,
  SET_KEY_SEARCH_TASK,
  ASSIGN_TASK_TO_USER_PROGRESS,
  ASSIGN_TASK_TO_USER_SUCCESS,
  ASSIGN_TASK_TO_USER_FAILED,
  ADD_USER_SKILL_IN_SPRINT_PROGRESS,
  ADD_USER_SKILL_IN_SPRINT_SUCCESS,
  ADD_USER_SKILL_IN_SPRINT_FAILED,
  LOG_BEHAVIOR_SPRINT_PROGRESS,
  LOG_BEHAVIOR_SPRINT_SUCCESS,
  LOG_BEHAVIOR_SPRINT_FAILED,
  LIST_BEHAVIORS_PROGRESS,
  LIST_BEHAVIORS_SUCCESS,
  LIST_BEHAVIORS_FAILED,
  GET_LIST_ALLOCATION_LABELS_REQUEST,
  GET_LIST_ALLOCATION_LABELS_SUCCESS,
  GET_LIST_ALLOCATION_LABELS_FAILED,
  LIST_USER_SKILLS_PROGRESS,
  LIST_USER_SKILLS_SUCCESS,
  LIST_USER_SKILLS_FAILED,
  REMOVE_USER_SKILL_PROGRESS,
  REMOVE_USER_SKILL_SUCCESS,
  REMOVE_USER_SKILL_FAILED,
  LIST_USER_SKILLS_BY_SPRINT_PROGRESS,
  LIST_USER_SKILLS_BY_SPRINT_SUCCESS,
  LIST_USER_SKILLS_BY_SPRINT_FAILED,
  REMOVE_USER_SKILLS_IN_SPRINT_PROGRESS,
  REMOVE_USER_SKILLS_IN_SPRINT_SUCCESS,
  REMOVE_USER_SKILLS_IN_SPRINT_FAILED,
  ADD_MY_SKILL_IN_SPRINT_SUCCESS,
  ADD_MY_SKILL_IN_SPRINT_PROGRESS,
  ADD_MY_SKILL_IN_SPRINT_FAILED,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_PROGRESS,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_SUCCESS,
  GET_LIST_LOG_BEHAVIOR_IN_SPRINT_FAILED,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_PROGRESS,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_SUCCESS,
  REMOVE_LOG_BEHAVIOR_IN_SPRINT_FAILED,
  UPDATE_ALLOCATION_REQUEST,
  UPDATE_ALLOCATION_SUCCESS,
  UPDATE_ALLOCATION_FAILED,
  FILTER_USER_ON_BOARD,
  CLEAR_FILTER_USER_ON_BOARD
} from '../07-constants/sprint';

export const getTaskBacklog = ({ projectId, tags, limit, offset }) => {
  return dispatch => {
    dispatch(updateStatus(GET_TASK_BACKLOG_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_task_backlog_by_project',
        projectId,
        tags,
        limit,
        offset
      }),
      null,
      data => {
        dispatch(updateStatus(GET_TASK_BACKLOG_PROJECT_SUCCESS, { backlog: data }));
      },
      err => {
        dispatch(updateStatus(GET_TASK_BACKLOG_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearBacklog = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_TASK_BACKLOG));
  };
};

export const getSuggestTags = projectId => {
  return dispatch => {
    dispatch(updateStatus(GET_SUGGEST_TAG_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'suggest_tag',
        projectId
      }),
      null,
      data => {
        dispatch(updateStatus(GET_SUGGEST_TAG_SUCCESS, { tags: data }));
      },
      err => {
        dispatch(updateStatus(GET_SUGGEST_TAG_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const changeEstimate = ({ taskId, estimate, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(CHANGE_ESTIMATE_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'add_estimate',
        taskId,
        projectId,
        estimate
      }),
      null,
      data => {
        dispatch(updateStatus(CHANGE_ESTIMATE_SUCCESS, { info: data }));
      },
      err => {
        dispatch(updateStatus(CHANGE_ESTIMATE_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createBacklogTask = ({
  content,
  estimate,
  projectId,
  tag,
  description,
  milestone,
  priority,
  dependencies,
  assignee,
  dueDate,
  files,
  type,
  issueId,
  userRelate
}) => {
  const formData = new FormData();
  formData.append('action', 'create_task');
  formData.append('content', content);
  formData.append('projectId', projectId);
  if (issueId) formData.append('issueId', issueId);
  estimate && formData.append('estimate', estimate);
  tag && Array.isArray(tag) && formData.append('tag', JSON.stringify(tag));
  description && formData.append('description', description);
  milestone && formData.append('milestone', milestone);
  priority && formData.append('priority', priority);
  dependencies && Array.isArray(dependencies) && formData.append('dependencies', JSON.stringify(dependencies));
  assignee && formData.append('assignee', assignee);
  dueDate && formData.append('dueDate', dueDate);
  if (files) {
    for (const file of files) {
      formData.append('files', file, file.name);
    }
  }
  type && formData.append('type', type);
  userRelate && Array.isArray(userRelate) && formData.append('userRelate', JSON.stringify(userRelate));

  return dispatch => {
    dispatch(updateStatus(CREATE_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultFormDataConfig(formData),
      null,
      () => {
        dispatch(updateStatus(CREATE_TASK_SUCCESS));
      },
      err => {
        dispatch(updateStatus(CREATE_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateBacklogTask = ({
  taskId,
  content,
  estimate,
  tag,
  description,
  projectId,
  milestone,
  priority,
  dependencies,
  assignee,
  dueDate,
  type,
  userRelate
}) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_task_backlog',
        taskId,
        projectId,
        content,
        estimate,
        tag,
        description,
        milestone,
        priority,
        dependencies,
        assignee,
        dueDate,
        type,
        userRelate: JSON.stringify(userRelate)
      }),
      null,
      data => {
        dispatch(updateStatus(UPDATE_TASK_SUCCESS, { info: data }));
      },
      err => {
        dispatch(updateStatus(UPDATE_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getDetailTask = ({ taskId, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(GET_DETAIL_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_detail_task',
        taskId,
        projectId
      }),
      null,
      data => {
        dispatch(updateStatus(GET_DETAIL_TASK_SUCCESS, { info: data }));
      },
      err => {
        dispatch(updateStatus(GET_DETAIL_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearDetailBacklogTask = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_CURRENT_TASK));
  };
};

export const clearSprintsDiffCompleted = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SPRINTS_DIFF_COMPLETED));
  };
};

export const deleteTask = ({ taskId, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(DELETE_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_task',
        taskId,
        projectId
      }),
      null,
      () => {
        dispatch(updateStatus(DELETE_TASK_SUCCESS));
      },
      err => {
        dispatch(updateStatus(DELETE_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getSprints = ({ projectId }) => {
  return dispatch => {
    dispatch(updateStatus(GET_SPRINTS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_sprints_by_projectId',
        projectId
      }),
      null,
      data => {
        dispatch(updateStatus(GET_SPRINTS_SUCCESS, { sprints: data }));
      },
      err => {
        dispatch(updateStatus(GET_SPRINTS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateSprint = ({ sprintId, name, status, startDate, endDate, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(UPDATE_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'update_sprint',
        sprintId,
        projectId,
        name,
        status,
        startDate,
        endDate
      }),
      null,
      () => {
        dispatch(updateStatus(UPDATE_SPRINT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(UPDATE_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const deleteSprint = ({ sprintId, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(DELETE_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_sprint',
        sprintId,
        projectId
      }),
      null,
      () => {
        dispatch(updateStatus(DELETE_SPRINT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(DELETE_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const createSprint = ({ projectId, name, startDate, endDate }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_sprint',
      projectId,
      name,
      startDate,
      endDate
    })
  };

  return dispatch => {
    dispatch(updateStatus(CREATE_SPRINT_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(CREATE_SPRINT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(CREATE_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getSprintDiffCompleted = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_sprints_diff_completed_by_projectId',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_SPRINTS_DIFF_COMPLETED_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_SPRINTS_DIFF_COMPLETED_SUCCESS, {
            sprintsDiffCompleted: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_SPRINTS_DIFF_COMPLETED_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getDetailSprint = ({ sprintId, projectId }) => {
  return dispatch => {
    dispatch(updateStatus(GET_DETAIL_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'get_sprint',
        sprintId,
        projectId
      }),
      null,
      data => {
        dispatch(updateStatus(GET_DETAIL_SPRINT_SUCCESS, { info: data }));
      },
      err => {
        dispatch(updateStatus(GET_DETAIL_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const clearDetailSprint = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_DETAIL_SPRINT));
  };
};

export const searchBacklogTask = ({ projectId, key }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'filter_task_backlog_by_project',
      projectId,
      key
    })
  };

  return dispatch => {
    dispatch(updateStatus(SEARCH_BACKLOG_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(SEARCH_BACKLOG_TASK_SUCCESS, { backlog: data }));
      },
      err => {
        dispatch(updateStatus(SEARCH_BACKLOG_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeTaskToBacklog = ({ taskId = null, projectId = null, type = null, sprintId = null }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_task_to_backlog',
      taskId,
      projectId,
      type,
      sprintId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_TASK_TO_BACKLOG_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_TASK_TO_BACKLOG_SUCCESS));
      },
      err => {
        dispatch(updateStatus(REMOVE_TASK_TO_BACKLOG_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addTaskToSprint = ({ sprintId = null, taskId = null, projectId = null, type = null }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_sprint_of_task',
      sprintId,
      taskId,
      projectId,
      type
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_TASK_TO_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(ADD_TASK_TO_SPRINT_SUCCESS));
      },
      err => {
        dispatch(updateStatus(ADD_TASK_TO_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeAllocation = ({ allocationId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_allocation',
      allocationId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(REMOVE_ALLOCATION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(REMOVE_ALLOCATION_SUCCESS));
      },
      err => {
        dispatch(updateStatus(REMOVE_ALLOCATION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addAllocation = ({ sprintId, data }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_allocation',
      sprintId,
      data
    })
  };

  return dispatch => {
    dispatch(updateStatus(ADD_ALLOCATION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(ADD_ALLOCATION_SUCCESS));
      },
      err => {
        dispatch(updateStatus(ADD_ALLOCATION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const statisticalActiveSprint = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'statistical_sprint_active_by_project',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_STATISTICAL_ACTIVE_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(
          updateStatus(GET_STATISTICAL_ACTIVE_SPRINT_SUCCESS, {
            statistical: data
          })
        );
      },
      err => {
        dispatch(updateStatus(GET_STATISTICAL_ACTIVE_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getWorkflowByProject = ({ projectId, type, assignee }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_workflow_state_by_projectId',
      projectId,
      type,
      assignee
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_WORKFLOW_BY_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_WORKFLOW_BY_PROJECT_SUCCESS, { workflow: data }));
      },
      err => {
        dispatch(updateStatus(GET_WORKFLOW_BY_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const moveTask = ({ cardId, sourceLaneId, targetLaneId, position, cardDetails, projectId }) => {
  const data = { cardId, sourceLaneId, targetLaneId, position, cardDetails };
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'create_task_state',
      taskId: cardId,
      toState: targetLaneId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(MOVE_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch(updateStatus(MOVE_TASK_SUCCESS), { info: data });
      },
      err => {
        dispatch(updateStatus(MOVE_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const filterUserOnBoard = assignee => {
  return updateStatus(FILTER_USER_ON_BOARD, { assignee });
};

export const clearFilterUserOnBoard = () => {
  return updateStatus(CLEAR_FILTER_USER_ON_BOARD);
};

export const switchSprintProject = project => {
  return updateStatus(SWITCH_SPRINT_PROJECT, { project });
};

export const clearCurrentTask = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_CURRENT_TASK));
  };
};

export const clearBoard = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_ALL_BOARD));
  };
};

export const clearSprintsList = () => {
  return dispatch => {
    dispatch(updateStatus(CLEAR_SPRINTS_LIST));
  };
};

export const getUsersByProject = ({ projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_users_by_project',
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_USERS_BY_PROJECT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_USERS_BY_PROJECT_SUCCESS, { users: data }));
      },
      err => {
        dispatch(updateStatus(GET_USERS_BY_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const suggestMilestones = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'get_milestone_suggestions'
    })
  };

  return dispatch => {
    dispatch(updateStatus(SUGGEST_MILESTONES_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(SUGGEST_MILESTONES_SUCCESS, { milestones: data }));
      },
      err => {
        dispatch(updateStatus(SUGGEST_MILESTONES_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getTasks = ({ projectId, type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_task_backlog_by_project',
      projectId,
      type
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_ALL_TASKS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_ALL_TASKS_SUCCESS, { tasks: data.rows }));
      },
      err => {
        dispatch(updateStatus(GET_ALL_TASKS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getUsersOfTask = ({ taskId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_users_of_task',
      taskId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_USERS_OF_TASK_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(GET_USERS_OF_TASK_SUCCESS, { users: data }));
      },
      err => {
        dispatch(updateStatus(GET_USERS_OF_TASK_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const setTypeGetTask = type => {
  return dispatch => {
    dispatch(updateStatus(SET_TYPE_GET_TASK, { type }));
  };
};

export const setActiveTabBoard = type => {
  return dispatch => {
    dispatch(updateStatus(SET_ACTIVE_TAB_BOARD, { type }));
  };
};

export const setKeySearchTask = key => {
  return dispatch => {
    dispatch(updateStatus(SET_KEY_SEARCH_TASK, { key }));
  };
};

export const assignTaskToUser = ({ taskId, userId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'assign_task',
      taskId,
      userId
    })
  };
  return dispatch => {
    dispatch(updateStatus(ASSIGN_TASK_TO_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(ASSIGN_TASK_TO_USER_SUCCESS, result));
      },
      err => {
        dispatch(updateStatus(ASSIGN_TASK_TO_USER_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addUserSkillInSprint = ({ userId, skillId, note, sprintId, type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'add_user_skill_in_sprint',
      userId,
      skillId,
      note,
      sprintId,
      type
    })
  };
  return dispatch => {
    dispatch(updateStatus(ADD_USER_SKILL_IN_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_USER_SKILL_IN_SPRINT_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_USER_SKILL_IN_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addMySkillInSprint = ({ skillId, note, sprintId, type }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'add_my_skill_in_sprint',
      skillId,
      note,
      sprintId,
      type
    })
  };
  return dispatch => {
    dispatch(updateStatus(ADD_MY_SKILL_IN_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(ADD_MY_SKILL_IN_SPRINT_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_MY_SKILL_IN_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getAllBehaviors = () => {
  return dispatch => {
    dispatch(updateStatus(LIST_BEHAVIORS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_behaviors_for_sprint'
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_BEHAVIORS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_BEHAVIORS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const logBehaviorSprint = (userId, sprintId, behaviors) => {
  return dispatch => {
    dispatch(updateStatus(LOG_BEHAVIOR_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'log_behavior_sprint',
        userId,
        sprintId,
        behaviors
      }),
      null,
      data => {
        dispatch(updateStatus(LOG_BEHAVIOR_SPRINT_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LOG_BEHAVIOR_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListAllocationLabels = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_allocation_labels'
    })
  };

  return dispatch => {
    dispatch(updateStatus(GET_LIST_ALLOCATION_LABELS_REQUEST));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        const allocationLabels = data.map(label => ({
          group: { id: label.id, name: label.name.toUpperCase() },
          allocations: [],
          totalTimeCurrent: 0
        }));
        allocationLabels.push({
          group: { id: 0, name: 'NOGROUP' },
          allocations: [],
          totalTimeCurrent: 0
        });
        dispatch({
          type: GET_LIST_ALLOCATION_LABELS_SUCCESS,
          allocationLabels
        });
      },
      err => {
        dispatch(updateStatus(GET_LIST_ALLOCATION_LABELS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListUserSkillsBySprint = sprintId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_user_skills_by_sprint',
      sprintId
    })
  };
  return dispatch => {
    dispatch(updateStatus(LIST_USER_SKILLS_BY_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(LIST_USER_SKILLS_BY_SPRINT_SUCCESS, {
            listUserSkillsSprint: result
          })
        );
      },
      err => {
        dispatch(updateStatus(LIST_USER_SKILLS_BY_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const listUserSkills = sprintId => {
  return dispatch => {
    dispatch(updateStatus(LIST_USER_SKILLS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_my_user_skills',
        sprintId
      }),
      null,
      data => {
        dispatch(updateStatus(LIST_USER_SKILLS_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(LIST_USER_SKILLS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeLogBehaviorInSprint = logBehaviorId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_LOG_BEHAVIOR_IN_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_log_behavior_in_sprint',
        logBehaviorId
      }),
      null,
      () => {
        dispatch(updateStatus(REMOVE_LOG_BEHAVIOR_IN_SPRINT_SUCCESS, logBehaviorId));
      },
      err => {
        dispatch(updateStatus(REMOVE_LOG_BEHAVIOR_IN_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const removeUserSkill = userSkillId => {
  return dispatch => {
    dispatch(updateStatus(REMOVE_USER_SKILL_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'remove_my_user_skill',
        userSkillId
      }),
      null,
      data => {
        dispatch(updateStatus(REMOVE_USER_SKILL_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(REMOVE_USER_SKILL_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListLogBehaviorInSprint = sprintId => {
  return dispatch => {
    dispatch(updateStatus(GET_LIST_LOG_BEHAVIOR_IN_SPRINT_PROGRESS));
    if (sprintId)
      callApi(
        getEnv('REACT_APP_API_SERVER') + '/api/rpc',
        defaultPostConfig({
          action: 'get_list_log_behavior_in_sprint',
          sprintId
        }),
        null,
        data => {
          dispatch(updateStatus(GET_LIST_LOG_BEHAVIOR_IN_SPRINT_SUCCESS, data));
        },
        err => {
          dispatch(updateStatus(GET_LIST_LOG_BEHAVIOR_IN_SPRINT_FAILED, { error: err }));
          dispatch(updateStatus(CHECK_ERROR, { error: err }));
        }
      );
  };
};

export const removeUserSkillsInSprint = userSkillId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'remove_user_skill_in_sprint',
      userSkillId
    })
  };
  return dispatch => {
    dispatch(updateStatus(REMOVE_USER_SKILLS_IN_SPRINT_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(REMOVE_USER_SKILLS_IN_SPRINT_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(REMOVE_USER_SKILLS_IN_SPRINT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateAllocation = ({ allocationId, label, effortPercent, startDate, endDate }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_allocation',
      allocationId,
      label,
      effortPercent,
      startDate,
      endDate
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_ALLOCATION_REQUEST));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      res => {
        dispatch({ type: UPDATE_ALLOCATION_SUCCESS, allocation: res });
      },
      error => {
        dispatch({ type: UPDATE_ALLOCATION_FAILED, error });
        dispatch(updateStatus(CHECK_ERROR, { error }));
      }
    );
  };
};
