/*
 *=================================================================================================
 * Admin Project Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { CHECK_ERROR } from '../07-constants/common';
import { updateStatus } from '../08-helpers/common';
import {
  PROJECT_GET_CURRENT_USERS_PROGRESS,
  PROJECT_GET_CURRENT_USERS_SUCCESS,
  PROJECT_GET_CURRENT_USERS_FAILED,
  UPDATE_MENTOR_USER_PROGRESS,
  UPDATE_MENTOR_USER_SUCCESS,
  UPDATE_MENTOR_USER_FAILED,
  PROJECT_GET_ADMIN_PROGRESS,
  PROJECT_GET_ADMIN_SUCCESS,
  PROJECT_GET_ADMIN_FAILED,
  ADD_USER_PROGRESS,
  ADD_USER_SUCCESS,
  ADD_USER_FAILED,
  PROJECT_UPDATE_USER_SUCCESS,
  PROJECT_UPDATE_USER_PROGRESS,
  PROJECT_UPDATE_USER_FAILED
} from '../07-constants/adminproject';

export const getListAdmin = (username, sort) => {
  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_ADMIN_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'list_users',
        username,
        sort,
        isActive: true
      }),
      null,
      data => {
        dispatch(updateStatus(PROJECT_GET_ADMIN_SUCCESS, { listAdmins: data }));
      },
      err => {
        dispatch(updateStatus(PROJECT_GET_ADMIN_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getUsers = type => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'list_current_users',
      type
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_GET_CURRENT_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(PROJECT_GET_CURRENT_USERS_SUCCESS, { listUsers: data }));
      },
      err => {
        dispatch(updateStatus(PROJECT_GET_CURRENT_USERS_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateUser = params => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_user_role',
      userRoleId: params.id,
      userId: params.userId,
      newRoleId: params.roleId
    })
  };

  return dispatch => {
    dispatch(updateStatus(PROJECT_UPDATE_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      res => {
        const Role = { id: params.roleId, name: params.roleName };
        const data = { ...res, Role };
        dispatch(updateStatus(PROJECT_UPDATE_USER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(PROJECT_UPDATE_USER_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const addUser = params => {
  return dispatch => {
    dispatch(updateStatus(ADD_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig({
        action: 'add_user',
        userId: params.userId,
        username: params.username,
        roleId: params.roleId
      }),
      null,
      res => {
        const Role = { id: params.roleId, name: params.roleName };
        const data = { ...res, role: { ...res.role, Role } };
        dispatch(updateStatus(ADD_USER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(ADD_USER_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const updateUserMentor = (userId, mentorId) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'api_call_will_replace_this'
    },
    body: JSON.stringify({
      action: 'update_user_mentor',
      userId: userId,
      mentor: mentorId
    })
  };

  return dispatch => {
    dispatch(updateStatus(UPDATE_MENTOR_USER_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(UPDATE_MENTOR_USER_SUCCESS, data));
      },
      err => {
        dispatch(updateStatus(UPDATE_MENTOR_USER_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
