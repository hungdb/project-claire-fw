/*
 *=================================================================================================
 * Alert System Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { updateStatus, getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';

import {
  LIST_ALERT_SUBSCRIPTION_PROGRESS,
  LIST_ALERT_SUBSCRIPTION_SUCCESS,
  LIST_ALERT_SUBSCRIPTION_FAILED,
  LIST_USERS_PROGRESS,
  LIST_USERS_SUCCESS,
  LIST_USERS_FAILED,
  LIST_PROJECT_PROGRESS,
  LIST_PROJECT_SUCCESS,
  LIST_PROJECT_FAILED
} from '../07-constants/alert-system';

export const getListAlertSystem = ({ page, limit, userId, projectId }) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'get_list_alert_subscription',
      page,
      limit,
      userId,
      projectId
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_ALERT_SUBSCRIPTION_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(
          updateStatus(LIST_ALERT_SUBSCRIPTION_SUCCESS, {
            listAlertSystem: result.data,
            total: result.total
          })
        );
      },
      err => {
        dispatch(updateStatus(LIST_ALERT_SUBSCRIPTION_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListUser = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_current_users',
      isActive: true
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_USERS_PROGRESS));
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch(updateStatus(LIST_USERS_SUCCESS, { listUser: result.users }));
      },
      err => {
        dispatch(updateStatus(LIST_USERS_FAILED, err));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};

export const getListProject = () => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_projects'
    })
  };

  return dispatch => {
    dispatch(updateStatus(LIST_PROJECT_PROGRESS));
    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      data => {
        dispatch(updateStatus(LIST_PROJECT_SUCCESS, { listProject: data }));
      },
      err => {
        dispatch(updateStatus(LIST_PROJECT_FAILED, { error: err }));
        dispatch(updateStatus(CHECK_ERROR, { error: err }));
      }
    );
  };
};
