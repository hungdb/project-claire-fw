/*
 *=================================================================================================
 * User Note Actions
 *=================================================================================================
 */
import { getEnv } from '../env';
import { callApi } from '../05-utils/commonUtils';
import { getTokenBearer } from '../08-helpers/common';
import { CHECK_ERROR } from '../07-constants/common';
import {
  CREATE_USER_NOTE,
  CREATE_USER_NOTE_SUCCESS,
  CREATE_USER_NOTE_FAILED,
  GET_USER_NOTES,
  GET_USER_NOTES_SUCCESS,
  GET_USER_NOTES_FAILED,
  UPDATE_USER_NOTE,
  UPDATE_USER_NOTE_SUCCESS,
  UPDATE_USER_NOTE_FAILED,
  DELETE_USER_NOTE,
  DELETE_USER_NOTE_SUCCESS,
  DELETE_USER_NOTE_FAILED
} from '../07-constants/user-note';

const createUserNote = (userId, content) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'create_user_note',
      userId,
      content
    })
  };

  return dispatch => {
    dispatch({ type: CREATE_USER_NOTE });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch({
          type: CREATE_USER_NOTE_SUCCESS
        });
      },
      error => {
        dispatch({
          type: CREATE_USER_NOTE_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

const getUserNotes = userId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'list_notes_of_specific_user',
      userId
    })
  };

  return dispatch => {
    dispatch({ type: GET_USER_NOTES });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch({
          type: GET_USER_NOTES_SUCCESS,
          userNotes: result
        });
      },
      error => {
        dispatch({
          type: GET_USER_NOTES_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

const updateUserNote = (userNoteId, content) => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'update_user_note',
      userNoteId,
      content
    })
  };

  return dispatch => {
    dispatch({ type: UPDATE_USER_NOTE });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      result => {
        dispatch({
          type: UPDATE_USER_NOTE_SUCCESS,
          userNoteUpdated: result
        });
      },
      error => {
        dispatch({
          type: UPDATE_USER_NOTE_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

const deleteUserNote = userNoteId => {
  const config = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenBearer()
    },
    body: JSON.stringify({
      action: 'remove_user_note',
      userNoteId
    })
  };

  return dispatch => {
    dispatch({ type: DELETE_USER_NOTE });

    return callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      config,
      null,
      () => {
        dispatch({
          type: DELETE_USER_NOTE_SUCCESS,
          userNoteIdDeleted: userNoteId
        });
      },
      error => {
        dispatch({
          type: DELETE_USER_NOTE_FAILED,
          error
        });
        dispatch({
          type: CHECK_ERROR,
          data: { error }
        });
      }
    );
  };
};

export { createUserNote, getUserNotes, updateUserNote, deleteUserNote };
