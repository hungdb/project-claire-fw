import React, { memo } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';
import { customBorderColor } from '../../08-helpers/common';

function ProjectRoleSelector({ projectRoles, onChange }) {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Select
          placeholder={t('select_project_role')}
          options={projectRoles.map(p => ({ label: p.name, value: p.id }))}
          styles={customBorderColor}
          onChange={value => {
            onChange && onChange(value);
          }}
        />
      )}
    </NamespacesConsumer>
  );
}

ProjectRoleSelector.propTypes = {
  projectRoles: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired
};

const mapStateToProps = store => {
  return {
    projectRoles: store.permissionScheme.projectRoles
  };
};

export default connect(mapStateToProps)(memo(ProjectRoleSelector));
