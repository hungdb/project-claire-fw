import React from 'react';
import RenderRoutes from '../09-routes/RenderRoutes';
import { ToastContainer } from 'react-toastify';
import { object } from 'prop-types';

const App = ({ route }) => (
  <div className="app">
    <ToastContainer />
    <RenderRoutes routes={route.routes} />
  </div>
);

App.propTypes = {
  route: object
};

App.defaultProps = {};

export default App;
