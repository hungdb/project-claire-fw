import { array, bool, func, object } from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { getPermissionSchemes } from '../../../01-actions/permission-scheme';
import SelectPropsBox from '../../../03-components/SelectPropsBox';

function AddProjectDialog({
  loadPermissionSchemes,
  onClose,
  onOK,
  isOpen,
  permissionSchemes,
  value,
  projectGroups,
  workflows
}) {
  const [projectName, setProjectName] = useState('');
  const [description, setDescription] = useState('');
  const [projectGroupId, setProjectGroupId] = useState(null);
  const [projectWorkflowId, setProjectWorkflowId] = useState(null);
  const [projectPermissionSchemeId, setProjectPermissionSchemeId] = useState(null);

  useEffect(() => {
    loadPermissionSchemes();
  }, []);

  useEffect(() => {
    if (isOpen) {
      setProjectName('');
      setDescription('');
      setProjectGroupId(null);
      setProjectWorkflowId(null);
      setProjectPermissionSchemeId(null);
    }
  }, [isOpen]);

  const groupId = !projectGroupId && value ? value.projectGroupId : projectGroupId;
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={() => onClose && onClose()} centered backdrop="static">
          <ModalHeader toggle={() => onClose && onClose()}>{t('new_project')}</ModalHeader>
          <ModalBody className={'overflow-visible'}>
            <div className="content-modal d-flex flex-column align-items-stretch">
              <Input
                onChange={e => setProjectName(e.target.value)}
                className={'flex-grow-1 focus-border'}
                autoFocus
                placeholder={t('project_name')}
              />
              <Input
                type="textarea"
                className={'margin-top-15px focus-border'}
                row="4"
                placeholder={t('description')}
                onChange={e => setDescription(e.target.value)}
              />
              <div className={'margin-top-15px'}>
                <SelectPropsBox
                  options={projectGroups}
                  value={groupId}
                  onChange={e => setProjectGroupId(e.value)}
                  keyValue="id"
                  keyLabel="name"
                  isClearable={false}
                  placeholder="select_project_group"
                />
              </div>
              <div className={'margin-top-15px'}>
                <SelectPropsBox
                  options={workflows}
                  value={projectWorkflowId}
                  onChange={e => setProjectWorkflowId(e.value)}
                  keyValue="id"
                  keyLabel="name"
                  isClearable={false}
                  placeholder="select_project_workflow"
                />
              </div>
              <div className={'margin-top-15px'}>
                <SelectPropsBox
                  options={permissionSchemes}
                  value={projectPermissionSchemeId}
                  onChange={e => setProjectPermissionSchemeId(e.value)}
                  keyValue="id"
                  keyLabel="name"
                  isClearable={false}
                  placeholder="select_project_permission_scheme"
                />
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <div>
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onClose && onClose();
                  }}
                >
                  {t('cancel')}
                </Button>
                <Button
                  disabled={!(projectName && groupId && projectWorkflowId && projectPermissionSchemeId)}
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onOK &&
                      onOK({
                        projectName,
                        description,
                        projectGroupId: groupId,
                        projectWorkflowId,
                        projectPermissionSchemeId
                      });
                  }}
                >
                  {t('add')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

AddProjectDialog.propTypes = {
  loadPermissionSchemes: func,
  onClose: func,
  onOK: func,
  isOpen: bool,
  permissionSchemes: array,
  projectGroups: array,
  workflows: array,
  value: object
};

const mapStateToProps = store => {
  return {
    permissionSchemes: store.permissionScheme.permissionSchemes,
    projectGroups: store.group.projectGroups,
    workflows: store.workflow.workflows
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadPermissionSchemes: () => {
      dispatch(getPermissionSchemes());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(AddProjectDialog));
