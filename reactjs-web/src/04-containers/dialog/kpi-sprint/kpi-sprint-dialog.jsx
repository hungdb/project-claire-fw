import { array, bool, func, object } from 'prop-types';
import React, { Fragment, memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import Select from 'react-select';
import { Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { POINT_KPI_LEADER as point } from '../../../05-utils/commonData';
import './stylesKpiSprintDialog.css';

function KpiSprintDialog({
  editMode,
  isOpen,
  itemKpiSprint,
  onClose,
  onCreateKPISprint,
  onUpdate,
  listProject,
  onRemove
}) {
  const [projectState, setProjectState] = useState(null);
  const [sprintState, setSprintState] = useState(null);
  const [projectManagerState, setProjectManagerState] = useState(null);
  const [workManagementPointState, setWorkManagementPointState] = useState(null);
  const [peopleManagementPointState, setPeopleManagementPointState] = useState(null);
  const [processPointState, setProcessPointState] = useState(null);

  let project = projectState ? projectState : listProject && listProject[0];
  project = editMode ? (itemKpiSprint && itemKpiSprint.project) || project : project;
  const listSprint = (project && project.sprints) || [];
  let sprint = sprintState || (listSprint && listSprint[0]);
  sprint = editMode ? (itemKpiSprint && itemKpiSprint.sprint) || sprint : sprint;
  const leaders = (project && project.leaders) || [];
  let projectManager = projectManagerState || (leaders && leaders[0]);
  projectManager = editMode ? (itemKpiSprint && itemKpiSprint.user) || projectManager : projectManager;
  let workPoint = editMode ? point.find(a => a.value === itemKpiSprint.workManagementPoint) || point[1] : point[1];
  workPoint = workManagementPointState || workPoint;
  let peoplePoint = editMode ? point.find(a => a.value === itemKpiSprint.peopleManagementPoint) || point[1] : point[1];
  peoplePoint = peopleManagementPointState || peoplePoint;
  let processPoint = editMode ? point.find(a => a.value === itemKpiSprint.processPoint) || point[1] : point[1];
  processPoint = processPointState || processPoint;

  const clearAll = () => {
    setProjectState(null);
    setSprintState(null);
    setProjectManagerState(null);
    setWorkManagementPointState(null);
    setPeopleManagementPointState(null);
    setProcessPointState(null);
  };

  const checkConditionUpdate = (
    workManagementPointState,
    peopleManagementPointState,
    processPointState,
    itemUpdate
  ) => {
    if (
      workManagementPointState &&
      workManagementPointState.value &&
      workManagementPointState.value !== itemUpdate.workManagementPoint
    ) {
      return true;
    } else if (
      peopleManagementPointState &&
      peopleManagementPointState.value &&
      peopleManagementPointState.value !== itemUpdate.peopleManagementPoint
    ) {
      return true;
    } else if (processPointState && processPointState.value && processPointState.value !== itemUpdate.processPoint) {
      return true;
    }
    return false;
  };

  const handleUpdateKpiSprint = itemUpdate => {
    if (
      (workManagementPointState === null && peopleManagementPointState === null && processPointState === null) ||
      !checkConditionUpdate(workManagementPointState, peopleManagementPointState, processPointState, itemUpdate)
    ) {
      onClose && onClose();
      clearAll();
    } else {
      const _itemUpdate = {
        kpiSprintId: itemUpdate.id,
        workManagementPoint:
          (workManagementPointState && workManagementPointState.value) || itemUpdate.workManagementPoint,
        peopleManagementPoint:
          (peopleManagementPointState && peopleManagementPointState.value) || itemUpdate.peopleManagementPoint,
        processPoint: (processPointState && processPointState.value) || itemUpdate.processPoint,
        user: itemUpdate.user
      };
      onUpdate && onUpdate(_itemUpdate);
      clearAll();
    }
  };

  const handleButtonRemove = itemRemove => {
    onRemove && onRemove(itemRemove);
    clearAll();
  };

  const handleButtonAddKpiSprint = (projectManager, sprint, project, workPoint, peoplePoint, processPoint) => {
    const listUsers = (project && project.users) || [];
    if (listUsers && listUsers.length > 0 && projectManager && sprint && project) {
      const user = listUsers.find(a => {
        if (a && a.User && a.User.name && a.User.name === projectManager.name) {
          return a;
        }
        return false;
      });
      const kpiCreation = {
        userId: user && user.userId,
        sprintId: sprint.id,
        projectId: project.id,
        workManagementPoint: workPoint.value,
        peopleManagementPoint: peoplePoint.value,
        processPoint: processPoint.value,
        user: user && user.User,
        sprint,
        project
      };
      onCreateKPISprint && onCreateKPISprint(kpiCreation);
      clearAll();
    }
  };

  const renderContentDialog = (
    t,
    listProject,
    point,
    project,
    listSprint,
    sprint,
    leaders,
    projectManager,
    workPoint,
    peoplePoint,
    processPoint
  ) => {
    return (
      <ModalBody>
        <div className="modal-body-dialog">
          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.project')}</Label>
            <Select
              isClearable={false}
              isSearchable={false}
              isDisabled={editMode}
              value={{
                label: (project && project.name) || '',
                value: (project && project.id) || ''
              }}
              onChange={e => {
                const indexProject = listProject.findIndex(a => a.name.toLowerCase() === e.label.toLowerCase());
                if (indexProject !== -1) {
                  setProjectState(listProject[indexProject]);
                  setSprintState(null);
                  setProjectManagerState(null);
                }
              }}
              options={listProject.map(item => ({ label: item.name, value: item.id }))}
            />
          </FormGroup>
          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.sprint')}</Label>
            <Select
              isDisabled={editMode}
              isClearable={false}
              isSearchable={false}
              value={{
                label: (sprint && sprint.name) || '',
                value: (sprint && sprint.id) || ''
              }}
              onChange={e => {
                const indexSprint = listSprint.findIndex(a => a.name.toLowerCase() === e.label.toLowerCase());
                indexSprint !== -1 && setSprintState(listSprint[indexSprint]);
              }}
              options={listSprint.map(item => ({ label: item.name, value: item.id }))}
            />
          </FormGroup>

          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.leader')}</Label>
            <Select
              isDisabled={editMode}
              isClearable={false}
              isSearchable={false}
              value={{
                label: (projectManager && projectManager.name) || '',
                value: (projectManager && projectManager.name) || ''
              }}
              onChange={e => {
                const indexProjectManager = leaders.findIndex(a => a.name.toLowerCase() === e.label.toLowerCase());
                indexProjectManager !== -1 && setProjectManagerState(leaders[indexProjectManager]);
              }}
              options={leaders.map(item => ({ label: item.name, value: item.id }))}
            />
          </FormGroup>
          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.work_management')}</Label>
            <Select
              isClearable={false}
              isSearchable={false}
              value={{
                label: workPoint.label,
                value: workPoint.value
              }}
              onChange={e => {
                const indexWorkPoint = point.findIndex(a => a.value === e.value);
                indexWorkPoint !== -1 && setWorkManagementPointState(point[indexWorkPoint]);
              }}
              options={point.map(item => ({ label: item.label, value: item.value }))}
            />
          </FormGroup>
          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.people_management')}</Label>
            <Select
              isClearable={false}
              isSearchable={false}
              value={{
                label: peoplePoint.label,
                value: peoplePoint.value
              }}
              onChange={e => {
                const indexPeoplePoint = point.findIndex(a => a.value === e.value);
                indexPeoplePoint !== -1 && setPeopleManagementPointState(point[indexPeoplePoint]);
              }}
              options={point.map(item => ({ label: item.label, value: item.value }))}
            />
          </FormGroup>
          <FormGroup className="mb-4">
            <Label>{t('kpi_leader_sprint.process_management')}</Label>
            <Select
              isClearable={false}
              isSearchable={false}
              value={{
                label: processPoint.label,
                value: processPoint.value
              }}
              onChange={e => {
                const indexProcessPoint = point.findIndex(a => a.value === e.value);
                indexProcessPoint !== -1 && setProcessPointState(point[indexProcessPoint]);
              }}
              options={point.map(item => ({ label: item.label, value: item.value }))}
            />
          </FormGroup>
        </div>
      </ModalBody>
    );
  };

  const renderButtonDialog = (t, projectManager, sprint, project, workPoint, peoplePoint, processPoint) => {
    return (
      <ModalFooter>
        <div className={'flex-grow-1 d-flex justify-content-between'}>
          <div>
            {editMode && (
              <Button
                className={'outline-base-remove-btn'}
                size={'sm'}
                onClick={() => {
                  handleButtonRemove(itemKpiSprint);
                  clearAll();
                }}
              >
                {t('remove')}
              </Button>
            )}
          </div>
          <div>
            <Button
              className={'margin-right-10px outline-base-btn'}
              size={'sm'}
              onClick={() => {
                onClose && onClose();
                clearAll();
              }}
            >
              {t('cancel')}
            </Button>
            <Button
              disabled={false}
              className={'base-btn'}
              size={'sm'}
              onClick={() => {
                editMode && handleUpdateKpiSprint(itemKpiSprint);
                !editMode &&
                  handleButtonAddKpiSprint(projectManager, sprint, project, workPoint, peoplePoint, processPoint);
              }}
            >
              {t(editMode ? 'save' : 'add')}
            </Button>
          </div>
        </div>
      </ModalFooter>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal
          isOpen={isOpen}
          centered
          toggle={() => {
            onClose && onClose();
            clearAll();
          }}
          backdrop="static"
        >
          <Fragment>
            <ModalHeader
              toggle={() => {
                onClose && onClose();
                clearAll();
              }}
            >
              {editMode ? t('kpi_leader_sprint.update_kpi_sprint') : t('kpi_leader_sprint.add_kpi_sprint')}
            </ModalHeader>
            {renderContentDialog(
              t,
              listProject,
              point,
              project,
              listSprint,
              sprint,
              leaders,
              projectManager,
              workPoint,
              peoplePoint,
              processPoint
            )}
            {renderButtonDialog(t, projectManager, sprint, project, workPoint, peoplePoint, processPoint)}
          </Fragment>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

KpiSprintDialog.propTypes = {
  editMode: bool,
  isOpen: bool,
  itemKpiSprint: object,
  onClose: func,
  onCreateKPISprint: func,
  onUpdate: func,
  listProject: array,
  onRemove: func
};

export default memo(KpiSprintDialog);
