import React, { memo, useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import { object, array, func, number } from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {
  addUserIntoProject,
  getAllUsers,
  removeUserFromProject,
  updateUserRoleInProject
} from '../../../01-actions/userInProject';
import { listRole } from '../../../01-actions/userInProject';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import './DialogUserInProject.css';
import { mapArrayGetValue } from '../../../08-helpers/common';

const DialogUserInProject = ({
  selectedProject,
  usersInProject,
  users,
  allRoles,
  getAllUsers,
  listRole,
  addUserIntoProject,
  onClose,
  updateUserRoleInProject,
  removeUserFromProject,
  currentUser
}) => {
  const [state, setState] = useState({
    currentUserId: null,
    currentRoleIds: [],
    isOpenConfirmModal: false
  });

  const { currentUserId, currentRoleIds, isOpenConfirmModal } = state;

  useEffect(() => {
    getAllUsers();
    listRole();
  }, []);

  useEffect(() => {
    if (currentUser) {
      const currentRoleIds = usersInProject
        .filter(userRole => userRole.userId === currentUser)
        .map(userRole => ({
          value: userRole.ProjectRole && userRole.ProjectRole.id,
          label: userRole.ProjectRole && userRole.ProjectRole.name
        }));
      setState({
        ...state,
        currentUserId: currentUser,
        currentRoleIds
      });
    }
  }, [currentUser]);

  const handleSelectUser = e => {
    setState({
      currentUserId: e && e.value,
      currentRoleIds: []
    });
  };

  const handleSelectRole = roleIds => {
    setState({
      ...state,
      currentRoleIds: roleIds
    });
  };

  const canNotSubmit = () => {
    const userId = currentUser || currentUserId;
    return !userId || !currentRoleIds.length;
  };

  const create = () => {
    const projectRoleIds = mapArrayGetValue(currentRoleIds);
    addUserIntoProject({
      userId: currentUserId,
      projectId: selectedProject.id,
      projectRoleIds
    });
    onClose && onClose();
  };

  const update = () => {
    const roleIds = mapArrayGetValue(currentRoleIds);
    updateUserRoleInProject({
      projectId: selectedProject.id,
      userId: currentUser,
      roleIds
    });
    onClose && onClose();
  };

  const deleteItem = () => {
    removeUserFromProject({
      userId: currentUser,
      projectId: selectedProject.id
    });
    onClose && onClose();
  };

  const openConfirmDialog = () => setState({ ...state, isOpenConfirmModal: true });
  const onCloseConfirmDialog = () => setState({ ...state, isOpenConfirmModal: false });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Modal isOpen={true} toggle={() => onClose && onClose()} centered backdrop="static">
            <ModalHeader>{currentUser ? t('edit_user_in_project') : t('add_user_into_project')}</ModalHeader>
            <ModalBody className={'overflow-visible'}>
              <SelectPropsBox
                options={users}
                keyValue="id"
                keyLabel="name"
                classes="mb-3"
                placeholder="select_user"
                value={currentUserId}
                isDisabled={!!currentUser}
                onChange={handleSelectUser}
              />
              <SelectPropsBox
                isDisabled={!currentUser && !currentUserId}
                placeholder={t('select_role')}
                value={currentRoleIds}
                keyValue="id"
                keyLabel="name"
                isMulti
                options={allRoles}
                onChange={handleSelectRole}
              />
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                {currentUser && (
                  <Button className={'outline-base-remove-btn'} size={'sm'} onClick={openConfirmDialog}>
                    {t('remove')}
                  </Button>
                )}
                <Button
                  className={'margin-right-10px ml-auto  outline-base-btn'}
                  size={'sm'}
                  onClick={() => onClose && onClose()}
                >
                  {t('cancel')}
                </Button>
                <Button
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => (currentUser ? update() : create())}
                  disabled={canNotSubmit()}
                >
                  {currentUser ? t('save') : t('add')}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
          <ConfirmationModal isOpen={isOpenConfirmModal} funConfirm={deleteItem} funClose={onCloseConfirmDialog}>
            {t('are_you_sure')}
          </ConfirmationModal>
        </>
      )}
    </NamespacesConsumer>
  );
};

DialogUserInProject.propTypes = {
  selectedProject: object,
  usersInProject: array,
  users: array,
  allRoles: array,
  getAllUsers: func,
  listRole: array,
  addUserIntoProject: func,
  onClose: func,
  updateUserRoleInProject: func,
  removeUserFromProject: func,
  currentUser: number
};

const mapStateToProps = store => {
  return {
    selectedProject: store.project.selectedProject,
    usersInProject: store.userInProject.usersInProject,
    users: store.userInProject.users,
    allRoles: store.userInProject.listRole
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllUsers,
      listRole,
      addUserIntoProject,
      removeUserFromProject,
      updateUserRoleInProject
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(DialogUserInProject));
