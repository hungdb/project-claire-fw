import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import { object, array, func } from 'prop-types';
import Select from 'react-select';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { addAllUserIntoProject } from '../../../01-actions/userInProject';
import { listRole } from '../../../01-actions/userInProject';
import './DialogUserInProject.css';

class DialogUsersInProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRoleIds: []
    };
  }

  static propTypes = {
    selectedProject: object,
    allRoles: array,
    listRole: func,
    onClose: func,
    addAllUserIntoProject: func
  };

  componentDidMount() {
    this.props.listRole();
  }
  handleSelectRole(roleIds) {
    this.setState({
      currentRoleIds: roleIds
    });
  }

  canNotSubmit() {
    //-- check if the role existed
    return !this.state.currentRoleIds.length;
  }

  addAllUsers() {
    this.props.addAllUserIntoProject({
      projectId: this.props.selectedProject.id,
      projectRoleIds: this.state.currentRoleIds
    });
    this.props.onClose && this.props.onClose();
  }

  _renderRoleSelector(t) {
    const { allRoles } = this.props;
    const { currentRoleIds } = this.state;
    const filteredRoles = allRoles;
    let selectedRoles = allRoles.filter(r => currentRoleIds.includes(r.id));
    selectedRoles = selectedRoles.map(r => ({ label: r.name, value: r.id }));

    return (
      <Select
        placeholder={t('select_role')}
        value={selectedRoles}
        isMulti={true}
        options={filteredRoles.map(p => ({ label: p.name, value: p.id }))}
        onChange={e => {
          this.handleSelectRole(e.map(item => item.value));
        }}
      />
    );
  }

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal
            isOpen={true}
            toggle={() => {
              this.props.onClose && this.props.onClose();
            }}
            centered
            backdrop="static"
          >
            <ModalHeader
              toggle={() => {
                this.props.onClose && this.props.onClose();
              }}
            >
              {t('add_all_users_into_project')}
            </ModalHeader>
            <ModalBody className={'overflow-visible'}>
              <div>
                <div>{this._renderRoleSelector(t)}</div>
              </div>
            </ModalBody>
            <ModalFooter className="d-flex justify-content-between">
              <div></div>
              <div>
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    this.props.onClose && this.props.onClose();
                  }}
                >
                  {t('cancel')}
                </Button>
                <Button
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => this.addAllUsers()}
                  disabled={this.canNotSubmit()}
                >
                  {t('add')}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    selectedProject: store.project.selectedProject,
    allRoles: store.userInProject.listRole
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listRole,
      addAllUserIntoProject
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DialogUsersInProject);
