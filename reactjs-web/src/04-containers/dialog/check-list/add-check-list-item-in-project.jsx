import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import CheckListItemSelector from '../../check-list-item-selector/check-list-item-selector';
import { bool, func, object } from 'prop-types';

class AddCheckListItemInProjectDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItemCheckList: null
    };
  }

  static propTypes = {
    isOpen: bool,
    removeMode: bool,
    onClose: func,
    onOK: func,
    value: object
  };

  componentDidUpdate(prevProps) {
    const { isOpen: prevIsOpen } = prevProps;
    const { isOpen } = this.props;
    if (!prevIsOpen && isOpen) {
      this.setState({
        selectedItemCheckList: null
      });
    }
  }

  render() {
    const { onClose } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal isOpen={this.props.isOpen} toggle={() => onClose && onClose()} centered backdrop="static">
            <ModalHeader toggle={() => onClose && onClose()}>
              {t(this.props.removeMode ? 'remove_item_check_to_project' : 'add_item_check_to_project')}
            </ModalHeader>
            <ModalBody className={'overflow-visible'}>
              <div className="content-modal">
                {this.props.removeMode ? (
                  <p className="margin-top-15px">
                    {t('Are you sure you want to delete this item from a checklist project?')}{' '}
                  </p>
                ) : (
                  <CheckListItemSelector
                    onChange={e => {
                      this.setState({
                        selectedItemCheckList: e.value
                      });
                    }}
                  />
                )}
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div />
                <div>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => {
                      this.props.onClose && this.props.onClose();
                    }}
                  >
                    {t('cancel')}
                  </Button>
                  {this.props.removeMode ? (
                    <Button
                      className={'outline-base-remove-btn'}
                      size={'sm'}
                      onClick={() => {
                        this.props.onOK &&
                          this.props.onOK(true, {
                            itemId: this.props.value.itemCheck
                          });
                      }}
                    >
                      {t('remove')}
                    </Button>
                  ) : (
                    <Button
                      disabled={!this.state.selectedItemCheckList}
                      className={'base-btn'}
                      size={'sm'}
                      onClick={() => {
                        this.props.onOK &&
                          this.props.onOK(false, {
                            checkItemId: this.state.selectedItemCheckList
                          });
                      }}
                    >
                      {t('add')}
                    </Button>
                  )}
                </div>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default AddCheckListItemInProjectDialog;
