import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { object, bool, func } from 'prop-types';

class CheckListDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      id: 0
    };
  }

  static propTypes = {
    value: object,
    isOpen: bool,
    toggle: func,
    editMode: bool,
    onOK: func
  };

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    this.setState({
      id: nextProps.value ? nextProps.value.id : 0,
      name: nextProps.value ? nextProps.value.name : '',
      description: nextProps.value ? nextProps.value.description : ''
    });
  }

  onChangeValue = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onAddValue = () => {
    const { editMode, onOK, value } = this.props;
    const { name, description } = this.state;
    onOK &&
      onOK(editMode, 0, {
        id: editMode && value.id,
        name: name,
        description: description
      });
  };

  onRemoveValue = () => {
    const { onOK, editMode, value } = this.props;
    const { name, description } = this.state;
    onOK &&
      onOK(editMode, 1, {
        id: editMode && value.id,
        name: name,
        description: description
      });
  };

  render() {
    const { isOpen, toggle, editMode } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal
            centered
            fade={true}
            isOpen={isOpen}
            toggle={() => {
              toggle && toggle();
            }}
            backdrop="static"
          >
            <ModalHeader
              toggle={() => {
                toggle && toggle();
              }}
            >
              {t(editMode ? 'edit_item_check' : 'new_item_check')}
            </ModalHeader>
            <ModalBody>
              <div className="content-modal">
                <FormGroup>
                  <Label for="inputName">{t('name')}</Label>
                  <Input
                    type="text"
                    name="name"
                    id="inputName"
                    value={this.state.name}
                    onChange={e => this.onChangeValue(e)}
                    placeholder={t('something_to_do')}
                  />
                </FormGroup>
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-end'}>
                {editMode && (
                  <Button
                    size={'sm'}
                    onClick={() => this.onRemoveValue()}
                    className={'outline-base-remove-btn mr-auto'}
                  >
                    {t('remove')}
                  </Button>
                )}
                <Button
                  size={'sm'}
                  onClick={() => {
                    toggle && toggle();
                  }}
                  className={'margin-right-10px outline-base-btn'}
                >
                  {t('cancel')}
                </Button>
                <Button
                  disabled={!this.state.name}
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => this.onAddValue()}
                >
                  {t(editMode ? 'save' : 'add')}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default CheckListDialog;
