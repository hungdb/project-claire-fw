import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { TextField, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { NamespacesConsumer } from 'react-i18next';
import PermissionConditionSelector from '../../permission-condition-selector/permission-condition-selector';
import { object, bool, func } from 'prop-types';

class PermissionConditionDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setting: '',
      condition: ''
    };
  }

  static propTypes = {
    data: object,
    isOpen: bool,
    editMode: bool,
    onClose: func,
    onOK: func
  };

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    this.setState({ condition: nextProps.data ? nextProps.data.condition : '' });
    this.setState({ setting: nextProps.data ? nextProps.data.setting : '' });
  }

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            <Dialog
              PaperProps={{
                style: {
                  overflow: 'visible',
                  width: 600
                }
              }}
              open={this.props.isOpen}
              onClose={() => {
                this.props.onClose && this.props.onClose();
              }}
              scroll="body"
              aria-labelledby="form-dialog-title"
            >
              <div style={{ width: '580px', height: '100%', overflow: 'visible' }}>
                <DialogTitle id="form-dialog-title">
                  {t(!this.props.editMode ? 'New permission condition' : 'Update permission condition')}
                </DialogTitle>
                <DialogContent className={'overflow-visible'}>
                  <div className="content-modal d-flex flex-column align-items-stretch">
                    {!this.props.editMode ? (
                      <PermissionConditionSelector
                        onChange={async e => {
                          await this.setState({
                            condition: e.value
                          });
                        }}
                      />
                    ) : (
                      <TextField
                        defaultValue={this.state.condition}
                        className={'flex-grow-1 margin-top-15px'}
                        disabled={true}
                        label={t('condition')}
                      />
                    )}
                    <TextField
                      defaultValue={this.state.setting}
                      onChange={e => {
                        this.setState({
                          setting: e.target.value.replace(/\s/g, '')
                        });
                      }}
                      className={'flex-grow-1 margin-top-15px'}
                      autoFocus
                      label={t('Setting')}
                    />
                  </div>
                </DialogContent>
                <DialogActions>
                  <div className={'flex-grow-1 d-flex justify-content-between'}>
                    <div>
                      {this.props.editMode && (
                        <Button
                          className={'outline-base-remove-btn'}
                          size={'sm'}
                          onClick={() => {
                            this.props.onOK &&
                              this.props.onOK(this.props.editMode, 1, {
                                name: this.state.condition,
                                value: this.state.setting
                              });
                          }}
                        >
                          {t('remove')}
                        </Button>
                      )}
                    </div>
                    <div>
                      <Button
                        className={'margin-right-10px outline-base-btn'}
                        size={'sm'}
                        onClick={() => {
                          this.props.onClose && this.props.onClose();
                        }}
                      >
                        {t('cancel')}
                      </Button>
                      <Button
                        disabled={!this.state.condition}
                        className={'base-btn'}
                        size={'sm'}
                        onClick={() => {
                          this.props.onOK &&
                            this.props.onOK(this.props.editMode, 0, {
                              name: this.state.condition,
                              value: this.state.setting
                            });
                        }}
                      >
                        {t(this.props.editMode ? 'save' : 'add')}
                      </Button>
                    </div>
                  </div>
                </DialogActions>
              </div>
            </Dialog>
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

export default PermissionConditionDialog;
