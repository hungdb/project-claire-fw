import React, { PureComponent } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogContent from '@material-ui/core/DialogContent';
import { NamespacesConsumer } from 'react-i18next';
import { TextField } from '@material-ui/core';

class ChangeEstimateDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentTask: null,
      estimate: 0
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currentTask: nextProps.value ? nextProps.value : ''
    });
  }

  componentWillUnmount() {
    this.setState({
      currentTask: null,
      estimate: 0
    });
  }

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Dialog
            PaperProps={{
              style: {
                overflow: 'visible'
              }
            }}
            open={this.props.isOpen}
            onClose={() => {
              this.props.onClose && this.props.onClose();
            }}
          >
            <div style={{ width: '600px', overflow: 'visible' }}>
              <DialogTitle>{t('change_estimate')}</DialogTitle>
              <DialogContent>
                <DialogContentText>{t('about_estimate')}</DialogContentText>
                <div className="content-modal d-flex flex-column align-items-stretch">
                  <TextField
                    defaultValue={this.state.currentTask && this.state.currentTask.estimate}
                    type="number"
                    className={'flex-grow-1 margin-top-15px'}
                    onChange={e => this.setState({ estimate: e.target.value })}
                    label={t('Estimate')}
                  />
                </div>
              </DialogContent>
              <DialogActions>
                <div className={'flex-grow-1 d-flex justify-content-end'}>
                  <div>
                    <Button
                      className={'margin-right-10px'}
                      color={'default'}
                      variant="outlined"
                      size="small"
                      onClick={() => {
                        this.props.onClose && this.props.onClose();
                      }}
                    >
                      {t('cancel')}
                    </Button>
                    <Button
                      color={'primary'}
                      disabled={!this.state.estimate}
                      variant="contained"
                      size="small"
                      onClick={() => {
                        this.props.onOK &&
                          this.props.onOK({
                            taskId: this.state.currentTask && this.state.currentTask.id,
                            estimate: this.state.estimate,
                            projectId: this.state.currentTask && this.state.currentTask.projectId
                          });
                      }}
                    >
                      {t('add')}
                    </Button>
                  </div>
                </div>
              </DialogActions>
            </div>
          </Dialog>
        )}
      </NamespacesConsumer>
    );
  }
}

export default ChangeEstimateDialog;
