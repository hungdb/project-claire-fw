import { array, bool, func, object } from 'prop-types';
import React, { Fragment, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import { PROJECT_ACTIVE_STATUS } from '../../../05-utils/commonData';

function UpdateProjectDialog({ value, isOpen, onClose, onOK, onDelete, projectGroups }) {
  const [projectName, setProjectName] = useState('');
  const [groupId, setGroupId] = useState(null);
  const [projectRate, setProjectRate] = useState(0);
  const [isActive, setIsActive] = useState(null);
  const [isDelete, setIsDelete] = useState(false);

  useEffect(() => {
    if (isOpen && value) {
      const getValue = PROJECT_ACTIVE_STATUS.find(item => item.value === value.isActive);
      setProjectName(value.name);
      setProjectRate(value.projectRate);
      setIsActive(getValue);
      setGroupId(value.groupId);
    }
  }, [isOpen]);

  const canUpdate = () => {
    if (value) {
      return (
        (projectName && projectName !== value.name) ||
        (projectRate && projectRate !== value.projectRate) ||
        (groupId && groupId !== value.groupId) ||
        isActive !== value.isActive
      );
    }
    return false;
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Fragment>
          <Modal isOpen={isOpen} toggle={() => onClose && onClose()} centered backdrop="static">
            <ModalHeader toggle={() => onClose && onClose()}>{t('edit_project')}</ModalHeader>
            <ModalBody>
              <div className="content-modal d-flex">
                <Input
                  defaultValue={projectName}
                  onChange={e => setProjectName(e.target.value)}
                  className={'flex-grow-1'}
                  autoFocus
                  placeholder={t('project_name')}
                />
              </div>
              <div className={'margin-top-15px'}>
                <SelectPropsBox
                  options={projectGroups}
                  value={groupId}
                  onChange={e => setGroupId(e.value)}
                  keyValue="id"
                  keyLabel="name"
                  isClearable={false}
                  placeholder="select_project_group"
                />
              </div>
              <div className="content-modal d-flex mt-4">
                <SelectPropsBox
                  isClearable={false}
                  classes={'flex-grow-1'}
                  placeholder={'project_status'}
                  options={PROJECT_ACTIVE_STATUS}
                  onChange={e => setIsActive(e)}
                  value={isActive && isActive.value}
                />
              </div>
              <div className="content-modal d-flex mt-4">
                <Input
                  defaultValue={projectRate}
                  onChange={e => setProjectRate(e.target.value)}
                  className={'flex-grow-1'}
                  placeholder={t('project_rate')}
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div>
                  <Button
                    className={'margin-right-10px outline-base-remove-btn'}
                    size={'sm'}
                    onClick={() => setIsDelete(true)}
                  >
                    {t('remove')}
                  </Button>
                </div>
                <div>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => {
                      onClose && onClose();
                    }}
                  >
                    {t('cancel')}
                  </Button>
                  <Button
                    disabled={!canUpdate()}
                    className={'base-btn'}
                    size={'sm'}
                    onClick={() => {
                      onOK &&
                        onOK({
                          id: value.id,
                          name: projectName,
                          isActive: isActive && isActive.value,
                          groupId,
                          projectRate
                        });
                    }}
                  >
                    {t('save')}
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </Modal>
          <ConfirmationModal
            isOpen={isDelete}
            funConfirm={() => onDelete(value)}
            funClose={() => setIsDelete(false)}
            funCloseParent={() => onClose()}
          >
            {t('are_you_sure')}
          </ConfirmationModal>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

UpdateProjectDialog.propTypes = {
  value: object,
  isOpen: bool,
  onClose: func,
  onOK: func,
  onDelete: func,
  projectGroups: array
};

const mapStateToProps = store => {
  return {
    projectGroups: store.group.projectGroups
  };
};

export default connect(mapStateToProps)(UpdateProjectDialog);
