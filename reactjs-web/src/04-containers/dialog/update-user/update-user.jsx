import React, { Component, Fragment } from 'react';
import { Button, FormGroup, Label } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import RoleUserSelector from '../../role-user-selector/role-user-selector';
import { getFullname } from '../../../08-helpers/common';
import { object, bool, func } from 'prop-types';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';

class UpdateUserDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      firstname: '',
      lastname: '',
      roleId: '',
      roleName: '',
      roles: ''
    };
  }

  static propTypes = {
    value: object,
    isOpen: bool,
    onClose: func,
    onOK: func
  };

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    this.setState({
      username: nextProps.value ? nextProps.value.username : '',
      firstname: nextProps.value ? nextProps.value.firstname : '',
      lastname: nextProps.value ? nextProps.value.lastname : '',
      roles: nextProps.value ? nextProps.value.roles : ''
    });
  }

  componentWillUnmount() {
    this.setState({
      username: '',
      firstname: '',
      lastname: '',
      roleId: '',
      roleName: '',
      roles: ''
    });
  }

  _renderContent = t => {
    return (
      <Fragment>
        <FormGroup>
          <Label>{t('Full Name')}</Label>
          <FieldInput
            type="text"
            disabled={true}
            defaultValue={getFullname(this.state.firstname, this.state.lastname)}
          />
        </FormGroup>
        <FormGroup className={'margin-top-15px'}>
          <Label>{t('User Name')}</Label>
          <FieldInput type="text" disabled={true} defaultValue={this.state.username} />
        </FormGroup>
        <FormGroup className={'margin-top-30px'}>
          <RoleUserSelector
            values={this.state.roles}
            onChange={e => {
              this.setState({ roleId: e.value, roleName: e.label });
            }}
          />
        </FormGroup>
      </Fragment>
    );
  };

  _renderAction = t => {
    return (
      <Button
        disabled={!this.state.roleId}
        className={'base-btn'}
        size="sm"
        onClick={() => {
          this.props.onOK &&
            this.props.onOK({
              roleId: this.state.roleId,
              roleName: this.state.roleName
            });
        }}
      >
        {t('add')}
      </Button>
    );
  };

  render() {
    const { isOpen, onClose } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <BaseDialog
            title={t('update_role_user')}
            size="md"
            isOpen={isOpen}
            onClose={onClose}
            dialogContent={this._renderContent(t)}
            actionBtn={this._renderAction(t)}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

export default UpdateUserDialog;
