import React, { useState, useMemo, memo, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { bool, func, any, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import {
  getOptionUserRelate,
  mapArrayGetValue,
  filterUserRelateInProject,
  safelyParseJSON
} from '../../../08-helpers/common';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import FieldInput from '../../../03-components/FieldInput';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import { TASK_TYPE_OPTIONS } from '../../../05-utils/commonData';

const IssueDialog = ({ value, isOpen, onClose, onOK, issueItem, onRemove }) => {
  const [state, setState] = useState({
    subject: '',
    estimate: '',
    content: '',
    selectedOptionRelate: null,
    selectedAssignee: '',
    files: null,
    typeTask: TASK_TYPE_OPTIONS[0].value
  });
  const { subject, estimate, content, selectedOptionRelate, selectedAssignee, files, typeTask } = state;

  const typeTaskBug = typeTask === TASK_TYPE_OPTIONS[1].value;
  const optionUserRelate = useMemo(() => getOptionUserRelate(value.usersIntoProject), [value.usersIntoProject]);
  const userRelateMemo = useMemo(
    () => filterUserRelateInProject(value.usersIntoProject, issueItem && safelyParseJSON(issueItem.userRelate)),
    [issueItem]
  );

  const clearState = () => {
    setState({
      subject: '',
      content: '',
      estimate: '',
      selectedOptionRelate: null,
      selectedAssignee: null,
      files: null,
      typeTask: TASK_TYPE_OPTIONS[0].value
    });
  };

  useEffect(() => {
    setState({
      subject: issueItem ? issueItem.subject : '',
      content: issueItem ? issueItem.content : '',
      estimate: issueItem ? issueItem.estimate : '',
      selectedOptionRelate: userRelateMemo,
      selectedAssignee: issueItem ? issueItem.assignee : '',
      files: null,
      typeTask: issueItem ? issueItem.typeTask : TASK_TYPE_OPTIONS[0].value
    });
  }, [issueItem]);

  const onClickSubmit = () => {
    const userRelate = mapArrayGetValue(selectedOptionRelate);

    onOK({
      subject,
      content,
      estimate,
      userRelate,
      assignee: selectedAssignee,
      files,
      typeTask
    });
    clearState();
  };

  const onChangeAttachments = e => setState({ ...state, files: e.target.files });
  const changeContent = v => setState({ ...state, content: v });
  const onChangeInput = (value, name) => setState({ ...state, [name]: value });
  const onChangeOptionRelate = selectedOptionRelate => setState({ ...state, selectedOptionRelate });
  const onChangeAssignee = e => setState({ ...state, selectedAssignee: e && e.value });
  const onChangetypeTask = e =>
    setState({
      ...state,
      typeTask: e && e.value,
      selectedOptionRelate: typeTaskBug && []
    });

  return (
    <NamespacesConsumer>
      {t => (
        <>
          <Modal isOpen={isOpen} toggle={onClose} centered={true} size="lg" backdrop="static">
            <ModalHeader toggle={onClose}>{t(issueItem ? 'update_issue' : 'create_issue')}</ModalHeader>
            <ModalBody>
              <label>{t('subject')}</label>
              <FieldInput
                classes="mb-4"
                type="text"
                name="subject"
                onChangeValue={onChangeInput}
                defaultValue={subject}
              />
              <MarkdownEditor label={t('content')} onChange={changeContent} value={content} />
              <div className={'mt-3'}>
                <label className={'mb-1'}>{t('task_type')}</label>
                <SelectPropsBox
                  keyLabel="label"
                  keyValue="value"
                  value={typeTask}
                  options={TASK_TYPE_OPTIONS}
                  onChange={onChangetypeTask}
                />
              </div>
              <div className={'mt-3'}>
                <label className={'mb-1'}>{t('user_relate')}</label>
                <SelectPropsBox
                  isAnimatate
                  isMulti={true}
                  keyLabel="label"
                  keyValue="value"
                  isDisabled={!typeTaskBug}
                  value={selectedOptionRelate}
                  options={optionUserRelate}
                  onChange={onChangeOptionRelate}
                />
              </div>
              <div className={'mt-3'}>
                <label className={'mb-1'}>{t('assignee')}</label>
                <SelectPropsBox
                  keyLabel="label"
                  keyValue="value"
                  value={selectedAssignee}
                  options={optionUserRelate}
                  onChange={onChangeAssignee}
                />
              </div>
              <div>
                <label className="margin-top-15px">{t('estimate')}</label>
                <FieldInput
                  classes="mb-4"
                  type="number"
                  name="estimate"
                  defaultValue={estimate}
                  onChangeValue={onChangeInput}
                />
              </div>
              {!issueItem && (
                <div>
                  <label className={'margin-top-15px'}>{t('attachments')}</label>
                  <br />
                  <small style={{ color: 'red' }}>* {t('limit_3_files')}</small>
                  <br />
                  <input type="file" multiple onChange={onChangeAttachments} />
                </div>
              )}
            </ModalBody>
            <ModalFooter className="d-flex">
              {issueItem && (
                <Button className={'mr-auto outline-base-remove-btn'} size={'sm'} onClick={onRemove}>
                  {t('remove')}
                </Button>
              )}
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                {t('close')}
              </Button>
              <Button size={'sm'} disabled={!subject} className={'base-btn'} onClick={onClickSubmit}>
                {t('save')}
              </Button>
            </ModalFooter>
          </Modal>
        </>
      )}
    </NamespacesConsumer>
  );
};

IssueDialog.propTypes = {
  isOpen: bool,
  onClose: func,
  onOK: func,
  value: any,
  issueItem: object,
  onRemove: func
};

export default memo(IssueDialog);
