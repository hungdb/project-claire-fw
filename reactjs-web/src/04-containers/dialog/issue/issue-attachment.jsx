import React, { useState, useEffect, Fragment, memo } from 'react';
import { Button, Row, Col } from 'reactstrap';
import { func, number, array, object, bool, any } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { PAGINATE_ATTACHMENT_LIMIT, TOAST_ERROR_STYLE } from '../../../05-utils/commonData';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  calculateLength,
  formatDateTime,
  getIconFile,
  isAllowPermission,
  truncateText
} from '../../../08-helpers/common';
import {
  getAttachmentsOfIssue,
  loadMoreAttachmentsOfIssue,
  removeAttachmentOfIssue,
  uploadFileToIssue
} from '../../../01-actions/issue';
import { downloadFile } from '../../../01-actions/wiki';
import {
  DOWNLOAD_ATTACHMENT,
  PERM_REMOVE_ATTACHMENT_BY_ISSUE,
  PERM_ADD_ATTACHMENT_BY_ISSUE
} from '../../../07-constants/app-permission';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import { toast } from 'react-toastify';
import i18n from '../../../i18n';
import AppText from '../../../03-components/AppText';
import Upload from '../../../03-components/Upload';

function IssueAttachment({
  uploadFileToIssue,
  getAttachmentsOfIssue,
  loadMoreAttachmentsOfIssue,
  removeAttachmentOfIssue,
  downloadFile,
  attachmentsOfIssue,
  attachmentsOfIssueTotal,
  appPermissions,
  projectId,
  value,
  uploadEnable,
  deleteEnable
}) {
  const [openUpload, setOpenUpload] = useState(false);
  const [attachment, setAttachment] = useState(null);
  const [attachmentAction, setAttachmentAction] = useState(false);
  const [attachmentOffset, setAttachmentOffset] = useState(0);
  const [attachmentHoverActor, setAttachmentHoverActor] = useState(0);
  const [openAlert, setOpenAlert] = useState(false);

  useEffect(() => {
    getAttachmentsOfIssue({
      issueId: value.issueId,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset: attachmentOffset
    });
  }, []);

  const toggleUpload = () => setOpenUpload(!openUpload);

  const handleUpload = acceptedFiles => {
    if (!value.issueId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      uploadFileToIssue({
        files: acceptedFiles,
        issueId: value.issueId
      });
    }
    setAttachmentOffset(0);
  };

  const openAlertAttachment = attachment => {
    setOpenAlert(true);
    setAttachment(attachment);
  };

  const handleDownloadAttachment = attachment => {
    if (!projectId || !attachment || (attachment && !attachment.url)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      downloadFile({
        filePath: attachment.url,
        projectId
      });
    }
  };

  const _renderItemFile = attachment => {
    const isAttachmentHoverActor = attachmentAction && attachment.id === attachmentHoverActor;

    return (
      <div
        className={'file d-flex align-items-center mb-3 '}
        onMouseEnter={() => {
          setAttachmentAction(true);
          setAttachmentHoverActor(attachment.id);
        }}
        onMouseLeave={() => {
          setAttachmentAction(false);
          setAttachmentHoverActor(attachment.id);
        }}
      >
        <i className={`${getIconFile(attachment.mimeType)} mr-3 file-icon`} />
        <div className="w-100">
          <div title={attachment.name}>{truncateText(attachment.name, 30)}</div>
          <div className="d-flex justify-content-between align-items-center">
            <small>
              <i className="fa fa-clock-o" /> {formatDateTime(attachment.createdAt, 'LLLL', 1)}
            </small>
            {deleteEnable &&
              isAttachmentHoverActor &&
              isAllowPermission(appPermissions, PERM_REMOVE_ATTACHMENT_BY_ISSUE, projectId) && (
                <span className="at-btn-remove" onClick={() => openAlertAttachment(attachment)}>
                  <i className="fa fa fa-trash-o" />
                </span>
              )}
            {isAttachmentHoverActor && isAllowPermission(appPermissions, DOWNLOAD_ATTACHMENT, projectId) && (
              <span className="at-btn-download" onClick={() => handleDownloadAttachment(attachment)}>
                <i className="fa fa-eye" />
              </span>
            )}
          </div>
        </div>
      </div>
    );
  };

  const loadMoreAttachment = () => {
    loadMoreAttachmentsOfIssue({
      issueId: value.issueId,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset: attachmentOffset + PAGINATE_ATTACHMENT_LIMIT
    });
    setAttachmentOffset(attachmentOffset + PAGINATE_ATTACHMENT_LIMIT);
  };

  const closeAlert = () => setOpenAlert(false);

  const handleRemoveAttachment = () => {
    if (!attachment || (attachment && !attachment.id)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      removeAttachmentOfIssue({
        attachmentId: attachment.id
      });
    }
    closeAlert();
  };

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
            <AppText type={'title-menu'}>{t('attachments')}:</AppText>
            {uploadEnable && isAllowPermission(appPermissions, PERM_ADD_ATTACHMENT_BY_ISSUE, projectId) && (
              <Button className={'orange-btn'} size={'md'} onClick={() => toggleUpload()}>
                {!openUpload ? (
                  <i className={'fa fa-plus margin-right-5px'} />
                ) : (
                  <i className={'fa fa-minus margin-right-5px'} />
                )}
                {!openUpload ? t('upload_file') : t('close_upload_file')}
              </Button>
            )}
          </div>
          {uploadEnable && openUpload && (
            <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
              <Upload onDrop={(acceptedFiles, rejectedFiles) => handleUpload(acceptedFiles, rejectedFiles)} />
            </div>
          )}
          <Row className={'detail-task-box-attachment-area'}>
            {attachmentsOfIssue.map((attachment, index) => (
              <Col md="4" key={index}>
                {attachment && _renderItemFile(attachment)}
              </Col>
            ))}
          </Row>
          {calculateLength(attachmentsOfIssue) < attachmentsOfIssueTotal && (
            <div className={'text-center text-uppercase cursor-pointer'}>
              <i className="fa fa-repeat mr-1" />
              <span onClick={() => loadMoreAttachment()}>{t('view_more')}</span>
            </div>
          )}
          <ConfirmationModal
            isOpen={openAlert}
            txtTitle={t('confirm_remove')}
            txtCancel={t('cancel')}
            txtConfirm={t('confirm')}
            funConfirm={handleRemoveAttachment}
            funClose={closeAlert}
          >
            {t('are_you_sure_remove_this_attachment')}
          </ConfirmationModal>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

IssueAttachment.propTypes = {
  uploadFileToIssue: func,
  getAttachmentsOfIssue: func,
  loadMoreAttachmentsOfIssue: func,
  removeAttachmentOfIssue: func,
  downloadFile: func,
  attachmentsOfIssue: array,
  attachmentsOfIssueTotal: number,
  appPermissions: array,
  projectId: any,
  value: object,
  uploadEnable: bool,
  deleteEnable: bool
};

IssueAttachment.defaultProps = {
  uploadEnable: true,
  deleteEnable: true
};

const mapStateToProps = state => ({
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  attachmentsOfIssue: state.issue.attachmentsOfIssue,
  attachmentsOfIssueTotal: state.issue.attachmentsOfIssueTotal,
  appPermissions: state.auth.permissions
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAttachmentsOfIssue,
      loadMoreAttachmentsOfIssue,
      removeAttachmentOfIssue,
      uploadFileToIssue,
      downloadFile
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(IssueAttachment));
