import breaks from 'remark-breaks';
import ReactMarkdown from 'react-markdown';
import i18n from '../../../i18n';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import AppText from '../../../03-components/AppText';
import IssueAttachment from './issue-attachment';
import TaskType from '../../../03-components/TaskType';
import React, { useState, memo, useEffect, Fragment } from 'react';
import { Badge, Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { toast } from 'react-toastify';
import { string, any, func, object, number, array, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { PAGINATE_COMMENT_ISSUE_LIMIT, TOAST_ERROR_STYLE } from '../../../05-utils/commonData';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { calculateLength, formatDateTime, fromNow, isAllowPermission, linkImage } from '../../../08-helpers/common';
import { linkRenderTarget } from '../../../08-helpers/component';
import { getIssue } from '../../../01-actions/issue';
import { getUsersByProject } from '../../../01-actions/sprint';
import { PERM_UPDATE_COMMENT_ISSUE, PERM_DELETE_COMMENT_ISSUE } from '../../../07-constants/app-permission';
import {
  addCommentIssue,
  deleteCommentIssue,
  getCommentsOfIssue,
  updateCommentIssue,
  loadMoreCommentsOfIssue
} from '../../../01-actions/comment';
import {
  ADD_COMMENT_ISSUE_FAILED,
  ADD_COMMENT_ISSUE_SUCCESS,
  UPDATE_COMMENT_ISSUE_FAILED,
  UPDATE_COMMENT_ISSUE_SUCCESS
} from '../../../07-constants/comment';

function ViewIssueDialog({
  status,
  value,
  onClose,
  currentIssue,
  addCommentIssue,
  getCommentsOfIssue,
  updateCommentIssue,
  deleteCommentIssue,
  commentsOfIssue,
  commentsOfIssueTotal,
  userId,
  loadMoreCommentsOfIssue,
  usersIntoProject,
  appPermissions,
  projectId,
  isOpen,
  getIssue,
  getUsersByProject
}) {
  const [currentPage, setCurrentPage] = useState(1);
  const [commentAction, setCommentAction] = useState(false);
  const [commentHoverActor, setCommentHoverActor] = useState(null);
  const [isEditComment, setIsEditComment] = useState(false);
  const [commentIssueId, setCommentIssueId] = useState(0);
  const [content, setContent] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [openAlertComment, setOpenAlertComment] = useState(false);

  useEffect(() => {
    getCommentsOfIssue({
      issueId: value.issueId,
      limit: PAGINATE_COMMENT_ISSUE_LIMIT,
      page: currentPage
    });
    getIssue(value.issueId);
    getUsersByProject({ projectId });
  }, []);

  useEffect(() => {
    switch (status) {
      case ADD_COMMENT_ISSUE_SUCCESS:
      case ADD_COMMENT_ISSUE_FAILED:
      case UPDATE_COMMENT_ISSUE_SUCCESS:
      case UPDATE_COMMENT_ISSUE_FAILED:
        setIsLoading(false);
        break;
      default:
        break;
    }
  }, [status]);

  const loadMoreComments = () => {
    loadMoreCommentsOfIssue({
      issueId: value.issueId,
      limit: PAGINATE_COMMENT_ISSUE_LIMIT,
      page: currentPage + 1
    });
    setCurrentPage(currentPage + 1);
  };

  const handleOpenAlertComment = comment => {
    setOpenAlertComment(true);
    setCommentIssueId(comment.id);
  };

  const closeAlertComment = () => setOpenAlertComment(false);

  const handleRemoveComment = () => {
    if (!commentIssueId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      deleteCommentIssue({
        commentIssueId
      });
      setIsEditComment(false);
      setContent(null);
      setCommentIssueId(0);
    }
  };

  let userRelate = [];
  if (currentIssue.userRelate)
    userRelate = JSON.parse(currentIssue.userRelate).map(item => {
      const user = usersIntoProject.find(user => user.userId === item);
      return user && user.User && user.User.name;
    });

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          <Modal isOpen={isOpen} toggle={onClose} centered={true} size="xl" backdrop="static">
            <ModalHeader toggle={onClose} className={'view-issue'}>
              #{currentIssue.number} {currentIssue.subject}
            </ModalHeader>
            <ModalBody>
              <div className={'content-issue'}>
                <ReactMarkdown
                  plugins={[breaks]}
                  className="markdown-content"
                  source={currentIssue.content}
                  renderers={{ link: props => linkRenderTarget(props) }}
                />
              </div>
              <div className="d-flex mb-3">
                <p className="mb-0 mr-3">{t('task_type')}:</p>
                <TaskType type={currentIssue.typeTask} />
              </div>
              <div className="d-flex">
                {t('user_relate')}:
                {userRelate.length ? (
                  userRelate.map((user, i) => (
                    <Badge key={i} color="info" className="ml-2 align-items-center pt-1">
                      #{user}
                    </Badge>
                  ))
                ) : (
                  <span className={'margin-left-5px font-size-14 text-muted'}>{t('none')}</span>
                )}
              </div>
              <div className={'mt-3'}>
                <label className={'mb-1'}>
                  {`${t('assignee')}:`}
                  {currentIssue.assigneeAs && (
                    <Badge color="info" className="ml-2 align-items-center pt-1">
                      #{currentIssue.assigneeAs.name}
                    </Badge>
                  )}
                </label>
              </div>
              <div className={'mt-3'}>
                <label className={'mb-1'}>{`${t('estimate')}: ${currentIssue.estimate}`}</label>
              </div>
              <IssueAttachment value={value} />

              <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                <AppText type={'title-menu'}>{t('comments')}:</AppText>
              </div>

              {calculateLength(commentsOfIssue) < commentsOfIssueTotal ? (
                <div className="content">
                  <u className="more margin-top-10px" onClick={() => loadMoreComments()}>
                    {t('view_comments_older')}
                  </u>
                </div>
              ) : null}
              {calculateLength(commentsOfIssue) ? (
                <div className="view-issue">
                  {commentsOfIssue.map((comment, index) => {
                    return (
                      <div
                        className="panel panel-white post panel-shadow"
                        key={index}
                        onMouseEnter={() => {
                          if (userId === comment.userId) {
                            setCommentAction(true);
                            setCommentHoverActor(comment.id);
                          }
                        }}
                        onMouseLeave={() => {
                          if (userId === comment.userId) {
                            setCommentAction(false);
                            setCommentHoverActor(comment.id);
                          }
                        }}
                      >
                        <div className="post-heading">
                          <div className="pull-left image">
                            <img
                              src={linkImage(comment.User && comment.User.avatar)}
                              className="img-circle avatar"
                              alt="user profile"
                            />
                          </div>
                          <div className="pull-left meta">
                            <div className="title">
                              <b>@{comment.User && comment.User.name}</b>
                            </div>
                            <br />
                            <p className="text-muted time">
                              <i className="fa fa-clock-o margin-right-5px" />
                              {formatDateTime(comment.createdAt, 'LLLL', 1)}
                            </p>
                          </div>
                          <small className="right">{fromNow(comment.createdAt)}</small>
                        </div>
                        <div className="post-description">
                          <ReactMarkdown
                            plugins={[breaks]}
                            source={comment.content}
                            className="bubble markdown-content mt-3 md-mt-1"
                            renderers={{ link: props => linkRenderTarget(props) }}
                          />
                          <div className="right">
                            {isAllowPermission(appPermissions, PERM_DELETE_COMMENT_ISSUE, projectId) &&
                              commentAction &&
                              comment.id === commentHoverActor && (
                                <span
                                  className={'d-inline-block remove-btn'}
                                  color={'primary'}
                                  onClick={() => handleOpenAlertComment(comment)}
                                >
                                  <i className="fa fa fa-trash-o" />
                                </span>
                              )}

                            {isAllowPermission(appPermissions, PERM_UPDATE_COMMENT_ISSUE, projectId) &&
                              commentAction &&
                              comment.id === commentHoverActor && (
                                <span
                                  className={'d-inline-block edit-btn'}
                                  color={'primary'}
                                  onClick={() => {
                                    setIsEditComment(true);
                                    setContent(comment && comment.content);
                                    setCommentIssueId(comment && comment.id);
                                  }}
                                >
                                  <i className="fa fa-edit" />
                                </span>
                              )}
                          </div>
                        </div>
                        <div className="underlined" />
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div className="text-center margin-top-15px">
                  <p className={'font-size-15 text-muted'}>{t('no_comment')}</p>
                </div>
              )}
              <u className={'margin-top-10px full'}>{t('add_comment')}: </u>
              {!isEditComment && <MarkdownEditor value={content} onChange={value => setContent(value)} />}

              {isEditComment && <MarkdownEditor value={content} onChange={value => setContent(value)} />}
              <div>
                <Button
                  disabled={!content || isLoading}
                  className={'add-comment-button base-btn'}
                  size={'sm'}
                  onClick={() => {
                    if (!isEditComment) {
                      addCommentIssue({
                        issueId: value.issueId,
                        content: content
                      });
                    } else {
                      updateCommentIssue({
                        content: content,
                        commentIssueId: commentIssueId
                      });
                    }
                    setIsEditComment(false);
                    setContent(null);
                    setCommentIssueId(0);
                  }}
                >
                  {t('comment')}
                </Button>
                {isEditComment && (
                  <Button
                    className={'margin-right-10px outline-base-btn cancel-comment-button'}
                    size={'sm'}
                    onClick={() => {
                      setIsEditComment(false);
                      setContent(null);
                      setCommentIssueId(0);
                    }}
                  >
                    {t('cancel')}
                  </Button>
                )}
              </div>
            </ModalBody>

            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <Button
                  className={'ml-auto margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onClose && onClose();
                  }}
                >
                  {t('cancel')}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
          <ConfirmationModal
            isOpen={openAlertComment}
            txtTitle={t('confirm_remove')}
            txtCancel={t('cancel')}
            txtConfirm={t('confirm')}
            funConfirm={() => handleRemoveComment()}
            funClose={() => closeAlertComment()}
          >
            {t('are_you_sure_remove_this_comment')}
          </ConfirmationModal>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

ViewIssueDialog.propTypes = {
  status: string,
  value: any,
  onClose: func,
  currentIssue: object,
  addCommentIssue: func,
  getCommentsOfIssue: func,
  updateCommentIssue: func,
  deleteCommentIssue: func,
  commentsOfIssue: array,
  commentsOfIssueTotal: number,
  userId: number,
  loadMoreCommentsOfIssue: func,
  suggestTasks: array,
  usersIntoProject: array,
  appPermissions: array,
  projectId: any,
  isOpen: bool,
  getIssue: func,
  getUsersByProject: func
};

const mapStateToProps = state => ({
  currentIssue: state.issue.currentIssue,
  status: state.comment.status,
  commentsOfIssue: state.comment.commentsOfIssue,
  commentsOfIssueTotal: state.comment.commentsOfIssueTotal,
  suggestTasks: state.sprint.suggestTasks,
  usersIntoProject: state.sprint.usersIntoProject,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  // auth
  userId: state.auth && state.auth.user && state.auth.user.id,
  appPermissions: state.auth.permissions
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addCommentIssue,
      getCommentsOfIssue,
      updateCommentIssue,
      deleteCommentIssue,
      loadMoreCommentsOfIssue,
      getIssue,
      getUsersByProject
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(ViewIssueDialog));
