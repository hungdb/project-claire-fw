import React, { memo, useState, useEffect } from 'react';
import Switch from 'react-switch';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { object, bool, func } from 'prop-types';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import FieldInput from '../../../03-components/FieldInput';
import './style.css';

const ProjectGroupDialog = ({ value, isOpen, onClose, editMode, onOK, onRemove }) => {
  const [state, setState] = useState({
    projectGroupName: '',
    isActive: true,
    statusAlert: false
  });

  const { projectGroupName, isActive, statusAlert } = state;

  useEffect(() => {
    setState({
      ...state,
      projectGroupName: value && value.name,
      isActive: value ? value.isActive : true
    });
  }, [value]);

  const openAlertRemove = () => setState({ ...state, statusAlert: true });
  const closeAlertRemove = () => setState({ ...state, statusAlert: false });
  const onChangeActive = isActive => setState({ ...state, isActive });

  const onOkDialog = () => {
    onOK &&
      onOK(editMode, {
        id: editMode && value.id,
        name: projectGroupName,
        isActive: isActive
      });
  };

  const onRemoveProjectGroup = () => onRemove && onRemove({ id: editMode && value.id });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Modal centered={true} isOpen={isOpen} toggle={onClose} backdrop="static">
            <ModalHeader toggle={onClose}>{t(editMode ? 'edit_project_group' : 'new_project_group')}</ModalHeader>
            <ModalBody>
              <FieldInput
                type="text"
                placeholder={'project_group_name'}
                defaultValue={projectGroupName}
                onChangeValue={value => setState({ ...state, projectGroupName: value })}
              />
              <div className="d-flex mt-3">
                <Switch
                  width={45}
                  height={20}
                  onColor="#0ba1ee"
                  checked={isActive}
                  onChange={onChangeActive}
                  className="align-middle ml-auto pl-2 pr-1"
                />
                <p className="font-size-14 ml-1 mb-0 default-text-color">{t('active')}</p>
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={`flex-grow-1 d-flex ${editMode ? 'justify-content-between' : 'justify-content-end'}`}>
                <div>
                  {editMode && (
                    <Button size={'sm'} className={'outline-base-remove-btn'} onClick={() => openAlertRemove()}>
                      {t('remove')}
                    </Button>
                  )}
                </div>
                <div>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => onClose && onClose()}
                  >
                    {t('cancel')}
                  </Button>
                  <Button disabled={!projectGroupName} className={'base-btn'} size={'sm'} onClick={onOkDialog}>
                    {t(editMode ? 'save' : 'add')}
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </Modal>

          <ConfirmationModal
            isOpen={statusAlert}
            txtCancel={t('cancel')}
            txtConfirm={t('confirm')}
            txtTitle={t('confirm_remove')}
            funClose={closeAlertRemove.bind(this)}
            funConfirm={onRemoveProjectGroup}
          >
            {t('are_you_sure_remove_this_group')}
          </ConfirmationModal>
        </>
      )}
    </NamespacesConsumer>
  );
};

ProjectGroupDialog.propTypes = {
  value: object,
  isOpen: bool,
  onClose: func,
  editMode: bool,
  onOK: func,
  onRemove: func
};

export default memo(ProjectGroupDialog);
