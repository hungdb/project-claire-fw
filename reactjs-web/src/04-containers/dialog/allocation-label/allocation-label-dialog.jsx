import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Label, Input, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { MODE_DIALOG } from '../../../05-utils/commonData';

class AllocationLabelDialog extends PureComponent {
  state = {
    name: ''
  };

  static propTypes = {
    isOpen: PropTypes.bool,
    mode: PropTypes.any,
    onCreate: PropTypes.func,
    onSave: PropTypes.func,
    onRemove: PropTypes.func,
    toggle: PropTypes.func,
    item: PropTypes.object
  };

  onChangeName = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onClickAdd = () => {
    const { onCreate } = this.props;
    const { name } = this.state;
    onCreate({ name });
    this.setState({ name: '' });
  };

  onClickSave = () => {
    const { onSave, item } = this.props;
    const { name } = this.state;
    onSave({ id: item.id, name });
    this.setState({ name: '' });
  };

  onClickRemove = () => {
    const { onRemove, item } = this.props;
    onRemove({ id: item.id });
    this.setState({ name: '' });
  };

  componentDidUpdate(prevProps) {
    const { isOpen: prevIsOpen } = prevProps;
    const { isOpen, item } = this.props;
    if (!prevIsOpen && isOpen) {
      const name = item ? item.name || '' : '';
      this.setState({ name });
    }
  }

  render() {
    const { toggle, isOpen, mode, ...props } = this.props;
    const { name } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal
            className="allocation-label-dialog"
            isOpen={isOpen}
            toggle={toggle}
            backdrop="static"
            centered
            {...props}
            fade={true}
          >
            <ModalHeader toggle={toggle}>
              {t(mode === MODE_DIALOG.SAVE ? 'update_allocation_label' : 'add_allocation_label')}
            </ModalHeader>
            <ModalBody>
              <div className="content-modal">
                <FormGroup>
                  <Label>{t('name')} </Label>
                  <Input type="text" name="name" value={name} onChange={this.onChangeName} />
                </FormGroup>
              </div>
            </ModalBody>
            <ModalFooter className="dialog-actions flex-grow-1 d-flex justify-content-between">
              <div>
                {mode === MODE_DIALOG.SAVE && (
                  <Button className={'outline-base-remove-btn pull-left'} size={'sm'} onClick={this.onClickRemove}>
                    {t('remove')}
                  </Button>
                )}
              </div>
              <div>
                <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={toggle}>
                  {t('cancel')}
                </Button>
                {mode === MODE_DIALOG.SAVE && (
                  <Button disabled={!name} className={'base-btn'} size={'sm'} onClick={this.onClickSave}>
                    {t('save')}
                  </Button>
                )}
                {mode === MODE_DIALOG.ADD && (
                  <Button disabled={!name} className={'base-btn'} size={'sm'} onClick={this.onClickAdd}>
                    {t('add')}
                  </Button>
                )}
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default AllocationLabelDialog;
