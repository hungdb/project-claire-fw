import BaseDialog from '../../../03-components/Dialog/BaseDialog';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import FieldInput from '../../../03-components/FieldInput';
import React, { Fragment, memo, useState, useEffect } from 'react';
import { object, array } from 'prop-types';
import { Button, FormGroup, Label } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength } from '../../../08-helpers/common';

function DialogWorkflowTransition({ workflowTransitionCRUD, currentWorkflow, listWorkflowState }) {
  const { status, title, closeDialogWorkflowTransition, currentWorkflowTransition } = workflowTransitionCRUD;
  const [valueText, setValueText] = useState('');
  const [valueToSelect, setValueToSelect] = useState(null);
  const [valueFromSelect, setValueFromSelect] = useState(null);

  const getSelectOptions = data => {
    return calculateLength(data)
      ? data.map(value => {
          return { ...value, value: value.id, label: value.name.toUpperCase() };
        })
      : [];
  };

  useEffect(() => {
    let valueText = '';
    let valueToSelect = null;
    let valueFromSelect = null;
    if (status === 2) {
      const { fromStates, toStates, description } = currentWorkflowTransition;
      valueFromSelect = getSelectOptions(listWorkflowState).find(item => item.value === (fromStates && fromStates.id));
      valueToSelect = getSelectOptions(listWorkflowState).find(item => item.value === (toStates && toStates.id));
      valueText = description;
    }
    setValueText(valueText);
    setValueToSelect(valueToSelect);
    setValueFromSelect(valueFromSelect);
  }, [workflowTransitionCRUD]);

  const handleFromSelect = valueFromSelect => {
    setValueFromSelect(valueFromSelect);
  };

  const handleToSelect = valueToSelect => {
    setValueToSelect(valueToSelect);
  };

  const handleText = value => {
    setValueText(value);
  };

  const submitForm = () => {
    workflowTransitionCRUD.createUpdateWorkFlowTransition({
      workflowId: currentWorkflow.id,
      fromState: parseInt(valueFromSelect.value),
      toState: parseInt(valueToSelect.value),
      description: valueText,
      currentFromState: valueFromSelect,
      currentToState: valueToSelect,
      currentWorkflowTransitionId: currentWorkflowTransition && currentWorkflowTransition.id
    });
    workflowTransitionCRUD.closeDialogWorkflowTransition();
  };

  const deleteWorkflowTransition = () => {
    workflowTransitionCRUD.deleteWorkflowTransition({
      id: currentWorkflowTransition.id
    });
    workflowTransitionCRUD.closeDialogWorkflowTransition();
  };

  const isSubmitForm = () => {
    if ((valueFromSelect && valueFromSelect.value === '') || (valueToSelect && valueToSelect.value === '')) {
      return true;
    }
    if (workflowTransitionCRUD.status === 2) {
      const { fromStates, toStates, description } = currentWorkflowTransition;
      return (
        fromStates.id === Number(valueFromSelect && valueFromSelect.value) &&
        toStates.id === Number(valueToSelect && valueToSelect.value) &&
        description === valueText
      );
    }
  };

  const _renderContent = t => {
    return (
      <Fragment>
        <FormGroup className="mb-3">
          <Label>{t('from_state')}</Label>
          <SelectPropsBox
            value={valueFromSelect && valueFromSelect.value}
            options={getSelectOptions(listWorkflowState)}
            onChange={handleFromSelect}
          />
        </FormGroup>
        <FormGroup className="mb-3">
          <Label>{t('to_state')}</Label>
          <SelectPropsBox
            value={valueToSelect && valueToSelect.value}
            options={getSelectOptions(listWorkflowState)}
            onChange={handleToSelect}
          />
        </FormGroup>
        <FormGroup className="mb-3">
          <Label>{t('description')}</Label>
          <FieldInput type="text" defaultValue={valueText} onChangeValue={handleText} />
        </FormGroup>
      </Fragment>
    );
  };

  const _renderActionsRight = (status, t) => {
    return (
      <Fragment>
        <Button className={'base-btn'} size={'sm'} onClick={submitForm} disabled={isSubmitForm()}>
          {status === 1 ? t('add') : t('save')}
        </Button>
      </Fragment>
    );
  };

  const _renderActionsLeft = (status, t) => {
    return (
      <Fragment>
        {status === 2 && (
          <Button className={'outline-base-remove-btn'} size={'sm'} onClick={deleteWorkflowTransition}>
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          title={`${title} workflow transition`}
          isOpen={!!status}
          onClose={closeDialogWorkflowTransition}
          dialogContent={_renderContent(t)}
          actionBtn={_renderActionsRight(status, t)}
          actionBtnLeft={_renderActionsLeft(status, t)}
        />
      )}
    </NamespacesConsumer>
  );
}

DialogWorkflowTransition.propTypes = {
  workflowTransitionCRUD: object,
  currentWorkflow: object,
  listWorkflowState: array
};

export default memo(DialogWorkflowTransition);
