import { object } from 'prop-types';
import React, { Fragment, memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, FormGroup, Label } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog/BaseDialog';
import FieldInput from '../../../03-components/FieldInput';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import { EFFORT_TYPES_ARRAY } from '../../../05-utils/commonData';
import './style.css';

function DialogWorkFlowState({ workflowStateCRUD, currentWorkflow }) {
  const { status, title, closeDialogWorkflowState, currentWorkflowStatus } = workflowStateCRUD;
  const [valueText, setValueText] = useState('');
  const [valueSelectType, setValueSelectType] = useState(null);
  const [valueSelectOrdinal, setValueSelectOrdinal] = useState(null);

  const getSelectOptions = () => {
    return EFFORT_TYPES_ARRAY.map((label, index) => {
      return { value: index, label: label.toUpperCase() };
    });
  };

  useEffect(() => {
    let valueText = '';
    let effortType = null;
    let valueSelectType = null;
    let valueSelectOrdinal = null;
    if (workflowStateCRUD.status === 2) {
      if (currentWorkflowStatus) {
        valueText = currentWorkflowStatus.name.toUpperCase();
        effortType = currentWorkflowStatus.effortType;
        valueSelectOrdinal = currentWorkflowStatus.ordinalNumber;
      }
      valueSelectType = getSelectOptions().find(item => item.label.toLowerCase() === effortType);
    }
    setValueText(valueText);
    setValueSelectType(valueSelectType);
    setValueSelectOrdinal(valueSelectOrdinal);
  }, [workflowStateCRUD]);

  const handleText = value => {
    setValueText(value);
  };

  const handleSelectType = valueSelectType => setValueSelectType(valueSelectType);

  const handleSelectOrdinal = valueSelectOrdinal => {
    setValueSelectOrdinal(valueSelectOrdinal);
  };

  const submitForm = () => {
    workflowStateCRUD.createUpdateWorkFlowState({
      workflowId: currentWorkflow.id,
      id: currentWorkflowStatus.id,
      name: valueText,
      type: valueSelectType && valueSelectType.label.toLowerCase(),
      ordinal: valueSelectOrdinal
    });
    workflowStateCRUD.closeDialogWorkflowState();
  };

  const deleteWorkflowState = () => {
    workflowStateCRUD.deleteWorkflowState({ id: currentWorkflowStatus.id });
    workflowStateCRUD.closeDialogWorkflowState();
  };

  const isSubmitForm = () => {
    if (valueText === '' || valueSelectType === null || valueSelectOrdinal === '' || Number(valueSelectOrdinal) < 1) {
      return true;
    }
    if (workflowStateCRUD.status === 2) {
      return (
        currentWorkflowStatus.name.toUpperCase() === valueText.toUpperCase() &&
        currentWorkflowStatus.effortType.toUpperCase() === valueSelectType.label.toUpperCase() &&
        currentWorkflowStatus.ordinalNumber === Number(valueSelectOrdinal)
      );
    }
  };

  const _renderContent = t => {
    return (
      <Fragment>
        <FormGroup className="mb-4">
          <Label>{t('name_state')}</Label>
          <FieldInput type="text" classes="upcase-text" onChangeValue={handleText} defaultValue={valueText} />
        </FormGroup>
        <FormGroup className="mb-4">
          <Label>{t('select_the_effort_type_of_the_new_state')}</Label>
          <SelectPropsBox
            options={getSelectOptions()}
            value={valueSelectType && valueSelectType.value}
            onChange={handleSelectType}
          />
        </FormGroup>
        <FormGroup>
          <Label>{t('select_the_ordinal_of_the_new_state')}</Label>
          <FieldInput
            type="number"
            onChangeValue={value => handleSelectOrdinal(value)}
            defaultValue={valueSelectOrdinal}
          />
        </FormGroup>
      </Fragment>
    );
  };

  const _renderActionsRight = (status, t) => {
    return (
      <Fragment>
        <Button className="base-btn" size="sm" onClick={submitForm} disabled={isSubmitForm()}>
          {status === 1 ? t('add') : t('save')}
        </Button>
      </Fragment>
    );
  };

  const _renderActionsLeft = (status, t) => {
    return (
      <Fragment>
        {status === 2 && (
          <Button className={'outline-base-remove-btn'} size={'sm'} onClick={deleteWorkflowState}>
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          isOpen={!!status}
          onClose={closeDialogWorkflowState}
          title={`${title} workflow state`}
          dialogWidth="md"
          dialogContent={_renderContent(t)}
          actionBtn={_renderActionsRight(status, t)}
          actionBtnLeft={_renderActionsLeft(status, t)}
        />
      )}
    </NamespacesConsumer>
  );
}

DialogWorkFlowState.propTypes = {
  workflowStateCRUD: object,
  currentWorkflow: object
};

export default memo(DialogWorkFlowState);
