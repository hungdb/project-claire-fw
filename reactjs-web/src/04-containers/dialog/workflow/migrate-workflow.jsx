import { array, bool, func } from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import Select from 'react-select';
import { Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getProjects } from '../../../01-actions/project';
import {
  clearWorkflowStatesForMigrate,
  getWorkflowsForMigrate,
  getWorkflowStatesForMigrate,
  workflowMigrate
} from '../../../01-actions/workflow';
import { calculateLength, customBorderColor } from '../../../08-helpers/common';
import './style.css';

function MigrateWorkflowDialog({
  projects,
  workflows,
  listFromStateForMigrate,
  listToStateForMigrate,
  getProjects,
  getWorkflowsForMigrate,
  getWorkflowStatesForMigrate,
  clearWorkflowStatesForMigrate,
  workflowMigrate,
  onClose,
  isOpen
}) {
  const [valueSelectProjectId, setValueSelectProjectId] = useState('');
  const [workflowOfProject, setWorkflowOfProject] = useState('');
  const [valueSelectWorkflowId, setValueSelectWorkflowId] = useState('');
  const [valueSelectWorkflowStateIds, setValueSelectWorkflowStateIds] = useState([]);
  const [content, setContent] = useState(false);

  useEffect(() => {
    getProjects();
    getWorkflowsForMigrate();
  }, []);

  const clearState = () => {
    setValueSelectProjectId('');
    setWorkflowOfProject('');
    setValueSelectWorkflowId('');
    setValueSelectWorkflowStateIds([]);
    setContent(false);
  };

  const handleClose = () => {
    clearWorkflowStatesForMigrate();
    clearState();
  };

  const handleSelectProject = e => {
    const projectId = e.value;
    const project = projects.find(p => p.id === Number(projectId));
    getWorkflowStatesForMigrate({ id: project.workflowSchemeId, type: 'from' });
    const wf = workflows.find(w => w.id === Number(project.workflowSchemeId));
    setValueSelectProjectId(projectId);
    setWorkflowOfProject(wf.name);
    setValueSelectWorkflowId('');
    setValueSelectWorkflowStateIds([]);
    setContent(false);
  };

  const handleSelectWorkflow = e => {
    const workflowId = e.value;
    getWorkflowStatesForMigrate({ id: workflowId, type: 'to' });
    setValueSelectWorkflowId(workflowId);
    setValueSelectWorkflowStateIds([]);
    setContent(false);
  };

  const handleSelectWorkflowState = e => {
    const stateId = e.value;
    const index = e.index;
    const tmp = [...valueSelectWorkflowStateIds];
    tmp[index] = stateId;
    setValueSelectWorkflowStateIds(tmp);
    if (!tmp.includes(undefined) && tmp.length === listFromStateForMigrate.length) {
      setContent(true);
    }
  };

  const submitForm = () => {
    const data = {
      projectId: valueSelectProjectId,
      workflowId: valueSelectWorkflowId,
      workflowStateIds: valueSelectWorkflowStateIds
    };
    workflowMigrate(data);
    onClose();
    clearState();
    getProjects();
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal
          size="lg"
          centered
          isOpen={isOpen}
          toggle={() => {
            onClose && onClose();
          }}
          backdrop="static"
        >
          <ModalHeader
            toggle={() => {
              onClose && onClose();
            }}
          >
            {t('migrate_workflow')}
          </ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">
              <Row className="mb-4">
                <Col xs="12">
                  <Select
                    placeholder={t('select_project')}
                    value={projects
                      .filter(i => i.id === valueSelectProjectId)
                      .map(i => ({ value: i.id, label: i.name }))}
                    style={customBorderColor}
                    onChange={handleSelectProject}
                    options={projects.map(p => ({ label: p.name, value: p.id }))}
                  />
                </Col>
              </Row>
              <Row className="align-items-end">
                <Col sm="5">
                  <h6 className="label">{t('from_workflow_scheme')}</h6>
                  <p
                    className="text-uppercase 
                      mb-0 sm-text-center min-height-sm-40
                      d-flex align-items-center
                    "
                  >
                    {workflowOfProject || t('none')}
                  </p>
                </Col>
                <Col sm="1" className="sm-arrow-bottom top">
                  <i className={'fa fa-arrow-right '} />
                </Col>
                <Col sm="6">
                  <h6 className="label mb-3 m-sm-bottom-10">{t('to_workflow_scheme')}</h6>
                  <Select
                    placeholder={t('select_workflow')}
                    value={workflows
                      .filter(i => i.id === valueSelectWorkflowId)
                      .map(i => ({ value: i.id, label: i.name }))}
                    style={customBorderColor}
                    onChange={handleSelectWorkflow}
                    options={workflows
                      .filter(item => item.name !== workflowOfProject)
                      .map(item => ({ value: item.id, label: item.name }))}
                  />
                </Col>
              </Row>
              <div>
                {calculateLength(listFromStateForMigrate) ? (
                  <h6 className="mb-0 mt-3">{t('state_migration')}</h6>
                ) : null}
                {listFromStateForMigrate.map((item, index) => {
                  return (
                    <Row key={index} className="align-items-center mt-2">
                      <Col sm="5" className="sm-text-center">
                        {item.name.toUpperCase()}
                      </Col>
                      <Col sm="1" className="sm-arrow-bottom">
                        <i className="fa fa-arrow-right" />
                      </Col>
                      <Col sm="6">
                        <Select
                          placeholder={t('select_state')}
                          value={listToStateForMigrate
                            .filter(i => i.id === valueSelectWorkflowStateIds[index])
                            .map(i => ({ value: i.id, label: i.name.toUpperCase() }))}
                          className={'select'}
                          style={customBorderColor}
                          onChange={handleSelectWorkflowState}
                          options={listToStateForMigrate.map(p => ({
                            label: p.name.toUpperCase(),
                            value: p.id,
                            index
                          }))}
                        />
                      </Col>
                    </Row>
                  );
                })}
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-between'}>
              <div />
              <div>
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    handleClose();
                    onClose && onClose();
                  }}
                >
                  {t('cancel')}
                </Button>
                <Button disabled={!content} className={'base-btn'} size={'sm'} onClick={submitForm}>
                  {t('save')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

MigrateWorkflowDialog.propTypes = {
  projects: array,
  workflows: array,
  listFromStateForMigrate: array,
  listToStateForMigrate: array,
  getProjects: func,
  getWorkflowsForMigrate: func,
  getWorkflowStatesForMigrate: func,
  clearWorkflowStatesForMigrate: func,
  workflowMigrate: func,
  onClose: func,
  isOpen: bool
};

const mapStateToProps = store => {
  return {
    error: store.common.error,
    workflows: store.workflow.workflowsForMigrate,
    listFromStateForMigrate: store.workflow.listFromStateForMigrate,
    listToStateForMigrate: store.workflow.listToStateForMigrate,
    projects: store.project.projects
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getProjects,
      getWorkflowsForMigrate,
      getWorkflowStatesForMigrate,
      workflowMigrate,
      clearWorkflowStatesForMigrate
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(MigrateWorkflowDialog));
