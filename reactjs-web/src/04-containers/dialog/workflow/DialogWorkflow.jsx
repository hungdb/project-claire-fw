import { object } from 'prop-types';
import React, { Fragment, useEffect, useState, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, FormGroup, Label } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog/BaseDialog';
import FieldInput from '../../../03-components/FieldInput';
import Multicheck from '../../../03-components/Multicheck';

function DialogWorkFlow({ workflowCRUD, currentWorkflow }) {
  const { status, title, closeDialogWorkflow } = workflowCRUD;
  const [valueText, setValueText] = useState('');
  const [allowParallelTask, setAllowParallelTask] = useState(false);

  useEffect(() => {
    setValueText(status === 2 ? currentWorkflow.name : '');
    setAllowParallelTask(status === 2 ? currentWorkflow.allowParallelTask : false);
  }, [workflowCRUD]);

  const handleText = value => {
    setValueText(value);
  };

  const submitForm = () => {
    workflowCRUD.createUpdateWorkFlow({
      id: currentWorkflow ? currentWorkflow.id : null,
      name: valueText,
      allowParallelTask
    });
    closeDialogWorkflow();
  };

  const isSubmitForm = () => {
    if (valueText === '') {
      return true;
    }
    if (status === 2) {
      return currentWorkflow.name === valueText && currentWorkflow.allowParallelTask === allowParallelTask;
    }
  };

  const deleteWorkflow = () => {
    workflowCRUD.removeWorkflow({ id: currentWorkflow.id });
    closeDialogWorkflow();
  };

  const _renderContent = t => {
    return (
      <Fragment>
        <FormGroup>
          <Label>{t('Give a name')}</Label>
          <FieldInput type="text" defaultValue={valueText} onChangeValue={handleText} />
        </FormGroup>
        <FormGroup className="margin-top-15px d-flex">
          <Multicheck
            items={[{ label: t('allow_parallel_task'), checked: allowParallelTask }]}
            onChange={(index, checked) => setAllowParallelTask(checked)}
          />
        </FormGroup>
      </Fragment>
    );
  };

  const _renderActionsRight = (status, t) => {
    return (
      <Fragment>
        <Button size="sm" className="base-btn" onClick={submitForm} disabled={isSubmitForm()}>
          {status === 1 ? t('add') : t('save')}
        </Button>
      </Fragment>
    );
  };

  const _renderActionsLeft = (status, t) => {
    return (
      <Fragment>
        {status === 2 && (
          <Button onClick={deleteWorkflow} className="outline-base-remove-btn" size="sm">
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          title={`${t(title)} workflow`}
          onClose={closeDialogWorkflow}
          isOpen={!!status}
          dialogContent={_renderContent(t)}
          actionBtn={_renderActionsRight(status, t)}
          actionBtnLeft={_renderActionsLeft(status, t)}
        />
      )}
    </NamespacesConsumer>
  );
}

DialogWorkFlow.propTypes = {
  workflowCRUD: object,
  currentWorkflow: object
};

export default memo(DialogWorkFlow);
