import React, { Component, Fragment } from 'react';
import { Button } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, any, func } from 'prop-types';
import RoleUserSelector from '../../role-user-selector/role-user-selector';
import BaseDialog from '../../../03-components/Dialog';

class AddUserDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleId: null,
      roleName: null
    };
  }

  static propTypes = {
    isOpen: bool,
    user: any,
    onClose: func,
    onOK: func
  };

  _renderContent = t => {
    const { user } = this.props;
    return (
      <Fragment>
        <div>{t('select_role_for_new_user') + ': ' + (!!user && user.username)}</div>
        <div className={'margin-top-15px'}>
          <RoleUserSelector
            onChange={e => {
              this.setState({ roleId: e.value, roleName: e.label });
            }}
          />
        </div>
      </Fragment>
    );
  };

  _renderAction = t => {
    const { roleId, roleName } = this.state;
    const { user, onOK } = this.props;
    return (
      <Button
        disabled={roleId === null}
        className={'base-btn'}
        size={'sm'}
        onClick={() => {
          onOK &&
            onOK({
              id: user.id,
              roleId: roleId,
              roleName: roleName
            });
        }}
      >
        {t('add')}
      </Button>
    );
  };

  render() {
    const { isOpen, onClose } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <BaseDialog
            size="md"
            isOpen={isOpen}
            title={t('Add User')}
            onClose={onClose}
            dialogContent={this._renderContent(t)}
            actionBtn={this._renderAction(t)}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

export default AddUserDialog;
