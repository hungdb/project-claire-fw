import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import ControlDatePicker from '../../../../03-components/DateTimePicker';
import { CREATE_SPRINT_SUCCESS } from '../../../../07-constants/sprint';
import { getDateAt000000ZeroGMT, getDateAt235959ZeroGMT } from '../../../../08-helpers/common';

function CreateSprintDialog({ isOpen, onClose, onOK, status }) {
  const [name, setName] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    if (status === CREATE_SPRINT_SUCCESS) {
      setName(null);
      setStartDate(null);
      setEndDate(null);
    }
  }, [status]);
  
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered={true} size="md" backdrop="static">
          <ModalHeader toggle={onClose}>{t('create_sprint')}</ModalHeader>
          <ModalBody className={'overflow-visible'}>
            <div className="content-modal d-flex flex-column align-items-stretch">
              <Input
                className={'focus-border'}
                autoFocus={true}
                placeholder={t('name')}
                onChange={e => setName(e.target.value)}
              />
              <div className={'margin-top-15px'}>
                <ControlDatePicker
                  isLocalFormat
                  value={startDate}
                  format="DD/MM/YYYY"
                  onChange={e => setStartDate(e)}
                  placeholder={t('start_date')}
                />
              </div>
              <div className={'margin-top-15px'}>
                <ControlDatePicker
                  isLocalFormat
                  value={endDate}
                  format="DD/MM/YYYY"
                  onChange={e => setEndDate(e)}
                  placeholder={t('end_date')}
                />
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button
                className={'margin-right-10px outline-base-btn'}
                size={'sm'}
                onClick={() => {
                  onClose && onClose();
                }}
              >
                {t('cancel')}
              </Button>
              <Button
                disabled={!name || !startDate || !endDate}
                className={'base-btn'}
                size={'sm'}
                onClick={() => {
                  const start = startDate ? getDateAt000000ZeroGMT(startDate) : null;
                  const end = endDate ? getDateAt235959ZeroGMT(endDate) : null;
                  onOK && onOK({ name, startDate: start, endDate: end });
                }}
              >
                {t('add')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

CreateSprintDialog.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  onOK: PropTypes.func,
  status: PropTypes.string
};

export default memo(CreateSprintDialog);
