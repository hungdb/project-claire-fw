import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import FieldInput from '../../../../03-components/FieldInput';
import SelectPropsBox from '../../../../03-components/SelectPropsBox';
import { REMOVE_SPRINT } from '../../../../07-constants/app-permission';
import {
  formatDateTime,
  getDateAt000000ZeroGMT,
  getDateAt235959ZeroGMT,
  isAllowPermission,
  calculateLength
} from '../../../../08-helpers/common';

const STATUS = ['active', 'unactive', 'completed'];

function DialogUpdateSprint({ isOpen, onClose, onOK, onDelete, appPermissions, value }) {
  const [id, setId] = useState(0);
  const [name, setName] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [status, setStatus] = useState({});
  const [projectId, setProjectId] = useState(0);

  const getStatusOptions = data => {
    return calculateLength(data)
      ? data.map((item, index) => {
          return { label: item, value: index };
        })
      : [];
  };

  useEffect(() => {
    if (isOpen) {
      let id = 0;
      let name = null;
      let startDate = null;
      let endDate = null;
      let status = {};
      let projectId = 0;
      if (value) {
        id = value.id;
        name = value.name;
        startDate = value.startDate;
        endDate = value.endDate;
        status = getStatusOptions(STATUS).find(item => item.label === value.status);
        projectId = value.projectId;
      }
      setId(id);
      setName(name);
      setStartDate(startDate);
      setEndDate(endDate);
      setStatus(status);
      setProjectId(projectId);
    }
  }, [isOpen]);
  
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered={true} size="md" backdrop="static">
          <ModalHeader toggle={onClose}>{t('update_sprint')}</ModalHeader>
          <ModalBody className={'overflow-visible'}>
            <div className="content-modal d-flex flex-column align-items-stretch">
              <FieldInput
                defaultValue={name}
                name="name"
                className={'flex-grow-1 margin-top-15px'}
                onChangeValue={value => setName(value)}
                placeholder={'name'}
              />
              <SelectPropsBox
                isClearable={false}
                value={status && status.value}
                placeholder={'status'}
                classes={'flex-grow-1 margin-top-15px'}
                options={getStatusOptions(STATUS)}
                onChange={status => setStatus(status)}
              />
              <div className={'margin-top-15px popup'}>
                <DatePicker
                  className={'flex-grow-1'}
                  value={formatDateTime(startDate, 'DD/MM/YYYY')}
                  placeholderText={t('start_date')}
                  dropdownMode={'scroll'}
                  onChange={e => setStartDate(e ? getDateAt000000ZeroGMT(e) : null)}
                />
              </div>
              <div className={'margin-top-15px popup'}>
                <DatePicker
                  className={'flex-grow-1'}
                  value={formatDateTime(endDate, 'DD/MM/YYYY')}
                  placeholderText={t('end_date')}
                  dropdownMode={'scroll'}
                  onChange={e => setEndDate(e ? getDateAt235959ZeroGMT(e) : null)}
                />
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-between'}>
              <div>
                {isAllowPermission(appPermissions, REMOVE_SPRINT, projectId) ? (
                  <Button
                    className={'outline-base-remove-btn'}
                    size={'sm'}
                    onClick={() => {
                      onDelete && onDelete(id, projectId);
                    }}
                  >
                    {t('remove')}
                  </Button>
                ) : null}
              </div>
              <div>
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onClose && onClose();
                  }}
                >
                  {t('cancel')}
                </Button>
                <Button
                  disabled={!name || !startDate || !endDate || !status}
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onOK &&
                      onOK({
                        id: id,
                        name: name,
                        startDate: startDate,
                        endDate: endDate,
                        status: status.label,
                        projectId: projectId
                      });
                  }}
                >
                  {t('save')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

DialogUpdateSprint.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onOK: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  appPermissions: PropTypes.array,
  value: PropTypes.object
};

const mapStateToProps = store => ({
  // auth
  appPermissions: store.auth.permissions,
});

export default connect(
  mapStateToProps,
  null
)(memo(DialogUpdateSprint));
