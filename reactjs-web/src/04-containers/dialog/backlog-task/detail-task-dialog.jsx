import { any, array, bool, func, number, object, string } from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import ReactMarkdown from 'react-markdown';
import connect from 'react-redux/es/connect/connect';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { toast } from 'react-toastify';
import { Badge, Button, Col, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import breaks from 'remark-breaks';
import {
  addComment,
  deleteComment,
  getCommentsOfTask,
  getCommentsOfUser,
  loadMoreCommentsOfTask,
  updateComment
} from '../../../01-actions/comment';
import { assignTaskToUser, clearCurrentTask, getDetailTask, getUsersByProject } from '../../../01-actions/sprint';
import {
  downloadFile,
  getAttachmentsOfTask,
  loadMoreAttachmentsOfTask,
  removeAttachmentOfTask,
  uploadFileToTask
} from '../../../01-actions/wiki';
import AppText from '../../../03-components/AppText';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import FileItemDetail from '../../../03-components/FileItemDetail';
import Loader from '../../../03-components/Loader';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import Tag from '../../../03-components/Tag';
import TaskType from '../../../03-components/TaskType';
import Upload from '../../../03-components/Upload';
import UserAvatar from '../../../03-components/UserAvatar/index';
import {
  EFFORT_TYPES,
  PAGINATE_ATTACHMENT_LIMIT,
  PAGINATE_COMMENT_LIMIT,
  TASK_TYPE_OPTIONS,
  TOAST_ERROR_STYLE
} from '../../../05-utils/commonData';
import defaultAvatar from '../../../06-assets/default_avatar.png';
import {
  ASSIGN_TASK,
  DOWNLOAD_ATTACHMENT,
  LIST_USER_INTO_PROJECT,
  PERM_ADD_ATTACHMENT_BY_TASK,
  PERM_DELETE_COMMENT_TASK,
  PERM_REMOVE_ATTACHMENT_BY_TASK,
  PERM_UPDATE_COMMENT_TASK
} from '../../../07-constants/app-permission';
import {
  ADD_COMMENT_FAILED,
  ADD_COMMENT_SUCCESS,
  UPDATE_COMMENT_FAILED,
  UPDATE_COMMENT_SUCCESS
} from '../../../07-constants/comment';
import {
  calculateLength,
  customBorderColor,
  filterUserRelateInProject,
  formatDateTime,
  fromNow,
  getPriority,
  isAllowPermission,
  parseJSON,
  removeDuplicateObjInArray,
  safelyParseJSON
} from '../../../08-helpers/common';
import { linkRenderTarget } from '../../../08-helpers/component';
import { getEnv } from '../../../env';
import i18n from '../../../i18n';
import IssueAttachment from '../issue/issue-attachment';
import './style.css';

function DetailTaskDialog({
  currentTask,
  selectedProject,
  getAttachmentsOfTask,
  loadMoreAttachmentsOfTask,
  attachmentsOfTask,
  attachmentsOfTaskTotal,
  removeAttachmentOfTask,
  downloadFile,
  uploadFileToTask,
  getCommentsOfTask,
  loadMoreCommentsOfTask,
  commentsOfTask,
  commentsOfTaskTotal,
  clearCurrentTask,
  addComment,
  deleteComment,
  updateComment,
  onClose,
  value,
  isOpen,
  status,
  getUsersByProject,
  assignTaskToUser,
  onDirectToTask,
  appPermissions,
  usersIntoProject,
  userId
}) {
  const [openUpload, setOpenUpload] = useState(false);
  const [attachmentId, setAttachmentId] = useState(null);
  const [attachmentOffset, setAttachmentOffset] = useState(0);
  const [openAlert, setOpenAlert] = useState(false);
  const [comment, setComment] = useState(null);
  const [commentOffset, setCommentOffset] = useState(0);
  const [commentAction, setCommentAction] = useState(false);
  const [commentHoverActor, setCommentHoverActor] = useState(0);
  const [content, setContent] = useState('');
  const [openAlertComment, setOpenAlertComment] = useState(false);
  const [isEditComment, setIsEditComment] = useState(false);
  const [commentId, setCommentId] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const selectedProjectId = value.projectId;
    getAttachmentsOfTask({
      projectId: selectedProjectId,
      taskId: value.taskId,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset: attachmentOffset
    });
    getCommentsOfTask({
      projectId: selectedProjectId,
      taskId: value.taskId,
      limit: PAGINATE_COMMENT_LIMIT,
      offset: commentOffset
    });

    if (isAllowPermission(appPermissions, LIST_USER_INTO_PROJECT))
      getUsersByProject({
        projectId: selectedProjectId
      });
    return () => {
      clearCurrentTask();
    };
  }, []);

  useEffect(() => {
    switch (status) {
      case ADD_COMMENT_SUCCESS:
      case ADD_COMMENT_FAILED:
      case UPDATE_COMMENT_SUCCESS:
      case UPDATE_COMMENT_FAILED:
        setIsLoading(false);
        break;
      default:
        break;
    }
  }, [status]);

  const handleUpload = acceptedFiles => {
    const selectedProjectId = value.projectId;
    if (!selectedProjectId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      uploadFileToTask({
        files: acceptedFiles,
        taskId: value.taskId,
        projectId: selectedProjectId
      });
    }
    setAttachmentOffset(0);
  };

  const toggleUpload = () => setOpenUpload(!openUpload);

  const loadMoreAttachment = () => {
    const selectedProjectId = value.projectId;
    const offset = attachmentOffset + PAGINATE_ATTACHMENT_LIMIT;
    loadMoreAttachmentsOfTask({
      projectId: selectedProjectId,
      taskId: value.taskId,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset
    });
    setAttachmentOffset(offset);
  };

  const loadMoreComments = () => {
    const selectedProjectId = value.projectId;
    const offset = commentOffset + PAGINATE_COMMENT_LIMIT;
    loadMoreCommentsOfTask({
      projectId: selectedProjectId,
      taskId: value.taskId,
      limit: PAGINATE_COMMENT_LIMIT,
      offset
    });
    setCommentOffset(offset);
  };

  const openAlertAttachment = attachmentId => {
    setOpenAlert(true);
    setAttachmentId(attachmentId);
  };

  const handleOpenAlertComment = comment => {
    setOpenAlertComment(true);
    setComment(comment);
  };

  const closeAlert = () => {
    setOpenAlert(false);
  };

  const handleRemoveAttachment = () => {
    const selectedProjectId = value.projectId;
    if (!selectedProjectId || !attachmentId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      removeAttachmentOfTask({
        attachmentId,
        projectId: selectedProjectId
      });
    }
    closeAlert();
  };

  const handleRemoveComment = () => {
    const selectedProjectId = value.projectId;
    if (!selectedProjectId || !comment || (comment && !comment.id)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      deleteComment({
        commentId: comment.id,
        projectId: selectedProjectId
      });

      if (comment.id === commentId) {
        setIsEditComment(false);
        setContent('');
        setCommentId(0);
      }
    }
    closeAlert();
  };

  const handleDownloadAttachment = attachment => {
    const selectedProjectId = value.projectId;
    if (!selectedProjectId || !attachment || (attachment && !attachment.url)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      downloadFile({
        filePath: attachment.url,
        projectId: selectedProjectId
      });
    }
  };

  const closeAlertComment = () => {
    setOpenAlertComment(false);
  };

  const handleSelectUserAssign = (userId, taskId) => {
    if (currentTask.assignee !== userId) {
      assignTaskToUser({ taskId, userId });
    }
  };

  const goToDependencyTask = dependencyTaskId => {
    onClose();
    onDirectToTask(dependencyTaskId);
  };

  const tags = parseJSON(currentTask && currentTask.tag);

  const isAllowAssign =
    isAllowPermission(appPermissions, ASSIGN_TASK, value.projectId) &&
    calculateLength(usersIntoProject) > 0 &&
    currentTask &&
    currentTask.WorkflowState &&
    (currentTask.WorkflowState.effortType === EFFORT_TYPES.UNTOUCHED ||
      currentTask.WorkflowState.effortType === EFFORT_TYPES.HOLDING);

  const enableRemoveAttachment = isAllowPermission(appPermissions, PERM_REMOVE_ATTACHMENT_BY_TASK, selectedProject.id);
  const enableDownloadAttachment = isAllowPermission(appPermissions, DOWNLOAD_ATTACHMENT, selectedProject.id);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          {isOpen && currentTask ? (
            <Modal
              isOpen={isOpen}
              toggle={() => onClose && onClose()}
              centered={true}
              size="xl"
              className="detail-task-modal"
              backdrop="static"
            >
              <ModalHeader toggle={() => onClose && onClose()}>{`#${
                currentTask.taskCode ? currentTask.taskCode : currentTask.id
              } ${currentTask.content}`}</ModalHeader>
              <ModalBody className="p-0 pt-3 pb-4">
                <div className="content-modal flex-column align-items-stretch">
                  {
                    <div className="mb-2 ml-3">
                      {tags &&
                        tags.map((item, index) => {
                          return <Tag key={index} name={item} />;
                        })}
                    </div>
                  }

                  <Container>
                    {currentTask.description && (
                      <div className="detail-task-box margin-bottom-15px">
                        {/*<label className='full'>Description: </label>*/}
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={currentTask.description}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      </div>
                    )}
                    <Row>
                      <Col md="6">
                        <label>
                          {t('estimate')}: {currentTask.estimate || 0}h
                        </label>
                      </Col>
                      <Col md="6" className="min-height-60px-xs min-height-auto-md">
                        <label>
                          {t('created_at')}: {formatDateTime(currentTask.createdAt, 'LLLL', 1)}
                        </label>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <label>
                          {t('effort_type')}: {currentTask.WorkflowState ? currentTask.WorkflowState.effortType : null}
                        </label>
                      </Col>
                      <Col md="6">
                        <label>
                          {t('due_date')}:{' '}
                          {currentTask.dueDate ? formatDateTime(currentTask.dueDate, 'DD/MM/YYYY', 1) : t('none')}
                        </label>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <label>
                          {t('priority')}: {currentTask.priority ? getPriority(currentTask.priority) : t('not_set')}
                        </label>
                      </Col>
                      <Col md="6" className="d-flex">
                        <div className="margin-right-5px">{t('Dependencies')}:</div>
                        <div>
                          {currentTask.dependencies.length ? (
                            currentTask.dependencies.map((dep, i) => {
                              const parentTaskCode = dep.parentTask && dep.parentTask.taskCode;
                              const parentTaskId = dep.parentTask && dep.parentTask.id;
                              const parentContent = dep.parentTask && dep.parentTask.content;
                              return (
                                <Badge
                                  key={i}
                                  color="info"
                                  className="mr-1 mb-1 d-flex align-items-center cursor-pointer"
                                  style={{ float: 'left' }}
                                  onClick={() => goToDependencyTask(dep.parentId)}
                                >
                                  {`#${parentTaskCode ? parentTaskCode : parentTaskId} ${parentContent}`}
                                </Badge>
                              );
                            })
                          ) : (
                            <div>{t('none')}</div>
                          )}
                        </div>
                      </Col>
                    </Row>
                    <Row className="justify-content-end mb-2">
                      {calculateLength(currentTask.relateUsers) > 0 && (
                        <Col md="6" className="d-flex justify-content-start align-items-center">
                          <div className={'margin-right-5px'}>{t('members')}:</div>
                          <div>
                            {calculateLength(currentTask.relateUsers) > 0 &&
                              currentTask.relateUsers.map((user, index) => {
                                return (
                                  <div key={index} className="mr-1 mt-1">
                                    <Link to={`/profile/${user.id}`}>
                                      <img
                                        className={'avatar-mini margin-right-5px'}
                                        alt={user.name}
                                        title={user.name}
                                        src={user.avatar ? `${getEnv('S3_BUCKET')}/${user.avatar}` : defaultAvatar}
                                      />
                                    </Link>
                                    {user && !isNaN(user.percentEffort) ? (
                                      <span> {user.percentEffort.toFixed(0) + '%'}</span>
                                    ) : null}
                                  </div>
                                );
                              })}
                          </div>
                        </Col>
                      )}
                      <Col md="6" className="d-flex align-items-center ">
                        <label>
                          {t('task_type')}: <TaskType type={currentTask.type} />
                        </label>
                        {currentTask.type === TASK_TYPE_OPTIONS[1].value && (
                          <>
                            <label className="pl-2 pr-2">{t('user_relate')} :</label>
                            {filterUserRelateInProject(usersIntoProject, safelyParseJSON(currentTask.userRelate)).map(
                              (item, index) => (
                                <div key={index} className="mr-1">
                                  <UserAvatar id={index} user={item.User} />
                                </div>
                              )
                            )}
                          </>
                        )}
                      </Col>
                    </Row>
                    <Row>
                      <Col md={{ size: 6, offset: 6 }} className="d-flex align-items-center">
                        <label className={'mb-0 mr-1'}>{t('assignee')}:</label>
                        <Select
                          className={'select-user-assign'}
                          placeholder={t('select_user')}
                          styles={customBorderColor}
                          isSearchable={true}
                          isClearable={false}
                          value={{
                            value: currentTask.assigneeAs && currentTask.assigneeAs.id,
                            label: currentTask.assigneeAs && currentTask.assigneeAs.name
                          }}
                          isDisabled={!isAllowAssign}
                          onChange={user => handleSelectUserAssign(user.value, currentTask.id)}
                          options={removeDuplicateObjInArray({ list: usersIntoProject, key: 'userId' }).map(u => ({
                            label: u.User.name,
                            value: u.User.id
                          }))}
                        />
                      </Col>
                    </Row>
                  </Container>
                  <hr className="mt-4" />

                  <Container>
                    <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                      <AppText type={'title-menu'}>{t('attachments')}:</AppText>
                      {isAllowPermission(appPermissions, PERM_ADD_ATTACHMENT_BY_TASK, selectedProject.id) && (
                        <Button className={'orange-btn'} size={'md'} onClick={() => toggleUpload()}>
                          {!openUpload ? (
                            <i className={'fa fa-plus margin-right-5px'} />
                          ) : (
                            <i className={'fa fa-minus margin-right-5px'} />
                          )}
                          {!openUpload ? t('upload_file') : t('close_upload_file')}
                        </Button>
                      )}
                    </div>
                    {openUpload && (
                      <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                        <Upload onDrop={(acceptedFiles, rejectedFiles) => handleUpload(acceptedFiles, rejectedFiles)} />
                      </div>
                    )}
                    <Row className={'detail-task-box-attachment-area'}>
                      {attachmentsOfTask.map((attachment, index) => (
                        <Col md="4" key={index}>
                          <FileItemDetail
                            enableRemove={enableRemoveAttachment}
                            enableDownload={enableDownloadAttachment}
                            attachment={attachment}
                            onRemoveAttachments={openAlertAttachment}
                            onOpenAlertAttachment={handleDownloadAttachment}
                          />
                        </Col>
                      ))}
                    </Row>
                  </Container>
                  {calculateLength(attachmentsOfTask) < attachmentsOfTaskTotal ? (
                    <div className={'text-center text-uppercase'}>
                      <i className="fa fa-repeat mr-1" />
                      <span onClick={() => loadMoreAttachment()}>{t('view_more')}</span>
                    </div>
                  ) : null}
                  <ConfirmationModal
                    isOpen={openAlert}
                    txtTitle={t('confirm_remove')}
                    txtCancel={t('cancel')}
                    txtConfirm={t('confirm')}
                    funConfirm={handleRemoveAttachment}
                    funClose={closeAlert}
                  >
                    {t('are_you_sure_remove_this_attachment')}
                  </ConfirmationModal>
                  <ConfirmationModal
                    isOpen={openAlertComment}
                    txtTitle={t('confirm_remove')}
                    content={t('are_you_sure_remove_this_comment')}
                    txtCancel={t('cancel')}
                    txtConfirm={t('confirm')}
                    funConfirm={() => handleRemoveComment()}
                    funClose={() => closeAlertComment()}
                  >
                    {t('are_you_sure_remove_this_comment')}
                  </ConfirmationModal>

                  <Container>
                    <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                      <AppText type={'title-menu'}>{t('issue')}:</AppText>
                    </div>
                    {currentTask.Issue ? (
                      <div className="margin-left-15px">
                        <label className="mb-0 margin-right-5px">{t('subject')}:</label>
                        <Link
                          to={`/sprints/${value.projectId}/issues/${currentTask.Issue.status}/${currentTask.Issue.id}`}
                        >
                          {currentTask.Issue.subject}
                        </Link>
                        <IssueAttachment
                          value={{
                            issueId: currentTask.Issue.id
                          }}
                          uploadEnable={false}
                          deleteEnable={false}
                        />
                      </div>
                    ) : (
                      <div className="content">
                        <p>{t('no_issue')}</p>
                      </div>
                    )}

                    <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                      <AppText type={'title-menu'}>{t('streamline')}:</AppText>
                    </div>
                    <div className={'default-paper p-3'}>
                      {calculateLength(currentTask.detail) ? (
                        currentTask.detail.map((item, index) => (
                          <Row key={index} className="streamline-item">
                            <Col md="3" className="col-3">
                              <Link to={`/profile/${item.User.id}`}>
                                <img
                                  className={'avatar-mini'}
                                  alt={item.User && item.User.name}
                                  title={item.User && item.User.name}
                                  src={
                                    item.User && item.User.avatar
                                      ? `${getEnv('S3_BUCKET')}/${item.User && item.User.avatar}`
                                      : defaultAvatar
                                  }
                                />
                              </Link>
                            </Col>
                            <Col md="4" className="col-9">{`${item.fromStates.name} -> ${item.toStates.name}`}</Col>
                            <Col md="5" className="col-12">
                              <i className={'fa fa-clock-o'} /> {formatDateTime(item.createdAt, 'LLLL', 1)}
                            </Col>
                          </Row>
                        ))
                      ) : (
                        <div className={'text-center'}>
                          <p>{t('not_found')}</p>
                        </div>
                      )}
                    </div>

                    <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                      <AppText type={'title-menu'}>{t('comments')}:</AppText>
                    </div>
                    {calculateLength(commentsOfTask) < commentsOfTaskTotal ? (
                      <div className="content">
                        <u className="more" onClick={() => loadMoreComments()}>
                          {t('view_comments_older')}
                        </u>
                      </div>
                    ) : null}
                    {calculateLength(commentsOfTask) ? (
                      <div className="detail-task-box mb-4">
                        {commentsOfTask.map((comment, index) => {
                          return (
                            <div
                              className="panel panel-white post panel-shadow"
                              key={index}
                              onMouseEnter={() => {
                                if (userId === comment.userId) {
                                  setCommentAction(true);
                                  setCommentHoverActor(comment.id);
                                }
                              }}
                              onMouseLeave={() => {
                                if (userId === comment.userId) {
                                  setCommentAction(false);
                                  setCommentHoverActor(comment.id);
                                }
                              }}
                            >
                              <div className="post-heading">
                                <div className="image">
                                  <Link to={`/profile/${comment.User.id}`}>
                                    <img
                                      src={`${getEnv('S3_BUCKET')}/${comment.User.avatar}`}
                                      className="img-circle avatar"
                                      alt="user profile"
                                    />
                                  </Link>
                                </div>
                                <div className="meta">
                                  <div className="title">
                                    <b>@{comment.User && comment.User.name}</b>
                                  </div>
                                  <p className="text-muted time">
                                    <i className="fa fa-clock-o margin-right-5px" />
                                    {formatDateTime(comment.createdAt, 'LLLL', 1)}
                                  </p>
                                </div>
                                <small>{fromNow(comment.createdAt)}</small>
                              </div>
                              <div className="post-description">
                                <ReactMarkdown
                                  plugins={[breaks]}
                                  source={comment.content}
                                  className="bubble markdown-content p-3"
                                  renderers={{ link: props => linkRenderTarget(props) }}
                                />
                                <div className="right">
                                  {isAllowPermission(appPermissions, PERM_DELETE_COMMENT_TASK, selectedProject.id) &&
                                    commentAction &&
                                    comment.id === commentHoverActor && (
                                      <span
                                        className={'d-inline-block remove-btn'}
                                        color={'primary'}
                                        onClick={() => handleOpenAlertComment(comment)}
                                      >
                                        <i className="fa fa fa-trash-o" />
                                      </span>
                                    )}
                                  {isAllowPermission(appPermissions, PERM_UPDATE_COMMENT_TASK, selectedProject.id) &&
                                    commentAction &&
                                    comment.id === commentHoverActor && (
                                      <span
                                        className={'d-inline-block edit-btn'}
                                        color={'primary'}
                                        onClick={() => {
                                          setIsEditComment(true);
                                          setContent(comment && comment.content);
                                          setCommentId(comment && comment.id);
                                        }}
                                      >
                                        <i className="fa fa-edit" />
                                      </span>
                                    )}
                                </div>
                              </div>
                              <div className="underlined"></div>
                            </div>
                          );
                        })}
                      </div>
                    ) : (
                      <div className="content">
                        <p>{t('no_comment')}</p>
                      </div>
                    )}
                    <u className={'mt-3 full'}>{t('add_comment')}: </u>
                    {!isEditComment && (
                      <MarkdownEditor
                        value={content}
                        onChange={value => setContent(value)}
                        // onKeyPress={ createNewComment }
                      />
                    )}

                    {isEditComment && (
                      <MarkdownEditor
                        value={content}
                        onChange={value => setContent(value)}
                        // onKeyPress={ createNewComment }
                      />
                    )}
                    <div>
                      <Button
                        disabled={!content || isLoading}
                        className={'add-comment-button base-btn'}
                        size={'sm'}
                        onClick={() => {
                          if (!isEditComment) {
                            addComment({
                              taskId: value.taskId,
                              projectId: value.projectId,
                              content: content
                            });
                          } else {
                            updateComment({
                              projectId: value.projectId,
                              content: content,
                              commentId: commentId
                            });
                          }
                          setIsEditComment(false);
                          setContent('');
                          setCommentId(0);
                          setIsLoading(true);
                        }}
                      >
                        {t('comment')}
                      </Button>
                      {isEditComment && (
                        <Button
                          className={'margin-right-10px outline-base-btn cancel-comment-button'}
                          size={'sm'}
                          onClick={() => {
                            setIsEditComment(false);
                            setContent('');
                            setCommentId(0);
                          }}
                        >
                          {t('cancel')}
                        </Button>
                      )}
                    </div>
                  </Container>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onClose && onClose();
                  }}
                >
                  {t('close')}
                </Button>
              </ModalFooter>
            </Modal>
          ) : (
            <Loader isOpen={!currentTask} />
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

DetailTaskDialog.propTypes = {
  currentTask: object,
  selectedProject: object,
  getAttachmentsOfTask: func,
  loadMoreAttachmentsOfTask: func,
  attachmentsOfTask: array,
  attachmentsOfTaskTotal: number,
  removeAttachmentOfTask: func,
  downloadFile: func,
  uploadFileToTask: func,
  getCommentsOfTask: func,
  loadMoreCommentsOfTask: func,
  commentsOfTask: array,
  commentsOfTaskTotal: number,
  clearCurrentTask: func,
  addComment: func,
  deleteComment: func,
  updateComment: func,
  onClose: func,
  value: object,
  isOpen: bool,
  status: string,
  getUsersByProject: func,
  assignTaskToUser: func,
  onDirectToTask: func,
  appPermissions: array,
  usersIntoProject: array,
  userId: any
};

const mapStateToProps = store => {
  return {
    error: store.common.error,
    currentTask: store.sprint.currentTask,
    selectedProject: store.sprint.selectedProject,
    attachmentsOfTask: store.wiki.attachmentsOfTask,
    attachmentsOfTaskTotal: store.wiki.attachmentsOfTaskTotal,
    commentsOfTask: store.comment.commentsOfTask,
    commentsOfTaskTotal: store.comment.commentsOfTaskTotal,
    status: store.comment.status,
    usersIntoProject: store.sprint.usersIntoProject,
    appPermissions: store.auth.permissions,
    userId: store.auth && store.auth.user && store.auth.user.id
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getDetailTask,
      uploadFileToTask,
      getAttachmentsOfTask,
      loadMoreAttachmentsOfTask,
      removeAttachmentOfTask,
      downloadFile,
      getCommentsOfTask,
      getCommentsOfUser,
      loadMoreCommentsOfTask,
      clearCurrentTask,
      addComment,
      deleteComment,
      updateComment,
      getUsersByProject,
      assignTaskToUser
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(DetailTaskDialog));
