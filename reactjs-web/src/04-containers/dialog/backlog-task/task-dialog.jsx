import React, { memo, useEffect, useState, useRef, useMemo } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, FormGroup, Label } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { WithContext as ReactTags } from 'react-tag-input';
import {
  formatDateTime,
  getDateAt235959ZeroGMT,
  isAllowPermission,
  calculateLength,
  safelyParseJSON,
  filterUserRelateInProject,
  mapArrayGetValue,
  getOptionUserRelate
} from '../../../08-helpers/common';
import './style.css';
import { PRIORITY_OPTIONS, TASK_TYPE_OPTIONS } from '../../../05-utils/commonData';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsersByProject, getTasks, getSuggestTags } from '../../../01-actions/sprint';
import { func, array, any, bool, string, object } from 'prop-types';
import DatePicker from 'react-datepicker';
import {
  LIST_USER_INTO_PROJECT,
  PERM_LIST_TASK_BACKLOG_BY_PROJECT,
  PERM_REMOVE_TASK
} from '../../../07-constants/app-permission';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import FieldInput from '../../../03-components/FieldInput';
import SelectPropsBox from '../../../03-components/SelectPropsBox';

const KeyCodes = {
  comma: 188,
  enter: 13
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

const TaskDialog = ({
  getUsersByProject,
  usersIntoProject,
  onOK,
  projectId,
  isOpen,
  suggestTasks,
  onClose,
  getTasks,
  title,
  appPermissions,
  currentTask,
  onDelete,
  taskItem,
  tagsSuggest
}) => {
  const [state, setState] = useState({
    content: '',
    description: '',
    estimate: '',
    tags: [],
    selectedOptionPriority: null,
    selectedOptionAssign: null,
    selectedOptionDependencies: null,
    dueDate: null,
    selectedOptionTaskType: null,
    issueId: null,
    allUserRelate: []
  });

  const files = useRef(null);

  const {
    content,
    description,
    estimate,
    tags,
    selectedOptionPriority,
    selectedOptionAssign,
    selectedOptionDependencies,
    dueDate,
    selectedOptionTaskType,
    issueId,
    allUserRelate
  } = state;

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, LIST_USER_INTO_PROJECT, projectId)) getUsersByProject({ projectId });
    if (isAllowPermission(appPermissions, PERM_LIST_TASK_BACKLOG_BY_PROJECT, projectId))
      getTasks({ projectId, type: 1 });
    getSuggestTags(projectId);
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const getDepenciesTask = dependencies => {
    return (
      calculateLength(dependencies) &&
      dependencies.map(item => ({
        value: item.parentId,
        label: `#${item.parentId} ${item.parentTask && item.parentTask.content}`
      }))
    );
  };

  const getTags = allTags => {
    return calculateLength(allTags)
      ? allTags.map(item => ({
          id: item,
          text: item
        }))
      : [];
  };

  const checkUserRelateParam =
    (currentTask && safelyParseJSON(currentTask.userRelate)) || (taskItem && safelyParseJSON(taskItem.userRelate));

  const userRelateMemo = useMemo(() => filterUserRelateInProject(usersIntoProject, checkUserRelateParam), [
    usersIntoProject,
    currentTask,
    taskItem
  ]);
  const dependciesTaskMemo = useMemo(() => getDepenciesTask(currentTask && currentTask.dependencies), [currentTask]);
  const optionUsers = useMemo(() => getOptionUserRelate(usersIntoProject), [usersIntoProject]);

  // component will recieve props will set props to state
  useEffect(() => {
    /*
      taskItem is props passed from tabissue when use tranfrom issue to task
    */
    const dataItem = taskItem || currentTask;

    setState({
      content: dataItem ? dataItem.content : '',
      description: dataItem ? dataItem.description : '',
      estimate: dataItem ? dataItem.estimate : '',
      tags: dataItem && dataItem.tag ? getTags(safelyParseJSON(dataItem.tag)) : [],
      selectedOptionPriority: dataItem ? dataItem.priority : '',
      selectedOptionAssign: dataItem ? (dataItem.assigneeAs && dataItem.assigneeAs.id) || dataItem.assignee : '',
      selectedOptionDependencies: dataItem ? dependciesTaskMemo : [],
      dueDate: dataItem ? dataItem.dueDate : '',
      selectedOptionTaskType: dataItem ? dataItem.type : TASK_TYPE_OPTIONS[0].value,
      issueId: dataItem ? dataItem.issueId : '',
      allUserRelate: dataItem ? userRelateMemo : []
    });
  }, [currentTask, taskItem]);

  const handleTagDelete = i => {
    setState({
      ...state,
      tags: [...state.tags].filter((tag, index) => index !== i)
    });
  };

  const handleTagAddition = tag => {
    setState({
      ...state,
      tags: [...state.tags, tag]
    });
  };

  const handleTagDrag = (tag, currPos, newPos) => {
    const newTags = tags.slice();
    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);
    setState({
      ...state,
      tags: newTags
    });
  };

  const clearAll = () => {
    setState({
      content: null,
      description: null,
      estimate: 0,
      tags: [],
      selectedOptionPriority: null,
      selectedOptionAssign: null,
      selectedOptionDependencies: null,
      dueDate: null,
      selectedOptionTaskType: TASK_TYPE_OPTIONS[0].value,
      issueId: null
    });
  };

  const optionTask =
    suggestTasks &&
    suggestTasks.map(task => {
      return {
        value: task && task.id,
        label: `#${task && task.taskCode ? task && task.taskCode : task && task.id} ${task && task.content}`
      };
    });

  const onOkDialog = () => {
    const dependencies = mapArrayGetValue(selectedOptionDependencies);
    const userRelate = mapArrayGetValue(allUserRelate);
    const fileData = !currentTask ? files && files.current.files : [];

    onOK({
      content,
      description,
      estimate: Number(estimate) || 0,
      tags,
      priority: selectedOptionPriority,
      dependencies,
      assignee: selectedOptionAssign,
      dueDate,
      files: fileData,
      type: selectedOptionTaskType,
      issueId: issueId || null,
      userRelate
    });
    clearAll();
  };

  const onChangePriority = e => setState({ ...state, selectedOptionPriority: e && e.value });
  const onChangeDueDate = e => setState({ ...state, dueDate: e ? getDateAt235959ZeroGMT(e) : null });
  const onChangeAssignee = e => setState({ ...state, selectedOptionAssign: e && e.value });
  const onChangeDependencies = selectedOptionDependencies => setState({ ...state, selectedOptionDependencies });
  const onChangeRelateUser = allUserRelate => setState({ ...state, allUserRelate });
  const onChangeInputValue = (value, name) => setState({ ...state, [name]: value });
  const onChangeTaskType = e =>
    setState({
      ...state,
      selectedOptionTaskType: e && e.value,
      allUserRelate: e && e.value === TASK_TYPE_OPTIONS[0].value && []
    });

  const _tagsSuggest = tagsSuggest.map(suggestion => ({ id: suggestion, text: suggestion }));

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered={true} size="xl" backdrop="static">
          <ModalHeader toggle={onClose}>{title}</ModalHeader>
          <ModalBody className="overflow-y-auto overflow-x-hidden">
            <div className="content-modal d-flex flex-column align-items-stretch">
              <FieldInput
                placeholder={t('Task name')}
                type="text"
                classes="mb-4"
                defaultValue={content}
                name="content"
                onChangeValue={onChangeInputValue}
              />
              <MarkdownEditor
                label={t('Description')}
                value={description}
                onChange={value => setState({ ...state, description: value })}
              />
              <Row>
                <Col md="6">
                  <FormGroup className="group-select-tag">
                    <Label className={'margin-top-15px'}>{t('tags')}</Label>
                    <ReactTags
                      autofocus={false}
                      tags={tags}
                      inline={true}
                      suggestions={_tagsSuggest}
                      delimiters={delimiters}
                      handleDelete={handleTagDelete}
                      handleAddition={handleTagAddition}
                      handleDrag={handleTagDrag}
                      placeholder={t('select_tags')}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('dependencies')}</Label>
                    <SelectPropsBox
                      isMulti
                      isAnimatate
                      keyLabel="label"
                      keyValue="value"
                      placeholder="Select"
                      options={optionTask}
                      value={selectedOptionDependencies}
                      onChange={onChangeDependencies}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('assignee')}</Label>
                    <SelectPropsBox
                      isClearable={false}
                      value={selectedOptionAssign}
                      options={optionUsers}
                      keyValue="value"
                      keyLabel="label"
                      placeholder="Select"
                      onChange={onChangeAssignee}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('task_type')}</Label>
                    <SelectPropsBox
                      isClearable={false}
                      value={selectedOptionTaskType}
                      options={TASK_TYPE_OPTIONS}
                      keyValue="value"
                      keyLabel="label"
                      placeholder="Select"
                      onChange={onChangeTaskType}
                    />
                  </FormGroup>
                </Col>
                <Col md="6">
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('due_date')}</Label>
                    <div className={'d-flex popup backlog-date-picker'}>
                      <DatePicker
                        className={'flex-grow-1'}
                        value={dueDate ? formatDateTime(dueDate, 'DD/MM/YYYY') : ''}
                        placeholderText={t('end_date')}
                        dropdownMode={'scroll'}
                        onChange={onChangeDueDate}
                      />
                    </div>
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('priority')}</Label>
                    <SelectPropsBox
                      isClearable={false}
                      value={selectedOptionPriority}
                      options={PRIORITY_OPTIONS}
                      keyValue="value"
                      keyLabel="label"
                      onChange={onChangePriority}
                      placeholder="Select"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('estimate')}</Label>
                    <FieldInput
                      placeholder={t('Estimate')}
                      type="number"
                      defaultValue={estimate}
                      name="estimate"
                      onChangeValue={onChangeInputValue}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className={'margin-top-15px'}>{t('user_relate')}</Label>
                    <SelectPropsBox
                      isMulti
                      isAnimatate
                      value={allUserRelate}
                      options={optionUsers}
                      keyValue="value"
                      keyLabel="label"
                      isDisabled={!selectedOptionTaskType || selectedOptionTaskType === 1}
                      onChange={onChangeRelateUser}
                      placeholder="Select"
                    />
                  </FormGroup>
                </Col>
                {!currentTask && (
                  <Col sm="12">
                    <FormGroup>
                      <Label className={'margin-top-15px'}>{t('attachments')}</Label>
                      <br />
                      <small style={{ color: 'red' }}>* {t('limit_3_files')}</small>
                      <br />
                      <input type="file" multiple ref={files} />
                    </FormGroup>
                  </Col>
                )}
              </Row>
            </div>
          </ModalBody>
          <ModalFooter className="mt-4">
            <div className={'flex-grow-1 d-flex'}>
              {isAllowPermission(appPermissions, PERM_REMOVE_TASK) && currentTask && (
                <Button className="outline-base-remove-btn mr-auto" size={'sm'} onClick={onDelete}>
                  {t('remove')}
                </Button>
              )}
              <Button
                className={'margin-right-10px outline-base-btn ml-auto'}
                size={'sm'}
                onClick={() => onClose && onClose()}
              >
                {t('cancel')}
              </Button>
              <Button disabled={!content} className={'base-btn'} size={'sm'} onClick={onOkDialog}>
                {t('save')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

TaskDialog.propTypes = {
  getUsersByProject: func,
  usersIntoProject: array,
  onDelete: func,
  onOK: func,
  projectId: any,
  isOpen: bool,
  suggestTasks: array,
  onClose: func,
  getTasks: func,
  title: string,
  appPermissions: array,
  taskItem: object,
  currentTask: object,
  tagsSuggest: array
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  usersIntoProject: store.sprint.usersIntoProject,
  suggestTasks: store.sprint.suggestTasks,
  currentTask: store.sprint.currentTask,

  tagsSuggest: store.sprint.tags
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsersByProject,
      getTasks,
      getSuggestTags
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(TaskDialog));
