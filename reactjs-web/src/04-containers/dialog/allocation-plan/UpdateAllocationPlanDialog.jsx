import { array, bool, func, object, string } from 'prop-types';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import ControlDatePicker from '../../../03-components/DateTimePicker';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import { UPDATE_ALLOCATION_PLANS_SUCCESS } from '../../../07-constants/allocation-plan';
import { getDateAt000000ZeroGMT, getDateAt235959ZeroGMT, getDateUTCtoLocal } from '../../../08-helpers/common';
import styles from './AddAllocationPlanDialog.module.css';

function UpdateAllocationPlanDialog({ isOpen, status, onClose, listUsers, onOK, value }) {

  const [effortPercent, setEffortPercent] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    if (status === UPDATE_ALLOCATION_PLANS_SUCCESS) {
      onClose();
    }
  }, [status]);

  useEffect(() => {
    if (isOpen && value && value.id) {
      setEffortPercent(value.effortPercent);
      setStartDate(getDateUTCtoLocal(value.startDate));
      setEndDate(getDateUTCtoLocal(value.endDate));
    }
  }, [isOpen]);

  const handleUpdateAllocation = () => {
    onOK({
      id: value.id,
      effortPercent: Number(effortPercent),
      startDate: getDateAt000000ZeroGMT(startDate),
      endDate: getDateAt235959ZeroGMT(endDate)
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} centered fade={false} toggle={onClose} backdrop="static">
          <ModalHeader toggle={onClose}>{t('update_allocation_plan')}</ModalHeader>
          <ModalBody className="pb-2">
            <Row className={styles.row2}>
              <Col xs={12} className="px-2 mb-3">
                <SelectPropsBox
                  options={listUsers}
                  value={value && value.userId}
                  keyValue="id"
                  keyLabel="name"
                  isDisabled
                  placeholder={'select_user'}
                />
              </Col>

              <Col xs={12} className="px-2 mb-3">
                <Input
                  defaultValue={(value && value.effortPercent) || ''}
                  type="number"
                  placeholder={t('effort')}
                  onChange={e => setEffortPercent(e.target.value)}
                  name="effortPercent"
                  className="focus-border"
                />
              </Col>

              <Col xs={12} className="px-2 mb-3">
                <ControlDatePicker
                  isLocalFormat
                  value={startDate}
                  format="DD/MM/YYYY"
                  onChange={startDate => setStartDate(startDate)}
                  placeholder={t('start_date')}
                />
              </Col>

              <Col xs={12} className="px-2 mb-3">
                <ControlDatePicker
                  isLocalFormat
                  value={endDate}
                  format="DD/MM/YYYY"
                  onChange={endDate => setEndDate(endDate)}
                  placeholder={t('end_date')}
                />
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter className="d-flex justify-content-between">
            <Button className={'outline-base-btn'} size={'sm'} onClick={onClose}>
              {t('cancel')}
            </Button>
            <Button className={'base-btn'} size={'sm'} onClick={handleUpdateAllocation}>
              {t('Update')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

UpdateAllocationPlanDialog.propTypes = {
  isOpen: bool,
  listUsers: array,
  value: object,
  status: string,
  onClose: func,
  onOK: func
};

export default memo(UpdateAllocationPlanDialog);
