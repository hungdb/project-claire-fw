import { array, bool, func, string } from 'prop-types';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import Select from 'react-select';
import { Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import ControlDatePicker from '../../../03-components/DateTimePicker';
import { CREATE_ALLOCATION_PLANS_SUCCESS } from '../../../07-constants/allocation-plan';
import {
  getDateAt000000ZeroGMT,
  getDateAt235959ZeroGMT,
  calculateLength
} from '../../../08-helpers/common';
import styles from './AddAllocationPlanDialog.module.css';

function AddAllocationPlanDialog({ isOpen, status, onClose, listUsers, onOK }) {
  const [allocationRaws, setAllocationRaws] = useState([
    {
      users: null,
      effort: null,
      startDate: null,
      endDate: null
    }
  ]);

  useEffect(() => {
    if (status === CREATE_ALLOCATION_PLANS_SUCCESS) {
      setAllocationRaws([
        {
          users: null,
          effort: null,
          startDate: null,
          endDate: null
        }
      ]);
      onClose();
    }
  }, [status]);

  const handleChangeAllocation = (index, valueField, nameField) => {
    const newAllocationRaw = allocationRaws.map((allocation, i) => {
      if (i === index)
        return {
          ...allocation,
          [nameField]: valueField
        };
      return allocation;
    });
    setAllocationRaws(newAllocationRaw);
  };

  const handleAddAllocationRaw = () => {
    const newAllocationRaw = [
      ...allocationRaws,
      {
        users: null,
        effort: null,
        startDate: null,
        endDate: null
      }
    ];
    setAllocationRaws(newAllocationRaw);
  };

  const handleRemoveAllocationRaw = index => {
    const newAllocationRaw = allocationRaws.filter((allocation, i) => i !== index);
    setAllocationRaws(newAllocationRaw);
  };

  const formatAllocationRawsToSend = allocationRaws => {
    let allocations = [];

    allocationRaws.forEach(
      allocationRaw =>
        allocationRaw.users &&
        allocationRaw.users.forEach(user => {
          allocations = [
            ...allocations,
            {
              userId: user.value,
              effortPercent: allocationRaw.effort,
              startDate: allocationRaw.startDate,
              endDate: getDateAt235959ZeroGMT(new Date(allocationRaw.endDate)),
            }
          ];
        })
    );
    return allocations;
  };

  const handleAddAllocation = () => {
    onOK(formatAllocationRawsToSend(allocationRaws));
  };

  const userOptions = Array.isArray(listUsers) ? listUsers.map(user => ({ value: user.id, label: user.name })) : [];

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} centered size="xl" fade={false} toggle={onClose} backdrop="static">
          <ModalHeader toggle={onClose}>{t('add_allocation_plan')}</ModalHeader>
          <ModalBody>
            {calculateLength(allocationRaws)
              ? allocationRaws.map((allocation, i) => (
                <Row key={`key-${i}`} className={`mb-2 ${styles.row2}`}>
                  <Col className="px-2 mb-3">
                    <Select
                      className={styles.selectUser}
                      placeholder={t('select_users')}
                      isMulti
                      closeMenuOnSelect={false}
                      options={userOptions}
                      value={allocation.users}
                      onChange={users => handleChangeAllocation(i, users, 'users')}
                    />
                  </Col>
                  <Input
                    className={`mx-2 mb-3 focus-border ${styles.inputEffort}`}
                    type="number"
                    placeholder={t('effort')}
                    value={allocation.effort || ''}
                    onChange={e => handleChangeAllocation(i, Number(e.target.value), 'effort')}
                  />
                  <div className={`mx-2 mb-3  ${styles.inputDate}`}>
                    <ControlDatePicker
                      value={allocation.startDate}
                      format="DD/MM/YYYY"
                      placeholder="start_date"
                      onChange={startDate =>
                        handleChangeAllocation(i, startDate ? getDateAt000000ZeroGMT(startDate) : null, 'startDate')
                      }
                    />
                  </div>
                  <div className={`mx-2 mb-3  ${styles.inputDate}`}>
                    <ControlDatePicker
                      value={allocation.endDate}
                      format="DD/MM/YYYY"
                      placeholder="start_date"
                      onChange={endDate =>
                        handleChangeAllocation(i, endDate ? getDateAt000000ZeroGMT(endDate) : null, 'endDate')
                      }
                    />
                  </div>
                  <div className="d-flex align-items-center mx-2 mb-3">
                    <i
                      className={`fa fa-times-circle text-warning cursor-pointer ${styles.btnRmAlc}`}
                      onClick={() => handleRemoveAllocationRaw(i)}
                    />
                  </div>
                </Row>
              ))
              : null}
            <div className="text-center">
              <i
                className={`fa fa-plus-circle text-success cursor-pointer ${styles.btnAddAlc}`}
                onClick={handleAddAllocationRaw}
              />
            </div>
          </ModalBody>
          <ModalFooter className="d-flex justify-content-between">
            <Button className={'outline-base-btn'} size={'sm'} onClick={onClose}>
              {t('cancel')}
            </Button>
            <Button className={'base-btn'} size={'sm'} onClick={handleAddAllocation}>
              {t('add')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

AddAllocationPlanDialog.propTypes = {
  isOpen: bool,
  status: string,
  onClose: func,
  listUsers: array,
  onOK: func,
};

export default memo(AddAllocationPlanDialog);
