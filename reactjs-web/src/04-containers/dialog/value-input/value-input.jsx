import React, { PureComponent } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { any, string, bool, func } from 'prop-types';

class ValueInputDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  static propTypes = {
    value: any,
    inputLabel: any,
    allowEmpty: any,
    title: string,
    cancelTitle: string,
    okTitle: string,
    isOpen: bool,
    onClose: func,
    onOK: func
  };

  componentDidUpdate(prevProps) {
    const { isOpen: prevIsOpen } = prevProps;
    const { isOpen } = this.props;
    if (!prevIsOpen && isOpen) {
      this.setState({
        value: ''
      });
    }
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        toggle={() => {
          this.props.onClose && this.props.onClose();
        }}
        centered
        backdrop="static"
      >
        <ModalHeader
          toggle={() => {
            this.props.onClose && this.props.onClose();
          }}
        >
          {this.props.title}
        </ModalHeader>
        <ModalBody>
          <div className="content-modal d-flex">
            <Input
              defaultValue={this.props.value}
              onChange={e => {
                this.setState({ value: e.target.value });
              }}
              className={'flex-grow-1'}
              autoFocus
              placeholder={this.props.inputLabel}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <div className={'flex-grow-1 d-flex justify-content-end'}>
            <div>
              <Button
                className={'margin-right-10px outline-base-btn'}
                size={'sm'}
                onClick={() => {
                  this.setState({ value: '' });
                  this.props.onClose && this.props.onClose();
                }}
              >
                {this.props.cancelTitle}
              </Button>
              <Button
                disabled={!this.props.allowEmpty && !this.state.value}
                className={'base-btn'}
                size={'sm'}
                onClick={() => {
                  this.setState({ value: '' });
                  this.props.onOK && this.props.onOK(this.state.value);
                }}
              >
                {this.props.okTitle}
              </Button>
            </div>
          </div>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ValueInputDialog;
