import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { object, bool, func } from 'prop-types';

class AddProjectEnvironmentDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      envName: ''
    };
  }

  static propTypes = {
    value: object,
    isOpen: bool,
    editMode: bool,
    onClose: func,
    onRemove: func,
    onOK: func
  };

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    this.setState({ permissionSchemeName: nextProps.value ? nextProps.value.name : '' });
  }

  componentDidUpdate(prevProps) {
    const { isOpen: prevIsOpen } = prevProps;
    const { isOpen } = this.props;
    if (!prevIsOpen && isOpen) {
      this.setState({
        envName: ''
      });
    }
  }

  render() {
    const { onClose } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal isOpen={this.props.isOpen} toggle={() => onClose && onClose()} centered backdrop="static">
            <ModalHeader toggle={() => onClose && onClose()}>{t('new_deployment_env')}</ModalHeader>
            <ModalBody>
              <div className="content-modal d-flex">
                <Input
                  defaultValue={this.state.envName}
                  onChange={e => {
                    this.setState({ envName: e.target.value });
                  }}
                  className={'flex-grow-1'}
                  autoFocus
                  placeholder={t('project_deployment_env_name')}
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div>
                  {this.props.editMode && (
                    <Button
                      className={'outline-base-remove-btn'}
                      size={'sm'}
                      onClick={() => {
                        this.props.onRemove && this.props.onRemove();
                      }}
                    >
                      {t('remove')}
                    </Button>
                  )}
                </div>
                <div>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => {
                      this.props.onClose && this.props.onClose();
                    }}
                  >
                    {t('cancel')}
                  </Button>
                  <Button
                    disabled={!this.state.envName}
                    className={'base-btn'}
                    size={'sm'}
                    onClick={() => {
                      this.props.onOK &&
                        this.props.onOK({
                          name: this.state.envName
                        });
                    }}
                  >
                    {t('add')}
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default AddProjectEnvironmentDialog;
