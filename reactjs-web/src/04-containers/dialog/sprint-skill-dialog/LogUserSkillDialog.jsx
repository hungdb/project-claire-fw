import React, { PureComponent } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { listAllSkills } from '../../../01-actions/skill.js';
import { removeDuplicateObjInArray } from '../../../08-helpers/common';
import { SKILL_TYPES } from '../../../05-utils/commonData';
import { addUserSkillInSprint, getUsersByProject } from '../../../01-actions/sprint';
import Select from 'react-select';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, bool, func, object, number } from 'prop-types';
import MarkdownEditor from '../../../03-components/MarkdownEditor';

class SprintAdminSkillDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      skillId: 0,
      descriptionSkill: 0,
      userSkillId: 0,
      userId: 0
    };
  }

  static propTypes = {
    allSkills: array,
    isOpen: bool,
    onClose: func,
    listAllSkills: func,
    activeSprint: object,
    addUserSkillInSprint: func,
    getUsersByProject: func,
    usersIntoProject: array,
    userSelfId: number
  };

  componentDidMount() {
    this.init(this.props);
  }

  init = props => {
    const { projectId } = props;
    this.props.listAllSkills();
    this.props.getUsersByProject({ projectId });
  };

  handleSelectSkill(select) {
    this.setState({
      skillId: select && select.value
    });
  }

  handleSelectUser(select) {
    this.setState({
      userId: select && select.value
    });
  }

  _renderUserSelector = t => {
    return (
      <Select
        options={
          this.props.usersIntoProject &&
          this.props.userSelfId &&
          removeDuplicateObjInArray({ list: this.props.usersIntoProject, key: 'userId' })
            .filter(u => u.User.id !== this.props.userSelfId)
            .map(u => ({ value: u.User.id, label: u.User.name }))
        }
        placeholder={t('select_user')}
        onChange={e => this.handleSelectUser(e)}
      />
    );
  };

  _renderSkillSelector(t) {
    return (
      <Select
        options={this.props.allSkills && this.props.allSkills.map(s => ({ value: s.id, label: s.name }))}
        placeholder={t('select_skill')}
        onChange={e => this.handleSelectSkill(e)}
      />
    );
  }

  handleAddNewUserSkill = () => {
    const { skillId, descriptionSkill, userId } = this.state;
    const { activeSprint } = this.props;
    const sprintId = activeSprint.id;
    this.props.addUserSkillInSprint({ userId, skillId, note: descriptionSkill, sprintId, type: SKILL_TYPES.project });
    this.handleClose();
  };

  handleClose = () => {
    this.setState(
      {
        skillId: 0,
        descriptionSkill: 0
      },
      () => {
        this.props.onClose();
      }
    );
  };

  render() {
    const { skillId, descriptionSkill, userId } = this.state;
    const { onClose } = this.props;
    const disableBtn = userId === 0 || skillId === 0 || descriptionSkill === 0;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal isOpen={this.props.isOpen} toggle={onClose} centered={true} size="lg" backdrop="static">
            <ModalHeader toggle={onClose}>{t('log_user_skill')}</ModalHeader>
            <ModalBody>
              <div className="content-modal d-flex flex-column align-items-stretch">
                <div>{this._renderUserSelector(t)}</div>
                <div className="mt-4">{this._renderSkillSelector(t)}</div>
                <div style={{ zIndex: 0 }}>
                  <MarkdownEditor
                    className={'flex-grow-1 full margin-top-40px'}
                    label={t('Description')}
                    onChange={e => {
                      this.setState({ descriptionSkill: e });
                    }}
                  />
                  <p className={'mt-1 font-size-14'}>
                    (*) {t('description_about_any_things_that_user_learned_or_improved_with_this_skill')}
                  </p>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-end'}>
                <Button
                  className={'outline-base-btn mr-1'}
                  size={'sm'}
                  onClick={() => this.props.onClose && this.handleClose()}
                >
                  {t('cancel')}
                </Button>
                <Button
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => {
                    this.handleAddNewUserSkill();
                  }}
                  disabled={disableBtn}
                >
                  {t('save')}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    allSkills: store.skill.allSkills,
    activeSprint: store.sprint.activeSprint,
    usersIntoProject: store.sprint.usersIntoProject,
    userSelfId: store.auth.user.id,
    projectId: store.sprint.selectedProject && store.sprint.selectedProject.id
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listAllSkills,
      addUserSkillInSprint,
      getUsersByProject
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SprintAdminSkillDialog);
