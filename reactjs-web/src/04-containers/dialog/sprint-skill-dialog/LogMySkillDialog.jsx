import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { listAllSkills } from '../../../01-actions/skill.js';
import { addMySkillInSprint } from '../../../01-actions/sprint';
import { SKILL_TYPES } from '../../../05-utils/commonData';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { array, bool, func, object } from 'prop-types';
import LazyComponent from '../../../03-components/LazyComponent/index.jsx';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
const MarkdownEditor = LazyComponent(() => import('../../../03-components/MarkdownEditor'));
const SelectPropsBox = LazyComponent(() => import('../../../03-components/SelectPropsBox'));

const SprintUserSkillDialog = ({ allSkills, isOpen, onClose, listAllSkills, activeSprint, addMySkillInSprint }) => {
  const [state, setState] = useState({
    skillId: 0,
    descriptionSkill: ''
  });

  const { skillId, descriptionSkill } = state;

  useEffect(() => {
    listAllSkills();
  }, []);

  const handleSelectSkill = select => setState({ ...state, skillId: select && select.value });

  const handleClose = () => {
    setState({
      skillId: 0,
      descriptionSkill: ''
    });
    onClose();
  };

  const handleAddNewUserSkill = () => {
    const sprintId = activeSprint.id;
    addMySkillInSprint({ skillId, note: descriptionSkill, sprintId, type: SKILL_TYPES.yourSelf });
    handleClose();
  };

  const disableBtn = !skillId || !descriptionSkill;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered={true} size="lg" backdrop="static">
          <ModalHeader toggle={onClose}>{t('log_my_skill')}</ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">
              <SelectPropsBox
                placeholder="select_skill"
                keyLabel="name"
                keyValue="id"
                options={allSkills}
                onChange={handleSelectSkill}
                value={skillId}
              />
              <div style={{ zIndex: 0 }}>
                <MarkdownEditor
                  className={'flex-grow-1 full margin-top-40px'}
                  label={t('Description')}
                  onChange={e => setState({ ...state, descriptionSkill: e })}
                />
                <p className={'mt-1 font-size-14'}>
                  (*) {t('description_about_any_things_that_you_learned_or_improved_with_this_skill')}
                </p>
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button
                className={'outline-base-btn margin-right-10px'}
                size={'sm'}
                onClick={() => onClose && handleClose()}
              >
                {t('cancel')}
              </Button>
              <Button className={'base-btn'} size={'sm'} onClick={handleAddNewUserSkill} disabled={disableBtn}>
                {t('save')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

SprintUserSkillDialog.propTypes = {
  allSkills: array,
  isOpen: bool,
  onClose: func,
  listAllSkills: func,
  activeSprint: object,
  addMySkillInSprint: func
};

const mapStateToProps = store => {
  return {
    allSkills: store.skill.allSkills,
    activeSprint: store.sprint.activeSprint
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listAllSkills,
      addMySkillInSprint
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(SprintUserSkillDialog));
