import React, { memo, useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import Switch from 'react-switch';
import Select from 'react-select';
import { connect } from 'react-redux';
import { array, bool, func, object } from 'prop-types';

function UserSkillDialog({ allSkills, isOpen, editMode, onClose, value, onOK }) {
  const [skillId, setSkillId] = useState(0);
  const [evaluationPoint, setEvaluationPoint] = useState(0);
  const [userSkillId, setUserSkillId] = useState(0);
  const [isStandard, setIsStandard] = useState(false);

  const clearAll = () => {
    setSkillId(0);
    setEvaluationPoint(0);
    setUserSkillId(0);
    setIsStandard(false);
  };

  const handleSelectSkill = select => setSkillId(select.value);

  useEffect(() => {
    if (isOpen) {
      setSkillId(value ? value.skillId : 0);
      setEvaluationPoint(value ? value.evaluationPoint : 0);
      setUserSkillId(value ? value.userSkillId : 0);
      setIsStandard(value ? value.isStandard : false);
    }
  }, [isOpen]);

  const onSubmitValue = removeMode => {
    onOK(editMode, removeMode, {
      id: userSkillId,
      skillId,
      evaluationPoint: Number(evaluationPoint),
      isStandard
    });
    clearAll();
  };

  const disableBtn = !skillId || !evaluationPoint;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered backdrop="static">
          <ModalHeader toggle={onClose}>{t(editMode ? 'edit_user_skill' : 'new_user_skill')}</ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">
              {!editMode ? allSkills && (
                <Select
                  options={allSkills.map(s => ({ value: s.id, label: s.name }))}
                  placeholder={t('select_skill')}
                  onChange={handleSelectSkill}
                  className="margin-top-15px"
                />
              ) : (
                <Input
                  defaultValue={value.name}
                  className={'flex-grow-1 margin-top-15px focus-border'}
                  disabled={true}
                  placeholder={t('skill')}
                />
              )}
              <Input
                defaultValue={!editMode ? '' : value.evaluationPoint}
                type="number"
                onChange={e => setEvaluationPoint(e.target.value)}
                className={'flex-grow-1 full margin-top-40px focus-border'}
                autoFocus
                placeholder={t('evaluation_point')}
              />
              <div className="margin-top-30px margin-bottom-15px">
                <Switch
                  width={45}
                  height={20}
                  onColor="#0ba1ee"
                  checked={isStandard}
                  onChange={checked => setIsStandard(checked)}
                  className="align-middle pr-3"
                />
                <span>{t('standard')}</span>
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-between'}>
              <div>
                {editMode && (
                  <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => onSubmitValue(1)}>
                    {t('remove')}
                  </Button>
                )}
              </div>
              <div>
                <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                  {t('cancel')}
                </Button>
                <Button
                  disabled={disableBtn}
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => onSubmitValue(0)}
                >
                  {t(editMode ? 'save' : 'add')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

UserSkillDialog.propTypes = {
  allSkills: array,
  isOpen: bool,
  onClose: func,
  editMode: bool,
  value: object,
  onOK: func
};

const mapStateToProps = store => {
  return {
    allSkills: store.skill.allSkills
  };
};

export default connect(mapStateToProps)(memo(UserSkillDialog));
