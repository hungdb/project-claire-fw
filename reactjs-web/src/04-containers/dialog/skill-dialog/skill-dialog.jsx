import React, { useEffect, useState, memo } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, object, func } from 'prop-types';

function SkillDialog({ isOpen, editMode, onClose, value, onOK }) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [linkIcon, setLinkIcon] = useState('');

  useEffect(() => {
    if (isOpen) {
      setName(value ? value.name : '');
      setDescription(value ? value.description : '');
      setLinkIcon(value ? value.linkIcon : '');
    }
  }, [isOpen]);

  const onSubmitValue = removeMode => {
    onOK(editMode, removeMode, {
      id: editMode && value.id,
      name,
      description,
      linkIcon
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered backdrop="static">
          <ModalHeader toggle={onClose}>{t(editMode ? 'edit_skill' : 'new_skill')}</ModalHeader>
          <ModalBody>
            <div className="content-modal">
              <Input
                defaultValue={t(!editMode ? '' : value.name)}
                onChange={e => setName(e.target.value)}
                className={'flex-grow-1 full focus-border'}
                placeholder={t('name')}
                name="name"
              />
              <Input
                defaultValue={t(!editMode ? '' : value.description)}
                onChange={e => setDescription(e.target.value)}
                className={'flex-grow-1 full margin-top-40px focus-border'}
                placeholder={t('description')}
                name="description"
              />
              <Input
                defaultValue={!editMode ? '' : value.linkIcon}
                onChange={e => setLinkIcon(e.target.value)}
                className={'flex-grow-1 full margin-top-40px focus-border'}
                placeholder={t('link_icon')}
                name="linkIcon"
              />
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-between'}>
              <div>
                {editMode && (
                  <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => onSubmitValue(true)}>
                    {t('remove')}
                  </Button>
                )}
              </div>
              <div>
                <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                  {t('cancel')}
                </Button>
                <Button disabled={!name} className={'base-btn'} size={'sm'} onClick={() => onSubmitValue(false)}>
                  {t(editMode ? 'save' : 'add')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

SkillDialog.propTypes = {
  isOpen: bool,
  editMode: bool,
  value: object,
  onClose: func,
  onOK: func
};

export default memo(SkillDialog);
