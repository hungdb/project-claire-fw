import React, { useState, memo, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func } from 'prop-types';
import ControlDatePicker from '../../../03-components/DateTimePicker/ControlDatePicker';
import { getEnv } from '../../../env';

const KPI_REVIEW_POINT_PERCENT = Number(getEnv('KPI_REVIEW_POINT_PERCENT'));
const KPI_TEST_POINT_PERCENT = Number(getEnv('KPI_TEST_POINT_PERCENT'));

function SkillKPIDialog({ isOpen, onClose, onAddSkillKPI }) {
  const [dateSelect, setDateSelect] = useState(null);
  const [pointSkillReview, setPointSkillReview] = useState(0);
  const [pointSkillTest, setPointSkillTest] = useState(0);

  useEffect(() => {
    if (isOpen) {
      setDateSelect('');
      setPointSkillReview(0);
      setPointSkillTest(0);
    }
  }, [isOpen]);

  const sumUpKPIPoint = () => {
    const result = pointSkillReview * KPI_REVIEW_POINT_PERCENT + pointSkillTest * KPI_TEST_POINT_PERCENT;
    return result && result.toFixed(2);
  };

  const disableBtn = pointSkillReview === 0 || (pointSkillTest === 0 && dateSelect);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered backdrop="static">
          <ModalHeader toggle={onClose}>{t('add_skill_kpi')}</ModalHeader>
          <ModalBody>
            <div className="content-modal pt-1">
              <ControlDatePicker
                isLocalFormat
                value={dateSelect}
                format="DD/MM/YYYY"
                placeholder={t('select_date')}
                onChange={date => setDateSelect(date)}
              />
              <Input
                type="number"
                className={'flex-grow-1 full mt-3 focus-border'}
                placeholder={t('point_skill_review')}
                onChange={e => setPointSkillReview(e.target.value)}
              />
              <Input
                type="number"
                className={'flex-grow-1 full mt-3  focus-border'}
                placeholder={t('point_skill_test')}
                onChange={e => setPointSkillTest(e.target.value)}
              />
              <div className={'mt-5 text-center border'}>
                KPI SKILL = {KPI_REVIEW_POINT_PERCENT * 100}% * {pointSkillReview} + {KPI_TEST_POINT_PERCENT * 100}% *{' '}
                {pointSkillTest} = {sumUpKPIPoint()}
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button
                className={'margin-right-10px outline-base-btn'}
                size={'sm'}
                onClick={() => {
                  onClose && onClose();
                }}
              >
                {t('cancel')}
              </Button>
              <Button
                disabled={!!disableBtn}
                className={'base-btn'}
                size={'sm'}
                onClick={() => {
                  onAddSkillKPI &&
                    onAddSkillKPI(Number(pointSkillReview), Number(pointSkillTest), dateSelect || new Date());
                  onClose && onClose();
                }}
              >
                {t('add')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

SkillKPIDialog.propTypes = {
  isOpen: bool,
  onClose: func,
  onAddSkillKPI: func
};

export default memo(SkillKPIDialog);
