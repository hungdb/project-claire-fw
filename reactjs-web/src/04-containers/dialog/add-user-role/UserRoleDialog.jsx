import { Modal, ModalHeader, ModalBody, ModalFooter, Input, Button } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import React, { memo, useState, useEffect } from 'react';
import { bool, any, func } from 'prop-types';

const UserRoleDialog = ({ isOpen, allRoles, role, onClose, onOK }) => {
  const [roleName, setRoleName] = useState('');
  const isEditMode = !!role;

  useEffect(() => {
    setRoleName(role ? role.name : '');
  }, [role]);

  return (
    <NamespacesConsumer>
      {t => (
        <Modal isOpen={isOpen} centered zIndex="9999" backdrop="static">
          <ModalHeader>{isEditMode ? t('edit_user_role') : t('add_user_role')}</ModalHeader>
          <ModalBody>
            <Input placeholder={t('user_role_name')} value={roleName} onChange={e => setRoleName(e.target.value)} />
          </ModalBody>
          <ModalFooter>
            <div className={'d-flex justify-content-between align-items-center w-100'}>
              <div>
                {isEditMode && (
                  <Button
                    className={'outline-base-remove-btn'}
                    size={'sm'}
                    onClick={() => {
                      onOK && onOK(null);
                      setRoleName('');
                    }}
                  >
                    {t('remove')}
                  </Button>
                )}
              </div>
              <div>
                <Button className={'outline-base-btn margin-right-5px'} size={'sm'} onClick={onClose}>
                  {t('cancel')}
                </Button>
                <Button
                  disabled={!roleName || allRoles.findIndex(r => r.name === roleName) >= 0}
                  className={'base-btn'}
                  size={'sm'}
                  onClick={() => {
                    onOK && onOK(roleName);
                    setRoleName('');
                  }}
                >
                  {isEditMode ? t('save') : t('add')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

UserRoleDialog.propTypes = {
  isOpen: bool,
  allRoles: any,
  role: any,
  onClose: func,
  onOK: func
};

export default memo(UserRoleDialog);
