import React, { useState, Fragment, memo, useEffect } from 'react';
import { Button, FormGroup } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { object, bool, func } from 'prop-types';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';

function ProjectPermissionSchemeDialog({ permissionScheme, isOpen, onClose, onOK }) {
  const [projectPermissionSchemeName, setProjectPermissionSchemeName] = useState('');

  useEffect(() => {
    setProjectPermissionSchemeName(permissionScheme && permissionScheme.name);
  }, [permissionScheme]);

  const _renderContent = (editMode, t) => {
    return (
      <FormGroup className="mb-2">
        <FieldInput
          type="text"
          placeholder={'project_permission_scheme_name'}
          defaultValue={projectPermissionSchemeName}
          onChangeValue={value => setProjectPermissionSchemeName(value)}
        />
      </FormGroup>
    );
  };

  const _renderActionsRight = (editMode, t) => {
    return (
      <Fragment>
        <Button
          disabled={!projectPermissionSchemeName}
          className={'base-btn'}
          size={'sm'}
          onClick={() => {
            onOK && onOK({ permissionSchemeName: projectPermissionSchemeName });
          }}
        >
          {t(editMode ? 'save' : 'add')}
        </Button>
      </Fragment>
    );
  };

  const _renderActionsLeft = (editMode, t) => {
    return (
      <Fragment>
        {editMode && (
          <Button
            className={'outline-base-remove-btn'}
            size={'sm'}
            onClick={() => {
              onOK && onOK({ permissionSchemeName: null });
            }}
          >
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  const editModeScheme = !!permissionScheme;
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          title={t(editModeScheme ? 'edit_project_permission_scheme' : 'new_project_permission_scheme')}
          isOpen={isOpen}
          onClose={onClose}
          dialogContent={_renderContent(editModeScheme, t)}
          actionBtn={_renderActionsRight(editModeScheme, t)}
          actionBtnLeft={_renderActionsLeft(editModeScheme, t)}
        />
      )}
    </NamespacesConsumer>
  );
}

ProjectPermissionSchemeDialog.propTypes = {
  permissionScheme: object,
  isOpen: bool,
  editMode: bool,
  onClose: func,
  onOK: func
};

export default memo(ProjectPermissionSchemeDialog);
