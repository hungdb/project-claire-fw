import React, { memo, useState } from 'react';
import { Button, Row, Col, Table } from 'reactstrap';
import './styles.css';
import { NamespacesConsumer } from 'react-i18next';
import Multicheck from '../../../03-components/Multicheck';
import { object, array, func, bool, number } from 'prop-types';
import { TYPE_ACTION_BEHAVIOR_OBJECT, TYPE_APPLY_BEHAVIOR_OBJECT } from '../../../05-utils/commonData';
import DatePicker from 'react-datepicker';
import { formatDateTime, getDateAt000000ZeroGMT, calculateLength, convertUTCString } from '../../../08-helpers/common';
import BaseDialog from '../../../03-components/Dialog';

const KPIBehaviourDialog = ({
  user,
  itemKPIBehaviour,
  listBehaviour,
  isOpen,
  showMode,
  onClose,
  onCreateKpiBehaviour,
  defaultPoint,
  maxPoint
}) => {
  const [state, setState] = useState({
    hasBorderDate: false,
    date: new Date(),
    arrayPointAddChecked: [],
    arrayPointSubChecked: [],
    pointSub: 0,
    pointAdd: 0
  });

  const { hasBorderDate, date, arrayPointAddChecked, arrayPointSubChecked, pointSub, pointAdd } = state;

  const clearAll = () => {
    setState({
      ...state,
      date: new Date(),
      arrayPointAddChecked: [],
      arrayPointSubChecked: [],
      pointSub: 0,
      pointAdd: 0
    });
  };

  const renderDatePicker = t => {
    return (
      <Row className="mb-4" style={{ marginLeft: '0px' }}>
        <Col xs={12} sm={6} lg={4} className="pl-0">
          <div
            className={`date-picker-input d-flex justify-content-between align-items-center ${
              hasBorderDate ? 'has-border' : ''
            }`}
          >
            <div className={'date-picker-container'}>
              <DatePicker
                value={formatDateTime(date, 'DD/MM/YYYY')}
                className={'date-picker'}
                dropdownMode={'select'}
                placeholderText={t('start_date')}
                isClearable={true}
                selected={date}
                onFocus={() => setState({ ...state, hasBorderDate: true })}
                onBlur={() => setState({ ...state, hasBorderDate: false })}
                onChange={e => setState({ ...state, date: e ? getDateAt000000ZeroGMT(e) : null })}
              />
            </div>
            <i className={'date-picker-icon fa fa-calendar mr-1 ml-2'} aria-hidden="true" />
          </div>
        </Col>
      </Row>
    );
  };

  const renderListBehaviour = t => {
    let arrayBehaviourAdd = [];
    let arrayBehaviourSub = [];
    if (showMode) {
      const logDetailBehaviors = itemKPIBehaviour && itemKPIBehaviour.logDetailBehaviors;
      logDetailBehaviors &&
        // eslint-disable-next-line
        logDetailBehaviors.map(item => {
          if (item && item.behavior && item.behavior.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD) {
            arrayBehaviourAdd = [...arrayBehaviourAdd, item.behavior];
          } else if (item && item.behavior && item.behavior.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB) {
            arrayBehaviourSub = [...arrayBehaviourSub, item.behavior];
          }
        });
    } else {
      const listBehaviourGeneral = listBehaviour.filter(
        b => b.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.GENERAL && b.isApply === true
      );
      // eslint-disable-next-line
      listBehaviourGeneral.map(item => {
        if (item && item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD) {
          arrayBehaviourAdd = [...arrayBehaviourAdd, item];
        } else if (item && item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB) {
          arrayBehaviourSub = [...arrayBehaviourSub, item];
        }
      });
    }
    const handleChecked = (array, id) => {
      if (array) {
        if (array.length === 0) {
          return false;
        } else {
          const index = array.findIndex(a => a === id);
          if (index !== -1) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    };

    const table = (
      <Table className={'table table-bordered table-kpi-behaviour-dialog'}>
        <tbody>
          {calculateLength(arrayBehaviourAdd) ? (
            arrayBehaviourAdd.map((item, index) => (
              <tr key={index}>
                {index === 0 && (
                  <td className={'td-center'} rowSpan={arrayBehaviourAdd.length + 1}>
                    {t('point')} (+)
                  </td>
                )}
                <td>{item.activity}</td>
                <td className={'td-center'}>{item.point}</td>
                {!showMode ? (
                  <td className={'td-center'}>
                    <Multicheck
                      items={[{ checked: handleChecked(arrayPointAddChecked, item.id) }]}
                      onChange={() => {
                        let arrayUpdateAdd = arrayPointAddChecked;
                        let _pointAdd = pointAdd;
                        if (handleChecked(arrayPointAddChecked, item.id)) {
                          const indexUpdate = arrayUpdateAdd.findIndex(a => a === item.id);
                          _pointAdd -= item.point;
                          arrayUpdateAdd = [
                            ...arrayUpdateAdd.slice(0, indexUpdate),
                            ...arrayUpdateAdd.slice(indexUpdate + 1)
                          ];
                        } else {
                          _pointAdd += item.point;
                          arrayUpdateAdd = [...arrayUpdateAdd, item.id];
                        }
                        setState({ ...state, arrayPointAddChecked: arrayUpdateAdd, pointAdd: _pointAdd });
                      }}
                      styleCheckbox={{ margin: 'auto' }}
                    />
                  </td>
                ) : null}
              </tr>
            ))
          ) : (
            <tr>
              <td className={'td-center'} rowSpan={2}>
                {t('point')} (+)
              </td>
              <td colSpan={3} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
          {showMode && (
            <tr>
              <td>
                <strong>{t('total')}</strong>
              </td>
              <td className={'td-center'}>
                <strong>{itemKPIBehaviour.totalPointAdd}</strong>
              </td>
            </tr>
          )}
        </tbody>
        <tbody>
          {calculateLength(arrayBehaviourSub) ? (
            arrayBehaviourSub.map((item, index) => (
              <tr key={index}>
                {index === 0 && (
                  <td className={'td-center'} rowSpan={arrayBehaviourSub.length + 1}>
                    {t('point')} (-)
                  </td>
                )}
                <td>{item.activity}</td>
                <td className={'td-center'}>{item.point}</td>
                {!showMode ? (
                  <td className={'td-center'}>
                    <Multicheck
                      items={[{ checked: handleChecked(arrayPointSubChecked, item.id) }]}
                      onChange={() => {
                        let _pointSub = pointSub;
                        let arrayUpdateSub = arrayPointSubChecked;
                        if (handleChecked(arrayPointSubChecked, item.id)) {
                          _pointSub -= item.point;
                          const indexUpdate = arrayUpdateSub.findIndex(a => a === item.id);
                          arrayUpdateSub = [
                            ...arrayUpdateSub.slice(0, indexUpdate),
                            ...arrayUpdateSub.slice(indexUpdate + 1)
                          ];
                        } else {
                          _pointSub += item.point;
                          arrayUpdateSub = [...arrayUpdateSub, item.id];
                        }
                        setState({ ...state, arrayPointSubChecked: arrayUpdateSub, pointSub: _pointSub });
                      }}
                      styleCheckbox={{ margin: 'auto' }}
                    />
                  </td>
                ) : null}
              </tr>
            ))
          ) : (
            <tr>
              <td rowSpan={2} className={'td-center'}>
                {t('point')} (-)
              </td>
              <td colSpan={3} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
          {showMode && (
            <tr>
              <td>
                <strong>{t('total')}</strong>
              </td>
              <td className={'td-center'}>
                <strong>{itemKPIBehaviour.totalPointSub}</strong>
              </td>
            </tr>
          )}
        </tbody>
        {showMode && (
          <tbody>
            <tr>
              <td className={'td-center'} rowSpan={arrayBehaviourSub.length}>
                <strong>{t('total')}</strong>
              </td>
              <td>
                <strong>DEFAULT_BEHAVIOR_POINT + Total_Point(+) - Total_Point(-)</strong>
              </td>
              <td className={'td-center'}>
                <strong className={'color-base'}>{itemKPIBehaviour.point}</strong>
              </td>
            </tr>
          </tbody>
        )}
      </Table>
    );
    return <div className="table-responsive">{table}</div>;
  };

  const renderContentDialog = t => {
    return (
      <div className="content-modal d-flex flex-column align-items-stretch">
        {!showMode && renderDatePicker(t)}
        {renderListBehaviour(t)}
        {showMode && (
          <>
            <p>{`* DEFAULT_BEHAVIOR_POINT = ${defaultPoint} (${t('points')})`}</p>
            <p className="mb-0">{`* ${t('MAX')}: ${maxPoint} (${t('points')})`}</p>
          </>
        )}
      </div>
    );
  };

  // const renderSummaryPoint = t => {
  //   const summaryPoint = pointAdd - pointSub;
  //   return (
  //     <div className={'mt-2 mr-5 ml-5 text-center border font-weight-bold'}>
  //       {t('behaviour_management.KPI_point')} = {pointAdd} - {pointSub} = {summaryPoint}P
  //     </div>
  //   );
  // };

  const handleButtonOk = () => {
    if (user && listBehaviour) {
      const arrayIdBehaviourSelected = [...arrayPointAddChecked, ...arrayPointSubChecked];
      const itemKpiBehaviour = {
        userId: user.id || user.value,
        behaviors: arrayIdBehaviourSelected,
        evaluatedAt: convertUTCString(getDateAt000000ZeroGMT(date))
      };
      onCreateKpiBehaviour && onCreateKpiBehaviour(itemKpiBehaviour);
    } else {
      onClose && onClose();
    }
    clearAll();
  };

  const renderButtonDialog = t => {
    return (
      <>
        {!showMode && (
          <Button
            disabled={state.arrayPointAddChecked.length === 0 && state.arrayPointSubChecked.length === 0}
            className={'base-btn'}
            size={'sm'}
            onClick={() => {
              handleButtonOk();
            }}
          >
            {t('add')}
          </Button>
        )}
      </>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          dialogWidth="xl"
          isOpen={isOpen}
          onClose={onClose}
          dialogContent={renderContentDialog(t)}
          actionBtn={renderButtonDialog(t)}
          title={`${
            showMode ? t('behaviour_management.show_log_kpi') : t('behaviour_management.add_behaviour_KPI')
          } : ${(user && user.name) || user.label} `}
        />
      )}
    </NamespacesConsumer>
  );
};

KPIBehaviourDialog.propTypes = {
  user: object,
  itemKPIBehaviour: object,
  listBehaviour: array,
  isOpen: bool,
  showMode: bool,
  onClose: func,
  onCreateKpiBehaviour: func,
  defaultPoint: number,
  maxPoint: number
};

export default memo(KPIBehaviourDialog);
