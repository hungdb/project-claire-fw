import React, { memo, useEffect, useState } from 'react';
import { Button, FormGroup, Label } from 'reactstrap';
import {
  ARRAY_TYPE_APPLY as arrayTypeApply,
  ARRAY_TYPE_ACTION as arrayTypeAction,
  TYPE_SUB_APPLY as arrayTypeSubApply
} from '../../../05-utils/commonData';
import { NamespacesConsumer } from 'react-i18next';
import { bool, object, func, array } from 'prop-types';
import { isAllowPermission, calculateLength } from '../../../08-helpers/common';
import { PERM_REMOVE_BEHAVIOR } from '../../../07-constants/app-permission';
import BaseDialog from '../../../03-components/Dialog';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import FieldInput from '../../../03-components/FieldInput';
import ControlDatePicker from '../../../03-components/DateTimePicker';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import './style.css';

const BehaviourDialog = ({ editMode, itemBehaviour, isOpen, onClose, onRemove, onSave, onCreate, appPermissions }) => {
  const [state, setState] = useState({
    activity: '',
    point: '',
    typeApply: null,
    typeSubApply: null,
    typeAction: null,
    isApply: '',
    startDate: new Date(),
    endDate: null,
    description: ''
  });

  const { activity, point, typeApply, typeSubApply, typeAction, startDate, endDate, description } = state;

  // eslint-disable-next-line
  // componentWillReceiveProps(nextProps, nextContext) {
  //   if (editMode) {
  //     const typeApply =
  //       itemBehaviour && arrayTypeApply.find(item => item.value === itemBehaviour.typeApply);
  //     const typeAction =
  //       itemBehaviour && arrayTypeAction.find(item => item.value === itemBehaviour.typeAction);
  //     const typeSubApply =
  //       itemBehaviour && arrayTypeSubApply.find(item => item.value === itemBehaviour.typeSubApply);
  //     setState({ ...state,
  //       typeApply,
  //       typeAction,
  //       point: itemBehaviour && itemBehaviour.point,
  //       description: itemBehaviour && itemBehaviour.description,
  //       activity: itemBehaviour && itemBehaviour.activity,
  //       isApply: itemBehaviour && itemBehaviour.isApply,
  //       typeSubApply,
  //       startDate: itemBehaviour && itemBehaviour.startDate,
  //       endDate: itemBehaviour && itemBehaviour.endDate
  //     });
  //   } else {
  //     setState({ ...state,
  //       activity: '',
  //       point: '',
  //       description: null,
  //       typeApply: null,
  //       typeSubApply: null,
  //       typeAction: null,
  //       isApply: '',
  //       startDate: new Date(),
  //       endDate: null
  //     });
  //   }
  // }

  const clearAll = () => {
    setState({
      activity: '',
      point: '',
      description: '',
      typeApply: null,
      typeAction: null,
      isApply: '',
      typeSubApply: null,
      startDate: new Date(),
      endDate: null
    });
  };

  useEffect(() => {
    return () => {
      clearAll();
    };
  }, []);

  useEffect(() => {
    const typeApply = itemBehaviour && arrayTypeApply.find(item => item.value === itemBehaviour.typeApply);
    const typeAction = itemBehaviour && arrayTypeAction.find(item => item.value === itemBehaviour.typeAction);
    const typeSubApply = itemBehaviour && arrayTypeSubApply.find(item => item.value === itemBehaviour.typeSubApply);
    setState({
      typeApply: typeApply || null,
      typeAction: typeAction || null,
      point: editMode ? itemBehaviour.point : '',
      description: editMode ? itemBehaviour.description : null,
      activity: editMode ? itemBehaviour.activity : '',
      isApply: editMode ? itemBehaviour.isApply : '',
      typeSubApply: typeSubApply || null,
      startDate: editMode ? itemBehaviour.startDate : '',
      endDate: editMode ? itemBehaviour.endDate : ''
    });
  }, [editMode]);

  const getSelectOptions = data => {
    return calculateLength(data)
      ? data.map(item => {
          return { value: item.value, label: item.label.toLocaleUpperCase() };
        })
      : [];
  };

  const onChangeStartDate = e => {
    setState({ ...state, startDate: e });
  };

  const onChangeEndDate = e => {
    setState({ ...state, endDate: e });
  };

  const renderContentGeneral = t => {
    return (
      <>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.start_date')}</Label>
          <ControlDatePicker
            value={startDate}
            format="DD/MM/YYYY HH:mm"
            isLocalFormat={1}
            showTimeSelect={true}
            placeholder={t('behaviour_management.start_date')}
            onChange={onChangeStartDate}
          />
        </FormGroup>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.end_date')}</Label>
          <ControlDatePicker
            value={endDate}
            format="DD/MM/YYYY HH:mm"
            isLocalFormat={1}
            showTimeSelect={true}
            placeholder={t('behaviour_management.end_date')}
            onChange={onChangeEndDate}
          />
        </FormGroup>
        <FormGroup className="mb-4">
          <MarkdownEditor
            label={t('behaviour_management.description')}
            value={description}
            onChange={content => setState({ ...state, description: content })}
            className={'mark-down-editor'}
          />
        </FormGroup>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.type_sub_apply')}</Label>
          <SelectPropsBox
            placeholder="behaviour_management.select_type_sub_apply"
            value={typeSubApply && typeSubApply.value}
            options={arrayTypeSubApply}
            onChange={typeSubApply => setState({ ...state, typeSubApply })}
          />
        </FormGroup>
      </>
    );
  };

  const renderContentDialog = t => {
    const typeApplyOptions = getSelectOptions(arrayTypeApply);
    const typeActionsOptions = getSelectOptions(arrayTypeAction);
    return (
      <>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.activity')}</Label>
          <FieldInput
            type="text"
            defaultValue={activity}
            onChangeValue={value => setState({ ...state, activity: value })}
          />
        </FormGroup>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.point')}</Label>
          <FieldInput
            type="number"
            defaultValue={point}
            onChangeValue={value => setState({ ...state, point: value })}
          />
        </FormGroup>
        <FormGroup className="mb-4">
          <Label>{t('behaviour_management.type_apply')}</Label>
          <SelectPropsBox
            placeholder="behaviour_management.select_type_apply"
            value={typeApply && typeApply.value}
            options={typeApplyOptions}
            onChange={typeApply => setState({ ...state, typeApply })}
          />
        </FormGroup>
        {typeApply && typeApply.label === arrayTypeApply[0].label && renderContentGeneral(t)}
        <FormGroup>
          <Label>{t('behaviour_management.type_action')}</Label>
          <SelectPropsBox
            placeholder="behaviour_management.select_type_action"
            value={typeAction && typeAction.value}
            options={typeActionsOptions}
            onChange={typeAction => setState({ ...state, typeAction })}
          />
        </FormGroup>
      </>
    );
  };

  const handleButtonRemove = () => {
    onRemove && onRemove(itemBehaviour);
    clearAll();
  };

  const handleButtonOk = () => {
    const checkPoint = isNaN(point);
    if (editMode) {
      if ((!activity && !point && !typeApply && !typeAction) || checkPoint) {
        onSave && onSave();
      } else {
        const behaviourUpdate = {
          behaviorId: itemBehaviour.id,
          activity: activity,
          point: Number(point),
          typeApply: typeApply && Number(typeApply.value),
          typeAction: typeAction && Number(typeAction.value),
          isApply: itemBehaviour.isApply,
          description: description,
          typeSubApply: typeApply && typeApply.label === arrayTypeApply[0].label ? Number(typeSubApply.value) : null,
          startDate: (typeApply && typeApply.label === arrayTypeApply[0].label && startDate) || null,
          endDate: (typeApply && typeApply.label === arrayTypeApply[0].label && endDate) || null
        };
        onSave && onSave(behaviourUpdate);
      }
      clearAll();
    } else {
      if (activity && point && !checkPoint) {
        const newBehaviour = {
          activity,
          point: Number(point),
          typeApply: typeApply && Number(typeApply.value),
          typeAction: typeAction && Number(typeAction.value),
          isApply: false,
          description: description,
          typeSubApply: typeApply && typeApply.label === arrayTypeApply[0].label ? Number(typeSubApply.value) : null,
          startDate: (typeApply && typeApply.label === arrayTypeApply[0].label && startDate) || null,
          endDate: (typeApply && typeApply.label === arrayTypeApply[0].label && endDate) || null
        };
        onCreate && onCreate(newBehaviour);
        clearAll();
      }
    }
  };

  const renderButtonDialog = (t, editMode) => {
    return (
      <>
        {editMode && isAllowPermission(appPermissions, PERM_REMOVE_BEHAVIOR) && (
          <Button
            className={'outline-base-remove-btn'}
            size={'sm'}
            onClick={() => {
              handleButtonRemove();
              clearAll();
            }}
          >
            {t('remove')}
          </Button>
        )}
        <Button
          disabled={
            !activity ||
            !point ||
            isNaN(point) ||
            (typeApply && typeApply.label === arrayTypeApply[0].label && !typeSubApply)
          }
          className={'base-btn'}
          size={'sm'}
          onClick={() => {
            handleButtonOk();
          }}
        >
          {t(editMode ? 'save' : 'add')}
        </Button>
      </>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          dialogWidth={'lg'}
          isOpen={isOpen}
          onClose={onClose}
          title={editMode ? t('behaviour_management.edit_behaviour') : t('behaviour_management.new_behaviour')}
          dialogContent={renderContentDialog(t)}
          actionBtn={renderButtonDialog(t, editMode)}
          classes={'behavior-dialog'}
          backdrop={'static'}
        />
      )}
    </NamespacesConsumer>
  );
};

BehaviourDialog.propTypes = {
  editMode: bool,
  itemBehaviour: object,
  isOpen: bool,
  onClose: func,
  onRemove: func,
  onSave: func,
  onCreate: func,
  appPermissions: array
};

export default memo(BehaviourDialog);
