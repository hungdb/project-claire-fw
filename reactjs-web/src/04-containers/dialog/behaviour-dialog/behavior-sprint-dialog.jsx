import React, { memo, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';
import { getUsersByProject, getAllBehaviors, logBehaviorSprint } from '../../../01-actions/sprint';
import LazyComponent from '../../../03-components/LazyComponent';
import { TYPE_APPLY_BEHAVIOR_OBJECT } from '../../../05-utils/commonData';
import { removeDuplicateObjInArray, calculateLength } from '../../../08-helpers/common';
import './style.css';

const BehaviorTableBody = LazyComponent(() => import('./table-body'));
const TableHead = LazyComponent(() => import('../../../03-components/TableHead'));
const SelectOptionBox = LazyComponent(() => import('../../../03-components/SelectStateBox'));

const fields = ['type', 'behaviour_management.behavior', 'behaviour_management.point', 'action'];
const classes = [
  'align-middle text-center',
  'align-middle text-center',
  'align-middle text-center w-8p',
  'align-middle text-center w-5p'
];
const BehaviorSprintDialog = ({
  isOpen,
  onClose,
  usersIntoProject,
  projectId,
  sprintId,
  allBehaviorsInSprint,
  getUsersByProject,
  getAllBehaviors,
  logBehaviorSprint
}) => {
  const [state, setState] = useState({
    userId: 0,
    responsibilityPoint: {},
    dedicationPoint: {},
    responsibilityListId: [],
    dedicationListId: []
  });

  const { userId, responsibilityPoint, dedicationPoint, responsibilityListId, dedicationListId } = state;

  useEffect(() => {
    getUsersByProject({ projectId: projectId });
    getAllBehaviors();
  }, []);

  const onMergeListId = () => {
    return [...responsibilityListId, ...dedicationListId];
  };
  useEffect(() => {
    onMergeListId();
  }, [responsibilityListId, dedicationListId]);

  const handleSuggestUsers = usersIntoProject => {
    return calculateLength(usersIntoProject)
      ? removeDuplicateObjInArray({ list: usersIntoProject, key: 'userId' }).map(u => {
          return {
            id: u.User && u.User.id,
            name: u.User && u.User.name
          };
        })
      : [];
  };

  const responsibility =
    allBehaviorsInSprint &&
    allBehaviorsInSprint.filter(
      item => item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY && item.isApply
    );
  const dedication =
    allBehaviorsInSprint &&
    allBehaviorsInSprint.filter(
      item => item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_DEDICATION && item.isApply
    );

  const onChangeUser = user => setState({ ...state, userId: user && user.id });
  const onGetListDedicationId = dedicationListId => setState({ ...state, dedicationListId });
  const onGetListResponsibilityId = responsibilityListId => setState({ ...state, responsibilityListId });
  const onCalculateDedicationPoint = (plusPoint, minusPoint) =>
    setState({ ...state, dedicationPoint: { plusPoint, minusPoint } });
  const onCalculateResponsibilityPoint = (plusPoint, minusPoint) =>
    setState({ responsibilityPoint: { plusPoint, minusPoint } });

  const onAddBehaviourPoint = () => {
    logBehaviorSprint(userId, sprintId, onMergeListId());
    onClose && onClose();
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered={true} size="xl" backdrop="static">
          <ModalHeader toggle={onClose}>{t('behaviour_management.log_behavior_in_sprint')}</ModalHeader>
          <ModalBody>
            <Row>
              <Col sm="6" md="5">
                <SelectOptionBox
                  options={handleSuggestUsers(usersIntoProject)}
                  onChange={onChangeUser}
                  keyLabel={'name'}
                  keyValue={'id'}
                  placeholder={t('select_user')}
                />
              </Col>
            </Row>
            <div className="content-modal pt-4">
              <div className="table-responsive">
                <table className="table table-bordered behavior-table">
                  <thead>
                    <tr>
                      <TableHead fields={fields} t={t} classes={classes} />
                    </tr>
                  </thead>
                  <BehaviorTableBody
                    data={dedication}
                    title={t('behaviour_management.dedication')}
                    getListId={onGetListDedicationId}
                    calculatePoint={onCalculateDedicationPoint}
                  />
                  <BehaviorTableBody
                    data={responsibility}
                    title={t('behaviour_management.responsibility')}
                    getListId={onGetListResponsibilityId}
                    calculatePoint={onCalculateResponsibilityPoint}
                  />
                </table>
              </div>
              <div className="mt-4 text-center border font-weight-bold">
                <p>
                  {t('behaviour_management.point_dedication')} ={' '}
                  {dedicationPoint.plusPoint + dedicationPoint.minusPoint || 0}
                </p>
                <p className="mb-0">
                  {t('behaviour_management.point_responsibility')} ={' '}
                  {responsibilityPoint.plusPoint + responsibilityPoint.minusPoint || 0}
                </p>
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={() => onClose && onClose()}>
                {t('cancel')}
              </Button>
              <Button className={'base-btn'} size={'sm'} onClick={onAddBehaviourPoint} disabled={!userId}>
                {t('add')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

BehaviorSprintDialog.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  usersIntoProject: PropTypes.array,
  projectId: PropTypes.number,
  sprintId: PropTypes.number,
  allBehaviorsInSprint: PropTypes.array,
  getUsersByProject: PropTypes.func,
  getAllBehaviors: PropTypes.func,
  logBehaviorSprint: PropTypes.func
};

const mapStateToProps = store => {
  return {
    usersIntoProject: store.sprint.usersIntoProject,
    allBehaviorsInSprint: store.sprint.allBehaviorsInSprint
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsersByProject,
      getAllBehaviors,
      logBehaviorSprint
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(BehaviorSprintDialog));
