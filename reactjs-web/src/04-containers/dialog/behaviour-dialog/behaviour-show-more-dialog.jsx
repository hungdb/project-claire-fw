import React, { memo } from 'react';
import Markdown from 'react-markdown';
import { NamespacesConsumer } from 'react-i18next';
import './style.css';
import BaseDialog from '../../../03-components/Dialog';
import { bool, func, object } from 'prop-types';
import { Table } from 'reactstrap';
import { formatDateTime } from '../../../08-helpers/common';
import { TYPE_ACTION_BEHAVIOR_OBJECT, TYPE_APPLY_BEHAVIOR_OBJECT, TYPE_SUB_APPLY } from '../../../05-utils/commonData';
import Multicheck from '../../../03-components/Multicheck';

const BehaviourShowMoreDialog = ({ isOpen, onClose, itemBehaviour }) => {
  const renderBodyBehaviour = t => {
    // const id = itemBehaviour.id || '';
    const activity = itemBehaviour.activity || '';
    const point = itemBehaviour.point || '';
    const typeApply =
      itemBehaviour.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.GENERAL
        ? t('behaviour_management.general')
        : itemBehaviour.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY
        ? t('behaviour_management.sprint_responsibility')
        : t('behaviour_management.sprint_dedication');
    const typeAction =
      itemBehaviour.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD
        ? t('behaviour_management.add')
        : t('behaviour_management.sub');
    const isApply = itemBehaviour.isApply;
    const createdAt = formatDateTime(itemBehaviour.createdAt, 'LLLL', 1);
    const startDate = (itemBehaviour.startDate && formatDateTime(itemBehaviour.startDate, 'LLLL', 1)) || t('not_found');
    const endDate = (itemBehaviour.endDate && formatDateTime(itemBehaviour.endDate, 'LLLL', 1)) || t('not_found');
    const description = itemBehaviour.description || '';
    const typeSubApply =
      itemBehaviour.typeSubApply && TYPE_SUB_APPLY.find(item => item.value === itemBehaviour.typeSubApply);
    return (
      <tbody>
        <tr>
          <td className={'align-middle text-center w-10p'}> {t('behaviour_management.activity')} </td>
          <td className={'align-middle text-center w-40p '}> {activity} </td>
        </tr>
        <tr>
          <td className={'align-middle text-center w-10p'}> {t('behaviour_management.point')} </td>
          <td className={'align-middle text-center w-40p '}> {point} </td>
        </tr>
        <tr>
          <td className={'align-middle text-center w-10pr'}>{t('behaviour_management.is_apply')} </td>
          <td className={'align-middle text-center w-40p'}>
            <Multicheck disable={true} items={[{ checked: isApply }]} styleCheckbox={{ margin: 'auto' }} />
          </td>
        </tr>
        <tr>
          <td className={'align-middle text-center w-10p'}> {t('behaviour_management.type_apply')} </td>
          <td className={'align-middle text-center w-40p'}> {typeApply && typeApply.toUpperCase()} </td>
        </tr>
        <tr>
          <td className={'align-middle text-center w-10p'}>{t('behaviour_management.type_action')} </td>
          <td className={'align-middle text-center w-40p'}> {typeAction && typeAction.toUpperCase()} </td>
        </tr>
        {itemBehaviour.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.GENERAL && (
          <>
            <tr>
              <td className={'align-middle text-center w-10p'}> {t('behaviour_management.start_date')} </td>
              <td className={'align-middle text-center w-40p'}> {startDate} </td>
            </tr>
            <tr>
              <td className={'align-middle text-center w-10p'}> {t('behaviour_management.end_date')} </td>
              <td className={'align-middle text-center w-40p'}> {endDate} </td>
            </tr>
            <tr>
              <td className={'align-middle text-center w-10p'}> {t('behaviour_management.description')} </td>
              <td className={'align-middle text-center w-40p'}>
                <Markdown source={description} className={'markdown-content'} />
              </td>
            </tr>
            <tr>
              <td className={'align-middle text-center w-10p'}> {t('behaviour_management.type_sub_apply')} </td>
              <td className={'align-middle text-center w-40p'}>
                {' '}
                {(typeSubApply && typeSubApply.label) || t('not_found')}{' '}
              </td>
            </tr>
          </>
        )}
        <tr>
          <td className={'align-middle text-center w-10p'}> {t('behaviour_management.create_at')} </td>
          <td className={'align-middle text-center w-40p'}> {createdAt} </td>
        </tr>
      </tbody>
    );
  };

  const renderContentDialog = t => {
    return (
      <Table bordered responsive striped hover>
        {itemBehaviour ? (
          renderBodyBehaviour(t)
        ) : (
          <tr>
            <td colSpan={10} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </Table>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          title={'Behaviour-#' + (itemBehaviour && itemBehaviour.code)}
          isOpen={isOpen}
          dialogWidth={'lg'}
          onClose={onClose}
          dialogContent={itemBehaviour && renderContentDialog(t)}
          labelBtnCancel={t('OK')}
          classes={'behavior-show-more-dialog'}
        />
      )}
    </NamespacesConsumer>
  );
};

BehaviourShowMoreDialog.propTypes = {
  isOpen: bool.isRequired,
  onClose: func,
  itemBehaviour: object.isRequired
};

export default memo(BehaviourShowMoreDialog);
