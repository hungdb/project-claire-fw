import React, { memo } from 'react';
import { Button, Table } from 'reactstrap';
import './styles.css';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { NamespacesConsumer } from 'react-i18next';
import { array, func, bool, string } from 'prop-types';
import { formatDateTime, calculateLength, isAllowPermission } from '../../../08-helpers/common';
import styles from '../../_pages/Home/AdminPanel/Behaviours/stylesBehaviour';
import AppText from '../../../03-components/AppText';
import TableHead from '../../../03-components/TableHead';
import { PERM_REMOVE_KPI_BEHAVIOR } from '../../../07-constants/app-permission';

const KPIBehaviourInMonthDialog = ({ isOpen, onClose, onRemove, listKpiBehaviour, evaluatedAt, appPermissions }) => {
  const renderButtonDialog = t => {
    return (
      <DialogActions>
        <div className={'flex-grow-1 d-flex justify-content-end'}>
          <div style={styles.viewButton}>
            <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
              {t('cancel')}
            </Button>
          </div>
        </div>
      </DialogActions>
    );
  };

  const renderHeaderKPIBehaviour = t => {
    const fields = [
      'no',
      'behaviour_management.KPI_point',
      'actor',
      'user',
      'behaviour_management.evaluated_at',
      'behaviour_management.action'
    ];
    const classes = [
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center'
    ];
    return (
      <thead>
        <tr>
          <TableHead fields={fields} t={t} classes={classes} />
        </tr>
      </thead>
    );
  };

  const renderBodyKPIBehaviour = t => {
    return (
      <tbody>
        {calculateLength(listKpiBehaviour) ? (
          listKpiBehaviour.map((item, index) => {
            if (item) {
              const number = index + 1;
              const point = item.point || 0;
              const actor = item.infoActor && item.infoActor.name;
              const user = item.user && item.user.name;
              const evaluatedAt = formatDateTime(item.evaluatedAt, 'LLLL');
              return (
                <tr key={item.id}>
                  <td className={'align-middle text-center'}> {number} </td>
                  <td className={'align-middle text-center'}>
                    <strong className={'color-base'}>{point}</strong>
                  </td>
                  <td className={'align-middle text-center'}> {actor} </td>
                  <td className={'align-middle text-center'}> {user} </td>
                  <td className={'align-middle text-center'}> {evaluatedAt} </td>
                  <td className={'align-middle text-center'}>
                    {isAllowPermission(appPermissions, PERM_REMOVE_KPI_BEHAVIOR) && (
                      <span title={t('remove_kpi')} className={'cursor-pointer'} onClick={() => onRemove(item.id)}>
                        <i className="fa fa-trash text-danger" />
                      </span>
                    )}
                  </td>
                </tr>
              );
            }
            return '';
          })
        ) : (
          <tr>
            <td colSpan={10} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Dialog maxWidth="lg" fullWidth open={isOpen} onClose={onClose}>
          <div style={{ width: 'auto', height: 'auto' }}>
            <DialogTitle>
              <AppText type={'title-header'}>
                {`${t('behaviour_management.show_kpi_behaviors_in_month')}: ${evaluatedAt}`}
              </AppText>
            </DialogTitle>
            <DialogContent>
              <Table bordered responsive striped hover>
                {renderHeaderKPIBehaviour(t)}
                {renderBodyKPIBehaviour(t)}
              </Table>
            </DialogContent>
            {renderButtonDialog(t)}
          </div>
        </Dialog>
      )}
    </NamespacesConsumer>
  );
};

KPIBehaviourInMonthDialog.propTypes = {
  isOpen: bool,
  onClose: func,
  onRemove: func,
  listKpiBehaviour: array,
  evaluatedAt: string,
  appPermissions: array
};

export default memo(KPIBehaviourInMonthDialog);
