import React, { PureComponent, Fragment } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import Checkbox from '@material-ui/core/Checkbox';
import { array, func, string } from 'prop-types';
import { TYPE_APPLY_BEHAVIOR_OBJECT, TYPE_ACTION_BEHAVIOR_OBJECT } from '../../../05-utils/commonData';
import { calculateLength } from '../../../08-helpers/common';

class BehaviorTableBody extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      listOption: []
    };
  }

  static propTypes = {
    data: array,
    calculatePoint: func,
    getListId: func,
    title: string
  };

  sumPointByCheckedAndType = (data, typePoint) => {
    return calculateLength(data)
      ? data
          .filter(item => {
            if (typePoint === TYPE_ACTION_BEHAVIOR_OBJECT.SUB) {
              return item.checked && item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB;
            } else {
              return item.checked && item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD;
            }
          })
          .map(item => item.point)
          .reduce((a, b) => a + b, 0)
      : 0;
  };

  getListIdByChecked = data => {
    const checkedItem =
      calculateLength(data) &&
      // eslint-disable-next-line
      data.map(item => {
        if (item.checked) {
          return item.id;
        }
      });
    const result = checkedItem && checkedItem.filter(item => item);
    return result || [];
  };

  getCheckBoxValue = () => {
    let { plusPoint, minusPoint } = 0;
    const { calculatePoint, getListId } = this.props;
    const { listOption } = this.state;

    // on get minus and plus point
    plusPoint = this.sumPointByCheckedAndType(listOption, TYPE_ACTION_BEHAVIOR_OBJECT.ADD);
    minusPoint = this.sumPointByCheckedAndType(listOption, TYPE_ACTION_BEHAVIOR_OBJECT.SUB);
    // pass minus and plus point to parent component
    calculatePoint(plusPoint, minusPoint);

    // on get array id and pass to parent to call API
    const arrayId = this.getListIdByChecked(listOption);
    getListId(arrayId);
  };

  onChangeCheckBoxValue = (e, id, point, typeAction) => {
    const { listOption } = this.state;
    const indexItem = listOption.findIndex(item => item.id === id);
    indexItem !== -1
      ? this.setState(
          {
            listOption: [
              ...listOption.slice(0, indexItem),
              {
                ...listOption[indexItem],
                checked: e.target.checked
              },
              ...listOption.slice(indexItem + 1)
            ]
          },
          () => {
            this.getCheckBoxValue();
          }
        )
      : this.setState(
          {
            listOption: [
              ...listOption,
              {
                id,
                point: typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB ? -point : point,
                typeAction,
                checked: e.target.checked
              }
            ]
          },
          () => {
            this.getCheckBoxValue();
          }
        );
  };

  _renderBehaviorItemValue = item => {
    return (
      <Fragment>
        <td>{item.activity}</td>
        {/*<td>{ item.decider }</td>*/}
        <td className="text-center">
          {item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB ? '-' : '+'} {item.point}
        </td>
        <td className="text-center">
          <Checkbox
            onChange={e => {
              this.onChangeCheckBoxValue(e, item.id, item.point, item.typeAction);
            }}
            color="primary"
          />
        </td>
      </Fragment>
    );
  };

  _renderAllBehaviors = (behaviorList, t) => {
    return behaviorList.map((item, index) => (
      <tr key={item.id}>
        {index === 0 ? (
          <Fragment>
            <td rowSpan={calculateLength(behaviorList)} className="align-middle font-weight-bold">
              {item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY
                ? t('behaviour_management.responsibility')
                : t('behaviour_management.dedication')}
            </td>
            {this._renderBehaviorItemValue(item)}
          </Fragment>
        ) : (
          this._renderBehaviorItemValue(item)
        )}
      </tr>
    ));
  };

  withoutBehaviorsData = (title, t) => {
    return (
      <tr>
        <td className="font-weight-bold">{title}</td>
        <td className="text-center" colSpan={4}>
          {t('not_found')}
        </td>
      </tr>
    );
  };

  render() {
    const { data, title } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <tbody>
            {calculateLength(data) ? this._renderAllBehaviors(data, t) : this.withoutBehaviorsData(title, t)}
          </tbody>
        )}
      </NamespacesConsumer>
    );
  }
}

export default BehaviorTableBody;
