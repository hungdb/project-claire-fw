import React, { PureComponent } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, object, func, array } from 'prop-types';
import { PERM_REMOVE_SALE_STATUS } from '../../../07-constants/app-permission';
import { isAllowPermission } from '../../../08-helpers/common';

class SaleStatusDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };
  }

  static propTypes = {
    isOpen: bool,
    editMode: bool,
    value: object,
    onClose: func,
    onOK: func,
    appPermissions: array
  };

  componentDidUpdate(prevProps) {
    const { isOpen: prevIsOpen } = prevProps;
    const { isOpen } = this.props;
    if (!prevIsOpen && isOpen) {
      const { value } = this.props;
      this.setState({
        name: value ? value.name : ''
      });
    }
  }

  onChangeField = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmitValue = removeMode => {
    const { onOK, editMode, value } = this.props;
    const { name } = this.state;
    onOK(editMode, removeMode, {
      id: editMode && value.id,
      name
    });
  };

  render() {
    const { isOpen, editMode, onClose, value, appPermissions } = this.props;
    const { name } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal isOpen={isOpen} toggle={onClose} centered backdrop="static">
            <ModalHeader toggle={onClose}>{t(editMode ? 'edit_sale_status' : 'new_sale_status')}</ModalHeader>
            <ModalBody>
              <div className="content-modal">
                <Input
                  defaultValue={editMode ? value && value.name : ''}
                  onChange={this.onChangeField}
                  className={'flex-grow-1 full'}
                  placeholder={t('name')}
                  name="name"
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div>
                  {editMode && isAllowPermission(appPermissions, PERM_REMOVE_SALE_STATUS) && (
                    <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => this.onSubmitValue(true)}>
                      {t('remove')}
                    </Button>
                  )}
                </div>
                <div>
                  <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                    {t('cancel')}
                  </Button>
                  <Button disabled={!name} className={'base-btn'} size={'sm'} onClick={() => this.onSubmitValue(false)}>
                    {t(editMode ? 'save' : 'add')}
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default SaleStatusDialog;
