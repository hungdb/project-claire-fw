import React, { PureComponent, Fragment } from 'react';
import { bool, func, object, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { FormGroup, Label, Button } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';
import { PERM_REMOVE_SALE_PRIORITY } from '../../../07-constants/app-permission';
import { isAllowPermission } from '../../../08-helpers/common';

export class SalePriorityDialog extends PureComponent {
  static propTypes = {
    isOpen: bool,
    editMode: bool,
    onClose: func,
    onOK: func,
    onRemove: func,
    value: object,
    appPermissions: array
  };

  state = {
    priorityName: '',
    priorityLevel: ''
  };

  // eslint-disable-next-line
  componentWillReceiveProps = nextProps => {
    if (nextProps.editMode) {
      this.setState({
        priorityName: nextProps.value && nextProps.value.name,
        priorityLevel: nextProps.value && nextProps.value.ordinalNumber
      });
    } else {
      this.setState({
        priorityName: '',
        priorityLevel: ''
      });
    }
  };

  _renderContents = t => {
    const { priorityName, priorityLevel } = this.state;
    return (
      <Fragment>
        <FormGroup>
          <Label className="font-weight-500">{t('name')}</Label>
          <FieldInput
            type="text"
            defaultValue={priorityName}
            onChangeValue={value => this.setState({ priorityName: value })}
          />
        </FormGroup>
        <FormGroup>
          <Label className="font-weight-500">{t('priority_level')}</Label>
          <FieldInput
            type="number"
            defaultValue={priorityLevel}
            onChangeValue={value => this.setState({ priorityLevel: value })}
          />
        </FormGroup>
      </Fragment>
    );
  };

  _renderActions = t => {
    const { editMode, value, onRemove, appPermissions } = this.props;
    const { priorityName, priorityLevel } = this.state;
    const disable = !priorityName || !priorityLevel;
    return (
      <Fragment>
        {editMode && isAllowPermission(appPermissions, PERM_REMOVE_SALE_PRIORITY) && (
          <Button size="sm" className="outline-base-remove-btn" onClick={() => onRemove(value.id)}>
            {t('remove')}
          </Button>
        )}
        <Button size="sm" className="base-btn" disabled={disable} onClick={() => this.onSubmitValue()}>
          {t(editMode ? 'save' : 'add')}
        </Button>
      </Fragment>
    );
  };

  onSubmitValue = () => {
    const { onOK, editMode, value } = this.props;
    const { priorityName, priorityLevel } = this.state;
    const id = editMode && value.id;
    onOK(id, priorityName, parseInt(priorityLevel));
  };

  render() {
    const { isOpen, onClose, editMode } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <BaseDialog
            size="md"
            title={editMode ? t('update_sale_priority') : t('new_sale_priority')}
            isOpen={isOpen}
            onClose={onClose}
            dialogContent={this._renderContents(t)}
            actionBtn={this._renderActions(t)}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

export default SalePriorityDialog;
