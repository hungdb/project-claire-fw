import './style.css';
import SuggestionTextField from '../../../03-components/SuggestionTextField/SuggestionTextField';
import i18n from '../../../i18n';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import ControlDatePicker from '../../../03-components/DateTimePicker';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import MarkdownEditor from '../../../03-components/MarkdownEditor';
import ReactMarkdown from 'react-markdown';
import breaks from 'remark-breaks';
import connect from 'react-redux/es/connect/connect';
import GroupFileUpload from '../../../03-components/GroupFileUpload';
import FileItemDetail from '../../../03-components/FileItemDetail';
import React, { PureComponent } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Input, Button, FormGroup, Label, Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, object, func, array, number } from 'prop-types';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';
import { PERM_REMOVE_SALE } from '../../../07-constants/app-permission';
import { isAllowPermission, calculateLength } from '../../../08-helpers/common';
import { linkRenderTarget } from '../../../08-helpers/component';
import {
  listAttachmentBySale,
  removeAttachmentBySale,
  addAttachmentBySale,
  downloadSaleAttachments
} from '../../../01-actions/sale';
import { MAX_FILE_UPLOAD, TOAST_ERROR_STYLE } from '../../../05-utils/commonData';

const ATTACHMENT_LIMIT = 10;

class SaleDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pic: '',
      company: '',
      appointmentStartDate: null,
      appointmentEndDate: null,
      appointmentPlace: '',
      discussionContent: '',
      memoMeeting: '',
      memoProgress: '',
      otherMemo: '',
      contact: '',
      categoryId: '',
      priorityId: '',
      statusId: '',
      note: '',
      files: [],
      attachmentOffset: 0,
      attachmentLimit: ATTACHMENT_LIMIT,
      confirmAlert: false,
      attachmentId: '',
      customerId: ''
    };
  }

  static propTypes = {
    isOpen: bool,
    editMode: bool,
    showMode: bool,
    value: object,
    onClose: func,
    onOK: func,
    appPermissions: array,
    saleStatuses: array,
    saleCategories: array,
    salePriorities: array,
    listUsers: array,
    saleAttachments: array,
    listAttachmentBySale: func,
    removeAttachmentBySale: func,
    addAttachmentBySale: func,
    downloadSaleAttachments: func,
    totalAttachments: number,
    listCustomer: array
  };

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    const initState = {
      pic: '',
      company: '',
      appointmentStartDate: null,
      appointmentEndDate: null,
      appointmentPlace: '',
      discussionContent: '',
      memoMeeting: '',
      memoProgress: '',
      otherMemo: '',
      contact: '',
      categoryId: '',
      priorityId: '',
      statusId: '',
      files: [],
      attachmentLimit: ATTACHMENT_LIMIT,
      customerId: ''
    };
    if (nextProps.value) {
      this.setState({
        pic: nextProps.value.pic,
        company: nextProps.value.company,
        appointmentStartDate: new Date(nextProps.value.appointmentStartDate),
        appointmentEndDate: new Date(nextProps.value.appointmentEndDate),
        appointmentPlace: nextProps.value.appointmentPlace,
        discussionContent: nextProps.value.discussionContent,
        memoMeeting: nextProps.value.memoMeeting,
        memoProgress: nextProps.value.memoProgress,
        otherMemo: nextProps.value.otherMemo,
        contact: nextProps.value.contact,
        categoryId: nextProps.value.categoryId,
        priorityId: nextProps.value.priorityId,
        statusId: nextProps.value.statusId,
        customerId: nextProps.value.customerId
      });
    } else this.setState(initState);
  }

  componentDidUpdate = prevProps => {
    const { listAttachmentBySale, value, editMode, showMode } = this.props;
    const { attachmentOffset, attachmentLimit } = this.state;
    if (value !== prevProps.value && (value && value.id)) {
      if (editMode || showMode) {
        listAttachmentBySale(value.id, attachmentOffset, attachmentLimit);
      }
    }
  };

  onChangeField = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmitValue = removeMode => {
    const { onOK, editMode, value } = this.props;
    const {
      categoryId,
      priorityId,
      statusId,
      pic,
      company,
      appointmentStartDate,
      appointmentEndDate,
      appointmentPlace,
      discussionContent,
      memoMeeting,
      memoProgress,
      otherMemo,
      contact,
      note,
      files,
      customerId
    } = this.state;
    onOK(editMode, removeMode, {
      id: editMode && value.id,
      categoryId,
      priorityId,
      statusId,
      pic,
      company,
      appointmentStartDate,
      appointmentEndDate,
      appointmentPlace,
      discussionContent,
      memoMeeting,
      memoProgress,
      otherMemo,
      contact,
      note,
      files,
      customerId
    });
  };

  handleChangeType = (selected, e) => {
    const value = selected ? selected.value : '';
    this.setState({ [e.name]: value });
  };

  onUploadFile = files => {
    const { editMode, value, addAttachmentBySale } = this.props;

    if (!editMode) {
      if (calculateLength([...this.state.files]) < MAX_FILE_UPLOAD) {
        this.setState({
          files: [...this.state.files, files[0]]
        });
      } else {
        toast.error(i18n.t('limit_3_files'), TOAST_ERROR_STYLE);
      }
    }

    if (editMode && value) {
      addAttachmentBySale(value.id, files, value.actor);
    }
  };

  onClearAttachmentState = id => {
    const data = [...this.state.files];
    for (let i = 0; i < calculateLength(data); i++) {
      if (i === id) data.splice(i, 1);
    }
    this.setState({ files: data });
  };

  _onRemoveAttachmentsBySale = () => {
    const { attachmentId } = this.state;
    const { removeAttachmentBySale } = this.props;
    removeAttachmentBySale(attachmentId);
    this.onCloseAlert();
  };

  _downloadSalesAttachments = attachments => {
    const { downloadSaleAttachments } = this.props;
    downloadSaleAttachments(attachments.url);
  };

  loadMoreAttachments = () => {
    this.setState(
      {
        attachmentLimit: this.state.attachmentLimit + this.state.attachmentLimit
      },
      () => {
        const { value, listAttachmentBySale } = this.props;
        const { attachmentOffset, attachmentLimit } = this.state;
        listAttachmentBySale(value.id, attachmentOffset, attachmentLimit);
      }
    );
  };

  openConfirmDialog = attachmentId => {
    this.setState({
      confirmAlert: true,
      attachmentId
    });
  };

  onCloseAlert = () => {
    this.setState({
      confirmAlert: false,
      attachmentId: ''
    });
  };

  onChangeCustomer = e => {
    if (e) {
      const { listCustomer } = this.props;
      const result = listCustomer.find(item => item.id === e.value);
      if (result)
        this.setState({
          company: result.company,
          customerId: e.value
        });
      else {
        this.setState({
          customerId: e.value
        });
      }
    } else {
      this.setState({
        company: '',
        customerId: ''
      });
    }
  };

  getCustomerByCompany = listCustomer => {
    const { company } = this.state;
    const result = listCustomer.filter(c => c.company.includes(company));
    if (!calculateLength(result) || !company) return listCustomer;
    return result;
  };

  getCompanySuggestion = data => (calculateLength(data) ? [...new Set(data.map(item => item.company))] : []);

  render() {
    const {
      isOpen,
      editMode,
      onClose,
      value,
      appPermissions,
      saleStatuses,
      saleCategories,
      salePriorities,
      listUsers,
      showMode,
      saleAttachments,
      totalAttachments,
      listCustomer
    } = this.props;
    const {
      categoryId,
      priorityId,
      statusId,
      pic,
      appointmentStartDate,
      appointmentEndDate,
      files,
      confirmAlert,
      customerId,
      company
    } = this.state;

    const disableSubmit = !(statusId && categoryId && priorityId && pic && appointmentStartDate && appointmentEndDate);
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal isOpen={isOpen} toggle={onClose} centered size="lg" backdrop="static">
            <ModalHeader toggle={onClose}>
              {t(editMode ? 'edit_sale' : showMode ? 'sale_detail' : 'new_sale')}
            </ModalHeader>
            <ModalBody>
              <div className="content-modal sale-dialog">
                <Row>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('status')}*</Label>
                      <SelectPropsBox
                        isDisabled={showMode}
                        options={saleStatuses}
                        value={statusId}
                        onChange={this.handleChangeType}
                        keyValue="id"
                        keyLabel="name"
                        placeholder={'status'}
                        name={'statusId'}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('category')}*</Label>
                      <SelectPropsBox
                        isDisabled={showMode}
                        options={saleCategories}
                        value={categoryId}
                        onChange={this.handleChangeType}
                        keyValue="id"
                        keyLabel="name"
                        placeholder={'category'}
                        name={'categoryId'}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('priority')}*</Label>
                      <SelectPropsBox
                        isDisabled={showMode}
                        options={salePriorities}
                        value={priorityId}
                        onChange={this.handleChangeType}
                        keyValue="id"
                        keyLabel="name"
                        placeholder={'priority'}
                        name={'priorityId'}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('pic')}*</Label>
                      <SelectPropsBox
                        isDisabled={showMode}
                        options={listUsers}
                        value={pic}
                        onChange={this.handleChangeType}
                        keyValue="id"
                        keyLabel="name"
                        placeholder={'pic'}
                        name={'pic'}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <FormGroup className="mb-4">
                      <Label>{t('appointment_place')}*</Label>
                      <Input
                        disabled={showMode}
                        defaultValue={editMode || showMode ? value && value.appointmentPlace : ''}
                        onChange={this.onChangeField}
                        className={'flex-grow-1 full focus-border'}
                        name="appointmentPlace"
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('note')}*</Label>
                      <Input
                        disabled={showMode}
                        defaultValue={editMode || showMode ? value && value.note : ''}
                        onChange={this.onChangeField}
                        className={'flex-grow-1 full focus-border'}
                        name="note"
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <FormGroup className="mb-4">
                      <Label>{t('appointment_start_date')}*</Label>
                      <ControlDatePicker
                        isLocalFormat={1}
                        disabled={showMode}
                        value={appointmentStartDate}
                        format="DD/MM/YYYY HH:mm"
                        placeholder={t('appointment_start_date')}
                        showTimeSelect={true}
                        onChange={appointmentStartDate => this.setState({ appointmentStartDate })}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="6">
                    <FormGroup className="mb-4">
                      <Label>{t('appointment_end_date')}*</Label>
                      <ControlDatePicker
                        isLocalFormat={1}
                        disabled={showMode}
                        value={appointmentEndDate}
                        format="DD/MM/YYYY HH:mm"
                        placeholder={t('appointment_end_date')}
                        showTimeSelect={true}
                        onChange={appointmentEndDate => this.setState({ appointmentEndDate })}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('company')}</Label>
                      <SuggestionTextField
                        disabled={showMode}
                        defaultValue={company}
                        onClickToSelect={company => this.setState({ company })}
                        onChangeToSelect={company => this.setState({ company })}
                        suggestions={this.getCompanySuggestion(listCustomer)}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup className="mb-4">
                      <Label>{t('customer')}</Label>
                      <SelectPropsBox
                        isDisabled={showMode}
                        options={this.getCustomerByCompany(listCustomer)}
                        onChange={this.onChangeCustomer}
                        value={customerId}
                        keyValue="id"
                        keyLabel="name"
                        placeholder={'customer'}
                        isClearable={true}
                        name={'customerId'}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup className="mb-4 file-upload-area">
                      <Label className="d-block mb-3">{t('attachments')}</Label>
                      {!showMode ? (
                        <GroupFileUpload
                          multiple={false}
                          onHandleUpload={this.onUploadFile}
                          notes={
                            editMode ? (
                              <div className="d-flex w-100 justify-content-center">
                                <p className="font-size-12 mb-0">*{t('attachments_will_be_upload_when_selected')}</p>
                              </div>
                            ) : null
                          }
                        />
                      ) : null}
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="sale-attachment-area">
                  {/* render all files when create new sales */}
                  {calculateLength(files) && !editMode && !showMode
                    ? files.map((file, index) => (
                        <Col sm="6" key={index}>
                          <FileItemDetail
                            enableRemove
                            index={index}
                            attachment={file}
                            onRemoveAttachments={this.onClearAttachmentState}
                          />
                        </Col>
                      ))
                    : null}
                  {/* render props saleAttachments when edit sale or show sale detail */}
                  {calculateLength(saleAttachments) && (editMode || showMode) ? (
                    saleAttachments.map(file => (
                      <Col sm="6" key={file.id}>
                        <FileItemDetail
                          attachment={file}
                          enableRemove={!showMode}
                          onOpenAlertAttachment={this._downloadSalesAttachments}
                          onRemoveAttachments={this.openConfirmDialog}
                        />
                      </Col>
                    ))
                  ) : showMode ? (
                    <Col>
                      <div className="files-not-found mb-4">
                        <p className="text-muted">{t('not_found')}</p>
                      </div>
                    </Col>
                  ) : null}
                  {/* Paginate attachments */}
                  {totalAttachments > calculateLength(saleAttachments) ? (
                    <Col>
                      <div className={'text-center text-uppercase mb-3'}>
                        <i className="fa fa-repeat mr-1" />
                        <span className="pointer-view-more" onClick={this.loadMoreAttachments}>
                          {t('view_more')}
                        </span>
                      </div>
                    </Col>
                  ) : null}
                </Row>
                <ConfirmationModal
                  isOpen={confirmAlert}
                  txtTitle={t('confirm_remove')}
                  txtCancel={t('cancel')}
                  txtConfirm={t('confirm')}
                  funConfirm={this._onRemoveAttachmentsBySale}
                  funClose={this.onCloseAlert}
                >
                  {t('are_you_sure_remove_this_attachment')}
                </ConfirmationModal>
                <Row>
                  <Col xs={12}>
                    <FormGroup className="mb-4">
                      <Label>{t('discussion_content')}</Label>
                      {!showMode && (
                        <MarkdownEditor
                          value={editMode || showMode ? value && value.discussionContent : ''}
                          onChange={discussionContent => this.setState({ discussionContent })}
                        />
                      )}
                      {showMode && (
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={(value && value.discussionContent) || t('not_found')}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      )}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <FormGroup className="mb-4">
                      <Label>{t('memo_meeting')}</Label>
                      {!showMode && (
                        <MarkdownEditor
                          value={editMode || showMode ? value && value.memoMeeting : ''}
                          onChange={memoMeeting => this.setState({ memoMeeting })}
                        />
                      )}
                      {showMode && (
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={(value && value.memoMeeting) || t('not_found')}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12}>
                    <FormGroup className="mb-4">
                      <Label>{t('memo_progress')}</Label>
                      {!showMode && (
                        <MarkdownEditor
                          value={editMode || showMode ? value && value.memoProgress : ''}
                          onChange={memoProgress => this.setState({ memoProgress })}
                        />
                      )}
                      {showMode && (
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={(value && value.memoProgress) || t('not_found')}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      )}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <FormGroup className="mb-4">
                      <Label>{t('other_memo')}</Label>
                      {!showMode && (
                        <MarkdownEditor
                          value={editMode || showMode ? value && value.otherMemo : ''}
                          onChange={otherMemo => this.setState({ otherMemo })}
                        />
                      )}
                      {showMode && (
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={(value && value.otherMemo) || t('not_found')}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12}>
                    <FormGroup className="mb-4">
                      <Label>{t('contact')}</Label>
                      {!showMode && (
                        <MarkdownEditor
                          value={editMode || showMode ? value && value.contact : ''}
                          onChange={contact => this.setState({ contact })}
                        />
                      )}
                      {showMode && (
                        <ReactMarkdown
                          plugins={[breaks]}
                          className="markdown-content"
                          source={(value && value.contact) || t('not_found')}
                          renderers={{ link: props => linkRenderTarget(props) }}
                        />
                      )}
                    </FormGroup>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div>
                  {editMode && isAllowPermission(appPermissions, PERM_REMOVE_SALE) && (
                    <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => this.onSubmitValue(true)}>
                      {t('remove')}
                    </Button>
                  )}
                </div>
                <div>
                  <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                    {t('cancel')}
                  </Button>
                  {!showMode && (
                    <Button
                      disabled={disableSubmit}
                      className={'base-btn'}
                      size={'sm'}
                      onClick={() => this.onSubmitValue(false)}
                    >
                      {t(editMode ? 'save' : 'add')}
                    </Button>
                  )}
                </div>
              </div>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    saleAttachments: store.sale.saleAttachments,
    totalAttachments: store.sale.totalAttachments
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAttachmentBySale,
      removeAttachmentBySale,
      addAttachmentBySale,
      downloadSaleAttachments
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaleDialog);
