import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bool, object, func, array } from 'prop-types';
import { FormGroup, Label, Button } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';

class MarketDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      market: ''
    };
  }

  static propTypes = {
    isOpen: bool,
    editMode: bool,
    value: object,
    onClose: func,
    onRemoveMarket: func,
    onOK: func,
    appPermissions: array,
    onOk: func
  };

  // eslint-disable-next-line
  componentWillReceiveProps = nextProps => {
    if (nextProps.value !== this.props.value) {
      this.setState({
        market: nextProps.value ? nextProps.value.name : ''
      });
    }
  };

  onChangeMarket = value => {
    this.setState({ market: value });
  };

  onSubmitValue = () => {
    const { onOk } = this.props;
    const { market } = this.state;
    onOk(market);
  };

  _renderContent = t => {
    // const { editMode, value } = this.props;
    const { market } = this.state;
    return (
      <FormGroup>
        <Label>{t('market_name')}</Label>
        <FieldInput type="text" defaultValue={market} onChangeValue={this.onChangeMarket} />
      </FormGroup>
    );
  };

  _renderAction = t => {
    const { editMode } = this.props;
    const { market } = this.state;
    return (
      <Button disabled={!market} className={'base-btn'} size={'sm'} onClick={this.onSubmitValue}>
        {t(editMode ? 'save' : 'add')}
      </Button>
    );
  };

  _renderRemoveBtn = t => {
    const { editMode, onRemoveMarket, value } = this.props;
    return (
      <>
        {editMode ? (
          <Button className="outline-base-remove-btn" size={'sm'} onClick={() => onRemoveMarket(value && value.id)}>
            {t('remove')}
          </Button>
        ) : null}
      </>
    );
  };

  render() {
    const { editMode, ...restProps } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <BaseDialog
            dialogWidth="md"
            title={t(editMode ? 'edit_market' : 'add_market')}
            dialogContent={this._renderContent(t)}
            actionBtn={this._renderAction(t)}
            actionBtnLeft={this._renderRemoveBtn(t)}
            {...restProps}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

export default MarketDialog;
