import PropTypes from 'prop-types';
import React, { Fragment, useState, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';
import PermissionSelector from '../../permission-selector/permission-selector';

function PermissionSchemeDetailDialog({ isOpen, permissions, permission, onClose, onOK }) {
  const [permissionSelected, setPermissionSelected] = useState(null);

  const _renderContent = (editMode, t) => {
    return (
      <div className="content-modal d-flex flex-column align-items-stretch">
        {!editMode ? (
          <PermissionSelector
            permissions={permissions}
            onChange={e => {
              setPermissionSelected(e.value);
            }}
          />
        ) : (
          <FieldInput defaultValue={permission.permission} disabled={true} placeholder={t('permission')} type="text" />
        )}
      </div>
    );
  };

  const _renderActionRight = (editMode, t) => {
    return (
      <Fragment>
        <Button
          disabled={!permissionSelected || editMode}
          className={'base-btn'}
          size={'sm'}
          onClick={() => {
            onOK &&
              onOK({
                permission: permissionSelected,
                permissionConditions: []
              });
          }}
        >
          {t(editMode ? 'save' : 'add')}
        </Button>
      </Fragment>
    );
  };

  const _renderActionLeft = (editMode, t) => {
    return (
      <Fragment>
        {editMode && (
          <Button
            size={'sm'}
            className={'outline-base-remove-btn'}
            onClick={() => {
              onOK && onOK({ permission: null });
            }}
          >
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  const editMode = !!permission;
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          isOpen={isOpen}
          onClose={onClose}
          title={!editMode ? t('add_permission') : t('update_permission')}
          dialogContent={_renderContent(editMode, t)}
          actionBtn={_renderActionRight(editMode, t)}
          actionBtnLeft={_renderActionLeft(editMode, t)}
        />
      )}
    </NamespacesConsumer>
  );
}

PermissionSchemeDetailDialog.propTypes = {
  isOpen: PropTypes.bool,
  permissions: PropTypes.any,
  permission: PropTypes.any,
  onClose: PropTypes.func,
  onOK: PropTypes.func
};

export default memo(PermissionSchemeDetailDialog);
