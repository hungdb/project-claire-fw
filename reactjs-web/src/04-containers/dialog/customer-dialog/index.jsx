import './style.css';
import i18n from '../../../i18n';
import ConfirmationModal from '../../../03-components/ConfirmationModal';
import UploadAvatar from '../../../03-components/UploadAvatar';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';
import SelectPropsBox from '../../../03-components/SelectPropsBox';
import imgAvt from '../../../06-assets/default_avatar.png';
import defaultAvatar from '../../../06-assets/default_avatar.png';
import GroupFileUpload from '../../../03-components/GroupFileUpload';
import SuggestionTextField from '../../../03-components/SuggestionTextField/SuggestionTextField';
import ReactGallery from '../../../03-components/ReactGallery';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { object, bool, func, array } from 'prop-types';
import { FormGroup, Label, Button, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CUSTOMER_STATUS, MAX_FILE_UPLOAD, TOAST_ERROR_STYLE } from '../../../05-utils/commonData';
import { calculateLength, linkImage } from '../../../08-helpers/common';
import { allowMimeTypesByType } from '../../../08-helpers/mime_types';
import { toast } from 'react-toastify';
import {
  getListAttachmentByCustomer,
  removeAttachmentsByCustomer,
  addAttachmentsByCustomer,
  downloadCustomerAttachments,
  onFormatData
} from '../../../01-actions/customer';

class CustomerDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      marketId: null,
      name: '',
      note: '',
      status: 1,
      company: '',
      files: [],
      avatar: null,
      previewAvartar: defaultAvatar,
      confirmAlert: false,
      attachmentId: null,
      openGallery: false,
      isSelectAvatar: false,
      test: {}
    };
  }

  static propTypes = {
    isOpen: bool,
    editMode: bool,
    onClose: func,
    onOk: func,
    value: object,

    getListMarket: func,
    getListAttachmentByCustomer: func,
    removeAttachmentsByCustomer: func,
    addAttachmentsByCustomer: func,
    downloadCustomerAttachments: func,
    onRemove: func,
    onFormatData: func,
    listMarkets: array,
    customerAttachments: array,
    showMode: bool,
    showDetail: bool,
    listCustomer: array,
    attachmentFormated: array
  };

  componentWillReceiveProps = nextProps => {
    //eslint-disable-line
    if (nextProps.value !== this.props.value) {
      this.setState({
        marketId: nextProps.value ? nextProps.value.marketId : '',
        name: nextProps.value ? nextProps.value.name : '',
        note: nextProps.value ? nextProps.value.note : '',
        status: nextProps.value ? nextProps.value.status : 1,
        company: nextProps.value ? nextProps.value.company : '',
        avatar: nextProps.value ? nextProps.value.avatar : '',
        previewAvartar: nextProps.value ? nextProps.value.avatar || defaultAvatar : defaultAvatar,
        isSelectAvatar: false,
        files: []
      });
    }
  };

  clearAllState = () => {
    this.setState({
      marketId: null,
      name: '',
      note: null,
      status: 1,
      company: '',
      files: [],
      isSelectAvatar: false
    });
  };

  componentDidUpdate = prevProps => {
    const { value, getListAttachmentByCustomer, onFormatData, customerAttachments, editMode } = this.props;
    if (prevProps.value !== value && value) {
      getListAttachmentByCustomer(value.id);
    }

    if (calculateLength(customerAttachments) && prevProps.customerAttachments !== customerAttachments && !editMode) {
      onFormatData(this.props.customerAttachments);
    }
  };

  handleChangeType = (selected, e) => {
    const value = selected ? selected.value : '';
    this.setState({ [e.name]: value });
  };

  onClearAttachmentState = id => {
    const data = [...this.state.files];
    for (let i = 0; i < calculateLength(data); i++) {
      if (i === id) data.splice(i, 1);
    }
    this.setState({ files: data });
  };

  onUploadFile = files => {
    const { editMode, value, addAttachmentsByCustomer } = this.props;
    if (!editMode) {
      if (calculateLength([...this.state.files, ...files]) < MAX_FILE_UPLOAD) {
        this.setState({
          files: [...this.state.files, ...files].map(file =>
            Object.assign(file, {
              preview: URL.createObjectURL(file)
            })
          )
        });
      } else {
        toast.error(i18n.t('limit_2_files'), TOAST_ERROR_STYLE);
      }
    }
    if (editMode && value) {
      addAttachmentsByCustomer(value.id, files);
    }
  };

  onSubmitValue = () => {
    const { onOk } = this.props;
    const { name, company, marketId, status, files, note, avatar } = this.state;

    onOk({
      name,
      company,
      marketId,
      status,
      files,
      note,
      avatar
    });
    this.clearAllState();
  };

  openConfirmDialog = attachmentId => {
    this.setState({
      confirmAlert: true,
      attachmentId
    });
  };

  onCloseAlert = () => {
    this.setState({
      confirmAlert: false,
      attachmentId: ''
    });
  };

  _onRemoveAttachmentsByCustomer = () => {
    const { removeAttachmentsByCustomer } = this.props;
    const { attachmentId } = this.state;
    removeAttachmentsByCustomer(attachmentId);
  };

  onPreviewImage = (avatar, previewAvartar) => {
    this.setState({
      avatar: avatar,
      isSelectAvatar: true,
      previewAvartar
    });
  };

  getCompanySuggestion = data => (calculateLength(data) ? [...new Set(data.map(item => item.company))] : []);

  _renderContent = t => {
    const {
      editMode,
      customerAttachments,
      showMode,
      listMarkets,
      listCustomer,
      showDetail,
      attachmentFormated
    } = this.props;
    const { marketId, name, status, company, files, note, isSelectAvatar, previewAvartar } = this.state;
    const mimes = allowMimeTypesByType('image');

    return (
      <>
        {(!showMode || showDetail) && (
          <>
            <Row className="mb-3">
              <FormGroup className="col-12">
                <Label>{t('avatar')}*</Label>
                <UploadAvatar
                  onSelectImage={this.onPreviewImage}
                  defaultAvatar={
                    isSelectAvatar // check is select image before upload
                      ? previewAvartar //display image when selected
                      : // if edit customer, image url will be using link image to get url ${linkImage(previewAvartar)}
                      // when add new customer image url is default ${imgAvt}
                      editMode || showDetail
                      ? linkImage(previewAvartar)
                      : imgAvt
                  }
                  mimes={mimes}
                  disableEdit={showDetail}
                />
              </FormGroup>
              <FormGroup className="col-sm-12 col-md-6">
                <Label>{t('name')}*</Label>
                <FieldInput
                  type="text"
                  defaultValue={name}
                  onChangeValue={value => this.setState({ name: value })}
                  name="name"
                  disabled={showDetail}
                />
              </FormGroup>
              <FormGroup className="col-sm-12 col-md-6">
                <Label>{t('market_name')}*</Label>
                <SelectPropsBox
                  options={listMarkets}
                  keyValue="id"
                  keyLabel="name"
                  value={marketId}
                  onChange={this.handleChangeType}
                  placeholder={'market_name'}
                  name={'marketId'}
                  isDisabled={showDetail}
                />
              </FormGroup>
            </Row>
            <Row className="mb-3">
              <FormGroup className="col-sm-12 col-md-6">
                <Label>{t('company')}</Label>
                <SuggestionTextField
                  disabled={showDetail}
                  defaultValue={company}
                  onClickToSelect={company => this.setState({ company })}
                  onChangeToSelect={company => this.setState({ company })}
                  suggestions={this.getCompanySuggestion(listCustomer)}
                />
              </FormGroup>
              <FormGroup className="col-sm-12 col-md-6">
                <Label>{t('status')}</Label>
                <SelectPropsBox
                  isDisabled={showDetail}
                  options={CUSTOMER_STATUS}
                  keyValue="value"
                  keyLabel="label"
                  value={status}
                  isClearable={false}
                  onChange={this.handleChangeType}
                  placeholder={'status'}
                  name={'status'}
                />
              </FormGroup>
            </Row>

            <FormGroup className="mb-4 file-upload-area">
              <Label className="d-block mb-3">{t('images')}</Label>
              {!showDetail ? (
                <GroupFileUpload
                  multiple={true}
                  onHandleUpload={this.onUploadFile}
                  mimes={mimes}
                  notes={
                    editMode ? (
                      <div className="d-flex w-100 justify-content-center">
                        <p className="font-size-12 mb-0">*{t('attachments_will_be_upload_when_selected')}</p>
                      </div>
                    ) : null
                  }
                />
              ) : null}
            </FormGroup>
          </>
        )}
        <Row className="sale-attachment-area">
          {/* render attachemnt by state when create new customer */}
          {calculateLength(files) && !editMode
            ? files.map((file, index) => (
                <Col sm="6" md="4" key={index}>
                  <div className="preview-files-upload">
                    <img className="w-100p mb-4 img-thumbnail" src={file.preview} alt={file.name} />
                    <span onClick={() => this.onClearAttachmentState(index)} className="at-btn-remove">
                      <i className="fa fa-3x fa-trash-o" />
                    </span>
                  </div>
                </Col>
              ))
            : null}

          {/* render all attachments by props when edit customer */}
          {calculateLength(customerAttachments) && editMode
            ? customerAttachments.map((file, index) => (
                <Col sm={12} md={6} key={index}>
                  <div className="preview-files-upload">
                    <img alt={t('business_card')} className="w-100p mb-4 img-thumbnail" src={linkImage(file.url)} />
                    {editMode && (
                      <span onClick={() => this.openConfirmDialog(file.id)} className="at-btn-remove">
                        {' '}
                        <i className="fa fa-3x fa-trash-o" />
                      </span>
                    )}
                  </div>
                </Col>
              ))
            : editMode && (
                <Col sm="12">
                  <p className="mb-0 text-center">{t('not_found')}</p>
                </Col>
              )}

          {/* render React Galery when show business cards or SHOW customer infor */}
          {calculateLength(attachmentFormated) && (showMode || showDetail) ? (
            <Col className="mb-3">
              <ReactGallery photos={attachmentFormated} />
            </Col>
          ) : (
            (showMode || showDetail) && (
              <Col sm="12">
                <p className="mb-0 text-center">{t('not_found')}</p>
              </Col>
            )
          )}
        </Row>
        {(!showMode || showDetail) && (
          <Row className="mb-3">
            <FormGroup className="col-12 col-md-12">
              <Label>{t('note')}</Label>
              <FieldInput
                type="text"
                defaultValue={note}
                onChangeValue={value => this.setState({ note: value })}
                name="note"
                disabled={showDetail}
              />
            </FormGroup>
          </Row>
        )}
      </>
    );
  };

  _renderAction = t => {
    const { marketId, avatar, name } = this.state;
    const { editMode, showMode, showDetail } = this.props;
    return (
      <>
        {!showMode && !showDetail ? (
          <Button
            disabled={!marketId || !name || !avatar || (editMode && !avatar)}
            className={'base-btn'}
            size={'sm'}
            onClick={this.onSubmitValue}
          >
            {editMode ? t('save') : t('add')}
          </Button>
        ) : null}
      </>
    );
  };

  _renderRemoveBtn = t => {
    const { value, editMode, onRemove } = this.props;
    return (
      <>
        {editMode && value ? (
          <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => onRemove(value.id)}>
            {t('remove')}
          </Button>
        ) : null}
      </>
    );
  };

  render() {
    const { editMode, showMode, showDetail, ...restProps } = this.props;
    const { confirmAlert } = this.state;
    const title = (editMode && 'edit_customer') || (showMode && 'business_cards') || (showDetail && 'customer_infor');

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <>
            <BaseDialog
              dialogWidth={showMode ? 'xl' : 'lg'}
              classes="customer-dialog"
              title={t(title || 'add_customer')}
              dialogContent={this._renderContent(t)}
              actionBtn={this._renderAction(t)}
              actionBtnLeft={this._renderRemoveBtn(t)}
              {...restProps}
            />
            <ConfirmationModal
              isOpen={confirmAlert}
              txtTitle={t('confirm_remove')}
              txtCancel={t('cancel')}
              txtConfirm={t('confirm')}
              funConfirm={this._onRemoveAttachmentsByCustomer}
              funClose={this.onCloseAlert}
            >
              {t('are_you_sure_remove_this_attachment')}
            </ConfirmationModal>
          </>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    customerAttachments: store.customer.customerAttachments,
    attachmentFormated: store.customer.attachmentFormated
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListAttachmentByCustomer,
      removeAttachmentsByCustomer,
      addAttachmentsByCustomer,
      downloadCustomerAttachments,
      onFormatData
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerDialog);
