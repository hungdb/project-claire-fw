import { bool, func, number, object } from 'prop-types';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Table } from 'reactstrap';
import TableHead from '../../../../03-components/TableHead';
import { ROUNDED_NUMBER_KPI } from '../../../../05-utils/commonData';
import { calculateLength, formatDateTime, toFixed } from '../../../../08-helpers/common';
import './styles.css';

const TABLE_FIELDS = ['no', 'evaluate_at', 'content_kpi_chart.total_point'];

function KPIDetailYearDialog({ item, isOpen, onClose, maxPoint }){

  const renderList = t => {
    if (!item) {
      return;
    }

    return (
      <Table bordered responsive>
        <thead>
          <tr>
            <TableHead fields={TABLE_FIELDS} t={t} />
          </tr>
        </thead>
        <tbody>
          {calculateLength(item.data) ? (
            item.data.map((item, index) => {
              return (
                <tr key={index + 1}>
                  <td>{index + 1}</td>
                  <td>{formatDateTime(item.startOfMonth, 'MMM YYYY', 1)}</td>
                  <td>
                    <strong className="color-base">{toFixed(item.point, ROUNDED_NUMBER_KPI) || 0}</strong>
                  </td>
                </tr>
              );
            })
          ) : (
            <tr>
              <td colSpan={10} className="font-size-15 text-muted">
                {t('no_kpi_data')}
              </td>
            </tr>
          )}
        </tbody>

        <tbody>
          <tr>
            <td className={'td-center'}>
              <strong>{t('total')}</strong>
            </td>
            <td>
              <strong>AVG(KPI_OF_MONTH)</strong>
            </td>
            <td className={'td-center'}>
              <strong className={'color-base'}>{toFixed(item.point, ROUNDED_NUMBER_KPI)}</strong>
            </td>
          </tr>
        </tbody>
      </Table>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered size="lg" className="kpi-detail-year-dialog">
          <ModalHeader toggle={onClose}>{t('kpi_detail')}</ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">
              {renderList(t)}
              <p className="mb-0">{`* ${t('MAX')}: ${maxPoint} (${t('points')})`}</p>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                {t('cancel')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

KPIDetailYearDialog.propTypes = {
  item: object,
  isOpen: bool,
  onClose: func,
  maxPoint: number
};

export default memo(KPIDetailYearDialog);
