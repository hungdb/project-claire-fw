import { bool, func, object } from 'prop-types';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Table } from 'reactstrap';
import { ROUNDED_NUMBER_KPI } from '../../../../05-utils/commonData';
import { calculateLength, formatDateTime, toFixed } from '../../../../08-helpers/common';
import TableHead from '../../../../03-components/TableHead';
import './styles.css';

const KPI_ITEMS_TABLE_FIELDS = [
  'no',
  'user',
  'content_kpi_chart.review_point',
  'content_kpi_chart.test_point',
  'content_kpi_chart.total_point',
  'actor',
  'evaluate_at'
];
function KPIItemSkillDialog({ item, isOpen, onClose }) {

  const renderTable = t => {
    return item && (
      <Table bordered responsive striped hover>
        <thead>
          <tr>
            <TableHead fields={KPI_ITEMS_TABLE_FIELDS} t={t} />
          </tr>
        </thead>
        <tbody>
          {calculateLength(item.data) ? (
            item.data.map((item, index) => {
              return (
                <tr key={index + 1}>
                  <td>{index + 1}</td>
                  <td>{item.user && item.user.name}</td>
                  <td>
                    <strong className="color-base">{toFixed(item.reviewPoint || 0, ROUNDED_NUMBER_KPI)}</strong>
                  </td>
                  <td>
                    <strong className="color-base">{toFixed(item.testPoint || 0, ROUNDED_NUMBER_KPI)}</strong>
                  </td>
                  <td>
                    <strong className="color-base">{toFixed(item.point || 0, ROUNDED_NUMBER_KPI)}</strong>
                  </td>
                  <td>{item.infoActor && item.infoActor.name}</td>
                  <td>{formatDateTime(item.evaluatedAt, 'LLLL')}</td>
                </tr>
              );
            })
          ) : (
            <tr>
              <td colSpan={10} className="font-size-15 text-muted">
                {t('no_kpi_data')}
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered size="lg" className="kpi-items-dialog">
          <ModalHeader toggle={onClose}>{t('kpi_items')}</ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">{renderTable(t)}</div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                {t('cancel')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

KPIItemSkillDialog.propTypes = {
  item: object,
  isOpen: bool,
  onClose: func
};

export default memo(KPIItemSkillDialog);
