import React, { PureComponent } from 'react';
import { bool, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Label, Input, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { calculateLength } from '../../../08-helpers/common';

class KpiCoefficientDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      percentSkillReview: null,
      percentSkillTest: null,
      percentDoProcess: null,
      percentPeopleManagement: null,
      percentWorkManagement: null,
      percentDedication: null,
      percentResponsibility: null,
      percentTaskComplete: null
    };
  }

  static propTypes = {
    isOpen: bool,
    dataUpdate: object,
    onSave: func,
    onCancel: func
  };

  onChangePercent(item, newPercent) {
    this.setState({
      [item.variable]: newPercent
    });
  }

  handleButtonSave() {
    const { dataUpdate, onSave } = this.props;
    const newData = {};
    for (let i = 0; i < dataUpdate.length; i++) {
      const variable = dataUpdate[i].variable;
      newData[variable] = Number(this.state[variable]) / 100;
    }
    newData && onSave(newData);
  }

  checkCondition() {
    const { dataUpdate } = this.props;
    if (dataUpdate) {
      let totalPercent = 0;
      for (let i = 0; i < dataUpdate.length; i++) {
        const variable = dataUpdate[i].variable;
        if (!this.state[variable] || Number(this.state[variable]) < 0 || Number(this.state[variable]) > 100) {
          return true;
        } else {
          totalPercent += Number(this.state[variable]);
        }
      }
      return totalPercent !== 100;
    } else {
      return true;
    }
  }

  render() {
    const { isOpen, dataUpdate, onCancel, ...props } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Modal
            className="allocation-label-dialog"
            isOpen={isOpen}
            toggle={onCancel}
            centered
            fade={true}
            backdrop="static"
            {...props}
          >
            <ModalHeader toggle={onCancel}>{t('kpi_coefficient_setting.title_update_dialog')}</ModalHeader>
            <ModalBody>
              <div className="content-modal">
                {dataUpdate &&
                  calculateLength(dataUpdate) &&
                  dataUpdate
                    .map(item => {
                      if (item) {
                        return (
                          <FormGroup>
                            <Label>{item.label} (%) </Label>
                            <Input
                              type="number"
                              placeholder={item.value}
                              value={this.state[item.variable]}
                              onChange={e => this.onChangePercent(item, e.target.value)}
                            />
                          </FormGroup>
                        );
                      } else {
                        return null;
                      }
                    })
                    .filter(Boolean)}
              </div>
            </ModalBody>
            <ModalFooter className="dialog-actions flex-grow-1 d-flex justify-content-between">
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onCancel}>
                {t('cancel')}
              </Button>
              <Button
                disabled={this.checkCondition()}
                className={'base-btn'}
                size={'sm'}
                onClick={() => this.handleButtonSave()}
              >
                {t('save')}
              </Button>
            </ModalFooter>
          </Modal>
        )}
      </NamespacesConsumer>
    );
  }
}

export default KpiCoefficientDialog;
