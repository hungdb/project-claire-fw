import { bool, func, number, object } from 'prop-types';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Table } from 'reactstrap';
import { ROUNDED_NUMBER_KPI, TYPE_ACTION_BEHAVIOR_OBJECT } from '../../../../05-utils/commonData';
import { calculateLength, toFixed } from '../../../../08-helpers/common';
import './styles.css';

function KPIDetailMonthDialog({ item, isOpen, onClose, defaultPoint, maxPoint }) {

  const renderList = t => {
    if (!item) {
      return;
    }
    let arrayAdd = [];
    let arraySub = [];

    Array.isArray(item.data) &&
      item.data.forEach(kpiItem => {
        const logs = kpiItem.logDetailBehaviors;
        const inforKPI = {
          evaluatedAt: kpiItem.evaluatedAt,
          infoActor: kpiItem.infoActor
        };
        Array.isArray(logs) &&
          logs.forEach(item => {
            const kpiBehavior = {
              ...item.behavior,
              ...inforKPI
            };
            if (item && item.behavior && item.behavior.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD) {
              arrayAdd = [...arrayAdd, kpiBehavior];
            } else if (item && item.behavior && item.behavior.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB) {
              arraySub = [...arraySub, kpiBehavior];
            }
          });
      });

    return (
      <Table bordered responsive>
        <tbody>
          {calculateLength(arrayAdd) ? (
            arrayAdd.map((item, index) => (
              <tr key={index}>
                {index === 0 && (
                  <td className={'td-center'} rowSpan={arrayAdd.length + 1}>
                    {t('point')} (+)
                  </td>
                )}
                <td>{item.activity}</td>
                <td className={'td-center'}>{toFixed(item.point, ROUNDED_NUMBER_KPI)}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td className={'td-center'} rowSpan={2}>
                {t('point')} (+)
              </td>
              <td colSpan={3} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}

          <tr>
            <td>
              <strong>{t('total')}</strong>
            </td>
            <td className={'td-center'}>
              <strong>{toFixed(item.totalPointAdd, ROUNDED_NUMBER_KPI)}</strong>
            </td>
          </tr>
        </tbody>
        <tbody>
          {calculateLength(arraySub) ? (
            arraySub.map((item, index) => (
              <tr key={index}>
                {index === 0 && (
                  <td className={'td-center'} rowSpan={arraySub.length + 1}>
                    {t('point')} (-)
                  </td>
                )}
                <td>{item.activity}</td>
                <td className={'td-center'}>{toFixed(item.point, ROUNDED_NUMBER_KPI)}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td rowSpan={2} className={'td-center'}>
                {t('point')} (-)
              </td>
              <td colSpan={3} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
          <tr>
            <td>
              <strong>{t('total')}</strong>
            </td>
            <td className={'td-center'}>
              <strong>{toFixed(item.totalPointSub, ROUNDED_NUMBER_KPI)}</strong>
            </td>
          </tr>
        </tbody>

        <tbody>
          <tr>
            <td className={'td-center'} rowSpan={arraySub.length}>
              <strong>{t('total')}</strong>
            </td>
            <td>
              <strong>DEFAULT_BEHAVIOR_POINT + Total_Point(+) - Total_Point(-)</strong>
            </td>
            <td className={'td-center'}>
              <strong className={'color-base'}>{toFixed(item.point, ROUNDED_NUMBER_KPI)}</strong>
            </td>
          </tr>
        </tbody>
      </Table>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={onClose} centered size="lg" className="kpi-detail-dialog">
          <ModalHeader toggle={onClose}>{t('kpi_detail')}</ModalHeader>
          <ModalBody>
            <div className="content-modal d-flex flex-column align-items-stretch">
              {renderList(t)}
              <p>{`* DEFAULT_POINT = ${defaultPoint} (${t('points')})`}</p>
              <p className="mb-0">{`* ${t('MAX')}: ${maxPoint} (${t('points')})`}</p>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-end'}>
              <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={onClose}>
                {t('cancel')}
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

KPIDetailMonthDialog.propTypes = {
  item: object,
  isOpen: bool,
  onClose: func,
  defaultPoint: number,
  maxPoint: number
};

export default memo(KPIDetailMonthDialog);
