import React, { PureComponent } from 'react';
import { TextField, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Button } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func, array, string } from 'prop-types';
import { formatDateTime, getDateAt000000ZeroGMT, getDateAt235959ZeroGMT } from '../../../08-helpers/common';
import Select from 'react-select';
import { ADD_ALLOCATION_SUCCESS } from '../../../07-constants/sprint';
import DatePicker from 'react-datepicker';

class CreateSprintDialog extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      effortPercent: 0,
      label: null,
      startDate: null,
      endDate: null,
      selectedOptionUser: null
    };
  }

  static propTypes = {
    isOpen: bool,
    onClose: func,
    onOK: func,
    users: array,
    status: string
  };

  componentDidUpdate() {
    if (this.props.status === ADD_ALLOCATION_SUCCESS) {
      this.setState({
        effortPercent: 0,
        startDate: new Date(),
        endDate: new Date(),
        selectedOptionUser: null,
        label: null
      });
    }
  }

  componentWillUnmount() {
    this.setState({
      effortPercent: 0,
      startDate: null,
      endDate: null,
      selectedOptionUser: null,
      label: null
    });
  }

  changeChooseUser = selectedOptionUser => {
    this.setState({ selectedOptionUser });
  };

  handleLabel = label => {
    this.setState({ label });
  };

  render() {
    const { label } = this.state;
    let usersInProject = {};
    this.props.users.forEach(u => {
      if (!usersInProject[u.userId]) {
        usersInProject[u.userId] = u;
      }
    });
    usersInProject = Object.values(usersInProject);
    const allocationOptions = this.props.allocationLabels
      .map(label => ({ value: label.group.id, label: label.group.name }))
      .filter(label => label.label.toUpperCase() !== 'NOGROUP');

    console.log(this.props.users);

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Dialog
            PaperProps={{
              style: {
                overflow: 'visible'
              }
            }}
            open={this.props.isOpen}
            onClose={() => {
              this.props.onClose && this.props.onClose();
            }}
          >
            <div style={{ width: '600px', overflow: 'visible' }}>
              <DialogTitle>{t('add_allocation')}</DialogTitle>
              <DialogContent className={'overflow-visible'}>
                <div className="content-modal d-flex flex-column align-items-stretch">
                  <Select
                    placeholder={t('select_user')}
                    value={this.state.selectedOptionUser}
                    options={usersInProject.map(p => ({ label: p.User && p.User.name, value: p.User && p.User.id }))}
                    onChange={select => this.changeChooseUser(select)}
                  />
                  <TextField
                    className="flex-grow-1 margin-top-15px mb-3"
                    defaultValue={this.state.effortPercent}
                    type="number"
                    required
                    name="effort"
                    onChange={e => {
                      this.setState({ effortPercent: parseFloat(e.target.value) });
                    }}
                    label={t('effort_percent')}
                  />
                  <Select value={label} options={allocationOptions} onChange={this.handleLabel} />
                  <div className={'margin-top-15px d-flex popup'}>
                    <DatePicker
                      value={formatDateTime(this.state.startDate, 'DD/MM/YYYY')}
                      placeholderText={t('start_date')}
                      dropdownMode={'scroll'}
                      onChange={e => {
                        this.setState({
                          startDate: e ? getDateAt000000ZeroGMT(e) : null
                        });
                      }}
                    />
                  </div>
                  <div className={'margin-top-15px d-flex popup'}>
                    <DatePicker
                      className={'flex-grow-1'}
                      value={formatDateTime(this.state.endDate, 'DD/MM/YYYY')}
                      placeholderText={t('end_date')}
                      dropdownMode={'scroll'}
                      onChange={e => {
                        this.setState({
                          endDate: e ? getDateAt235959ZeroGMT(e) : null
                        });
                      }}
                    />
                  </div>
                </div>
              </DialogContent>
              <DialogActions>
                <div className={'flex-grow-1 d-flex justify-content-end'}>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => {
                      this.props.onClose && this.props.onClose();
                    }}
                  >
                    {t('cancel')}
                  </Button>
                  <Button
                    disabled={
                      !this.state.selectedOptionUser ||
                      !this.state.effortPercent ||
                      !this.state.startDate ||
                      !this.state.endDate
                    }
                    className={'base-btn'}
                    size={'sm'}
                    onClick={() => {
                      this.props.onOK &&
                        this.props.onOK({
                          userId: this.state.selectedOptionUser && this.state.selectedOptionUser.value,
                          effortPercent: this.state.effortPercent,
                          labelId: label && label.value,
                          startDate: this.state.startDate,
                          endDate: this.state.endDate
                        });
                    }}
                  >
                    {t('add')}
                  </Button>
                </div>
              </DialogActions>
            </div>
          </Dialog>
        )}
      </NamespacesConsumer>
    );
  }
}

export default CreateSprintDialog;
