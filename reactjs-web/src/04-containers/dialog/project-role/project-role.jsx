import { bool, func, object } from 'prop-types';
import React, { Fragment, memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, FormGroup, Label } from 'reactstrap';
import BaseDialog from '../../../03-components/Dialog';
import FieldInput from '../../../03-components/FieldInput';
import './style.css';

function ProjectRoleDialog({ valuePR, isOpen, editMode, onClose, onOK }) {
  const [projectRoleName, setProjectRoleName] = useState('');
  const [priorityLevel, setPriorityLevel] = useState(null);

  useEffect(() => {
    setProjectRoleName(valuePR && valuePR.name);
    setPriorityLevel(valuePR && valuePR.priorityLevel);
  }, [valuePR]);

  const onChangeRole = value => setProjectRoleName(value);
  const onChangePriority = value => setPriorityLevel(value);

  const _renderContent = t => {
    return (
      <Fragment>
        <FormGroup>
          <Label>{t('Role Project')}</Label>
          <FieldInput
            type="text"
            placeholder={'project_role_name'}
            onChangeValue={onChangeRole}
            defaultValue={projectRoleName}
          />
        </FormGroup>
        <FormGroup>
          <Label>{t('priority')}</Label>
          <FieldInput
            type="number"
            title={t('priority_level')}
            onChangeValue={onChangePriority}
            defaultValue={priorityLevel}
          />
        </FormGroup>
      </Fragment>
    );
  };

  const _renderBtnActionRight = t => {
    return (
      <Fragment>
        <Button
          size="sm"
          className="base-btn"
          disabled={!projectRoleName && !priorityLevel}
          onClick={() => {
            onOK &&
              onOK(editMode, 0, {
                id: editMode && valuePR.id,
                name: projectRoleName,
                priorityLevel: Number(priorityLevel)
              });
          }}
        >
          {t(editMode ? 'save' : 'add')}
        </Button>
      </Fragment>
    );
  };

  const _renderBtnActionLeft = t => {
    return (
      <Fragment>
        {editMode && (
          <Button
            size="sm"
            className="outline-base-remove-btn"
            onClick={() => {
              onOK &&
                onOK(editMode, 1, {
                  id: editMode && valuePR.id
                });
            }}
          >
            {t('remove')}
          </Button>
        )}
      </Fragment>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          dialogWidth="md"
          isOpen={isOpen}
          onClose={onClose}
          classes="project-role-dialog"
          actionBtn={_renderBtnActionRight(t)}
          actionBtnLeft={_renderBtnActionLeft(t)}
          dialogContent={_renderContent(t)}
          title={t(editMode ? 'edit_project_role' : 'new_project_role')}
        />
      )}
    </NamespacesConsumer>
  );
}

ProjectRoleDialog.propTypes = {
  valuePR: object,
  value: object,
  isOpen: bool,
  editMode: bool,
  onClose: func,
  onOK: func
};

export default memo(ProjectRoleDialog);
