import { any } from 'prop-types';
import React, { memo } from 'react';
import Select from 'react-select';
import { ISSUE_OPTIONS } from '../../05-utils/commonData';

function IssueStatusSelector({ value, ...props }) {
  const issueSelects = ISSUE_OPTIONS.filter(item => item.value === Number(value.status));

  return (
    value && (
      <Select
        value={Array.isArray(issueSelects) ? issueSelects[0] : null}
        options={ISSUE_OPTIONS.filter(o => o.value !== Number(value.status))}
        {...props}
      />
    )
  );
}

IssueStatusSelector.propTypes = {
  value: any,
};

export default memo(IssueStatusSelector);
