import React, { memo } from 'react';
import { array, func, any } from 'prop-types';
import { Button, ButtonGroup } from 'reactstrap';
import { PROJECT_TABS } from '../../../../05-utils/commonData';

const ProjectSettingTabs = ({ onChangeTab, visibleTabs, selectedSettingTab, t }) => (
  <ButtonGroup className="button-group-tab">
    {PROJECT_TABS.map(tab =>
      !visibleTabs.includes(tab.value) ? null : (
        <Button
          key={tab.value}
          onClick={() => onChangeTab(tab)}
          active={selectedSettingTab === tab.value}
          className="tabs-btn project-page"
        >
          {t(tab.label)}
        </Button>
      )
    )}
  </ButtonGroup>
);

ProjectSettingTabs.propTypes = {
  onChangeTab: func,
  visibleTabs: array,
  selectedSettingTab: any,
  t: any
};

export default memo(ProjectSettingTabs);
