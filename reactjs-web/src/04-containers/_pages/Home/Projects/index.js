import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPermissionSchemes } from '../../../../01-actions/permission-scheme';
import { addNotification } from '../../../../01-actions/notification';
import { fireSocketAddComment } from '../../../../01-actions/comment';
import { setSelectedSettingTab } from '../../../../01-actions/project';
import Project from './Project.jsx';

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    projects: store.project.projects,
    selectedProject: store.project.selectedProject,
    selectedSettingTab: store.project.selectedSettingTab,
    selectedProjectGroup: store.group.selectedProjectGroup
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getPermissionSchemes,
      addNotification,
      fireSocketAddComment,
      setSelectedSettingTab
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Project);
