import ProjectList from './ProjectList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { clearUserInProject, getUsers } from '../../../../../01-actions/userInProject';
import { getWorkflows } from '../../../../../01-actions/workflow';
import {
  getProjectEnvironments,
  switchProject,
  getProjects,
  createProject,
  clearSwitchProject,
  setSelectedSettingTab,
  updateProject,
  removeProjects,
  getMyProjects
} from '../../../../../01-actions/project';

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  roles: state.auth.roles,
  projects: state.project.projects,
  selectedProjectGroup: state.group.selectedProjectGroup,
  selectedProject: state.project.selectedProject
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProjectEnvironments,
      switchProject,
      getProjects,
      getMyProjects,
      createProject,
      getWorkflows,
      clearSwitchProject,
      clearUserInProject,
      getUsers,
      setSelectedSettingTab,
      updateProject,
      removeProjects
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProjectList)
);
