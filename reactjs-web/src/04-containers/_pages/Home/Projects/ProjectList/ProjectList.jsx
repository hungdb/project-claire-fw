import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useEffect, useState } from 'react';
import { array, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission, calculateLength, promiseDelayImport } from '../../../../../08-helpers/common';
import { PROJECT_ACTIVE_STATUS } from '../../../../../05-utils/commonData';
import {
  PERM_ADD_PROJECT,
  PERM_LIST_MY_PROJECTS,
  PERM_LIST_PROJECTS,
  PERM_LIST_WORKFLOW
} from '../../../../../07-constants/app-permission';

const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <Skeleton width={121.13} height={34} />
);

const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={100} />
);

const AddProjectDialog = LazyComponent(() => import('../../../../dialog/add-project/add-project'));
const UpdateProjectDialog = LazyComponent(() => import('../../../../dialog/update-project/update-project'));
const ProjectsByStatus = LazyComponent(() => import('./ProjectsByStatus'));

const ProjectList = ({
  appPermissions,
  roles,
  projects,
  selectedProjectGroup,
  selectedProject,
  match,
  history,
  getProjectEnvironments,
  switchProject,
  getProjects,
  getMyProjects,
  createProject,
  getWorkflows,
  clearSwitchProject,
  clearUserInProject,
  getUsers,
  setSelectedSettingTab,
  updateProject,
  removeProjects
}) => {
  const [state, setState] = useState({
    showAddProjectDialog: false,
    showProjectDialog: false,
    editProject: null
  });

  const { showAddProjectDialog, showProjectDialog, editProject } = state;

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_PROJECTS)) {
      getProjects();
    } else if (isAllowPermission(appPermissions, PERM_LIST_MY_PROJECTS)) {
      getMyProjects();
    }

    if (isAllowPermission(appPermissions, PERM_LIST_WORKFLOW)) {
      getWorkflows();
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  useEffect(() => {
    if (Number(match.params.projectId)) {
      for (const p of projects) {
        if (p.id === Number(match.params.projectId)) {
          switchProject(p);
          break;
        }
      }
    }
  }, [projects]);

  const onEditProject = p => setState({ ...state, editProject: p, showProjectDialog: true });
  const onOpenAddProjectDialog = () => setState({ ...state, showAddProjectDialog: true });
  const onCloseAddProjectDialog = () => setState({ ...state, showAddProjectDialog: false });
  const onCloseUpdateProjectDialog = () => setState({ ...state, showProjectDialog: false });

  const onAddProject = value => {
    setState({ ...state, showAddProjectDialog: false });
    createProject(value);
  };

  const onUpdateProject = newValue => {
    setState({ ...state, editProject: null, showProjectDialog: false });
    updateProject(newValue);
  };

  const projectsInactive = projects.filter(
    p => p.groupId === selectedProjectGroup.id && p.isActive === PROJECT_ACTIVE_STATUS[0].value
  );
  const projectsActive = projects.filter(
    p => p.groupId === selectedProjectGroup.id && p.isActive === PROJECT_ACTIVE_STATUS[1].value
  );
  const projectsFinished = projects.filter(
    p => p.groupId === selectedProjectGroup.id && p.isActive === PROJECT_ACTIVE_STATUS[2].value
  );

  const allProject = [projectsActive, projectsInactive, projectsFinished];

  return (
    <NamespacesConsumer>
      {t => (
        <>
          <div className="d-flex justify-content-between align-items-center margin-bottom-30px">
            <AppText type={'title-header'}>{t('projects')}</AppText>
            {isAllowPermission(appPermissions, PERM_ADD_PROJECT) && (
              <Button type={'orange'} onClick={onOpenAddProjectDialog}>
                <i className={'fa fa-plus margin-right-5px'} /> {t('add_project')}
              </Button>
            )}
          </div>
          {calculateLength(allProject) &&
            allProject.map((item, index) => (
              <>
                {calculateLength(item) && index ? <hr /> : null}
                <div className="left">
                  <ProjectsByStatus
                    roles={roles}
                    history={history}
                    getUsers={getUsers}
                    projectsByStatus={item}
                    switchProject={switchProject}
                    onEditProject={onEditProject}
                    appPermissions={appPermissions}
                    selectedProject={selectedProject}
                    clearUserInProject={clearUserInProject}
                    clearSwitchProject={clearSwitchProject}
                    selectedProjectGroup={selectedProjectGroup}
                    setSelectedSettingTab={setSelectedSettingTab}
                    getProjectEnvironments={getProjectEnvironments}
                  />
                </div>
              </>
            ))}
          <AddProjectDialog
            isOpen={showAddProjectDialog}
            value={{ projectGroupId: selectedProjectGroup.id }}
            onClose={onCloseAddProjectDialog}
            onOK={onAddProject}
          />

          <UpdateProjectDialog
            isOpen={showProjectDialog}
            value={editProject}
            onClose={onCloseUpdateProjectDialog}
            onOK={onUpdateProject}
            onDelete={removeProjects}
          />
        </>
      )}
    </NamespacesConsumer>
  );
};

ProjectList.propTypes = {
  appPermissions: array,
  roles: array,
  projects: array,
  selectedProjectGroup: func,
  selectedProject: object,
  match: object,
  history: object,
  getProjectEnvironments: func,
  switchProject: func,
  getProjects: func,
  getMyProjects: func,
  createProject: func,
  getWorkflows: func,
  clearSwitchProject: func,
  clearUserInProject: func,
  getUsers: func,
  setSelectedSettingTab: func,
  updateProject: func,
  removeProjects: func
};

export default memo(ProjectList);
