import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo } from 'react';
import { array, object, func } from 'prop-types';
import { isAllowPermission, calculateLength, promiseDelayImport } from '../../../../../08-helpers/common';
import {
  LIST_USER_INTO_PROJECT,
  // PERM_LIST_PROJECT_ENVIRONMENTS,
  PERM_UPDATE_PROJECT
} from '../../../../../07-constants/app-permission';

const ProjectBullet = LazyComponent(
  promiseDelayImport(import('../ProjectBullet')),
  <Skeleton height={37} width={150} />
);

const ProjectsByStatus = ({
  appPermissions,
  selectedProject,
  // getProjectEnvironments,
  clearSwitchProject,
  clearUserInProject,
  switchProject,
  getUsers,
  setSelectedSettingTab,
  roles,
  onEditProject,
  selectedProjectGroup,
  projectsByStatus,
  history
}) => {
  const checkEditPermission = isAllowPermission(
    appPermissions,
    PERM_UPDATE_PROJECT,
    selectedProject && selectedProject.id
  );

  const changeProjectRoute = projectId => {
    history.push(`/projects/${selectedProjectGroup.id}/${projectId}`);
  };

  const onSelectProject = p => {
    // if (isAllowPermission(appPermissions, PERM_LIST_PROJECT_ENVIRONMENTS, selectedProject && selectedProject.id)) {
    //   getProjectEnvironments({ projectId: p.id });
    // }
    clearSwitchProject();
    clearUserInProject();
    switchProject(p);
    if (isAllowPermission(appPermissions, LIST_USER_INTO_PROJECT, p.id)) {
      getUsers({ projectId: p.id });
    }
    setSelectedSettingTab(5);
    changeProjectRoute(p.id);
  };

  return calculateLength(projectsByStatus)
    ? projectsByStatus.map((p, index) => (
        <div
          key={index}
          className="d-inline-flex margin-bottom-5px margin-right-10px min-width-120px"
          onClick={() => onSelectProject(p)}
        >
          <ProjectBullet
            key={index}
            value={p}
            roles={roles}
            editable={checkEditPermission}
            onEdit={() => onEditProject(p)}
            selected={p === selectedProject}
          />
        </div>
      ))
    : null;
};

ProjectsByStatus.propTypes = {
  appPermissions: array,
  selectedProject: object,
  getProjectEnvironments: func,
  clearSwitchProject: func,
  clearUserInProject: func,
  switchProject: func,
  getUsers: func,
  setSelectedSettingTab: func,
  roles: array,
  onEditProject: func,
  selectedProjectGroup: object,
  history: object,
  projectsByStatus: array
};

export default memo(ProjectsByStatus);
