import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect } from 'react';
import { array, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Container, Row, Col } from 'reactstrap';
import { calculateLength, isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import {
  PERM_UPDATE_GROUP,
  PERM_CREATE_GROUP,
  PERM_LIST_GROUPS,
  PERM_LIST_MY_GROUPS,
  PERM_TOGGLE_NOTIFICATION_ENABLE_GROUP
} from '../../../../../07-constants/app-permission';

const SelectStateBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectStateBox')),
  <Skeleton height={38} />
);
const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <Skeleton width={159.7} height={34} />
);
const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={130} />
);
const ProjectGroupDialog = LazyComponent(() => import('../../../../dialog/project-group/project-group'));
const ProjectGroupByStatus = LazyComponent(() => import('./ProjectGroupByStatus'));

const ProjectGroupList = ({
  projectGroups,
  selectedProjectGroup,
  match,
  history,
  switchProjectGroup,
  getGroups,
  getMyGroups,
  addGroup,
  updateGroup,
  clearSwitchGroupProject,
  clearUserInProject,
  deleteGroup,
  // getProjects,
  switchProject,
  appPermissions,
  listProjects,
  toggleNotificationGroup,
  location,
  getUsersSubscriptionProjectsAlert
}) => {
  const [state, setState] = useState({
    showProjectGroupDialog: false,
    editProjectGroup: null
  });

  const { showProjectGroupDialog, editProjectGroup } = state;

  const reloadGroups = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_GROUPS)) {
      getGroups();
    } else if (isAllowPermission(appPermissions, PERM_LIST_MY_GROUPS)) {
      getMyGroups();
    }
  };

  useEffect(() => {
    reloadGroups();
  }, [appPermissions]);

  useEffect(() => {
    if (projectGroups) {
      // getProjects();
      if (Number(match.params.groupId)) {
        for (const g of projectGroups) {
          if (g && g.id === Number(match.params.groupId)) {
            switchProjectGroup(g);
            break;
          }
        }
      }
    }
  }, [projectGroups]);

  const getListProjectOption = data =>
    calculateLength(data) ? data.map(item => ({ ...item, value: item.id, label: item.name })) : [];

  const allGroup = [projectGroups.filter(g => !!g.isActive), projectGroups.filter(g => !g.isActive)];
  const projectOptions = getListProjectOption(listProjects);
  const checkEditGroupProject = [PERM_TOGGLE_NOTIFICATION_ENABLE_GROUP, PERM_UPDATE_GROUP].some(permissionName =>
    isAllowPermission(appPermissions, permissionName)
  );

  const onToggleGroupAlert = (groupId, isEnabled) => toggleNotificationGroup(groupId, isEnabled);
  const openGroupDialog = () => setState({ editProjectGroup: null, showProjectGroupDialog: true });
  const onCloseDialog = () => setState({ ...state, showProjectGroupDialog: false });
  const onEditGroup = g => setState({ editProjectGroup: g, showProjectGroupDialog: true });

  const onFilterProject = data => {
    const projectGroupSelected = projectGroups.find(p => p.id === (data && Number(data.groupId)));
    const projectSelected = listProjects.find(p => p.id === (data && Number(data.id)));
    projectGroupSelected && switchProjectGroup(projectGroupSelected);
    if (projectSelected && projectGroupSelected) {
      switchProject(projectSelected);
      getUsersSubscriptionProjectsAlert({ projectId: projectSelected.id });
      if (location.url !== '/projects') {
        history.replace(`/projects/${projectGroupSelected.id}/${projectSelected.id}/info`);
      }
    } else {
      clearSwitchGroupProject();
    }
  };

  const onSelectGroup = g => {
    clearSwitchGroupProject();
    clearUserInProject();
    switchProjectGroup(g);
    history.push(`/projects/${g.id}`);
  };

  const onDeleteGroup = value => {
    deleteGroup(value);
    setState({ showProjectGroupDialog: false });
  };

  const onOkDialog = (editMode, newValue) => {
    setState({ editProjectGroup: null, showProjectGroupDialog: false });
    if (editMode) {
      updateGroup(newValue);
    } else {
      addGroup(newValue);
    }
  };

  return (
    <NamespacesConsumer>
      {t => (
        <Container fluid className="px-0">
          <div className="margin-bottom-40px">
            <AppText className="margin-bottom-15px" type={'title-header'}>
              {t('project_groups')}
            </AppText>
            <Row className="justify-content-end align-items-center margin-bottom-15px">
              <Col xs={12} sm={6} md={4} className="mb-3">
                <SelectStateBox
                  placeholder="select_project"
                  classes="ml-auto mr-2"
                  options={projectOptions}
                  onChange={onFilterProject}
                />
              </Col>
              <Col xs={12} sm={6} md={4} className="mb-3 col-auto">
                {isAllowPermission(appPermissions, PERM_CREATE_GROUP) && (
                  <Button onClick={openGroupDialog} type="orange">
                    <i className="fa fa-plus margin-right-5px" />
                    {t('add_project_group')}
                  </Button>
                )}
              </Col>
            </Row>
            {calculateLength(allGroup) &&
              allGroup.map((item, index) => (
                <>
                  {calculateLength(item) && index ? <hr /> : null}
                  <ProjectGroupByStatus
                    appPermissions={appPermissions}
                    groupProject={item}
                    checkUpdatePermission={checkEditGroupProject}
                    onEditGroup={onEditGroup}
                    onSelectGroup={onSelectGroup}
                    selectedProjectGroup={selectedProjectGroup}
                    onToggleGroupAlert={onToggleGroupAlert}
                  />
                </>
              ))}
          </div>
          <ProjectGroupDialog
            isOpen={showProjectGroupDialog}
            editMode={!!editProjectGroup}
            value={editProjectGroup}
            onRemove={onDeleteGroup}
            onClose={onCloseDialog}
            onOK={onOkDialog}
          />
        </Container>
      )}
    </NamespacesConsumer>
  );
};
ProjectGroupList.propTypes = {
  roles: array,
  projectGroups: array,
  selectedProjectGroup: func,
  match: object,
  history: object,
  location: object,
  switchProjectGroup: func,
  getGroups: func,
  getMyGroups: func,
  addGroup: func,
  updateGroup: func,
  clearSwitchGroupProject: func,
  clearUserInProject: func,
  deleteGroup: func,
  getProjects: func,
  switchProject: func,
  appPermissions: array,
  listProjects: array,
  toggleNotificationGroup: func,
  getUsersSubscriptionProjectsAlert: func
};

export default memo(ProjectGroupList);
