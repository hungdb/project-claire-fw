import Switch from 'react-switch';
import FontAwesomeIcon from '../../../../../03-components/FontAwesomeIcon';
import React, { memo, useState } from 'react';
import { array, bool, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { PERM_TOGGLE_NOTIFICATION_ENABLE_GROUP, PERM_UPDATE_GROUP } from '../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../08-helpers/common';

const ProjectGroupBullet = ({
  selected,
  editable,
  value,
  onClick,
  onEdit,
  onToggleAlert,
  defaultValue,
  appPermissions
}) => {
  const [state, setState] = useState({
    dropdownOpen: false,
    droptionPosition: 0
  });

  const { dropdownOpen, droptionPosition } = state;

  const toggle = () => setState({ ...state, dropdownOpen: !state.dropdownOpen });
  const toggleAlert = checked => onToggleAlert(checked);

  const calcDropdownPosition = e => {
    const btnWidth = document.getElementsByClassName('project-group-bullet selected')[0].clientWidth;
    const btnSpaceToViewPort = window.innerWidth - e.touches[0].clientX;
    const dropDownWith = 230;
    if (btnWidth + btnSpaceToViewPort < dropDownWith)
      setState({ ...state, droptionPosition: dropDownWith - (btnWidth + btnSpaceToViewPort) });
    else setState({ ...state, droptionPosition: 0 });
  };

  return (
    <NamespacesConsumer>
      {t => (
        <Dropdown
          className={`
            ${selected ? 'selected' : ''}
            ${value && value.isActive ? '' : 'closed'}
            flex-grow-1 project-group-bullet no-text-select cursor-pointer`}
          direction="right"
          toggle={toggle}
          isOpen={dropdownOpen}
        >
          <span onClick={onClick} className="name">
            {value.name || ''}
          </span>
          {selected && editable && (
            <DropdownToggle>
              <span className="fa-icon" onTouchStart={calcDropdownPosition}>
                <FontAwesomeIcon classes="fa fa-list-ul font-size-20 white-color" />
              </span>
            </DropdownToggle>
          )}
          <DropdownMenu style={{ marginLeft: `-${droptionPosition}px` }}>
            {isAllowPermission(appPermissions, PERM_UPDATE_GROUP) && (
              <DropdownItem onClick={() => onEdit && onEdit()}>
                <div className={'d-flex cursor-pointer'}>
                  <i className="fa fa-pencil font-size-20 color-base" />
                  <p className="font-size-14 ml-17 mb-0 default-text-color">{t('edit_project_group')}</p>
                </div>
              </DropdownItem>
            )}
            {isAllowPermission(appPermissions, PERM_TOGGLE_NOTIFICATION_ENABLE_GROUP) && (
              <DropdownItem toggle={false} className="pt-3 pb-2 d-flex">
                <Switch
                  width={45}
                  height={20}
                  onColor="#0ba1ee"
                  checked={!!defaultValue}
                  onChange={toggleAlert}
                  className="align-middle ml-auto pl-2 pr-1"
                />
                <p className="font-size-14 ml-1 mb-0 default-text-color">{t('toggle_group_alert')}</p>
              </DropdownItem>
            )}
          </DropdownMenu>
        </Dropdown>
      )}
    </NamespacesConsumer>
  );
};

ProjectGroupBullet.propTypes = {
  selected: bool,
  editable: bool,
  value: object,
  onClick: func,
  onEdit: func,
  onToggleAlert: func,
  defaultValue: bool,
  appPermissions: array
};

export default memo(ProjectGroupBullet);
