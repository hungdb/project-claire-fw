import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo } from 'react';
import { calculateLength, promiseDelayImport } from '../../../../../08-helpers/common';
import { array, bool, func } from 'prop-types';

const ProjectGroupBullet = LazyComponent(
  promiseDelayImport(import('./ProjectGroupBullet'), 1500),
  <Skeleton height={37} width={150} />
);

const ProjectGroupByStatus = ({
  appPermissions,
  groupProject,
  checkUpdatePermission,
  onEditGroup,
  onToggleGroupAlert,
  onSelectGroup,
  selectedProjectGroup
}) =>
  calculateLength(groupProject) > 0 &&
  groupProject.map((g, index) => (
    <div key={index} className="d-inline-flex margin-right-10px margin-bottom-10px min-width-120px group-project-btn">
      <ProjectGroupBullet
        appPermissions={appPermissions}
        key={index}
        value={g}
        editable={checkUpdatePermission}
        onEdit={() => onEditGroup(g)}
        defaultValue={g.isNotificationEnable}
        onToggleAlert={checked => onToggleGroupAlert(g.id, checked)}
        selected={g === selectedProjectGroup}
        onClick={() => onSelectGroup(g)}
      />
    </div>
  ));

ProjectGroupByStatus.propTypes = {
  appPermissions: array,
  groupProject: array,
  checkUpdatePermission: bool,
  onEditGroup: func,
  onToggleGroupAlert: func,
  onSelectGroup: func,
  selectedProjectGroup: func
};

export default memo(ProjectGroupByStatus);
