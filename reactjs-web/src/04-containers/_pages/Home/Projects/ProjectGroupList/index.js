import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import ProjectGroupList from './ProjectGroupList';
import {
  clearSwitchGroupProject,
  getProjects,
  switchProject,
  setSelectedSettingTab,
  getUsersSubscriptionProjectsAlert
} from '../../../../../01-actions/project';
import {
  switchProjectGroup,
  getGroups,
  addGroup,
  updateGroup,
  deleteGroup,
  getMyGroups,
  toggleNotificationGroup
} from '../../../../../01-actions/group';
import { clearUserInProject } from '../../../../../01-actions/userInProject';

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  projectGroups: state.group.projectGroups,
  selectedProjectGroup: state.group.selectedProjectGroup,
  listProjects: state.project.projects
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchProjectGroup,
      getGroups,
      addGroup,
      updateGroup,
      clearSwitchGroupProject,
      clearUserInProject,
      deleteGroup,
      getMyGroups,
      getProjects,
      switchProject,
      setSelectedSettingTab,
      toggleNotificationGroup,
      getUsersSubscriptionProjectsAlert
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProjectGroupList)
);
