import cx from 'classnames';
import { bool, func, object } from 'prop-types';
import React, { memo } from 'react';

function ProjectBullet({ selected, editable, value, onClick, onEdit }) {
  return (
    <div
      className={cx(
        'flex-grow-1 project-group-bullet no-text-select cursor-pointer',
        selected && 'selected',
        !(value && value.isActive) && 'closed',
        value && value.isActive === 2 && 'finished'
      )}
      onClick={() => {
        onClick && onClick();
      }}
    >
      <span className="name">{value && value.name}</span>
      {onEdit && selected && editable && (
        <span title="edit" onClick={() => onEdit()} className="fa-icon">
          <i className="fa fa-pencil font-size-20 white-color" />
        </span>
      )}
    </div>
  );
}

ProjectBullet.propTypes = {
  selected: bool,
  value: object,
  onClick: func,
  onEdit: func,
  editable: bool
};

export default memo(ProjectBullet);
