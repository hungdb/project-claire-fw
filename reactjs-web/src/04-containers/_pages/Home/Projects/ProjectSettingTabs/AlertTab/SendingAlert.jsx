import './style.css';
import React, { memo } from 'react';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import { func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const Multicheck = LazyComponent(() => import('../../../../../../03-components/Multicheck'));

const SendingAlert = ({
  selectedProject,
  updateProjectAlertEnable,
  envSubscriptions,
  updateProjectAlertSubscription
}) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'setting-tab'}>
          <div className={'margin-bottom-15px d-flex flex-wrap-wrap justify-content-between align-items-center'}>
            <div className={'min-width-120px'}>
              <AppText type={'content-header'} className={'margin-bottom-10px'}>
                {t('project_notification_enabling')}
              </AppText>
            </div>
            <Multicheck
              className={'flex-grow-1 flex-wrap-wrap'}
              items={[{ label: t('enabled'), checked: selectedProject.isNotificationEnable }]}
              onChange={(index, checked) => {
                updateProjectAlertEnable({
                  projectId: selectedProject.id,
                  isEnabled: checked
                });
              }}
            />
          </div>
          <div className={'margin-bottom-15px'}>
            <AppText type={'content-header'} className={'margin-bottom-10px'}>
              {t('server_status_alert_subscription')}
            </AppText>
            <div className="margin-bottom-5px">
              <div className={'d-flex flex-wrap-wrap margin-bottom-5px justify-content-between multicheck-container'}>
                <AppText className={'min-width-120px'} type={'content-sub-header'}>
                  {t('environments')}
                </AppText>
                <Multicheck
                  className={'flex-grow-1 flex-wrap-wrap'}
                  items={envSubscriptions}
                  onChange={(index, checked) => {
                    const environmentIds = checked
                      ? envSubscriptions
                          .filter(sub => sub.checked)
                          .map(sub => sub.value)
                          .concat([envSubscriptions[index].value])
                      : envSubscriptions
                          .filter(sub => sub.checked && sub.value !== envSubscriptions[index].value)
                          .map(sub => sub.value);
                    updateProjectAlertSubscription({
                      projectId: selectedProject.id,
                      environmentIds: environmentIds
                    });
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

SendingAlert.propTypes = {
  selectedProject: object,
  updateProjectAlertEnable: func,
  envSubscriptions: object,
  updateProjectAlertSubscription: func
};

export default memo(SendingAlert);
