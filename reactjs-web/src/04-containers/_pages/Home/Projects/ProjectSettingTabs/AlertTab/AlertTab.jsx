import './style.css';
import React, { memo, useState, useEffect } from 'react';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import { array, func, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, isAllowPermission } from '../../../../../../08-helpers/common';
import { SUBSCRIPTION_METHODS } from '../../../../../../05-utils/commonData';
import {
  GET_LOGS_OF_PROJECT,
  GET_PROJECT_ALERT_SUBSCRIPTION,
  LIST_PROJECT_ENVIRONMENTS,
  PERM_CALL_METHOD_PROJECT_ALERT_SUBSCRIPTION,
  PERM_GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT,
  PERM_TOGGLE_NOTIFICATION_ENABLE,
  PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION
} from '../../../../../../07-constants/app-permission';
import {
  GET_LOGS_OF_PROJECT_PROGRESS,
  GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT
} from '../../../../../../07-constants/project';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const Multicheck = LazyComponent(() => import('../../../../../../03-components/Multicheck'));
const UserSubscribeTable = LazyComponent(() => import('../Table/UserSubscribeTable'));
// const SendingAlert = LazyComponent(() => import('./SendingAlert'));

const ProjectAlertTab = ({
  appPermissions,
  selectedProjectEnvironments,
  logsOfProject,
  getUserAlertSubscription,
  getLogOfProject,
  getProjectEnvironments,
  selectedProject,
  selectedProjectSubscription,
  updateProjectAlertEnable,
  updateProjectAlertSubscription,
  // clearLogHistory,
  status,
  // usersSubscriptionAlert,
  getUsersSubscriptionProjectsAlert
}) => {
  const [typeTerminal, setTypeTerminal] = useState(0);

  const reloadSubscription = selectedProject => {
    if (selectedProject) {
      if (isAllowPermission(appPermissions, GET_PROJECT_ALERT_SUBSCRIPTION, selectedProject.id)) {
        getUserAlertSubscription({ projectId: selectedProject.id });
      }
      if (isAllowPermission(appPermissions, LIST_PROJECT_ENVIRONMENTS, selectedProject.id)) {
        getProjectEnvironments({ projectId: selectedProject.id });
      }
    }
  };

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, GET_LOGS_OF_PROJECT, selectedProject.id)) {
      getLogOfProject({ projectId: selectedProject.id });
    }
    if (isAllowPermission(appPermissions, PERM_GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT, selectedProject.id)) {
      getUsersSubscriptionProjectsAlert({ projectId: selectedProject.id });
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  useEffect(() => {
    reloadSubscription(selectedProject);
    if (isAllowPermission(appPermissions, GET_LOGS_OF_PROJECT, selectedProject.id)) {
      getLogOfProject({ projectId: selectedProject.id });
    }
    if (isAllowPermission(appPermissions, LIST_PROJECT_ENVIRONMENTS, selectedProject.id)) {
      getProjectEnvironments({ projectId: selectedProject.id });
    }
    if (isAllowPermission(appPermissions, GET_PROJECT_ALERT_SUBSCRIPTION, selectedProject.id)) {
      getUserAlertSubscription({ projectId: selectedProject.id });
    }
  }, [selectedProject]);

  const refreshLogHistory = () => {
    selectedProject && getLogOfProject({ projectId: selectedProject.id });
  };

  const refreshUsersSubscribe = () => {
    selectedProject && getUsersSubscriptionProjectsAlert({ projectId: selectedProject.id });
  };

  const envSubscriptions =
    selectedProjectEnvironments &&
    selectedProjectEnvironments.map(e => ({
      value: e.id,
      label: e.name
    }));

  const methodSubscriptions = SUBSCRIPTION_METHODS.map(method => {
    const isCall = method.label === SUBSCRIPTION_METHODS[2].label;
    const isPermission = isAllowPermission(
      appPermissions,
      PERM_CALL_METHOD_PROJECT_ALERT_SUBSCRIPTION,
      selectedProject.id
    );
    if (!isCall || (isCall && isPermission)) {
      return { ...method };
    }
    return null;
  }).filter(Boolean);

  if (selectedProjectSubscription) {
    selectedProjectSubscription.environmentIds.forEach(id => {
      // eslint-disable-next-line
      envSubscriptions.some(sub => {
        if (sub.value === id) {
          sub.checked = true;
          return true;
        }
      });
    });

    selectedProjectSubscription.methodIds.forEach(med => {
      // eslint-disable-next-line
      methodSubscriptions.some(sub => {
        if (sub.value === med) {
          sub.checked = true;
          return true;
        }
      });
    });
  }

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'setting-tab'}>
            <div className={'margin-bottom-15px d-flex flex-wrap-wrap justify-content-between align-items-center'}>
              <div className={'min-width-120px'}>
                <AppText type={'content-header'} className={'margin-bottom-10px'}>
                  {t('project_notification_enabling')}
                </AppText>
              </div>
              {isAllowPermission(appPermissions, PERM_TOGGLE_NOTIFICATION_ENABLE, selectedProject.id) ? (
                <Multicheck
                  className={'flex-grow-1 flex-wrap-wrap'}
                  items={[{ label: t('enabled'), checked: selectedProject.isNotificationEnable }]}
                  onChange={(index, checked) => {
                    updateProjectAlertEnable({
                      projectId: selectedProject.id,
                      isEnabled: checked
                    });
                  }}
                />
              ) : (
                <Multicheck
                  className={'flex-grow-1 flex-wrap-wrap'}
                  items={[{ label: t('enabled'), checked: selectedProject.isNotificationEnable }]}
                  disable={true}
                />
              )}
            </div>
            <div className={'margin-bottom-15px'}>
              <AppText type={'content-header'} className={'margin-bottom-10px'}>
                {t('server_status_alert_subscription')}
              </AppText>
              <div className="margin-bottom-5px">
                <div className={'d-flex flex-wrap-wrap margin-bottom-5px justify-content-between multicheck-container'}>
                  <AppText className={'min-width-120px'} type={'content-sub-header'}>
                    {t('environments')}
                  </AppText>
                  {isAllowPermission(appPermissions, PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION, selectedProject.id) ? (
                    <Multicheck
                      className={'flex-grow-1 flex-wrap-wrap'}
                      items={envSubscriptions}
                      onChange={(index, checked) => {
                        const environmentIds = checked
                          ? envSubscriptions
                              .filter(sub => sub.checked)
                              .map(sub => sub.value)
                              .concat([envSubscriptions[index].value])
                          : envSubscriptions
                              .filter(sub => sub.checked && sub.value !== envSubscriptions[index].value)
                              .map(sub => sub.value);
                        updateProjectAlertSubscription({
                          projectId: selectedProject.id,
                          environmentIds: environmentIds
                        });
                      }}
                    />
                  ) : (
                    <Multicheck className={'flex-grow-1 flex-wrap-wrap'} items={envSubscriptions} disable={true} />
                  )}
                </div>

                <div className={'d-flex flex-wrap-wrap margin-bottom-5px methods-area'}>
                  <AppText className={'min-width-120px'} type={'content-sub-header'}>
                    {t('methods')}
                  </AppText>
                  {isAllowPermission(appPermissions, PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION, selectedProject.id) ? (
                    <Multicheck
                      className={'flex-grow-1 flex-wrap-wrap'}
                      items={methodSubscriptions}
                      onChange={(index, checked) => {
                        const methodIds = checked
                          ? methodSubscriptions
                              .filter(sub => sub.checked)
                              .map(sub => sub.value)
                              .concat([methodSubscriptions[index].value])
                          : methodSubscriptions
                              .filter(sub => sub.checked && sub.value !== methodSubscriptions[index].value)
                              .map(sub => sub.value);
                        updateProjectAlertSubscription({
                          projectId: selectedProject.id,
                          methodIds
                        });
                      }}
                    />
                  ) : (
                    <Multicheck className={'flex-grow-1 flex-wrap-wrap'} items={methodSubscriptions} disable={true} />
                  )}
                </div>
              </div>
            </div>
          </div>

          {isAllowPermission(appPermissions, GET_LOGS_OF_PROJECT, selectedProject.id) && (
            <div className={'log-history setting-tab margin-top-15px'}>
              <div className={'min-width-120px'}>
                <AppText type={'content-header'} className={'margin-bottom-10px'}>
                  {t('log_history')}
                </AppText>
                {!typeTerminal ? (
                  <i className="fa fa-square option" onClick={() => setTypeTerminal(1)} />
                ) : (
                  <i className="fa fa-square-o option" onClick={() => setTypeTerminal(0)} />
                )}

                <i
                  className="fa fa-refresh option margin-left-10px margin-right-10px"
                  onClick={() => refreshLogHistory()}
                />

                {status === GET_LOGS_OF_PROJECT_PROGRESS && <i>{t('loading')}</i>}
              </div>
              <div className={`${typeTerminal && 'log-terminal-black'} log-terminal`}>
                {logsOfProject &&
                  logsOfProject.map((log, index) => {
                    const logContent = log.value.replace('\n', '');
                    return <div key={index}>{logContent}</div>;
                  })}
                {logsOfProject && calculateLength(logsOfProject) === 0 && (
                  <div className={'text-center'}>
                    <i>{t('not_found')}</i>
                  </div>
                )}
              </div>
            </div>
          )}

          {isAllowPermission(appPermissions, PERM_GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT, selectedProject.id) && (
            <div className={'log-history setting-tab margin-top-15px'}>
              <div className={'min-width-120px'}>
                <AppText type={'content-header'} className={'margin-bottom-10px'}>
                  {t('users_subscribe_alert')}
                </AppText>

                <i
                  className="fa fa-refresh option margin-left-10px margin-right-10px"
                  onClick={() => {
                    refreshUsersSubscribe();
                  }}
                />
                {status === GET_USERS_ALERT_SUBSCRIPTION_OF_PROJECT && <i>{t('loading')}</i>}

                <div className="d-flex">
                  <UserSubscribeTable />
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectAlertTab.propTypes = {
  appPermissions: array,
  roles: array,
  selectedProjectEnvironments: array,
  logsOfProject: array,
  getUserAlertSubscription: func,
  getLogOfProject: func,
  getProjectEnvironments: func,
  selectedProject: object,
  selectedProjectSubscription: object,
  updateProjectAlertEnable: func,
  updateProjectAlertSubscription: func,
  // clearLogHistory: func,
  status: string,
  // usersSubscriptionAlert: array,
  getUsersSubscriptionProjectsAlert: func
};

export default memo(ProjectAlertTab);
