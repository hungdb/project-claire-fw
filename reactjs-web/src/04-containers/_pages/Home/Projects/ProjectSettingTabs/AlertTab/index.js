import ProjectAlertTab from './AlertTab';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  clearLogHistory,
  getLogOfProject,
  getProjectEnvironments,
  getUserAlertSubscription,
  getUsersSubscriptionProjectsAlert,
  updateProjectAlertEnable,
  updateProjectAlertSubscription
} from '../../../../../../01-actions/project';

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,

    selectedProject: store.project.selectedProject,
    selectedProjectEnvironments: store.project.selectedProjectEnvironments,
    selectedProjectSubscription: store.project.projectAlertSubscription,
    logsOfProject: store.project.logsOfProject,
    status: store.project.status,
    usersSubscriptionAlert: store.project.usersSubscriptionAlert
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getProjectEnvironments,
      getUserAlertSubscription,
      updateProjectAlertSubscription,
      updateProjectAlertEnable,
      getLogOfProject,
      clearLogHistory,
      getUsersSubscriptionProjectsAlert
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectAlertTab);
