import styles from './style';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo, useMemo } from 'react';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col } from 'reactstrap';
import { calculateLength, getArrayMember, promiseDelayImport } from '../../../../../../08-helpers/common';

const UserAvatar = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/UserAvatar')),
  <Skeleton circle={true} width={30} height={30} />
);

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));

const ProjectMember = ({ infoProject }) => {
  const arrayUsers = (infoProject && infoProject.users) || [];
  const arrayMemberMemo = useMemo(() => getArrayMember(arrayUsers), [arrayUsers]);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('information_project.member').toUpperCase()}</AppText>
          </div>
          <Row className={'align-items-baseline margin-bottom-40px view-member'} style={styles.viewMember}>
            {calculateLength(arrayMemberMemo) ? (
              arrayMemberMemo.map((user, index) => (
                <Col id={index} style={styles.viewAvatar} key={index}>
                  <UserAvatar
                    className="avatar"
                    id={user && user.userId}
                    key={user && user.userId}
                    user={user && user.User}
                    src={`/profile/${user && user.userId}`}
                  />
                  {user &&
                    user.role &&
                    calculateLength(user.role) &&
                    user.role.map((item, index) => {
                      return (
                        <AppText key={index} className={'text-role'}>
                          {item && item.name && item.name.toUpperCase()}
                        </AppText>
                      );
                    })}
                </Col>
              ))
            ) : (
              <Col>
                <span className={'font-size-15 text-muted'}>{t('not_found')}</span>
              </Col>
            )}
          </Row>
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectMember.propTypes = {
  infoProject: object
};

export default memo(ProjectMember);
