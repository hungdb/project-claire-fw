import styles from './style';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const Multicheck = LazyComponent(() => import('../../../../../../03-components/Multicheck'));

const ProjectGroup = ({ infoProject }) => {
  const group = infoProject && infoProject.Group;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('information_project.group').toUpperCase()}</AppText>
          </div>
          {group ? (
            <div className={'margin-bottom-40px view-group'}>
              <AppText className={'text-title d-inline-block'}>{t('information_project.group_name') + ': '}</AppText>
              <AppText className={'text-group d-inline-block margin-right-30px'}>{group.name}</AppText>
              {group.isActive && (
                <Multicheck
                  items={[{ checked: group.isActive, label: t('information_project.is_active') }]}
                  className="check-active"
                />
              )}
            </div>
          ) : (
            <div className={'text-center'}>
              <span className={'font-size-15 text-muted'}>{t('not_found')}</span>
            </div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectGroup.propTypes = {
  infoProject: object
};

export default memo(ProjectGroup);
