import styles from './style';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { array, func, number, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Badge, Row, Col } from 'reactstrap';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const ProjectWorkFlow = ({ infoProject }) => {
  const workFlow = infoProject && infoProject.Workflow;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('information_project.work_flow').toUpperCase()}</AppText>
          </div>
          {workFlow ? (
            <Row className={'margin-bottom-40px view-group'}>
              <Col xs={12} sm={6} md={4}>
                <AppText className={'text-title d-inline-block'}>{t('information_project.work_flow_name')}</AppText>
                <AppText className={'text-group d-inline-block'}>{workFlow.name}</AppText>
              </Col>
              <Col xs={12} sm={6} md={4}>
                <AppText className={'text-title d-inline-block'}>
                  {t('information_project.allow_parallel_task')}
                </AppText>
                <Badge color={workFlow.allowParallelTask ? 'primary' : 'secondary'} className={'badge-text'}>
                  {workFlow.allowParallelTask ? t('information_project.allow') : t('information_project.not_allow')}
                </Badge>
              </Col>
            </Row>
          ) : (
            <Col>
              <span className={'font-size-15 text-muted'}>{t('not_found')}</span>
            </Col>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectWorkFlow.propTypes = {
  appPermissions: array,
  projectId: number,
  roles: array,
  status: string,
  infoProject: object,
  getDetailProject: func
};

export default memo(ProjectWorkFlow);
