import './stylesInfoProject.css';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../../03-components/SpinLoader';
import React, { memo, useEffect } from 'react';
import { array, func, number, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { PERM_GET_DETAIL_PROJECT } from '../../../../../../07-constants/app-permission';
import { isAllowPermission, promiseDelayImport } from '../../../../../../08-helpers/common';

const SprintDataTable = LazyComponent(promiseDelayImport(import('./SprintDataTable')));
const ProjectDescription = LazyComponent(promiseDelayImport(import('./ProjectDescription')));
const ProjectMember = LazyComponent(promiseDelayImport(import('./ProjectMember')));
const ProjectWorkFlow = LazyComponent(promiseDelayImport(import('./ProjectWorkFlow')));
const ProjectGroup = LazyComponent(promiseDelayImport(import('./ProjectGroup')));
const ProjectPermissionScheme = LazyComponent(promiseDelayImport(import('./ProjectPermissionScheme')), <SpinLoader />);

const InfoProjectTab = ({ appPermissions, projectId, infoProject, getDetailProject }) => {
  useEffect(() => {
    if (isAllowPermission(appPermissions, PERM_GET_DETAIL_PROJECT, projectId)) {
      getDetailProject(projectId);
    }
  }, [projectId, appPermissions]);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          {isAllowPermission(appPermissions, PERM_GET_DETAIL_PROJECT, projectId) && (
            <div className={'setting-tab'}>
              <ProjectDescription infoProject={infoProject} />

              <ProjectMember infoProject={infoProject} />

              <ProjectGroup infoProject={infoProject} />

              <ProjectWorkFlow infoProject={infoProject} />

              <ProjectPermissionScheme infoProject={infoProject} />

              <SprintDataTable infoProject={infoProject} />
            </div>
          )}
        </>
      )}
    </NamespacesConsumer>
  );
};

InfoProjectTab.propTypes = {
  appPermissions: array,
  projectId: number,
  roles: array,
  status: string,
  infoProject: object,
  getDetailProject: func
};

export default memo(InfoProjectTab);
