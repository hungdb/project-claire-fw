import styles from './style';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Col } from 'reactstrap';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));

const ProjectPermissionScheme = ({ infoProject }) => {
  const permissionScheme = infoProject && infoProject.PermissionScheme;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('information_project.permission_scheme').toUpperCase()}</AppText>
          </div>
          {permissionScheme ? (
            <div className={'margin-bottom-40px view-group'}>
              <AppText className={'text-title d-inline-block'}>
                {t('information_project.permission_scheme_name') + ': '}
              </AppText>
              <AppText className={'text-group d-inline-block'}>{permissionScheme.name}</AppText>
            </div>
          ) : (
            <Col>
              <span className={'font-size-15 text-muted'}>{t('not_found')}</span>
            </Col>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectPermissionScheme.propTypes = {
  infoProject: object
};

export default memo(ProjectPermissionScheme);
