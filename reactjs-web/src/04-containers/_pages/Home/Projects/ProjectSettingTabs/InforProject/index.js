import InfoProjectTab from './InfoProjectTab';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getDetailProject } from '../../../../../../01-actions/project';

const mapStateToProps = state => {
  return {
    appPermissions: state.auth.permissions,
    roles: state.auth.roles,
    infoProject: state.project.infoProject,
    status: state.project.status,
    projectId: state.project.selectedProject.id
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getDetailProject
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoProjectTab);
