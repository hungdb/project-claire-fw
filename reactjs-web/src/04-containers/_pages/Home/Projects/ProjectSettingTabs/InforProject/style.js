const styles = {
  viewDescription:{
    marginBottom:' 40px ',
    flexDirection: 'column'
  },
  viewMember: {
    width: 'fit-content',
  },
  viewAvatar: {
    textAlign: 'center',
    maxWidth: 'fit-content',
  },
  imgAvatar: {
    margin: 'auto',
    marginBottom: '10px',
    width: '40px',
    height: '40px',
  },
  iconTitle: {
    marginRight: '5px',
    marginBottom: '2px'
  }
};
export default styles;