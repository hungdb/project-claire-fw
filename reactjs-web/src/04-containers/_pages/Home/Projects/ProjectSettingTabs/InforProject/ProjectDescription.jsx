import styles from './style';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Badge, Row, Col } from 'reactstrap';
import { formatDateTime } from '../../../../../../08-helpers/common';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const ProjectDescription = ({ infoProject }) => {
  const renderDescription = (t, description, createdAt, isActive, isNotificationEnable, projectRate) => {
    return (
      <div className={'view-group mb-5'}>
        <Row>
          {description && description !== '' && (
            <Col xs={12} sm={6} md={4} lg={2}>
              <AppText className={'text-title d-inline-block'}>{t('information_project.description') + ': '}</AppText>
              <AppText className={'text-group d-inline-block'}> {description} </AppText>
            </Col>
          )}
          <Col xs={12} sm={6} md={4} lg={2}>
            <AppText className={'text-title d-inline-block'}>{t('information_project.is_active') + ': '}</AppText>
            <Badge color={isActive ? 'success' : 'warning'} className={'badge-text pt-1'}>
              {isActive ? t('information_project.allow') : t('information_project.not_allow')}
            </Badge>
          </Col>
          <Col xs={12} sm={6} md={4} lg={2}>
            <AppText className={'text-title d-inline-block'}>{t('information_project.notification') + ': '}</AppText>
            <Badge color={isNotificationEnable ? 'success' : 'warning'} className={'badge-text pt-1'}>
              {isNotificationEnable ? t('information_project.allow') : t('information_project.not_allow')}
            </Badge>
          </Col>
          <Col xs={12} sm={6} md={4} lg={2}>
            <AppText className={'text-title d-inline-block'}>{t('information_project.project_rate') + ': '}</AppText>
            <AppText className={'text-group d-inline-block'}>
              <Badge color={'info'}>{projectRate}</Badge>
            </AppText>
          </Col>
          <Col xs={12} sm={6} md={4} lg={3}>
            <AppText className={'text-title d-inline-block'}>{t('behaviour_management.create_at') + ': '}</AppText>
            <AppText className={'text-group d-inline-block'}>{createdAt.toUpperCase()}</AppText>
          </Col>
        </Row>
      </div>
    );
  };

  const description = infoProject && infoProject.description;
  const createdAt = (infoProject && formatDateTime(infoProject.createdAt, 'LLLL', 1)) || '';

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('information_project.description').toUpperCase()}</AppText>
          </div>
          {infoProject &&
            renderDescription(
              t,
              description,
              createdAt,
              infoProject.isActive,
              infoProject.isNotificationEnable,
              infoProject.projectRate
            )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectDescription.propTypes = {
  infoProject: object
};

export default memo(ProjectDescription);
