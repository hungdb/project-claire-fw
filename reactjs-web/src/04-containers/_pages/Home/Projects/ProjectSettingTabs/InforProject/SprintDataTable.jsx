import styles from './style';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { calculateLength, formatDateTime } from '../../../../../../08-helpers/common';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const TableHead = LazyComponent(() => import('../../../../../../03-components/TableHead'));

const SprintDataTable = ({ infoProject }) => {
  const renderSprint = (t, arraySprint) => {
    const fields = [
      'no',
      'information_project.sprint_name',
      'information_project.sprint_status',
      'information_project.start_date',
      'information_project.end_date'
    ];
    const classes = [
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center'
    ];
    return (
      <Table bordered responsive>
        <thead>
          <tr>
            <TableHead fields={fields} t={t} classes={classes} />
          </tr>
        </thead>
        <tbody>
          {calculateLength(arraySprint) ? (
            arraySprint.map((item, index) => {
              if (item) {
                const number = index + 1;
                const sprintName = item.name || '';
                const status = item.status || '';
                const startDate = formatDateTime(item.startDate, 'LLLL', 1);
                const endDate = formatDateTime(item.endDate, 'LLLL', 1);
                return (
                  <tr key={item.id || index}>
                    <td className={'align-middle text-center'}> {number} </td>
                    <td className={'align-middle text-center'}> {sprintName} </td>
                    <td className={'align-middle text-center'}> {status} </td>
                    <td className={'align-middle text-center'}> {startDate} </td>
                    <td className={'align-middle text-center'}> {endDate} </td>
                  </tr>
                );
              } else {
                return <div />;
              }
            })
          ) : (
            <tr>
              <td colSpan={10} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  };

  const sprints = infoProject && infoProject.sprints;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <div className={'d-flex align-items-center margin-bottom-15px'}>
            <i className={'fa fa-get-pocket'} style={styles.iconTitle} />
            <AppText type={'content-header'}>{t('sprints').toUpperCase()}</AppText>
          </div>
          <div>{renderSprint(t, sprints)}</div>
        </>
      )}
    </NamespacesConsumer>
  );
};

SprintDataTable.propTypes = {
  infoProject: object
};
export default memo(SprintDataTable);
