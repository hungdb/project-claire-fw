import React, { memo } from 'react';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func, number, array, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateProjectAlertSubscriptionByUser } from '../../../../../../01-actions/project';
import { SUBSCRIPTION_METHODS } from '../../../../../../05-utils/commonData';

const Multicheck = LazyComponent(() => import('../../../../../../03-components/Multicheck'));
const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));

const SubscriptionDialog = ({
  isOpen,
  onClose,
  itemAlertSubByUser,
  selectedProjectEnvironments,
  updateProjectAlertSubscriptionByUser,
  selectedProjectId
}) => {
  let envSubscriptions =
    selectedProjectEnvironments &&
    selectedProjectEnvironments.map(e => ({
      value: e.id,
      label: e.name
    }));
  let methodSubscriptions = [...SUBSCRIPTION_METHODS];

  if (itemAlertSubByUser) {
    itemAlertSubByUser.environmentIds.forEach(env => {
      if (envSubscriptions.some(sub => sub.value === env.id)) {
        const index = envSubscriptions.findIndex(m => m.value === env.id);
        const sub = envSubscriptions.find(m => m.value === env.id);
        const data = { ...sub, checked: true };
        envSubscriptions = [...envSubscriptions.slice(0, index), data, ...envSubscriptions.slice(index + 1)];
      }
    });
    itemAlertSubByUser.methodIds.forEach(med => {
      if (methodSubscriptions.some(sub => sub.value === med)) {
        const index = methodSubscriptions.findIndex(m => m.value === med);
        const sub = methodSubscriptions.find(m => m.value === med);
        const data = { ...sub, checked: true };
        methodSubscriptions = [...methodSubscriptions.slice(0, index), data, ...methodSubscriptions.slice(index + 1)];
      }
    });
  }

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} toggle={() => onClose && onClose()} centered backdrop="static">
          <ModalHeader toggle={() => onClose && onClose()}>{t('update_alert_subscription')}</ModalHeader>
          <ModalBody className={'overflow-visible'}>
            <div className="margin-bottom-5px">
              <div className={'d-flex flex-wrap-wrap margin-bottom-5px justify-content-between multicheck-container'}>
                <AppText className={'min-width-120px'} type={'content-sub-header'}>
                  {t('environments')}
                </AppText>
                <Multicheck
                  className={'flex-grow-1 flex-wrap-wrap'}
                  items={envSubscriptions}
                  onChange={(index, checked) => {
                    const environmentIds = checked
                      ? envSubscriptions
                          .filter(sub => sub.checked)
                          .map(sub => sub.value)
                          .concat([envSubscriptions[index].value])
                      : envSubscriptions
                          .filter(sub => sub.checked && sub.value !== envSubscriptions[index].value)
                          .map(sub => sub.value);
                    updateProjectAlertSubscriptionByUser({
                      userId: itemAlertSubByUser && itemAlertSubByUser.userId,
                      projectId: selectedProjectId,
                      environmentIds: environmentIds
                    });
                  }}
                />
              </div>
              <div className={'d-flex flex-wrap-wrap margin-bottom-5px methods-area'}>
                <AppText className={'min-width-120px'} type={'content-sub-header'}>
                  {t('methods')}
                </AppText>
                <Multicheck
                  className={'flex-grow-1 flex-wrap-wrap'}
                  items={methodSubscriptions}
                  onChange={(index, checked) => {
                    const methodIds = checked
                      ? methodSubscriptions
                          .filter(sub => sub.checked)
                          .map(sub => sub.value)
                          .concat([methodSubscriptions[index].value])
                      : methodSubscriptions
                          .filter(sub => sub.checked && sub.value !== methodSubscriptions[index].value)
                          .map(sub => sub.value);
                    updateProjectAlertSubscriptionByUser({
                      userId: itemAlertSubByUser && itemAlertSubByUser.userId,
                      projectId: selectedProjectId,
                      methodIds
                    });
                  }}
                />
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <div className={'flex-grow-1 d-flex justify-content-between'}>
              <div />
              <div>
                <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={() => onClose()}>
                  {t('close')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

SubscriptionDialog.propTypes = {
  isOpen: bool,
  onClose: func,
  updateProjectAlertSubscriptionByUser: func,
  selectedProjectId: number,
  selectedProjectEnvironments: array,
  itemAlertSubByUser: object
};

const mapStateToProps = store => {
  return {
    selectedProjectId: store.project.selectedProject.id,
    selectedProjectEnvironments: store.project.selectedProjectEnvironments,
    itemAlertSubByUser: store.project.itemAlertSubByUser
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      updateProjectAlertSubscriptionByUser
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(SubscriptionDialog));
