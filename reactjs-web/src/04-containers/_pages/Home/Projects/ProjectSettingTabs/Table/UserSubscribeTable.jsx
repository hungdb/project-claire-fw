import './style.css';
import UserAvatar from '../../../../../../03-components/UserAvatar';
import FontAwesomeIcon from '../../../../../../03-components/FontAwesomeIcon';
import TableHead from '../../../../../../03-components/TableHead';
import SubscriptionDialog from './SubscriptionDialog';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { calculateLength, isAllowPermission } from '../../../../../../08-helpers/common';
import { SUBSCRIBE_METHODS } from '../../../../../../05-utils/commonData';
import {
  getUsersSubscriptionProjectsAlert,
  setItemAlertSubscriptionByUser
} from '../../../../../../01-actions/project';
import { PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION_BY_USER } from '../../../../../../07-constants/app-permission';
import { array, func, number, object } from 'prop-types';

const fields = ['no', 'user', 'environments', 'methods'];
const classes = ['w-5p', 'w-5p', 'w-30p', 'w-30p'];

const _renderEnvironments = (env, t) => {
  if (calculateLength(env)) {
    return env.map(e => (
      <span key={e.id} className="mb-0">
        {e.name}
      </span>
    ));
  }
  return <span className="mb-0">{t('not_found')}</span>;
};

const _renderMethods = (data, t) => {
  if (calculateLength(data)) {
    return data.map((item, index) => {
      const result = SUBSCRIBE_METHODS.find(med => {
        if (med.value === item) {
          return med;
        }
        return null;
      });

      return <FontAwesomeIcon key={index} classes={`${result.class} font-size-24 color-base mr-1`} />;
    });
  }
  return <span>{t('not_found')}</span>;
};

const UserSubscribeTable = ({
  usersSubscriptionAlert,
  appPermissions,
  selectedProjectId,
  setItemAlertSubscriptionByUser,
  itemAlertSubByUser
}) => {
  const [isAlertSubscription, setIsAlertSubscription] = useState(false);

  const handleAlertSubClose = () => setIsAlertSubscription(false);
  const handleAlertSubOpen = item => {
    setItemAlertSubscriptionByUser(item);
    setIsAlertSubscription(true);
  };

  useEffect(() => {
    // this.props.getUsersSubscriptionProjectsAlert({ projectId: this.props.selectedProjectId });
    // return () => {
    //   SetItem(null);
    // };
  });

  const _renderTableContent = (data, t) => {
    return (
      data &&
      data.map((item, index) => {
        return (
          <tr key={item.id}>
            <td className="text-center">{index + 1}</td>
            <td>
              <UserAvatar user={item.User} id={index} />
            </td>
            <td>{_renderEnvironments(item.environmentIds, t)}</td>
            <td>{_renderMethods(item.methodIds, t)}</td>
            {isAllowPermission(appPermissions, PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION_BY_USER, selectedProjectId) && (
              <td className={'text-center align-middle'}>
                <span className={'cursor-pointer'} onClick={() => handleAlertSubOpen(item)}>
                  <i className="fa fa-pencil color-base" />
                </span>
              </td>
            )}
          </tr>
        );
      })
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Table bordered responsive hover striped className="user-subscription-table">
            <thead>
              <tr>
                <TableHead fields={fields} t={t} classes={classes} />
                {isAllowPermission(
                  appPermissions,
                  PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION_BY_USER,
                  selectedProjectId
                ) && <th className={'font-size-14 font-weight-500 text-center align-middle w-10p'}>{t('action')}</th>}
              </tr>
            </thead>
            <tbody>
              {calculateLength(usersSubscriptionAlert) ? (
                _renderTableContent(usersSubscriptionAlert, t)
              ) : (
                <tr>
                  <td
                    colSpan={
                      isAllowPermission(
                        appPermissions,
                        PERM_UPDATE_PROJECT_ALERT_SUBSCRIPTION_BY_USER,
                        selectedProjectId
                      )
                        ? 5
                        : 4
                    }
                    className="text-center text-muted"
                  >
                    {t('not_found')}
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
          {isAlertSubscription && itemAlertSubByUser && (
            <SubscriptionDialog isOpen={isAlertSubscription} onClose={handleAlertSubClose} />
          )}
        </>
      )}
    </NamespacesConsumer>
  );
};

UserSubscribeTable.propTypes = {
  usersSubscriptionAlert: array,
  selectedProjectId: number,
  getUsersSubscriptionProjectsAlert: func,
  appPermissions: array,
  setItemAlertSubscriptionByUser: func,
  itemAlertSubByUser: object
};

const mapStateToProps = store => {
  return {
    selectedProjectId: store.project.selectedProject.id,
    usersSubscriptionAlert: store.project.usersSubscriptionAlert,
    appPermissions: store.auth.permissions,
    itemAlertSubByUser: store.project.itemAlertSubByUser
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsersSubscriptionProjectsAlert,
      setItemAlertSubscriptionByUser
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(UserSubscribeTable));
