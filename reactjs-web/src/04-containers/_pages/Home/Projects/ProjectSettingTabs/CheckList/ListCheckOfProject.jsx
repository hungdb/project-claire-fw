import LazyComponent from '../../../../../../03-components/LazyComponent/index';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { func, object, array } from 'prop-types';
import { calculateLength, isAllowPermission } from '../../../../../../08-helpers/common';
import { CHECK_LIST_BY_PROJECT, REMOVE_CHECK_ITEM_FROM_PROJECT } from '../../../../../../07-constants/app-permission';

const CheckListItem = LazyComponent(() => import('./CheckListItem'));

const ListCheckOfProject = ({
  checkUncheckItem,
  listItemsCheckOfProject,
  selectedProject,
  onSelectCheckList,
  appPermissions
}) => {
  const handleCheck = value => {
    if (isAllowPermission(appPermissions, CHECK_LIST_BY_PROJECT, selectedProject.id)) checkUncheckItem(value);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          {calculateLength(listItemsCheckOfProject)
            ? listItemsCheckOfProject.map((item, index) => (
                <div key={index} className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
                  <CheckListItem
                    label={item.CheckListItem.name}
                    checked={item.isChecked}
                    onClick={handleCheck}
                    value={{
                      checkItemId: item.checkItemId,
                      projectId: item.projectId,
                      isChecked: !item.isChecked
                    }}
                  />
                  {isAllowPermission(appPermissions, REMOVE_CHECK_ITEM_FROM_PROJECT, selectedProject.id) && (
                    <i
                      className="fa fa-trash cursor-pointer color-base-remove-btn"
                      onClick={() => onSelectCheckList(item.id)}
                    />
                  )}
                </div>
              ))
            : null}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ListCheckOfProject.propTypes = {
  checkUncheckItem: func,
  listItemsCheckOfProject: array,
  selectedProject: object,
  onSelectCheckList: func,
  appPermissions: array
};

export default memo(ListCheckOfProject);
