import ProjectCheckListTab from './CheckList';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import {
  createItemCheck,
  listAllItemCheck,
  removeItemCheck,
  updateItemCheck,
  listItemCheckOfProject,
  checkUncheckItem,
  clearItemChecked,
  addItemCheckToProject,
  removeItemCheckOfProject
} from '../../../../../../01-actions/check-list';

const mapStateToProps = store => {
  return {
    // auth
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,

    // common
    error: store.common.error,

    Status: store.checkList.status,
    checkListError: store.checkList.checkListError,
    listItemsCheck: store.checkList.listItemsCheck,
    listItemsCheckOfProject: store.checkList.listItemsCheckOfProject,
    selectedProject: store.project.selectedProject
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAllItemCheck,
      createItemCheck,
      removeItemCheck,
      updateItemCheck,
      listItemCheckOfProject,
      checkUncheckItem,
      clearItemChecked,
      addItemCheckToProject,
      removeItemCheckOfProject
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectCheckListTab);
