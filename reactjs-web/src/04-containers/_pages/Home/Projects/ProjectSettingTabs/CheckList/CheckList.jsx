import LazyComponent from '../../../../../../03-components/LazyComponent/index';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { func, object, array } from 'prop-types';
import { calculateLength, isAllowPermission } from '../../../../../../08-helpers/common';
import { ADD_CHECK_ITEM_TO_PROJECT, LIST_ITEM_CHECK_OF_PROJECT } from '../../../../../../07-constants/app-permission';
import { Button } from 'reactstrap';

const ListCheckOfProject = LazyComponent(() => import('./ListCheckOfProject'));
const AddCheckListItemInProjectDialog = LazyComponent(() =>
  import('../../../../../dialog/check-list/add-check-list-item-in-project')
);

const ProjectCheckListTab = ({
  addItemCheckToProject,
  listAllItemCheck,
  listItemCheckOfProject,
  checkUncheckItem,
  listItemsCheckOfProject,
  selectedProject,
  removeItemCheckOfProject,
  appPermissions
}) => {
  const [state, setState] = useState({
    showAddItemCheckList: false,
    isRemove: false,
    selectedItem: 0
  });

  const { showAddItemCheckList, isRemove, selectedItem } = state;

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, LIST_ITEM_CHECK_OF_PROJECT, selectedProject.id)) {
      listItemCheckOfProject({ projectId: selectedProject.id });
    }
  };
  useEffect(() => {
    checkRoleAndLoad();
  }, [isAllowPermission]);

  useEffect(() => {
    listItemCheckOfProject({ projectId: selectedProject.id });
  }, [selectedProject.id]);

  const onAddCheckListItem = () => {
    listAllItemCheck();
    setState({
      ...state,
      showAddItemCheckList: true,
      isRemove: false
    });
  };

  const onOkChecklistDialog = (removeMode, value) => {
    setState({
      ...state,
      showAddItemCheckList: false
    });
    if (removeMode) {
      removeItemCheckOfProject(value);
    } else {
      addItemCheckToProject({
        projectId: selectedProject.id,
        checkItemId: value.checkItemId
      });
    }
  };

  const onCloseDialogCheckList = () =>
    setState({
      ...state,
      showAddItemCheckList: false
    });

  const listItemChecked = listItemsCheckOfProject.filter(item => item.isChecked === true);
  const onSelectCheckList = selectedItem =>
    setState({
      showAddItemCheckList: true,
      isRemove: true,
      selectedItem
    });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'setting-tab'}>
          <div className={'d-flex align-items-center justify-content-between margin-bottom-15px'}>
            {isAllowPermission(appPermissions, ADD_CHECK_ITEM_TO_PROJECT, selectedProject.id) && (
              <Button className={'default-btn'} onClick={onAddCheckListItem} size={'md'}>
                <i className="fa fa-plus" aria-hidden="true" />
                &nbsp;&nbsp;{t('add_check_item')}
              </Button>
            )}
            <AddCheckListItemInProjectDialog
              isOpen={showAddItemCheckList}
              onClose={onCloseDialogCheckList}
              value={{ itemCheck: selectedItem }}
              removeMode={isRemove}
              onOK={onOkChecklistDialog}
            />
            <label className="title font-size-14">
              {`${calculateLength(listItemChecked)}/${calculateLength(listItemsCheckOfProject)}`} checked
            </label>
          </div>
          <ListCheckOfProject
            checkUncheckItem={checkUncheckItem}
            listItemsCheckOfProject={listItemsCheckOfProject}
            selectedProject={selectedProject}
            onSelectCheckList={onSelectCheckList}
            appPermissions={appPermissions}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProjectCheckListTab.propTypes = {
  addItemCheckToProject: func,
  listAllItemCheck: func,
  listItemCheckOfProject: func,
  checkUncheckItem: func,
  listItemsCheckOfProject: array,
  selectedProject: object,
  removeItemCheckOfProject: func,
  appPermissions: array
};

export default memo(ProjectCheckListTab);
