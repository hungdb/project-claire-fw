import React, { memo } from 'react';
import { bool, func, string, object } from 'prop-types';
import { CheckBox, CheckBoxOutlineBlank } from '@material-ui/icons';

const CheckListItem = ({ checked, onClick, label, value }) => {
  return (
    <div className={'no-text-select hover-response d-inline-flex cursor-pointer min-width-120px'}>
      {checked ? (
        <div className={'main-color'}>
          <CheckBox onClick={() => onClick(value)} />
        </div>
      ) : (
        <CheckBoxOutlineBlank onClick={() => onClick(value)} />
      )}
      <div className={'margin-left-5px font-size-14'} onClick={() => onClick(value)}>
        {' '}
        {label}
      </div>
    </div>
  );
};

CheckListItem.propTypes = {
  checked: bool,
  onClick: func,
  label: string,
  value: object
};

export default memo(CheckListItem);
