import React, { memo } from 'react';
import { array, number, string, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { UPDATE_ROLE_USER_INTO_PROJECT } from '../../../../../../07-constants/app-permission';
import { Table } from 'reactstrap';

const DataTable = ({ appPermissions, projectId, users, onEdit }) => {
  const getUserRoles = () => {
    const userRoles = {};
    users &&
      users.forEach(u => {
        if (!userRoles[u.userId]) {
          userRoles[u.userId] = {
            user: u,
            roles: []
          };
        }
        userRoles[u.userId].roles.push(u['ProjectRole']);
      });
    return userRoles;
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <th>#</th>
                <th>{t('user')}</th>
                <th>{t('role')}</th>
                <th>{t('action')}</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(getUserRoles())
                .sort((a, b) => {
                  const firstValueCompare = a.user && a.user.ProjectRole && a.user.ProjectRole.priorityLevel;
                  const nextValueCompare = b.user && b.user.ProjectRole && b.user.ProjectRole.priorityLevel;
                  return firstValueCompare - nextValueCompare;
                })
                .map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{item.user && item.user.User && item.user.User.name}</td>
                      <td className={'font-size-14'}>
                        {item.roles &&
                          item.roles.reduce((accum, role) => {
                            return accum + (accum ? ', ' : '') + role.name;
                          }, '')}
                      </td>
                      <td>
                        {isAllowPermission(appPermissions, UPDATE_ROLE_USER_INTO_PROJECT, projectId) && (
                          <span
                            title={t('edit')}
                            className={'cursor-pointer'}
                            onClick={() => onEdit(item.user && item.user.userId)}
                          >
                            <i className="fa fa-pencil color-base-btn" />
                          </span>
                        )}
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </div>
      )}
    </NamespacesConsumer>
  );
};

DataTable.propTypes = {
  appPermissions: array,
  projectId: number,
  roles: array,
  status: string,
  users: array,
  onEdit: func
};

export default memo(DataTable);
