import './UserInProject.css';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../../03-components/SpinLoader';
import React, { memo, useState, useEffect } from 'react';
import { array, number, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button } from 'reactstrap';
import { isAllowPermission, promiseDelayImport } from '../../../../../../08-helpers/common';
import {
  ADD_USER_INTO_PROJECT,
  PERM_ADD_ALL_USER_INTO_PROJECT,
  LIST_USER_INTO_PROJECT
} from '../../../../../../07-constants/app-permission';
import {
  UIP_USER_CREATE_SUCCESS,
  UIP_USER_DELETE_SUCCESS,
  UIP_USER_UPDATE_SUCCESS
} from '../../../../../../07-constants/userInProject';

const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const DialogUserInProject = LazyComponent(() => import('../../../../../dialog/user-in-project/DialogUserInProject'));
const DialogUsersInProject = LazyComponent(() => import('../../../../../dialog/user-in-project/DialogUsersInProject'));
const DataTable = LazyComponent(promiseDelayImport(import('./DataTable')), <SpinLoader color="primary" type="grow" />);

const UserInProject = ({ appPermissions, projectId, usersInProjectStatus, actions, users }) => {
  const [state, setState] = useState({
    showDialog: false,
    showDialogAll: false,
    editUserId: null
  });

  const { showDialog, showDialogAll, editUserId } = state;

  useEffect(() => {
    if (isAllowPermission(appPermissions, LIST_USER_INTO_PROJECT, projectId)) {
      actions.getUsers({ projectId: projectId });
    }
  }, [appPermissions, projectId]);

  useEffect(() => {
    switch (usersInProjectStatus) {
      case UIP_USER_CREATE_SUCCESS:
      case UIP_USER_UPDATE_SUCCESS:
      case UIP_USER_DELETE_SUCCESS:
        actions.getUsers({ projectId: projectId });
        break;

      default:
        break;
    }
  }, [usersInProjectStatus]);

  const onEditItem = editUserId => {
    setState({
      ...state,
      showDialog: true,
      editUserId
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'setting-tab user-in-project-tab'}>
          {showDialog && (
            <DialogUserInProject
              currentUser={editUserId}
              onClose={() => {
                setState({
                  ...state,
                  showDialog: false,
                  editUserId: null
                });
              }}
            />
          )}
          {showDialogAll && (
            <DialogUsersInProject
              onClose={() =>
                setState({
                  ...state,
                  showDialogAll: false
                })
              }
            />
          )}
          <div className={'d-flex justify-content-between align-items-center margin-bottom-15px'}>
            <AppText type={'content-header'}>{t('users_in_project')}</AppText>
            <div className={'d-flex justify-content-between align-items-center'}>
              {isAllowPermission(appPermissions, ADD_USER_INTO_PROJECT, projectId) && (
                <Button
                  className={'default-btn mr-2'}
                  onClick={() => {
                    setState({
                      ...state,
                      showDialog: true
                    });
                  }}
                  size={'md'}
                >
                  <i className="fa fa-plus" aria-hidden="true" />
                  &nbsp;&nbsp;{t('add_user')}
                </Button>
              )}
              {isAllowPermission(appPermissions, PERM_ADD_ALL_USER_INTO_PROJECT, projectId) && (
                <Button
                  className={'default-btn'}
                  onClick={() =>
                    setState({
                      ...state,
                      showDialogAll: true
                    })
                  }
                  size={'md'}
                >
                  <i className="fa fa-plus" aria-hidden="true" />
                  &nbsp;&nbsp;{t('add_all_users')}
                </Button>
              )}
            </div>
          </div>
          <DataTable appPermissions={appPermissions} projectId={projectId} users={users} onEdit={onEditItem} />
        </div>
      )}
    </NamespacesConsumer>
  );
};

UserInProject.propTypes = {
  appPermissions: array,
  projectId: number,
  roles: array,
  status: string,
  usersInProjectStatus: string,
  actions: object,
  users: array
};

export default memo(UserInProject);
