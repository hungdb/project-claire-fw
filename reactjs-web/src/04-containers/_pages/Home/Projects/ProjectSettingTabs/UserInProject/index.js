import UserInProject from './UserInProject';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  addUserIntoProject,
  clearUserInProject,
  getAllUsers,
  getUsers,
  listRole,
  removeUserFromProject,
  updateUserRoleInProject
} from '../../../../../../01-actions/userInProject';

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,

    status: store.project.status,
    usersInProjectStatus: store.userInProject.status,
    users: store.userInProject.usersInProject,
    projectId: store.project.selectedProject.id
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getUsers,
      addUserIntoProject,
      updateUserRoleInProject,
      removeUserFromProject,
      getAllUsers,
      listRole,
      clearUserInProject
    },
    dispatch
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInProject);
