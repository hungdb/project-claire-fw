import ProjectDeploymentTab from './DeploymentTab';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  addProjectEnvironment,
  clearHealthCheckData,
  getProjectEnvironments,
  loadHealthCheckData,
  toggleServerDetail,
  updateProjectEnvironment
} from '../../../../../../01-actions/project';

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,

    status: store.project.status,
    selectedProject: store.project.selectedProject,
    selectedProjectEnvironments: store.project.selectedProjectEnvironments,
    healthCheck: store.project.healthCheck
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addProjectEnvironment,
      getProjectEnvironments,
      updateProjectEnvironment,
      loadHealthCheckData,
      toggleServerDetail,
      clearHealthCheckData
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDeploymentTab);
