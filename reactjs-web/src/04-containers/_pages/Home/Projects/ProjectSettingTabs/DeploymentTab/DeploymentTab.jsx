import { PureComponent, Fragment } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import moment from 'moment';
import AppText from '../../../../../../03-components/AppText';
import React from 'react';
import Select from 'react-select';
import AddProjectEnvironmentDialog from '../../../../../dialog/add-project-env/add-project-env';
import ValueInputDialog from '../../../../../dialog/value-input/value-input';
import ServerStatusIcon from '../../../../../../03-components/ServerStatusIcon';
import { PROJECT_UPDATE_EVN_SUCCESS } from '../../../../../../07-constants/project';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import {
  LIST_PROJECT_ENVIRONMENTS,
  PERM_GET_HEALTHCHECK_DATA,
  PERM_CREATE_ENVIRONMENT,
  PERM_UPDATE_ENVIRONMENT
} from '../../../../../../07-constants/app-permission';
import { Button, Table, Row, Col } from 'reactstrap';
import { func, string, object, array } from 'prop-types';

class ProjectDeploymentTab extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      reloadHealthCheck: false,
      selectedEnv: null,
      showAddProjectEnvDialog: false,
      showEditHealthCheckDialog: false
    };
    this.intervalHandler = null;
    this.handleEnvChange = this.handleEnvChange.bind(this);
  }

  static propTypes = {
    selectedProjectEnvironments: array,
    appPermissions: array,
    clearHealthCheckData: func,
    loadHealthCheckData: func,
    getProjectEnvironments: func,
    toggleServerDetail: func,
    addProjectEnvironment: func,
    updateProjectEnvironment: func,
    selectedProject: object,
    healthCheck: object,
    status: string
  };

  handleEnvChange(envId) {
    const selectedEnv = this.props.selectedProjectEnvironments.find(e => e.id === envId);

    this.intervalHandler && clearInterval(this.intervalHandler);
    this.props.clearHealthCheckData();
    if (
      selectedEnv &&
      selectedEnv.serverHealthCheck &&
      isAllowPermission(this.props.appPermissions, PERM_GET_HEALTHCHECK_DATA)
    ) {
      //-- set loop
      this.props.loadHealthCheckData({ envId });
      this.intervalHandler = setInterval(async () => {
        this.props.loadHealthCheckData({ envId });
      }, 10000);
    }
  }

  componentWillUnmount() {
    this.intervalHandler && clearInterval(this.intervalHandler);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selectedProjectEnvironments && nextProps.selectedProjectEnvironments.length) {
      const selectedEnvId =
        !prevState.selectedEnvId || !nextProps.selectedProjectEnvironments.find(e => e.id === prevState.selectedEnvId)
          ? nextProps.selectedProjectEnvironments.length
            ? nextProps.selectedProjectEnvironments[0].id
            : null
          : prevState.selectedEnvId;
      return { selectedEnvId };
    }
    return null;
  }

  componentDidMount() {
    if (isAllowPermission(this.props.appPermissions, LIST_PROJECT_ENVIRONMENTS, this.props.selectedProject.id)) {
      this.props.getProjectEnvironments({ projectId: this.props.selectedProject.id });
    }
    if (this.state.selectedEnvId) {
      this.handleEnvChange(this.state.selectedEnvId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.selectedProject !== prevProps.selectedProject) {
      if (isAllowPermission(this.props.appPermissions, LIST_PROJECT_ENVIRONMENTS, this.props.selectedProject.id)) {
        this.props.getProjectEnvironments({ projectId: this.props.selectedProject.id });
      }
    }
    if (this.state.selectedEnvId !== prevState.selectedEnvId) {
      this.handleEnvChange(this.state.selectedEnvId);
    }

    if (this.props.status === PROJECT_UPDATE_EVN_SUCCESS && this.state.reloadHealthCheck) {
      this.setState({ reloadHealthCheck: false });
      this.handleEnvChange(this.state.selectedEnvId);
    }

    // if (nextProps.selectedProjectEnvironments && nextProps.selectedProjectEnvironments.length) {
    //   this.setState({
    //     selectedEnv: !this.state.selectedEnv || !nextProps.selectedProjectEnvironments.find(e => e.id === this.state.selectedEnv.id) ?
    //       nextProps.selectedProjectEnvironments[0] : this.state.selectedEnv
    //   });
    // }
  }

  render() {
    const { healthCheck } = this.props;
    const selectedEnv = this.props.selectedProjectEnvironments.find(e => e.id === this.state.selectedEnvId);

    const isAllow = isAllowPermission(
      this.props.appPermissions,
      LIST_PROJECT_ENVIRONMENTS,
      this.props.selectedProject.id
    );

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Fragment>
            {isAllow && (
              <div className={'setting-tab overflow-visible'}>
                <Row className={'justify-content-between margin-bottom-15px'}>
                  <Col xs={12} sm={6} md={4} lg={3}>
                    <AppText type={'content-header'}>{t('environments')}</AppText>
                  </Col>
                  <Col className="col-auto">
                    {isAllowPermission(
                      this.props.appPermissions,
                      PERM_CREATE_ENVIRONMENT,
                      this.props.selectedProject.id
                    ) && (
                      <Button
                        className={'default-btn'}
                        onClick={() => {
                          this.setState({ showAddProjectEnvDialog: true });
                        }}
                        size={'md'}
                      >
                        <i className="fa fa-plus" aria-hidden="true" />
                        &nbsp;&nbsp;{t('add_environment')}
                      </Button>
                    )}
                  </Col>
                </Row>
                <div className={'max-width-240px margin-bottom-40px'}>
                  <Select
                    value={selectedEnv ? { label: selectedEnv.name, value: selectedEnv.id } : null}
                    placeholder={t('select_environment')}
                    options={this.props.selectedProjectEnvironments.map(p => ({ label: p.name, value: p.id }))}
                    onChange={e => {
                      this.setState({ selectedEnvId: e.value });
                      this.handleEnvChange(e.value);
                    }}
                  />
                </div>
                {selectedEnv &&
                  (selectedEnv.serverHealthCheck && this.props.healthCheck.loading ? (
                    <div className={'text-center'}>{t('loading_health_check_data')}</div>
                  ) : (
                    <div>
                      <div className={'margin-top-15px'}>
                        <AppText className={'min-width-240px d-inline-block'} type={'content-header'}>
                          {t('status_server_endpoint')}
                        </AppText>
                        {selectedEnv.serverHealthCheck ? (
                          <div className={'d-inline-block margin-right-40px'}>
                            <ServerStatusIcon
                              active={
                                healthCheck && healthCheck.healthCheckServer && healthCheck.healthCheckServer.isAlive
                              }
                              className="d-inline-block"
                            />
                            {/* eslint-disable-next-line */}
                            <a className={'margin-left-15px'} target="_blank" href={selectedEnv.serverHealthCheck}>
                              {selectedEnv.serverHealthCheck}
                            </a>
                          </div>
                        ) : (
                          <div className={'d-inline-block'}>{t('not_set')}</div>
                        )}
                        {isAllowPermission(
                          this.props.appPermissions,
                          PERM_UPDATE_ENVIRONMENT,
                          this.props.selectedProject.id
                        ) && (
                          <div
                            className={'link-style d-inline-block'}
                            onClick={() => {
                              this.setState({ showEditHealthCheckDialog: true });
                            }}
                          >
                            {t('change')}
                          </div>
                        )}
                      </div>
                      <div className={'d-flex flex-column margin-top-15px'}>
                        <div>
                          <AppText className={'min-width-240px d-inline-block'} type={'content-header'}>
                            {t('server_status')}
                          </AppText>
                          <ServerStatusIcon
                            active={
                              healthCheck && healthCheck.healthCheckData && !healthCheck.healthCheckData.serviceDown
                            }
                            className="d-inline-block"
                          />
                        </div>
                        <div className={'d-flex justify-content-stretch margin-top-15px'}>
                          {selectedEnv.serverHealthCheck ? (
                            <Table bordered responsive hover striped>
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>{t('id')}</th>
                                  <th>{t('status')}</th>
                                  <th>{t('more')}</th>
                                </tr>
                              </thead>
                              <tbody>
                                {!!this.props.healthCheck && !!this.props.healthCheck.healthCheckData ? (
                                  this.props.healthCheck.healthCheckData.servers.map((server, index) => (
                                    <Fragment key={index}>
                                      <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{server.serverName}</td>
                                        <td>
                                          <div className={'d-flex justify-content-start align-items-center'}>
                                            <ServerStatusIcon
                                              active={!!server.services && !server.services.some(s => !s || !s.isAlive)}
                                            />
                                            <div className={'margin-left-15px'}>
                                              {!!server.services &&
                                                !!server.services.length &&
                                                moment(
                                                  server.services
                                                    .concat()
                                                    .sort((s1, s2) => (s1.lastContact > s2.lastContact ? 1 : -1))[0]
                                                    .lastContact
                                                ).fromNow()}
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <span
                                            className={'cursor-pointer'}
                                            onClick={() => {
                                              this.props.toggleServerDetail(server.serverName);
                                            }}
                                          >
                                            {server.isShowDetail ? (
                                              <i className="fa fa-chevron-up" />
                                            ) : (
                                              <i className="fa fa-chevron-down" />
                                            )}
                                          </span>
                                        </td>
                                      </tr>
                                      {server.isShowDetail && (
                                        <tr>
                                          <td colSpan={5}>
                                            <Table
                                              bordered
                                              responsive
                                              hover
                                              striped
                                              style={{ backgroundColor: '#eeeeee' }}
                                            >
                                              <thead>
                                                <tr>
                                                  <th>
                                                    <div>{t('service')}</div>
                                                  </th>
                                                  <th>
                                                    <div>{t('status')}</div>
                                                  </th>
                                                  <th>
                                                    <div>{t('last_contact')}</div>
                                                  </th>
                                                  <th>
                                                    <div>{t('last_problem')}</div>
                                                  </th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                {!!server.services &&
                                                  server.services.map((service, index) => (
                                                    <tr key={index}>
                                                      <td>
                                                        <div className={service.isLocal ? 'font-weight-bold' : ''}>
                                                          {service.serviceName}
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <ServerStatusIcon active={service.isAlive} />
                                                      </td>
                                                      <td>
                                                        <div>
                                                          {!service.lastContact
                                                            ? '---'
                                                            : moment(service.lastContact).fromNow()}
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <div>
                                                          {!service.lastProblem
                                                            ? '---'
                                                            : service.lastProblem.code +
                                                              (service.lastProblem.extra
                                                                ? '\n' + JSON.stringify(service.lastProblem.extra)
                                                                : '')}
                                                        </div>
                                                      </td>
                                                    </tr>
                                                  ))}
                                              </tbody>
                                            </Table>
                                          </td>
                                        </tr>
                                      )}
                                    </Fragment>
                                  ))
                                ) : (
                                  <tr>
                                    <td colSpan={5}>
                                      <div className={'text-center content-header text-muted'}>{t('no_data')}</div>
                                    </td>
                                  </tr>
                                )}
                              </tbody>
                            </Table>
                          ) : (
                            <div className={'flex-grow-1 text-center text-muted font-size-14'}>
                              {t('health_check_url_not_set')}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  ))}
                <AddProjectEnvironmentDialog
                  isOpen={this.state.showAddProjectEnvDialog}
                  onClose={() => {
                    this.setState({ showAddProjectEnvDialog: false });
                  }}
                  onOK={value => {
                    this.setState({ showAddProjectEnvDialog: false });
                    this.props.addProjectEnvironment({
                      ...value,
                      projectId: this.props.selectedProject.id
                    });
                  }}
                />

                {selectedEnv && (
                  <ValueInputDialog
                    isOpen={this.state.showEditHealthCheckDialog}
                    title={t('update_health_check_server')}
                    inputLabel={t('health_check_server_url')}
                    value={selectedEnv.serverHealthCheck}
                    cancelTitle={t('cancel')}
                    allowEmpty={true}
                    okTitle={t('OK')}
                    onClose={() => {
                      this.setState({ showEditHealthCheckDialog: false });
                    }}
                    onOK={value => {
                      this.setState({ showEditHealthCheckDialog: false });
                      this.setState({ reloadHealthCheck: true });
                      this.props.updateProjectEnvironment({
                        envId: selectedEnv.id,
                        serverHealthCheck: value
                      });
                    }}
                  />
                )}
              </div>
            )}
          </Fragment>
        )}
      </NamespacesConsumer>
    );
  }
}
export default ProjectDeploymentTab;
