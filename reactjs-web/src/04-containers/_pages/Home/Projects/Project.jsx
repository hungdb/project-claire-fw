import './style.css';
import LazyComponent from '../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect } from 'react';
import { array, object, func, number } from 'prop-types';
import { matchPath } from 'react-router-dom';
import { NamespacesConsumer } from 'react-i18next';
import { PROJECT_TAB } from '../../../../05-utils/commonData';
import { EVENT_NEW_COMMENT, EVENT_NOTIFICATION } from '../../../../07-constants/socket';
import { isAllowPermission, promiseDelayImport } from '../../../../08-helpers/common';
import {
  GET_PROJECT_ALERT_SUBSCRIPTION,
  LIST_ITEM_CHECK_OF_PROJECT,
  LIST_PROJECT_ENVIRONMENTS,
  LIST_USER_INTO_PROJECT,
  PERM_GET_DETAIL_PROJECT
} from '../../../../07-constants/app-permission';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../08-helpers/socket';

const ProjectGroupList = LazyComponent(() => import('./ProjectGroupList'));
const ProjectList = LazyComponent(() => import('./ProjectList'));
const RenderRoutes = LazyComponent(() => import('../../../../09-routes/RenderRoutes'));

const ProjectSettingTabs = LazyComponent(
  promiseDelayImport(import('./ProjectSettingTabs'), 1200),
  <Skeleton width={690} height={38} />
);

const checkAllowTab = (appPermissions, selectedProject) => [
  { value: 1, allow: isAllowPermission(appPermissions, LIST_ITEM_CHECK_OF_PROJECT, selectedProject.id) },
  { value: 2, allow: isAllowPermission(appPermissions, LIST_USER_INTO_PROJECT, selectedProject.id) },
  { value: 3, allow: isAllowPermission(appPermissions, GET_PROJECT_ALERT_SUBSCRIPTION, selectedProject.id) },
  { value: 4, allow: isAllowPermission(appPermissions, LIST_PROJECT_ENVIRONMENTS, selectedProject.id) },
  { value: 5, allow: isAllowPermission(appPermissions, PERM_GET_DETAIL_PROJECT, selectedProject.id) }
];

const Project = ({
  appPermissions,
  selectedProjectGroup,
  selectedProject,
  route,
  location,
  match,
  history,
  getPermissionSchemes,
  addNotification,
  fireSocketAddComment,
  selectedSettingTab,
  setSelectedSettingTab
}) => {
  const [state, setState] = useState({
    visibleTabs: []
  });

  const { visibleTabs } = state;

  useEffect(() => {
    const socket = connectSocket();

    for (const [i, r] of Object.entries(route.routes)) {
      if (matchPath(location.pathname, r.path) && Number(i) !== route.routes.length - 1) {
        setSelectedSettingTab(Number(i) + 1);
        break;
      }
    }

    getPermissionSchemes();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
        if (res.type === EVENT_NEW_COMMENT) fireSocketAddComment({ data: res.data });
      }
    });

    return () => {
      removeListener({ socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
    };
  }, []);

  const checkSwitchTab = data => {
    switch (data) {
      case PROJECT_TAB.checklist.value:
        return PROJECT_TAB.checklist.path;
      case PROJECT_TAB.userRole.value:
        return PROJECT_TAB.userRole.path;
      case PROJECT_TAB.alertNotifi.value:
        return PROJECT_TAB.alertNotifi.path;
      case PROJECT_TAB.deployment.value:
        return PROJECT_TAB.deployment.path;
      case PROJECT_TAB.info.value:
        return PROJECT_TAB.info.path;
      default:
        break;
    }
  };

  useEffect(() => {
    if (selectedProject && selectedProjectGroup && match.url === '/projects') {
      const tab = checkSwitchTab(selectedSettingTab);
      history.push(`projects/${selectedProjectGroup.id}/${selectedProject.id}/${tab}`);
    }
  }, [match, selectedProject, selectedProjectGroup]);

  useEffect(() => {
    let visibleTabsClone = visibleTabs;
    if (selectedProject) {
      const tabConditions = checkAllowTab(appPermissions, selectedProject);
      tabConditions.forEach(tab => {
        if (tab.allow) {
          if (!visibleTabsClone.includes(tab.value)) {
            visibleTabsClone = [...visibleTabsClone, tab.value];
          }
        } else {
          const index = visibleTabsClone.findIndex(t => t === tab.value);
          if (index >= 0) {
            visibleTabsClone = [...visibleTabsClone.slice(0, index), ...visibleTabsClone.slice(index + 1)];
          }
        }
      });
      if (visibleTabs !== visibleTabsClone) {
        setState({ visibleTabs: visibleTabsClone });
      }
    }
  }, [appPermissions, selectedProject]);

  const changeTabRoute = tabId => {
    const tab = tabId && checkSwitchTab(tabId);
    history.push(`${match.url}/${tab}`);
  };

  const onChangeTab = tab => {
    setSelectedSettingTab(tab.value);
    changeTabRoute(tab.value);
  };

  const _renderProjectSettings = t => (
    <div className="projects margin-top-10px">
      <div className="right">
        <div className="tabs-header margin-bottom-15px">
          <ProjectSettingTabs
            onChangeTab={onChangeTab}
            visibleTabs={visibleTabs}
            selectedSettingTab={selectedSettingTab}
            t={t}
          />
        </div>
        <div className="overflow-visible">
          <RenderRoutes routes={route.routes} />
        </div>
      </div>
    </div>
  );

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page page-home-projects">
          <ProjectGroupList />
          {selectedProjectGroup && <ProjectList />}
          {selectedProjectGroup && selectedProject && _renderProjectSettings(t)}
        </div>
      )}
    </NamespacesConsumer>
  );
};

Project.propTypes = {
  appPermissions: array,
  projects: array,
  selectedProjectGroup: func,
  selectedProject: object,
  route: object,

  location: object,
  match: object,
  history: object,

  getPermissionSchemes: func,
  addNotification: func,
  fireSocketAddComment: func,

  selectedSettingTab: number,
  setSelectedSettingTab: func
};

export default memo(Project);
