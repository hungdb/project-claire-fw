import { func, array } from 'prop-types';
import React, { PureComponent, Fragment } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col, Table } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'reactstrap';
import MarketDialog from '../../../../../dialog/market-dialog';
import { createMarket, getListMarket, updateMarket, removeMarket } from '../../../../../../01-actions/market';
import { PERM_GET_LIST_MARKET, PERM_CREATE_MARKET } from '../../../../../../07-constants/app-permission';
import TableHead from '../../../../../../03-components/TableHead';
import { isAllowPermission, calculateLength, formatDateTime } from '../../../../../../08-helpers/common';

const TABLE_FIELDS = ['no', 'market_name', 'created_at', 'action'];

class TabMarket extends PureComponent {
  static propTypes = {
    clearSaleSetting: func,
    appPermissions: array,

    allMarkets: array,

    createMarket: func,
    getListMarket: func,
    updateMarket: func,
    removeMarket: func
  };

  state = {
    marketDialogStatus: false,
    marketItem: null,
    isEditMarket: false
  };

  componentDidMount() {
    this.checkRoleAndLoad();
  }

  componentDidUpdate(prevProps) {
    if (this.props.appPermissions !== prevProps.appPermissions) {
      this.checkRoleAndLoad();
    }
  }

  checkRoleAndLoad = () => {
    const { appPermissions, getListMarket } = this.props;
    if (isAllowPermission(appPermissions, PERM_GET_LIST_MARKET)) {
      getListMarket();
    }
  };

  openMarketDialog = () => {
    this.setState({ marketDialogStatus: true });
  };

  onCloseMarketDialog = () => {
    this.setState({
      marketDialogStatus: false,
      isEditMarket: false,
      marketItem: null
    });
  };

  onOkDialog = marketName => {
    const { createMarket, updateMarket } = this.props;
    const { marketItem } = this.state;
    if (marketItem) {
      if (marketItem.name !== marketName) {
        updateMarket(marketItem.id, marketName);
      }
    } else {
      createMarket(marketName);
    }
    this.onCloseMarketDialog();
  };

  onEditMarket = marketItem => {
    this.setState(
      {
        marketItem,
        isEditMarket: true
      },
      this.openMarketDialog()
    );
  };

  onRemoveMarket = marketId => {
    const { removeMarket } = this.props;
    removeMarket(marketId);
    this.onCloseMarketDialog();
  };

  _renderTableBody = (length, data, t) => {
    const { appPermissions } = this.props;
    const body = calculateLength(data) ? (
      data.map((item, index) => {
        return (
          <tr key={item.id}>
            <td>{index + 1}</td>
            <td>{item.name}</td>
            <td>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
            <td className={'text-center align-middle'}>
              {isAllowPermission(appPermissions, PERM_CREATE_MARKET) && (
                <span className={'cursor-pointer'} onClick={() => this.onEditMarket(item)}>
                  <i className="fa fa-pencil color-base-btn" />
                </span>
              )}
            </td>
          </tr>
        );
      })
    ) : (
      <tr>
        <td colSpan={length}>{t('not_found')}</td>
      </tr>
    );
    return <tbody className="text-center">{body}</tbody>;
  };

  render() {
    const { marketDialogStatus, marketItem, isEditMarket } = this.state;
    const { appPermissions, allMarkets } = this.props;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Fragment>
            <Row className="mb-3">
              <Col sm="12" className="d-flex justify-content-end">
                <Button className={'orange-btn'} onClick={this.openMarketDialog} size={'md'}>
                  <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                </Button>
              </Col>
            </Row>
            <Row>
              {isAllowPermission(appPermissions, PERM_GET_LIST_MARKET) && (
                <Table bordered responsive striped>
                  <thead>
                    <tr>
                      <TableHead fields={TABLE_FIELDS} t={t} />
                    </tr>
                  </thead>
                  {this._renderTableBody(calculateLength(TABLE_FIELDS), allMarkets, t)}
                </Table>
              )}
            </Row>
            <MarketDialog
              isOpen={marketDialogStatus}
              onClose={this.onCloseMarketDialog}
              onOk={this.onOkDialog}
              value={marketItem}
              editMode={isEditMarket}
              onRemoveMarket={this.onRemoveMarket}
            />
          </Fragment>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    allMarkets: store.market.allMarkets
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createMarket,
      getListMarket,
      updateMarket,
      removeMarket
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabMarket);
