import { func } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import { clearSaleSetting } from '../../../../../01-actions/sale';
import TabsController from '../../../../../03-components/TabsController';
import TabMarket from './TabMarket/index';
import TabCustomer from './TabCustomer/index';

const CUSTOMER_TAB = [
  {
    value: 1,
    label: 'customer'
  },
  {
    value: 2,
    label: 'market'
  }
];

class CustomerPage extends PureComponent {
  static propTypes = {
    clearSaleSetting: func
  };

  constructor(props) {
    super(props);
    this.state = {
      tabCustomer: CUSTOMER_TAB[0].value
    };
  }

  onChangeTabs = tabCustomer => {
    this.setState({ tabCustomer });
  };

  render() {
    const tabFields = CUSTOMER_TAB.map(item => item.label);
    const { tabCustomer } = this.state;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div className="page">
            <div className="d-flex justify-content-between align-items-center tabs margin-bottom-30px">
              <TabsController allTabsField={tabFields} onChangeTabs={this.onChangeTabs} />
            </div>
            {tabCustomer === CUSTOMER_TAB[0].value && <TabCustomer />}
            {tabCustomer === CUSTOMER_TAB[1].value && <TabMarket />}
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      clearSaleSetting
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(CustomerPage);
