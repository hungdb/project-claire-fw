import { func, array } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Col, Input, Row } from 'reactstrap';
import SelectPropsBox from '../../../../../../03-components/SelectPropsBox';
import { CUSTOMER_STATUS } from '../../../../../../05-utils/commonData';

class CustomerFilter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoryId: '',
      pic: '',
      start: null,
      end: null,
      status: 1,
      marketId: '',
      company: '',
      name: ''
    };
  }

  componentDidMount = () => {
    // this.onFilterCustomer();
  };

  static propTypes = {
    filterCustomer: func,
    listCustomer: array,
    listMarkets: array
  };

  handleChangeType = (selected, e) => {
    const value = selected ? selected.value : '';
    this.setState({ [e.name]: value }, () => {
      this.onFilterCustomer();
    });
  };

  onChangeField = e => {
    this.setState({ [e.target.name]: e.target.value }, () => {
      setTimeout(() => {
        this.onFilterCustomer();
      }, 500);
    });
  };

  onFilterCustomer = () => {
    const { filterCustomer } = this.props;
    const { status, marketId, name, company } = this.state;
    filterCustomer({
      status,
      marketId,
      name,
      company
    });
  };

  render() {
    const { listMarkets } = this.props;
    const { status, marketId } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Row className="mb-4">
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={listMarkets}
                value={marketId}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'market'}
                name={'marketId'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={CUSTOMER_STATUS}
                value={status}
                onChange={this.handleChangeType}
                keyValue="value"
                keyLabel="label"
                isClearable={false}
                placeholder={'status'}
                name={'status'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <Input
                onChange={this.onChangeField}
                className={'flex-grow-1 full focus-border'}
                name="company"
                placeholder={t('company')}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <Input
                onChange={this.onChangeField}
                className={'flex-grow-1 full focus-border'}
                name="name"
                placeholder={t('customer_name')}
              />
            </Col>
          </Row>
        )}
      </NamespacesConsumer>
    );
  }
}

export default CustomerFilter;
