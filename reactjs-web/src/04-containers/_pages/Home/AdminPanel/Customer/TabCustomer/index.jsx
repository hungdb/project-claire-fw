import { func, array, number, string } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col, Table } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'reactstrap';
import {
  PERM_CREATE_CUSTOMER,
  PERM_GET_LIST_CUSTOMER,
  PERM_UPDATE_CUSTOMER
} from '../../../../../../07-constants/app-permission';
import TableHead from '../../../../../../03-components/TableHead';
import UserAvatar from '../../../../../../03-components/UserAvatar';
import { isAllowPermission, calculateLength } from '../../../../../../08-helpers/common';
import CustomerDialog from '../../../../../dialog/customer-dialog/index';
import { TablePagination, Tooltip } from '@material-ui/core';
import {
  createCustomer,
  getListCustomer,
  updateCustomer,
  removeCustomer,
  clearAllDataFormated
} from '../../../../../../01-actions/customer';
import { ROWS_PER_PAGE_OPTIONS } from '../../../../../../05-utils/commonData';
import CustomerFilter from './Filter';
import { getListMarket } from '../../../../../../01-actions/market';
import './style.css';

const TABLE_FIELDS = ['no', 'avatar', 'customer', 'market', 'company', 'note', 'action'];
const PAGE_NUMBER = 1;
const LIMIT_NUMBER = 10;
const TIME_DELAY_CLOSE = 500;

class TabCustomer extends PureComponent {
  static propTypes = {
    appPermissions: array,
    createCustomer: func,
    getListCustomer: func,
    updateCustomer: func,
    removeCustomer: func,
    getListMarket: func,
    clearAllDataFormated: func,

    allCustomers: array,
    totalCustomer: number,
    allMarkets: array,
    status: string
  };

  state = {
    customerStatusDialog: false,
    customerItem: null,
    page: PAGE_NUMBER,
    limit: LIMIT_NUMBER,
    status: 1,
    name: '',
    marketId: '',
    company: '',
    isEditCustomer: false,
    isShowAttachments: false,
    dataSearch: {}
  };

  componentDidMount = () => {
    this.checkRoleAndLoad();
  };

  componentDidUpdate = prevProps => {
    if (this.props.appPermissions !== prevProps.appPermissions) {
      this.checkRoleAndLoad();
    }
  };

  checkRoleAndLoad = () => {
    const { appPermissions } = this.props;
    const { getListMarket } = this.props;
    if (isAllowPermission(appPermissions, PERM_GET_LIST_CUSTOMER)) {
      getListMarket();
    }
  };

  onChangePage = (event, page) => {
    this.setState({ page }, () => this.getListCustomer());
  };

  onChangeRowsPerPage = event => {
    const limit = event.target.value;
    this.setState({ page: 0, limit }, () => this.getListCustomer());
  };

  getListCustomer = () => {
    const { page, limit, dataSearch } = this.state;
    const { name, marketId, company, status } = dataSearch;
    const { getListCustomer } = this.props;
    /*
      when data load is control by 2 function are onChangePage and onChangeRowsPerPage that is to paginate data
      because server can't accpet value page = 0, so must add page is minium equal 1
      */
    const pageValid = page + 1;
    getListCustomer(pageValid, Number(limit), name, marketId, company, status);
  };

  openCustomerDialog = () => {
    this.setState({ customerStatusDialog: true });
  };

  delayTimeWhenClose = () => {
    setTimeout(() => {
      this.setState({
        isEditCustomer: false,
        isShowAttachments: false,
        customerItem: null
      });
    }, TIME_DELAY_CLOSE);
  };

  onCloseCustomerDialog = () => {
    const { clearAllDataFormated } = this.props;
    clearAllDataFormated();
    this.setState(
      {
        customerStatusDialog: false
      },
      this.delayTimeWhenClose()
    );
  };

  onOkDialog = payload => {
    const { isEditCustomer, customerItem } = this.state;
    const { createCustomer, updateCustomer } = this.props;

    if (isEditCustomer && customerItem) {
      if (
        customerItem.name !== payload.name ||
        customerItem.marketId !== payload.marketId ||
        customerItem.note !== payload.note ||
        customerItem.company !== payload.company ||
        customerItem.status !== payload.status ||
        customerItem.avatar !== payload.avatar
      ) {
        updateCustomer(customerItem.id, payload);
      }
    } else {
      createCustomer(payload);
    }
    this.onCloseCustomerDialog();
  };

  onRemoveCustomer = customerid => {
    const { removeCustomer } = this.props;
    removeCustomer(customerid);
    this.onCloseCustomerDialog();
  };

  onFilterCustomer = dataSearch => {
    this.setState(
      {
        page: 0,
        dataSearch
      },
      () => this.getListCustomer()
    );
  };

  _renderTbody = (data, length, t) => {
    const { appPermissions } = this.props;

    const result = calculateLength(data) ? (
      data.map((item, index) => {
        return (
          <tr key={index}>
            <td className="text-center align-middle">{index + 1}</td>
            <td className="text-center align-middle">
              <UserAvatar id={index} user={item} />
            </td>
            <td className="text-center align-middle">{item.name}</td>
            <td className="text-center align-middle">{item.market ? item.market.name : t('not_found')}</td>
            <td className="text-center align-middle">{item.company || t('not_found')}</td>
            <td className="text-center align-middle">{item.note || t('not_found')}</td>
            <td className="text-center align-middle">
              <Tooltip title={t('business_card')} placement="bottom">
                <span
                  className={'cursor-pointer mr-2'}
                  onClick={() =>
                    this.setState(
                      {
                        customerItem: item,
                        isShowAttachments: true
                      },
                      this.openCustomerDialog()
                    )
                  }
                >
                  <i className="fa fa-address-card-o color-base-btn" />
                </span>
              </Tooltip>

              {isAllowPermission(appPermissions, PERM_UPDATE_CUSTOMER) && (
                <Tooltip title={t('edit_customer')} placement="bottom">
                  <span
                    className={'cursor-pointer'}
                    onClick={() =>
                      this.setState(
                        {
                          customerItem: item,
                          isEditCustomer: true
                        },
                        this.openCustomerDialog()
                      )
                    }
                  >
                    <i className="fa fa-pencil color-base-btn" />
                  </span>
                </Tooltip>
              )}
            </td>
          </tr>
        );
      })
    ) : (
      <tr>
        <td colSpan={length}>{t('not_found')}</td>
      </tr>
    );
    return <tbody className="text-center">{result}</tbody>;
  };

  render() {
    const { appPermissions, allCustomers, totalCustomer, allMarkets } = this.props;
    const { customerStatusDialog, customerItem, isEditCustomer, limit, page, isShowAttachments } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <>
            <CustomerFilter listMarkets={allMarkets} filterCustomer={this.onFilterCustomer} />
            <Row className="mb-3">
              <Col sm="12" className="d-flex justify-content-end">
                {isAllowPermission(appPermissions, PERM_CREATE_CUSTOMER) && (
                  <Button className={'orange-btn'} onClick={this.openCustomerDialog} size={'md'}>
                    <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add_customer')}
                  </Button>
                )}
              </Col>
            </Row>
            <Row>
              <Table bordered responsive striped className="customer-table">
                <thead>
                  <tr>
                    <TableHead fields={TABLE_FIELDS} t={t} />
                  </tr>
                </thead>
                {this._renderTbody(allCustomers, calculateLength(TABLE_FIELDS), t)}
                <tfoot>
                  <tr>
                    <TablePagination
                      rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
                      colSpan={calculateLength(TABLE_FIELDS)}
                      count={totalCustomer}
                      rowsPerPage={Number(limit)}
                      page={page}
                      SelectProps={{ native: true }}
                      onChangePage={this.onChangePage}
                      onChangeRowsPerPage={this.onChangeRowsPerPage}
                      labelRowsPerPage={t('rows_per_page')}
                      labelDisplayedRows={({ from, to, count }) => `${from} - ${to} of ${count}`}
                    />
                  </tr>
                </tfoot>
              </Table>
            </Row>
            <CustomerDialog
              isOpen={customerStatusDialog}
              onClose={this.onCloseCustomerDialog}
              onOk={this.onOkDialog}
              value={customerItem}
              editMode={isEditCustomer}
              showMode={isShowAttachments}
              onRemove={this.onRemoveCustomer}
              listMarkets={allMarkets}
              listCustomer={allCustomers}
            />
          </>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    allCustomers: store.customer.allCustomers,
    totalCustomer: store.customer.totalCustomer,
    allMarkets: store.market.allMarkets,
    status: store.customer.status
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createCustomer,
      getListCustomer,
      updateCustomer,
      removeCustomer,
      getListMarket,
      clearAllDataFormated
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabCustomer);
