import './styles.css';
import LazyComponent from '../../../../../03-components/LazyComponent/index';
import SpinLoader from '../../../../../03-components/SpinLoader/SpinLoader';
import TabsPlaceHolder from '../../../../../03-components/PlaceHolderComponent/Tab';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import { array, func } from 'prop-types';

const widthPerItem = [147, 154];

const Filter = LazyComponent(() => import('./Filter/'));
const KPIBehavior = LazyComponent(() => import('./KPIBehavior/'));
const DataTable = LazyComponent(promiseDelayImport(import('./DataTable/')), <SpinLoader color="primary" type="grow" />);
const TabController = LazyComponent(
  promiseDelayImport(import('./TabController')),
  <div className="mb-3">
    <TabsPlaceHolder widthPerItem={widthPerItem} height={38} />
  </div>
);

const TAB_USER_BEHAVIOUR = 0;
const TAB_USER_KPI = 1;

const Behaviours = ({
  listBehaviour,
  getBehaviours,
  createBehaviour,
  updateBehaviour,
  removeBehaviour,
  clearBehaviours,
  clearKPIBehaviour,
  appPermissions
}) => {
  const [state, setState] = useState({
    statusTabBehaviour: TAB_USER_BEHAVIOUR,
    listBehaviourFiltered: []
  });

  const { statusTabBehaviour, listBehaviourFiltered } = state;

  useEffect(() => {
    getBehaviours();
    return () => {
      clearBehaviours();
      clearKPIBehaviour();
    };
  }, []);

  const onChangeTabBehaviour = (e, value) => setState({ ...state, statusTabBehaviour: value });

  const onFilterBehaviour = listBehaviourFiltered => setState({ ...state, listBehaviourFiltered });

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className="page skill-management">
          <TabController
            appPermissions={appPermissions}
            statusTabBehaviour={statusTabBehaviour}
            onChangeTabBehaviour={onChangeTabBehaviour}
          />

          {statusTabBehaviour === TAB_USER_BEHAVIOUR && (
            <>
              <Filter onFilter={onFilterBehaviour} listBehaviour={listBehaviour} />
              <DataTable
                createBehaviour={createBehaviour}
                updateBehaviour={updateBehaviour}
                removeBehaviour={removeBehaviour}
                appPermissions={appPermissions}
                data={listBehaviourFiltered}
              />
            </>
          )}

          {statusTabBehaviour === TAB_USER_KPI && <KPIBehavior />}
        </div>
      )}
    </NamespacesConsumer>
  );
};

Behaviours.propTypes = {
  listBehaviour: array,
  getBehaviours: func,
  createBehaviour: func,
  updateBehaviour: func,
  removeBehaviour: func,
  clearBehaviours: func,
  clearKPIBehaviour: func,
  appPermissions: array
};

export default memo(Behaviours);
