import Multicheck from '../../../../../../03-components/Multicheck';
import styles from './../stylesBehaviour';
import LazyComponent from '../../../../../../03-components/LazyComponent/index';
import TableHead from '../../../../../../03-components/TableHead';
import React, { memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Table } from 'reactstrap';
import { formatDateTime, calculateLength, isAllowPermission } from '../../../../../../08-helpers/common';
import { array, func } from 'prop-types';
import { TYPE_APPLY_BEHAVIOR_OBJECT, TYPE_ACTION_BEHAVIOR_OBJECT } from '../../../../../../05-utils/commonData';
import {
  PERM_LIST_BEHAVIOURS,
  PERM_CREATE_BEHAVIOR,
  PERM_UPDATE_BEHAVIOR
} from '../../../../../../07-constants/app-permission';

const BehaviourDialog = LazyComponent(() => import('../../../../../dialog/behaviour-dialog/behaviour-dialog'));
const BehaviourShowMoreDialog = LazyComponent(() =>
  import('../../../../../dialog/behaviour-dialog/behaviour-show-more-dialog')
);

const DataTable = ({ createBehaviour, updateBehaviour, removeBehaviour, appPermissions, data }) => {
  const [state, setState] = useState({
    itemBehaviour: {},
    showDialogBehaviour: false,
    isEdit: false,
    showMoreDialogBehaviour: false
  });

  const { itemBehaviour, showDialogBehaviour, isEdit, showMoreDialogBehaviour } = state;

  const renderDialogBehavior = () => {
    return (
      <>
        <BehaviourDialog
          appPermissions={appPermissions}
          isOpen={showDialogBehaviour}
          editMode={isEdit}
          itemBehaviour={itemBehaviour}
          onClose={() => setState({ ...state, showDialogBehaviour: false, isEdit: false })}
          onCreate={behaviourCreation => {
            setState({ ...state, showDialogBehaviour: false, isEdit: false });
            behaviourCreation &&
              createBehaviour({
                activity: behaviourCreation.activity,
                point: behaviourCreation.point,
                typeApply: behaviourCreation.typeApply,
                typeAction: behaviourCreation.typeAction,
                isApply: behaviourCreation.isApply,
                startDate: behaviourCreation.startDate,
                endDate: behaviourCreation.endDate,
                description: behaviourCreation.description,
                typeSubApply: behaviourCreation.typeSubApply
              });
          }}
          onSave={behaviourUpdate => {
            setState({ ...state, showDialogBehaviour: false, isEdit: false });
            behaviourUpdate &&
              updateBehaviour({
                behaviorId: behaviourUpdate.behaviorId,
                activity: behaviourUpdate.activity,
                point: behaviourUpdate.point,
                typeApply: behaviourUpdate.typeApply,
                typeAction: behaviourUpdate.typeAction,
                isApply: behaviourUpdate.isApply,
                startDate: behaviourUpdate.startDate,
                endDate: behaviourUpdate.endDate,
                description: behaviourUpdate.description,
                typeSubApply: behaviourUpdate.typeSubApply
              });
          }}
          onRemove={behaviourRemove => {
            setState({ ...state, showDialogBehaviour: false, isEdit: false });
            behaviourRemove && removeBehaviour({ behaviorId: behaviourRemove.id });
          }}
        />
        <BehaviourShowMoreDialog
          isOpen={showMoreDialogBehaviour}
          onClose={() => setState({ ...state, showMoreDialogBehaviour: false })}
          itemBehaviour={itemBehaviour}
        />
      </>
    );
  };

  const renderHeaderBehaviours = t => {
    const fields = [
      'no',
      'behaviour_management.code',
      'behaviour_management.activity',
      'behaviour_management.point',
      'behaviour_management.type_apply',
      'behaviour_management.type_action',
      'behaviour_management.is_apply',
      'behaviour_management.create_at',
      'behaviour_management.action'
    ];
    const classes = [
      'align-middle text-center w-5p',
      'align-middle text-center w-5p',
      'align-middle text-center w-50p',
      'align-middle text-center w-5p',
      'align-middle text-center w-10p',
      'align-middle text-center w-5p',
      'align-middle text-center w-5p',
      'align-middle text-center w-10p',
      'align-middle text-center w-5p'
    ];
    return (
      <thead>
        <tr>
          <TableHead fields={fields} t={t} classes={classes} />
        </tr>
      </thead>
    );
  };

  const renderBodyBehaviour = t => {
    return (
      <tbody>
        {calculateLength(data) ? (
          data
            .map((item, index) => {
              if (item) {
                const number = index + 1;
                const id = item.id || '';
                const code = item.code || '';
                const activity = item.activity || '';
                const point = item.point || '';
                const typeApply =
                  item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.GENERAL
                    ? t('behaviour_management.general')
                    : item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY
                    ? t('behaviour_management.sprint_responsibility')
                    : t('behaviour_management.sprint_dedication');
                const typeAction =
                  item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD
                    ? t('behaviour_management.add')
                    : t('behaviour_management.sub');
                const isApply = item.isApply;
                const createdAt = formatDateTime(item.createdAt, 'LLLL', 1);
                return (
                  <tr key={number}>
                    <td className={'align-middle text-center'}> {number} </td>
                    <td className={'align-middle text-center'}>
                      <strong>{code}</strong>
                    </td>
                    <td className={'align-middle'}> {activity} </td>
                    <td className={'align-middle text-center'}> {point} </td>
                    <td className={'align-middle text-center'}> {typeApply} </td>
                    <td className={'align-middle text-center'}> {typeAction} </td>
                    <td className={'align-middle text-center'}>
                      <Multicheck
                        items={[{ checked: isApply }]}
                        onChange={(index, checked) => {
                          const behaviourUpdate = {
                            behaviorId: id,
                            activity,
                            point,
                            typeApply: item.typeApply,
                            typeAction: item.typeAction,
                            isApply: checked,
                            startDate: item.startDate,
                            endDate: item.endDate,
                            description: item.description,
                            typeSubApply: item.typeSubApply
                          };
                          updateBehaviour(behaviourUpdate);
                        }}
                        styleCheckbox={styles.multiCheck}
                      />
                    </td>
                    <td className={'align-middle text-center'}> {createdAt} </td>
                    <td className={'align-middle text-center'}>
                      <span
                        title={t('edit_behavior')}
                        className={'cursor-pointer margin-right-10px'}
                        onClick={() => setState({ ...state, showMoreDialogBehaviour: true, itemBehaviour: item })}
                      >
                        <i className="fa fa-eye color-base" />
                      </span>
                      {isAllowPermission(appPermissions, PERM_UPDATE_BEHAVIOR) && (
                        <span
                          title={t('edit_behavior')}
                          className={'cursor-pointer'}
                          onClick={() =>
                            setState({ ...state, showDialogBehaviour: true, isEdit: true, itemBehaviour: item })
                          }
                        >
                          <i className="fa fa-pencil color-base-btn" />
                        </span>
                      )}
                    </td>
                  </tr>
                );
              } else {
                return null;
              }
            })
            .filter(Boolean)
        ) : (
          <tr>
            <td colSpan={10} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  const renderContentBehaviour = t => {
    return (
      <div>
        <div className={'d-flex justify-content-end mb-4'}>
          {isAllowPermission(appPermissions, PERM_CREATE_BEHAVIOR) && (
            <Button
              className={'orange-btn'}
              onClick={() => setState({ ...state, showDialogBehaviour: true, isEdit: false })}
              size={'md'}
            >
              <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add_behavior')}
            </Button>
          )}
        </div>
        {isAllowPermission(appPermissions, PERM_LIST_BEHAVIOURS) && (
          <Table bordered responsive striped hover>
            {renderHeaderBehaviours(t)}
            {renderBodyBehaviour(t)}
          </Table>
        )}
      </div>
    );
  };

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <>
          {renderDialogBehavior()}
          {renderContentBehaviour(t)}
        </>
      )}
    </NamespacesConsumer>
  );
};

DataTable.propTypes = {
  createBehaviour: func,
  updateBehaviour: func,
  removeBehaviour: func,
  appPermissions: array,
  data: array
};

export default memo(DataTable);
