import LazyComponent from '../../../../../../03-components/LazyComponent/index';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect, useMemo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col } from 'reactstrap';
import { promiseDelayImport, calculateLength } from '../../../../../../08-helpers/common';
import { func, array } from 'prop-types';
import { ARRAY_TYPE_APPLY, ARRAY_IS_APPLY } from '../../../../../../05-utils/commonData';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const Filter = ({ onFilter, listBehaviour }) => {
  const [state, setState] = useState({
    filterTypeApply: ARRAY_TYPE_APPLY[0].value,
    filterIsApply: ARRAY_IS_APPLY[0].value
  });

  const { filterTypeApply, filterIsApply } = state;

  const onSelectTypeApply = e => setState({ ...state, filterTypeApply: e && e.value });
  const onSelectIsApply = e => setState({ ...state, filterIsApply: e && e.value });

  const computeData = data => {
    return calculateLength(data)
      ? data.filter(item => {
          let isFilter = true;
          if (filterTypeApply) {
            isFilter = isFilter && item.typeApply === filterTypeApply;
          }
          if ([ARRAY_IS_APPLY[0].value, ARRAY_IS_APPLY[1].value].includes(filterIsApply)) {
            isFilter = isFilter && item.isApply === !!filterIsApply;
          }
          return isFilter;
        })
      : [];
  };

  const computeDataMemo = useMemo(() => computeData(listBehaviour), [filterTypeApply, filterIsApply, listBehaviour]);

  useEffect(() => {
    onFilter(computeDataMemo);
  }, [filterTypeApply, filterIsApply, listBehaviour]);

  const renderFilterBehaviour = t => {
    return (
      <Row className="mb-4">
        <Col xs="12" md="6" lg="3" className="pt-1 mb-3 m-md-bottom-0">
          <SelectPropsBox
            placeholder={'behaviour_management.filter_type_apply'}
            value={filterTypeApply}
            options={ARRAY_TYPE_APPLY}
            onChange={onSelectTypeApply}
          />
        </Col>
        <Col xs="12" md="6" lg="3" className="pt-1">
          <SelectPropsBox
            placeholder={'behaviour_management.filter_is_apply'}
            value={filterIsApply}
            options={ARRAY_IS_APPLY}
            onChange={onSelectIsApply}
          />
        </Col>
      </Row>
    );
  };

  return <NamespacesConsumer ns={'translations'}>{t => <>{renderFilterBehaviour(t)}</>}</NamespacesConsumer>;
};

Filter.propTypes = {
  onFilter: func,
  listBehaviour: array
};

export default memo(Filter);
