import LazyComponent from '../../../../../../03-components/LazyComponent';
import TableHead from '../../../../../../03-components/TableHead';
import React, { memo, useState, useMemo, useEffect } from 'react';
import { array, func, object, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { getEnv } from '../../../../../../env';
import { PERM_GET_LIST_KPI_BEHAVIOR } from '../../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { formatDateTime, calculateLength, calcKPIBehaviorMonth } from '../../../../../../08-helpers/common';

const KPIBehaviourDialog = LazyComponent(() => import('../../../../../dialog/behaviour-dialog/kpi-behaviour-dialog'));
const KPIBehaviourInMonthDialog = LazyComponent(() =>
  import('../../../../../dialog/behaviour-dialog/kpi-behaviour-in-month-dialog')
);
const DEFAULT_BEHAVIOR_POINT = getEnv('DEFAULT_BEHAVIOR_POINT');
const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');

const DataTable = ({
  listBehaviour,
  listCurrentUsers,
  currentUser,
  createKPIBehaviour,
  removeKPIBehaviour,
  appPermissions,
  dataFiltered,
  isOpenAddKPIBehaviour, // biến này là props được truyền vào từ component cha để bật modal KPIBehaviourDialog khi thêm mới KPI
  onCloseKPIDialog
}) => {
  const [state, setState] = useState({
    itemKPIBehaviour: {},
    showKPIBehaviourInMonth: false,
    isViewKPIBehaviour: false, // state này xem chi tiết modal KPIBehaviourDialog
    showMode: false // state này là 1 biến cờ để phân biệt modal KPIBehaviourDialog là đang thêm mới hoặc xem chi tiết
  });

  const { itemKPIBehaviour, showKPIBehaviourInMonth, isViewKPIBehaviour, showMode } = state;

  const onClickShowKPIBehavior = itemKPIBehaviour => {
    // khi xem chi tiết KPI behaviour sẽ setState isViewKPIBehaviour = true
    setState({ ...state, isViewKPIBehaviour: true, itemKPIBehaviour, showMode: true });
  };

  const onClickShowKPIBehaviorInMonth = itemKPIBehaviour => {
    setState({ ...state, showKPIBehaviourInMonth: true, itemKPIBehaviour });
  };

  const onCloseKPIBehaviour = () => {
    // Nếu đang add KPI behaviour thì set props ngược lại component cha để đóng KPIBehaviourDialog
    if (isOpenAddKPIBehaviour) onCloseKPIDialog();

    // Nếu đang xem chi tiết KPI behaviour thì sẽ setState isViewKPIBehaviour = false để đóng KPIBehaviourDialog
    if (isViewKPIBehaviour) setState({ ...state, isViewKPIBehaviour: false });
  };

  /* 
    vì KPIBehaviourDialog đang dùng chung cho cả 2 chức năng thêm và xem chi tiết
    nên effect này thực hiện sau khi clear isViewKPIBehaviour để đóng modal thì sẽ delay 1 khoảng thời gian để clear showMode
    sẽ tránh được cảm giác khó chịu khi thay đổi trạng thái chi tiết về thêm mới 1 cách đồng thời khi đóng modal 
  */
  useEffect(() => {
    if (!isViewKPIBehaviour) {
      setTimeout(() => setState({ ...state, showMode: false }), 400);
    }
  }, [isViewKPIBehaviour]);

  const renderKPIDialogBehavior = () => {
    const user = currentUser || listCurrentUsers[0];

    return (
      <KPIBehaviourDialog
        user={user}
        isOpen={isOpenAddKPIBehaviour || isViewKPIBehaviour}
        showMode={showMode}
        itemKPIBehaviour={itemKPIBehaviour}
        listBehaviour={listBehaviour}
        onClose={onCloseKPIBehaviour}
        onCreateKpiBehaviour={itemKPIBehaviour => {
          setState({ ...state, isViewKPIBehaviour: false });
          itemKPIBehaviour &&
            createKPIBehaviour({
              userId: itemKPIBehaviour.userId,
              behaviors: itemKPIBehaviour.behaviors,
              evaluatedAt: itemKPIBehaviour.evaluatedAt
            });
        }}
        defaultPoint={DEFAULT_BEHAVIOR_POINT}
        maxPoint={KPI_MAXIMUM_POINT}
      />
    );
  };

  const onClickRemoveKPIBehavior = id => {
    setState({ ...state, showKPIBehaviourInMonth: false });
    removeKPIBehaviour({ kpiBehaviorId: id });
  };

  const renderKPIDialogBehaviorInMonth = () => {
    const evaluatedAt = formatDateTime(itemKPIBehaviour.evaluatedAt, 'MMM YYYY');

    return (
      <KPIBehaviourInMonthDialog
        appPermissions={appPermissions}
        isOpen={showKPIBehaviourInMonth}
        listKpiBehaviour={itemKPIBehaviour.items}
        onClose={() => setState({ ...state, showKPIBehaviourInMonth: false })}
        onRemove={onClickRemoveKPIBehavior}
        evaluatedAt={evaluatedAt}
      />
    );
  };

  const renderHeaderKPIBehaviour = t => {
    const fields = [
      'no',
      'behaviour_management.KPI_point',
      'actor',
      'user',
      'behaviour_management.evaluated_at',
      'behaviour_management.action'
    ];
    const classes = [
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center',
      'align-middle text-center'
    ];
    return (
      <thead>
        <tr>
          <TableHead fields={fields} t={t} classes={classes} />
        </tr>
      </thead>
    );
  };

  const behaviourMonthMemo = useMemo(() => calcKPIBehaviorMonth(dataFiltered), [dataFiltered]);

  const renderBodyKPIBehaviour = t => {
    return (
      <tbody>
        {calculateLength(behaviourMonthMemo) ? (
          behaviourMonthMemo.map((item, index) => {
            if (item) {
              const number = index + 1;
              const point = item.point || 0;
              let actor = '';
              if (Array.isArray(item.actors) && item.actors.length > 0) {
                actor = item.actors.map(item => item.name).join(', ');
              }
              const user = item.user && item.user.name;
              const evaluatedAt = formatDateTime(item.evaluatedAt, 'MMM YYYY');
              return (
                <tr key={item.id}>
                  <td className={'align-middle text-center'}> {number} </td>
                  <td className={'align-middle text-center'}>
                    <strong className={'color-base'}>{point}</strong>
                  </td>
                  <td className={'align-middle text-center'}> {actor} </td>
                  <td className={'align-middle text-center'}> {user} </td>
                  <td className={'align-middle text-center'}> {evaluatedAt} </td>
                  <td className={'align-middle text-center'}>
                    <span
                      title={t('show_items')}
                      className={'cursor-pointer mr-2'}
                      onClick={() => onClickShowKPIBehaviorInMonth(item)}
                    >
                      <i className="fa fa-history color-base" />
                    </span>
                    <span
                      title={t('show_detail')}
                      className={'cursor-pointer mr-2'}
                      onClick={() => onClickShowKPIBehavior(item)}
                    >
                      <i className="fa fa-eye color-base" />
                    </span>
                  </td>
                </tr>
              );
            }
            return '';
          })
        ) : (
          <tr>
            <td colSpan={10} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div>
          {isAllowPermission(appPermissions, PERM_GET_LIST_KPI_BEHAVIOR) && (
            <Table bordered responsive striped hover>
              {renderHeaderKPIBehaviour(t)}
              {renderBodyKPIBehaviour(t)}
            </Table>
          )}
          {renderKPIDialogBehavior()}
          {renderKPIDialogBehaviorInMonth()}
        </div>
      )}
    </NamespacesConsumer>
  );
};

DataTable.propTypes = {
  listBehaviour: array,
  listCurrentUsers: array,
  getListKpiBehavior: func,
  createKPIBehaviour: func,
  removeKPIBehaviour: func,
  getUsers: func,
  currentUser: object,
  appPermissions: array,
  dataFiltered: array,
  isOpenAddKPIBehaviour: bool,
  onCloseKPIDialog: func
};

export default memo(DataTable);
