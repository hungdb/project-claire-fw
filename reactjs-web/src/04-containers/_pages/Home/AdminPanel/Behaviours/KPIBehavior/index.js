import KPIBehavior from './KPIBehavior';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import { getUsers } from '../../../../../../01-actions/adminproject';
import {
  getListKpiBehavior,
  createKPIBehaviour,
  removeKPIBehaviour,
  createMultipleKPIBehaviour
} from '../../../../../../01-actions/behaviours';

const mapStateToProps = state => {
  return {
    appPermissions: state.auth.permissions,

    listBehaviour: state.behaviour && state.behaviour.listBehaviour,
    listKpiBehaviour: state.behaviour && state.behaviour.listKpiBehaviour,
    listCurrentUsers: state.admins.listUsers,
    statusBehaviour: state.behaviour.status,
    kpiBehaviorId: state.behaviour.kpiBehaviorId
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListKpiBehavior,
      getUsers,
      createKPIBehaviour,
      removeKPIBehaviour,
      createMultipleKPIBehaviour
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KPIBehavior);
