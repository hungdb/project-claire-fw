import LazyComponent from '../../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../../03-components/SpinLoader';
import Skeleton from 'react-loading-skeleton';
import XLSX from 'xlsx';
import Upload from '../../../../../../03-components/Upload';
import i18n from '../../../../../../i18n';
import styles from './styles';
import React, { memo, useState, useEffect } from 'react';
import { array, func, string, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { Col, Row } from 'reactstrap';
import {
  getDateAt000000ZeroGMT,
  isValidDate,
  convertUTCString,
  parseJSON,
  excelDateToJSDate,
  promiseDelayImport
} from '../../../../../../08-helpers/common';
import { mimeTypeFormat } from '../../../../../../08-helpers/mime_types';
import { TYPE_LIST_USER, TOAST_ERROR_STYLE } from '../../../../../../05-utils/commonData';
import { getEnv } from '../../../../../../env';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import {
  PERM_CREATE_MULTIPLE_KPI_BEHAVIOR,
  PERM_CREATE_KPI_BEHAVIOR
} from '../../../../../../07-constants/app-permission';

const Filter = LazyComponent(() => import('./Filter'));
const DataTable = LazyComponent(promiseDelayImport(import('./DataTable')), <SpinLoader color="primary" type="grow" />);
const Button = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/Button')),
  <Skeleton width={108.95} height={34} />
);
const Text = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/Text')),
  <div className={'pull-right'}>
    <Skeleton width={200} />
  </div>
);

const LINK_FORMAT_FILE_KPI_BEHAVIOR = getEnv('LINK_FORMAT_FILE_KPI_BEHAVIOR');

const KPIBehavior = ({
  listBehaviour,
  listKpiBehaviour,
  listCurrentUsers,
  getListKpiBehavior,
  createKPIBehaviour,
  removeKPIBehaviour,
  getUsers,
  createMultipleKPIBehaviour,
  // statusBehaviour,
  // kpiBehaviorId,
  appPermissions
}) => {
  const [state, setState] = useState({
    startDate: new Date(),
    endDate: null,
    currentUser: null,
    showKPIDialogBehaviour: false,
    isSeenKPIBehaviour: false,
    listBehaviorFilter: null,
    openUploadKpi: false,
    showKPIBehaviourInMonth: false,
    listBehaviourFiltered: []
  });

  const { showKPIDialogBehaviour, currentUser, openUploadKpi, listBehaviourFiltered } = state;

  useEffect(() => {
    getUsers(TYPE_LIST_USER.DEFAULT);
    getListKpiBehavior({ userId: null, startDate: null, endDate: null, limit: null, page: null });
  }, []);

  // useEffect(() => {
  //   if (statusBehaviour === REMOVE_KPI_BEHAVIOUR_SUCCESS) {
  //     const itemsUpdate = itemKPIBehaviour.items.filter(item => item.id !== kpiBehaviorId);

  //     const itemKPIBehaviourUpdate = {
  //       ...itemKPIBehaviour,
  //       items: itemsUpdate
  //     };
  //     setState({
  //       ...state,
  //       itemKPIBehaviour: itemKPIBehaviourUpdate
  //     });
  //   }
  // }, [statusBehaviour]);

  const toggleUploadKpi = () => setState({ ...state, openUploadKpi: !openUploadKpi });

  const handleUploadKpi = acceptedFiles => {
    if (acceptedFiles.length === 0 || !acceptedFiles[0]) {
      return toast.error(i18n.t('file_not_accepted'), TOAST_ERROR_STYLE);
    }

    const reader = new FileReader();
    reader.onload = evt => {
      /* Parse data */
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: 'binary' });

      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      let data = XLSX.utils.sheet_to_json(ws);

      if (!Array.isArray(data)) {
        return toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
      }
      if (data.length === 0) {
        return toast.error(i18n.t('missing_required_value'), TOAST_ERROR_STYLE);
      }
      if (data.length > getEnv('LIMIT_ROW_ISSUE_IMPORT_FROM_FILE')) {
        return toast.error(i18n.t('invalid_number_of_rows'), TOAST_ERROR_STYLE);
      }
      const isMissingRequiredValue = data.some(item => {
        return !item || !item.username || !item['list behavior code'];
      });
      if (isMissingRequiredValue) {
        return toast.error(i18n.t('missing_required_value'), TOAST_ERROR_STYLE);
      }

      const isInvalidDate = data.some(item => {
        let evaluatedAt = item['evaluated at'];
        if (evaluatedAt) {
          evaluatedAt = Number.isInteger(evaluatedAt) ? excelDateToJSDate(evaluatedAt) : new Date(evaluatedAt);
          return !isValidDate(evaluatedAt);
        }
        return false;
      });
      if (isInvalidDate) {
        return toast.error(i18n.t('invalid_column_evaluated_at_in_file'), TOAST_ERROR_STYLE);
      }

      data = data.map(item => {
        let codeArray = item['list behavior code'].split(',');
        codeArray = codeArray.map(code => `"${code.trim()}"`);
        const codes = parseJSON(`[${codeArray.join(',')}]`);

        let evaluatedAt = item['evaluated at'];
        if (evaluatedAt) {
          evaluatedAt = Number.isInteger(evaluatedAt) ? excelDateToJSDate(evaluatedAt) : new Date(evaluatedAt);
        } else {
          evaluatedAt = getDateAt000000ZeroGMT(new Date());
        }
        evaluatedAt = convertUTCString(evaluatedAt);
        return {
          username: item.username,
          codes,
          evaluatedAt
        };
      });
      createMultipleKPIBehaviour({ data });
      setState({ ...state, isCreatedFromFile: true });
    };
    reader.readAsBinaryString(acceptedFiles[0]);
  };

  const onFilterAllBehaviour = (listBehaviourFiltered, currentUser) => {
    setState({
      listBehaviourFiltered,
      currentUser
    });
  };

  const onCloseKPIDialog = () => setState({ ...state, showKPIDialogBehaviour: false });

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div>
          <Filter
            listCurrentUsers={listCurrentUsers}
            listKpiBehaviour={listKpiBehaviour}
            onFilter={onFilterAllBehaviour}
          />
          <div className={'d-flex justify-content-end mb-3 m-md-bottom-15'}>
            {isAllowPermission(appPermissions, PERM_CREATE_KPI_BEHAVIOR) && currentUser && (
              <Button type={'orange'} onClick={() => setState({ ...state, showKPIDialogBehaviour: true })}>
                <i className={'fa fa-plus'} aria-hidden={'true'} />
                &nbsp;&nbsp;{t('add_kpi_behavior')}
              </Button>
            )}

            {isAllowPermission(appPermissions, PERM_CREATE_MULTIPLE_KPI_BEHAVIOR) && (
              <Button
                type={'orange'}
                className={'margin-left-10px'}
                onClick={toggleUploadKpi}
                css={openUploadKpi ? styles.importButton : styles.openImportButton}
              >
                <i className={'fa fa-plus margin-right-5px'} />
                {t(openUploadKpi ? 'close_import_file' : 'import_file')}
              </Button>
            )}
          </div>
          {isAllowPermission(appPermissions, PERM_CREATE_MULTIPLE_KPI_BEHAVIOR) && (
            <Row>
              <Col>
                <Text
                  className={'font-size-15px margin-bottom-15px cursor-pointer'}
                  style={{ color: '#0067B1', textDecoration: 'underline', float: 'right' }}
                  click={() => {
                    const win = window.open(LINK_FORMAT_FILE_KPI_BEHAVIOR, '_blank');
                    win.focus();
                  }}
                >
                  <i className={'fa fa-external-link'} /> {t('format_file_kpi_behavior')}
                </Text>
              </Col>
            </Row>
          )}

          {openUploadKpi && (
            <Row className="mb-4">
              <Col md={12}>
                <Upload
                  mimes={[mimeTypeFormat.xls, mimeTypeFormat.xlsx, mimeTypeFormat.csv]}
                  onDrop={handleUploadKpi}
                  multiple={false}
                />
              </Col>
            </Row>
          )}
          <DataTable
            getUsers={getUsers}
            currentUser={currentUser}
            listBehaviour={listBehaviour}
            appPermissions={appPermissions}
            listCurrentUsers={listCurrentUsers}
            onCloseKPIDialog={onCloseKPIDialog}
            dataFiltered={listBehaviourFiltered}
            getListKpiBehavior={getListKpiBehavior}
            createKPIBehaviour={createKPIBehaviour}
            removeKPIBehaviour={removeKPIBehaviour}
            isOpenAddKPIBehaviour={showKPIDialogBehaviour}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

KPIBehavior.propTypes = {
  listBehaviour: array,
  listKpiBehaviour: array,
  listCurrentUsers: array,
  getListKpiBehavior: func,
  createKPIBehaviour: func,
  removeKPIBehaviour: func,
  getUsers: func,
  createMultipleKPIBehaviour: func,
  statusBehaviour: string,
  kpiBehaviorId: number,
  appPermissions: array
};

export default memo(KPIBehavior);
