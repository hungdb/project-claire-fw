const styles = {
  importButton: {
    minWidth: 160
  },
  openImportButton: {
    minWidth: 120
  }
};

export default styles;
