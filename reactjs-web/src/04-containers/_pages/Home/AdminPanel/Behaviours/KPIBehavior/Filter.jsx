import LazyComponent from '../../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect, useMemo } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Col, Row } from 'reactstrap';
import {
  getDateAt000000ZeroGMT,
  getStartOfMonth,
  getEndOfMonth,
  promiseDelayImport,
  calculateLength
} from '../../../../../../08-helpers/common';

const Text = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/Text')),
  <Skeleton width={280} />
);

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);
const ControlDatePicker = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/DateTimePicker')),
  <Skeleton height={38} />
);

const Filter = ({ listKpiBehaviour, listCurrentUsers, onFilter }) => {
  const [state, setState] = useState({
    startDate: new Date(),
    endDate: null,
    currentUser: null
  });

  const { startDate, endDate, currentUser } = state;

  const onChangeStartDate = e => {
    const startDate = e ? getDateAt000000ZeroGMT(e) : null;
    setState({ ...state, startDate });
  };

  const onChangeEndDate = e => {
    const endDate = e ? getDateAt000000ZeroGMT(e) : null;
    setState({ ...state, endDate });
  };

  const onSelectUser = currentUser => setState({ ...state, currentUser });

  const filterResult = listKpiOfUser => {
    return (
      calculateLength(listKpiOfUser) &&
      listKpiOfUser.filter(item => {
        let isFilter = true;
        if (currentUser) {
          isFilter = item && currentUser && item.userId === currentUser.value;
        }
        if (isFilter && startDate) {
          isFilter = isFilter && item.evaluatedAt >= getStartOfMonth(startDate);
        }
        if (isFilter && endDate) {
          isFilter = isFilter && item.evaluatedAt <= getEndOfMonth(endDate);
        }
        return isFilter;
      })
    );
  };

  const filterResultMemo = useMemo(() => filterResult(listKpiBehaviour), [
    currentUser,
    listKpiBehaviour,
    startDate,
    endDate
  ]);

  useEffect(() => {
    onFilter(filterResultMemo, currentUser);
  }, [listKpiBehaviour, currentUser, startDate, endDate]);

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Row>
          <Col xs={12} md={4} lg={4} className="pt-1 mb-3">
            <SelectPropsBox
              options={listCurrentUsers}
              placeholder={t('select_user')}
              value={currentUser && currentUser.value}
              keyValue="id"
              keyLabel="name"
              onChange={onSelectUser}
            />
            <p>
              <Text className={'font-size-13 mt-2'}>(*) {t('select_a_user_to_filter_or_add_a_new_kpi_behavior')}</Text>
            </p>
          </Col>
          <Col xs={12} md={4} lg={4} className="pt-1 mb-3">
            <ControlDatePicker
              value={startDate}
              format="MM/YYYY"
              placeholder="start_date"
              onChange={onChangeStartDate}
            />
          </Col>
          <Col xs={12} md={4} lg={4} className="pt-1 mb-3">
            <ControlDatePicker value={endDate} format="MM/YYYY" placeholder="end_date" onChange={onChangeEndDate} />
          </Col>
        </Row>
      )}
    </NamespacesConsumer>
  );
};

Filter.propTypes = {
  listKpiBehaviour: array,
  listCurrentUsers: array,
  onFilter: func
};

export default memo(Filter);
