import './styles.css';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { array, func, number } from 'prop-types';
import { Button, ButtonGroup } from 'reactstrap';
import {
  PERM_CREATE_MULTIPLE_KPI_BEHAVIOR,
  PERM_GET_LIST_KPI_BEHAVIOR,
  PERM_CREATE_KPI_BEHAVIOR,
  PERM_REMOVE_KPI_BEHAVIOR,
  PERM_LIST_BEHAVIOURS,
  PERM_CREATE_BEHAVIOR,
  PERM_UPDATE_BEHAVIOR,
  PERM_REMOVE_BEHAVIOR
} from '../../../../../07-constants/app-permission';

const behaviorPermissions = [PERM_LIST_BEHAVIOURS, PERM_CREATE_BEHAVIOR, PERM_UPDATE_BEHAVIOR, PERM_REMOVE_BEHAVIOR];
const kpiBehaviorPermissions = [
  PERM_CREATE_MULTIPLE_KPI_BEHAVIOR,
  PERM_GET_LIST_KPI_BEHAVIOR,
  PERM_CREATE_KPI_BEHAVIOR,
  PERM_REMOVE_KPI_BEHAVIOR
];

const getTabUserBehaviour = appPermissions => {
  return {
    BEHAVIOUR: {
      value: 0,
      label: 'behaviour_management.title_behaviour',
      allow: isAllowPermission(appPermissions, behaviorPermissions)
    },
    KPI: {
      value: 1,
      label: 'behaviour_management.title_KPI',
      allow: isAllowPermission(appPermissions, kpiBehaviorPermissions)
    }
  };
};

const TabController = ({ appPermissions, statusTabBehaviour, onChangeTabBehaviour }) => {
  const renderTabBehaviour = (t, statusTabBehaviour) => {
    const TAB_USER_BEHAVIOUR = getTabUserBehaviour(appPermissions);

    return (
      <div className="d-flex justify-content-between align-items-center tabs mb-2 m-sm-bottom-15">
        <ButtonGroup className="button-group-tab">
          {Object.values(TAB_USER_BEHAVIOUR).map(tab => (
            <>
              {tab.allow && (
                <Button
                  key={tab.value}
                  onClick={e => onChangeTabBehaviour(e, tab.value)}
                  active={statusTabBehaviour === tab.value}
                  className="tabs-btn"
                >
                  {t(tab.label)}
                </Button>
              )}
            </>
          ))}
        </ButtonGroup>
      </div>
    );
  };

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => <div>{renderTabBehaviour(t, statusTabBehaviour)}</div>}
    </NamespacesConsumer>
  );
};

TabController.propTypes = {
  listBehaviour: array,
  getBehaviours: func,
  createBehaviour: func,
  updateBehaviour: func,
  removeBehaviour: func,
  clearBehaviours: func,
  clearKPIBehaviour: func,
  appPermissions: array,
  onChangeTabBehaviour: func,
  statusTabBehaviour: number
};

export default memo(TabController);
