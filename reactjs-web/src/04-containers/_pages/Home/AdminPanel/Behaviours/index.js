import Behaviours from './Behaviours';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import {
  createBehaviour,
  getBehaviours,
  removeBehaviour,
  updateBehaviour,
  clearBehaviours,
  clearKPIBehaviour
} from '../../../../../01-actions/behaviours';

const mapStateToProps = state => {
  return {
    appPermissions: state.auth.permissions,
    listBehaviour: state.behaviour && state.behaviour.listBehaviour
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getBehaviours,
      createBehaviour,
      updateBehaviour,
      removeBehaviour,
      clearBehaviours,
      clearKPIBehaviour
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Behaviours);
