import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import { array, func, object } from 'prop-types';
import React, { memo, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { PERM_LIST_WORKFLOW } from '../../../../../07-constants/app-permission';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const PermissionSchemeManagement = LazyComponent(promiseDelayImport(import('./PermissionSchemeManagement')));

const WorkflowManagement = LazyComponent(
  promiseDelayImport(import('./WorkflowManagement/WorkflowManagement')),
  <SpinLoader />
);

function Workflow({ appPermissions, addNotification, getWorkflows, getPermissionSchemes }) {
  const callUpdate = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_WORKFLOW)) {
      getWorkflows();
      getPermissionSchemes();
    }
  };

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    callUpdate();
  }, [appPermissions]);

  return (
    <NamespacesConsumer ns="translations">
      {() => (
        <div className={'page'}>
          <WorkflowManagement />
          <PermissionSchemeManagement />
        </div>
      )}
    </NamespacesConsumer>
  );
}

Workflow.propTypes = {
  getWorkflows: func,
  getPermissionSchemes: func,
  addNotification: func,
  roles: array,
  history: object,
  appPermissions: array
};

export default memo(Workflow);
