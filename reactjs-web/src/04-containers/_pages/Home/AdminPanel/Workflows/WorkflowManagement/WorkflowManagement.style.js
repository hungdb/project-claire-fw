const styles = () => ({
  btnTab: {
    minHeight: 42,
  },
  tableWrap: {
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  formSelect: {
    minWidth: 150,
    marginBottom: 8,
  },
  btnTable: {
    width: 18,
    height: 18,
    border: 'none',
    color: '#3f51b5',
    cursor: 'pointer',
    background: 'transparent',
    '&:disabled': {
      color: '#D0D0D1',
      cursor: 'initial',
    },
  },
});

export {
  styles,
};
