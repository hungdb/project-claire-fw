import './style.css';
import Select from 'react-select';
import AppText from '../../../../../../03-components/AppText';
import DialogWorkflow from '../../../../../dialog/workflow/DialogWorkflow';
import DialogWorkflowState from '../../../../../dialog/workflow/DialogWorkflowState';
import DialogWorkflowTransition from '../../../../../dialog/workflow/DialogWorkflowTransition';
import TabsController from '../../../../../../03-components/TabsController';
import TableHead from '../../../../../../03-components/TableHead';
import {
  getWorkflow,
  getWorkflowStates,
  createWorkflow,
  updateWorkflow,
  removeWorkflow,
  createWorkflowState,
  updateWorkflowState,
  deleteWorkflowState,
  createWorkflowDetail,
  updateWorkflowDetail,
  removeWorkflowDetail
} from '../../../../../../01-actions/workflow';
import MigrateWorkflowDialog from '../../../../../dialog/workflow/migrate-workflow';
import React, { memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { object, array, string, func } from 'prop-types';
import { Table, Row, Col } from 'reactstrap';
import { Badge, Button } from 'reactstrap';
import { customBorderColor, isAllowPermission } from '../../../../../../08-helpers/common';
import {
  CREATE_WORKFLOW_SUCCESS,
  GET_WORKFLOW_SUCCESS,
  REMOVE_WORKFLOW_SUCCESS,
  UPDATE_WORKFLOW_SUCCESS,
  WORKFLOW_STATE_UPDATE_SUCCESS
} from '../../../../../../07-constants/workflow';
import {
  PERM_CREATE_WORKFLOW,
  PERM_CREATE_WORKFLOW_DETAIL,
  PERM_CREATE_WORKFLOW_STATE,
  PERM_MIGRATE_WORKFLOW,
  PERM_UPDATE_WORKFLOW,
  PERM_UPDATE_WORKFLOW_DETAIL,
  PERM_UPDATE_WORKFLOW_STATE
} from '../../../../../../07-constants/app-permission';

const allTabField = ['States', 'Transitions'];
const transitionColumn = ['no', 'from_state', 'to_state', 'description', 'action'];
const stateColumn = ['no', 'state', 'type', 'ordinal', 'action'];

function WorkflowManagement({
  Status,
  workflows,
  listState,
  appPermissions,
  workflowDetail,
  getWorkflow,
  getWorkflowStates,
  createWorkflow,
  updateWorkflow,
  removeWorkflow,
  createWorkflowState,
  updateWorkflowState,
  deleteWorkflowState,
  createWorkflowDetail,
  updateWorkflowDetail,
  removeWorkflowDetail
}) {
  const [currentWorkflow, setCurrentWorkflow] = useState(null);
  const [statusTabWorkFlow, setStatusTabWorkFlow] = useState(1);
  const [showMigrateWorkFlowDialog, setShowMigrateWorkFlowDialog] = useState(false);
  const [workflowCRUD, setWorkflowCRUD] = useState({
    status: false,
    title: '',
    closeDialogWorkflow: () => {
      setWorkflowCRUD({ ...workflowCRUD, ...{ status: false } });
    },
    createUpdateWorkFlow: () => {},
    removeWorkflow: () => {}
  });
  const [workflowStateCRUD, setWorkflowStateCRUD] = useState({
    status: false,
    title: '',
    currentWorkflowStatus: {},
    closeDialogWorkflowState: () => {
      setWorkflowStateCRUD({ ...workflowStateCRUD, ...{ status: false } });
    },
    createUpdateWorkFlowState: () => {},
    deleteWorkflowState: () => {}
  });
  const [workflowTransitionCRUD, setWorkflowTransitionCRUD] = useState({
    status: false,
    title: '',
    currentWorkflowTransition: {},
    closeDialogWorkflowTransition: () => {
      setWorkflowTransitionCRUD({ ...workflowTransitionCRUD, ...{ status: false } });
    },
    createUpdateWorkFlowTransition: () => {},
    removeWorkflowTranstion: () => {}
  });

  useEffect(() => {
    switch (Status) {
      case GET_WORKFLOW_SUCCESS:
        if (workflows.length > 0) {
          const newCurrentWorkflow = workflows[0];
          setCurrentWorkflow(newCurrentWorkflow);
          getWorkflow(newCurrentWorkflow.id);
          getWorkflowStates({ id: newCurrentWorkflow.id });
        }
        break;
      case CREATE_WORKFLOW_SUCCESS: {
        const listIndex = workflows.length - 1;
        const newCurrentWorkflow = workflows[listIndex];
        setCurrentWorkflow(newCurrentWorkflow);
        getWorkflow(newCurrentWorkflow.id);
        getWorkflowStates({ id: newCurrentWorkflow.id });
        break;
      }
      case REMOVE_WORKFLOW_SUCCESS:
        if (workflows.length > 0) {
          const newCurrentWorkflow = workflows[0];
          setCurrentWorkflow(newCurrentWorkflow);
          getWorkflow(newCurrentWorkflow.id);
          getWorkflowStates({ id: newCurrentWorkflow.id });
        } else {
          setCurrentWorkflow({});
        }
        break;
      case UPDATE_WORKFLOW_SUCCESS: {
        const updatedWorkflow = workflows.find(w => w.id === currentWorkflow.id);
        if (currentWorkflow !== updatedWorkflow) {
          setCurrentWorkflow(updatedWorkflow);
        }
        break;
      }
      case WORKFLOW_STATE_UPDATE_SUCCESS:
        if (currentWorkflow && currentWorkflow.id) {
          getWorkflow(currentWorkflow.id);
        }
        break;
      default:
        break;
    }
  });

  const handleSelectWorkFlow = select => {
    const selectWorkflow = workflows.find(w => w.id === select.value);
    setCurrentWorkflow(selectWorkflow);
    getWorkflow(selectWorkflow.id);
    getWorkflowStates({ id: selectWorkflow.id });
  };

  const changeTabWorkFlow = statusTabWorkFlow => {
    setStatusTabWorkFlow(statusTabWorkFlow);
  };

  const openDialogWorkflowCreate = () => {
    setWorkflowCRUD({
      ...workflowCRUD,
      ...{
        status: 1,
        title: 'New',
        createUpdateWorkFlow: data => {
          createWorkflow(data);
        },
        removeWorkflow: () => {}
      }
    });
  };

  const openDialogWorkflowEdit = () => {
    setWorkflowCRUD({
      ...workflowCRUD,
      ...{
        status: 2,
        title: 'Edit',
        createUpdateWorkFlow: data => {
          updateWorkflow({
            id: data.id,
            name: data.name,
            allowParallelTask: data.allowParallelTask
          });
        },
        removeWorkflow: data => {
          removeWorkflow(data);
        }
      }
    });
  };

  const openDialogWorkflowStateCreate = () => {
    setWorkflowStateCRUD({
      ...workflowStateCRUD,
      ...{
        status: 1,
        title: 'New',
        currentWorkflowStatus: {},
        createUpdateWorkFlowState: data => {
          createWorkflowState(data);
        },
        deleteWorkflowState: () => {}
      }
    });
  };

  const openDialogWorkflowStateEdit = item => {
    setWorkflowStateCRUD({
      ...workflowStateCRUD,
      ...{
        status: 2,
        title: 'Edit',
        currentWorkflowStatus: item,
        createUpdateWorkFlowState: data => {
          updateWorkflowState(data);
        },
        deleteWorkflowState: data => {
          deleteWorkflowState(data);
        }
      }
    });
  };

  const openDialogWorkflowTransitionCreate = () => {
    setWorkflowTransitionCRUD({
      ...workflowTransitionCRUD,
      ...{
        status: 1,
        title: 'New',
        currentWorkflowTransition: {},
        createUpdateWorkFlowTransition: data => {
          createWorkflowDetail(data);
        },
        deleteWorkflowTransition: () => {}
      }
    });
  };

  const openDialogWorkflowTransitionEdit = item => {
    setWorkflowTransitionCRUD({
      ...workflowTransitionCRUD,
      ...{
        status: 2,
        title: 'Edit',
        currentWorkflowTransition: item,
        createUpdateWorkFlowTransition: data => {
          updateWorkflowDetail(data);
        },
        deleteWorkflowTransition: data => {
          removeWorkflowDetail(data);
        }
      }
    });
  };

  const _renderWorkflowSelector = t => {
    const obj = !currentWorkflow
      ? null
      : {
          label: currentWorkflow.name,
          value: currentWorkflow.id
        };
    return (
      <Select
        placeholder={t('select_project_workflow')}
        value={obj}
        styles={customBorderColor}
        options={workflows.map(p => ({ label: p.name, value: p.id }))}
        onChange={handleSelectWorkFlow.bind(this)}
      />
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'margin-bottom-40px'}>
          <MigrateWorkflowDialog
            isOpen={showMigrateWorkFlowDialog}
            onClose={() => setShowMigrateWorkFlowDialog(false)}
          />
          <DialogWorkflow currentWorkflow={currentWorkflow} workflowCRUD={workflowCRUD} />
          <DialogWorkflowState currentWorkflow={currentWorkflow} workflowStateCRUD={workflowStateCRUD} />
          <DialogWorkflowTransition
            currentWorkflow={currentWorkflow}
            listWorkflowState={listState}
            workflowTransitionCRUD={workflowTransitionCRUD}
          />
          <div className={'margin-bottom-15px'}>
            <Row className="mb-4">
              <Col sm="4">
                <AppText type={'title-header'}>{t('workflows')}</AppText>
              </Col>
              <Col sm="8" className="workflow-top-btn">
                {isAllowPermission(appPermissions, PERM_MIGRATE_WORKFLOW) && (
                  <Button
                    className={'orange-btn multi-btn'}
                    onClick={() => setShowMigrateWorkFlowDialog(true)}
                    size={'md'}
                  >
                    <i className={'fa fa-arrow-right margin-right-5px'} /> {t('migrate_workflow')}
                  </Button>
                )}

                {isAllowPermission(appPermissions, PERM_CREATE_WORKFLOW) && (
                  <Button className={'orange-btn'} onClick={openDialogWorkflowCreate} size={'md'}>
                    <i className={'fa fa-plus margin-right-5px'} /> {t('add_workflow')}
                  </Button>
                )}
              </Col>
            </Row>
            <Row>
              <Col sm="4" className="mb-3 select-workflow max-width-240px">
                {_renderWorkflowSelector(t)}
              </Col>
              {isAllowPermission(appPermissions, PERM_UPDATE_WORKFLOW) && (
                <Col sm="8">
                  <Button
                    className={'default-btn'}
                    onClick={openDialogWorkflowEdit}
                    disabled={!currentWorkflow}
                    size={'md'}
                  >
                    <i className="fa fa-pencil" aria-hidden="true" />
                    &nbsp;&nbsp;{t('edit')}
                  </Button>
                </Col>
              )}
            </Row>
          </div>

          {currentWorkflow && (
            <div>
              <div className={'text-right'}>
                {currentWorkflow.allowParallelTask && <Badge color="warning">{t('parallel_task_enabled')}</Badge>}
              </div>
              <div className="d-flex justify-content-between align-items-center mb-4">
                <TabsController allTabsField={allTabField} onChangeTabs={changeTabWorkFlow} />
              </div>
              {statusTabWorkFlow === 1 && (
                <Table hover responsive striped className={'default-paper table-no-border font-size-13'}>
                  <thead>
                    <tr>
                      <TableHead fields={stateColumn} t={t} />
                      <th>
                        <div className={'d-flex justify-content-end'}>
                          {isAllowPermission(appPermissions, PERM_CREATE_WORKFLOW_STATE) && (
                            <Button
                              className={'default-btn'}
                              onClick={openDialogWorkflowStateCreate}
                              disabled={!currentWorkflow.id}
                              size={'md'}
                            >
                              <i className="fa fa-plus" aria-hidden="true" />
                              &nbsp;&nbsp;{t('add')}
                            </Button>
                          )}
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {listState.map((item, index) => {
                      return (
                        <tr key={item.id}>
                          <td className={' text-center align-middle'}>{index + 1}</td>
                          <td className={' text-center align-middle text-uppercase'}>{item.name}</td>
                          <td className={' text-center align-middle text-uppercase'}>{item.effortType}</td>
                          <td className={' text-center align-middle'}>{item.ordinalNumber}</td>
                          <td className={' text-center align-middle'}>
                            {isAllowPermission(appPermissions, PERM_UPDATE_WORKFLOW_STATE) && (
                              <span className={'cursor-pointer'} onClick={() => openDialogWorkflowStateEdit(item)}>
                                <i className="fa fa-pencil color-base-btn" />
                              </span>
                            )}
                          </td>
                          <td />
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              )}
              {statusTabWorkFlow === 2 && (
                <Table hover responsive striped className={'default-paper table-no-border font-size-13'}>
                  <thead>
                    <tr>
                      <TableHead fields={transitionColumn} t={t} />
                      <th>
                        <div className={'d-flex justify-content-end'}>
                          {isAllowPermission(appPermissions, PERM_CREATE_WORKFLOW_DETAIL) && (
                            <Button
                              className={'default-btn'}
                              onClick={openDialogWorkflowTransitionCreate}
                              disabled={!currentWorkflow.id}
                              size={'md'}
                            >
                              <i className="fa fa-plus" aria-hidden="true" />
                              &nbsp;&nbsp;{t('add')}
                            </Button>
                          )}
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {workflowDetail.map((item, index) => {
                      return (
                        <tr key={item.id}>
                          <td className={' text-center align-middle'}>{index + 1}</td>
                          <td className={' text-center align-middle text-uppercase'}>{item.fromStates.name}</td>
                          <td className={' text-center align-middle text-uppercase'}>{item.toStates.name}</td>
                          <td className={' text-center align-middle'}>{item.description}</td>
                          <td className={' text-center align-middle'}>
                            {isAllowPermission(appPermissions, PERM_UPDATE_WORKFLOW_DETAIL) && (
                              <span className={'cursor-pointer'} onClick={() => openDialogWorkflowTransitionEdit(item)}>
                                <i className="fa fa-pencil color-base-btn" />
                              </span>
                            )}
                          </td>
                          <td />
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              )}
            </div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

WorkflowManagement.propTypes = {
  classes: object,
  Status: string,
  workflows: array,
  listState: array,
  workflowDetail: array,
  appPermissions: array,
  getWorkflow: func,
  getWorkflowStates: func,
  createWorkflow: func,
  updateWorkflow: func,
  removeWorkflow: func,
  createWorkflowState: func,
  updateWorkflowState: func,
  deleteWorkflowState: func,
  createWorkflowDetail: func,
  updateWorkflowDetail: func,
  removeWorkflowDetail: func
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    error: store.common.error,
    Status: store.workflow.status,
    workflowError: store.workflow.workflowError,
    workflows: store.workflow.workflows,
    listState: store.workflow.listState,
    workflowDetail: store.workflow.workflowDetail
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getWorkflow,
      getWorkflowStates,
      createWorkflow,
      updateWorkflow,
      removeWorkflow,
      createWorkflowState,
      updateWorkflowState,
      deleteWorkflowState,
      createWorkflowDetail,
      updateWorkflowDetail,
      removeWorkflowDetail
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(WorkflowManagement));
