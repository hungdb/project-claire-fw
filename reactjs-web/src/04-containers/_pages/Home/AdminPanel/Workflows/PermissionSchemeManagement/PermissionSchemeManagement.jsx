import { array, func, object } from 'prop-types';
import React, { useEffect, useState, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Col, Row, Table } from 'reactstrap';
import AppText from '../../../../../../03-components/AppText';
import TableHead from '../../../../../../03-components/TableHead';
import {
  PERM_CREATE_PERMISSION_SCHEME,
  PERM_CREATE_PERMISSION_SCHEME_DETAIL,
  PERM_CREATE_PROJECT_ROLE,
  PERM_EDIT_PERMISSION_SCHEME,
  PERM_UPDATE_PERMISSION_SCHEME_DEATAIL
} from '../../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import PermissionSchemeDetailDialog from '../../../../../dialog/permission-scheme-detail/permission-scheme-detail';
import ProjectPermissionSchemeDialog from '../../../../../dialog/permission-scheme/permission-scheme';
import ProjectRoleDialog from '../../../../../dialog/project-role/project-role';
import PermissionSchemeSelector from '../../../../../permission-scheme-selector/permission-scheme-selector';
import ProjectRoleSelector from '../../../../../project-role-selector/project-role-selector';
import RoleList from './RoleList';
import './style.css';

function PermissionSchemeManagement({
  getPermissionSchemes,
  getProjectRoles,
  listAllPermissions,
  getPermissionsOfProjectRole,
  getPermissionDetail,
  clearPermissionScheme,
  permissionsOfProjectRole,
  projectRoles,
  removePermissionScheme,
  updatePermissionScheme,
  createPermissionScheme,
  removeProjectRole,
  updateProjectRole,
  createProjectRole,
  removePermissionSchemeDetail,
  updatePermissionSchemeDetail,
  createPermissionSchemeDetail,
  permissionSchemes,
  appPermissions,
  allPermissions
}) {
  const [projectRoleId, setProjectRoleId] = useState(0);
  const [editPermissionScheme, setEditPermissionScheme] = useState(null);
  const [editPermissionSchemeDetail, setEditPermissionSchemeDetail] = useState(null);
  const [showAddPermissionSchemeDialog, setShowAddPermissionSchemeDialog] = useState(false);
  const [showAddPermissionSchemeDetailDialog, setShowAddPermissionSchemeDetailDialog] = useState(false);
  const [showAddProjectRoleDialog, setShowAddProjectRoleDialog] = useState(false);
  const [selectedPermissionScheme, setSelectedPermissionScheme] = useState(null);

  useEffect(() => {
    getPermissionSchemes();
    getProjectRoles();
    listAllPermissions();
    return () => {
      clearPermissionScheme();
    };
  }, []);

  useEffect(() => {
    if (!selectedPermissionScheme && permissionSchemes.length) {
      setSelectedPermissionScheme(permissionSchemes[0]);
    }
  });

  const roleListColumn = ['no', 'role', 'priority_level', 'action'];
  const permissionSchemaColumn = ['no', 'permission', 'setting', 'action'];

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'margin-bottom-40px'}>
            <div className={'d-flex justify-content-between align-items-center margin-bottom-30px'}>
              <AppText type={'title-header'}>{t('project_roles')}</AppText>
            </div>
            <Table hover responsive striped className={'default-paper table-no-border font-size-13'}>
              <thead>
                <tr>
                  <TableHead fields={roleListColumn} t={t} />
                  <th>
                    <div className={'d-flex justify-content-end'}>
                      {isAllowPermission(appPermissions, PERM_CREATE_PROJECT_ROLE) && (
                        <Button
                          className={'default-btn'}
                          onClick={() => {
                            setShowAddProjectRoleDialog(true);
                          }}
                          size={'md'}
                        >
                          <i className="fa fa-plus" aria-hidden="true" />
                          &nbsp;&nbsp;{t('add')}
                        </Button>
                      )}
                    </div>
                  </th>
                </tr>
              </thead>
              <RoleList
                appPermissions={appPermissions}
                projectRoles={projectRoles}
                onUpdate={updateProjectRole}
                onDelete={removeProjectRole}
              />
            </Table>
          </div>

          <Row>
            <Col sm="4">
              <AppText type={'title-header'}>{t('permission_schemes')}</AppText>
            </Col>
            {isAllowPermission(appPermissions, PERM_CREATE_PERMISSION_SCHEME) && (
              <Col sm="8" className="permission-scheme-btn mb-3">
                <Button
                  className={'orange-btn'}
                  onClick={() => {
                    setShowAddPermissionSchemeDialog(true);
                    setEditPermissionScheme(null);
                  }}
                  size={'md'}
                >
                  <i className={'fa fa-plus margin-right-5px'} /> {t('add_scheme')}
                </Button>
              </Col>
            )}
          </Row>
          <Row className="mb-4">
            <Col sm="6" className="max-width-240px">
              <PermissionSchemeSelector
                value={selectedPermissionScheme}
                onChange={e => {
                  setSelectedPermissionScheme(permissionSchemes.find(s => s.id === e.value));
                  getPermissionDetail({ id: e.value });
                  if (projectRoleId) {
                    getPermissionsOfProjectRole({
                      permissionSchemeId: e.value,
                      projectRoleId: projectRoleId
                    });
                  }
                }}
              />
            </Col>
            {isAllowPermission(appPermissions, PERM_EDIT_PERMISSION_SCHEME) && (
              <Col sm="6" className="edit-scheme-btn">
                <Button
                  className={'default-btn'}
                  onClick={() => {
                    setShowAddPermissionSchemeDialog(true);
                    setEditPermissionScheme(selectedPermissionScheme);
                  }}
                  size={'md'}
                >
                  <i className="fa fa-pencil" aria-hidden="true" />
                  &nbsp;&nbsp;{t('edit_scheme')}
                </Button>
              </Col>
            )}
          </Row>

          {selectedPermissionScheme && (
            <div>
              <div>
                <div className={'max-width-240px margin-bottom-15px'}>
                  <ProjectRoleSelector
                    onChange={e => {
                      setProjectRoleId(e.value);
                      getPermissionsOfProjectRole({
                        permissionSchemeId: selectedPermissionScheme.id,
                        projectRoleId: e.value
                      });
                    }}
                  />
                </div>
              </div>
              {!!projectRoleId && (
                <Table hover responsive striped className={'default-paper table-no-border font-size-13'}>
                  <thead>
                    <tr>
                      <TableHead fields={permissionSchemaColumn} t={t} />
                      <th>
                        <div className={'d-flex justify-content-end align-items-center'}>
                          {isAllowPermission(appPermissions, PERM_CREATE_PERMISSION_SCHEME_DETAIL) && (
                            <Button
                              className={'default-btn'}
                              disabled={!selectedPermissionScheme || !projectRoleId}
                              onClick={() => {
                                setShowAddPermissionSchemeDetailDialog(true);
                                setEditPermissionSchemeDetail(null);
                              }}
                              size={'md'}
                            >
                              <i className="fa fa-plus" aria-hidden="true" />
                              &nbsp;&nbsp;{t('add')}
                            </Button>
                          )}
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {permissionsOfProjectRole ? (
                      permissionsOfProjectRole.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td className={'text-center align-middle'}>{index + 1}</td>
                            <td className={'text-center align-middle'}>{item.permission}</td>
                            <td className={'text-center align-middle'}>{item.permissionCondition}</td>
                            <td className={'text-center align-middle'}>
                              {isAllowPermission(appPermissions, PERM_UPDATE_PERMISSION_SCHEME_DEATAIL) && (
                                <span
                                  className={'cursor-pointer'}
                                  onClick={() => {
                                    setShowAddPermissionSchemeDetailDialog(true);
                                    setEditPermissionSchemeDetail(item);
                                  }}
                                >
                                  <i className="fa fa-pencil color-base-btn" />
                                </span>
                              )}
                            </td>
                            <td />
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td colSpan={4}>{t('no_projects_permission')}</td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              )}

              <PermissionSchemeDetailDialog
                isOpen={showAddPermissionSchemeDetailDialog}
                editMode={!!editPermissionSchemeDetail}
                permissions={allPermissions && allPermissions.filter(p => p.projectRelated)}
                permission={editPermissionSchemeDetail}
                onClose={() => {
                  setShowAddPermissionSchemeDetailDialog(false);
                }}
                onOK={({ permission, permissionConditions }) => {
                  setShowAddPermissionSchemeDetailDialog(false);
                  if (editPermissionSchemeDetail) {
                    if (!permission) {
                      removePermissionSchemeDetail({
                        permissionSchemeDetailId: editPermissionSchemeDetail.id
                      });
                    } else {
                      updatePermissionSchemeDetail({
                        permissionSchemeId: selectedPermissionScheme.id,
                        permissionSchemeDetailId: editPermissionSchemeDetail.id,
                        projectRoleId: projectRoleId,
                        permission,
                        permissionConditions
                      });
                    }
                  } else {
                    createPermissionSchemeDetail({
                      permissionSchemeId: selectedPermissionScheme.id,
                      projectRoleId: projectRoleId,
                      permission,
                      permissionConditions
                    });
                  }
                }}
              />
            </div>
          )}
          <ProjectPermissionSchemeDialog
            isOpen={showAddPermissionSchemeDialog}
            editMode={!!editPermissionScheme}
            onClose={() => {
              setShowAddPermissionSchemeDialog(false);
            }}
            permissionScheme={editPermissionScheme}
            value={editPermissionScheme}
            onOK={({ permissionSchemeName }) => {
              if (editPermissionScheme) {
                if (!permissionSchemeName) {
                  removePermissionScheme({
                    id: editPermissionScheme.id
                  });
                  setSelectedPermissionScheme(null);
                } else {
                  const selectedPermissionScheme = {
                    id: editPermissionScheme.id,
                    name: permissionSchemeName
                  };
                  updatePermissionScheme(selectedPermissionScheme);
                  setSelectedPermissionScheme(selectedPermissionScheme);
                }
              } else {
                createPermissionScheme({
                  name: permissionSchemeName
                });
              }
              setShowAddPermissionSchemeDialog(false);
            }}
          />
          <ProjectRoleDialog
            isOpen={showAddProjectRoleDialog}
            editMode={false}
            valuePR={{ name: '', id: projectRoleId }}
            onClose={() => {
              setShowAddProjectRoleDialog(false);
            }}
            onOK={(editMode, removeMode, value) => {
              setShowAddProjectRoleDialog(false);
              createProjectRole(value);
            }}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

PermissionSchemeManagement.propTypes = {
  getPermissionSchemes: func,
  getProjectRoles: func,
  listAllPermissions: func,
  getPermissionsOfProjectRole: func,
  getPermissionDetail: func,
  clearPermissionScheme: func,
  permissionsOfProjectRole: array,
  projectRoles: array,
  removePermissionScheme: func,
  updatePermissionScheme: func,
  createPermissionScheme: func,
  removeProjectRole: func,
  updateProjectRole: func,
  createProjectRole: func,
  removePermissionSchemeDetail: func,
  updatePermissionSchemeDetail: func,
  createPermissionSchemeDetail: func,
  permissionSchemes: array,
  value: object,
  appPermissions: array,
  allPermissions: array
};

export default memo(PermissionSchemeManagement);
