import React, { memo, useState } from 'react';
import ProjectRoleDialog from '../../../../../dialog/project-role/project-role';
import { array, func } from 'prop-types';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { PERM_UPDATE_PROJECT_ROLE } from '../../../../../../07-constants/app-permission';

function RoleList({ projectRoles, appPermissions, onDelete, onUpdate }) {
  const [showAddProjectRoleDialog, setShowAddProjectRoleDialog] = useState(false);
  const [editProjectRole, setEditProjectRole] = useState(null);
  const [namePR, setNamePR] = useState('');
  const [priorityLevel, setPriorityLevel] = useState(null);
  const [projectRoleId, setProjectRoleId] = useState(0);

  return (
    <tbody>
      {projectRoles.map((item, index) => {
        const priorityLevel = item.priorityLevel || 'none';
        return (
          <tr key={index}>
            <td className={'text-center align-middle'}>{index + 1}</td>
            <td className={'text-center align-middle'}>{item.name}</td>
            <td className={'text-center align-middle'}>{priorityLevel}</td>
            <td className={'text-center align-middle'}>
              {isAllowPermission(appPermissions, PERM_UPDATE_PROJECT_ROLE) && (
                <span
                  className={'cursor-pointer'}
                  onClick={() => {
                    setShowAddProjectRoleDialog(true);
                    setEditProjectRole(1);
                    setNamePR(item.name);
                    setPriorityLevel(item.priorityLevel);
                    setProjectRoleId(item.id);
                  }}
                >
                  <i className="fa fa-pencil color-base-btn" />
                </span>
              )}
            </td>
            <td />
          </tr>
        );
      })}
      <ProjectRoleDialog
        isOpen={showAddProjectRoleDialog}
        editMode={!!editProjectRole}
        valuePR={{ name: namePR, id: projectRoleId, priorityLevel: priorityLevel }}
        onClose={() => {
          setShowAddProjectRoleDialog(false);
        }}
        onOK={(editMode, removeMode, value) => {
          setShowAddProjectRoleDialog(false);
          if (editMode) {
            if (removeMode) {
              onDelete(value);
            } else {
              onUpdate(value);
            }
          }
        }}
      />
    </tbody>
  );
}

RoleList.propTypes = {
  projectRoles: array,
  appPermissions: array,
  onDelete: func,
  onUpdate: func
};

export default memo(RoleList);
