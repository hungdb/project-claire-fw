import connect from 'react-redux/es/connect/connect';
import PermissionSchemeManagement from './PermissionSchemeManagement';
import { bindActionCreators } from 'redux';
import {
  getPermissionSchemes,
  createPermissionScheme,
  getPermissionDetail,
  getProjectRoles,
  getPermissionsOfProjectRole,
  updatePermissionScheme,
  createProjectRole,
  updateProjectRole,
  createPermissionSchemeDetail,
  removePermissionScheme,
  removePermissionSchemeDetail,
  removeProjectRole,
  updatePermissionSchemeDetail,
  clearPermissionScheme
} from '../../../../../../01-actions/permission-scheme';
import { addNotification } from '../../../../../../01-actions/notification';
import { listAllPermissions } from '../../../../../../01-actions/permission';

const mapStateToProps = store => {
  return {
    // common
    error: store.common.error,
    Status: store.permissionScheme.status,
    permissionSchemeError: store.permissionScheme.permissionSchemeError,
    permissionSchemes: store.permissionScheme.permissionSchemes,
    projectRoles: store.permissionScheme.projectRoles,
    permissionSchemeDetail: store.permissionScheme.permissionSchemeDetail,
    permissionsOfProjectRole: store.permissionScheme.permissionsOfProjectRole,
    allPermissions: store.permission.allPerms,
    appPermissions: store.auth.permissions
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getPermissionSchemes,
      createPermissionScheme,
      getPermissionDetail,
      getProjectRoles,
      getPermissionsOfProjectRole,
      updatePermissionScheme,
      createProjectRole,
      updateProjectRole,
      createPermissionSchemeDetail,
      removePermissionScheme,
      removePermissionSchemeDetail,
      removeProjectRole,
      updatePermissionSchemeDetail,
      clearPermissionScheme,
      addNotification,
      listAllPermissions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PermissionSchemeManagement);
