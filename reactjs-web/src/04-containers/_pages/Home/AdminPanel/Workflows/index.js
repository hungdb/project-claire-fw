import Workflow from './Workflow';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createWorkflow, getWorkflows } from '../../../../../01-actions/workflow';
import { createPermissionScheme, getPermissionSchemes } from '../../../../../01-actions/permission-scheme';
import { addNotification } from '../../../../../01-actions/notification';

const mapStateToProps = store => {
  return {
    // auth
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,
    workflows: store.workflow.workflows,
    permissionSchemes: store.permissionScheme.schemes
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createWorkflow,
      getWorkflows,
      createPermissionScheme,
      getPermissionSchemes,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Workflow);
