import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_tooltip';
import moment from 'moment';
import i18n from '../../../../../i18n';
import avatarDefault from '../../../../../06-assets/default_avatar.png';
import React, { memo, useRef, useEffect } from 'react';
import { object } from 'prop-types';
import { linkImage } from '../../../../../08-helpers/common';

function Chart({ data }) {
  const ganttContainer = useRef(null);

  const initGanttChart = () => {
    const daysStyle = date => {
      const today = new Date();
      if (date.getDay() === 6 || date.getDay() === 0) return 'text-white allocations-chart-dayoff';
      if (
        date.getFullYear() === today.getFullYear() &&
        date.getMonth() === today.getMonth() &&
        date.getDate() === today.getDate()
      )
        return 'text-white bg-warning admin-allocations-chart-today';
      return '';
    };

    window.gantt.config.readonly = true;
    window.gantt.config.show_errors = false;
    window.gantt.config.scale_height = 50;
    window.gantt.config.scale_unit = 'month';
    window.gantt.config.date_scale = '%F, %Y';
    window.gantt.config.subscales = [{ unit: 'day', step: 1, date: '%j, %D', css: daysStyle }];
    window.gantt.config.columns = [{ name: 'title', label: i18n.t('user'), tree: true, width: 180 }];
    window.gantt.config.layout = {
      css: 'gantt_container',
      cols: [
        {
          width: window.innerWidth >= 992 ? 180 : 180,
          rows: [
            {
              view: 'grid',
              scrollX: 'scrollHorGrid',
              scrollY: 'scrollVer',
              scrollable: true
            },
            {
              view: 'scrollbar',
              id: 'scrollHorGrid'
            }
          ]
        },
        {
          rows: [
            {
              view: 'timeline',
              scrollX: 'scrollHorTimeline',
              scrollY: 'scrollVer'
            },
            {
              view: 'scrollbar',
              id: 'scrollHorTimeline'
            }
          ]
        },
        {
          view: 'scrollbar',
          id: 'scrollVer'
        }
      ]
    };
    window.gantt.templates.task_class = (start, end, task) => {
      if (task.level === 1 || task.isDisable) {
        return 'd-none';
      }
      // if (task.freeEffort === 100) {
      //   return 'allocation-free-color-status';
      // }
    };
    window.gantt.templates.tooltip_text = (start, end, task) => {
      if (task.level === 2) {
        return `
        <b>${i18n.t('start_date')}</b> ${moment(start).format('DD/MM/YYYY, HH:mm:ss')}<br/>
        <b>${i18n.t('end_date')}</b> ${moment(end).format('DD/MM/YYYY, HH:mm:ss')}<br/>
        `;
      }
      if (task.level === 3) {
        return `
        ${task.tooltip.map(t => `<b>${i18n.t('effort')}</b> ${t.effort}%<br/>`).join('')}
        <b>${i18n.t('start_date')}</b> ${moment(start).format('DD/MM/YYYY, HH:mm:ss')}<br/>
        <b>${i18n.t('end_date')}</b> ${moment(end).format('DD/MM/YYYY, HH:mm:ss')}<br/>
        <b>${i18n.t('free')}</b> ${task.freeEffort}%<br/>
        `;
      }
    };

    window.gantt.templates.grid_file = item => {
      const avatar = item && item.avatar ? linkImage(item.avatar) : avatarDefault;
      return `<img class="gantt-avatar-icon" src="${avatar}"></img>`;
    };

    window.gantt.init(ganttContainer.current);
  };

  const resizeGanttChart = () => {
    clearTimeout(window.timer);
    window.timer = setTimeout(() => {
      initGanttChart();
    }, 300);
  };

  useEffect(() => {
    initGanttChart();
    window.gantt.parse(data);
    // data.data.forEach(t => {
    //   !t.parent && window.gantt.open(t.id);
    // });
    window.addEventListener('resize', resizeGanttChart);

    return () => {
      window.gantt.clearAll();
      window.removeEventListener('resize', resizeGanttChart);
    };
  }, []);

  useEffect(() => {
    window.gantt.clearAll();
    window.gantt.parse(data);
  });

  return <div ref={ganttContainer} style={{ width: '100%', height: '68vh' }} />;
}

Chart.propTypes = {
  data: object
};

export default memo(Chart);
