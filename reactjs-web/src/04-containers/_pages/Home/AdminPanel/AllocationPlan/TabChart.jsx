import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useEffect, useState } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { clearAllocationPlanOfUser, listAllocationsPlanOfUsers } from '../../../../../01-actions/allocation-plan';
import { PERM_LIST_ALLOCATION_PLAN_OF_USERS } from '../../../../../07-constants/app-permission';
import { MILISECONDS_IN_DAY } from '../../../../../05-utils/commonData';
import {
  calculateLength,
  convertDate,
  filterAllocationPlans,
  formatAllocationPlans,
  isAllowPermission,
  promiseDelayImport
} from '../../../../../08-helpers/common';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);
const FieldDate = LazyComponent(promiseDelayImport(import('../Allocations/FieldDate')), <Skeleton height={38} />);
const FieldInput = LazyComponent(promiseDelayImport(import('../Allocations/FieldInput')), <Skeleton height={38} />);

const Chart = LazyComponent(promiseDelayImport(import('./Chart'), 2000), <SpinLoader />);

function TabChart({ allocationPlanUsers, appPermissions, skills, mentors, listAllocationsPlanOfUsers }) {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [freeEffort, setFreeEffort] = useState('');
  const [skill, setSkill] = useState(null);
  const [user, setUser] = useState('');
  const [mentor, setMentor] = useState(null);

  useEffect(() => {
    const nowDate = new Date();
    const toDayTime = Date.UTC(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
    let startDate = null;
    let endDate = null;

    switch (new Date(toDayTime).getUTCDay()) {
      case 1:
        startDate = new Date(toDayTime);
        endDate = new Date(toDayTime + 7 * MILISECONDS_IN_DAY - 1000);
        break;
      case 2:
        startDate = new Date(toDayTime - 1 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 6 * MILISECONDS_IN_DAY - 1000);
        break;
      case 3:
        startDate = new Date(toDayTime - 2 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 5 * MILISECONDS_IN_DAY - 1000);
        break;
      case 4:
        startDate = new Date(toDayTime - 3 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 4 * MILISECONDS_IN_DAY - 1000);
        break;
      case 5:
        startDate = new Date(toDayTime - 4 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 3 * MILISECONDS_IN_DAY - 1000);
        break;
      case 6:
        startDate = new Date(toDayTime - 5 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 2 * MILISECONDS_IN_DAY - 1000);
        break;
      case 0:
        startDate = new Date(toDayTime - 6 * MILISECONDS_IN_DAY);
        endDate = new Date(toDayTime + 1 * MILISECONDS_IN_DAY - 1000);
        break;
      default:
        break;
    }
    setStartDate(startDate);
    setEndDate(endDate);
    setFreeEffort(null);

    return () => {
      clearAllocationPlanOfUser();
    };
  }, []);

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_ALLOCATION_PLAN_OF_USERS)) {
      listAllocationsPlanOfUsers();
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const handleEffort = freeEffort => setFreeEffort(freeEffort);

  const handleSkill = skill => setSkill(skill);

  const handleSearchUser = user => setUser(user);

  const handleMentor = mentor => setMentor(mentor);

  const handleStartDate = startDate => {
    if (startDate) {
      const choosedDate = new Date(startDate);
      const utcTime = Date.UTC(choosedDate.getFullYear(), choosedDate.getMonth(), choosedDate.getDate());
      setStartDate(new Date(utcTime));
    } else {
      setStartDate(startDate);
    }
  };

  const handleEndDate = endDate => {
    if (endDate) {
      const choosedDate = new Date(endDate);
      const utcTime = Date.UTC(choosedDate.getFullYear(), choosedDate.getMonth(), choosedDate.getDate(), 23, 59, 59);
      setEndDate(new Date(utcTime));
    } else {
      setEndDate(endDate);
    }
  };

  const dataFormat = formatAllocationPlans(allocationPlanUsers, startDate, endDate);
  const data = filterAllocationPlans({
    listData: dataFormat,
    startDate,
    endDate,
    freeEffort,
    user,
    skill,
    mentor
  });

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className="allocation-plans-chart">
          <Row className="mb-3 mx-n1">
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <FieldDate
                placeholder="start_date"
                value={startDate ? convertDate(startDate) : startDate}
                handleSelect={handleStartDate}
              />
            </Col>
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <FieldDate
                placeholder="end_date"
                value={endDate ? convertDate(endDate) : endDate}
                handleSelect={handleEndDate}
              />
            </Col>
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <FieldInput
                awesomeIcon="fa fa-hourglass-o"
                type="number"
                placeholder="effort_percent"
                value={freeEffort}
                handleInput={handleEffort}
              />
            </Col>
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <SelectPropsBox
                options={skills}
                value={skill && skill.value}
                onChange={handleSkill}
                placeholder="select_skill"
                className={'z-index-2'}
                isClearable={true}
              />
            </Col>
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <SelectPropsBox
                options={mentors}
                value={mentor && mentor.value}
                onChange={handleMentor}
                placeholder="mentor"
                className={'z-index-2'}
                isClearable={true}
              />
            </Col>
            <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
              <FieldInput
                awesomeIcon="fa fa-search"
                type="text"
                placeholder="search_user"
                value={user}
                handleInput={handleSearchUser}
              />
            </Col>
          </Row>
          {calculateLength(data) ? <Chart data={{ data: data }} /> : <p className="text-center">{t('no_data')}</p>}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabChart.propTypes = {
  allocationPlanUsers: array,
  appPermissions: array,
  skills: array,
  mentors: array,
  listAllocationsPlanOfUsers: func
};

const mapStateTopProps = state => ({
  appPermissions: state.auth.permissions,
  allocationPlanUsers: state.allocationPlan.allocationPlanUsers,
  mentors: state.allocationPlan.mentors,
  skills: state.allocationPlan.skills
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listAllocationsPlanOfUsers,
      clearAllocationPlanOfUser
    },
    dispatch
  );

export default connect(
  mapStateTopProps,
  mapDispatchToProps
)(memo(TabChart));
