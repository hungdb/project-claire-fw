import React, { memo, Fragment } from 'react';
import { array, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, ButtonGroup } from 'reactstrap';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { ALLOCATION_PLAN_TAB } from '../../../../../05-utils/commonData';

function Menu({ appPermissions, tabValue, onClick }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <ButtonGroup className="button-group-tab">
          {Object.values(ALLOCATION_PLAN_TAB).map(tab => (
            <Fragment key={tab.value}>
              {isAllowPermission(appPermissions, tab.permissions) && (
                <Button
                  key={tab.value}
                  onClick={e => onClick(e, tab.value)}
                  active={tabValue === tab.value}
                  className="tabs-btn"
                >
                  {t(tab.label)}
                </Button>
              )}
            </Fragment>
          ))}
        </ButtonGroup>
      )}
    </NamespacesConsumer>
  );
}

Menu.propTypes = {
  appPermissions: array,
  onClick: func,
  tabValue: number
};

export default memo(Menu);
