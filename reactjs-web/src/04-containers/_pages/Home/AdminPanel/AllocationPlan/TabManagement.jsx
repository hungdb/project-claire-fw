import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { useState, memo, useEffect } from 'react';
import connect from 'react-redux/es/connect/connect';
import { array, func, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getUsers } from '../../../../../01-actions/adminproject';
import {
  createAllocationPlan,
  listAllocationPlans,
  removeAllocationPlan,
  updateAllocationPlan
} from '../../../../../01-actions/allocation-plan';
import {
  PERM_CREATE_ALLOCATION_PLAN,
  PERM_UPDATE_ALLOCATION_PLAN,
  PERM_LIST_ALLOCATION_PLANS
} from '../../../../../07-constants/app-permission';
import {
  convertUTCString,
  isAllowPermission,
  isChangeObject,
  calculateLength,
  promiseDelayImport
} from '../../../../../08-helpers/common';

const AddAllocationPlanDialog = LazyComponent(() =>
  import('../../../../dialog/allocation-plan/AddAllocationPlanDialog')
);
const UpdateAllocationPlanDialog = LazyComponent(() =>
  import('../../../../dialog/allocation-plan/UpdateAllocationPlanDialog')
);
const ConfirmationModal = LazyComponent(() => import('../../../../../03-components/ConfirmationModal'));
const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <Skeleton width={73.64} height={34} />
);

const AllocationCard = LazyComponent(promiseDelayImport(import('./AllocationCard')), <Skeleton height={56} />);

function TabManagement({
  appPermissions,
  allocationPlans,
  listUsers,
  listAllocationPlans,
  createAllocationPlan,
  updateAllocationPlan,
  removeAllocationPlan,
  getUsers,
  status
}) {
  const [showDialogAddAllocationPlan, setShowDialogAddAllocationPlan] = useState(false);
  const [showDialogRemoveAllocationPlan, setShowDialogRemoveAllocationPlan] = useState(false);
  const [showDialogUpdateAllocationPlan, setShowDialogUpdateAllocationPlan] = useState(false);
  const [selectedAllocationPlan, setSelectedAllocationPlan] = useState(null);

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_ALLOCATION_PLANS)) {
      listAllocationPlans();
      getUsers();
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const openDialogAddSalePriority = () => setShowDialogAddAllocationPlan(true);

  const closeDialogAddAllocationPlan = () => setShowDialogAddAllocationPlan(false);

  const openDialogRemoveAllocationPlan = (e, selectedAllocationPlan) => {
    setShowDialogRemoveAllocationPlan(true);
    setSelectedAllocationPlan(selectedAllocationPlan);
    e.stopPropagation();
  };

  const closeDialogRemoveAllocationPlan = () => {
    setShowDialogRemoveAllocationPlan(false);
    setSelectedAllocationPlan(null);
  };

  const openDialogUpdateAllocationPlan = selectedAllocationPlan => {
    if (isAllowPermission(appPermissions, PERM_UPDATE_ALLOCATION_PLAN)) {
      setShowDialogUpdateAllocationPlan(true);
      setSelectedAllocationPlan(selectedAllocationPlan);
    }
  };

  const closeDialogUpdateAllocationPlan = () => {
    setShowDialogUpdateAllocationPlan(false);
    setSelectedAllocationPlan(null);
  };

  const handleRemoveAllocationPlan = () => {
    removeAllocationPlan(selectedAllocationPlan && selectedAllocationPlan.id);
    closeDialogRemoveAllocationPlan();
  };

  const onAddAllocationPlan = data => {
    createAllocationPlan(data);
  };

  const onUpdateAllocationPlan = data => {
    const oldValue = {
      ...selectedAllocationPlan,
      startDate: convertUTCString(selectedAllocationPlan.startDate),
      endDate: convertUTCString(selectedAllocationPlan.endDate)
    };
    const newValue = {
      ...data,
      startDate: convertUTCString(data.startDate),
      endDate: convertUTCString(data.endDate)
    };

    if (isChangeObject(newValue, oldValue)) {
      updateAllocationPlan(data);
      return;
    }
    closeDialogUpdateAllocationPlan();
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          {isAllowPermission(appPermissions, PERM_CREATE_ALLOCATION_PLAN) && (
            <Row className="ml-0 mr-0">
              <Col className="pl-2 pr-0 mb-3 d-flex justify-content-end">
                <Button type={'orange'} onClick={openDialogAddSalePriority}>
                  <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                </Button>
              </Col>
            </Row>
          )}

          <div className="allocation-area-col">
            <div className="allocation-area-col-main">
              {calculateLength(allocationPlans) ? (
                allocationPlans.map(item => (
                  <AllocationCard
                    key={item.id}
                    appPermissions={appPermissions}
                    item={item}
                    onUpdate={() => openDialogUpdateAllocationPlan(item)}
                    onRemove={e => openDialogRemoveAllocationPlan(e, item)}
                  />
                ))
              ) : (
                <p className="text-center mb-0 text-muted font-size-15">{t('not_found')}</p>
              )}
            </div>
          </div>

          <AddAllocationPlanDialog
            appPermissions={appPermissions}
            isOpen={showDialogAddAllocationPlan}
            onClose={closeDialogAddAllocationPlan}
            onOK={onAddAllocationPlan}
            listUsers={listUsers}
            status={status}
          />
          <UpdateAllocationPlanDialog
            appPermissions={appPermissions}
            isOpen={showDialogUpdateAllocationPlan}
            onClose={closeDialogUpdateAllocationPlan}
            onOK={onUpdateAllocationPlan}
            listUsers={listUsers}
            status={status}
            value={selectedAllocationPlan}
          />
          <ConfirmationModal
            isOpen={showDialogRemoveAllocationPlan}
            txtTitle={t('confirm_remove')}
            txtCancel={t('cancel')}
            txtConfirm={t('confirm')}
            funConfirm={handleRemoveAllocationPlan}
            funClose={closeDialogRemoveAllocationPlan}
          >
            {t('are_you_sure_remove_this_allocation_plan')}
          </ConfirmationModal>
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabManagement.propTypes = {
  appPermissions: array,
  allocationPlans: array,
  listUsers: array,
  listAllocationPlans: func,
  createAllocationPlan: func,
  updateAllocationPlan: func,
  removeAllocationPlan: func,
  getUsers: func,
  status: string
};

const mapStateToProps = state => {
  return {
    appPermissions: state.auth.permissions,
    allocationPlans: state.allocationPlan.allocationPlans,
    status: state.allocationPlan.status,
    listUsers: state.admins.listUsers
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAllocationPlans,
      createAllocationPlan,
      updateAllocationPlan,
      removeAllocationPlan,
      getUsers
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(TabManagement));
