import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo } from 'react';
import { array, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime, isAllowPermission, toFixed, promiseDelayImport } from '../../../../../08-helpers/common';
import { workloadAllocation } from '../../../../../05-utils/commonUtils';
import { PERM_REMOVE_ALLOCATION_PLAN } from '../../../../../07-constants/app-permission';

const UserAvatar = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/UserAvatar')),
  <Skeleton circle={true} width={40} height={40} />
);

function AllocationCard({ appPermissions, item, onUpdate, onRemove }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div key={item.id} className="d-flex justify-content-center allocation-card cursor-pointer" onClick={onUpdate}>
          <div className="col-3 col-md-3 flex-column d-flex justify-content-center">
            <UserAvatar id={item.id} className="avatar" user={item.User} src={`/profile/${item.User.id}`} />
          </div>
          <div className="col-9 col-md-9 pt-1 pb-1">
            <div>
              <span>@{item.User.name}</span>
              <span> ({toFixed(workloadAllocation(item.startDate, item.endDate, item.effortPercent), 2)}h)</span>
            </div>
            <div className="more-infor">
              <span>{item.effortPercent || 0}%</span> &nbsp;
              <span>{formatDateTime(item.startDate, 'DD/MM/YYYY')}</span> -{' '}
              <span>{formatDateTime(item.endDate, 'DD/MM/YYYY')}</span>
            </div>
          </div>
          {isAllowPermission(appPermissions, PERM_REMOVE_ALLOCATION_PLAN) && (
            <span className="btn-remove" onClick={onRemove}>
              x
            </span>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

AllocationCard.propTypes = {
  appPermissions: array,
  item: object,
  onUpdate: func,
  onRemove: func
};

export default memo(AllocationCard);
