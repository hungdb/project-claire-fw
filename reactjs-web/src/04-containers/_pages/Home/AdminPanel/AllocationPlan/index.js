import AdminAllocationPlans from './AdminAllocationPlans';
import connect from 'react-redux/es/connect/connect';
import { clearAllocationPlans } from '../../../../../01-actions/allocation-plan';
import { bindActionCreators } from 'redux';

const mapStateTopProps = state => ({
  appPermissions: state.auth.permissions
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      clearAllocationPlans
    },
    dispatch
  );
};

export default connect(
  mapStateTopProps,
  mapDispatchToProps
)(AdminAllocationPlans);
