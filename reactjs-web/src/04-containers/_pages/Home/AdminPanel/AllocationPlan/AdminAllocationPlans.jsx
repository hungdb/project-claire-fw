import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect } from 'react';
import { func, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col } from 'reactstrap';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import { ALLOCATION_PLAN_TAB } from '../../../../../05-utils/commonData';

const TabManagement = LazyComponent(() => import('./TabManagement'));
const TabChart = LazyComponent(() => import('./TabChart'));

const Menu = LazyComponent(promiseDelayImport(import('./Menu')), <Skeleton width={241} height={33} />);
const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);

function AdminAllocationPlans({ clearAllocationPlans, appPermissions }) {
  const [tabValue, setTabValue] = useState(ALLOCATION_PLAN_TAB.MANAGEMENT.value);
  useEffect(() => {
    return () => {
      clearAllocationPlans();
    };
  }, []);

  const changeTabSkill = (e, tabValue) => {
    setTabValue(tabValue);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page admin-allocation-plans">
          <Row className="mx-0">
            <Col className="px-0">
              <AppText type={'title-header'} className="mb-4">
                {t('allocation_plan')}
              </AppText>
            </Col>
            <Col className="pl-2 pr-0 d-flex justify-content-end">
              <div className="tabs margin-bottom-30px">
                <Menu
                  appPermissions={appPermissions}
                  tabValue={tabValue}
                  onClick={(e, value) => changeTabSkill(e, value)}
                />
              </div>
            </Col>
          </Row>

          {tabValue === ALLOCATION_PLAN_TAB.MANAGEMENT.value && <TabManagement />}
          {tabValue === ALLOCATION_PLAN_TAB.CHART.value && <TabChart />}
        </div>
      )}
    </NamespacesConsumer>
  );
}

AdminAllocationPlans.propTypes = {
  clearAllocationPlans: func,
  appPermissions: array
};

export default memo(AdminAllocationPlans);
