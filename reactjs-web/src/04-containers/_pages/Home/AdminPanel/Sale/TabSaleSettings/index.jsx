import { func } from 'prop-types';
import React, { PureComponent, Fragment } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import TabSaleCategory from './TabSaleCategory';
import TabSaleStatus from './TabSaleStatus';
import TabSalePriority from './TabSalePriority';
import TabsController from '../../../../../../03-components/TabsController';

const TAB_SETTINGS = [
  {
    value: 1,
    label: 'sale_status'
  },
  {
    value: 2,
    label: 'sale_priority'
  },
  {
    value: 3,
    label: 'sale_category'
  }
];

class TabSaleSettings extends PureComponent {
  static propTypes = {
    clearSaleSetting: func
  };

  state = {
    statusTab: 1
  };

  onChangeTabs = statusTab => this.setState({ statusTab });

  render() {
    const { statusTab } = this.state;
    const tabFields = TAB_SETTINGS.map(item => item.label);
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Fragment>
            <div className="d-flex justify-content-end mb-4">
              <TabsController allTabsField={tabFields} onChangeTabs={this.onChangeTabs} />
            </div>
            {statusTab === TAB_SETTINGS[0].value && <TabSaleStatus />}
            {statusTab === TAB_SETTINGS[1].value && <TabSalePriority />}
            {statusTab === TAB_SETTINGS[2].value && <TabSaleCategory />}
          </Fragment>
        )}
      </NamespacesConsumer>
    );
  }
}

export default TabSaleSettings;
