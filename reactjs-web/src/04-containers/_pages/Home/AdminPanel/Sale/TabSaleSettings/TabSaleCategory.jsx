import { array, func } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { Button, Col, Row, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import {
  listSaleCategory,
  createSaleCategory,
  updateSaleCategory,
  removeSaleCategory
} from '../../../../../../01-actions/sale';
import {
  PERM_LIST_SALE_CATEGORY,
  PERM_CREATE_SALE_CATEGORY,
  PERM_UPDATE_SALE_CATEGORY
} from '../../../../../../07-constants/app-permission';
import {
  displayNumberRecord,
  formatDateTime,
  isAllowPermission,
  calculateLength
} from '../../../../../../08-helpers/common';
import SaleCategoryDialog from '../../../../../dialog/sale-dialog/SaleCategoryDialog';
import TableHead from '../../../../../../03-components/TableHead';

const TABLE_FIELDS = ['no', 'name', 'created_at', 'action'];
class TabSaleCategory extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showDialogSaleCategory: false,
      isEditSaleCategory: false,
      itemSaleCategory: null
    };
  }

  static propTypes = {
    saleCategories: array,
    appPermissions: array,
    createSaleCategory: func,
    updateSaleCategory: func,
    removeSaleCategory: func,
    listSaleCategory: func
  };

  checkRoleAndLoad() {
    if (isAllowPermission(this.props.appPermissions, PERM_LIST_SALE_CATEGORY)) {
      this.props.listSaleCategory();
    }
  }

  componentDidMount() {
    this.checkRoleAndLoad();
  }

  openDialogAddSaleCategory = () => {
    this.setState({
      showDialogSaleCategory: true
    });
  };

  closeDialogSaleCategory = () => {
    this.setState({
      showDialogSaleCategory: false,
      itemSaleCategory: null,
      isEditSaleCategory: false
    });
  };

  onOKDialog = (editMode, removeMode, value) => {
    this.closeDialogSaleCategory();
    if (!editMode) {
      this.props.createSaleCategory(value);
      return;
    }
    if (removeMode) {
      this.props.removeSaleCategory(value.id);
      return;
    }
    const { itemSaleCategory } = this.state;
    const isValueChange = value.name !== itemSaleCategory.name;
    if (isValueChange) {
      this.props.updateSaleCategory(value);
    }
  };

  onClickUpdateItem = item => {
    this.setState({
      itemSaleCategory: item,
      isEditSaleCategory: true,
      showDialogSaleCategory: true
    });
  };

  render() {
    const { saleCategories, appPermissions } = this.props;
    const { showDialogSaleCategory, isEditSaleCategory, itemSaleCategory } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            {isAllowPermission(appPermissions, PERM_CREATE_SALE_CATEGORY) && (
              <Row className="ml-0 mr-0">
                <Col className="pl-2 pr-0 mb-3">
                  <div className={'d-flex justify-content-end'}>
                    <Button className={'orange-btn'} onClick={this.openDialogAddSaleCategory} size={'md'}>
                      <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                    </Button>
                  </div>
                </Col>
              </Row>
            )}

            {isAllowPermission(appPermissions, PERM_LIST_SALE_CATEGORY) && (
              <Table bordered responsive striped>
                <thead>
                  <tr>
                    <TableHead fields={TABLE_FIELDS} t={t} />
                  </tr>
                </thead>
                <tbody>
                  {calculateLength(saleCategories) ? (
                    saleCategories.map((item, index) => {
                      return (
                        <tr key={item.id}>
                          <td className={'text-center align-middle'}>{displayNumberRecord(1, 10, index)}</td>
                          <td className={'text-center align-middle'}>{item.name}</td>
                          <td className={'text-center align-middle'}>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
                          <td className={'text-center align-middle'}>
                            {isAllowPermission(appPermissions, PERM_UPDATE_SALE_CATEGORY) && (
                              <span className={'cursor-pointer'} onClick={() => this.onClickUpdateItem(item)}>
                                <i className="fa fa-pencil color-base-btn" />
                              </span>
                            )}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} className={'text-center font-size-15 text-muted'}>
                        {t('not_found')}
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            )}

            <SaleCategoryDialog
              appPermissions={appPermissions}
              isOpen={showDialogSaleCategory}
              editMode={isEditSaleCategory}
              value={itemSaleCategory}
              onClose={this.closeDialogSaleCategory}
              onOK={this.onOKDialog}
            />
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    saleCategories: store.sale.saleCategories
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listSaleCategory,
      createSaleCategory,
      updateSaleCategory,
      removeSaleCategory
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabSaleCategory);
