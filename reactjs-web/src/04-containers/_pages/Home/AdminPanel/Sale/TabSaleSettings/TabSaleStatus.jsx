import { array, func } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { Button, Col, Row, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import {
  listSaleStatus,
  createSaleStatus,
  updateSaleStatus,
  removeSaleStatus
} from '../../../../../../01-actions/sale';
import {
  PERM_LIST_SALE_STATUS,
  PERM_CREATE_SALE_STATUS,
  PERM_UPDATE_SALE_STATUS
} from '../../../../../../07-constants/app-permission';
import {
  displayNumberRecord,
  formatDateTime,
  isAllowPermission,
  calculateLength
} from '../../../../../../08-helpers/common';
import TableHead from '../../../../../../03-components/TableHead';
import SaleStatusDialog from '../../../../../dialog/sale-dialog/SaleStatusDialog';

const TABLE_FIELDS = ['no', 'name', 'created_at', 'action'];
class TabSaleStatus extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showDialogSaleStatus: false,
      isEditSaleStatus: false,
      itemSaleStatus: null
    };
  }

  static propTypes = {
    saleStatuses: array,
    appPermissions: array,
    createSaleStatus: func,
    updateSaleStatus: func,
    removeSaleStatus: func,
    listSaleStatus: func
  };

  checkRoleAndLoad() {
    if (isAllowPermission(this.props.appPermissions, PERM_LIST_SALE_STATUS)) {
      this.props.listSaleStatus();
    }
  }

  componentDidMount() {
    this.checkRoleAndLoad();
  }

  openDialogAddSaleCategory = () => {
    this.setState({
      showDialogSaleStatus: true
    });
  };

  closeDialogSaleCategory = () => {
    this.setState({
      showDialogSaleStatus: false,
      itemSaleStatus: null,
      isEditSaleStatus: false
    });
  };

  onOKDialog = (editMode, removeMode, value) => {
    this.closeDialogSaleCategory();
    if (!editMode) {
      this.props.createSaleStatus(value);
      return;
    }
    if (removeMode) {
      this.props.removeSaleStatus(value.id);
      return;
    }
    const { itemSaleStatus } = this.state;
    const isValueChange = value.name !== itemSaleStatus.name;
    if (isValueChange) {
      this.props.updateSaleStatus(value);
    }
  };

  onClickUpdateItem = item => {
    this.setState({
      itemSaleStatus: item,
      isEditSaleStatus: true,
      showDialogSaleStatus: true
    });
  };

  render() {
    const { saleStatuses, appPermissions } = this.props;
    const { showDialogSaleStatus, isEditSaleStatus, itemSaleStatus } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            {isAllowPermission(appPermissions, PERM_CREATE_SALE_STATUS) && (
              <Row className="ml-0 mr-0">
                <Col className="pl-2 pr-0 mb-3">
                  <div className={'d-flex justify-content-end'}>
                    <Button className={'orange-btn'} onClick={this.openDialogAddSaleCategory} size={'md'}>
                      <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                    </Button>
                  </div>
                </Col>
              </Row>
            )}

            {isAllowPermission(appPermissions, PERM_LIST_SALE_STATUS) && (
              <Table bordered responsive striped>
                <thead>
                  <tr>
                    <TableHead fields={TABLE_FIELDS} t={t} />
                  </tr>
                </thead>
                <tbody>
                  {calculateLength(saleStatuses) ? (
                    saleStatuses.map((item, index) => {
                      return (
                        <tr key={item.id}>
                          <td className={'text-center align-middle'}>{displayNumberRecord(1, 10, index)}</td>
                          <td className={'text-center align-middle'}>{item.name}</td>
                          <td className={'text-center align-middle'}>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
                          <td className={'text-center align-middle'}>
                            {isAllowPermission(appPermissions, PERM_UPDATE_SALE_STATUS) && (
                              <span className={'cursor-pointer'} onClick={() => this.onClickUpdateItem(item)}>
                                <i className="fa fa-pencil color-base-btn" />
                              </span>
                            )}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} className={'text-center font-size-15 text-muted'}>
                        {t('not_found')}
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            )}
            <SaleStatusDialog
              appPermissions={appPermissions}
              isOpen={showDialogSaleStatus}
              editMode={isEditSaleStatus}
              value={itemSaleStatus}
              onClose={this.closeDialogSaleCategory}
              onOK={this.onOKDialog}
            />
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    saleStatuses: store.sale.saleStatuses
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listSaleStatus,
      createSaleStatus,
      updateSaleStatus,
      removeSaleStatus
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabSaleStatus);
