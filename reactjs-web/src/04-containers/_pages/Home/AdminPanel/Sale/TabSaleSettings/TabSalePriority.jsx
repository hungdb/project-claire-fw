import { array, func } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { Button, Col, Row, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import {
  getListSalePriority,
  createSalePriority,
  updateSalePriority,
  removeSalePriority
} from '../../../../../../01-actions/sale';
import {
  PERM_GET_LIST_SALE_PRIORITY,
  PERM_CREATE_SALE_PRIORITY,
  PERM_UPDATE_SALE_PRIORITY
} from '../../../../../../07-constants/app-permission';
import { formatDateTime, isAllowPermission, calculateLength } from '../../../../../../08-helpers/common';
import TableHead from '../../../../../../03-components/TableHead';
import SalePriorityDialog from '../../../../../dialog/sale-dialog/SalePriorityDialog';

const TABLE_FIELDS = ['no', 'name', 'ordinal_number', 'created_at', 'action'];
class TabSalePriority extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showDialogSalePriority: false,
      isEditSalePriority: false,
      itemSaleCategory: null
    };
  }

  static propTypes = {
    appPermissions: array,
    salePriorities: array,
    getListSalePriority: func,
    createSalePriority: func,
    updateSalePriority: func,
    removeSalePriority: func
  };

  checkRoleAndLoad() {
    const { getListSalePriority, appPermissions } = this.props;
    if (isAllowPermission(appPermissions, PERM_GET_LIST_SALE_PRIORITY)) {
      getListSalePriority();
    }
  }

  componentDidMount() {
    this.checkRoleAndLoad();
  }

  openDialogAddSalePriority = () => {
    this.setState({
      showDialogSalePriority: true
    });
  };

  closeDialogSalePriority = () => {
    this.setState({
      showDialogSalePriority: false,
      isEditSalePriority: false
    });
  };

  onClickUpdateItem = item => {
    this.setState({
      itemSalePriority: item,
      isEditSalePriority: true,
      showDialogSalePriority: true
    });
  };

  _renderTableBody = (length, data, t) => {
    return calculateLength(data) ? (
      data.map((item, index) => {
        return (
          <tr key={index}>
            <td className={'text-center align-middle'}>{index + 1}</td>
            <td className={'text-center align-middle'}>{item.name}</td>
            <td className={'text-center align-middle'}>{item.ordinalNumber}</td>
            <td className={'text-center align-middle'}>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
            <td className={'text-center align-middle'}>
              {isAllowPermission(this.props.appPermissions, PERM_UPDATE_SALE_PRIORITY) && (
                <span className={'cursor-pointer'} onClick={() => this.onClickUpdateItem(item)}>
                  <i className="fa fa-pencil color-base-btn" />
                </span>
              )}
            </td>
          </tr>
        );
      })
    ) : (
      <tr className="text-center text-muted font-size-15">
        <td colSpan={length}>{t('not_found')}</td>
      </tr>
    );
  };

  onOKDialog = (id, name, ordinalNumber) => {
    const { createSalePriority, updateSalePriority } = this.props;
    const { isEditSalePriority, itemSalePriority } = this.state;
    if (!isEditSalePriority) createSalePriority(name, ordinalNumber);
    if (isEditSalePriority && (name !== itemSalePriority.name || ordinalNumber !== itemSalePriority.ordinalNumber))
      updateSalePriority(id, name, ordinalNumber);
    this.closeDialogSalePriority();
  };

  onRemoveItem = id => {
    const { removeSalePriority } = this.props;
    removeSalePriority(id);
    this.closeDialogSalePriority();
  };

  render() {
    const { salePriorities, appPermissions } = this.props;
    const { showDialogSalePriority, isEditSalePriority, itemSalePriority } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            {isAllowPermission(appPermissions, PERM_CREATE_SALE_PRIORITY) && (
              <Row className="ml-0 mr-0">
                <Col className="pl-2 pr-0 mb-3 d-flex justify-content-end">
                  <Button className={'orange-btn'} onClick={this.openDialogAddSalePriority} size={'md'}>
                    <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add_priority')}
                  </Button>
                </Col>
              </Row>
            )}

            {isAllowPermission(appPermissions, PERM_GET_LIST_SALE_PRIORITY) && (
              <Table bordered responsive striped>
                <thead>
                  <tr>
                    <TableHead fields={TABLE_FIELDS} t={t} />
                  </tr>
                </thead>
                <tbody>{this._renderTableBody(calculateLength(TABLE_FIELDS), salePriorities, t)}</tbody>
              </Table>
            )}

            <SalePriorityDialog
              appPermissions={appPermissions}
              isOpen={showDialogSalePriority}
              editMode={isEditSalePriority}
              value={itemSalePriority}
              onClose={this.closeDialogSalePriority}
              onOK={this.onOKDialog}
              onRemove={this.onRemoveItem}
            />
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    salePriorities: store.sale.salePriorities
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListSalePriority,
      createSalePriority,
      updateSalePriority,
      removeSalePriority
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabSalePriority);
