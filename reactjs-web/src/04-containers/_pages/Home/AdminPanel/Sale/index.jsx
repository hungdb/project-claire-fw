import { func } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { Button, ButtonGroup } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { clearSaleSetting } from '../../../../../01-actions/sale';
import './style.css';
import TabSale from './TabSale/index';
import TabSaleSettings from './TabSaleSettings';

const SALE_TAB = {
  SALE: {
    value: 1,
    label: 'sales'
  },
  STATUS: {
    value: 2,
    label: 'sale_settings'
  }
};

class SaleSetting extends PureComponent {
  static propTypes = {
    clearSaleSetting: func
  };

  constructor(props) {
    super(props);
    this.state = {
      tabSale: SALE_TAB.SALE.value
    };
  }

  componentWillUnmount() {
    const { clearSaleSetting } = this.props;
    clearSaleSetting();
  }

  changeTabSkill(e, value) {
    this.setState({ tabSale: value });
  }

  render() {
    const { tabSale } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div className="page sale-setting-management">
            <div className="d-flex justify-content-between align-items-center tabs margin-bottom-30px">
              <ButtonGroup className="button-group-tab">
                {Object.values(SALE_TAB).map(tab => (
                  <Button
                    key={tab.value}
                    onClick={e => this.changeTabSkill(e, tab.value)}
                    active={tabSale === tab.value}
                    className="tabs-btn"
                  >
                    {t(tab.label)}
                  </Button>
                ))}
              </ButtonGroup>
            </div>
            {tabSale === SALE_TAB.SALE.value && <TabSale />}
            {tabSale === SALE_TAB.STATUS.value && <TabSaleSettings />}
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      clearSaleSetting
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(SaleSetting);
