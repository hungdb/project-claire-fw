import { array, func, number } from 'prop-types';
import { TablePagination } from '@material-ui/core';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import connect from 'react-redux/es/connect/connect';
import { Button, Col, Row, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import {
  listSale,
  createSale,
  updateSale,
  removeSale,
  listSaleStatus,
  listSaleCategory,
  getListSalePriority
} from '../../../../../../01-actions/sale';
import { getUsers } from '../../../../../../01-actions/adminproject';
import { PERM_LIST_SALE, PERM_CREATE_SALE, PERM_UPDATE_SALE } from '../../../../../../07-constants/app-permission';
import {
  displayNumberRecord,
  formatDateTime,
  isAllowPermission,
  calculateLength,
  isChangeObject,
  convertUTCString
} from '../../../../../../08-helpers/common';
import SaleStatusDialog from '../../../../../dialog/sale-dialog/SaleDialog';
import TableHead from '../../../../../../03-components/TableHead';
import { ROWS_PER_PAGE_OPTIONS } from '../../../../../../05-utils/commonData';
import Filter from './Filter';
import { getListCustomer, clearAllDataFormated } from '../../../../../../01-actions/customer';
import CustomerDialog from '../../../../../dialog/customer-dialog/index';
import { getListMarket } from '../../../../../../01-actions/market';

const TABLE_FIELDS = [
  'no',
  'category',
  'company',
  'pic',
  'priority',
  'status',
  'appointment_start_date',
  'appointment_end_date',
  'note',
  'customer',
  'action'
];

const DEFAULT_STATUS = 1;

class TabSaleStatus extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showDialogSaleStatus: false,
      isEditSaleStatus: false,
      itemSaleStatus: null,
      showMoreSaleStatus: false,
      page: 0,
      limit: ROWS_PER_PAGE_OPTIONS[0],
      dataSearch: {},
      customerStatusDialog: false,
      customer: null,
      isShowDetail: false
    };
  }

  static propTypes = {
    saleStatuses: array,
    saleCategories: array,
    salePriorities: array,
    sales: array,
    appPermissions: array,
    createSale: func,
    updateSale: func,
    removeSale: func,
    listSale: func,
    listSaleStatus: func,
    listSaleCategory: func,
    getListSalePriority: func,
    getUsers: func,
    clearAllDataFormated: func,
    listUsers: array,
    total: number,
    getListCustomer: func,
    getListMarket: func,
    allCustomers: array,
    allMarkets: array
  };

  componentDidMount() {
    this.checkRoleAndLoad();
  }

  componentDidUpdate(prevProps) {
    const { appPermissions } = this.props;
    if (appPermissions !== prevProps.appPermissions) {
      this.checkRoleAndLoad();
    }
  }

  checkRoleAndLoad = () => {
    const { appPermissions } = this.props;
    if (isAllowPermission(appPermissions, PERM_LIST_SALE)) {
      const {
        listSaleStatus,
        listSaleCategory,
        getListSalePriority,
        getUsers,
        getListCustomer,
        getListMarket
      } = this.props;
      listSaleStatus();
      listSaleCategory();
      getListSalePriority();
      getUsers();
      getListMarket();
      getListCustomer('', '', '', '', '', DEFAULT_STATUS);
      this.getListSale();
    }
  };

  openDialogAddSaleCategory = () => {
    this.setState({
      showDialogSaleStatus: true
    });
  };

  closeDialogSaleCategory = () => {
    this.setState({
      showDialogSaleStatus: false,
      itemSaleStatus: null,
      isEditSaleStatus: false,
      showMoreSaleStatus: false
    });
  };

  onOKDialog = (editMode, removeMode, value) => {
    this.closeDialogSaleCategory();
    if (!editMode) {
      this.props.createSale(value);
      return;
    }
    if (removeMode) {
      this.props.removeSale(value.id);
      return;
    }
    const { itemSaleStatus } = this.state;
    const oldValue = {
      ...itemSaleStatus,
      appointmentDate: convertUTCString(itemSaleStatus.appointmentDate)
    };
    const newValue = {
      ...value,
      appointmentDate: convertUTCString(value.appointmentDate)
    };

    if (isChangeObject(newValue, oldValue)) {
      this.props.updateSale(value);
    }
  };

  onClickUpdateItem = item => {
    this.setState({
      itemSaleStatus: item,
      isEditSaleStatus: true,
      showDialogSaleStatus: true
    });
  };

  isShowMore = item => {
    this.setState({
      itemSaleStatus: item,
      showMoreSaleStatus: true,
      showDialogSaleStatus: true
    });
  };

  onChangePage = (event, page) => {
    this.setState({ page }, () => {
      this.getListSale();
    });
  };

  onChangeRowsPerPage = event => {
    const limit = event.target.value;
    this.setState({ page: 0, limit }, () => {
      this.getListSale();
    });
  };

  filterSale = dataSearch => {
    this.setState({ page: 0, dataSearch }, () => {
      this.getListSale();
    });
  };

  getListSale = () => {
    const { limit, dataSearch, page } = this.state;
    this.props.listSale({
      ...dataSearch,
      limit: Number(limit),
      page: page + 1
    });
  };

  onSelectCustomer = customerId => {
    const { allCustomers } = this.props;
    const customer = calculateLength(allCustomers) ? allCustomers.find(item => item.id === customerId) : {};
    if (customer) {
      this.setState(
        {
          customer
        },
        this.openCustomerDialog()
      );
    }
  };

  onCloseCustomerDialog = () => {
    const { clearAllDataFormated } = this.props;
    clearAllDataFormated();
    this.setState({
      customerStatusDialog: false,
      isShowDetail: false
    });
  };
  openCustomerDialog = () => {
    this.setState({
      customerStatusDialog: true,
      isShowDetail: true,
      customer: null
    });
  };

  render() {
    const {
      sales,
      appPermissions,
      saleStatuses,
      saleCategories,
      salePriorities,
      total,
      listUsers,
      allMarkets,
      allCustomers
    } = this.props;

    const {
      showDialogSaleStatus,
      isEditSaleStatus,
      itemSaleStatus,
      limit,
      page,
      showMoreSaleStatus,
      customer,
      customerStatusDialog
    } = this.state;

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            <Filter
              filterSale={this.filterSale}
              saleStatuses={saleStatuses}
              saleCategories={saleCategories}
              salePriorities={salePriorities}
              listUsers={listUsers}
              listCustomer={allCustomers}
            />
            {isAllowPermission(appPermissions, PERM_CREATE_SALE) && (
              <Row className="ml-0 mr-0">
                <Col className="pl-2 pr-0 mb-3">
                  <div className={'d-flex justify-content-end'}>
                    <Button className={'orange-btn'} onClick={this.openDialogAddSaleCategory} size={'md'}>
                      <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                    </Button>
                  </div>
                </Col>
              </Row>
            )}
            {isAllowPermission(appPermissions, PERM_LIST_SALE) && (
              <Table bordered responsive striped>
                <thead>
                  <tr>
                    <TableHead fields={TABLE_FIELDS} t={t} />
                  </tr>
                </thead>
                <tbody>
                  {calculateLength(sales) ? (
                    sales.map((item, index) => {
                      return (
                        <tr key={item.id}>
                          <td className={'text-center align-middle'}>{displayNumberRecord(1, 10, index)}</td>
                          <td className={'text-center align-middle'}>
                            {item.saleCategory ? item.saleCategory.name : null}
                          </td>
                          <td className={`text-center align-middle ${!item.company && 'text-muted'}`}>
                            {item.company || t('not_found')}
                          </td>
                          <td className={'text-center align-middle'}>{item.picAs ? item.picAs.name : null}</td>
                          <td className={'text-center align-middle'}>
                            {item.salePriority ? item.salePriority.name : null}
                          </td>
                          <td className={'text-center align-middle'}>
                            {item.saleStatus ? item.saleStatus.name : null}
                          </td>
                          <td className={'text-center align-middle'}>
                            {formatDateTime(item.appointmentStartDate, 'DD/MM/YYYY HH:mm', 1)}
                          </td>
                          <td className={'text-center align-middle'}>
                            {formatDateTime(item.appointmentEndDate, 'DD/MM/YYYY HH:mm', 1)}
                          </td>
                          <td className={'text-center align-middle'}>{item.note}</td>
                          <td className={'text-center align-middle'}>
                            {item.customer ? (
                              <span
                                className="font-size-15px cursor-pointer"
                                style={{ color: '#0067B1', textDecoration: 'underline' }}
                                onClick={() => this.onSelectCustomer(item.customerId)}
                              >
                                {item.customer.name}
                              </span>
                            ) : (
                              t('not_found')
                            )}
                          </td>
                          <td className={'text-center align-middle'}>
                            <span className={'cursor-pointer mr-2'} onClick={() => this.isShowMore(item)}>
                              <i className="fa fa-eye color-base-btn" />
                            </span>
                            {isAllowPermission(appPermissions, PERM_UPDATE_SALE) && (
                              <span className={'cursor-pointer'} onClick={() => this.onClickUpdateItem(item)}>
                                <i className="fa fa-pencil color-base-btn" />
                              </span>
                            )}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={13} className={'text-center font-size-15 text-muted'}>
                        {t('not_found')}
                      </td>
                    </tr>
                  )}
                </tbody>
                <tfoot>
                  <tr>
                    <TablePagination
                      rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
                      colSpan={13}
                      count={total || 0}
                      rowsPerPage={Number(limit)}
                      page={page}
                      SelectProps={{
                        native: true
                      }}
                      onChangePage={this.onChangePage}
                      onChangeRowsPerPage={this.onChangeRowsPerPage}
                      labelRowsPerPage={t('rows_per_page')}
                      labelDisplayedRows={({ from, to, count }) => `${from}-${to} of ${count}`}
                    />
                  </tr>
                </tfoot>
              </Table>
            )}
            <SaleStatusDialog
              isOpen={showDialogSaleStatus}
              editMode={isEditSaleStatus}
              value={itemSaleStatus}
              onClose={this.closeDialogSaleCategory}
              onOK={this.onOKDialog}
              saleStatuses={saleStatuses}
              saleCategories={saleCategories}
              salePriorities={salePriorities}
              listUsers={listUsers}
              listCustomer={allCustomers}
              showMode={showMoreSaleStatus}
              appPermissions={appPermissions}
            />
            <CustomerDialog
              isOpen={customerStatusDialog}
              onClose={this.onCloseCustomerDialog}
              listCustomer={allCustomers}
              listMarkets={allMarkets}
              showDetail={true}
              value={customer}
            />
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    sales: store.sale.sales,
    saleStatuses: store.sale.saleStatuses,
    saleCategories: store.sale.saleCategories,
    salePriorities: store.sale.salePriorities,
    total: store.sale.total,
    listUsers: store.admins.listUsers,
    allCustomers: store.customer.allCustomers,
    allMarkets: store.market.allMarkets
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listSale,
      createSale,
      updateSale,
      removeSale,
      listSaleStatus,
      listSaleCategory,
      getListSalePriority,
      getUsers,
      getListCustomer,
      getListMarket,
      clearAllDataFormated
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabSaleStatus);
