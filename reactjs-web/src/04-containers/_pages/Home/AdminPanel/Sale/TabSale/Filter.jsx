import { func, array } from 'prop-types';
import React, { PureComponent } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Col, Input, Row } from 'reactstrap';
import { debounce } from 'throttle-debounce';
import { getEndDateLocal } from '../../../../../../08-helpers/common';
import ControlDatePicker from '../../../../../../03-components/DateTimePicker';
import SelectPropsBox from '../../../../../../03-components/SelectPropsBox';
import i18n from '../../../../../../i18n';

class Filter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoryId: '',
      pic: '',
      start: null,
      end: null,
      statusId: -1,
      priorityId: '',
      keywords: '',
      customerId: ''
    };
  }

  componentDidMount() {
    this.filterSale();
  }

  static propTypes = {
    filterSale: func,
    saleStatuses: array,
    saleCategories: array,
    salePriorities: array,
    listUsers: array,
    listCustomer: array
  };

  handleChangeType = (selected, e) => {
    const value = selected ? selected.value : '';
    this.setState({ [e.name]: value }, () => {
      this.filterSale();
    });
  };

  handleChangeDateStart = start => {
    this.setState({ start }, () => {
      this.filterSale();
    });
  };

  handleChangeDateEnd = end => {
    const endDate = end ? getEndDateLocal(end) : '';
    this.setState({ end: endDate }, () => {
      this.filterSale();
    });
  };

  onChangeField = debounce(500, target => {
    this.setState({ [target.name]: target.value }, () => {
      this.filterSale();
    });
  });

  filterSale = () => {
    const { categoryId, priorityId, statusId, pic, start, end, keywords, customerId } = this.state;
    const { filterSale } = this.props;
    filterSale({
      categoryId,
      priorityId,
      statusId,
      pic,
      start,
      end,
      keywords,
      customerId
    });
  };

  render() {
    const { saleStatuses, saleCategories, salePriorities, listUsers, listCustomer } = this.props;
    const { categoryId, priorityId, statusId, pic, start, end, customerId } = this.state;
    const statusOptions = [{ id: -1, name: i18n.t('not_closed') }, ...saleStatuses];

    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Row>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={saleCategories}
                value={categoryId}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'category'}
                name={'categoryId'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={listUsers}
                value={pic}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'pic'}
                name={'pic'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <ControlDatePicker
                isLocalFormat={1}
                value={start}
                format="DD/MM/YYYY"
                onChange={this.handleChangeDateStart}
                placeholder={t('start_date')}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <ControlDatePicker
                isLocalFormat={1}
                value={end}
                format="DD/MM/YYYY"
                onChange={this.handleChangeDateEnd}
                placeholder={t('end_date')}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={statusOptions}
                value={statusId}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'status'}
                name={'statusId'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={salePriorities}
                value={priorityId}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'priority'}
                name={'priorityId'}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <Input
                onChange={e => this.onChangeField(e.target)}
                className={'flex-grow-1 full focus-border'}
                name="keywords"
                placeholder={t('keywords')}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-3">
              <SelectPropsBox
                options={listCustomer}
                value={customerId}
                onChange={this.handleChangeType}
                keyValue="id"
                keyLabel="name"
                isClearable={true}
                placeholder={'customer'}
                name={'customerId'}
              />
            </Col>
          </Row>
        )}
      </NamespacesConsumer>
    );
  }
}

export default Filter;
