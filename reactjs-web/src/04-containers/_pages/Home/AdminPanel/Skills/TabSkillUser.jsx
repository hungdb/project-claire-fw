import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import SpinLoader from '../../../../../03-components/SpinLoader';
import connect from 'react-redux/es/connect/connect';
import React, { memo, useState, useEffect } from 'react';
import { array, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getUsers } from '../../../../../01-actions/adminproject';
import { SKILL_TYPES } from '../../../../../05-utils/commonData';
import { PERM_LIST_SKILLS } from '../../../../../07-constants/app-permission';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import {
  createUserSkill,
  listSkillsByUser,
  removeUserSkill,
  updateUserSkill,
  handleFilterByUserIdSkill,
  listAllSkills
} from '../../../../../01-actions/skill';

const TabSkillUserTable = LazyComponent(promiseDelayImport(import('./TabSkillUserTable')), <SpinLoader />);

const UserSkillDialog = LazyComponent(() => import('../../../../dialog/skill-dialog/user-skill-dialog'));

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

let skillUrlQuery = null;
let tabSkill;

function TabSkillUser({
  userSkills,
  currentUsers,
  appPermissions,
  getUsers,
  listSkillsByUser,
  createUserSkill,
  removeUserSkill,
  updateUserSkill,
  handleFilterByUserIdSkill,
  listAllSkills,
  history,
  location,
  match
}) {
  const [showDialogUserSkill, setShowDialogUserSkill] = useState(false);
  const [isEditUserSkill, setIsEditUserSkill] = useState(false);
  const [evaluationPoint, setEvaluationPoint] = useState(0);
  const [isStandard, setIsStandard] = useState(false);
  const [userSkillId, setUserSkillId] = useState(0);
  const [skillChoose, setSkillChoose] = useState(0);
  const [name, setName] = useState('');
  const [userId, setUserId] = useState(0);

  const callUpdate = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_SKILLS)) {
      getUsers();
      listAllSkills();
    }
  };

  useEffect(() => {
    tabSkill = parseInt(localStorage.getItem('tabSkill'));
    const stringUrl = localStorage.getItem('skillUrlQuery');
    if (stringUrl) {
      skillUrlQuery = new Map(JSON.parse(stringUrl));
      history.push(`${match.path}?${queryString.stringify(skillUrlQuery.get(tabSkill))}`);
    } else {
      skillUrlQuery = new Map();
    }

    let { userId } = skillUrlQuery.get(tabSkill) ? skillUrlQuery.get(tabSkill) : {};
    if (userId) {
      userId = parseInt(userId);
      setUserId(userId);
    }

    callUpdate();
  }, []);

  useEffect(() => {
    const urlQuery = queryString.parse(history.location.search);
    skillUrlQuery.set(tabSkill, urlQuery);
    localStorage.setItem('skillUrlQuery', JSON.stringify([...skillUrlQuery]));

    if (!userId || userId <= 0) return;
    handleFilterByUserIdSkill(userId);
    listSkillsByUser({ userId, type: SKILL_TYPES.normal });
  }, [userId]);

  const openDialogUserSkill = () => {
    setShowDialogUserSkill(true);
    setIsEditUserSkill(false);
    setEvaluationPoint(0);
    setIsStandard(false);
    setName('');
    setUserSkillId(0);
    setSkillChoose(0);
  };

  const onChangeSelectUser = user => {
    const selectValue = user ? parseInt(user.value) : null;
    const urlQuery = queryString.parse(location.search);
    urlQuery.userId = selectValue;
    setUserId(selectValue);
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Row className="ml-0 mr-0">
            <Col xs="6" md="4" xl="2" className="pl-0 pr-2 mb-3 mt-1">
              <SelectPropsBox
                placeholder="select_user"
                options={currentUsers}
                onChange={user => onChangeSelectUser(user)}
                value={userId}
                keyValue="id"
                keyLabel="name"
                isClearable={false}
              />
            </Col>
            <Col className="pl-2 pr-0 mb-3 mt-1">
              <div className={'d-flex justify-content-end'}>
                {userId ? (
                  <Button className={'orange-btn'} onClick={() => openDialogUserSkill()} size={'md'}>
                    <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                  </Button>
                ) : null}
              </div>
            </Col>
          </Row>
          <TabSkillUserTable
            userSkills={userSkills}
            onUpdate={item => {
              setShowDialogUserSkill(true);
              setIsEditUserSkill(true);
              setEvaluationPoint(item.evaluationPoint);
              setIsStandard(item.isStandard);
              setName(item.skill.name);
              setUserSkillId(item.id);
              setSkillChoose(item.skillId);
            }}
          />
          <UserSkillDialog
            isOpen={showDialogUserSkill}
            editMode={!!isEditUserSkill}
            value={{
              evaluationPoint,
              isStandard,
              name,
              userSkillId,
              skillId: skillChoose
            }}
            onClose={() => setShowDialogUserSkill(false)}
            onOK={(editMode, removeMode, value) => {
              setShowDialogUserSkill(false);
              if (editMode) {
                if (removeMode) {
                  removeUserSkill(value);
                } else {
                  updateUserSkill(value);
                }
                setIsEditUserSkill(false);
              } else {
                createUserSkill({ ...value, userId });
              }
            }}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabSkillUser.propTypes = {
  userSkills: object,
  currentUsers: array,
  appPermissions: array,
  getUsers: func,
  listSkillsByUser: func,
  createUserSkill: func,
  removeUserSkill: func,
  updateUserSkill: func,
  handleFilterByUserIdSkill: func,
  listAllSkills: func,
  history: object,
  location: object,
  match: object
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    userSkills: store.skill.userSkills,
    currentUsers: store.admins.listUsers
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsers,
      listSkillsByUser,
      createUserSkill,
      removeUserSkill,
      updateUserSkill,
      handleFilterByUserIdSkill,
      listAllSkills
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TabSkillUser))
);
