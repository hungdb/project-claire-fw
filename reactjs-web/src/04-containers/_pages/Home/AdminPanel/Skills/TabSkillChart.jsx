import connect from 'react-redux/es/connect/connect';
import React, { memo, useEffect } from 'react';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { listAllSkills } from '../../../../../01-actions/skill';
import { PERM_GET_SKILLS_OF_ALL_USERS } from '../../../../../07-constants/app-permission';
import {
  calculateLength,
  isAllowPermission,
  promiseDelayImport,
  removeDuplicateObjInArray
} from '../../../../../08-helpers/common';

const SkillChart = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SkillChart'), 2000),
  <SpinLoader />
);

const titleRow = [
  'Skill',
  'People',
  { role: 'style' },
  {
    sourceColumn: 0,
    role: 'annotation',
    type: 'string',
    calc: 'stringify'
  }
];

function TabSkillChart({ appPermissions, listAllSkills, allSkills }) {
  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_GET_SKILLS_OF_ALL_USERS)) {
      const isStandard = true;
      listAllSkills(isStandard);
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const processData = allSkills => {
    const data = [titleRow];
    let totalNumber = 0;
    allSkills.forEach(item => {
      let numberPeople = 0;
      if (calculateLength(item.userSkill)) {
        const users = removeDuplicateObjInArray({ list: item.userSkill, key: 'userId' });
        numberPeople = users.length;
        totalNumber += numberPeople;
        data.push([item.name, numberPeople, '', numberPeople]);
      }
    });
    return { data, totalNumber };
  };
  const { data, totalNumber } = processData(allSkills);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="tab-skill-chart-page">
          <SkillChart
            data={data}
            height={`${allSkills.length * 30 + 220}px`}
            className="skill-chart"
            title={`${totalNumber} ${totalNumber === 1 ? t('person') : t('people')}`}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabSkillChart.propTypes = {
  appPermissions: array,
  listAllSkills: func,
  allSkills: array
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    allSkills: store.skill.allSkills
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAllSkills
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(TabSkillChart));
