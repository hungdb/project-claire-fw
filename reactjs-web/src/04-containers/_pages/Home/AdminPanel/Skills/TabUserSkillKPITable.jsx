import TableHead from '../../../../../03-components/TableHead';
import React, { memo } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime } from '../../../../../08-helpers/common';
import { Table } from 'reactstrap';
import { EVALUATE_KPI_SKILL } from '../../../../../05-utils/commonData';

const fields = ['no', 'kpi_point', 'evaluate', 'evaluate_at', 'created_at', 'action'];

const checkEvaluate = number => {
  if (number > EVALUATE_KPI_SKILL.POINT_ONE) return 'good';
  if (number < EVALUATE_KPI_SKILL.POINT_ONE && number > EVALUATE_KPI_SKILL.POINT_TWO) return 'normal';
  if (number < EVALUATE_KPI_SKILL.POINT_TWO) return 'bad';
  return 'invalid';
};

function TabUserSkillKPITable({ skillsKPIOfUser, onRemove }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Table bordered responsive className="text-center">
          <thead>
            <tr>
              <TableHead fields={fields} t={t} />
            </tr>
          </thead>
          <tbody>
            {skillsKPIOfUser &&
              skillsKPIOfUser.data &&
              skillsKPIOfUser.data.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.point}</td>
                    <td>{t(checkEvaluate(item.point))}</td>
                    <td>{formatDateTime(item.evaluatedAt, 'LLLL', 1)}</td>
                    <td>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
                    <td>
                      <span className={'cursor-pointer'}>
                        <i
                          className="fa fa fa-trash-o font-size-14 color-base-remove-btn"
                          onClick={() => onRemove(item.id)}
                        />
                      </span>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      )}
    </NamespacesConsumer>
  );
}

TabUserSkillKPITable.propTypes = {
  skillsKPIOfUser: array,
  onRemove: func
};

export default memo(TabUserSkillKPITable);
