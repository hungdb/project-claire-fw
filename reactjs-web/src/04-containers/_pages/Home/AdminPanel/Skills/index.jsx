import Skill from './Skill';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import { clearUserSkill } from '../../../../../01-actions/skill';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      clearUserSkill
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Skill);
