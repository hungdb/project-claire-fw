import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import Skeleton from 'react-loading-skeleton';
import connect from 'react-redux/es/connect/connect';
import React, { memo, useState, useEffect } from 'react';
import { array, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getSkillsOfAllUsers, listAllSkills } from '../../../../../01-actions/skill';
import { PERM_LIST_SKILLS } from '../../../../../07-constants/app-permission';
import { SKILL_RANK_TYPES } from '../../../../../05-utils/commonData';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import { calculateLength, getRanksByTypeSkill, isAllowPermission } from '../../../../../08-helpers/common';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import { debounce } from 'throttle-debounce';

let skillUrlQuery = null;
let tabSkill;
const TabAllSkillTable = LazyComponent(promiseDelayImport(import('./TabAllSkillTable')), <SpinLoader />);
const FieldInput = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/FieldInput')),
  <Skeleton height={38} />
);

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

function TabAllSkill({
  location,
  match,
  history,
  allSkillByUser,
  appPermissions,
  getSkillsOfAllUsers,
  listAllSkills,
  allSkills
}) {
  const [skillId, setSkillId] = useState(null);
  const [userName, setUserName] = useState('');
  const [rankSelected, setRankSelected] = useState(null);
  const [skillSelected, setSkillSelected] = useState(null);
  const [skillOptions, setSkillOptions] = useState([]);

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_SKILLS)) {
      getSkillsOfAllUsers();
      listAllSkills();
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  useEffect(() => {
    tabSkill = parseInt(localStorage.getItem('tabSkill'));
    const stringUrl = localStorage.getItem('skillUrlQuery');
    if (stringUrl) {
      skillUrlQuery = new Map(JSON.parse(stringUrl));
    } else {
      skillUrlQuery = new Map();
      return;
    }

    let { skillId, name, typeskill, rank } = skillUrlQuery.get(tabSkill) ? skillUrlQuery.get(tabSkill) : {};
    if (skillId) {
      skillId = parseInt(skillId);
      setSkillId(skillId);
    }
    if (name) setUserName(name);
    if (typeskill) {
      setSkillSelected(typeskill);
      const options = typeskill ? getRanksByTypeSkill(typeskill) : [];
      setSkillOptions(options);
      if (rank) setRankSelected(rank);
    }
  }, []);

  useEffect(() => {
    let queryArr = [];

    if (skillId) queryArr = [...queryArr, `skillId=${skillId}`];
    if (userName) queryArr = [...queryArr, `name=${userName}`];
    if (skillSelected) queryArr = [...queryArr, `typeskill=${skillSelected}`];
    if (rankSelected) queryArr = [...queryArr, `rank=${rankSelected}`];

    const query = queryArr.reduce((total, item) => {
      if (!total) return `${total}?${item}`;
      return `${total}&${item}`;
    }, '');
    history.push(`${match.path}${query}`);

    const urlQuery = queryString.parse(history.location.search);
    skillUrlQuery.set(0, urlQuery);
    localStorage.setItem('skillUrlQuery', JSON.stringify([...skillUrlQuery]));
  }, [skillId, userName, skillSelected, rankSelected]);

  const handleFilterSkill = skillId => {
    setSkillId(skillId);
  };

  const handleFilterUserName = name => {
    setUserName(name);
  };

  const handleFilterTypeSkill = typeSkill => {
    const options = typeSkill ? getRanksByTypeSkill(typeSkill) : [];
    setSkillOptions(options);
    setRankSelected(null);
    setSkillSelected(typeSkill);
  };

  const handleFilterRank = rank => {
    setRankSelected(rank);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="all-skill-by-user">
          <Row className="mb-3">
            <Col xs={12} sm={4} md={4} xl={3} className={'pt-1'}>
              <SelectPropsBox
                options={allSkills}
                value={skillId}
                onChange={selected => handleFilterSkill(selected ? selected.value : null)}
                placeholder="select_skill"
                keyLabel="name"
                keyValue="id"
              />
            </Col>
            <Col xs={12} sm={4} md={4} xl={3} className={'pt-1'}>
              <FieldInput
                type={'text'}
                onChangeValue={debounce(500, value => handleFilterUserName(value))}
                defaultValue={userName ? userName : undefined}
                placeholder={'search_by_username'}
                isDebounce={true}
              />
            </Col>
            <Col xs={12} sm={4} md={4} xl={3} className={'pt-1'}>
              <SelectPropsBox
                options={SKILL_RANK_TYPES}
                value={skillSelected}
                onChange={selected => handleFilterTypeSkill(selected ? selected.value : null)}
                placeholder="select_type_skill"
              />
            </Col>
            {calculateLength(skillOptions) ? (
              <Col xs={12} sm={4} md={4} xl={3} className={'pt-1'}>
                <SelectPropsBox
                  options={skillOptions}
                  value={rankSelected}
                  onChange={selected => handleFilterRank(selected ? selected.value : null)}
                  placeholder="select_rank"
                  keyValue="label"
                />
              </Col>
            ) : null}
          </Row>
          <TabAllSkillTable
            allSkillByUser={allSkillByUser}
            userName={userName}
            skillId={skillId}
            skillSelected={skillSelected}
            rankSelected={rankSelected}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabAllSkill.propTypes = {
  allSkillByUser: array,
  allSkills: array,
  appPermissions: array,
  getSkillsOfAllUsers: func,
  listAllSkills: func,
  history: object,
  location: object,
  match: object
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    allSkillByUser: store.skill.allSkillByUser,
    allSkills: store.skill.allSkills
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getSkillsOfAllUsers,
      listAllSkills
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TabAllSkill))
);
