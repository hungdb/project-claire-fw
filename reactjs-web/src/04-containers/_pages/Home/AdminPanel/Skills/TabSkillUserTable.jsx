import TableHead from '../../../../../03-components/TableHead';
import BadgeColor from '../../../../../03-components/BadgeColor';
import ProgressBar from '../../../../../03-components/ProgressBar';
import React, { memo } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime, displayNumberRecord, getRankOfSkill } from '../../../../../08-helpers/common';
import { Table } from 'reactstrap';

const fields = ['no', 'skill', 'evaluation_point', 'rank', 'standard', 'created_at', 'action'];

function TabSkillUserTable({ userSkills, onUpdate }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Table bordered responsive className="text-center">
          <thead className="font-weight-500">
            <tr>
              <TableHead fields={fields} t={t} />
            </tr>
          </thead>
          <tbody>
            {userSkills &&
              userSkills.data.map((item, index) => {
                const rank = getRankOfSkill(item.evaluationPoint, item.skill.name);

                return (
                  <tr key={item.id}>
                    <td>{displayNumberRecord(1, 10, index)}</td>
                    <td>{item.skill.name}</td>
                    {/*<td>{ item.evaluationPoint + '/100' }</td>*/}
                    <td>
                      <div>{item.evaluationPoint + '/100'}</div>
                      <ProgressBar textHident value={item.evaluationPoint} displayRank={false} />
                    </td>
                    <td>{rank && <BadgeColor backgroundColor={rank.backgroundColor}>{rank.label}</BadgeColor>}</td>
                    <td>{item.isStandard ? <i className="fa fa-check main-color" /> : null}</td>
                    <td>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
                    <td>
                      <span className={'cursor-pointer'} onClick={() => onUpdate(item)}>
                        <i className="fa fa-pencil color-base-btn" />
                      </span>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      )}
    </NamespacesConsumer>
  );
}

TabSkillUserTable.propTypes = {
  userSkills: array,
  onUpdate: func
};

export default memo(TabSkillUserTable);
