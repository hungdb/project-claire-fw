import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useState, useEffect } from 'react';
import { Button, ButtonGroup } from 'reactstrap';
import { func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { TAB_SKILL } from '../../../../../05-utils/commonData';
import { withRouter } from 'react-router';

const TabAllSkill = LazyComponent(() => import('./TabAllSkill'));
const TabSkill = LazyComponent(() => import('./TabSkill'));
const TabUserSkillKPI = LazyComponent(() => import('./TabUserSkillKPI'));
const TabSkillChart = LazyComponent(() => import('./TabSkillChart'));
const TabSkillUser = LazyComponent(() => import('./TabSkillUser'));

function Skill({ clearUserSkill, history, match, localtion }) {
  const [statusTabSkill, setStatusTabSkill] = useState(TAB_SKILL.ALL_SKILL_BY_USERS.value);

  useEffect(() => {
    const tabSkill = parseInt(localStorage.getItem('tabSkill'));
    setStatusTabSkill(tabSkill);
    return () => {
      clearUserSkill();
    };
  }, []);

  const changeTabSkill = (e, value) => {
    setStatusTabSkill(value);
    localStorage.setItem('tabSkill', value);
    history.push(`${match.path}`);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page skill-management">
          <div className="d-flex justify-content-between align-items-center tabs mb-3">
            <ButtonGroup className="button-group-tab">
              {Object.values(TAB_SKILL).map(tab => (
                <Button
                  key={tab.value}
                  onClick={e => changeTabSkill(e, tab.value)}
                  active={statusTabSkill === tab.value}
                  className="tabs-btn"
                >
                  {t(tab.label)}
                </Button>
              ))}
            </ButtonGroup>
          </div>
          {statusTabSkill === TAB_SKILL.ALL_SKILL_BY_USERS.value && <TabAllSkill />}
          {statusTabSkill === TAB_SKILL.USER_SKILL_KPI.value && <TabUserSkillKPI />}
          {statusTabSkill === TAB_SKILL.SKILL.value && <TabSkill />}
          {statusTabSkill === TAB_SKILL.USER_SKILL.value && <TabSkillUser />}
          {statusTabSkill === TAB_SKILL.SKILL_CHART.value && <TabSkillChart />}
        </div>
      )}
    </NamespacesConsumer>
  );
}

Skill.propTypes = {
  clearUserSkill: func,
  history: object,
  match: object,
  location: object
};

export default withRouter(memo(Skill));
