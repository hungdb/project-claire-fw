import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import SpinLoader from '../../../../../03-components/SpinLoader';
import connect from 'react-redux/es/connect/connect';
import React, { memo, useState, useEffect } from 'react';
import { array, any, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getUsers } from '../../../../../01-actions/adminproject';
import { ROWS_PER_PAGE_OPTIONS } from '../../../../../05-utils/commonData';
import { PERM_LIST_SKILLS } from '../../../../../07-constants/app-permission';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import {
  createKpiSkill,
  getListKPISkillOfUser,
  removeKpiSkill,
  handleFilterByUserIdKPI
} from '../../../../../01-actions/skill';

let skillUrlQuery = null;
let tabSkill;
const SkillKPIDialog = LazyComponent(() => import('../../../../dialog/skill-kpi-dialog/SkillKPIDialog'));
const Pagination = LazyComponent(promiseDelayImport(import('../../../../../03-components/Pagination')));
const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);
const TabUserSkillKPITable = LazyComponent(promiseDelayImport(import('./TabUserSkillKPITable')), <SpinLoader />);

function TabUserSkillKPI({
  currentUsers,
  appPermissions,
  skillsKPIOfUser,
  getUsers,
  createKpiSkill,
  getListKPISkillOfUser,
  removeKpiSkill,
  handleFilterByUserIdKPI,
  history,
  match,
  location
}) {
  const [limit, setLimit] = useState(ROWS_PER_PAGE_OPTIONS[0]);
  const [page, setPage] = useState(1);
  const [showDialogKPI, setShowDialogKPI] = useState(false);
  const [userIdKPI, setUserIdKPI] = useState(null);

  useEffect(() => {
    if (isAllowPermission(appPermissions, PERM_LIST_SKILLS)) {
      getUsers();
    }

    tabSkill = localStorage.getItem('tabSkill');
    const stringUrl = localStorage.getItem('skillUrlQuery');
    if (stringUrl) {
      skillUrlQuery = new Map(JSON.parse(stringUrl));
      history.push(`${match.path}?${queryString.stringify(skillUrlQuery.get(tabSkill))}`);
    } else {
      skillUrlQuery = new Map();
    }

    let { userIdKPI } = skillUrlQuery.get(tabSkill) ? skillUrlQuery.get(tabSkill) : {};
    if (userIdKPI) {
      userIdKPI = parseInt(userIdKPI);
      setUserIdKPI(userIdKPI);
    }
  }, []);

  useEffect(() => {
    handleFilterByUserIdKPI(userIdKPI);
    setLimit(ROWS_PER_PAGE_OPTIONS[0]);
    setPage(1);
    getListKPISkillOfUser(userIdKPI, 1, ROWS_PER_PAGE_OPTIONS[0]);

    const urlQuery = queryString.parse(history.location.search);
    skillUrlQuery.set(tabSkill, urlQuery);
    localStorage.setItem('skillUrlQuery', JSON.stringify([...skillUrlQuery]));
  }, [userIdKPI]);

  const onChangeSelectUser = user => {
    setUserIdKPI(user.value);
    const urlQuery = queryString.parse(location.search);
    urlQuery.userIdKPI = user.value;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Row className="mx-0">
            <Col xs="6" md="4" xl="2" className="pl-0 pr-2 mb-3 mt-1">
              <SelectPropsBox
                placeholder="select_user"
                options={currentUsers}
                onChange={user => onChangeSelectUser(user)}
                value={userIdKPI}
                keyValue="id"
                keyLabel="name"
                isClearable={false}
              />
            </Col>
            <Col className="pl-2 pr-0 mb-3 mt-1">
              {userIdKPI && (
                <div className={'d-flex justify-content-end'}>
                  <Button className={'orange-btn'} onClick={() => setShowDialogKPI(true)} size={'md'}>
                    <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
                  </Button>
                </div>
              )}
            </Col>
          </Row>
          <TabUserSkillKPITable skillsKPIOfUser={skillsKPIOfUser} onRemove={value => removeKpiSkill(value)} />
          {skillsKPIOfUser && skillsKPIOfUser.data && (
            <Pagination
              totalItems={skillsKPIOfUser.total}
              pageSize={limit}
              value={limit}
              onSelect={page => {
                setPage(page);
                getListKPISkillOfUser(userIdKPI, page, limit);
              }}
              onChange={e => {
                const limit = e.value;
                setLimit(limit);
                getListKPISkillOfUser(userIdKPI, page, limit);
              }}
              rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
              isClearable={false}
            />
          )}
          <SkillKPIDialog
            isOpen={showDialogKPI}
            onClose={() => setShowDialogKPI(false)}
            onAddSkillKPI={(reviewPoint, testPoint, date) => {
              createKpiSkill(userIdKPI, reviewPoint, testPoint, date);
            }}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabUserSkillKPI.propTypes = {
  currentUsers: array,
  appPermissions: array,
  skillsKPIOfUser: any,
  userIdKPI: any,
  getUsers: func,
  createKpiSkill: func,
  getListKPISkillOfUser: func,
  removeKpiSkill: func,
  handleFilterByUserIdKPI: func,
  history: object,
  match: object,
  location: object
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    currentUsers: store.admins.listUsers,
    skillsKPIOfUser: store.skill.skillsKPIOfUser,
    userIdKPI: store.skill.filter.userIdKPI
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsers,
      createKpiSkill,
      getListKPISkillOfUser,
      removeKpiSkill,
      handleFilterByUserIdKPI
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TabUserSkillKPI))
);
