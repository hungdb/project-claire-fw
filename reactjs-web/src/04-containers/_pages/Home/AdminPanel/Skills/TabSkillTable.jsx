import TableHead from '../../../../../03-components/TableHead';
import linkIconDefault from '../../../../../06-assets/images/default-image.jpg';
import React, { memo } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime, displayNumberRecord } from '../../../../../08-helpers/common';
import { Table } from 'reactstrap';

const fields = ['no', 'name', 'description', 'link_icon', 'created_at', 'action'];

function TabSkillTable({ skills, onUpdate }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Table bordered responsive className="text-center">
          <thead className="font-weight-500">
            <tr>
              <TableHead fields={fields} t={t} />
            </tr>
          </thead>
          <tbody>
            {skills &&
              skills.data &&
              skills.data.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{displayNumberRecord(skills.page, skills.limit, index)}</td>
                    <td>{item.name}</td>
                    <td>{item.description}</td>
                    <td>
                      <img className="link-icon" src={item.linkIcon || linkIconDefault} alt="Link icon" />
                    </td>
                    <td>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
                    <td>
                      <span className={'cursor-pointer'} onClick={() => onUpdate(item)}>
                        <i className="fa fa-pencil color-base-btn" />
                      </span>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      )}
    </NamespacesConsumer>
  );
}

TabSkillTable.propTypes = {
  skills: array,
  onUpdate: func
};

export default memo(TabSkillTable);
