import connect from 'react-redux/es/connect/connect';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect } from 'react';
import { func, object, array, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { createSkill, listSkills, removeSkill, updateSkill } from '../../../../../01-actions/skill';
import { ROWS_PER_PAGE_SKILL_OPTIONS } from '../../../../../05-utils/commonData';
import { PERM_LIST_SKILLS } from '../../../../../07-constants/app-permission';
import { GET_MY_ROLE_AND_PERMISSION_SUCCESS } from '../../../../../07-constants/auth';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';

const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <div className={'mb-3 mt-1'}>
    <Skeleton width={73.66} height={34} />
  </div>
);

const TabSkillTable = LazyComponent(promiseDelayImport(import('./TabSkillTable')), <SpinLoader />);

const Pagination = LazyComponent(promiseDelayImport(import('../../../../../03-components/Pagination')));

const SkillDialog = LazyComponent(() => import('../../../../dialog/skill-dialog/skill-dialog'));

function TabSkill({ listSkills, createSkill, removeSkill, updateSkill, skills, appPermissions, authStatus }) {
  const [limit, setLimit] = useState(ROWS_PER_PAGE_SKILL_OPTIONS[0]);
  const [skillId, setSkillId] = useState(0);
  const [page, setPage] = useState(1);
  const [showDialogSkill, setShowDialogSkill] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [linkIcon, setLinkIcon] = useState('');

  useEffect(() => {
    listSkills({ limit, page: 1 });
  }, []);

  useEffect(() => {
    if (authStatus === GET_MY_ROLE_AND_PERMISSION_SUCCESS) {
      if (isAllowPermission(appPermissions, PERM_LIST_SKILLS)) {
        listSkills({ limit, page });
      }
    }
  }, [authStatus]);

  const openDialogSkill = () => {
    setShowDialogSkill(true);
    setIsEdit(false);
    setName('');
    setDescription('');
    setLinkIcon('');
    setSkillId(0);
  };

  const onClickOkDialog = (editMode, removeMode, value) => {
    if (editMode) {
      if (removeMode) {
        removeSkill(value);
      } else {
        updateSkill(value);
      }
      setIsEdit(false);
    } else {
      createSkill(value);
    }
    setShowDialogSkill(false);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <div className={'d-flex justify-content-end'}>
            <Button type={'orange'} className={'mb-3 mt-1'} onClick={() => openDialogSkill()}>
              <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add')}
            </Button>
          </div>
          <TabSkillTable
            skills={skills}
            onUpdate={item => {
              setShowDialogSkill(true);
              setIsEdit(true);
              setName(item.name);
              setDescription(item.description);
              setLinkIcon(item.linkIcon);
              setSkillId(item.id);
            }}
          />
          {skills && skills.data && (
            <Pagination
              totalItems={skills.total}
              pageSize={limit}
              value={limit}
              onSelect={page => {
                setPage(page);
                listSkills({ page, limit });
              }}
              onChange={e => {
                const limit = e.value;
                setLimit(limit);
                listSkills({ page, limit });
              }}
              rowsPerPageOptions={ROWS_PER_PAGE_SKILL_OPTIONS}
              isClearable={false}
            />
          )}
          <SkillDialog
            isOpen={showDialogSkill}
            editMode={!!isEdit}
            value={{
              name: name,
              description: description,
              linkIcon: linkIcon,
              id: skillId
            }}
            onClose={() => setShowDialogSkill(false)}
            onOK={onClickOkDialog}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabSkill.propTypes = {
  listSkills: func,
  createSkill: func,
  removeSkill: func,
  updateSkill: func,
  skills: object,
  appPermissions: array,
  authStatus: string
};

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    authStatus: store.auth.status,
    skills: store.skill.skills
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listSkills,
      updateSkill,
      createSkill,
      removeSkill
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(TabSkill));
