import ProgressBar from '../../../../../03-components/ProgressBar';
import BadgeColor from '../../../../../03-components/BadgeColor';
import TableHead from '../../../../../03-components/TableHead';
import React, { memo, Fragment } from 'react';
import { array, number, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime, calculateLength } from '../../../../../08-helpers/common';
import { Table } from 'reactstrap';

const fields = ['no', 'user', 'no', 'skill', 'evaluation_point', 'rank', 'standard', 'created_at'];

const checkStandard = type => {
  let result = '';
  if (type) {
    result = <i className="fa fa-check main-color" />;
  }
  return result;
};

const _renderUserSkills = (userIndex, userName, userSkills, t) => {
  return calculateLength(userSkills) ? (
    userSkills.map((item, index) => {
      const skillDetail = (
        <Fragment>
          <td>{index + 1}</td>
          <td>{item.skill && item.skill.name}</td>
          <td>
            <div className="font-evaluation-point">{item.evaluationPoint + '/100'}</div>
            <ProgressBar textHident value={item.evaluationPoint} displayRank={false} />
          </td>
          <td>
            {item.rank && (
              <BadgeColor backgroundColor={item.rank && item.rank.backgroundColor}>
                {item.rank && item.rank.label}
              </BadgeColor>
            )}
          </td>
          <td>{checkStandard(item.isStandard)}</td>
          <td>{item.skill && formatDateTime(item.skill.createdAt, 'LLL', 1)}</td>
        </Fragment>
      );
      if (index === 0) {
        return (
          <tr key={index}>
            <td rowSpan={calculateLength(userSkills)}>{userIndex + 1}</td>
            <td rowSpan={calculateLength(userSkills)}>{userName}</td>
            {skillDetail}
          </tr>
        );
      } else {
        return <tr key={index}>{skillDetail}</tr>;
      }
    })
  ) : (
    <tr>
      <td>{userIndex}</td>
      <td>{userName}</td>
      <td colSpan="6">{t('not_found')}</td>
    </tr>
  );
};

const sortByUserSkill = (a, b) => {
  if (!calculateLength(a.userSkills)) return 1;
  else if (!calculateLength(b.userSkills)) return -1;
  else return a.userSkills[0].evaluationPoint - b.userSkills[0].evaluationPoint;
};

const _renderUsersInfo = (users, t) => {
  return users
    .sort((a, b) => sortByUserSkill(a, b))
    .map((item, index) => <tbody key={index}>{_renderUserSkills(index, item.name, item.userSkills, t)}</tbody>);
};

function TabAllSkillTable({ allSkillByUser, userName, skillId, skillSelected, rankSelected }) {
  const filterUserBySkill = allSkillByUser.filter(item => {
    let isFilter = true;
    if (userName) {
      isFilter = item.name.includes(userName);
    }
    if (isFilter && skillId) {
      isFilter = item.userSkills.some(userSkill => userSkill.skill && userSkill.skill.id === skillId);
    }
    if (isFilter && skillSelected) {
      isFilter = item.userSkills.some(userSkill => {
        return userSkill.rank && userSkill.rank.type === skillSelected;
      });
    }
    if (isFilter && rankSelected) {
      isFilter = item.userSkills.some(userSkill => {
        return userSkill.rank && userSkill.rank.label === rankSelected;
      });
    }
    return isFilter;
  });

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Table bordered responsive className={'admin-skill-table mb-0'}>
          <thead>
            <tr className="text-center">
              <TableHead fields={fields} t={t} />
            </tr>
          </thead>
          {!calculateLength(filterUserBySkill) ? (
            <tbody>
              <tr>
                <td colSpan={fields.length} className={'text-center font-size-15 text-muted'}>
                  {t('not_found')}
                </td>
              </tr>
            </tbody>
          ) : (
            _renderUsersInfo(filterUserBySkill, t)
          )}
        </Table>
      )}
    </NamespacesConsumer>
  );
}

TabAllSkillTable.propTypes = {
  allSkillByUser: array,
  skillId: number,
  userName: string,
  skillSelected: string,
  rankSelected: string
};

export default memo(TabAllSkillTable);
