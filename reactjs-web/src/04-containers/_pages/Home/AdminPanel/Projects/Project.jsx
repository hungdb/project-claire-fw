import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader/SpinLoader';
import React, { useEffect, memo, useState } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { array, func, object } from 'prop-types';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import { PERM_GET_USERS_OF_ALL_PROJECT } from '../../../../../07-constants/app-permission';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const Filters = LazyComponent(() => import('./Filters'));
const DataTable = LazyComponent(
  promiseDelayImport(import('./DataTable'), 2000),
  <SpinLoader color="primary" type="grow" />
);

const AppText = LazyComponent(() => import('../../../../../03-components/AppText'));

const Project = ({ getUsersOfAllProject, addNotification, usersInProjects, appPermissions, history, location }) => {
  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_GET_USERS_OF_ALL_PROJECT)) {
      getUsersOfAllProject();
    }
  };

  const [projectFiltered, setProjectFiltered] = useState([]);
  const [roleSelected, setRoleSelected] = useState('');

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const onFilterProject = (data, roleSelected) => {
    setProjectFiltered(data);
    setRoleSelected(roleSelected);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Container fluid className="px-md-5 py-4 py-md-5">
          <Row className="mb-4">
            <Col sm="3">
              <AppText type={'title-header'}>{t('members')}</AppText>
            </Col>
          </Row>
          <Filters history={history} onFilter={onFilterProject} location={location} usersInProjects={usersInProjects} />
          <DataTable roleSelected={roleSelected} projectFiltered={projectFiltered} />
        </Container>
      )}
    </NamespacesConsumer>
  );
};

Project.propTypes = {
  getUsersOfAllProject: func,
  addNotification: func,
  usersInProjects: array,
  appPermissions: array,
  history: object,
  location: object
};

export default memo(Project);
