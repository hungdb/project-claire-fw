import connect from 'react-redux/es/connect/connect';
import Project from './Project';
import { bindActionCreators } from 'redux';
import { getUsersOfAllProject } from '../../../../../01-actions/userInProject';
import { addNotification } from '../../../../../01-actions/notification';

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  usersInProjects: store.userInProject.usersInProjects
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getUsersOfAllProject,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Project);
