import React, { memo } from 'react';
import { array, string } from 'prop-types';
import { Link } from 'react-router-dom';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import TableHead from './../../../../../../03-components/TableHead';
import { calculateLength, formatDateTime } from './../../../../../../08-helpers/common';
import { PROJECT_ACTIVE_STATUS } from './../../../../../../05-utils/commonData';

const fields = [
  'no',
  'team',
  'total',
  'no',
  'name',
  'effort',
  'roles',
  'project_status',
  'duration_time',
  'sprint_active'
];
const classNames = [
  'w-5p text-center',
  'w-10p',
  'w-5p text-center',
  'w-5p text-center',
  'w-10p',
  'w-5p',
  'w-10p',
  'w-10p text-center',
  'w-10p',
  'w-20p text-center'
];

const DataTable = ({ projectFiltered, roleSelected }) => {
  const renderUserInfo = (user, userIndex, t) => (
    <>
      <td className="text-center align-middle">{userIndex}</td>
      <td className="text-center align-middle">{user && user.name}</td>
      <td className="text-center align-middle">{user && user.effort}</td>
      <td className="user-roles-data align-middle">
        {calculateLength(user.roles) ? (
          user.roles.map(role => (
            <p key={role.id} className="mb-1">
              {role.name}
            </p>
          ))
        ) : (
          <p>{t('not_found')}</p>
        )}
      </td>
    </>
  );

  const onFilterMemberByRole = members => {
    if (roleSelected) return calculateLength(members.roles) && members.roles.some(r => r.id === roleSelected);
    else return true;
  };

  const getPriorityLevel = data => {
    let result = 10;
    data &&
      data.roles.forEach(value => {
        if (Math.min(value.priorityLevel)) result = value.priorityLevel;
      });
    return result;
  };

  const renderSprintActive = (sprint, rowSpan, t) => {
    if (sprint) {
      const sprintUrl = `/sprints/${sprint.projectId}/board/1`;
      const sprintFromToEndDate = `${formatDateTime(sprint.startDate, 'DD/MM/YYYY')} - ${formatDateTime(
        sprint.endDate,
        'DD/MM/YYYY'
      )}`;
      return (
        <td className="align-middle" rowSpan={rowSpan}>
          <p className="mb-1">
            <Link className="disable-text-link text-bold" to={sprintUrl}>
              {' '}
              {sprint.name}{' '}
            </Link>
          </p>
          ( {sprintFromToEndDate} )
        </td>
      );
    }
    return (
      <td className="align-middle" rowSpan={rowSpan}>
        {t('no_sprint_active')}
      </td>
    );
  };

  const renderprojectFiltered = (project, projectIndex, t) => {
    const projectStatus = PROJECT_ACTIVE_STATUS.find(p => p.value === project.isActive);
    const totalMember = calculateLength(project.members);
    if (totalMember && project) {
      return project.members
        .filter(m => onFilterMemberByRole(m))
        .sort((a, b) => getPriorityLevel(a) - getPriorityLevel(b))
        .map((member, index) => {
          const createAt = formatDateTime(project.createdAt, 'DD/MM/YYYY');
          const finishAt = project.finishedAt && formatDateTime(project.finishedAt, 'DD/MM/YYYY');
          if (index === 0) {
            return (
              <tr>
                <td rowSpan={totalMember}>{projectIndex}</td>
                <td rowSpan={totalMember}>{project.name}</td>
                <td rowSpan={totalMember}>{totalMember}</td>
                {renderUserInfo(member, index, t)}
                <td rowSpan={totalMember} className="text-center align-middle">
                  {projectStatus && projectStatus.label}
                </td>
                <td rowSpan={totalMember} className="text-center align-middle">
                  <p className="mb-1 text-bold">{t('start_time')}</p>
                  {createAt}
                  {finishAt && (
                    <>
                      <hr />
                      <p className="mb-1 text-bold">{t('end_time')}</p>
                      <span>{finishAt}</span>
                    </>
                  )}
                </td>
                {renderSprintActive(project.sprintActive, totalMember, t)}
              </tr>
            );
          }
          return <tr key={index + 1}>{renderUserInfo(member, index, t)}</tr>;
        });
    }
    return (
      <tr>
        <td>{projectIndex}</td>
        <td>{project && project.name}</td>
        <td>{totalMember}</td>
        <td className="text-center align-middle text-muted" colSpan={7}>
          {t('not_found')}
        </td>
      </tr>
    );
  };

  const getAllProjects = (projects, t) => {
    // sort project's name is common to last record
    const common = projects.find(p => p.name.toUpperCase() === 'COMMON');
    const projectsExcludeCommon = projects.filter(p => p.name.toUpperCase() !== 'COMMON');
    const data = common ? [...projectsExcludeCommon, common] : [...projectsExcludeCommon];
    return (
      calculateLength(projects) &&
      data.map((project, index) => <tbody key={project.id}>{renderprojectFiltered(project, index, t)}</tbody>)
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Table bordered responsive className={'admin-project-table'}>
          <thead>
            <tr>
              <TableHead fields={fields} t={t} classes={classNames} />
            </tr>
          </thead>
          {calculateLength(projectFiltered) ? (
            getAllProjects(projectFiltered, t)
          ) : (
            <tr>
              <td colSpan={calculateLength(fields)} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
        </Table>
      )}
    </NamespacesConsumer>
  );
};

DataTable.propTypes = {
  projectFiltered: array,
  roleSelected: string
};

export default memo(DataTable);
