import Skeleton from 'react-loading-skeleton';
import queryString from 'query-string';
import LazyComponent from './../../../../../../03-components/LazyComponent';
import React, { memo, useState, useMemo, useEffect } from 'react';
import { array, func, object } from 'prop-types';
import { Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { PROJECT_ACTIVE_STATUS, PROJECT_MANAGER_NAME } from '../../../../../../05-utils/commonData';
import {
  promiseDelayImport,
  getAllPMByProject,
  getRolesOption,
  getUserOption,
  getProjectOption,
  calculateLength,
  getDateAt000000ZeroGMT,
  getDateAt235959ZeroGMT,
  onRewriteUrl
} from './../../../../../../08-helpers/common';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('./../../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const ControlDatePicker = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/DateTimePicker')),
  <Skeleton height={38} />
);

const Filters = ({ usersInProjects, onFilter, history, location }) => {
  const [state, setState] = useState({
    projectSelected: '',
    PMSelected: '',
    startDateSelected: null,
    projectStatus: PROJECT_ACTIVE_STATUS[1].value,
    userSelected: '',
    roleSelected: '',
    endDateSelected: null
  });

  const {
    projectSelected,
    PMSelected,
    projectStatus,
    userSelected,
    startDateSelected,
    roleSelected,
    endDateSelected
  } = state;

  const onFilterAllProjects = projects => {
    return calculateLength(projects)
      ? projects.filter(p => {
          let isFilter = true;
          if (projectSelected) isFilter = p.id === projectSelected;
          if (PMSelected) {
            const member = p.members.find(m => m.id === PMSelected);
            const role = member && member.roles.map(r => r);
            isFilter = isFilter && (role && role.some(value => value.name.toUpperCase() === PROJECT_MANAGER_NAME));
          }
          if (roleSelected) {
            const allRoles = [];
            p &&
              p.members.forEach(m => {
                m.roles.forEach(r => {
                  allRoles.push(r);
                });
              });
            isFilter = isFilter && (allRoles && allRoles.some(r => r.id === roleSelected));
          }
          if (projectStatus || projectStatus === 0) isFilter = isFilter && p.isActive === projectStatus;
          if (userSelected) isFilter = isFilter && p.members.some(m => m.id === userSelected);
          if (startDateSelected) {
            const isFilterStartDate =
              (p.createdAt && getDateAt000000ZeroGMT(new Date(p.createdAt))) <=
              (startDateSelected && getDateAt000000ZeroGMT(new Date(startDateSelected)));
            const isFilterEndDate =
              (p.finishedAt && getDateAt235959ZeroGMT(new Date(p.finishedAt))) >=
              (startDateSelected && getDateAt235959ZeroGMT(new Date(startDateSelected)));
            if (p.finishedAt) isFilter = isFilter && isFilterEndDate;
            isFilter = isFilter && isFilterStartDate;
          }
          if (endDateSelected) {
            const isFilterEndDate =
              (p.finishedAt && getDateAt235959ZeroGMT(new Date(p.finishedAt))) >=
              (endDateSelected && getDateAt235959ZeroGMT(new Date(endDateSelected)));
            const isFilterStartDate =
              (p.createdAt && getDateAt000000ZeroGMT(new Date(p.createdAt))) <=
              (endDateSelected && getDateAt000000ZeroGMT(new Date(endDateSelected)));
            if (p.finishedAt) isFilter = isFilter && isFilterEndDate;
            isFilter = isFilter && isFilterStartDate;
          }
          return isFilter;
        })
      : [];
  };

  const onFilterAllProjectsMemo = useMemo(() => onFilterAllProjects(usersInProjects), [state, usersInProjects]);
  const projectsOption = useMemo(() => getProjectOption(usersInProjects), [usersInProjects]);
  const projectManagerOptions = useMemo(() => getAllPMByProject(usersInProjects), [usersInProjects]);
  const usersOption = useMemo(() => getUserOption(usersInProjects), [usersInProjects]);
  const rolesOption = useMemo(() => getRolesOption(usersInProjects), [usersInProjects]);

  useEffect(() => {
    onFilter(onFilterAllProjectsMemo, roleSelected);
  }, [state, usersInProjects]);

  useEffect(() => {
    if (location.search) {
      const {
        projectSelected,
        PMSelected,
        projectStatus,
        userSelected,
        startDateSelected,
        roleSelected,
        endDateSelected
      } = queryString.parse(location.search);
      setState({
        PMSelected: Number(PMSelected) || '',
        userSelected: Number(userSelected) || '',
        roleSelected: Number(roleSelected) || '',
        projectSelected: Number(projectSelected) || '',
        endDateSelected: endDateSelected ? new Date(endDateSelected) : null,
        startDateSelected: startDateSelected ? new Date(startDateSelected) : null,
        projectStatus: projectStatus || projectStatus === 0 ? Number(projectStatus) : ''
      });
    }
  }, [location, calculateLength(usersInProjects)]);

  const setStateAndQueryUrl = (name, stateValue) => {
    setState({ ...state, ...(state[name] = stateValue) });
    onRewriteUrl(history, location, stateValue, name || '');
  };

  const onFilterProject = e => setStateAndQueryUrl('projectSelected', e && Number(e.value));
  const onFilterPM = e => setStateAndQueryUrl('PMSelected', e && Number(e.value));
  const onFilterProjectStatus = e => setStateAndQueryUrl('projectStatus', e && Number(e.value));
  const onFilterRoles = e => setStateAndQueryUrl('roleSelected', e && Number(e.value));
  const onFilterUser = e => setStateAndQueryUrl('userSelected', e && Number(e.value));
  const onFilterStartDate = startDateSelected => setStateAndQueryUrl('startDateSelected', startDateSelected);
  const onFilterEndDate = endDateSelected => setStateAndQueryUrl('endDateSelected', endDateSelected);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Row>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <SelectPropsBox
              placeholder={'select_project'}
              options={projectsOption}
              value={projectSelected}
              onChange={onFilterProject}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <SelectPropsBox
              value={PMSelected}
              onChange={onFilterPM}
              options={projectManagerOptions}
              placeholder={'select_project_manager'}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <SelectPropsBox
              value={projectStatus}
              onChange={onFilterProjectStatus}
              options={PROJECT_ACTIVE_STATUS}
              placeholder={'select_project_status'}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <SelectPropsBox
              value={userSelected}
              options={usersOption}
              onChange={onFilterUser}
              placeholder={'select_user'}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <ControlDatePicker
              value={startDateSelected}
              format="DD/MM/YYYY"
              isLocalFormat
              placeholder={t('start_date')}
              onChange={onFilterStartDate}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <ControlDatePicker
              value={endDateSelected}
              format="DD/MM/YYYY"
              isLocalFormat
              placeholder={t('end_date')}
              onChange={onFilterEndDate}
            />
          </Col>
          <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
            <SelectPropsBox
              value={roleSelected}
              options={rolesOption}
              placeholder={'select_role'}
              onChange={onFilterRoles}
            />
          </Col>
        </Row>
      )}
    </NamespacesConsumer>
  );
};

Filters.propTypes = {
  usersInProjects: array,
  onFilter: func,
  history: object,
  location: object
};

export default memo(Filters);
