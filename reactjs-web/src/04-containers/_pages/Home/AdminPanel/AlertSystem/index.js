import AlertSystem from './AlertSystem';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { getListAlertSystem, getListUser, getListProject } from '../../../../../01-actions/alert-system';

const mapStateTopProps = state => {
  return {
    listAlertSystem: state.alertSystem.listAlertSystem,
    total: state.alertSystem.total,
    listUser: state.alertSystem.listUser,
    listProject: state.alertSystem.listProject,
    appPermissions: state.auth.permissions
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListAlertSystem,
      getListUser,
      getListProject
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateTopProps,
    mapDispatchToProps
  )(AlertSystem)
);
