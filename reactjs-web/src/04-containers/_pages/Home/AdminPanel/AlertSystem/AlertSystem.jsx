import LazyComponent from '../../../../../03-components/LazyComponent';
import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import TableHead from './../../../../../03-components/TableHead';
import React, { memo, Fragment, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { array, func, number, object } from 'prop-types';
import { Col, Row, Table, Container, Badge } from 'reactstrap';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { SUBSCRIBE_METHODS } from '../../../../../05-utils/commonData';
import { promiseDelayImport, parseJSONHasValue, displayNumberRecord } from './../../../../../08-helpers/common';
import { ROWS_PER_PAGE_OPTIONS } from '../../../../../05-utils/commonData';
import {
  PERM_GET_LIST_ALERT_SUBSCRIPTION,
  PERM_LIST_USERS,
  PERM_LIST_PROJECTS
} from '../../../../../07-constants/app-permission';

const AppText = LazyComponent(() => import('../../../../../03-components/AppText'));
const Pagination = LazyComponent(promiseDelayImport(import('../../../../../03-components/Pagination')));
const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('./../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const methodColors = ['light', 'primary', 'success', 'info'];
const fields = ['No.', 'User', 'Project', 'Environments', 'Methods'];

const renderMethod = listMethodId => {
  {
    const listMethodIdParse = parseJSONHasValue(listMethodId).sort();
    if (listMethodId.length === 0) return 'NOT FOUNND';
    // eslint-disable-next-line
    return listMethodIdParse.map((methodId, index) => {
      const method = SUBSCRIBE_METHODS.filter(item => {
        return item.value === methodId;
      })[0];

      if (method) {
        return (
          <Badge color={methodColors[methodId]} pill key={index} style={{ margin: '5px' }}>
            {method.label}
          </Badge>
        );
      }
    });
  }
};

const AlertSystem = ({
  getListAlertSystem,
  getListUser,
  getListProject,
  listUser,
  listProject,
  listAlertSystem,
  total,
  history,
  match,
  appPermissions
}) => {
  const [userId, setUserId] = useState(null);
  const [projectId, setProjectId] = useState(null);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(ROWS_PER_PAGE_OPTIONS[1]);

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_USERS)) {
      getListUser();
    }
    if (isAllowPermission(appPermissions, PERM_LIST_PROJECTS)) {
      getListProject();
    }
    if (isAllowPermission(appPermissions, PERM_GET_LIST_ALERT_SUBSCRIPTION)) {
      getListAlertSystem({ page, limit, projectId, userId });
    }
  };

  useEffect(() => {
    const urlQuery = JSON.parse(localStorage.getItem('alertSystemUrlQuery'));
    const { userId, projectId } = urlQuery || {};

    if (userId) setUserId(parseInt(userId));
    if (projectId) setProjectId(parseInt(projectId));
  }, []);

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  useEffect(() => {
    let query = [];
    if (userId) {
      query = [...query, `userId=${userId}`];
    }
    if (projectId) {
      query = [...query, `projectId=${projectId}`];
    }

    query = query.reduce((total, item) => {
      if (!total) return `${total}?${item}`;
      return `${total}&${item}`;
    }, '');
    history.push(`${match.path}${query}`);

    const urlQuery = queryString.parse(history.location.search);
    localStorage.setItem('alertSystemUrlQuery', JSON.stringify(urlQuery));

    checkRoleAndLoad({ page, limit, projectId, userId });
  }, [userId, projectId, page, limit]);

  const onSelectUser = itemSelected => {
    const userId = itemSelected ? itemSelected.value : null;
    setUserId(userId);
  };

  const onSelectProject = itemSelected => {
    const projectId = itemSelected ? itemSelected.value : null;
    setProjectId(projectId);
  };

  const renderEnvironment = environments => {
    if (!environments || environments.length === 0) return 'NOT FOUND';
    return environments.map(item => item.name);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Container fluid className="px-md-5 py-4 py-md-5">
          <Row className="mb-4">
            <Col sm="3">
              <AppText type={'title-header'}>{t('List alert system')}</AppText>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
              <SelectPropsBox
                placeholder={'select_user'}
                options={listUser}
                value={userId}
                keyLabel="name"
                keyValue="id"
                onChange={itemSelected => onSelectUser(itemSelected)}
              />
            </Col>
            <Col xs="12" sm="6" md="4" xl="3" className="mb-2-xs mb-4">
              <SelectPropsBox
                value={projectId}
                options={listProject}
                placeholder={'select_project'}
                keyLabel="name"
                keyValue="id"
                onChange={itemSelected => onSelectProject(itemSelected)}
              />
            </Col>
          </Row>
          <Table bordered responsive hover className={'admin-project-table'}>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} />
              </tr>
            </thead>
            <tbody>
              {listAlertSystem.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <Fragment>
                      <td className="align-middle text-center">{displayNumberRecord(page, limit, index)}</td>
                      <td className="align-middle text-center">{item.User && item.User.name}</td>
                      <td className="align-middle text-center">{item.Project && item.Project.name}</td>
                      <td className="align-middle text-center">{renderEnvironment(item.environments)}</td>
                      <td className="align-middle">{renderMethod(item.methodIds)}</td>
                    </Fragment>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <Pagination
            totalItems={total}
            pageSize={limit}
            value={limit}
            activePageProps={page}
            onSelect={page => {
              setPage(page);
              getListAlertSystem({ page, limit, userId, projectId });
            }}
            onChange={e => {
              const limit = e.value;
              setLimit(limit);
              getListAlertSystem({ page, limit, userId, projectId });
            }}
            rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
            isClearable={false}
          />
        </Container>
      )}
    </NamespacesConsumer>
  );
};

AlertSystem.propTypes = {
  getListAlertSystem: func,
  getListUser: func,
  getListProject: func,
  total: number,
  listAlertSystem: array,
  listUser: array,
  listProject: array,
  history: object,
  location: object,
  match: object,
  appPermissions: array
};

export default memo(AlertSystem);
