import './styles.css';
import AppText from '../../../../../../03-components/AppText';
import TableHead from '../../../../../../03-components/TableHead';
import AllocationLabelDialog from '../../../../../dialog/allocation-label/allocation-label-dialog';
import { array, func } from 'prop-types';
import React, { useState, memo, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Table } from 'reactstrap';
import { MODE_DIALOG } from '../../../../../../05-utils/commonData';
import { calculateLength } from '../../../../../../08-helpers/common';

const AllocationLabel = ({
  allocationLabels,
  getAllocationLabels,
  createAllocationLabel,
  updateAllocationLabel,
  removeAllocationLabel,
  clearAllocationLabels
}) => {
  const [state, setState] = useState({
    allocationLabel: null,
    isOpen: false,
    modeDialog: null
  });

  const { allocationLabel, isOpen, modeDialog } = state;

  useEffect(() => {
    getAllocationLabels();
    return () => {
      clearAllocationLabels();
    };
  }, []);

  const toggleDialog = () => {
    setState({
      isOpen: false,
      modeDialog: false,
      allocationLabel: null
    });
  };

  const onCreateAllocationLabel = values => {
    setState({
      ...state,
      isOpen: false,
      modeDialog: null
    });
    createAllocationLabel(values);
  };

  const onUpdateAllocationLabel = values => {
    if (allocationLabel.name !== values.name) {
      updateAllocationLabel(values);
    }
    setState({ isOpen: false, modeDialog: null, allocationLabel: null });
  };

  const onRemoveAllocationLabel = values => {
    removeAllocationLabel(values);
    setState({ isOpen: false, modeDialog: null, allocationLabel: null });
  };

  const onOpenDialogUpdate = allocationLabel => {
    setState({
      isOpen: true,
      modeDialog: MODE_DIALOG.SAVE,
      allocationLabel
    });
  };

  const onOpenDialogAdd = () => {
    setState({
      ...state,
      isOpen: true,
      modeDialog: MODE_DIALOG.ADD
    });
  };

  const renderHeader = t => {
    const fields = ['no', 'allocation_label_management.name', 'allocation_label_management.action'];
    const classes = ['w-10p', 'w-80p', 'w-10p'];
    return (
      <thead>
        <tr>
          <TableHead fields={fields} t={t} classes={classes} />
        </tr>
      </thead>
    );
  };

  const renderBody = t => {
    return (
      <tbody>
        {calculateLength(allocationLabels) ? (
          allocationLabels.map((allocationLabel, index) => {
            if (allocationLabel) {
              const number = index + 1;
              const { name = '' } = allocationLabel;
              return (
                <tr key={number} style={{ textAlign: ' center' }}>
                  <td className={'font-size-13'}>{number}</td>
                  <td className={'font-size-13'}>{name}</td>
                  <td className={'font-size-13'}>
                    <span className={'cursor-pointer'} onClick={() => onOpenDialogUpdate(allocationLabel)}>
                      <i className="fa fa-pencil color-base-btn" />
                    </span>
                  </td>
                </tr>
              );
            }
            return '';
          })
        ) : (
          <tr>
            <td colSpan={4} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className={'margin-top-40px'}>
          <div className={'margin-bottom-30px d-flex'}>
            <AppText type={'title-header'}>{t('allocation_label_management.title_allocation_label')}</AppText>
          </div>
          <AllocationLabelDialog
            isOpen={isOpen}
            mode={modeDialog}
            item={allocationLabel}
            toggle={toggleDialog}
            onCreate={onCreateAllocationLabel}
            onSave={onUpdateAllocationLabel}
            onRemove={onRemoveAllocationLabel}
          />
          <div className={'d-flex justify-content-end mb-2'}>
            <Button className={'orange-btn'} onClick={onOpenDialogAdd} size={'md'}>
              <i className={'fa fa-plus'} /> &nbsp;&nbsp;{t('add_label')}
            </Button>
          </div>
          <Table bordered responsive striped hover>
            {renderHeader(t)}
            {renderBody(t)}
          </Table>
        </div>
      )}
    </NamespacesConsumer>
  );
};

AllocationLabel.propTypes = {
  allocationLabels: array,
  listCurrentUsers: array,
  getAllocationLabels: func,
  createAllocationLabel: func,
  updateAllocationLabel: func,
  removeAllocationLabel: func,
  clearAllocationLabels: func
};

export default memo(AllocationLabel);
