import AllocationLabel from './AllocationLabel';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import {
  getAllocationLabels,
  createAllocationLabel,
  updateAllocationLabel,
  removeAllocationLabel,
  clearAllocationLabels
} from '../../../../../../01-actions/allocation-label';

const mapStateToProps = state => {
  return {
    ...state.allocationLabel
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllocationLabels,
      createAllocationLabel,
      updateAllocationLabel,
      removeAllocationLabel,
      clearAllocationLabels
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllocationLabel);
