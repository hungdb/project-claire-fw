import './style.css';
import Switch from 'react-switch';
import FontAwesomeIcon from '../../../../../03-components/FontAwesomeIcon';
import SpinLoader from '../../../../../03-components/SpinLoader';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useEffect, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func, array } from 'prop-types';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import { PERM_GET_STATE_OF_ALERT_SYSTEM, PERM_UPDATE_ALERT_SYSTEM } from '../../../../../07-constants/app-permission';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const AppText = LazyComponent(() => import('../../../../../03-components/AppText'), null);
const CheckList = LazyComponent(promiseDelayImport(import('./CheckListManagement')), <SpinLoader color="primary" />);
const KPICoefficient = LazyComponent(promiseDelayImport(import('./KPICoefficient')), <SpinLoader color="primary" />);
const AllocationLabel = LazyComponent(promiseDelayImport(import('./AllocationLabel')), <SpinLoader color="primary" />);

const Setting = ({ isActive, activeInactiveAlertSystem, getStatusOfAlertSystem, appPermissions, addNotification }) => {
  const callUpdate = () => {
    if (isAllowPermission(appPermissions, PERM_GET_STATE_OF_ALERT_SYSTEM)) {
      getStatusOfAlertSystem();
    }
  };

  const handleToggle = () => activeInactiveAlertSystem();

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    callUpdate();
  }, [appPermissions]);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page skill-management">
          <div className={'d-flex margin-bottom-30px'}>
            <AppText type={'title-header'}>{t('setting')}</AppText>
          </div>
          <div className="border p-3">
            <div className="mb-3">
              <h5>{t('option')}</h5>
            </div>
            <div className="d-flex align-items-center">
              <FontAwesomeIcon classes="fa fa-bell color-base-btn" />
              <p className="mb-0 ml-3">{t('alert_system')}</p>
              <Switch
                onColor="#0ba1ee"
                checked={!!isActive}
                onChange={handleToggle}
                className="align-middle ml-auto"
                disabled={!isAllowPermission(appPermissions, PERM_UPDATE_ALERT_SYSTEM)}
              />
            </div>
          </div>
          <CheckList />
          <AllocationLabel />
          <KPICoefficient />
        </div>
      )}
    </NamespacesConsumer>
  );
};

Setting.propTypes = {
  isActive: bool,
  activeInactiveAlertSystem: func,
  getStatusOfAlertSystem: func,
  appPermissions: array,
  addNotification: func
};

export default memo(Setting);
