import connect from 'react-redux/es/connect/connect';
import Setting from './Setting';
import { bindActionCreators } from 'redux';
import { activeInactiveAlertSystem, getStatusOfAlertSystem } from '../../../../../01-actions/setting';
import { addNotification } from '../../../../../01-actions/notification';

const mapStateToProps = store => {
  return {
    // auth
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,
    status: store.setting.status,
    settingError: store.setting.settingError,
    isActive: store.setting.isActive
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      activeInactiveAlertSystem,
      getStatusOfAlertSystem,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Setting);
