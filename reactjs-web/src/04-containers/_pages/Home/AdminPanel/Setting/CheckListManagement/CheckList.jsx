import './style.css';
import TableHead from '../../../../../../03-components/TableHead';
import AppText from '../../../../../../03-components/AppText';
import CheckListDialog from '../../../../../dialog/check-list/check-list-item';
import React, { memo, useState, useEffect } from 'react';
import { Button, Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { func, array } from 'prop-types';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { PERM_CREATE_ITEM_CHECK, PERM_UPDATE_ITEM_CHECK } from '../../../../../../07-constants/app-permission';

const CheckList = ({
  listAllItemCheck,
  appPermissions,
  listItemsCheck,
  removeItemCheck,
  updateItemCheck,
  createItemCheck
}) => {
  const [state, setState] = useState({
    showItemCheckDialog: false,
    editItemCheck: null,
    name: '',
    description: '',
    itemCheckId: 0
  });

  const { showItemCheckDialog, editItemCheck, name, description, itemCheckId } = state;

  useEffect(() => {
    listAllItemCheck();
  }, []);

  const _renderHeader = t => {
    const fields = ['no', 'name', 'action'];
    const classes = ['w-10p', 'w-80p', 'w-10p'];
    return (
      <tr>
        <TableHead fields={fields} t={t} classes={classes} />
      </tr>
    );
  };

  const _renderBody = () => {
    return listItemsCheck.map((item, index) => {
      return (
        <tr key={index}>
          <td className="text-center font-size-13">{index + 1}</td>
          <td className="text-center font-size-13">{item.name}</td>
          <td className="text-center font-size-13">
            {isAllowPermission(appPermissions, PERM_UPDATE_ITEM_CHECK) && (
              <span
                className={'cursor-pointer'}
                onClick={() => {
                  setState({
                    showItemCheckDialog: true,
                    editItemCheck: 1,
                    name: item.name,
                    description: item.description,
                    itemCheckId: item.id
                  });
                }}
              >
                <i className="fa fa-pencil color-base-btn" />
              </span>
            )}
          </td>
        </tr>
      );
    });
  };

  const onCloseDialog = () => {
    setState({
      ...state,
      showItemCheckDialog: false
    });
  };

  const onClearState = () => {
    setState({
      showItemCheckDialog: true,
      editItemCheck: null,
      id: 0,
      name: '',
      description: ''
    });
  };

  const onOkDialog = (editMode, removeMode, value) => {
    setState({
      ...state,
      showItemCheckDialog: false
    });
    if (editMode) {
      if (removeMode) {
        removeItemCheck(value);
      } else {
        updateItemCheck(value);
      }
    } else {
      createItemCheck(value);
    }
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'margin-top-40px'}>
          <div className={'margin-bottom-30px d-flex'}>
            <AppText type={'title-header'}>{t('CheckList')}</AppText>
          </div>
          <div className={'d-flex justify-content-end mb-2'}>
            {isAllowPermission(appPermissions, PERM_CREATE_ITEM_CHECK) && (
              <Button className={'orange-btn'} onClick={onClearState} size={'md'}>
                <i className="fa fa-plus" aria-hidden="true" />
                &nbsp;&nbsp;{t('add_checklist')}
              </Button>
            )}
          </div>
          <Table responsive bordered striped hover>
            <thead>{_renderHeader(t)}</thead>
            <tbody>{_renderBody()}</tbody>
          </Table>
          <CheckListDialog
            isOpen={showItemCheckDialog}
            editMode={!!editItemCheck}
            toggle={onCloseDialog}
            value={{
              id: itemCheckId,
              name: name,
              description: description
            }}
            onOK={onOkDialog}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

CheckList.propTypes = {
  listAllItemCheck: func,
  appPermissions: array,
  listItemsCheck: array,
  removeItemCheck: func,
  updateItemCheck: func,
  createItemCheck: func
};

export default memo(CheckList);
