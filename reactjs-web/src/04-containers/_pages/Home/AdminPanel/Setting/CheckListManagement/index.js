import CheckList from './CheckList';
import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import {
  listAllItemCheck,
  createItemCheck,
  removeItemCheck,
  updateItemCheck,
  checkUncheckItem,
  listItemCheckOfProject,
  clearItemChecked
} from '../../../../../../01-actions/check-list';

const mapStateToProps = store => {
  return {
    // common
    appPermissions: store.auth.permissions,
    error: store.common.error,
    Status: store.checkList.status,
    checkListError: store.checkList.checkListError,
    listItemsCheck: store.checkList.listItemsCheck,
    listCheckedItemsOfProject: store.checkList.listCheckedItemsOfProject
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAllItemCheck,
      createItemCheck,
      removeItemCheck,
      updateItemCheck,
      checkUncheckItem,
      listItemCheckOfProject,
      clearItemChecked
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckList);
