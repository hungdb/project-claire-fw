import './styles.css';
import KpiCoefficientDialog from '../../../../../dialog/kpi-coefficient/kpi-coefficient-dialog';
import TableHead from '../../../../../../03-components/TableHead';
import AppText from '../../../../../../03-components/AppText';
import React, { memo, useEffect, useState } from 'react';
import { array, func } from 'prop-types';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, formatDateTime } from '../../../../../../08-helpers/common';
import { TYPE_KPI_COEFFICIENT } from '../../../../../../05-utils/commonData';

const KpiCoefficient = ({ listKpiCoefficient, getKpiCoefficient, updateKpiCoefficient }) => {
  const [state, setState] = useState({
    isOpenDialog: false,
    dataUpdate: null,
    typeUpdate: null
  });

  const { isOpenDialog, dataUpdate, typeUpdate } = state;

  useEffect(() => {
    getKpiCoefficient();
  }, []);

  const renderTableKpiCoefficient = t => {
    const fields = ['no', 'kpi_coefficient_setting.data', 'kpi_coefficient_setting.created_at', 'action'];
    const classes = ['w-10p', 'w-50p', 'w-20p', 'w-10p'];
    return (
      <Table responsive bordered striped hover>
        <thead>
          <tr>
            <TableHead fields={fields} t={t} classes={classes} />
          </tr>
        </thead>
        <tbody>
          {listKpiCoefficient && calculateLength(listKpiCoefficient) ? (
            listKpiCoefficient
              .map((item, index) => {
                if (item) {
                  const type = item.type;
                  const percentSkillReview = item.data.percentSkillReview * 100 || 0;
                  const percentSkillTest = item.data.percentSkillTest * 100 || 0;
                  const percentDoProcess = item.data.percentDoProcess * 100 || 0;
                  const percentPeopleManagement = item.data.percentPeopleManagement * 100 || 0;
                  const percentWorkManagement = item.data.percentWorkManagement * 100 || 0;
                  const percentDedication = item.data.percentDedication * 100 || 0;
                  const percentResponsibility = item.data.percentResponsibility * 100 || 0;
                  const percentTaskComplete = item.data.percentTaskComplete * 100 || 0;
                  const createdAt = formatDateTime(item.createdAt, 'LLLL', 1);
                  let bodyData = '';
                  switch (type) {
                    case TYPE_KPI_COEFFICIENT.KPI_SKILL:
                      bodyData = (
                        <ul className="pl-0 mb-0">
                          <li>
                            <p> {t('kpi_coefficient_setting.skill_review') + ':  ' + percentSkillReview + '(%)'} </p>
                          </li>
                          <li>
                            <p className="mb-0">
                              {' '}
                              {t('kpi_coefficient_setting.skill_test') + ':  ' + percentSkillTest + '(%)'}{' '}
                            </p>
                          </li>
                        </ul>
                      );
                      break;
                    case TYPE_KPI_COEFFICIENT.KPI_LEADER:
                      bodyData = (
                        <ul className="pl-0 mb-0">
                          <li>
                            <p> {t('kpi_coefficient_setting.do_process') + ':  ' + percentDoProcess + '(%)'} </p>
                          </li>
                          <li>
                            <p>
                              {' '}
                              {t('kpi_coefficient_setting.people_management') +
                                ':  ' +
                                percentPeopleManagement +
                                '(%)'}{' '}
                            </p>
                          </li>
                          <li>
                            <p className="mb-0">
                              {' '}
                              {t('kpi_coefficient_setting.work_management') +
                                ':  ' +
                                percentWorkManagement +
                                '(%)'}{' '}
                            </p>
                          </li>
                        </ul>
                      );
                      break;
                    case TYPE_KPI_COEFFICIENT.KPI_PROJECT:
                      bodyData = (
                        <ul className="pl-0 mb-0">
                          <li>
                            <p> {t('kpi_coefficient_setting.dedication') + ':  ' + percentDedication + '(%)'} </p>
                          </li>
                          <li>
                            <p>
                              {' '}
                              {t('kpi_coefficient_setting.responsibility') + ':  ' + percentResponsibility + '(%)'}{' '}
                            </p>
                          </li>
                          <li>
                            <p className="mb-0">
                              {' '}
                              {t('kpi_coefficient_setting.task_complete') + ':  ' + percentTaskComplete + '(%)'}{' '}
                            </p>
                          </li>
                        </ul>
                      );
                      break;
                    default:
                      break;
                  }
                  return (
                    <tr key={index}>
                      <td className={'align-middle text-center'}> {index + 1} </td>
                      <td className={'align-middle text-center body-data'}> {bodyData} </td>
                      <td className={'align-middle text-center'}> {createdAt} </td>
                      <td className={'align-middle text-center font-size-13'}>
                        <span
                          title={t('edit_behavior')}
                          className={'cursor-pointer'}
                          onClick={() => {
                            let dataUpdate = [];
                            switch (type) {
                              case TYPE_KPI_COEFFICIENT.KPI_SKILL:
                                dataUpdate = [
                                  {
                                    label: t('kpi_coefficient_setting.skill_review'),
                                    value: percentSkillReview,
                                    variable: 'percentSkillReview'
                                  },
                                  {
                                    label: t('kpi_coefficient_setting.skill_test'),
                                    value: percentSkillTest,
                                    variable: 'percentSkillTest'
                                  }
                                ];
                                break;
                              case TYPE_KPI_COEFFICIENT.KPI_LEADER:
                                dataUpdate = [
                                  {
                                    label: t('kpi_coefficient_setting.do_process'),
                                    value: percentDoProcess,
                                    variable: 'percentDoProcess'
                                  },
                                  {
                                    label: t('kpi_coefficient_setting.people_management'),
                                    value: percentPeopleManagement,
                                    variable: 'percentPeopleManagement'
                                  },
                                  {
                                    label: t('kpi_coefficient_setting.work_management'),
                                    value: percentWorkManagement,
                                    variable: 'percentWorkManagement'
                                  }
                                ];
                                break;
                              case TYPE_KPI_COEFFICIENT.KPI_PROJECT:
                                dataUpdate = [
                                  {
                                    label: t('kpi_coefficient_setting.dedication'),
                                    value: percentDedication,
                                    variable: 'percentDedication'
                                  },
                                  {
                                    label: t('kpi_coefficient_setting.responsibility'),
                                    value: percentResponsibility,
                                    variable: 'percentResponsibility'
                                  },
                                  {
                                    label: t('kpi_coefficient_setting.task_complete'),
                                    value: percentTaskComplete,
                                    variable: 'percentTaskComplete'
                                  }
                                ];
                                break;
                              default:
                                break;
                            }
                            setState({
                              isOpenDialog: true,
                              dataUpdate,
                              typeUpdate: type
                            });
                          }}
                        >
                          <i className="fa fa-pencil color-base-btn" />
                        </span>
                      </td>
                    </tr>
                  );
                } else {
                  return null;
                }
              })
              .filter(Boolean)
          ) : (
            <tr>
              <td colSpan={10} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'margin-top-40px'}>
          <div className={'margin-bottom-30px d-flex'}>
            <AppText type={'title-header'}>{t('kpi_coefficient_setting.title')}</AppText>
          </div>
          <KpiCoefficientDialog
            isOpen={isOpenDialog}
            onCancel={() => {
              setState({
                ...state,
                isOpenDialog: false,
                dataUpdate: null
              });
            }}
            dataUpdate={dataUpdate}
            onSave={newData => {
              updateKpiCoefficient(typeUpdate, newData);
              setState({
                isOpenDialog: false,
                dataUpdate: null,
                typeUpdate: null
              });
            }}
          />
          {renderTableKpiCoefficient(t)}
        </div>
      )}
    </NamespacesConsumer>
  );
};

KpiCoefficient.propTypes = {
  listKpiCoefficient: array,
  getKpiCoefficient: func,
  updateKpiCoefficient: func
};

export default memo(KpiCoefficient);
