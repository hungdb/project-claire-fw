import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import KPICoefficient from './KPICoefficient';
import { getKpiCoefficient, updateKpiCoefficient } from '../../../../../../01-actions/kpi-cofficient';

const mapStateToProps = state => {
  return {
    listKpiCoefficient: state.kpiCoefficient.listKpiCoefficient
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getKpiCoefficient,
      updateKpiCoefficient
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KPICoefficient);
