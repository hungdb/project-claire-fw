import KpiLeader from './KpiLeader';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import { addNotification } from '../../../../../01-actions/notification';
import {
  createKpiSprint,
  getKpiSprint,
  getListMyProjects,
  getListProject,
  removeKpiSprint,
  updateKpiSprint
} from '../../../../../01-actions/kpiLeader';

const mapStateToProps = state => {
  return {
    listKpiSprint: state.kpiLeader.listKpiSprint,
    totalKpiSprint: state.kpiLeader.total,
    listProjects: state.kpiLeader.listProjects,
    projectOptions: state.kpiLeader.projectOptions,
    managerOptions: state.kpiLeader.managerOptions
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListProject,
      getListMyProjects,
      getKpiSprint,
      createKpiSprint,
      removeKpiSprint,
      updateKpiSprint,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KpiLeader);
