const styles = {
  multiCheck: {
    margin: 'auto'
  },
  styleTableCell: {
    textAlign: 'center'
  },
  viewSelector: {
    marginTop: '20px',
    marginBottom: '20px'
  },
  select: {
    width: 300,
  }
};

export default styles;
