import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo } from 'react';
import { array, func, any } from 'prop-types';
import { Col, Row } from 'reactstrap';
import { promiseDelayImport } from '../../../../../08-helpers/common';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const KpiLeaderFilter = ({
  projectOptions,
  managerOptions,
  handleChangeProject,
  handleChangeManager,
  project,
  manager
}) => {
  return (
    <Row className="mb-2">
      <Col xs={12} sm={6} lg={3} className="pt-1">
        <SelectPropsBox
          options={projectOptions}
          value={project && project.value}
          placeholder="select_project"
          onChange={handleChangeProject}
        />
      </Col>
      <Col xs={12} sm={6} lg={3} className="pt-1">
        <SelectPropsBox
          options={managerOptions}
          value={manager && manager.value}
          placeholder="select_manager"
          onChange={handleChangeManager}
        />
      </Col>
    </Row>
  );
};

KpiLeaderFilter.propTypes = {
  projectOptions: array,
  managerOptions: array,
  handleChangeProject: func,
  handleChangeManager: func,
  project: any,
  manager: any
};

KpiLeaderFilter.defaultProps = {
  projectOptions: [],
  managerOptions: []
};

export default memo(KpiLeaderFilter);
