import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useEffect, useState, memo } from 'react';
import { Button, Table } from 'reactstrap';
import { POINT_KPI_LEADER, ROWS_PER_PAGE_OPTIONS, TYPES_KPI_SPRINT } from '../../../../../05-utils/commonData';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, formatDateTime } from '../../../../../08-helpers/common';
import { array, func, number, any } from 'prop-types';

const TableHead = LazyComponent(() => import('../../../../../03-components/TableHead'));
const Pagination = LazyComponent(() => import('../../../../../03-components/Pagination'));
const KpiSprintDialog = LazyComponent(() => import('../../../../dialog/kpi-sprint/kpi-sprint-dialog'));

const type = TYPES_KPI_SPRINT.LEADER;

const fields = [
  'no',
  'kpi_leader_sprint.sprint',
  'kpi_leader_sprint.project',
  'kpi_leader_sprint.user',
  'kpi_leader_sprint.work_management',
  'kpi_leader_sprint.people_management',
  'kpi_leader_sprint.process',
  'kpi_leader_sprint.summary',
  'kpi_leader_sprint.created_at',
  'kpi_leader_sprint.action'
];

const handlePoint = (t, point) => {
  if (point === POINT_KPI_LEADER[0].value) {
    return t('kpi_leader_sprint.very_good');
  }
  if (point >= POINT_KPI_LEADER[1].value) {
    return t('kpi_leader_sprint.good');
  }
  if (point >= POINT_KPI_LEADER[2].value) {
    return t('kpi_leader_sprint.normal');
  }
  if (point >= POINT_KPI_LEADER[3].value) {
    return t('kpi_leader_sprint.bad');
  }
  return t('kpi_leader_sprint.very_bad');
};

const filterKpiSprints = (kpiSprints, project, manager) => {
  return kpiSprints.filter(item => {
    let isFilter = true;
    if (project) {
      isFilter = item.projectId === project.value;
    }
    if (manager && isFilter) {
      isFilter = calculateLength(item.managers) && item.managers.includes(manager.label);
    }
    return isFilter;
  });
};

const KpiLeaderTable = ({
  listKpiSprint,
  project,
  manager,
  listProjects,
  createKpiSprint,
  updateKpiSprint,
  removeKpiSprint,
  totalKpiSprint,
  getKpiSprint,
  getListProject
}) => {
  const [showDialogKpiLeader, setShowDialogKpiLeader] = useState(false);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(ROWS_PER_PAGE_OPTIONS[0]);
  const [itemKpiSprint, setItemKpiSprint] = useState({});
  const [isEdit, setIsEdit] = useState(false);

  const onCloseDialog = () => {
    setShowDialogKpiLeader(false);
    setIsEdit(false);
  };

  const renderDialogKpiLeader = t => {
    return (
      <KpiSprintDialog
        t={t}
        isOpen={showDialogKpiLeader}
        editMode={isEdit}
        itemKpiSprint={itemKpiSprint}
        listProject={listProjects}
        onClose={onCloseDialog}
        onCreateKPISprint={kpiCreation => {
          onCloseDialog();
          kpiCreation && createKpiSprint(kpiCreation);
        }}
        onUpdate={kpiUpdate => {
          onCloseDialog();
          kpiUpdate && updateKpiSprint(kpiUpdate);
        }}
        onRemove={kpiRemove => {
          onCloseDialog();
          kpiRemove && removeKpiSprint({ kpiSprintId: kpiRemove.id });
        }}
      />
    );
  };

  const renderBodyKpiLeader = t => {
    return (
      <tbody>
        {calculateLength(listKpiSprint) ? (
          filterKpiSprints(listKpiSprint, project, manager).map((item, index) => {
            if (item) {
              const sprintName = (item.sprint && item.sprint.name) || '';
              const projectName = (item.project && item.project.name) || '';
              const userName = (item.user && item.user.name) || '';
              const workManagementPoint = item.workManagementPoint || 0;
              const peopleManagementPoint = item.peopleManagementPoint || 0;
              const processPoint = item.processPoint || 0;
              const summaryPoint = item.point * 100 || 0;
              const createdAt = formatDateTime(item.createdAt, 'LLLL', 1);

              return (
                <tr key={item.id} style={{ textAlign: ' center' }}>
                  <td className="pr-2 pl-4"> {index + 1} </td>
                  <td className="px-2"> {sprintName} </td>
                  <td className="px-2"> {projectName} </td>
                  <td className="px-2"> {userName} </td>
                  <td className="px-2"> {handlePoint(t, workManagementPoint)} </td>
                  <td className="px-2"> {handlePoint(t, peopleManagementPoint)} </td>
                  <td className="px-2"> {handlePoint(t, processPoint)} </td>
                  <td className="px-2"> {handlePoint(t, summaryPoint)} </td>
                  <td className="px-2"> {createdAt} </td>
                  <td className="px-2">
                    <span
                      className={'cursor-pointer'}
                      onClick={() => {
                        setShowDialogKpiLeader(true);
                        setIsEdit(true);
                        setItemKpiSprint(item);
                      }}
                    >
                      <i className="fa fa-pencil color-base-btn" />
                    </span>
                  </td>
                </tr>
              );
            }
            return null;
          })
        ) : (
          <tr>
            <td colSpan={11} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  useEffect(() => {
    getKpiSprint({ type, page, limit });
    getListProject();
  }, []);

  return (
    <NamespacesConsumer>
      {t => (
        <div>
          <div className={'d-flex justify-content-end mb-2'}>
            <Button
              className={'min-width-80px min-height-35px orange-btn'}
              onClick={() => setShowDialogKpiLeader(true)}
              size={'md'}
            >
              <i className={'fa fa-plus'} />
              &nbsp;&nbsp;{t('add')}
            </Button>
          </div>
          <Table bordered responsive className="text-center">
            <TableHead fields={fields} t={t} />
            {renderBodyKpiLeader(t)}
          </Table>
          <Pagination
            totalItems={totalKpiSprint}
            pageSize={limit}
            value={limit}
            onSelect={page => {
              setPage(page);
              getKpiSprint({ type, page, limit });
            }}
            onChange={e => {
              const limit = e.value;
              setLimit(limit);
              getKpiSprint({ type, page, limit });
            }}
            rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
            isClearable={false}
          />
          {renderDialogKpiLeader(t)}
        </div>
      )}
    </NamespacesConsumer>
  );
};

KpiLeaderTable.propTypes = {
  listKpiSprint: array,
  project: any,
  manager: any,
  listProjects: array,
  createKpiSprint: func,
  updateKpiSprint: func,
  removeKpiSprint: func,
  totalKpiSprint: number,
  getKpiSprint: func,
  getListProject: func
};

export default memo(KpiLeaderTable);
