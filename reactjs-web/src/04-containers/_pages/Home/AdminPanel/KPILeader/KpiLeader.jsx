import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { useState, useEffect, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, ButtonGroup } from 'reactstrap';
import { array, func, number } from 'prop-types';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const KpiLeaderTable = LazyComponent(promiseDelayImport(import('./KpiLeaderTable')), <SpinLoader />);
const KpiLeaderFilter = LazyComponent(() => import('./KpiLeaderFilter'));

function KpiLeader({
  getKpiSprint,
  getListProject,
  createKpiSprint,
  removeKpiSprint,
  updateKpiSprint,
  listKpiSprint,
  listProjects,
  totalKpiSprint,
  projectOptions,
  managerOptions,
  addNotification
}) {
  const [project, setProject] = useState(null);
  const [manager, setManager] = useState(null);

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  const handleChangeProject = project => {
    setProject(project);
  };

  const handleChangeManager = manager => {
    setManager(manager);
  };

  const renderTabReviewLeader = t => {
    return (
      <div className="d-flex justify-content-between align-items-center tabs">
        <div className="d-flex align-items-center mb-3">
          <ButtonGroup className="button-group-tab">
            <Button active={true} className="tabs-btn">
              {t('kpi_leader_sprint.title')}
            </Button>
          </ButtonGroup>
        </div>
      </div>
    );
  };

  return (
    <NamespacesConsumer>
      {t => (
        <div className="page skill-management">
          {renderTabReviewLeader(t)}
          <KpiLeaderFilter
            managerOptions={managerOptions}
            projectOptions={projectOptions}
            manager={manager}
            project={project}
            handleChangeManager={handleChangeManager}
            handleChangeProject={handleChangeProject}
          />
          <KpiLeaderTable
            listKpiSprint={listKpiSprint}
            project={project}
            manager={manager}
            listProjects={listProjects}
            createKpiSprint={createKpiSprint}
            updateKpiSprint={updateKpiSprint}
            removeKpiSprint={removeKpiSprint}
            totalKpiSprint={totalKpiSprint}
            getKpiSprint={getKpiSprint}
            getListProject={getListProject}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
}

KpiLeader.propTypes = {
  getKpiSprint: func,
  getListProject: func,
  createKpiSprint: func,
  removeKpiSprint: func,
  updateKpiSprint: func,
  listKpiSprint: array,
  listProjects: array,
  totalKpiSprint: number,
  projectOptions: array,
  managerOptions: array,
  addNotification: func
};

export default memo(KpiLeader);
