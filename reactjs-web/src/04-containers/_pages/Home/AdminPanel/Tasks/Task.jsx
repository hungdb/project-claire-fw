import './style.css';
import Skeleton from 'react-loading-skeleton';
import SpinLoader from '../../../../../03-components/SpinLoader';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useState, useEffect, memo } from 'react';
import { array, object, func } from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { COMPARISON_ESTIMATE_OPTIONS } from '../../../../../05-utils/commonData';
import { PERM_LIST_TASK_OF_ALL_USERS, PERM_LIST_PROJECTS } from '../../../../../07-constants/app-permission';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import { debounce } from 'throttle-debounce';
import {
  formatDateTime,
  calculateLength,
  isAllowPermission,
  promiseDelayImport
} from '../../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const TaskTable = LazyComponent(promiseDelayImport(import('./TaskTable'), 2000), <SpinLoader />);

const ControlDatePicker = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/DateTimePicker')),
  <Skeleton height={38} />
);

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const FieldInput = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/FieldInput')),
  <Skeleton height={38} />
);
const AppText = LazyComponent(() => import('../../../../../03-components/AppText'));

function Tasks({ taskOfUsers, appPermissions, _getTaskOfUsers, addNotification, getProjects, projects }) {
  const [username, setUsername] = useState('');
  const [projectId, setProjectId] = useState('');
  const [selectedEstimate, setSelectedEstimate] = useState(COMPARISON_ESTIMATE_OPTIONS[0]);
  const [date, setDate] = useState(new Date());

  const getParams = (date, projectId = null) => ({
    typeComparison: null,
    estimate: null,
    date: date ? formatDateTime(date, 'DD/MM/YYYY', 1) : null,
    name: null,
    projectId
  });

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_TASK_OF_ALL_USERS)) {
      _getTaskOfUsers(getParams(new Date()));
    }

    if (isAllowPermission(appPermissions, PERM_LIST_PROJECTS)) {
      getProjects();
    }
  };

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const onChangeDate = date => {
    setDate(date);
    _getTaskOfUsers(getParams(date));
  };

  const onSearchName = debounce(500, value => setUsername(value));

  const filterData = () => {
    let allUsersByTask = [];
    if (calculateLength(taskOfUsers)) {
      allUsersByTask = taskOfUsers.map(item => {
        const totalEstimateTime =
          calculateLength(item.tasks) && item.tasks.reduce((acc, { estimate }) => acc + estimate, 0);
        return { ...item, totalEstimateTime: totalEstimateTime || 0 };
      });
    }
    allUsersByTask = allUsersByTask.filter(item => {
      let isFilter = true;
      if (username) {
        isFilter = item.name.includes(username);
      }
      if (isFilter && selectedEstimate && selectedEstimate.value) {
        const estimateValue = selectedEstimate.value;
        if (estimateValue === 1) {
          isFilter = item.totalEstimateTime < 8;
        } else if (estimateValue === 2) {
          isFilter = item.totalEstimateTime === 8;
        } else if (estimateValue === 3) {
          isFilter = item.totalEstimateTime > 8;
        }
      }
      return isFilter;
    });
    return allUsersByTask;
  };

  const allUsersByTask = filterData();

  const onSelectProject = e => {
    const projectId = e && e.value;
    setProjectId(projectId);
    _getTaskOfUsers(getParams(date, projectId));
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Container fluid className="px-md-5 py-4 py-md-5">
          <div className={'d-flex margin-bottom-30px'}>
            <AppText type={'title-header'}>{t('admin_panel_child.user_task_summary')}</AppText>
          </div>
          <Row className="mb-4">
            <Col xs={12} sm={6} lg={3} className="pr-3 pt-1">
              <SelectPropsBox
                isClearable={true}
                options={COMPARISON_ESTIMATE_OPTIONS}
                value={selectedEstimate && selectedEstimate.value}
                placeholder="select_comparison_estimate"
                onChange={value => setSelectedEstimate(value)}
              />
            </Col>
            <Col xs={12} sm={6} lg={3} className="pt-1">
              <ControlDatePicker
                className={'date-picker'}
                value={date}
                format="DD/MM/YYYY"
                isLocalFormat={1}
                placeholder={'select_date'}
                onChange={e => onChangeDate(e)}
              />
            </Col>
            <Col xs={12} sm={6} lg={3} className="pt-1">
              <FieldInput
                type={'text'}
                onChangeValue={onSearchName}
                defaultValue={username ? username : undefined}
                placeholder={'search_by_username'}
                isDebounce={true}
              />
            </Col>
            <Col xs={12} sm={6} lg={3} className="pt-1">
              <SelectPropsBox
                isClearable={true}
                options={projects}
                keyLabel="name"
                keyValue="id"
                value={projectId}
                placeholder="select_project"
                onChange={onSelectProject}
              />
            </Col>
          </Row>
          <div>
            <strong>
              {t('total')}: {calculateLength(allUsersByTask)}
            </strong>
          </div>
          <TaskTable allUsersByTask={allUsersByTask} />
        </Container>
      )}
    </NamespacesConsumer>
  );
}

Tasks.propTypes = {
  match: object,
  history: object,
  taskOfUsers: array,
  appPermissions: array,
  projects: array,
  _getTaskOfUsers: func,
  getProjects: func,
  addNotification: func
};

export default memo(Tasks);
