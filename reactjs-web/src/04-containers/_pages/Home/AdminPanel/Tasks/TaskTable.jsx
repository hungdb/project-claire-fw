import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { Fragment, memo } from 'react';
import { calculateLength, formatDateTime } from '../../../../../08-helpers/common';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { array } from 'prop-types';

const TableHead = LazyComponent(() => import('../../../../../03-components/TableHead'));

const fields = ['no', 'username', 'total_estimate', 'task', 'status', 'start_date', 'end_date'];
const classNames = ['w-5 text-center', '', 'w-5 text-center', '', '', '', ''];

const TaskTable = ({ allUsersByTask }) => {
  const totalEstimates = tasks => {
    return tasks.reduce((total, item) => total + (item.estimate ? item.estimate : 0), 0);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Table bordered responsive hover className={'admin-task-table'}>
          <thead>
            <tr>
              <TableHead fields={fields} t={t} classes={classNames} />
            </tr>
          </thead>
          <tbody>
            {calculateLength(allUsersByTask) ? (
              allUsersByTask.map((user, indexUser) => {
                if (calculateLength(user.tasks)) {
                  return user.tasks.map((task, indexTask) => (
                    <tr key={task.id}>
                      {indexTask === 0 ? (
                        <Fragment>
                          <td className="align-middle text-center" rowSpan={calculateLength(user.tasks) || 1}>
                            {indexUser + 1}
                          </td>
                          <td className="align-middle" rowSpan={calculateLength(user.tasks) || 1}>
                            {user.name ? user.name : user.id}
                          </td>
                          <td className="align-middle text-center" rowSpan={calculateLength(user.tasks) || 1}>
                            {totalEstimates(user.tasks)}
                          </td>
                          <td>
                            {`[${task.Project.name}] [${task.Sprint.name}] ${task.taskCode ? task.taskCode : task.id} ${
                              task.content
                            }`}
                          </td>
                          <td className="text-uppercase">{task.WorkflowState && task.WorkflowState.name}</td>
                          <td>{formatDateTime(task.startDate, 'DD/MM/YYYY', 1)}</td>
                          <td>{formatDateTime(task.endDate, 'DD/MM/YYYY', 1)}</td>
                        </Fragment>
                      ) : (
                        <Fragment>
                          <td>{`[${task.Project.name}] [${task.Sprint.name}] ${
                            task.taskCode ? task.taskCode : task.id
                          } ${task.content}`}</td>
                          <td className="text-uppercase">{task.WorkflowState && task.WorkflowState.name}</td>
                          <td>{formatDateTime(task.startDate, 'DD/MM/YYYY', 1)}</td>
                          <td>{formatDateTime(task.endDate, 'DD/MM/YYYY', 1)}</td>
                        </Fragment>
                      )}
                    </tr>
                  ));
                }
                return (
                  <tr key={user.id}>
                    <td className="align-middle text-center">{indexUser + 1}</td>
                    <td className="align-middle">{user.name ? user.name : user.id}</td>
                    <td className="align-middle text-center">{0}</td>
                    <td className={'text-center font-size-15 text-muted'}>{t('free_time')}</td>
                    <td />
                    <td />
                    <td />
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan={7} className={'text-center font-size-15 text-muted'}>
                  {t('not_found')}
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      )}
    </NamespacesConsumer>
  );
};

TaskTable.propTypes = {
  allUsersByTask: array
};

export default memo(TaskTable);
