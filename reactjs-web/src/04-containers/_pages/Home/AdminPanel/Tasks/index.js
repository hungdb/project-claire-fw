import Tasks from './Task';
import { getTaskOfUsers as _getTaskOfUsers } from '../../../../../01-actions/task';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addNotification } from '../../../../../01-actions/notification';
import { getProjects } from '../../../../../01-actions/project';

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  taskOfUsers: store.task.taskOfUsers,
  projects: store.project.projects
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _getTaskOfUsers,
      addNotification,
      getProjects
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Tasks)
);
