import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useState } from 'react';
import { bool, number, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { createUserNote } from '../../../../../01-actions/user-note';

const FieldInput = LazyComponent(() => import('../../../../../03-components/FieldInput'));

const ModalAddNote = ({ isOpen, userId, closeModal, createUserNote }) => {
  const [content, setContent] = useState('');

  const handleContent = value => setContent(value);

  const submitCreateUserNote = () => {
    createUserNote(userId, content);
    closeModal();
  };

  return (
    <NamespacesConsumer>
      {t => (
        <Modal isOpen={isOpen} centered toggle={closeModal} fade={true} backdrop="static">
          <ModalHeader toggle={closeModal}>{t('add_note')}</ModalHeader>
          <ModalBody>
            <FieldInput type={'textarea'} onChangeValue={handleContent} defaultValue={content ? content : undefined} />
          </ModalBody>
          <ModalFooter>
            <Button className={'outline-base-btn'} size={'sm'} onClick={closeModal}>
              {t('cancel')}
            </Button>
            <Button className={'base-btn'} size={'sm'} onClick={submitCreateUserNote} disabled={!content}>
              {t('add')}
            </Button>{' '}
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

ModalAddNote.propTypes = {
  isOpen: bool,
  userId: number,
  closeModal: func,
  createUserNote: func
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createUserNote
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(memo(ModalAddNote));
