import './style.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useState, useEffect } from 'react';
import { bool, number, array, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NamespacesConsumer } from 'react-i18next';
import { Timeline, TimelineEvent } from 'react-event-timeline';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { formatDateTime } from '../../../../../08-helpers/common';
import { getUserNotes, updateUserNote, deleteUserNote } from '../../../../../01-actions/user-note';

const FieldInput = LazyComponent(() => import('../../../../../03-components/FieldInput'));
const ConfirmationModal = LazyComponent(() => import('../../../../../03-components/ConfirmationModal'));

const ModalListNote = ({ isOpen, userId, userNotes, closeModal, getUserNotes, updateUserNote, deleteUserNote }) => {
  const [state, setState] = useState({
    isConfirm: false,
    noteId: null,
    txtContent: ''
  });

  const { isConfirm, noteId, txtContent } = state;

  useEffect(() => {
    getUserNotes(userId);
  }, [userId]);

  return (
    <NamespacesConsumer>
      {t => (
        <Modal
          isOpen={isOpen}
          centered
          size="xl"
          toggle={closeModal}
          fade={true}
          backdrop="static"
          className={'model-list-note'}
        >
          <ModalHeader toggle={closeModal}>{t('list_note')}</ModalHeader>
          <ModalBody>
            <Timeline>
              {userNotes.map(n => (
                <div key={n.id} className={'note-wrap'}>
                  <TimelineEvent
                    title=""
                    createdAt={formatDateTime(n.createdAt, 'LLLL', 1)}
                    icon={<i className="fa fa-commenting-o" aria-hidden="true" />}
                    iconColor="#6fba1c"
                    width="100"
                  >
                    {n.id === noteId ? (
                      <div>
                        <FieldInput
                          type={'text'}
                          onChangeValue={value => setState({ ...state, txtContent: value })}
                          defaultValue={txtContent ? txtContent : undefined}
                        />
                        <div className="d-flex justify-content-end my-2">
                          <Button
                            className="outline-base-btn mr-1"
                            size="sm"
                            onClick={() => setState({ ...state, noteId: null, txtContent: '' })}
                          >
                            {t('cancel')}
                          </Button>
                          <Button
                            className="base-btn"
                            size="sm"
                            onClick={() => {
                              updateUserNote(n.id, txtContent);
                              setState({ ...state, noteId: null, txtContent: '' });
                            }}
                          >
                            {t('change')}
                          </Button>
                        </div>
                      </div>
                    ) : (
                      n.content
                    )}
                  </TimelineEvent>
                  <div className="user-note-action action">
                    <Badge
                      color="success"
                      className={'mr-1 cursor-pointer'}
                      onClick={() => {
                        setState({ ...state, noteId: n.id, txtContent: n.content });
                      }}
                    >
                      {t('edit')}
                    </Badge>
                    <Badge
                      color="danger"
                      className={'cursor-pointer'}
                      onClick={() => {
                        setState({ ...state, isConfirm: true, noteId: n.id, txtContent: n.content });
                      }}
                    >
                      {t('delete')}
                    </Badge>
                  </div>
                </div>
              ))}
            </Timeline>
            {
              <ConfirmationModal
                isOpen={isConfirm}
                txtTitle="confirm_delete"
                funConfirm={() => deleteUserNote(noteId)}
                funClose={() => setState({ ...state, isConfirm: false, noteId: null })}
              >
                {t('are_you_sure_delete')}
              </ConfirmationModal>
            }
          </ModalBody>
          <ModalFooter>
            <Button className={'outline-base-btn'} size={'sm'} onClick={closeModal}>
              {t('close')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

ModalListNote.propTypes = {
  isOpen: bool,
  userId: number,
  userNotes: array,
  closeModal: func,
  getUserNotes: func,
  updateUserNote: func,
  deleteUserNote: func
};

const mapStateToProps = state => ({
  userNotes: state.userNote.userNotes
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserNotes,
      updateUserNote,
      deleteUserNote
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(ModalListNote));
