import './style.css';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader/index';
import React, { memo, useState, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission, promiseDelayImport, calculateLength } from '../../../../../08-helpers/common';
import { array, number, func, object, string } from 'prop-types';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import { PERM_LIST_USERS } from '../../../../../07-constants/app-permission';
import { USERS_ALL_TABS_TYPE, TYPE_LIST_USER } from '../../../../../05-utils/commonData';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const TabsPlaceHolder = LazyComponent(() => import('../../../../../03-components/PlaceHolderComponent/Tab'));

const RolesAndPermissions = LazyComponent(() => import('./RoleAndPermission/'));

const ListUser = LazyComponent(promiseDelayImport(import('./ListUser')), <SpinLoader />);
const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton height={28} />
);
const arrayTabWidth = [88, 168, 206, 169];
const TabsController = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/TabsController')),
  <TabsPlaceHolder height={33} widthPerItem={arrayTabWidth} />
);
const UserFilter = LazyComponent(() => import('./UserFilter'));

const User = ({
  indentityTotal,
  users,
  currentUsers,
  getUsers,
  getRoles,
  addUser,
  addNotification,
  getListAdmin,
  updateUser,
  // roles,
  listRoles,
  // history,
  appPermissions,
  branches,
  thisUser
}) => {
  const [state, setState] = useState({
    allUsers: [],
    tabsStatus: 1,
    totalUsers: 0,
    allUsersByTab: []
  });

  const { allUsers, tabsStatus, totalUsers, allUsersByTab } = state;

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_LIST_USERS)) {
      getUsers(TYPE_LIST_USER.ALL);
      getRoles();
      getListAdmin();
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    checkRoleAndLoad();
  }, [appPermissions]);

  const notCustomerRole = roles =>
    calculateLength(roles) ? roles.some(r => r.Role && r.Role.name.toLowerCase() !== 'customer') : false;

  const getUsersInProjects = () => {
    return calculateLength(currentUsers)
      ? currentUsers
          .map(item => item.isActive && users.find(user => user.id === item.id))
          .filter(u => u && notCustomerRole(u.roles))
          .filter(Boolean)
      : [];
  };

  const getUsersOutOfSystem = () => {
    return calculateLength(users) ? users.filter(user => !calculateLength(user.roles)).filter(Boolean) : [];
  };

  const getUsersInActive = () => {
    const inactiveUsers = currentUsers.filter(user => !user.isActive);
    return calculateLength(inactiveUsers)
      ? inactiveUsers.map(item => users.find(user => user.id === item.id)).filter(Boolean)
      : [];
  };

  const onCountTotalUser = () => {
    const usersInProjects = getUsersInProjects();
    const usersOutOfSystem = getUsersOutOfSystem();
    const usersInActive = getUsersInActive();

    switch (tabsStatus) {
      case USERS_ALL_TABS_TYPE[0].value:
        setState({
          ...state,
          totalUsers: calculateLength(currentUsers),
          allUsers: users,
          allUsersByTab: users
        });
        break;
      case USERS_ALL_TABS_TYPE[1].value:
        setState({
          ...state,
          totalUsers: calculateLength(usersInProjects),
          allUsers: usersInProjects,
          allUsersByTab: usersInProjects
        });
        break;
      case USERS_ALL_TABS_TYPE[2].value:
        setState({
          ...state,
          totalUsers: calculateLength(usersOutOfSystem),
          allUsers: usersOutOfSystem,
          allUsersByTab: usersOutOfSystem
        });
        break;
      case USERS_ALL_TABS_TYPE[3].value:
        setState({
          ...state,
          totalUsers: calculateLength(usersInActive),
          allUsers: usersInActive,
          allUsersByTab: usersInActive
        });
        break;
      default:
        return null;
    }
  };

  useEffect(() => {
    onCountTotalUser();
  }, [tabsStatus, users, currentUsers]);

  const usersInProjects = getUsersInProjects();
  const usersOutOfSystem = getUsersOutOfSystem();
  const usersInActive = getUsersInActive();
  USERS_ALL_TABS_TYPE[0].total = calculateLength(users);
  USERS_ALL_TABS_TYPE[1].total = calculateLength(usersInProjects);
  USERS_ALL_TABS_TYPE[2].total = calculateLength(usersOutOfSystem);
  USERS_ALL_TABS_TYPE[3].total = calculateLength(usersInActive);

  const onFilterUsers = allUsers => setState({ ...state, allUsers });
  const onChangeTab = tabsStatus => setState({ ...state, tabsStatus });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page container-admin-page">
          <RolesAndPermissions />
          <div className={'d-flex margin-bottom-30px'}>
            <AppText type={'title-header'}>{t('user_management')}</AppText>
          </div>
          <div className="d-flex justify-content-end mb-4">
            <TabsController allTabs={USERS_ALL_TABS_TYPE} onChangeTabs={onChangeTab} />
          </div>
          <Row className="mb-4">
            <Col xs="12" md="12" xl="2" className="d-flex align-items-center mb-2-md count-user">
              <strong>{totalUsers}</strong>&nbsp;{t('users')}/<strong>{indentityTotal}</strong>
              &nbsp;{t('identity')}
            </Col>
            <Col sm="12" xl="10" className="mb-2-xs">
              <UserFilter
                branches={branches}
                allUsersByTab={allUsersByTab}
                users={users}
                listRoles={listRoles}
                onFilterUsers={onFilterUsers}
              />
            </Col>
          </Row>
          <Row>
            <Col xs="12">
              <ListUser
                tabsStatus={tabsStatus}
                thisUser={thisUser}
                listRoles={listRoles}
                currentUsers={currentUsers}
                updateUser={updateUser}
                addUser={addUser}
                allUsers={allUsers}
              />
            </Col>
          </Row>
        </div>
      )}
    </NamespacesConsumer>
  );
};

User.propTypes = {
  indentityTotal: number,
  userTotal: number,
  adminStatus: string,
  users: array,
  currentUsers: array,
  getUsers: func,
  getRoles: func,
  addUser: func,
  addNotification: func,
  getListAdmin: func,
  updateUser: func,
  // roles: array,
  listRoles: array,
  history: object,
  appPermissions: array,
  branches: array,
  thisUser: object
};

export default memo(User);
