import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { useState, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Table } from 'reactstrap';
import { isAllowPermission, calculateLength } from '../../../../../../08-helpers/common';
import { func, array, object } from 'prop-types';
import {
  PERM_CREATE_ROLE_PERMISSION,
  PERM_REMOVE_ROLE_PERMISSION,
  PERM_UPDATE_ROLE
} from '../../../../../../07-constants/app-permission';

const TableHead = LazyComponent(() => import('../../../../../../03-components/TableHead'));
const ConfirmationModal = LazyComponent(() => import('../../../../../../03-components/ConfirmationModal'));
const PermissionDialog = LazyComponent(() =>
  import('../../../../../dialog/permission-scheme-detail/permission-scheme-detail')
);

const rolePermissionColumn = ['no', 'permission', 'conditions', 'action'];

const DataTable = ({
  removeRolePermission,
  addRolePermission,
  allPermissions,
  appPermissions,
  roleDetail,
  currentRolePermissions,
  onEditRole
}) => {
  const [state, setState] = useState({
    isPermissionDialogOpen: false,
    permissionForAction: null,
    isConfirmRemoveRole: false,
    isConfirmRemovePermissionDialogOpen: false
  });

  const { isPermissionDialogOpen, permissionForAction, isConfirmRemovePermissionDialogOpen } = state;

  const isProjectRelated = permissionName => {
    const permission = allPermissions && allPermissions.find(p => p.permissionName === permissionName);
    return permission && permission.projectRelated;
  };

  const onRemovePermission = () => {
    removeRolePermission({
      rolePermissionId: permissionForAction.id
    });
  };

  const onClosePermissionConfirmDialog = () => {
    setState({
      ...state,
      permissionForAction: null,
      isConfirmRemovePermissionDialogOpen: false
    });
  };

  const onClosePermissionDialog = () => {
    setState({
      ...state,
      isPermissionDialogOpen: false
    });
  };

  const onOkPermissionDialog = ({ permission, permissionConditions }) => {
    //-- add new permission for this role
    setState({ ...state, isPermissionDialogOpen: false });
    addRolePermission({
      roleId: roleDetail.id,
      permission,
      permissionConditions: JSON.stringify(permissionConditions)
    });
  };

  const onOpenPermissionDialog = () => {
    setState({ ...state, isPermissionDialogOpen: true });
  };

  const onSelectPermissionToRemove = p => {
    setState({
      ...state,
      permissionForAction: p,
      isConfirmRemovePermissionDialogOpen: true
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          {!roleDetail ? (
            <div className={'margin-top-15px text-muted text-center text-italic'}>
              {t('select_role_to_display_permission')}
            </div>
          ) : (
            <div className={'d-flex flex-column margin-top-15px'}>
              <ConfirmationModal
                isOpen={isConfirmRemovePermissionDialogOpen}
                funConfirm={onRemovePermission}
                funClose={onClosePermissionConfirmDialog}
              >
                {t('confirm_remove_permission')}
              </ConfirmationModal>
              <PermissionDialog
                isOpen={isPermissionDialogOpen}
                permissions={allPermissions}
                onClose={onClosePermissionDialog}
                onOK={onOkPermissionDialog}
              />
              {roleDetail.isSystemRole && (
                <div className={'d-flex align-items-center'}>
                  <i className={'fa fa-info-circle margin-right-5px'} />
                  <span>{t('system_role_explain')}</span>
                </div>
              )}
              {!roleDetail.isEditable && (
                <div className={'d-flex align-items-center'}>
                  <i className={'fa fa-info-circle margin-right-5px'} /> <span>{t('readonly_permission_explain')}</span>
                </div>
              )}
              <div className="d-flex justify-content-end">
                {isAllowPermission(appPermissions, PERM_UPDATE_ROLE) &&
                  !roleDetail.isSystemRole &&
                  !roleDetail.isSuperUser && (
                    <Button className={'orange-btn margin-right-5px'} onClick={onEditRole} size={'md'}>
                      <i className={'fa fa-pencil margin-right-5px'} />
                      {t('edit_role')}
                    </Button>
                  )}
                {isAllowPermission(appPermissions, PERM_CREATE_ROLE_PERMISSION) && roleDetail.isEditable && (
                  <Button className={'orange-btn'} onClick={onOpenPermissionDialog} size={'md'}>
                    <i className={'fa fa-plus margin-right-5px'} />
                    {t('add_role_permission')}
                  </Button>
                )}
              </div>
              <Table hover responsive striped className={'default-paper table-no-border font-size-13 mt-2'}>
                <thead>
                  <tr>
                    <TableHead fields={rolePermissionColumn} t={t} />
                  </tr>
                </thead>
                <tbody>
                  {calculateLength(currentRolePermissions)
                    ? currentRolePermissions.map((p, index) => (
                        <tr key={index}>
                          <td className={'text-center align-middle'}>{index + 1}</td>
                          <td className={'text-center align-middle'}>
                            {p.permission + (isProjectRelated(p.permission) ? ' *' : '')}
                          </td>
                          <td className={'text-center align-middle'}>{p.permissionCondition}</td>
                          <td className={'text-center align-middle'}>
                            {roleDetail.isEditable ? (
                              <>
                                {isAllowPermission(appPermissions, PERM_REMOVE_ROLE_PERMISSION) && (
                                  <span
                                    className={'cursor-pointer font-size-18 margin-right-5px'}
                                    title={t('add_note')}
                                    onClick={() => onSelectPermissionToRemove(p)}
                                  >
                                    <i className="fa fa-trash text-danger" />
                                  </span>
                                )}
                              </>
                            ) : (
                              <div>{t('read_only')}</div>
                            )}
                          </td>
                        </tr>
                      ))
                    : null}
                </tbody>
              </Table>
              <div className={'margin-top-15px text-muted text-italic font-size-12'}>
                * {t('project_depend_explain')}
              </div>
            </div>
          )}
        </>
      )}
    </NamespacesConsumer>
  );
};

DataTable.propTypes = {
  addRolePermission: func,
  removeRolePermission: func,
  onEditRole: func,
  removeRole: func,
  allPermissions: array,
  appPermissions: array,
  currentRolePermissions: array,
  roleDetail: object
};

export default memo(DataTable);
