import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { useState, useEffect, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Row, Col } from 'reactstrap';
import { isAllowPermission, promiseDelayImport } from '../../../../../../08-helpers/common';
import { PERM_CREATE_ROLE } from '../../../../../../07-constants/app-permission';
import { func, array, string } from 'prop-types';
import { REMOVE_ROLE_SUCCESS } from '../../../../../../07-constants/permission';
import { getEnv } from '../../../../../../env';

const ConfirmationModal = LazyComponent(() => import('../../../../../../03-components/ConfirmationModal'));
const UserRoleDialog = LazyComponent(() => import('../../../../../dialog/add-user-role/UserRoleDialog'));
const AppText = LazyComponent(() => import('../../../../../../03-components/AppText'));
const DataTable = LazyComponent(() => import('./DataTable'));
const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);
const LINK_FORMAT_FILE_PERMISSION = getEnv('LINK_FORMAT_FILE_PERMISSION');

const RolesAndPermissions = ({
  listAllPermissions,
  listPermissions,
  removeRolePermission,
  removeRole,
  addRolePermission,
  updateRole,
  createRole,
  allRoles,
  allPermissions,
  appPermissions,
  currentRolePermissions,
  status
}) => {
  const [state, setState] = useState({
    selectedRole: null,
    isEditRoleDialogOpen: false,
    roleDetail: null,
    isConfirmRemoveRole: false
  });

  const { selectedRole, isEditRoleDialogOpen, roleDetail, isConfirmRemoveRole } = state;

  useEffect(() => {
    listAllPermissions();
  }, []);

  useEffect(() => {
    if (status === REMOVE_ROLE_SUCCESS) setState({ ...state, roleDetail: null });
  }, [status]);

  const handleChangeRole = e => {
    const roleDetail = allRoles.find(r => r.id === (e && e.value));
    setState({
      ...state,
      selectedRole: e && e.value,
      roleDetail
    });
    if (e) {
      listPermissions({ roleId: e.value });
    }
  };

  const onAddUserRole = () => {
    setState({
      ...state,
      isEditRoleDialogOpen: true,
      roleDetail: null,
      selectedRole: null
    });
  };

  const onAddNewRole = () => {
    setState({
      ...state,
      isEditRoleDialogOpen: false
    });
  };

  const onOkRoleDialog = roleName => {
    if (roleDetail) {
      //-- edit mode
      if (!roleName) {
        //-- remove the role
        setState({
          ...state,
          isConfirmRemoveRole: true,
          isEditRoleDialogOpen: false
        });
      } else {
        updateRole({
          id: roleDetail.id,
          name: roleName
        });
        setState({
          ...state,
          isEditRoleDialogOpen: false,
          roleDetail: {
            ...state.roleDetail,
            name: roleName
          }
        });
      }
    } else {
      createRole({ name: roleName });
      setState({ ...state, isEditRoleDialogOpen: false });
    }
  };

  const onEditRole = () => {
    setState({
      ...state,
      isEditRoleDialogOpen: true
    });
  };

  const onConfirm = () => {
    removeRole({ id: roleDetail.id });
    setState({
      ...state,
      isConfirmRemoveRole: false
    });
  };

  const onCancelModal = () => {
    setState({
      ...state,
      isConfirmRemoveRole: false
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="margin-bottom-30px">
          <Row>
            <Col sm="3" md="4">
              <AppText type={'title-header'}>{t('roles_and_permissions')}</AppText>
            </Col>
            {isAllowPermission(appPermissions, PERM_CREATE_ROLE) && (
              <Col xs="12" md="8" className="d-flex add-role-btn">
                <Button className="orange-btn" onClick={onAddUserRole} size="md">
                  <i className="fa fa-plus margin-right-5px" /> {t('add_user_role')}
                </Button>
              </Col>
            )}
          </Row>
          <Row className={'margin-top-15px'}>
            <Col xs="12" md="4">
              <SelectPropsBox
                isSearchable
                keyValue="id"
                keyLabel="name"
                options={allRoles}
                value={selectedRole}
                onChange={handleChangeRole}
              />
            </Col>
          </Row>
          <UserRoleDialog
            isOpen={isEditRoleDialogOpen}
            allRoles={allRoles}
            role={roleDetail}
            onClose={onAddNewRole}
            onOK={onOkRoleDialog}
          />
          <ConfirmationModal isOpen={isConfirmRemoveRole} funConfirm={onConfirm} funClose={onCancelModal}>
            {t('confirm_remove_role')}
          </ConfirmationModal>
          <div
            className={'font-size-15px margin-bottom-15px margin-top-15px cursor-pointer'}
            style={{ color: '#0067B1', textDecoration: 'underline' }}
            onClick={() => {
              const win = window.open(LINK_FORMAT_FILE_PERMISSION, '_blank');
              win.focus();
            }}
          >
            <i className={'fa fa-external-link'} /> {t('format_file_permission')}
          </div>
          <DataTable
            removeRole={removeRole}
            roleDetail={roleDetail}
            onEditRole={onEditRole}
            allPermissions={allPermissions}
            appPermissions={appPermissions}
            addRolePermission={addRolePermission}
            removeRolePermission={removeRolePermission}
            currentRolePermissions={currentRolePermissions}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

RolesAndPermissions.propTypes = {
  listAllPermissions: func,
  listPermissions: func,
  removeRolePermission: func,
  removeRole: func,
  addRolePermission: func,
  updateRole: func,
  createRole: func,
  allRoles: array,
  allPermissions: array,
  appPermissions: array,
  currentRolePermissions: array,
  status: string
};

export default memo(RolesAndPermissions);
