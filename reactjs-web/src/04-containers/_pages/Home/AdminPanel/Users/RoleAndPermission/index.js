import RolesAndPermissions from './RolesAndPermissions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  addRolePermission,
  createRole,
  listAllPermissions,
  listPermissions,
  removeRole,
  removeRolePermission,
  updateRole
} from '../../../../../../01-actions/permission';

const mapStateToProps = store => {
  return {
    appPermissions: store.auth.permissions,
    allPermissions: store.permission.allPerms,
    status: store.permission.status,
    currentRolePermissions: store.permission.listPerms,
    allRoles: store.permission.listRoles
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listPermissions,
      listAllPermissions,
      addRolePermission,
      removeRolePermission,
      createRole,
      updateRole,
      removeRole
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RolesAndPermissions);
