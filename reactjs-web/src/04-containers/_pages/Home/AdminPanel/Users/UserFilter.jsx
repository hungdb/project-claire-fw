import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useMemo, useState, useEffect } from 'react';
import { array, func } from 'prop-types';
import { Col, Row } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, promiseDelayImport, getAllMentorsOption } from '../../../../../08-helpers/common';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const FieldInput = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/FieldInput')),
  <Skeleton height={38} />
);

const UserFilter = ({ listRoles, users, allUsersByTab, branches, onFilterUsers }) => {
  const [state, setState] = useState({
    role: '',
    mentor: '',
    branch: '',
    userName: ''
  });

  const { role, mentor, branch, userName } = state;

  const onFilteredUsers = allUsersByTab => {
    return calculateLength(allUsersByTab)
      ? allUsersByTab.filter(u => {
          if (!u) return false;
          let isFilterSearch = u.username.includes(userName);
          if (isFilterSearch && calculateLength(role) > 0) {
            isFilterSearch = u.roles.some(r => role.some(item => item.value === r.roleId));
          }
          if (isFilterSearch && mentor) {
            isFilterSearch = u.infoMentor && u.infoMentor.id === mentor;
          }
          if (isFilterSearch && branch) {
            isFilterSearch = u.branch && u.branch.id === branch;
          }
          return isFilterSearch;
        })
      : [];
  };

  const userFilteredMemo = useMemo(() => onFilteredUsers(allUsersByTab), [role, mentor, branch, userName]);
  const rolesOptionMemo = useMemo(
    () => listRoles.filter(r => !r.isSystemRole).map(r => ({ value: r.id, label: r.name })),
    [listRoles]
  );
  const mentorOptionMemo = useMemo(() => getAllMentorsOption(users), [users]);

  useEffect(() => {
    onFilterUsers(userFilteredMemo);
  }, [role, mentor, branch, userName, users]);

  const onSelectRole = role => setState({ ...state, role });
  const onSelectMentor = e => setState({ ...state, mentor: e && e.value });
  const onSelectBranch = e => setState({ ...state, branch: e && e.value });
  const onChangeUserName = userName => setState({ ...state, userName });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Row className="align-items-center">
          <Col sm="12" md="6" xl="3" className="mb-2-md">
            <SelectPropsBox
              isMulti
              isAnimatate
              value={role}
              closeMenuOnSelect={false}
              placeholder={t('select')}
              options={rolesOptionMemo}
              onChange={onSelectRole}
            />
          </Col>
          <Col sm="12" md="6" xl="3" className="mb-2-md">
            <SelectPropsBox
              value={mentor}
              closeMenuOnSelect={false}
              onChange={onSelectMentor}
              options={mentorOptionMemo}
              placeholder={t('select_mentor')}
            />
          </Col>
          <Col sm="12" md="6" xl="3" className="mb-2-md">
            <SelectPropsBox
              isClearable
              value={branch}
              options={branches}
              onChange={onSelectBranch}
              placeholder={t('select_branch')}
            />
          </Col>
          <Col sm="12" md="6" xl="3" className="mb-2-md">
            <FieldInput
              name="userName"
              defaultValue={userName}
              onChangeValue={onChangeUserName}
              placeholder={t('Search by username')}
            />
          </Col>
        </Row>
      )}
    </NamespacesConsumer>
  );
};

UserFilter.propTypes = {
  listRoles: array,
  users: array,
  allUsersByTab: array,
  branches: array,
  onFilterUsers: func
};

export default memo(UserFilter);
