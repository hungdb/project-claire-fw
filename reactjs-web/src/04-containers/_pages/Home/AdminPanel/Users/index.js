import User from './User';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addNotification } from '../../../../../01-actions/notification';
import { addUser, getListAdmin, getUsers, updateUser, updateUserMentor } from '../../../../../01-actions/adminproject';
import { addRolePermission } from '../../../../../01-actions/permission';
import { listAllPermissions } from '../../../../../01-actions/permission';
import { listPermissions } from '../../../../../01-actions/permission';
import { removeRolePermission } from '../../../../../01-actions/permission';
import { createRole } from '../../../../../01-actions/permission';
import { updateRole } from '../../../../../01-actions/permission';
import { removeRole } from '../../../../../01-actions/permission';
import { getRoles } from '../../../../../01-actions/permission';

const mapStateToProps = store => {
  return {
    // auth
    thisUser: store.auth.user,
    appPermissions: store.auth.permissions,
    roleStatus: store.auth.status,
    // roles: store.auth.roles,
    listRoles: store.permission.listRoles,
    adminStatus: store.admins.status,
    adminError: store.admins.adminError,
    success: store.admins.success,
    users: store.admins.listAdmins,
    branches: store.admins.branches,
    currentUsers: store.admins.listUsers,
    indentityTotal: store.admins.listAdmins.length,
    userTotal: store.admins.listUsers.length
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getListAdmin,
      getUsers,
      updateUser,
      addUser,
      updateUserMentor,
      listPermissions,
      listAllPermissions,
      addRolePermission,
      removeRolePermission,
      createRole,
      updateRole,
      removeRole,
      getRoles,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(User);
