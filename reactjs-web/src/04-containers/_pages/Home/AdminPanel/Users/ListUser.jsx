import LazyComponent from '../../../../../03-components/LazyComponent';
import imgAvt from '../../../../../06-assets/default_avatar.png';
import React, { memo, useState } from 'react';
import { Link } from 'react-router-dom';
import { array, func, object } from 'prop-types';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, checkValue, getFullname, linkImage } from '../../../../../08-helpers/common';

const TableHead = LazyComponent(() => import('../../../../../03-components/TableHead/index'));
const UpdateUserDialog = LazyComponent(() => import('../../../../dialog/update-user/update-user'));
const AddUserDialog = LazyComponent(() => import('../../../../dialog/add-user/add-user'));
const ModalAddMentor = LazyComponent(() => import('./ModalAddMentor'));
const ModalListNote = LazyComponent(() => import('./ModalListNote'));
const ModalAddNote = LazyComponent(() => import('./ModalAddNote'));

const userColumn = ['no', 'avatar', 'username', 'fullname', 'mentor', 'roles', 'action'];

const ListUsers = ({ thisUser, listRoles, currentUsers, updateUser, addUser, allUsers }) => {
  const [state, setState] = useState({
    isModalListNote: false,
    isModalSelectMentor: false,
    onEditMentor: {},
    isModalAddNote: false,
    showAddUserDialog: false,
    showUpdateUserDialog: false,
    id: '',
    userForAction: {}
  });

  const {
    isModalListNote,
    isModalSelectMentor,
    onEditMentor,
    isModalAddNote,
    showAddUserDialog,
    showUpdateUserDialog,
    id,
    userForAction
  } = state;

  const closeModalUserNote = () => {
    setState({
      ...state,
      id: null,
      isModalAddNote: false,
      isModalListNote: false,
      isModalSelectMentor: false
    });
  };

  const isUserInSystem = userId => {
    return currentUsers && currentUsers.map(user => user.id).includes(userId);
  };

  const onUpdateUserRole = currentUser =>
    setState({ ...state, showUpdateUserDialog: true, userForAction: currentUser });
  const onViewUserNote = id => setState({ ...state, isModalListNote: true, id });
  const onAddUserToProject = user => setState({ ...state, showAddUserDialog: true, userForAction: user });
  const onAddUserNote = id => setState({ ...state, isModalAddNote: true, id });

  const onSelectMentor = user => {
    if (user)
      setState({
        ...state,
        id: user.id,
        isModalSelectMentor: true,
        onEditMentor: {
          isEdit: !!user.infoMentor,
          infoMentor: {
            name: user.infoMentor && user.infoMentor.name,
            id: user.infoMentor && user.infoMentor.id
          }
        }
      });
  };

  const _renderAllUsersActions = (user, t) => {
    if (thisUser && thisUser.id === (user && user.id)) {
      return <div>{t('yourself')}</div>;
    } else {
      if (isUserInSystem(user && user.id)) {
        return (
          <>
            <span
              className={'cursor-pointer font-size-18 margin-right-5px'}
              title={t('update_role')}
              onClick={() => onUpdateUserRole(user)}
            >
              <i className="fa fa-pencil color-base-btn" />
            </span>
            <span
              className={'cursor-pointer font-size-18 margin-right-5px'}
              title={t('view_note')}
              onClick={() => onViewUserNote(user && user.id)}
            >
              <i className="fa fa-history color-base-btn" />
            </span>
            <span
              className={'cursor-pointer font-size-18 margin-right-5px'}
              title={t('add_note')}
              onClick={() => onAddUserNote(user && user.id)}
            >
              <i className="fa fa-sticky-note-o color-base-btn" />
            </span>
            {
              <span
                className={'cursor-pointer font-size-18'}
                title={!user.infoMentor ? t('add_mentor') : t('edit_mentor')}
                onClick={() => onSelectMentor(user)}
              >
                <i className={` ${!user.infoMentor ? 'fa fa-plus' : 'fa fa-minus '} color-base-btn`} />
              </span>
            }
          </>
        );
      }
    }
    return (
      <span className={'cursor-pointer font-size-18'} title={t('add_user')} onClick={() => onAddUserToProject(user)}>
        <i className="fa fa-user-plus color-base-btn" />
      </span>
    );
  };

  const _renderAvatarUser = user => {
    if (user) {
      if (isUserInSystem(user.id)) {
        return (
          <Link to={`/profile/${user.id}`}>
            <img
              className="avatar-user"
              src={user.profilePhoto ? linkImage(user.profilePhoto) : imgAvt}
              alt={user.username}
              title={user.username}
            />
          </Link>
        );
      }
      return (
        <img
          className="avatar-user"
          src={user.profilePhoto ? linkImage(user.profilePhoto) : imgAvt}
          alt={user.username}
          title={user.username}
        />
      );
    }
  };

  const _renderUserRoles = user => {
    if (user && isUserInSystem(user.id)) {
      return (
        listRoles &&
        user.roles.map(r => {
          const role = listRoles.find(role => role.id === r.roleId);
          return role ? role.name : null;
        })
      );
    }
    return null;
  };

  const _renderAllUsers = (users, t) => {
    if (calculateLength(users)) {
      return users.map((list, index) => {
        return (
          <tr key={list && list.id}>
            <td className={'text-center align-middle'}>{index + 1}</td>
            <td className={'text-center align-middle'}>{_renderAvatarUser(list)}</td>
            <td className={'text-center align-middle'}>{checkValue(list && list.username)}</td>
            <td className={'text-center align-middle'}>{getFullname(list && list.firstname, list && list.lastname)}</td>
            <td className={'text-center align-middle'}>
              {(list && list.infoMentor) !== null ? list && list.infoMentor && list.infoMentor.name : t('No')}
            </td>
            <td className={'text-center align-middle'}> {_renderUserRoles(list)}</td>
            <td className={'text-center align-middle'}>{_renderAllUsersActions(list, t)}</td>
          </tr>
        );
      });
    } else {
      return (
        <tr>
          <td colSpan={7} className={'text-center font-size-15 text-muted'}>
            {t('no_users')}
          </td>
        </tr>
      );
    }
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Table responsive hover striped className="default-paper table-no-border font-size-13">
            <thead>
              <tr>
                <TableHead fields={userColumn} t={t} />
              </tr>
            </thead>
            <tbody>{_renderAllUsers(allUsers, t)}</tbody>
          </Table>
          <UpdateUserDialog
            isOpen={showUpdateUserDialog}
            value={userForAction}
            onClose={() => setState({ ...state, showUpdateUserDialog: false })}
            onOK={value => {
              setState({ ...state, showUpdateUserDialog: false });
              updateUser({
                //-- for the moment one user take one role, and the backend API update just one role
                id: userForAction.roles[0] && userForAction.roles[0].id,
                userId: userForAction.id,
                roleId: value.roleId,
                roleName: value.roleName
              });
            }}
          />
          <AddUserDialog
            isOpen={showAddUserDialog}
            user={userForAction}
            onClose={() => setState({ ...state, showAddUserDialog: false })}
            onOK={value => {
              setState({ ...state, showAddUserDialog: false });
              const user = allUsers.find(u => u.id === value.id);
              addUser({
                userId: value.id,
                username: user && user.username,
                roleId: value.roleId,
                roleName: value.roleName
              });
            }}
          />
          {isModalAddNote && (
            <ModalAddNote isOpen={isModalAddNote} userId={id} closeModal={closeModalUserNote.bind(this)} />
          )}
          <ModalAddMentor
            isOpen={isModalSelectMentor}
            userId={id}
            onEditMentor={onEditMentor}
            listUser={currentUsers}
            closeModal={closeModalUserNote.bind(this)}
          />
          {isModalListNote && (
            <ModalListNote isOpen={isModalListNote} userId={id} closeModal={closeModalUserNote.bind(this)} />
          )}
        </>
      )}
    </NamespacesConsumer>
  );
};

ListUsers.propTypes = {
  thisUser: object,
  listRoles: array,
  currentUsers: array,
  allUsers: array,
  updateUser: func,
  addUser: func
};

export default memo(ListUsers);
