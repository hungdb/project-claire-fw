import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useState, useEffect, memo } from 'react';
import { bool, func, array, object, string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { updateUserMentor } from '../../../../../01-actions/adminproject.js';

const SelectPropsBox = LazyComponent(() => import('../../../../../03-components/SelectPropsBox'));

const ModalAddMentor = ({ isOpen, listUser, closeModal, userId, updateUserMentor, onEditMentor }) => {
  const [state, setState] = useState({
    mentorId: null,
    mentorName: null
  });

  const { mentorId } = state;

  useEffect(() => {
    setState({
      mentorId: onEditMentor.isEdit ? onEditMentor.infoMentor && onEditMentor.infoMentor.id : null,
      mentorName: onEditMentor.isEdit ? onEditMentor.infoMentor && onEditMentor.infoMentor.name : null
    });
  }, [onEditMentor]);

  const onUpdateMentor = () => {
    updateUserMentor(userId, mentorId);
    closeModal();
  };

  const handleSelectMentor = value => {
    setState({
      mentorId: value.value,
      mentorName: value.label
    });
  };

  const removeUserIdInList = (listUser, id) => {
    return listUser.filter(l => l.id !== id);
  };

  const onRemoveMentor = () => {
    setState({ mentorId: null });
    updateUserMentor(userId, null);
    closeModal();
  };

  return (
    <NamespacesConsumer>
      {t => (
        <Modal isOpen={isOpen} centered toggle={closeModal} fade={true} backdrop="static">
          <ModalHeader toggle={closeModal}>{!onEditMentor.isEdit ? t('add_mentor') : t('edit_mentor')}</ModalHeader>
          <ModalBody>
            <SelectPropsBox
              className="w-100"
              isClearable={false}
              options={removeUserIdInList(listUser, userId).map(u => ({ label: u.name, value: u.id }))}
              value={mentorId}
              placeholder="select_mentor"
              onChange={e => handleSelectMentor(e)}
            />
          </ModalBody>
          <ModalFooter>
            <div className="flex-grow-1 d-flex justify-content-between">
              <div>
                {onEditMentor.isEdit && (
                  <Button className={'outline-base-remove-btn'} size={'sm'} onClick={() => onRemoveMentor()}>
                    {t('remove')}
                  </Button>
                )}
              </div>
              <div>
                <Button className={'margin-right-10px outline-base-btn'} size={'sm'} onClick={closeModal}>
                  {t('cancel')}
                </Button>
                <Button className={'base-btn'} size={'sm'} disabled={!mentorId} onClick={() => onUpdateMentor()}>
                  {!onEditMentor.isEdit ? t('add') : t('edit')}
                </Button>
              </div>
            </div>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

ModalAddMentor.propTypes = {
  isOpen: bool,
  listUser: array,
  closeModal: func,
  userId: string,
  updateUserMentor: func,
  onEditMentor: object
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      updateUserMentor
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(memo(ModalAddMentor));
