import React, { useRef, memo } from 'react';
import { string, func, any } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { debounce } from 'throttle-debounce';
import styles from './AdminAllocations.module.scss';

function FieldInput({ awesomeicon, type, placeholder, value, handleInput }) {
  const textInput = useRef(null);

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className={`d-flex justify-content-between align-items-center ${styles.inputOuter}`}>
          <input
            className={styles.inputInner}
            type={type}
            placeholder={t(placeholder)}
            defaultValue={value}
            ref={textInput}
            onChange={debounce(500, () => {
              handleInput(textInput.current.value);
            })}
          />
          <i className={`${awesomeicon} mr-1 ml-2 ${styles.icon}`} aria-hidden="true" />
        </div>
      )}
    </NamespacesConsumer>
  );
}

FieldInput.propTypes = {
  awesomeicon: string,
  type: string,
  placeholder: string,
  value: any,
  handleInput: func
};

export default memo(FieldInput);
