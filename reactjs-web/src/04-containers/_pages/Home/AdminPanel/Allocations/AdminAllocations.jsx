import './AdminAllocations.module.css';
import './style.css';
import styles from './AdminAllocations.module.scss';
import queryString from 'query-string';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useState, useEffect } from 'react';
import { array, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Container, Row, Col } from 'reactstrap';
import { MILISECONDS_IN_DAY } from '../../../../../05-utils/commonData';
import { debounce } from 'throttle-debounce';
import {
  formatAllocations,
  filterAllocations,
  convertDate,
  promiseDelayImport,
  getDateAt235959ZeroGMT,
  getDateAt000000ZeroGMT
} from '../../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';

const FieldInput = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/FieldInput')),
  <Skeleton height={38} />
);

const FieldDate = LazyComponent(promiseDelayImport(import('./FieldDate')), <Skeleton height={38} />);

const Chart = LazyComponent(promiseDelayImport(import('./Chart'), 3000), <SpinLoader />);

const AppText = LazyComponent(() => import('../../../../../03-components/AppText'));

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

function AdminAllocations({
  allocations,
  skills,
  projects,
  location,
  match,
  history,
  _getAllocations,
  addNotification
}) {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [effort, setEffort] = useState('');
  const [skill, setSkill] = useState(null);
  const [project, setProject] = useState(null);
  const [user, setUser] = useState('');

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket: socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket: socket });
    };
  }, []);

  useEffect(() => {
    const urlQuery = queryString.parse(location.search);
    const nowDate = new Date();
    const toDayTime = Date.UTC(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());

    _getAllocations();
    if (!Object.keys(urlQuery).length) {
      let startDate = null;
      let endDate = null;
      switch (new Date(toDayTime).getUTCDay()) {
        case 1:
          startDate = new Date(toDayTime);
          endDate = new Date(toDayTime + 7 * MILISECONDS_IN_DAY - 1000);
          break;
        case 2:
          startDate = new Date(toDayTime - MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + 6 * MILISECONDS_IN_DAY - 1000);
          break;
        case 3:
          startDate = new Date(toDayTime - 2 * MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + 5 * MILISECONDS_IN_DAY - 1000);
          break;
        case 4:
          startDate = new Date(toDayTime - 3 * MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + 4 * MILISECONDS_IN_DAY - 1000);
          break;
        case 5:
          startDate = new Date(toDayTime - 4 * MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + 3 * MILISECONDS_IN_DAY - 1000);
          break;
        case 6:
          startDate = new Date(toDayTime - 5 * MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + 2 * MILISECONDS_IN_DAY - 1000);
          break;
        case 0:
          startDate = new Date(toDayTime - 6 * MILISECONDS_IN_DAY);
          endDate = new Date(toDayTime + MILISECONDS_IN_DAY - 1000);
          break;
        default:
          break;
      }
      setStartDate(startDate);
      setEndDate(endDate);
      setEffort(null);
      urlQuery.startDate = new Date(startDate);
      urlQuery.endDate = new Date(endDate);
      urlQuery.effort = 1;
      history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
    } else {
      if (urlQuery.startDate) {
        setStartDate(urlQuery.startDate);
      }
      if (urlQuery.endDate) {
        setEndDate(urlQuery.endDate);
      }
      if (urlQuery.effort) {
        setEffort(Number(urlQuery.effort));
      }
      if (urlQuery.skill) {
        setSkill({ value: urlQuery.skill, label: urlQuery.skill });
      }
      if (urlQuery.project) {
        setProject({ value: urlQuery.project, label: urlQuery.project });
      }
      if (urlQuery.user) {
        setUser(urlQuery.user);
      }
    }
  }, []);

  const handleStartDate = startDate => {
    const urlQuery = queryString.parse(location.search);

    if (startDate) {
      const utcTime = getDateAt000000ZeroGMT(startDate);
      setStartDate(utcTime);
      urlQuery.startDate = utcTime;
      // const choosedDate = new Date(startDate);
      // const utcTime = Date.UTC(choosedDate.getFullYear(), choosedDate.getMonth(), choosedDate.getDate());
      // setStartDate(new Date(utcTime));
      // urlQuery.startDate = new Date(utcTime);
    } else {
      setStartDate(startDate);
      delete urlQuery.startDate;
    }
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const handleEndDate = endDate => {
    const urlQuery = queryString.parse(location.search);

    if (endDate) {
      const utcTime = getDateAt235959ZeroGMT(endDate);
      setEndDate(utcTime);
      urlQuery.endDate = utcTime;
      // const choosedDate = new Date(endDate);
      // const utcTime = Date.UTC(choosedDate.getFullYear(), choosedDate.getMonth(), choosedDate.getDate(), 23, 59, 59);
      // setEndDate(new Date(utcTime));
      // urlQuery.endDate = new Date(utcTime);
    } else {
      setEndDate(endDate);
      delete urlQuery.endDate;
    }
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const handleEffort = debounce(500, effort => {
    const urlQuery = queryString.parse(location.search);

    setEffort(effort);
    effort ? (urlQuery.effort = effort) : delete urlQuery.effort;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  });

  const handleSkill = skill => {
    const urlQuery = queryString.parse(location.search);

    setSkill(skill);
    skill ? (urlQuery.skill = skill.value) : delete urlQuery.skill;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const handleProject = project => {
    const urlQuery = queryString.parse(location.search);

    setProject(project);
    project ? (urlQuery.project = project.value) : delete urlQuery.project;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const handleSearchUser = debounce(500, user => {
    const urlQuery = queryString.parse(location.search);

    setUser(user);
    user ? (urlQuery.user = user) : delete urlQuery.user;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  });

  const _renderFilter = () => (
    <Row className={`mb-3 ${styles.rowCustom}`}>
      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <FieldDate
          placeholder="start_date"
          value={startDate ? convertDate(startDate) : startDate}
          handleSelect={handleStartDate}
        />
      </Col>

      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <FieldDate
          placeholder="end_date"
          value={endDate ? convertDate(endDate) : endDate}
          handleSelect={handleEndDate}
        />
      </Col>
      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <FieldInput
          awesomeicon="fa fa-hourglass-o"
          type={'number'}
          onChangeValue={handleEffort}
          defaultValue={!isNaN(Number(effort)) ? effort : undefined}
          placeholder={'effort_percent'}
          isDebounce={true}
        />
      </Col>
      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <SelectPropsBox
          classes={styles.zIndex}
          options={skills}
          value={skill && skill.value}
          onChange={handleSkill}
          placeholder="select_skill"
        />
      </Col>
      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <SelectPropsBox
          classes={styles.zIndex}
          options={projects}
          value={project && project.value}
          onChange={handleProject}
          placeholder="select_project"
        />
      </Col>
      <Col xs={12} sm={6} md={4} xl={2} className="px-1 mb-3">
        <FieldInput
          awesomeicon="fa fa-search"
          type={'text'}
          defaultValue={user}
          onChangeValue={handleSearchUser}
          placeholder={'search_user'}
          isDebounce={true}
        />
      </Col>
    </Row>
  );

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Container fluid className="px-md-5 py-4 py-md-5 admin-allocation">
          <AppText type={'title-header'} className="mb-4">
            {t('admin_allocations')}
          </AppText>
          {_renderFilter()}
          <Chart
            data={{
              data: filterAllocations(
                formatAllocations(allocations, startDate, endDate),
                startDate,
                endDate,
                effort,
                skill,
                project,
                user
              )
            }}
          />
        </Container>
      )}
    </NamespacesConsumer>
  );
}

AdminAllocations.propTypes = {
  allocations: array,
  skills: array,
  projects: array,
  location: object,
  match: object,
  history: object,
  _getAllocations: func,
  addNotification: func
};

export default memo(AdminAllocations);
