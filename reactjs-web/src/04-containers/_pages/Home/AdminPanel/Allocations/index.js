import { bindActionCreators } from 'redux';
import AdminAllocations from './AdminAllocations';
import { getAllocations as _getAllocations } from '../../../../../01-actions/admin-allocations';
import { connect } from 'react-redux';
import { addNotification } from '../../../../../01-actions/notification';

const mapStateTopProps = state => ({
  allocations: state.adminAllocations.allocations,
  skills: state.adminAllocations.skills,
  projects: state.adminAllocations.projects
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _getAllocations,
      addNotification
    },
    dispatch
  );

export default connect(
  mapStateTopProps,
  mapDispatchToProps
)(AdminAllocations);
