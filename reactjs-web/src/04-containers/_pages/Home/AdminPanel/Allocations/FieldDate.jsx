import DatePicker from 'react-datepicker';
import styles from './AdminAllocations.module.scss';
import React, { memo } from 'react';
import { string, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

function FieldDate({ placeholder, value, handleSelect }) {
  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className={`d-flex justify-content-between align-items-center ${styles.inputOuter}`}>
          <div className={styles.inputInnerDate}>
            <DatePicker
              dropdownMode={'select'}
              className={styles.inputDatePicker}
              placeholderText={t(placeholder)}
              isClearable={true}
              dateFormat="dd/MM/yyyy"
              selected={value}
              onChange={handleSelect}
            />
          </div>
          <i className={`fa fa-calendar mr-1 ml-2 ${styles.icon}`} aria-hidden="true" />
        </div>
      )}
    </NamespacesConsumer>
  );
}

FieldDate.propTypes = {
  placeholder: string,
  value: object,
  handleSelect: func
};

export default memo(FieldDate);
