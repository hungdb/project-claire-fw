import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_tooltip';
import i18n from '../../../../../i18n';
import avatarDefault from '../../../../../06-assets/default_avatar.png';
import React, { useRef, memo, useEffect } from 'react';
import { object } from 'prop-types';
import { linkImage, formatDateTime } from '../../../../../08-helpers/common';

function Chart({ data }) {
  const ganttContainer = useRef(null);

  const initGanttChart = () => {
    const daysStyle = date => {
      const today = new Date();
      if (date.getDay() === 6 || date.getDay() === 0) return 'text-white allocations-chart-dayoff';
      if (
        date.getFullYear() === today.getFullYear() &&
        date.getMonth() === today.getMonth() &&
        date.getDate() === today.getDate()
      )
        return 'text-white bg-warning admin-allocations-chart-today';
      return '';
    };

    window.gantt.config.readonly = true;
    window.gantt.config.show_errors = false;
    window.gantt.config.scale_height = 50;
    window.gantt.config.scale_unit = 'month';
    window.gantt.config.date_scale = '%F, %Y';
    window.gantt.config.subscales = [{ unit: 'day', step: 1, date: '%j, %D', css: daysStyle }];
    window.gantt.config.columns = [
      { name: 'title', label: i18n.t('group_by_skill'), tree: true, width: 180 }
      // { name: 'full_name', label: i18n.t('fullname'), width: 220 },
    ];
    window.gantt.config.layout = {
      css: 'gantt_container',
      cols: [
        {
          width: window.innerWidth >= 992 ? 180 : 180,
          rows: [
            {
              view: 'grid',
              scrollX: 'scrollHorGrid',
              scrollY: 'scrollVer',
              scrollable: true
            },
            {
              view: 'scrollbar',
              id: 'scrollHorGrid'
            }
          ]
        },
        {
          rows: [
            {
              view: 'timeline',
              scrollX: 'scrollHorTimeline',
              scrollY: 'scrollVer'
            },
            {
              view: 'scrollbar',
              id: 'scrollHorTimeline'
            }
          ]
        },
        {
          view: 'scrollbar',
          id: 'scrollVer'
        }
      ]
    };
    window.gantt.templates.task_class = (start, end, task) => {
      if (task.level === 1 || task.isDisable) {
        return 'd-none';
      }
      // if (task.freeEffort === 100) {
      //   return 'allocation-free-color-status';
      // }
    };
    window.gantt.templates.tooltip_text = (start, end, task) => {
      if (task.level === 2) {
        return `
        <b>${i18n.t('start_date')}</b> ${formatDateTime(start, 'DD/MM/YYYY, HH:mm:ss', 1)}<br/>
        <b>${i18n.t('end_date')}</b> ${formatDateTime(end, 'DD/MM/YYYY, HH:mm:ss', 1)}<br/>
        `;
      }
      if (task.level === 3) {
        return `
        ${task.tooltip.map(t => `<b>${t.project}:</b> ${t.effort}%<br/>`).join('')}
        <b>${i18n.t('start_date')}</b> ${formatDateTime(start, 'DD/MM/YYYY, HH:mm:ss', 1)}<br/>
        <b>${i18n.t('end_date')}</b> ${formatDateTime(end, 'DD/MM/YYYY, HH:mm:ss', 1)}<br/>
        <b>${i18n.t('free')}</b> ${task.freeEffort}%<br/>
        `;
      }
    };

    window.gantt.templates.grid_folder = item => {
      const icon = item.linkIcon;
      if (icon) {
        return `<img class="gantt-skill-icon" src="${icon}"/>`;
      } else {
        return "<div class='gantt_tree_icon gantt_folder_" + (item.$open ? 'open' : 'closed') + "'></div>";
      }
    };

    window.gantt.templates.grid_file = item => {
      const avatar = item && item.avatar ? linkImage(item.avatar) : avatarDefault;
      return `<img class="gantt-avatar-icon" src="${avatar}"/>`;
    };

    window.gantt.init(ganttContainer.current);
  };

  const resizeGanttChart = () => {
    clearTimeout(window.timer);
    window.timer = setTimeout(() => {
      initGanttChart();
    }, 300);
  };

  useEffect(() => {
    initGanttChart();
    window.gantt.parse(data);
    window.addEventListener('resize', resizeGanttChart);
    return () => {
      window.gantt.clearAll();
      window.removeEventListener('resize', resizeGanttChart);
    };
  }, []);

  useEffect(() => {
    window.gantt.clearAll();
    window.gantt.parse(data);
  });

  return <div ref={ganttContainer} style={{ width: '100%', height: '70vh' }} />;
}

Chart.propTypes = {
  data: object
};

export default memo(Chart);
