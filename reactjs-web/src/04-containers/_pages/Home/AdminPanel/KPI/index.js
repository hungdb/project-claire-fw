import KPI from './KPI';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addNotification as _addNotification } from '../../../../../01-actions/notification';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _addNotification
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KPI);
