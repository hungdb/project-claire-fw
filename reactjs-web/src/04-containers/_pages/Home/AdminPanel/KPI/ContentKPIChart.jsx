import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { Fragment, memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Button, ButtonGroup } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { KPI_TYPE, KPI_TYPE_TIME } from '../../../../../05-utils/commonData';
import { string, object, any, oneOfType } from 'prop-types';
import { sortObjectInArray, getDate, getMonth, getYear, promiseDelayImport } from '../../../../../08-helpers/common';

const KpiChart = LazyComponent(promiseDelayImport(import('./Chart/KpiChart')), <SpinLoader />);
const TabTableKPI = LazyComponent(() => import('./TabTableKPI/index'));

const KPI_TAB = {
  CHART: { value: 0, label: 'chart' },
  TABLE: { value: 1, label: 'table' }
};

function ContentKPIChart({ type, typeTime, projectId, year, startDate, endDate, kpiDataChart }) {
  const [statusTab, setStatusTab] = useState(KPI_TAB.CHART.value);

  const filterDataChartProject = ({ data, projectId, startDate, endDate }) => {
    return data.filter(item => {
      let isMapFilter = true;
      if (projectId) {
        isMapFilter = `${item.projectId}` === `${projectId}`;
      }
      if (isMapFilter && (startDate || endDate)) {
        const temp = item.sprint.endDate;
        const endDateSprint = new Date(getYear(temp), getMonth(temp), getDate(temp));
        if (startDate) {
          isMapFilter = endDateSprint.getTime() >= startDate.getTime();
        }
        if (isMapFilter && endDate) {
          isMapFilter = endDateSprint.getTime() <= endDate.getTime();
        }
      }
      return isMapFilter;
    });
  };

  const filterDataChartBehavior = ({ data, year }) => {
    return data.filter(item => {
      const yearKpi = `${getYear(item.evaluatedAt)}`;
      return year === yearKpi;
    });
  };

  const filterData = () => {
    let dataChart = kpiDataChart.data;
    if (!kpiDataChart || !kpiDataChart.total || kpiDataChart.total === 0) {
      dataChart = [];
    }
    const isBehaviorMonth = type === KPI_TYPE.behavior.value && typeTime === KPI_TYPE_TIME.month.value;
    if (isBehaviorMonth && year) {
      dataChart = filterDataChartBehavior({ data: kpiDataChart.data, year });
    }

    const isProjectType = type === KPI_TYPE.projectLeader.value || type === KPI_TYPE.projectMember.value;
    if (isProjectType && (projectId || startDate || endDate)) {
      dataChart = filterDataChartProject({ data: kpiDataChart.data, projectId, startDate, endDate });
    }

    sortObjectInArray(dataChart, ['evaluatedAt', 'userId']);
    return dataChart;
  };

  const renderTabs = (t, statusTab) => {
    return (
      <div className="d-flex justify-content-end align-items-center tabs mb-3">
        <ButtonGroup className="button-group-tab">
          {Object.values(KPI_TAB).map(tab => (
            <Button
              key={tab.value}
              onClick={() => setStatusTab(tab.value)}
              active={statusTab === tab.value}
              className="tabs-btn"
            >
              {t(tab.label)}
            </Button>
          ))}
        </ButtonGroup>
      </div>
    );
  };

  const dataChart = filterData();

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          {renderTabs(t, statusTab)}
          {statusTab === KPI_TAB.CHART.value && <KpiChart dataChart={dataChart} type={type} typeTime={typeTime} />}
          {statusTab === KPI_TAB.TABLE.value && <TabTableKPI data={dataChart} type={type} typeTime={typeTime} />}
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

ContentKPIChart.propTypes = {
  type: string,
  typeTime: string,
  projectId: any,
  year: string,
  startDate: oneOfType([string, object]),
  endDate: oneOfType([string, object]),
  kpiDataChart: object
};

const mapStateToProps = state => ({
  type: state.kpi.filter.type,
  typeTime: state.kpi.filter.typeTime,
  projectId: state.kpi.filter.projectId,
  year: state.kpi.filter.year,
  startDate: state.kpi.filter.startDate,
  endDate: state.kpi.filter.endDate,
  kpiDataChart: state.kpi.kpiDataChart
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(ContentKPIChart))
);
