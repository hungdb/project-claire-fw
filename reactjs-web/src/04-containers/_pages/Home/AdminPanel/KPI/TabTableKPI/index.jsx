import './styles.css';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../../03-components/SpinLoader';
import React, { memo } from 'react';
import { array, string } from 'prop-types';
import { withRouter } from 'react-router';
import { KPI_TYPE, KPI_TYPE_TIME } from '../../../../../../05-utils/commonData';
import { getEnv } from '../../../../../../env';
import { promiseDelayImport } from '../../../../../../08-helpers/common';
import {
  attributeAverageInArray,
  formatDateTime,
  getEndOfMonth,
  getEndOfYear,
  getStartOfMonth,
  getStartOfYear,
  groupBy
} from '../../../../../../08-helpers/common';

const KPIBehaviorTable = LazyComponent(promiseDelayImport(import('./KPIBehaviorTable')), <SpinLoader />);
const KPIProjectLeaderTable = LazyComponent(promiseDelayImport(import('./KPIProjectLeaderTable')), <SpinLoader />);
const KPIProjectMemberTable = LazyComponent(promiseDelayImport(import('./KPIProjectMemberTable')), <SpinLoader />);
const KPISkillTable = LazyComponent(promiseDelayImport(import('./KPISkillTable')), <SpinLoader />);

const DEFAULT_BEHAVIOR_POINT = getEnv('DEFAULT_BEHAVIOR_POINT');
const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');

function TableKPI({ type, typeTime, data }) {
  const filterKPIData = (itemKPI, typeTime) => {
    let startDate = '';
    let endDate = '';

    if (typeTime === KPI_TYPE_TIME.month.value) {
      startDate = formatDateTime(itemKPI.startOfMonth, 'YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
      endDate = formatDateTime(getEndOfMonth(itemKPI.startOfMonth), 'YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
    } else if (typeTime === KPI_TYPE_TIME.year.value) {
      startDate = formatDateTime(itemKPI.startOfYear, 'YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
      endDate = formatDateTime(getEndOfYear(itemKPI.startOfYear), 'YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
    }

    const listKPIData = data.filter(item => {
      if (item.user.id !== itemKPI.user.id) {
        return false;
      }
      if (item.evaluatedAt < startDate || item.evaluatedAt > endDate) {
        return false;
      }
      return true;
    });
    return listKPIData;
  };

  const processDataKPIMonth = rawData => {
    const keyGroup = 'startOfMonth';
    const keyUserGroup = 'userId';
    let groupData = rawData.map(item => ({
      ...item,
      startOfMonth: getStartOfMonth(item.evaluatedAt)
    }));
    groupData = groupBy(keyGroup)(groupData);
    let kpiUsers = [];

    for (const startMonthDate in groupData) {
      if (groupData.hasOwnProperty(startMonthDate)) {
        const kpiData = groupData[startMonthDate];
        const groupUserData = groupBy(keyUserGroup)(kpiData);
        const kpiUser = [];
        for (const userId in groupUserData) {
          if (
            groupUserData.hasOwnProperty(userId) &&
            Array.isArray(groupUserData[userId]) &&
            groupUserData[userId].length > 0
          ) {
            const kpiUserItem = groupUserData[userId][0];
            let point = DEFAULT_BEHAVIOR_POINT;
            let totalPointAdd = 0;
            let totalPointSub = 0;
            const infoActor = {};
            for (let i = 0; i < groupUserData[userId].length; i++) {
              const kpiBehaviour = groupUserData[userId][i];
              point += kpiBehaviour.point;
              totalPointAdd += kpiBehaviour.totalPointAdd;
              totalPointSub -= kpiBehaviour.totalPointSub;
              const actor = kpiBehaviour.infoActor;
              infoActor[actor.id] = actor.name;
            }
            if (point < 0) {
              point = 0;
            } else if (point > KPI_MAXIMUM_POINT) {
              point = KPI_MAXIMUM_POINT;
            }

            kpiUser.push({
              id: kpiUserItem.id,
              point,
              user: kpiUserItem.user,
              nameActors: Object.values(infoActor).join(', '),
              startOfMonth: kpiUserItem.startOfMonth,
              totalPointAdd,
              totalPointSub
            });
          }
        }
        kpiUsers = kpiUsers.concat(kpiUser);
      }
    }
    return kpiUsers;
  };

  const processDataKPIYear = rawData => {
    const keyGroup = 'startOfMonth';
    const keyYearGroup = 'startOfYear';
    const keyUserGroup = 'userId';
    let groupData = rawData.map(item => ({
      ...item,
      startOfMonth: getStartOfMonth(item.evaluatedAt),
      startOfYear: getStartOfYear(item.evaluatedAt)
    }));
    groupData = groupBy(keyYearGroup)(groupData);
    let kpiUsers = [];

    for (const startYearDate in groupData) {
      if (groupData.hasOwnProperty(startYearDate)) {
        const kpiUser = [];
        const kpiData = groupData[startYearDate];
        const groupUserData = groupBy(keyUserGroup)(kpiData);
        for (const userId in groupUserData) {
          if (
            groupUserData.hasOwnProperty(userId) &&
            Array.isArray(groupUserData[userId]) &&
            groupUserData[userId].length > 0
          ) {
            const kpiUserItem = groupUserData[userId][0];
            const groupMonthData = groupBy(keyGroup)(groupUserData[userId]);
            const pointMonths = [];
            const infoActor = {};
            for (const startMonthDate in groupMonthData) {
              if (groupMonthData.hasOwnProperty(startMonthDate)) {
                const monthKPIData = groupMonthData[startMonthDate];
                if (Array.isArray(monthKPIData) && monthKPIData.length > 0) {
                  let point = DEFAULT_BEHAVIOR_POINT;
                  for (let i = 0; i < monthKPIData.length; i++) {
                    const kpiBehaviour = monthKPIData[i];
                    point += kpiBehaviour.point;
                    const actor = kpiBehaviour.infoActor;
                    infoActor[actor.id] = actor.name;
                  }
                  if (point < 0) {
                    point = 0;
                  } else if (point > KPI_MAXIMUM_POINT) {
                    point = KPI_MAXIMUM_POINT;
                  }
                  pointMonths.push({ point });
                }
              }
            }
            const point = attributeAverageInArray(pointMonths, 'point');

            kpiUser.push({
              id: kpiUserItem.id,
              point,
              user: kpiUserItem.user,
              nameActors: Object.values(infoActor).join(', '),
              startOfMonth: kpiUserItem.startOfMonth,
              startOfYear: kpiUserItem.startOfYear
            });
          }
        }
        kpiUsers = kpiUsers.concat(kpiUser);
      }
    }

    return kpiUsers;
  };

  const renderTable = () => {
    switch (type) {
      case KPI_TYPE.behavior.value: {
        let dataTable = [];
        if (typeTime === KPI_TYPE_TIME.month.value) {
          dataTable = processDataKPIMonth(data);
        } else if (typeTime === KPI_TYPE_TIME.year.value) {
          dataTable = processDataKPIYear(data);
        }
        return (
          <KPIBehaviorTable
            data={dataTable}
            filterKPIData={filterKPIData}
            processDataKPIMonth={processDataKPIMonth}
            typeTime={typeTime}
          />
        );
      }
      case KPI_TYPE.skill.value:
        return (
          <KPISkillTable
            data={processDataKPIYear(data)}
            filterKPIData={filterKPIData}
            processDataKPIMonth={processDataKPIMonth}
          />
        );
      case KPI_TYPE.projectMember.value:
        return <KPIProjectMemberTable data={data} />;
      case KPI_TYPE.projectLeader.value:
        return <KPIProjectLeaderTable data={data} />;
      default:
        return null;
    }
  };

  return <div className="tab-table-kpi">{renderTable()}</div>;
}

TableKPI.propTypes = {
  type: string,
  typeTime: string,
  data: array
};

export default withRouter(memo(TableKPI));
