import LazyComponent from '../../../../../../03-components/LazyComponent';
import TableHead from '../../../../../../03-components/TableHead';
import React, { useState } from 'react';
import { array, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { KPI_TYPE_TIME } from '../../../../../../05-utils/commonData';
import { calculateLength, formatDateTime, toFixed } from '../../../../../../08-helpers/common';
import { getEnv } from '../../../../../../env';

const KpiDetailYearDialog = LazyComponent(() => import('../../../../../dialog/kpi-coefficient/kpi-detail-year-dialog'));
const KpiItemsSkillDialog = LazyComponent(() => import('../../../../../dialog/kpi-coefficient/kpi-items-skill-dialog'));

const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');
const TABLE_FIELDS = ['no', 'user', 'content_kpi_chart.total_point', 'actor', 'evaluate_at', 'action'];

function KPISkillTable({ filterKPIData, processDataKPIMonth, data }) {
  const [itemKPI, setItemKPI] = useState(null);
  const [showKPIDialogItemsSkill, setShowKPIDialogItemsSkill] = useState(false);
  const [showKPIDialogYearDetail, setShowKPIDialogYearDetail] = useState(false);

  const onClickShowDetailSkill = itemKPI => {
    const listKPIData = filterKPIData(itemKPI, KPI_TYPE_TIME.year.value);

    const listKPIMonth = processDataKPIMonth(listKPIData);
    setShowKPIDialogYearDetail(true);
    setItemKPI({ ...itemKPI, data: listKPIMonth });
  };

  const onCloseDetailDialog = () => {
    setShowKPIDialogYearDetail(false);
    setItemKPI(null);
  };

  const onClickShowItemsSkill = itemKPI => {
    const listKPIData = filterKPIData(itemKPI, KPI_TYPE_TIME.year.value);

    setShowKPIDialogItemsSkill(true);
    setItemKPI({ ...itemKPI, data: listKPIData });
  };

  const onCloseShowItemsSkillDialog = () => {
    setShowKPIDialogItemsSkill(false);
    setItemKPI(null);
  };

  return (
    <NamespacesConsumer>
      {t => (
        <>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={TABLE_FIELDS} t={t} />
              </tr>
            </thead>
            <tbody>
              {calculateLength(data) ? (
                data.map((item, index) => {
                  return (
                    <tr key={index + 1}>
                      <td>{index + 1}</td>
                      <td>{item.user && item.user.name}</td>
                      <td>
                        <strong className="color-base">{toFixed(item.point, 3) || 0}</strong>
                      </td>
                      <td>{item.nameActors}</td>
                      <td>{formatDateTime(item.startOfMonth, 'YYYY', 1)}</td>
                      <td className="font-size-14 color-base">
                        <span
                          title={t('show_items')}
                          className={'cursor-pointer mr-2'}
                          onClick={() => onClickShowItemsSkill(item)}
                        >
                          <i className="fa fa-history" />
                        </span>
                        <span
                          title={t('show_detail')}
                          className={'cursor-pointer mr-2'}
                          onClick={() => onClickShowDetailSkill(item)}
                        >
                          <i className="fa fa-eye" />
                        </span>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={10} className="font-size-15 text-muted">
                    {t('no_kpi_data')}
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
          <KpiItemsSkillDialog isOpen={showKPIDialogItemsSkill} item={itemKPI} onClose={onCloseShowItemsSkillDialog} />
          <KpiDetailYearDialog
            isOpen={showKPIDialogYearDetail}
            item={itemKPI}
            maxPoint={KPI_MAXIMUM_POINT}
            onClose={onCloseDetailDialog}
          />
        </>
      )}
    </NamespacesConsumer>
  );
}

KPISkillTable.propTypes = {
  data: array,
  filterKPIData: func,
  processDataKPIMonth: func
};

export default KPISkillTable;
