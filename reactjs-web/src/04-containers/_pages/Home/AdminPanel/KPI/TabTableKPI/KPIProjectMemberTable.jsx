import TableHead from '../../../../../../03-components/TableHead';
import React, { memo } from 'react';
import { array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { ROUNDED_NUMBER_KPI } from '../../../../../../05-utils/commonData';
import { calculateLength, formatDateTime, toFixed } from '../../../../../../08-helpers/common';

const TABLE_FIELDS = [
  'no',
  'content_kpi_chart.total_point',
  'content_kpi_chart.task_complete',
  'content_kpi_chart.miss_deadline',
  'content_kpi_chart.error_relate',
  'content_kpi_chart.project_name',
  'content_kpi_chart.sprint_name',
  'content_kpi_chart.end_date_sprint',
  'behaviour_management.create_at'
];

function KPIProjectMemberTable({ data }) {
  return (
    <NamespacesConsumer>
      {t => (
        <Table bordered responsive striped hover>
          <thead>
            <tr>
              <TableHead fields={TABLE_FIELDS} t={t} />
            </tr>
          </thead>
          <tbody>
            {calculateLength(data) ? (
              data.map((item, index) => {
                return (
                  <tr key={index + 1}>
                    <td>{index + 1}</td>
                    <td>
                      <strong className="color-base">{toFixed(item.point || 0, ROUNDED_NUMBER_KPI)}</strong>
                    </td>
                    <td>{item.taskComplete || 0}</td>
                    <td>{item.missDeadline || 0}</td>
                    <td>{item.errorRelate || 0}</td>
                    <td>{(item.project && item.project.name) || ''}</td>
                    <td>{(item.sprint && item.sprint.name) || ''}</td>
                    <td>{(item.sprint && formatDateTime(item.sprint.endDate, 'MMM DD, YYYY', 0)) || ''}</td>
                    <td>{formatDateTime(item.createdAt, 'LLL', 1)}</td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan={10} className="font-size-15 text-muted">
                  {t('no_kpi_data')}
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      )}
    </NamespacesConsumer>
  );
}

KPIProjectMemberTable.propTypes = {
  data: array
};

export default memo(KPIProjectMemberTable);
