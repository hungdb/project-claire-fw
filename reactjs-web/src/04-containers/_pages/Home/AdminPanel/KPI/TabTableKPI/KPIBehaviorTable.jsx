import LazyComponent from '../../../../../../03-components/LazyComponent';
import TableHead from '../../../../../../03-components/TableHead';
import React, { useState, memo } from 'react';
import { array, func, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table } from 'reactstrap';
import { KPI_TYPE_TIME } from '../../../../../../05-utils/commonData';
import { calculateLength, formatDateTime, toFixed } from '../../../../../../08-helpers/common';
import { getEnv } from '../../../../../../env';

const KpiDetailMonthDialog = LazyComponent(() =>
  import('../../../../../dialog/kpi-coefficient/kpi-detail-month-dialog')
);
const KpiDetailYearDialog = LazyComponent(() => import('../../../../../dialog/kpi-coefficient/kpi-detail-year-dialog'));
const KpiItemsDialog = LazyComponent(() => import('../../../../../dialog/kpi-coefficient/kpi-items-dialog'));

const DEFAULT_BEHAVIOR_POINT = getEnv('DEFAULT_BEHAVIOR_POINT');
const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');
const TABLE_FIELDS = ['no', 'user', 'content_kpi_chart.total_point', 'actor', 'evaluate_at', 'action'];

function KPIBehaviorTable({ typeTime, filterKPIData, processDataKPIMonth, data }) {
  const [itemKPI, setItemKPI] = useState(null);
  const [showKPIDialogMonthDetail, setShowKPIDialogMonthDetail] = useState(false);
  const [showKPIDialogYearDetail, setShowKPIDialogYearDetail] = useState(false);
  const [showKPIDialogItems, setShowKPIDialogItems] = useState(false);

  const onClickShowDetailBehavior = itemKPI => {
    const listKPIData = filterKPIData(itemKPI, typeTime);
    if (typeTime === KPI_TYPE_TIME.month.value) {
      setShowKPIDialogMonthDetail(true);
      setItemKPI({ ...itemKPI, data: listKPIData });
    } else if (typeTime === KPI_TYPE_TIME.year.value) {
      const listKPIMonth = processDataKPIMonth(listKPIData);
      setShowKPIDialogYearDetail(true);
      setItemKPI({ ...itemKPI, data: listKPIMonth });
    }
  };

  const onCloseDetailDialog = () => {
    setShowKPIDialogMonthDetail(false);
    setShowKPIDialogYearDetail(false);
    setItemKPI(null);
  };

  const onClickShowItems = itemKPI => {
    const listKPIData = filterKPIData(itemKPI, typeTime);
    setShowKPIDialogItems(true);
    setItemKPI({ ...itemKPI, data: listKPIData });
  };

  const onCloseShowItemsDialog = () => {
    setShowKPIDialogItems(false);
    setItemKPI(null);
  };

  let formatDate = '';
  if (typeTime === KPI_TYPE_TIME.month.value) {
    formatDate = 'MMM YYYY';
  } else if (typeTime === KPI_TYPE_TIME.year.value) {
    formatDate = 'YYYY';
  }

  return (
    <NamespacesConsumer>
      {t => (
        <>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={TABLE_FIELDS} t={t} />
              </tr>
            </thead>
            <tbody>
              {calculateLength(data) ? (
                data.map((item, index) => {
                  return (
                    <tr key={index + 1}>
                      <td>{index + 1}</td>
                      <td>{item.user && item.user.name}</td>
                      <td>
                        <strong className="color-base">{toFixed(item.point, 3) || 0}</strong>
                      </td>
                      <td>{item.nameActors}</td>
                      <td>{formatDateTime(item.startOfMonth, formatDate, 1)}</td>
                      <td className="font-size-14 color-base">
                        <span
                          title={t('show_items')}
                          className={'cursor-pointer mr-2'}
                          onClick={() => onClickShowItems(item)}
                        >
                          <i className="fa fa-history" />
                        </span>
                        <span
                          title={t('show_detail')}
                          className={'cursor-pointer mr-2'}
                          onClick={() => onClickShowDetailBehavior(item)}
                        >
                          <i className="fa fa-eye" />
                        </span>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={10} className="font-size-15 text-muted">
                    {t('no_kpi_data')}
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
          <KpiDetailMonthDialog
            isOpen={showKPIDialogMonthDetail}
            item={itemKPI}
            defaultPoint={DEFAULT_BEHAVIOR_POINT}
            maxPoint={KPI_MAXIMUM_POINT}
            onClose={onCloseDetailDialog}
          />
          <KpiDetailYearDialog
            isOpen={showKPIDialogYearDetail}
            item={itemKPI}
            maxPoint={KPI_MAXIMUM_POINT}
            onClose={onCloseDetailDialog}
          />
          <KpiItemsDialog isOpen={showKPIDialogItems} item={itemKPI} onClose={onCloseShowItemsDialog} />
        </>
      )}
    </NamespacesConsumer>
  );
}

KPIBehaviorTable.propTypes = {
  data: array,
  filterKPIData: func,
  processDataKPIMonth: func,
  typeTime: string
};

export default memo(KPIBehaviorTable);
