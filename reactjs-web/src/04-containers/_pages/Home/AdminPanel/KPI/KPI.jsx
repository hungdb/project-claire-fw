import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo, useEffect } from 'react';
import { func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { EVENT_NOTIFICATION } from '../../../../../07-constants/socket';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../../08-helpers/socket';

const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);

const ContentKPIChart = LazyComponent(() => import('./ContentKPIChart'));
const Filter = LazyComponent(() => import('./Filter'));

function KPI({ _addNotification }) {
  useEffect(() => {
    const socket = connectSocket();

    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        _addNotification({ type: res.type, data: res.data });
      }
    });

    return () => {
      removeListener({ socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
    };
  }, []);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page">
          <AppText className="mb-4" type="title-header">
            {t('kpi_management')}
          </AppText>
          <Filter />
          <div className="d-flex mt-4">
            <div className="w-100p">
              <ContentKPIChart />
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
}

KPI.propTypes = {
  _addNotification: func
};

export default memo(KPI);
