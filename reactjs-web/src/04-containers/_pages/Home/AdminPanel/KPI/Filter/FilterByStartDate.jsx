import 'react-datepicker/dist/react-datepicker.css';
import ControlDatePicker from '../../../../../../03-components/DateTimePicker';
import queryString from 'query-string';
import React, { memo, useEffect } from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { handleFilterByStartDate as _handleFilterByStartDate } from '../../../../../../01-actions/kpi';

function FilterByStartDate({ location, history, _handleFilterByStartDate, startDate }) {
  useEffect(() => {
    const urlQuery = queryString.parse(location.search);
    const startDate = urlQuery.startDate ? new Date(urlQuery.startDate) : null;
    _handleFilterByStartDate(startDate);
  }, []);

  const changeFilter = date => {
    const urlQuery = queryString.parse(location.search);

    if (date) {
      urlQuery.startDate = date;
    } else {
      delete urlQuery.startDate;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    _handleFilterByStartDate(date);
  };

  return (
    <ControlDatePicker
      isLocalFormat
      value={startDate}
      format="DD/MM/YYYY"
      placeholder="start_date"
      onChange={changeFilter}
    />
  );
}

FilterByStartDate.propTypes = {
  location: object,
  history: object,
  startDate: object,
  _handleFilterByStartDate: func
};

const mapStateToProps = store => ({
  startDate: store.kpi.filter.startDate
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _handleFilterByStartDate
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterByStartDate))
);
