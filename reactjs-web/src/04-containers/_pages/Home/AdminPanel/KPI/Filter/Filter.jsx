import LazyComponent from '../../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import queryString from 'query-string';
import React, { Fragment, memo, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { promiseDelayImport } from '../../../../../../08-helpers/common';
import { getProjects as _getProjects } from '../../../../../../01-actions/project';
import { string, object, func, array, any } from 'prop-types';
import {
  listUsers as _listUsers,
  handleFilterByType as _handleFilterByType,
  handleFilterByTypeTime as _handleFilterByTypeTime,
  handleFilterByProject as _handleFilterByProject,
  handleFilterByYear as _handleFilterByYear,
  handleFilterByUser as _handleFilterByUser,
  getListKpiToChart as _getListKpiToChart,
  clearListKpiToChart,
  clearListUser
} from '../../../../../../01-actions/kpi';
import {
  KPI_TYPE,
  KPI_TYPE_OPTIONS,
  KPI_TYPE_TIME,
  KPI_TYPE_TIME_OPTIONS
} from '../../../../../../05-utils/commonData';

const FilterByStartDate = LazyComponent(
  promiseDelayImport(import('./FilterByStartDate.jsx')),
  <Skeleton height={38} />
);
const FilterByEndDate = LazyComponent(promiseDelayImport(import('./FilterByEndDate.jsx')), <Skeleton height={38} />);
const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

function Filter({
  _getProjects,
  _listUsers,
  location,
  history,
  _handleFilterByType,
  _handleFilterByTypeTime,
  _handleFilterByProject,
  _handleFilterByYear,
  _handleFilterByUser,
  _getListKpiToChart,
  clearListUser,
  clearListKpiToChart,
  users,
  projects,
  type,
  typeTime,
  projectId,
  year,
  userId,
  yearOptions
}) {
  useEffect(() => {
    const urlQuery = queryString.parse(location.search);
    const type = urlQuery.type ? urlQuery.type : KPI_TYPE.behavior.value;
    let typeTime = urlQuery.typeTime ? urlQuery.typeTime : null;
    const projectId = urlQuery.projectId ? urlQuery.projectId : null;
    const year = urlQuery.year ? urlQuery.year : null;
    const userId = urlQuery.userId ? urlQuery.userId : null;

    if (type === KPI_TYPE.behavior.value && typeTime === null) {
      typeTime = KPI_TYPE_TIME.month.value;
    }

    if (type) _handleFilterByType(type);
    if (typeTime) _handleFilterByTypeTime(typeTime);
    if (projectId) _handleFilterByProject(projectId);
    if (year) _handleFilterByYear(year);
    if (userId) _handleFilterByUser(userId);

    _listUsers();
    _getProjects();

    if ((type === KPI_TYPE.projectMember.value || type === KPI_TYPE.projectLeader.value) && userId === null) {
      return;
    }
    _getListKpiToChart({ type, userId });

    return () => {
      clearListUser();
      clearListKpiToChart();
    };
  }, []);

  const handleChangeValue = (e, name) => {
    const urlQuery = queryString.parse(history.location.search);
    if (e) {
      urlQuery[name] = e.value;
    } else {
      delete urlQuery[name];
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
  };

  const handleChangeTypeTime = e => {
    const typeTimeSelect = !e || !e.value ? KPI_TYPE_TIME.month : e;

    handleChangeValue(typeTimeSelect, 'typeTime');
    _handleFilterByTypeTime(typeTimeSelect.value);
  };

  const handleChangeType = e => {
    const typeSelect = !e || !e.value ? KPI_TYPE.behavior : e;
    const type = typeSelect.value;
    let userIdSelected = userId;
    const isProjectTypeInit =
      (type === KPI_TYPE.projectMember.value || type === KPI_TYPE.projectLeader.value) && !userIdSelected;
    const isExistUsers = Array.isArray(users) && users.length > 0;

    handleChangeValue(typeSelect, 'type');
    _handleFilterByType(type);

    if (isProjectTypeInit && isExistUsers) {
      userIdSelected = users[0].id;
      handleChangeValue({ value: userIdSelected }, 'userId');
      _handleFilterByUser(userIdSelected);
    } else if (type === KPI_TYPE.behavior.value && !typeTime) {
      handleChangeTypeTime(KPI_TYPE_TIME.month);
    }
    _getListKpiToChart({ type, userId: userIdSelected });
  };

  const handleChangeProject = e => {
    handleChangeValue(e, 'projectId');
    _handleFilterByProject(e && e.value ? e.value : null);
  };

  const handleChangeYear = e => {
    const yearSelect = !e && Array.isArray(yearOptions) && yearOptions.length > 0 ? yearOptions[0] : e;

    handleChangeValue(yearSelect, 'year');
    _handleFilterByYear(yearSelect && yearSelect.value ? yearSelect.value : null);
  };

  const handleChangeUser = e => {
    let userId = e && e.value ? e.value : null;
    const isProjectTypeInit =
      (type === KPI_TYPE.projectMember.value || type === KPI_TYPE.projectLeader.value) && !userId;
    const isExistUsers = Array.isArray(users) && users.length > 0;

    if (isProjectTypeInit && isExistUsers) {
      userId = users[0].id;
      handleChangeValue({ value: userId }, 'userId');
    } else {
      handleChangeValue(e, 'userId');
    }
    _handleFilterByUser(userId);
    _getListKpiToChart({ type, userId });
  };

  const userOptions = users.map(p => ({ label: p.name, value: p.id }));
  const projectOptions = projects.map(p => ({ label: p.name, value: p.id }));

  return (
    <Row className="mx-n1">
      <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
        <SelectPropsBox
          options={KPI_TYPE_OPTIONS}
          value={type}
          onChange={handleChangeType}
          placeholder="select_kpi_type"
        />
      </Col>

      {type === KPI_TYPE.behavior.value && (
        <Fragment>
          <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
            <SelectPropsBox
              options={KPI_TYPE_TIME_OPTIONS}
              value={typeTime}
              onChange={handleChangeTypeTime}
              placeholder="select_kpi_type_time"
            />
          </Col>
          {typeTime === KPI_TYPE_TIME.month.value && (
            <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
              <SelectPropsBox
                options={yearOptions}
                value={year}
                onChange={handleChangeYear}
                placeholder="select_year"
              />
            </Col>
          )}
        </Fragment>
      )}
      {(type === KPI_TYPE.projectMember.value || type === KPI_TYPE.projectLeader.value) && (
        <Fragment>
          <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
            <SelectPropsBox
              options={projectOptions}
              value={parseFloat(projectId)}
              onChange={handleChangeProject}
              placeholder="select_project"
            />
          </Col>
          <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
            <FilterByStartDate />
          </Col>
          <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
            <FilterByEndDate />
          </Col>
        </Fragment>
      )}
      <Col xs="12" sm="4" md="4" xl="2" className="px-2 mt-3">
        <SelectPropsBox
          options={userOptions}
          value={parseFloat(userId)}
          onChange={handleChangeUser}
          placeholder="select_user"
        />
      </Col>
    </Row>
  );
}

Filter.propTypes = {
  _getProjects: func,
  _listUsers: func,
  location: object,
  history: object,
  _handleFilterByType: func,
  _handleFilterByTypeTime: func,
  _handleFilterByProject: func,
  _handleFilterByYear: func,
  _handleFilterByUser: func,
  _getListKpiToChart: func,
  clearListUser: func,
  clearListKpiToChart: func,
  users: array,
  projects: array,
  type: string,
  typeTime: string,
  projectId: any,
  year: string,
  userId: any,
  // kpiBehavior: object,
  yearOptions: array
};

const mapStateToProps = state => ({
  users: state.kpi.listUsers.users,
  projects: state.project.projects,
  type: state.kpi.filter.type,
  typeTime: state.kpi.filter.typeTime,
  projectId: state.kpi.filter.projectId,
  year: state.kpi.filter.year,
  userId: state.kpi.filter.userId,
  // kpiBehavior: state.kpi.kpiBehavior,
  yearOptions: state.kpi.yearOptions
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _listUsers,
      _getProjects,
      _handleFilterByType,
      _handleFilterByTypeTime,
      _handleFilterByProject,
      _handleFilterByYear,
      _handleFilterByUser,
      _getListKpiToChart,
      clearListKpiToChart,
      clearListUser
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(Filter))
);
