import 'react-datepicker/dist/react-datepicker.css';
import ControlDatePicker from '../../../../../../03-components/DateTimePicker';
import queryString from 'query-string';
import React, { memo, useEffect } from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { handleFilterByEndDate as _handleFilterByEndDate } from '../../../../../../01-actions/kpi';

function FilterByEndDate({ location, history, endDate, _handleFilterByEndDate }) {
  useEffect(() => {
    const urlQuery = queryString.parse(location.search);
    const endDate = urlQuery.endDate ? new Date(urlQuery.endDate) : null;
    _handleFilterByEndDate(endDate);
  }, []);

  const changeFilter = date => {
    const urlQuery = queryString.parse(location.search);

    if (date) {
      urlQuery.endDate = date;
    } else {
      delete urlQuery.endDate;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    _handleFilterByEndDate(date);
  };

  return (
    <ControlDatePicker
      isLocalFormat
      value={endDate}
      format="DD/MM/YYYY"
      placeholder="end_date"
      onChange={changeFilter}
    />
  );
}

FilterByEndDate.propTypes = {
  location: object,
  history: object,
  endDate: object,
  _handleFilterByEndDate: func
};

const mapStateToProps = state => ({
  endDate: state.kpi.filter.endDate
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _handleFilterByEndDate
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterByEndDate))
);
