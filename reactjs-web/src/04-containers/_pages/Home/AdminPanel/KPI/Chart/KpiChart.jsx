import React, { memo } from 'react';
import { KPIChartMonth, KPIChartYear, KPIChartProject } from '../../../../../../03-components/KPIChart';
import { KPI_TYPE, KPI_TYPE_TIME } from '../../../../../../05-utils/commonData';
import { getEnv } from '../../../../../../env';
import { string, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import {
  attributeAverageInArray,
  getStartOfMonth,
  getStartOfYear,
  groupBy,
  sortObjectInArray,
  getDate,
  getMonth,
  getYear,
  calculateLength
} from '../../../../../../08-helpers/common';

const DEFAULT_BEHAVIOR_POINT = getEnv('DEFAULT_BEHAVIOR_POINT');
const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');
const KPI_PROJECT_MAXIMUM_COEFFICIENT = getEnv('KPI_PROJECT_MAXIMUM_COEFFICIENT');

const processDataKPIMonth = rawData => {
  const result = [];
  const title = ['x'];
  const keyGroup = 'evaluatedAt';
  const keyUserGroup = 'userId';
  const data = rawData.map(item => ({
    ...item,
    evaluatedAt: getStartOfMonth(item.evaluatedAt)
  }));
  const groupData = groupBy(keyGroup)(data);
  const kpiDataChart = {};

  const monthAxis = [];
  const nameAxisObject = {};
  const nameAxis = [];
  // Convert data to object with key is nameAxis and monthAxis
  for (const startMonthDate in groupData) {
    if (groupData.hasOwnProperty(startMonthDate)) {
      monthAxis.push(startMonthDate);
      const kpiData = groupData[startMonthDate];
      const groupUserData = groupBy(keyUserGroup)(kpiData);
      const kpiUser = {};
      for (const userId in groupUserData) {
        if (groupUserData.hasOwnProperty(userId)) {
          if (Array.isArray(groupUserData[userId]) && groupUserData[userId].length > 0) {
            let point = DEFAULT_BEHAVIOR_POINT;
            for (let i = 0; i < groupUserData[userId].length; i++) {
              const kpiBehaviour = groupUserData[userId][i];
              point += kpiBehaviour.point;
            }
            if (point < 0) {
              point = 0;
            } else if (point > KPI_MAXIMUM_POINT) {
              point = KPI_MAXIMUM_POINT;
            }
            const name = groupUserData[userId][0].user.name;

            kpiUser[userId] = point;
            nameAxisObject[userId] = { userId, name };
          }
        }
      }
      kpiDataChart[startMonthDate] = kpiUser;
    }
  }
  for (const userId in nameAxisObject) {
    if (nameAxisObject.hasOwnProperty(userId)) nameAxis.push(nameAxisObject[userId]);
  }
  sortObjectInArray(nameAxis, ['name', 'userId']);
  monthAxis.sort();

  // Add title axis
  for (let i = 0; i < nameAxis.length; i += 1) {
    title.push(nameAxis[i].name || nameAxis[i].userId);
  }
  result.push(title);

  for (let i = 0; i < monthAxis.length; i += 1) {
    const month = monthAxis[i];
    const row = [getMonth(month) + 1];
    for (let j = 0; j < nameAxis.length; j += 1) {
      const userId = nameAxis[j].userId;
      row.push(kpiDataChart[month] ? kpiDataChart[month][userId] : null);
    }
    result.push(row);
  }
  return result;
};

const processDataKPIYear = rawData => {
  const result = [];
  const title = ['x'];
  const keyGroup = 'evaluatedAt';
  const keyYearGroup = 'startOfYear';
  const keyUserGroup = 'userId';
  const data = rawData.map(item => ({
    ...item,
    evaluatedAt: getStartOfMonth(item.evaluatedAt),
    startOfYear: getStartOfYear(item.evaluatedAt)
  }));
  const groupData = groupBy(keyYearGroup)(data);
  const kpiDataChart = {};
  const yearAxis = [];
  const nameAxisObject = {};
  const nameAxis = [];
  // Convert data to object with key is nameAxis and yearAxis
  for (const startYearDate in groupData) {
    if (groupData.hasOwnProperty(startYearDate)) {
      yearAxis.push(startYearDate);
      const kpiData = groupData[startYearDate];
      const groupUserData = groupBy(keyUserGroup)(kpiData);
      const kpiUser = {};

      // Avg point in year
      for (const userId in groupUserData) {
        if (
          groupUserData.hasOwnProperty(userId) &&
          Array.isArray(groupUserData[userId]) &&
          groupUserData[userId].length > 0
        ) {
          // Calc KPI month
          const groupMonthData = groupBy(keyGroup)(groupUserData[userId]);
          const pointMonths = [];
          for (const startMonthDate in groupMonthData) {
            if (groupMonthData.hasOwnProperty(startMonthDate)) {
              const monthKPIData = groupMonthData[startMonthDate];
              if (Array.isArray(monthKPIData) && monthKPIData.length > 0) {
                let point = DEFAULT_BEHAVIOR_POINT;
                for (let i = 0; i < monthKPIData.length; i++) {
                  const kpiBehaviour = monthKPIData[i];
                  point += kpiBehaviour.point;
                }
                if (point < 0) {
                  point = 0;
                } else if (point > KPI_MAXIMUM_POINT) {
                  point = KPI_MAXIMUM_POINT;
                }
                pointMonths.push({ point });
              }
            }
          }
          kpiUser[userId] = attributeAverageInArray(pointMonths, 'point');
          const name = groupUserData[userId][0].user.name;
          nameAxisObject[userId] = { userId, name };
        }
      }
      kpiDataChart[startYearDate] = kpiUser;
    }
  }
  for (const userId in nameAxisObject) {
    if (nameAxisObject.hasOwnProperty(userId)) nameAxis.push(nameAxisObject[userId]);
  }

  sortObjectInArray(nameAxis, ['name', 'userId']);
  yearAxis.sort();

  // Add title axis
  for (let j = 0; j < nameAxis.length; j += 1) {
    title.push(nameAxis[j].name || nameAxis[j].userId);
  }
  result.push(title);

  for (let i = 0; i < yearAxis.length; i += 1) {
    const year = yearAxis[i];
    const row = [getYear(year)];
    for (let j = 0; j < nameAxis.length; j += 1) {
      const userId = nameAxis[j].userId;
      row.push(kpiDataChart[year][userId]);
    }
    result.push(row);
  }
  return result;
};

const processDataKPIProject = rawData => {
  const result = [];
  const title = [{ type: 'date', label: 'Day' }];
  const projectGroup = 'projectId';
  const keyGroup = 'endDate';
  const data = rawData.map(item => ({
    ...item,
    endDate: item.sprint ? item.sprint.endDate : ''
  }));
  const groupProjectData = groupBy(projectGroup)(data);
  const kpiDataChart = {};
  const dateAxis = [];
  const projectAxisObject = {};
  const projectAxis = [];
  const oldPoint = {};

  // Convert data to object with key is projectAxis and dateAxis
  for (const projectId in groupProjectData) {
    if (groupProjectData.hasOwnProperty(projectId)) {
      let groupData = groupProjectData[projectId];
      groupData = groupBy(keyGroup)(groupData);
      for (const endDateSprint in groupData) {
        if (groupData.hasOwnProperty(endDateSprint)) {
          dateAxis.push(endDateSprint);
          const kpiProjectData = groupData[endDateSprint];
          const kpiUser = {};

          kpiUser[projectId] = kpiProjectData[0].point;
          const name = kpiProjectData[0].project && kpiProjectData[0].project.name;
          const endDateLastSprint = kpiProjectData[0].sprint && kpiProjectData[0].sprint.endDate;
          if (!projectAxisObject[projectId]) {
            const createdAt = kpiProjectData[0].project && kpiProjectData[0].project.createdAt;
            projectAxisObject[projectId] = {
              projectId,
              name,
              createdAt,
              endDateLastSprint
            };
          } else if (endDateLastSprint > projectAxisObject[projectId].endDateLastSprint) {
            projectAxisObject[projectId].endDateLastSprint = endDateLastSprint;
          }
          kpiDataChart[endDateSprint] = { ...kpiDataChart[endDateSprint], ...kpiUser };
        }
      }
    }
  }

  for (const projectId in projectAxisObject) {
    if (projectAxisObject.hasOwnProperty(projectId)) {
      projectAxis.push(projectAxisObject[projectId]);
      dateAxis.push(projectAxisObject[projectId].createdAt);
    }
  }

  sortObjectInArray(projectAxis, ['name', 'projectId']);
  dateAxis.sort();

  // Add title axis
  for (let j = 0; j < projectAxis.length; j += 1) {
    title.push(projectAxis[j].name || projectAxis[j].projectId);
  }
  result.push(title);

  for (let i = 0; i < dateAxis.length; i += 1) {
    const date = dateAxis[i];
    const row = [new Date(getYear(date), getMonth(date), getDate(date))];
    for (let j = 0; j < projectAxis.length; j += 1) {
      const projectId = projectAxis[j].projectId;
      let point = kpiDataChart[date] ? kpiDataChart[date][projectId] : null;

      if (point || point === 0) {
        oldPoint[projectId] = point;
      } else if (projectAxis[j].createdAt === date) {
        point = 0;
      } else if (projectAxis[j].endDateLastSprint < date) {
        point = null;
      } else {
        // Set previous point for next date
        point = oldPoint[projectId];
      }
      row.push(point);
    }
    result.push(row);
  }
  return result;
};

const KpiChart = ({ dataChart, type, typeTime }) => {
  return (
    <NamespacesConsumer>
      {t => (
        <div className="kpi-chart">
          {!calculateLength(dataChart) ? (
            <div className="text-center margin-top-15px font-size-15px text-muted">{t('no_kpi_data')}</div>
          ) : type === KPI_TYPE.projectMember.value ? (
            <KPIChartProject height="500px" chartType="LineChart" data={processDataKPIProject(dataChart)} />
          ) : type === KPI_TYPE.projectLeader.value ? (
            <KPIChartProject
              height="500px"
              chartType="LineChart"
              data={processDataKPIProject(dataChart)}
              vMax={KPI_PROJECT_MAXIMUM_COEFFICIENT}
            />
          ) : type === KPI_TYPE.behavior.value && typeTime === KPI_TYPE_TIME.month.value ? (
            <KPIChartMonth
              height="500px"
              chartType="LineChart"
              data={processDataKPIMonth(dataChart)}
              vMax={KPI_MAXIMUM_POINT}
            />
          ) : (
            <KPIChartYear
              height="500px"
              chartType="LineChart"
              data={processDataKPIYear(dataChart)}
              vMax={KPI_MAXIMUM_POINT}
            />
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

KpiChart.propTypes = {
  type: string,
  typeTime: string,
  dataChart: array
};

export default memo(KpiChart);
