import Home from './Home';
import { bindActionCreators } from 'redux';
import { getMyRolePermission, getUserInfo } from '../../../01-actions/auth';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getMyRolePermission,
      getUserInfo
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(Home);
