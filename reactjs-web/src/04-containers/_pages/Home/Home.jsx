import './style.css';
import React, { useState, useEffect, lazy } from 'react';
import { object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

const Toolbar = lazy(() => import('../../../03-components/Toolbar'));
const Backdrop = lazy(() => import('../../../03-components/Backdrop'));
const RenderRoutes = lazy(() => import('../../../09-routes/RenderRoutes'));

const Home = ({ route, history, location, getMyRolePermission, getUserInfo }) => {
  const [sideDrawerOpen, setSideDrawerOpen] = useState(false);
  const backdropClickHandler = () => setSideDrawerOpen(false);

  useEffect(() => {
    const sprintsUrlCache = localStorage.getItem('sprintsUrlCache');
    if (sprintsUrlCache) {
      location.pathname === '/' && history.push(sprintsUrlCache);
    } else {
      location.pathname === '/' && history.push('/sprints');
    }
    getMyRolePermission();
    getUserInfo();
  }, []);

  let backdrop;
  if (sideDrawerOpen) {
    backdrop = <Backdrop click={backdropClickHandler} />;
  }

  return (
    <NamespacesConsumer ns="translations">
      {() => (
        <div style={{ height: '100%' }}>
          <Toolbar />
          {backdrop}
          <div>
            <div className="content-page">
              <RenderRoutes routes={route.routes} />
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

Home.propTypes = {
  history: object,
  location: object,
  getMyRolePermission: func,
  route: object,
  getUserInfo: func
};

Home.defaultProps = {};

export default Home;
