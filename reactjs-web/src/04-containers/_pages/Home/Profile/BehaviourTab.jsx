import TableHead from '../../../../03-components/TableHead';
import AppText from '../../../../03-components/AppText';
import connect from 'react-redux/es/connect/connect';
import SelectPropsBox from '../../../../03-components/SelectPropsBox';
import BehaviourShowMoreDialog from '../../../dialog/behaviour-dialog/behaviour-show-more-dialog';
import React, { Fragment, memo, useState } from 'react';
import { array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Table, Row, Col } from 'reactstrap';
import { calculateLength } from '../../../../08-helpers/common';
import {
  TYPE_APPLY_BEHAVIOR_OBJECT,
  TYPE_ACTION_BEHAVIOR_OBJECT,
  ARRAY_TYPE_APPLY,
  TYPE_SUB_APPLY
} from '../../../../05-utils/commonData';

const fields = [
  'no',
  'behaviour_management.activity',
  'behaviour_management.point',
  'behaviour_management.type_apply',
  'behaviour_management.type_action',
  'behaviour_management.action'
];

const classes = [
  'align-middle text-center w-5p',
  'align-middle text-center w-15p',
  'align-middle text-center w-5p',
  'align-middle text-center w-10p',
  'align-middle text-center w-10p',
  'align-middle text-center w-5p'
];

const BehaviourTab = ({ listBehaviour }) => {
  const [defaultTypeApply, setDefaultTypeApply] = useState(ARRAY_TYPE_APPLY[0]);
  const [showMoreDialogBehaviour, setShowMoreDialogBehaviour] = useState(false);
  const [itemBehaviour, setItemBehaviour] = useState(null);

  const onFilterBehaviorGeneral = (arrayData, typeGeneral) => {
    let data = arrayData;
    if (typeGeneral === TYPE_SUB_APPLY[0].value) {
      data = data.filter(
        item =>
          item &&
          item.isApply &&
          item.typeApply === ARRAY_TYPE_APPLY[0].value &&
          item.typeSubApply === TYPE_SUB_APPLY[0].value
      );
    } else if (typeGeneral === TYPE_SUB_APPLY[1].value) {
      data = data.filter(
        item =>
          item &&
          item.isApply &&
          item.typeApply === ARRAY_TYPE_APPLY[0].value &&
          item.typeSubApply === TYPE_SUB_APPLY[1].value
      );
    }
    return data;
  };

  const renderBodyBehaviour = (t, length, typeGeneral) => {
    let data = [];
    if (calculateLength(listBehaviour) && defaultTypeApply.value === ARRAY_TYPE_APPLY[0].value) {
      data = onFilterBehaviorGeneral(listBehaviour, typeGeneral);
    } else if (calculateLength(listBehaviour) && defaultTypeApply.value === ARRAY_TYPE_APPLY[1].value) {
      data = listBehaviour.filter(item => item.isApply && item.typeApply === ARRAY_TYPE_APPLY[1].value);
    } else if (calculateLength(listBehaviour) && defaultTypeApply.value === ARRAY_TYPE_APPLY[2].value) {
      data = listBehaviour.filter(item => item.isApply && item.typeApply === ARRAY_TYPE_APPLY[2].value);
    }
    return (
      <tbody>
        {calculateLength(data) ? (
          data
            .map((item, index) => {
              if (item) {
                const number = index + 1;
                const activity = item.activity || '';
                const point = item.point || '';
                const typeApply =
                  item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.GENERAL
                    ? t('behaviour_management.general')
                    : item.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY
                    ? t('behaviour_management.sprint_responsibility')
                    : t('behaviour_management.sprint_dedication');
                const typeAction =
                  item.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.ADD
                    ? `${t('behaviour_management.add')} (+)`
                    : `${t('behaviour_management.sub')} (-)`;
                return (
                  <tr key={number}>
                    <td className={'align-middle text-center'}> {number} </td>
                    <td className={'align-middle text-center'}> {activity} </td>
                    <td className={'align-middle text-center'}> {point} </td>
                    <td className={'align-middle text-center'}> {typeApply} </td>
                    <td className={'align-middle text-center'}> {typeAction} </td>
                    <td className={'align-middle text-center'}>
                      <span
                        onClick={() => {
                          setShowMoreDialogBehaviour(true);
                          setItemBehaviour(item);
                        }}
                        className={'cursor-pointer'}
                      >
                        <i className="fa fa-eye color-base" />
                      </span>
                    </td>
                  </tr>
                );
              } else {
                return null;
              }
            })
            .filter(Boolean)
        ) : (
          <tr>
            <td colSpan={length} className={'text-center font-size-15 text-muted'}>
              {t('not_found')}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  const onFilterBehaviour = e => setDefaultTypeApply(e);

  const renderDialogBehavior = () => {
    return (
      <BehaviourShowMoreDialog
        isOpen={showMoreDialogBehaviour}
        onClose={() => setShowMoreDialogBehaviour(false)}
        itemBehaviour={itemBehaviour}
      />
    );
  };

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <Fragment>
          <Row className="mb-4">
            <Col md="6" lg="3">
              <SelectPropsBox
                isClearable={false}
                options={ARRAY_TYPE_APPLY}
                value={defaultTypeApply && defaultTypeApply.value}
                placeholder="behaviour_management.filter_type_apply"
                onChange={onFilterBehaviour}
              />
            </Col>
          </Row>

          {renderDialogBehavior()}

          {defaultTypeApply.value === ARRAY_TYPE_APPLY[1].value && (
            <Table bordered responsive striped hover>
              <TableHead fields={fields} t={t} classes={classes} />
              {renderBodyBehaviour(t, calculateLength(fields))}
            </Table>
          )}

          {defaultTypeApply.value === ARRAY_TYPE_APPLY[2].value && (
            <Table bordered responsive striped hover>
              <TableHead fields={fields} t={t} classes={classes} />
              {renderBodyBehaviour(t, calculateLength(fields))}
            </Table>
          )}

          {defaultTypeApply.value === ARRAY_TYPE_APPLY[0].value && (
            <Fragment>
              <AppText type={'content-header'}> {t('behaviour_management.standard')} </AppText>
              <Table bordered responsive striped hover>
                <TableHead fields={fields} t={t} classes={classes} />
                {renderBodyBehaviour(t, calculateLength(fields), TYPE_SUB_APPLY[0].value)}
              </Table>
            </Fragment>
          )}

          {defaultTypeApply.value === ARRAY_TYPE_APPLY[0].value && (
            <Fragment>
              <AppText type={'content-header'}> {t('behaviour_management.bonus')} </AppText>
              <Table bordered responsive striped hover>
                <TableHead fields={fields} t={t} classes={classes} />
                {renderBodyBehaviour(t, calculateLength(fields), TYPE_SUB_APPLY[1].value)}
              </Table>
            </Fragment>
          )}
        </Fragment>
      )}
    </NamespacesConsumer>
  );
};

BehaviourTab.propTypes = {
  listBehaviour: array
};

BehaviourTab.defaultProps = {};

const mapStateToProps = state => {
  return {
    listBehaviour: state.behaviour && state.behaviour.listBehaviour
  };
};

export default connect(
  mapStateToProps,
  null
)(memo(BehaviourTab));
