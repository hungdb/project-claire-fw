import Markdown from 'react-markdown';
import LazyComponent from '../../../../03-components/LazyComponent';
import { object, func } from 'prop-types';
import React, { memo, useState, useEffect } from 'react';
import { Table } from 'reactstrap';
import { ROWS_PER_PAGE_SKILL_OPTIONS } from '../../../../05-utils/commonData';
import { NamespacesConsumer } from 'react-i18next';

const LogTabTableBody = LazyComponent(() => import('./LogTabTableBody'));
const BaseDialog = LazyComponent(() => import('../../../../03-components/Dialog'));
const TableHead = LazyComponent(() => import('../../../../03-components/TableHead'));
const Pagination = LazyComponent(() => import('../../../../03-components/Pagination'));

const fields = [
  'no',
  'tab_log_profile.skill_name',
  'tab_log_profile.description_skill',
  'tab_log_profile.project_name',
  'tab_log_profile.sprint_name',
  'behaviour_management.create_at'
];

const classes = ['w-5p', 'w-10p', 'w-10p', 'w-10p', 'w-10p', 'w-10p'];

const LogTab = ({ logMySkill, listMyUserSkills }) => {
  const [pageSize, setPageSize] = useState(ROWS_PER_PAGE_SKILL_OPTIONS[0]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isOpenDialogDescription, setIsOpenDialogDescription] = useState(false);
  const [description, setDescription] = useState('');

  const listLogMySkill = (logMySkill && logMySkill.data && logMySkill.data.data) || [];

  useEffect(() => {
    listMyUserSkills(pageSize, currentPage);
  }, []);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} classes={classes} />
              </tr>
            </thead>
            <LogTabTableBody
              currentPage={currentPage}
              pageSize={pageSize}
              listLogMySkill={listLogMySkill}
              setIsOpenDialogDescription={value => setIsOpenDialogDescription(value)}
              setDescription={value => setDescription(value)}
            />
          </Table>
          <Pagination
            totalItems={(logMySkill && logMySkill.data && logMySkill.data.total) || 1}
            pageSize={pageSize}
            value={pageSize}
            onSelect={page => {
              setCurrentPage(page);
              listMyUserSkills(pageSize, page);
            }}
            onChange={e => {
              setPageSize(e.value);
              listMyUserSkills(e.value, currentPage);
            }}
            rowsPerPageOptions={ROWS_PER_PAGE_SKILL_OPTIONS}
            isClearable={false}
          />
          {isOpenDialogDescription ? (
            <BaseDialog
              isOpen={isOpenDialogDescription}
              dialogWidth={'lg'}
              title={t('description')}
              labelBtnCancel={t('OK')}
              dialogContent={<Markdown className="markdown-content" source={description} />}
              onClose={() => {
                setIsOpenDialogDescription(false);
                setDescription('');
              }}
            />
          ) : null}
        </>
      )}
    </NamespacesConsumer>
  );
};

LogTab.propTypes = {
  logMySkill: object,
  listMyUserSkills: func
};

LogTab.defaultProps = {};

export default memo(LogTab);
