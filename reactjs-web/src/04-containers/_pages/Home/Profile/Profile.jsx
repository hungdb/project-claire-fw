import './style.css';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../03-components/LazyComponent';
import SpinLoader from '../../../../03-components/SpinLoader';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { object, func, array } from 'prop-types';
import { Tab, Tabs } from '@material-ui/core';
import { calculateLength } from '../../../../08-helpers/common';
import { getEnv } from '../../../../env';
import avatarDefault from '../../../../06-assets/images/avatar-1.png';
import { FilterAllocationChart, FilterKPIChart } from './Filter';
import { processDataAllocation } from '../../../../05-utils/commonUtils';
import { promiseDelayImport } from '../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../08-helpers/socket';
import { EVENT_NOTIFICATION } from '../../../../07-constants/socket';

const EffortChart = LazyComponent(
  promiseDelayImport(import('../../../../03-components/EffortChart')),
  <SpinLoader color="primary" type="grow" />
);

const ProgressBar = LazyComponent(promiseDelayImport(import('../../../../03-components/ProgressBar')), <Skeleton />);

const TabLog = LazyComponent(promiseDelayImport(import('./LogTab')), <SpinLoader color="primary" type="grow" />);

const BehaviourTab = LazyComponent(
  promiseDelayImport(import('./BehaviourTab')),
  <SpinLoader color="primary" type="grow" />
);

const ContentKPIChart = LazyComponent(
  promiseDelayImport(import('../AdminPanel/KPI/ContentKPIChart')),
  <SpinLoader color="primary" type="grow" />
);

const Profile = ({
  skills,
  allocations,
  userInfor,
  user,
  startDate,
  endDate,
  match,
  logMySkill,
  listMyUserSkills,
  getBehaviours,
  listAllocationsOfUser,
  clearSkillOfUser,
  clearAllocationOfUser,
  listSkillsByUser,
  addNotification
}) => {
  const [selectedTab, setSelectedTab] = useState(1);

  const getIdUserProfile = () => {
    let userId = '';
    if (!isNaN(match.params.userId)) {
      userId = match.params.userId;
    } else if (user && !isNaN(user.id)) {
      userId = user.id;
    }
    return userId;
  };

  const isMyProfile = (user && user.id) === getIdUserProfile();

  const handleSelectAllocationTab = () => {
    const userId = getIdUserProfile();
    if (userId) {
      listAllocationsOfUser(userId);
    }
  };

  const handleSelectSkillTab = () => {
    const userId = getIdUserProfile();
    if (userId) {
      listSkillsByUser(userId);
    }
  };

  const callUpdate = () => {
    const userId = getIdUserProfile();
    if (userId) {
      listAllocationsOfUser(userId);
      // listSkillsByUser(userId);
      setSelectedTab(1);
    }
  };

  const clearUpdate = () => {
    clearSkillOfUser();
    clearAllocationOfUser();
  };

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
      clearUpdate();
    };
  }, []);

  useEffect(() => {
    callUpdate();
  }, [user.id, match.params.userId]);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page profile">
          <div className={'profile-avatar'}>
            <h5>
              <img
                className={'avatar'}
                alt="avatar"
                src={!!userInfor && userInfor.avatar ? `${getEnv('S3_BUCKET')}/${userInfor.avatar}` : avatarDefault}
              />
              {userInfor && userInfor.name ? userInfor.name : ''}
            </h5>
            <h6 className={'mt-1'}>
              <i className={'fa fa-user-secret'} /> {t('mentor')}:{' '}
              {userInfor && userInfor.infoMentor && userInfor.infoMentor.name}
            </h6>
          </div>
          <hr />

          <Tabs
            value={selectedTab || false}
            className={'tabs'}
            classes={{
              indicator: 'hidden-tabs-indicator'
            }}
            onChange={(e, value) => setSelectedTab(value)}
          >
            <Tab
              className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
              value={1}
              label={t('allocation')}
              onClick={() => handleSelectAllocationTab()}
            />
            <Tab
              className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
              value={2}
              label={t('skill')}
              onClick={() => handleSelectSkillTab()}
            />
            {isMyProfile && (
              <Tab
                className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
                value={3}
                label={t('kpi')}
              />
            )}
            {isMyProfile && (
              <Tab
                className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
                value={5}
                label={t('tab_log_profile.behaviour')}
                onClick={() => getBehaviours()}
              />
            )}
            {isMyProfile && (
              <Tab
                className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
                value={4}
                label={t('tab_log_profile.log')}
              />
            )}
          </Tabs>

          {selectedTab === 1 &&
            (calculateLength(allocations) ? (
              <div className={'text-center'}>
                <FilterAllocationChart />
                <EffortChart data={processDataAllocation(allocations, startDate, endDate)} height={500} />
              </div>
            ) : (
              <div className={'text-center margin-top-15px font-size-15px text-muted'}>{t('no_allocation_data')}</div>
            ))}

          {selectedTab === 2 && (
            <div className={'profile-content'}>
              {calculateLength(skills) ? (
                <div className="skills-area">
                  {skills.map(s => (
                    <ProgressBar key={s.skillId} name={s.skill && s.skill.name} value={s.evaluationPoint} />
                  ))}
                </div>
              ) : (
                <div className={'text-center margin-top-15px font-size-15px text-muted'}>
                  {t('user_cannot_any_skill')}
                </div>
              )}
            </div>
          )}

          {isMyProfile && selectedTab === 3 && (
            <div>
              <FilterKPIChart userId={user.id} />
              <ContentKPIChart />
            </div>
          )}

          {selectedTab === 4 && <TabLog logMySkill={logMySkill} listMyUserSkills={listMyUserSkills} />}

          {selectedTab === 5 && <BehaviourTab />}
        </div>
      )}
    </NamespacesConsumer>
  );
};

Profile.propTypes = {
  startDate: object,
  endDate: object,
  listAllocationsOfUser: func,
  listSkillsByUser: func,
  listMyUserSkills: func,
  clearAllocationOfUser: func,
  clearSkillOfUser: func,
  getBehaviours: func,
  user: object,
  history: object,
  match: object,
  userInfor: object,
  allocations: array,
  skills: array,
  logMySkill: object,
  addNotification: func
};

Profile.defaultProps = {};

export default memo(Profile);
