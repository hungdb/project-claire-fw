import { calculateLength, displayNumberRecord, formatDateTime } from '../../../../08-helpers/common';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { number, array, func } from 'prop-types';

const LogTabTableBody = ({ listLogMySkill, currentPage, pageSize, setIsOpenDialogDescription, setDescription }) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <tbody>
          {calculateLength(listLogMySkill) ? (
            listLogMySkill
              .map((item, index) => {
                if (item) {
                  const skillName = (item.skill && item.skill.name) || '';
                  const descriptionSkill = item.note || '';
                  const projectName = (item.project && item.project.name) || '';
                  const sprintName = (item.sprint && item.sprint.name) || '';
                  const createdAt = formatDateTime(item.createdAt, 'LLLL', 1);
                  return (
                    <tr key={item.id}>
                      <td className={'text-center align-middle'}>
                        {' '}
                        {displayNumberRecord(currentPage, pageSize, index)}{' '}
                      </td>
                      <td className={'text-center align-middle'}> {skillName} </td>
                      <td className={'text-left align-middle'}>
                        <i
                          className="fa fa-eye text-center align-middle font-size-14 color-base icon-show-description cursor-pointer"
                          onClick={() => {
                            setIsOpenDialogDescription(true);
                            setDescription(descriptionSkill);
                          }}
                        />
                      </td>
                      <td className={'text-center align-middle'}> {projectName} </td>
                      <td className={'text-center align-middle'}> {sprintName} </td>
                      <td className={'text-center align-middle'}> {createdAt} </td>
                    </tr>
                  );
                } else {
                  return null;
                }
              })
              .filter(Boolean)
          ) : (
            <tr>
              <td colSpan={10} className={'text-center font-size-15 text-muted'}>
                {t('not_found')}
              </td>
            </tr>
          )}
        </tbody>
      )}
    </NamespacesConsumer>
  );
};

LogTabTableBody.propTypes = {
  listLogMySkill: array,
  currentPage: number,
  pageSize: number,
  setIsOpenDialogDescription: func,
  setDescription: func
};

LogTabTableBody.defaultProps = {};

export default memo(LogTabTableBody);
