import styles from './styles';
import withStyles from 'react-jss';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { Row, Col } from 'reactstrap';
import { promiseDelayImport } from '../../../../../08-helpers/common';

const FilterByStartDate = LazyComponent(
  promiseDelayImport(import('../../Allocations/Filter/FilterByStartDate')),
  <Skeleton height={38} />
);

const FilterByEndDate = LazyComponent(
  promiseDelayImport(import('../../Allocations/Filter/FilterByEndDate')),
  <Skeleton height={38} />
);

const FilterAllocationChart = ({ classes }) => {
  return (
    <Row className={classes.rowCustom}>
      <Col xs="12" md="6" xl="6" className="px-2 mb-4 mt-2 text-center">
        <FilterByStartDate />
      </Col>
      <Col xs="12" md="6" xl="6" className="px-2 mb-4 mt-2 text-center">
        <FilterByEndDate />
      </Col>
    </Row>
  );
};

FilterAllocationChart.propTypes = {
  classes: object
};

export default withStyles(styles)(memo(FilterAllocationChart));
