const styles = {
  tableWrap: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: '992px',
  },
  rowCustom: {
    marginRight: '-8px',
    marginLeft: '-8px',
  },
  badge: {
    minWidth: '100px',
    borderRadius: '20px',
    color: '#fff',
  },
  badgeStatus: {
    minWidth: '80px',
    padding: '6px',
    borderRadius: '20px',
    color: '#fff',
  },
  badgeText: {
    fontSize: '13px',
    color: '#6c757d',
  },
  line: {
    width: '100%',
    borderBottom: '1px solid #eeeeee',
  },
  input: {
    backgroundColor: 'white',
    minHeight: '38px',
    padding: '2px 8px',
    border: '1px solid hsl(0,0%,80%)',
    borderRadius: '4px',
  },
  datePicker: {
    width: '100%',
    border: 'none',
    '&:focus': {
      outline: 'none',
    },
  },
  dateContainer: {
    width: 'calc(100% - 26px)',
    '& .react-datepicker-wrapper': {
      width: '100%',
    },
    '& .react-datepicker__input-container': {
      width: '100%',
    },
  },
  icon: {
    color: 'hsl(0,0%,80%)',
  },
  btnTabView: {
    width: '48px',
    height: '36px',
    border: '1px solid hsl(0,0%,80%)',
    borderRadius: '4px',
    fontSize: '22px',
    cursor: 'pointer',
    transition: 'all 0.3s',
    '&:hover': {
      '& i': {
        color: '#6c757d',
      },
    },
    '&.selected': {
      '& i': {
        color: '#6c757d',
      }
    },
  },
  vertical: {
    height: '32px',
    marginRight: '10px',
    marginLeft: '10px',
    borderRight: '1px solid hsl(0,0%,80%)',
  },
  tableRow: {
    '&:hover': {
      backgroundColor: '#f2f2f2',
    },
  },
  iconAction: {
    display: 'inline-block',
    width: '22px',
    fontSize: '18px',
    cursor: 'pointer',
  },
};

export default styles;
