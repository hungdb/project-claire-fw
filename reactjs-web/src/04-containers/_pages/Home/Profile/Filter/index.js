import FilterAllocationChart from './FilterAllocationChart';
import FilterKPIChart from './FilterKPIChart';

export { FilterAllocationChart, FilterKPIChart };
