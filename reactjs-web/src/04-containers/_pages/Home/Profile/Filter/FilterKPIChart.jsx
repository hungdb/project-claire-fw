import styles from './styles';
import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { Fragment, memo, useEffect } from 'react';
import { object, func, array, string } from 'prop-types';
import { Row, Col } from 'reactstrap';
import { withStyles } from '@material-ui/core';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getMyProjects } from '../../../../../01-actions/project';
import { KPI_TYPE, KPI_TYPE_OPTIONS, KPI_TYPE_TIME, KPI_TYPE_TIME_OPTIONS } from '../../../../../05-utils/commonData';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { PERM_LIST_MY_PROJECTS } from '../../../../../07-constants/app-permission';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import {
  handleFilterByType as _handleFilterByType,
  handleFilterByTypeTime as _handleFilterByTypeTime,
  handleFilterByProject as _handleFilterByProject,
  handleFilterByYear as _handleFilterByYear,
  getMyListKpiToChart as _getMyListKpiToChart,
  clearListKpiToChart
} from '../../../../../01-actions/kpi';

const FilterByStartDate = LazyComponent(
  promiseDelayImport(import('../../AdminPanel/KPI/Filter/FilterByStartDate')),
  <Skeleton height={38} />
);

const FilterByEndDate = LazyComponent(
  promiseDelayImport(import('../../AdminPanel/KPI/Filter/FilterByEndDate')),
  <Skeleton height={38} />
);

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const FilterKPIChart = ({
  classes,
  projects,
  type,
  typeTime,
  projectId,
  year,
  yearOptions,
  getMyProjects,
  location,
  _handleFilterByType,
  _handleFilterByTypeTime,
  _handleFilterByProject,
  _handleFilterByYear,
  _getMyListKpiToChart,
  history,
  appPermissions,
  clearListKpiToChart
}) => {
  const projectOptions = projects.map(p => ({ label: p.name, value: p.id }));

  const handleChangeValue = (e, name) => {
    const urlQuery = queryString.parse(history.location.search);
    if (e) {
      urlQuery[name] = e.value;
    } else {
      delete urlQuery[name];
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
  };

  const handleChangeTypeTime = e => {
    const typeTimeSelect = !e || !e.value ? KPI_TYPE_TIME.month : e;
    handleChangeValue(typeTimeSelect, 'typeTime');
    _handleFilterByTypeTime(typeTimeSelect.value);
  };

  const handleChangeType = e => {
    const typeSelect = !e || !e.value ? KPI_TYPE.behavior : e;
    const type = typeSelect.value;

    handleChangeValue(typeSelect, 'type');
    _handleFilterByType(type);
    if (type === KPI_TYPE.behavior.value && !typeTime) {
      handleChangeTypeTime(KPI_TYPE_TIME.month);
    }
    _getMyListKpiToChart({ type });
  };

  const handleChangeProject = e => {
    handleChangeValue(e, 'projectId');
    _handleFilterByProject(e && e.value ? e.value : null);
  };

  const handleChangeYear = e => {
    const yearSelect = !e && Array.isArray(yearOptions) && yearOptions.length > 0 ? yearOptions[0] : e;
    handleChangeValue(yearSelect, 'year');
    _handleFilterByYear(yearSelect && yearSelect.value ? yearSelect.value : null);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    const type = urlQuery.type ? urlQuery.type : KPI_TYPE.behavior.value;
    let typeTime = urlQuery.typeTime ? urlQuery.typeTime : null;
    const projectId = urlQuery.projectId ? urlQuery.projectId : null;
    const year = urlQuery.year ? urlQuery.year : null;

    if (type === KPI_TYPE.behavior.value && typeTime === null) {
      typeTime = KPI_TYPE_TIME.month.value;
    }

    if (type) _handleFilterByType(type);
    if (typeTime) _handleFilterByTypeTime(typeTime);
    if (projectId) _handleFilterByProject(projectId);
    if (year) _handleFilterByYear(year);

    if (isAllowPermission(appPermissions, PERM_LIST_MY_PROJECTS)) {
      getMyProjects();
    }
    _getMyListKpiToChart({ type });
  };

  useEffect(() => {
    init();
    return clearListKpiToChart();
  }, []);

  return (
    <Row className={classes.rowCustom}>
      <Col xs="6" md="4" xl="2" className="px-2 mb-3">
        <SelectPropsBox
          options={KPI_TYPE_OPTIONS}
          value={type}
          onChange={handleChangeType}
          placeholder="select_kpi_type"
        />
      </Col>

      {type === KPI_TYPE.behavior.value && (
        <Fragment>
          <Col xs="6" md="4" xl="2" className="px-2 mb-3">
            <SelectPropsBox
              options={KPI_TYPE_TIME_OPTIONS}
              value={typeTime}
              onChange={handleChangeTypeTime}
              placeholder="select_kpi_type_time"
            />
          </Col>
          {typeTime === KPI_TYPE_TIME.month.value && (
            <Col xs="6" md="4" xl="2" className="px-2 mb-3">
              <SelectPropsBox
                options={yearOptions}
                value={year}
                onChange={handleChangeYear}
                placeholder="select_year"
              />
            </Col>
          )}
        </Fragment>
      )}
      {(type === KPI_TYPE.projectMember.value || type === KPI_TYPE.projectLeader.value) && (
        <Fragment>
          <Col xs="6" md="4" xl="2" className="px-2 mb-3">
            <SelectPropsBox
              options={projectOptions}
              value={parseFloat(projectId)}
              onChange={handleChangeProject}
              placeholder="select_project"
            />
          </Col>
          <Col xs="6" md="4" xl="2" className="px-2 mb-3">
            <FilterByStartDate />
          </Col>
          <Col xs="6" md="4" xl="2" className="px-2 mb-3">
            <FilterByEndDate />
          </Col>
        </Fragment>
      )}
    </Row>
  );
};

FilterKPIChart.propTypes = {
  classes: object,
  history: object,
  getMyProjects: func,
  location: object,
  _handleFilterByType: func,
  _handleFilterByTypeTime: func,
  _handleFilterByProject: func,
  _handleFilterByYear: func,
  _getMyListKpiToChart: func,
  clearListKpiToChart: func,
  projects: array,
  type: string,
  typeTime: string,
  projectId: string,
  year: string,
  // kpiBehavior: object,
  yearOptions: array,
  appPermissions: array
};

FilterKPIChart.defaultProps = {};

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  projects: state.project.projects,
  type: state.kpi.filter.type,
  typeTime: state.kpi.filter.typeTime,
  projectId: state.kpi.filter.projectId,
  year: state.kpi.filter.year,
  // kpiBehavior: state.kpi.kpiBehavior,
  yearOptions: state.kpi.yearOptions
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getMyProjects,
      _handleFilterByType,
      _handleFilterByTypeTime,
      _handleFilterByProject,
      _handleFilterByYear,
      _getMyListKpiToChart,
      clearListKpiToChart
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(memo(FilterKPIChart)))
);
