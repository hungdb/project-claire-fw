import connect from 'react-redux/es/connect/connect';
import Profile from './Profile';
import { listMyUserSkills } from '../../../../01-actions/skill';
import { bindActionCreators } from 'redux';
import { getBehaviours } from '../../../../01-actions/behaviours';
import {
  listSkillsByUser,
  listAllocationsOfUser,
  clearAllocationOfUser,
  clearSkillOfUser
} from '../../../../01-actions/allocation';
import { addNotification } from '../../../../01-actions/notification';

const mapStateToProps = state => {
  return {
    // auth
    appPermissions: state.auth.permissions,
    roles: state.auth.roles,
    status: state.auth.status,
    skills: state.allocation.skillsOfUser,
    logMySkill: state.skill.logMySkill,
    allocations: state.allocation.allocationsOfUser,
    userInfor: state.allocation.userInfor,
    user: state.auth.user,
    startDate: state.allocation.filter.startDate,
    endDate: state.allocation.filter.endDate,
    listProjects: state.kpiLeader.listProjects
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listSkillsByUser,
      listAllocationsOfUser,
      clearAllocationOfUser,
      clearSkillOfUser,
      listMyUserSkills,
      getBehaviours,
      addNotification
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
