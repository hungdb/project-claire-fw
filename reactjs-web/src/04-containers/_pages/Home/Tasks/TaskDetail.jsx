import ReactMarkdown from 'react-markdown';
import breaks from 'remark-breaks';
import React, { memo } from 'react';
import { linkRenderTarget } from '../../../../08-helpers/component';
import { formatDateTime, truncateText } from '../../../../08-helpers/common';
import { NamespacesConsumer } from 'react-i18next';
import { object } from 'prop-types';

const TaskDetail = ({ taskInfo }) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="mb-5 task-container bg-white">
          <div className="d-flex justify-content-between first-content">
            <div className="task-content" title={taskInfo.content}>
              {truncateText(taskInfo.content, 50)}
            </div>
            <div className={'project-name'}>{taskInfo.Project.name}</div>
          </div>
          <div className="d-flex justify-content-between first-content">
            <div />
            <p className="sprint-name">{taskInfo.Sprint.name}</p>
          </div>
          {
            <div>
              {/*<p>{ 'Description' }</p>*/}
              {/* <div className={ 'margin-top-15px' }>{ taskInfo.description }</div> */}
              <ReactMarkdown
                plugins={[breaks]}
                className="markdown-content"
                source={taskInfo.description}
                renderers={{ link: props => linkRenderTarget(props) }}
              />
              <hr />
              <div className="d-flex justify-content-between first-content">
                <div className="font-size-14 font-weight-500">{t('created_at')}</div>
                <p className="font-size-14">{formatDateTime(taskInfo.createdAt, 'LLLL', 1)}</p>
              </div>
              <div className="d-flex justify-content-between first-content">
                <div className="font-size-14 font-weight-500">{t('created_by')}</div>
                <p className={'creator'}>{taskInfo.creatorAs.name}</p>
              </div>
            </div>
          }
          <hr />
          {/*<p className="font-weight-bold">{ 'Streamline' }</p>*/}
          {taskInfo.listTaskState.map(item => (
            <div key={item.id} className="d-flex justify-content-between first-content">
              <div className="font-size-14">{item.User.name}</div>
              <div className="font-size-14">{`${item.fromStates.name} -> ${item.toStates.name}`}</div>
              <p className="font-size-14">{formatDateTime(item.createdAt, 'LLLL', 1)}</p>
            </div>
          ))}
        </div>
      )}
    </NamespacesConsumer>
  );
};

TaskDetail.propTypes = {
  taskInfo: object
};

TaskDetail.defaultProps = {};

export default memo(TaskDetail);
