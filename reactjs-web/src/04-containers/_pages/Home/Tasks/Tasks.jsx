import './style.css';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../03-components/LazyComponent';
import React, { memo, useEffect, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength, promiseDelayImport } from '../../../../08-helpers/common';
import { EVENT_NOTIFICATION } from '../../../../07-constants/socket';
import { array, func } from 'prop-types';
import {
  connectSocket,
  errorListener,
  removeListener,
  disconnectSocket,
  subscribeEvent
} from '../../../../08-helpers/socket';

const AppText = LazyComponent(() => import('../../../../03-components/AppText'));

const Task = LazyComponent(promiseDelayImport(import('../../../../03-components/Task')), <Skeleton height={35} />);

const TaskDetail = LazyComponent(promiseDelayImport(import('./TaskDetail')), <Skeleton count={5} />);

const ProjectCard = LazyComponent(
  promiseDelayImport(import('../../../../03-components/ProjectCard')),
  <Skeleton count={4} />
);

const Tasks = ({ projects, tasks, getProjects, getTasks, addNotification }) => {
  const [taskInfo, setTaskInfo] = useState({});
  const [taskSelect, setTaskSelect] = useState(null);

  const selectTask = id => {
    const task = tasks.find(item => item.id === id);
    setTaskSelect(id);
    setTaskInfo(task);
  };

  const init = () => {
    getProjects();
    getTasks();
  };

  useEffect(() => {
    init();
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
    };
  }, []);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page my-current-project">
          <div>
            <div className={'d-flex margin-bottom-30px'}>
              <AppText type={'title-header'}>{t('my_current_projects')}</AppText>
            </div>
            {!!calculateLength(projects) &&
              projects.map(item => (
                <ProjectCard
                  key={item.id}
                  projectName={item.Project && item.Project.name}
                  projectId={item.Project && item.Project.id}
                  sprint={item.name}
                  efforts={item.efforts}
                  startDate={item.startDate}
                  endDate={item.endDate}
                  timeCompleted={item.totalTimeCompleted}
                />
              ))}
          </div>
          <br />
          <div>
            <div className={'d-flex margin-bottom-30px'}>
              <AppText type={'title-header'}>{t('my_current_tasks')}</AppText>
            </div>
            <div className="row">
              <div className="col-md-6 col-lg-5 mb-4">
                {!!calculateLength(tasks) &&
                  tasks.map(task => (
                    <Task
                      key={task.id}
                      taskId={task.id}
                      isSelect={taskSelect === task.id}
                      taskContent={task.content}
                      selectTask={selectTask}
                      estimate={task.estimate}
                      startDate={task.startDate}
                      endDate={task.endDate}
                      totalTime={task.totalTime}
                    />
                  ))}
              </div>
              <div className="col-md-6 col-lg-7 mb-4">{taskSelect ? <TaskDetail taskInfo={taskInfo} /> : null}</div>
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

Tasks.propTypes = {
  getProjects: func,
  getTasks: func,
  tasks: array,
  projects: array,
  addNotification: func
};

Tasks.defaultProps = {};

export default memo(Tasks);
