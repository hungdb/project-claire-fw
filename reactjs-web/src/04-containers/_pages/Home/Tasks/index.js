import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import Tasks from './Tasks';
import { getProjects, getTasks } from '../../../../01-actions/task';
import { addNotification } from '../../../../01-actions/notification';

const mapStateToProps = store => ({
  status: store.task.status,
  projects: store.task.projects,
  tasks: store.task.tasks
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProjects,
      getTasks,
      addNotification
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tasks);
