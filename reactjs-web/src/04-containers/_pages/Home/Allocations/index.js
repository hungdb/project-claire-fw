import Allocation from './Allocation';
import { bindActionCreators } from 'redux';
import { addNotification } from '../../../../01-actions/notification';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addNotification
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(Allocation);
