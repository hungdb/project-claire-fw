import './style.css';
import LazyComponent from '../../../../03-components/LazyComponent';
import React, { memo, useEffect } from 'react';
import { func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { EVENT_NOTIFICATION } from '../../../../07-constants/socket';
import { Row, Col } from 'reactstrap';
import { promiseDelayImport } from '../../../../08-helpers/common';
import {
  connectSocket,
  disconnectSocket,
  errorListener,
  removeListener,
  subscribeEvent
} from '../../../../08-helpers/socket';

const AppText = LazyComponent(() => import('../../../../03-components/AppText'));
const UserInfo = LazyComponent(promiseDelayImport(import('./UserInfo')));
const Pagination = LazyComponent(() => import('./Pagination'));
const Users = LazyComponent(() => import('./Users'));
const Filter = LazyComponent(() => import('./Filter'));

const Allocation = ({ addNotification }) => {
  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
      }
    });
    return () => {
      removeListener({ socket, events: [EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
    };
  }, []);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page">
          <AppText className="mb-4" type="title-header">
            {t('allocation_management')}
          </AppText>
          <Filter />
          <Row className={'mt-4'}>
            <Col xs={12} sm={4} md={3} xl={3} className="mt-5">
              <Users />
              <Pagination />
            </Col>
            <Col xs={12} sm={8} md={9} xl={9} className="">
              <UserInfo />
            </Col>
          </Row>
        </div>
      )}
    </NamespacesConsumer>
  );
};

Allocation.propTypes = {
  addNotification: func
};

Allocation.defaultProps = {};

export default memo(Allocation);
