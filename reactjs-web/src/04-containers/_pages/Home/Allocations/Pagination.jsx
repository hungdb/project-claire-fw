import queryString from 'query-string';
import FontAwesomeIcon from '../../../../03-components/FontAwesomeIcon';
import React, { memo, useEffect, useMemo } from 'react';
import { number, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterByPage } from '../../../../01-actions/allocation';

const HOCButtonPaginate = prop => () => {
  return props =>
    !props[prop] ? (
      <FontAwesomeIcon classes={props.classActived} onClick={props.onClick} />
    ) : (
      <FontAwesomeIcon classes={props.classDisabled} />
    );
};

const ButtonLeft = HOCButtonPaginate('disabled')();
const ButtonRight = HOCButtonPaginate('disabled')();

const Pagination = ({ total, perPage, page, location, match, history, handleFilterByPage }) => {
  const calculateLastPage = (totalParam, perPageParam) => {
    return totalParam % perPageParam === 0
      ? Math.max(1, totalParam / perPageParam)
      : (totalParam - (totalParam % perPageParam)) / perPageParam + 1;
  };

  const computedLastPage = useMemo(() => calculateLastPage(total, perPage), [total, perPage]);

  const changePage = step => {
    const urlQuery = queryString.parse(location.search);
    const lastPage = computedLastPage;
    const condition = step < 0 ? page > 1 && page <= lastPage : page >= 1 && page < lastPage;

    if (condition) {
      handleFilterByPage(page + step);
      urlQuery.page = page + step;
      history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
    }
  };

  const init = () => {
    const lastPage = computedLastPage;
    const urlQuery = queryString.parse(location.search);
    const currentPage = page <= lastPage ? page : lastPage;

    handleFilterByPage(currentPage);
    urlQuery.page = currentPage;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const disabledNext = useMemo(() => !(page < computedLastPage), [page, total, perPage]);
  const disabledPre = useMemo(() => !(page > 1), [page]);

  useEffect(() => {
    init();
  }, [total]);

  return (
    <div className="d-flex justify-content-center my-4 allocation-user-pagination">
      <ButtonLeft
        disabled={disabledPre}
        classActived={'fa-arrow-circle-left mr-2 btn-left'}
        classDisabled={'fa-arrow-circle-left mr-2 btn-left-disabled'}
        onClick={() => changePage(-1)}
      />
      <ButtonRight
        disabled={disabledNext}
        classActived={'fa-arrow-circle-right mr-2 btn-right'}
        classDisabled={'fa-arrow-circle-right mr-2 btn-right-disabled'}
        onClick={() => changePage(1)}
      />
    </div>
  );
};

Pagination.propTypes = {
  location: object,
  match: object,
  history: object,
  total: number,
  perPage: number,
  page: number,
  handleFilterByPage: func
};

Pagination.defaultProps = {};

const mapStateToProps = state => ({
  total: state.allocation.listUsers.total,
  perPage: state.allocation.filter.perPage,
  page: state.allocation.filter.page
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByPage
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(Pagination))
);
