import queryString from 'query-string';
import FieldInput from '../../../../../03-components/FieldInput';
import React, { memo, useEffect } from 'react';
import { string, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterBySearchName } from '../../../../../01-actions/allocation';
import { debounce } from 'throttle-debounce';

const FilterSearch = ({ name, location, history, handleFilterBySearchName }) => {
  const changeFilter = debounce(500, key => {
    const urlQuery = queryString.parse(location.search);
    if (key) {
      urlQuery.name = key;
    } else {
      delete urlQuery.name;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterBySearchName(key);
  });

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    const key = urlQuery.name ? urlQuery.name : null;
    handleFilterBySearchName(key);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <FieldInput
      className={'w-100'}
      type={'text'}
      onChangeValue={changeFilter}
      defaultValue={name ? name : undefined}
      placeholder={'search_by_username'}
      isDebounce={true}
    />
  );
};

FilterSearch.propTypes = {
  location: object,
  match: object,
  history: object,
  classes: object,
  name: string,
  handleFilterBySearchName: func
};

FilterSearch.defaultProps = {};

const mapStateToProps = state => ({
  name: state.allocation.filter.name
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterBySearchName
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterSearch))
);
