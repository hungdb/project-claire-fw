import withStyles from 'react-jss';
import styles from './styles';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { object } from 'prop-types';
import { Row, Col } from 'reactstrap';
import { promiseDelayImport } from '../../../../../08-helpers/common';

const FilterByEndDate = LazyComponent(promiseDelayImport(import('./FilterByEndDate.jsx')), <Skeleton height={38} />);
const FilterBySkill = LazyComponent(promiseDelayImport(import('./FilterBySkill.jsx')), <Skeleton height={38} />);
const FilterByProject = LazyComponent(promiseDelayImport(import('./FilterByProject.jsx')), <Skeleton height={38} />);
const FilterSearch = LazyComponent(promiseDelayImport(import('./FilterSearch.jsx')), <Skeleton height={38} />);
const FilterEffortPercent = LazyComponent(
  promiseDelayImport(import('./FilterEffortPercent.jsx')),
  <Skeleton height={38} />
);
const FilterByStartDate = LazyComponent(
  promiseDelayImport(import('./FilterByStartDate.jsx')),
  <Skeleton height={38} />
);

const Filter = ({ classes }) => {
  return (
    <Row className={classes.rowCustom}>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterByStartDate />
      </Col>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterByEndDate />
      </Col>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterEffortPercent />
      </Col>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterBySkill />
      </Col>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterByProject />
      </Col>
      <Col xs="12" sm="6" md="4" xl="2" className="px-2 mb-3">
        <FilterSearch />
      </Col>
    </Row>
  );
};

Filter.propTypes = {
  classes: object
};

export default withStyles(styles)(memo(Filter));
