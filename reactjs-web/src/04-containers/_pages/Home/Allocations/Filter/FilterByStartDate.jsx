import queryString from 'query-string';
import ControlDatePicker from '../../../../../03-components/DateTimePicker';
import React, { memo, useEffect } from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterByStartDate } from '../../../../../01-actions/allocation';

const FilterByStartDate = ({ classes, startDate, location, history, handleFilterByStartDate }) => {
  const changeFilter = date => {
    const urlQuery = queryString.parse(location.search);
    if (date) {
      urlQuery.startDate = date;
    } else {
      delete urlQuery.startDate;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterByStartDate(date);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    let startDate = null;
    if (urlQuery.startDate) {
      startDate = new Date(urlQuery.startDate);
    } else {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      const lastWeekTime = today.getTime() - 7 * 24 * 60 ** 2 * 1000;
      startDate = new Date(lastWeekTime);
    }
    handleFilterByStartDate(startDate);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <ControlDatePicker
      value={startDate}
      format={'DD/MM/YYYY'}
      isLocalFormat={1}
      placeholder={'start_date'}
      onChange={e => changeFilter(e)}
    />
  );
};

FilterByStartDate.propTypes = {
  location: object,
  match: object,
  history: object,
  startDate: object,
  handleFilterByStartDate: func
};

FilterByStartDate.defaultProps = {};

const mapStateToProps = store => ({
  startDate: store.allocation.filter.startDate
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByStartDate
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterByStartDate))
);
