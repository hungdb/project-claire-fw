import queryString from 'query-string';
import SelectPropsBox from '../../../../../03-components/SelectPropsBox';
import React, { memo, useEffect } from 'react';
import { number, array, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterByProject } from '../../../../../01-actions/allocation';
import { getMyProjects } from '../../../../../01-actions/project';

const FilterByProject = ({ myProjects, projectId, location, history, handleFilterByProject, getMyProjects }) => {
  const changeFilter = e => {
    const urlQuery = queryString.parse(location.search);
    if (e) {
      urlQuery.projectId = e.value;
    } else {
      delete urlQuery.projectId;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterByProject(e && e.value);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    const projectId = urlQuery.projectId ? Number(urlQuery.projectId) : null;
    getMyProjects();
    handleFilterByProject(projectId);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <SelectPropsBox
      placeholder={'select_project'}
      value={projectId}
      options={myProjects}
      keyLabel={'name'}
      keyValue={'id'}
      onChange={changeFilter}
    />
  );
};

FilterByProject.propTypes = {
  location: object,
  match: object,
  history: object,
  projectId: number,
  myProjects: array,
  getMyProjects: func,
  handleFilterByProject: func,
  getMyProject: func
};

FilterByProject.defaultProps = {};

const mapStateToProps = state => ({
  myProjects: state.project.projects,
  projectId: state.allocation.filter.projectId
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getMyProjects,
      handleFilterByProject
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterByProject))
);
