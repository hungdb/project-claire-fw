import queryString from 'query-string';
import ControlDatePicker from '../../../../../03-components/DateTimePicker';
import React, { memo, useEffect } from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterByEndDate } from '../../../../../01-actions/allocation';

const FilterByEndDate = ({ endDate, location, history, handleFilterByEndDate }) => {
  const changeFilter = date => {
    const urlQuery = queryString.parse(location.search);
    if (date) {
      urlQuery.endDate = date;
    } else {
      delete urlQuery.endDate;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterByEndDate(date);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    let endDate = null;
    if (urlQuery.endDate) {
      endDate = new Date(urlQuery.endDate);
    } else {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      const nextWeekTime = today.getTime() + 7 * 24 * 60 ** 2 * 1000;
      endDate = new Date(nextWeekTime);
    }
    handleFilterByEndDate(endDate);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <ControlDatePicker
      value={endDate}
      format={'DD/MM/YYYY'}
      isLocalFormat={1}
      placeholder={'end_date'}
      onChange={e => changeFilter(e)}
    />
  );
};

FilterByEndDate.propTypes = {
  location: object,
  match: object,
  history: object,
  endDate: object,
  handleFilterByEndDate: func
};

FilterByEndDate.defaultProps = {};

const mapStateToProps = state => ({
  endDate: state.allocation.filter.endDate
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByEndDate
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterByEndDate))
);
