import queryString from 'query-string';
import FieldInput from '../../../../../03-components/FieldInput';
import React, { memo, useEffect } from 'react';
import { number, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterByEffortPercent } from '../../../../../01-actions/allocation';
import { debounce } from 'throttle-debounce';

const FilterEffortPercent = ({ location, history, handleFilterByEffortPercent, effortPercent }) => {
  const changeFilter = debounce(500, effortParam => {
    const urlQuery = queryString.parse(location.search);
    const effortConvert = effortParam < 0 || isNaN(effortParam) ? 0 : effortParam > 100 ? 100 : effortParam;

    if (effortConvert) {
      urlQuery.effortPercent = effortConvert;
    } else {
      delete urlQuery.effortPercent;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterByEffortPercent(effortConvert);
  });

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    const effortPercent = urlQuery.effortPercent ? Number(urlQuery.effortPercent) : null;
    handleFilterByEffortPercent(effortPercent);
  };

  useEffect(() => {
    init();
  }, [effortPercent]);

  return (
    <FieldInput
      className={'w-100'}
      type={'number'}
      onChangeValue={changeFilter}
      // defaultValue={ !isNaN(effortPercent) ? effortPercent : undefined }
      placeholder={'effort_percent'}
      isDebounce={true}
    />
  );
};

FilterEffortPercent.propTypes = {
  location: object,
  match: object,
  history: object,
  classes: object,
  effortPercent: number,
  handleFilterByEffortPercent: func
};

FilterEffortPercent.defaultProps = {};

const mapStateToProps = state => ({
  effortPercent: state.allocation.filter.effortPercent
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByEffortPercent
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterEffortPercent))
);
