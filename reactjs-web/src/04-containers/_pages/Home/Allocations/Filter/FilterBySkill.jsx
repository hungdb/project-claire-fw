import queryString from 'query-string';
import SelectPropsBox from '../../../../../03-components/SelectPropsBox';
import React, { memo, useEffect } from 'react';
import { number, array, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { listAllSkills } from '../../../../../01-actions/skill';
import { handleFilterBySkill } from '../../../../../01-actions/allocation';

const FilterBySkill = ({ skillId, allSkills, location, history, handleFilterBySkill, listAllSkills }) => {
  const changeFilter = e => {
    const urlQuery = queryString.parse(location.search);
    if (e) {
      urlQuery.skillId = e.value;
    } else {
      delete urlQuery.skillId;
    }
    history.push(`${location.pathname}?${queryString.stringify(urlQuery)}`);
    handleFilterBySkill(e && e.value);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    const skillId = urlQuery.skillId ? Number(urlQuery.skillId) : null;
    listAllSkills();
    handleFilterBySkill(skillId);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <SelectPropsBox
      placeholder={'select_skill'}
      value={skillId}
      options={allSkills}
      keyLabel={'name'}
      keyValue={'id'}
      onChange={changeFilter}
    />
  );
};

FilterBySkill.propTypes = {
  location: object,
  match: object,
  history: object,
  skillId: number,
  allSkills: array,
  listAllSkills: func,
  handleFilterBySkill: func
};

FilterBySkill.defaultProps = {};

const mapStateToProps = state => ({
  allSkills: state.skill.allSkills,
  skillId: state.allocation.filter.skillId
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listAllSkills,
      handleFilterBySkill
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterBySkill))
);
