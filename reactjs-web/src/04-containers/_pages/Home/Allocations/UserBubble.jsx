import React, { memo } from 'react';
import { bool, object, func } from 'prop-types';

const UserBubble = ({ selected, user, onClick }) => {
  return (
    <div onClick={onClick}>
      <div className={`no-text-select cursor-pointer user-effort-chart ${selected ? ' active' : ''}`}>
        <div className="font-size-14">@{user.name ? user.name : user.id}</div>
      </div>
    </div>
  );
};

UserBubble.propTypes = {
  selected: bool,
  user: object,
  onClick: func
};

export default memo(UserBubble);
