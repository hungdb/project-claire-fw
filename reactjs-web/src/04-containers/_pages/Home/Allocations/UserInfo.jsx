import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import TabsController from '../../../../03-components/TabsController';
import LazyComponent from '../../../../03-components/LazyComponent';
import SpinLoader from '../../../../03-components/SpinLoader';
import React, { Fragment, memo, useState, useEffect, useMemo } from 'react';
import { array, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { NamespacesConsumer } from 'react-i18next';
import { calculateLength } from '../../../../08-helpers/common';
import { listAllocationsOfUser, listSkillsByUser } from '../../../../01-actions/allocation';
import { processDataAllocation } from '../../../../05-utils/commonUtils';
import { promiseDelayImport } from '../../../../08-helpers/common';

const EffortChart = LazyComponent(
  promiseDelayImport(import('../../../../03-components/EffortChart')),
  <SpinLoader color="primary" type="grow" />
);

const ProgressBar = LazyComponent(promiseDelayImport(import('../../../../03-components/ProgressBar')), <Skeleton />);

const UserInfo = ({
  selectedUser,
  skillsOfUser,
  location,
  match,
  history,
  allocationsOfUser,
  startDate,
  endDate,
  listAllocationsOfUser,
  listSkillsByUser
}) => {
  const [activeTab, setActiveTab] = useState(1);

  const changeTab = activeTab => {
    const urlQuery = queryString.parse(location.search);
    setActiveTab(activeTab);
    urlQuery.tab = activeTab;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const processDataAllocationMemo = useMemo(() => {
    return processDataAllocation(allocationsOfUser, startDate, endDate);
  }, [allocationsOfUser, startDate, endDate]);

  const computedDataChar = () => {
    return calculateLength(allocationsOfUser) ? processDataAllocationMemo : [[]];
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    urlQuery.tab && setActiveTab(Number(urlQuery.tab));
    if (urlQuery.userId) {
      listAllocationsOfUser(Number(urlQuery.userId));
      listSkillsByUser(Number(urlQuery.userId));
    } else if (selectedUser) {
      listAllocationsOfUser(selectedUser.id);
      listSkillsByUser(selectedUser.id);
    }
  };

  useEffect(() => {
    init();
  }, [selectedUser]);

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          <div className={'d-flex justify-content-end tabs mb-2'}>
            {selectedUser && (
              <TabsController allTabsField={['chart', 'skills']} onChangeTabs={changeTab} tabId={activeTab} />
            )}
          </div>
          <div className="allocation-right-side">
            {activeTab === 1 &&
              (!selectedUser ? (
                <div className={'text-center margin-top-15px font-size-15px text-muted'}>{t('select_an_user')}</div>
              ) : computedDataChar()[0].length > 0 ? (
                <EffortChart data={computedDataChar()} height={500} />
              ) : (
                <div className={'text-center margin-top-15px font-size-15px text-muted'}>{t('no_allocation_data')}</div>
              ))}
            {activeTab === 2 &&
              (!selectedUser ? (
                <div className={'text-center margin-top-15px font-size-15px text-muted'}>{t('select_an_user')}</div>
              ) : calculateLength(skillsOfUser) ? (
                <div className="skills-area">
                  {skillsOfUser.map((s, i) => (
                    <ProgressBar key={i} name={s.skill.name} value={s.evaluationPoint} />
                  ))}
                </div>
              ) : (
                <div className={'text-center margin-top-15px font-size-15px text-muted'}>
                  {t('user_cannot_any_skill')}
                </div>
              ))}
          </div>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
};

UserInfo.propTypes = {
  location: object,
  match: object,
  path: object,
  history: object,
  selectedUser: object,
  startDate: object,
  endDate: object,
  allocationsOfUser: array,
  skillsOfUser: array,
  listAllocationsOfUser: func,
  listSkillsByUser: func
};

UserInfo.defaultProps = {};

const mapStateToProps = state => ({
  selectedUser: state.allocation.selectedUser,
  startDate: state.allocation.filter.startDate,
  endDate: state.allocation.filter.endDate,
  allocationsOfUser: state.allocation.allocationsOfUser,
  skillsOfUser: state.allocation.skillsOfUser
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listAllocationsOfUser,
      listSkillsByUser
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(UserInfo))
);
