import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../03-components/LazyComponent';
import React, { memo, useEffect } from 'react';
import { number, string, array, object, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { filterAllocation, switchUserAllocation } from '../../../../01-actions/allocation';
import { calculateLength, promiseDelayImport } from '../../../../08-helpers/common';

const UserBubble = LazyComponent(promiseDelayImport(import('./UserBubble')), <Skeleton height={40} />);

const Users = ({
  users,
  selectedUser,
  switchUserAllocation,
  location,
  match,
  history,
  startDate,
  endDate,
  effortPercent,
  skillId,
  projectId,
  name,
  page,
  filterAllocation
}) => {
  const selectUserUrl = user => {
    const urlQuery = queryString.parse(location.search);
    urlQuery.userId = user.id;
    history.push(`${match.path}?${queryString.stringify(urlQuery)}`);
  };

  const init = () => {
    const urlQuery = queryString.parse(location.search);
    switchUserAllocation(urlQuery.userId ? { id: Number(urlQuery.userId) } : selectedUser);
    filterAllocation({
      startDate,
      endDate,
      effortPercent,
      skillId,
      projectId,
      name,
      page
    });
  };

  useEffect(() => {
    init();
  }, [startDate, endDate, effortPercent, skillId, projectId, name, page]);

  return (
    <div className="allocation-users-area">
      {calculateLength(users)
        ? users.map(u => (
            <UserBubble
              key={u.id}
              onClick={() => {
                switchUserAllocation(u);
                selectUserUrl(u);
              }}
              user={u}
              selected={u.id === (selectedUser && selectedUser.id)}
            />
          ))
        : null}
    </div>
  );
};

Users.propTypes = {
  location: object,
  match: object,
  history: object,
  users: array,
  selectedUser: object,
  startDate: object,
  endDate: object,
  effortPercent: number,
  skillId: number,
  projectId: number,
  name: string,
  page: number,
  filterAllocation: func,
  switchUserAllocation: func
};

Users.defaultProps = {};

const mapStateToProps = state => ({
  users: state.allocation.listUsers.users,
  selectedUser: state.allocation.selectedUser,
  startDate: state.allocation.filter.startDate,
  endDate: state.allocation.filter.endDate,
  effortPercent: state.allocation.filter.effortPercent,
  skillId: state.allocation.filter.skillId,
  projectId: state.allocation.filter.projectId,
  name: state.allocation.filter.name,
  page: state.allocation.filter.page
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      filterAllocation,
      switchUserAllocation
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(Users))
);
