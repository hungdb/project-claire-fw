import withStyles from 'react-jss';
import styles from './styles';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useEffect } from 'react';
import { number, array, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Paper } from '@material-ui/core';
import { calculateLength } from '../../../../../08-helpers/common';

const UserProgress = LazyComponent(() => import('./UserProgress'));

const TabProgress = ({
  sprintProgressMembers,
  classes,
  activeSprintId,
  resetProgressMembersInActiveSprint,
  getProgressMembersInActiveSprint,
  selectedProject
}) => {
  useEffect(() => {
    if (activeSprintId) {
      getProgressMembersInActiveSprint(activeSprintId);
    } else {
      resetProgressMembersInActiveSprint();
    }
  }, [activeSprintId, selectedProject]);

  return (
    <NamespacesConsumer>
      {t => (
        <Paper className="default-paper progress-container">
          {calculateLength(sprintProgressMembers) ? (
            sprintProgressMembers.map(m => <UserProgress key={m.id} classes={classes} member={m} />)
          ) : (
            <div className="text-center">
              <p className={'text-muted font-size-15'}>{t('not_found')}</p>
            </div>
          )}
        </Paper>
      )}
    </NamespacesConsumer>
  );
};

TabProgress.propTypes = {
  activeSprintId: number,
  sprintProgressMembers: array,
  selectedProject: object,
  resetProgressMembersInActiveSprint: func,
  getProgressMembersInActiveSprint: func,
  classes: object
};

TabProgress.defaultProps = {};

export default withStyles(styles)(memo(TabProgress));
