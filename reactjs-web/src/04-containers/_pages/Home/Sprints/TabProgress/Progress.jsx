import React, { memo } from 'react';
import { Progress as TimeProgress } from 'reactstrap';
import { object } from 'prop-types';

const computedTimeProgress = (completed, total) => {
  return ((completed / total) * 100).toFixed(0);
};

const Progress = ({ member }) => (
  <TimeProgress color="success" value={computedTimeProgress(member.totalTimeCompleted, member.totalTime)}>
    {computedTimeProgress(member.totalTimeCompleted, member.totalTime)}%
  </TimeProgress>
);

Progress.propTypes = {
  member: object
};

Progress.defaultProps = {};

export default memo(Progress);
