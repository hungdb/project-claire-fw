import TabProgress from './TabProgress';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getProgressMembersInActiveSprint,
  resetProgressMembersInActiveSprint
} from '../../../../../01-actions/sprint-progress';

const mapStateToProps = state => ({
  sprintProgressMembers: state.sprintProgress.sprintProgressMembers,
  activeSprintId: state.sprint.activeSprint && state.sprint.activeSprint.id,
  selectedProject: state.sprint.selectedProject
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getProgressMembersInActiveSprint,
      resetProgressMembersInActiveSprint
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabProgress);
