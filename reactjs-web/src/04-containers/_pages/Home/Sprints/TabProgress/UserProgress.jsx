import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { toFixed, promiseDelayImport } from '../../../../../08-helpers/common';
import { object } from 'prop-types';

const Text = LazyComponent(promiseDelayImport(import('../../../../../03-components/Text')), <Skeleton width={50} />);

const Progress = LazyComponent(promiseDelayImport(import('./Progress')), <Skeleton />);

const UserAvatar = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/UserAvatar')),
  <Skeleton circle={true} height={30} width={30} />
);

const UserProgress = ({ classes, member }) => {
  return (
    <div className="d-flex align-items-center mb-3">
      <div className={`${classes.avatar} mr-3`}>
        <UserAvatar id={member.id} user={member} src={`/profile/${member.id}`} />
      </div>
      <div className="w-100">
        <div className="d-flex justify-content-between">
          <Text className={classes.textUser}>{member.name}</Text>
          <Text className={classes.textTime}>
            {toFixed(member.totalTimeCompleted, 1)} / {toFixed(member.totalTime, 1)}h
          </Text>
        </div>
        <Progress member={member} />
      </div>
    </div>
  );
};

UserProgress.propTypes = {
  member: object,
  classes: object
};

UserProgress.defaultProps = {};

export default memo(UserProgress);
