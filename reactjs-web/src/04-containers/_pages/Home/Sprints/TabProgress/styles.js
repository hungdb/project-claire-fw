export default {
  root: {
    color: 'red'
  },
  avatar: {
    marginTop: '10px'
  },
  textUser: {
    fontSize: 16,
    color: '#6c757d'
  },
  textTime: {
    fontSize: 14,
    color: '#6c757d'
  }
};
