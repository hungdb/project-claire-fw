import moment from 'moment';
import i18n from '../../../../../i18n';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import styles from './SprintManagement.module.scss';
import React, { Fragment, useEffect, useRef, useState, memo } from 'react';
import { any, array, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { Button } from 'reactstrap';
import { workloadAllocations, workloadTasks } from '../../../../../05-utils/commonUtils';
import { CREATE_ALLOCATION } from '../../../../../07-constants/app-permission';
import {
  DurationTime,
  formatDateTime,
  isAllowPermission,
  toFixed,
  parseJSON,
  calculateLength,
  promiseDelayImport
} from '../../../../../08-helpers/common';
import { TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';

const TaskSuggestCard = LazyComponent(promiseDelayImport(import('./TaskSuggestCard')), <Skeleton height={58} />);

const TaskCard = LazyComponent(promiseDelayImport(import('./TaskCard')), <Skeleton height={58} />);

const AllocationCard = LazyComponent(promiseDelayImport(import('./AllocationCard')), <Skeleton height={58} />);

const FieldInput = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/FieldInput')),
  <Skeleton height={38} />
);

const Text = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Text')),
  <Skeleton width={155.33} />
);

const TaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/task-dialog'));
const AddAllocation = LazyComponent(() => import('./AddAllocation'));
const UpdateAllocation = LazyComponent(() => import('./UpdateAllocation'));
const ConfirmationModal = LazyComponent(() => import('../../../../../03-components/ConfirmationModal'));

function SprintManagement({
  sprint,
  searchBacklogTask,
  currentProjectId,
  currentSprintId,
  status,
  searchBacklog,
  usersOfProject,
  removeTaskToBacklog,
  addTaskToSprint,
  getDetailSprint,
  removeAllocation,
  addAllocation,
  getUsersOfProject,
  setKeySearchTask,
  appPermissions,
  allocationLabels,
  currentTask,
  _clearCurrentTask,
  _getDetailTask,
  _deleteTask,
  _updateBacklogTask,
  _updateAllocation,
  tags,
  allocations,
  oldAllocations
}) {
  const wrapperRef = useRef(null);
  const [autoCompleteTask, setAutoCompleteTask] = useState(true);
  const [openAllocationDialog, setOpenAllocationDialog] = useState(false);
  const [openAlertConfirmDeleteAllocation, setOpenAlertConfirmDeleteAllocation] = useState(false);
  const [selectedAllocation, setSelectedAllocation] = useState(null);
  const [isUpdateTask, setIsUpdateTask] = useState(false);
  const [isUpdateAllocation, setIsUpdateAllocation] = useState(false);
  const [allocationSelected, setAllocationSelected] = useState(null);

  const handleClickOutside = event => {
    if (wrapperRef && wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setAutoCompleteTask(false);
    }
  };

  useEffect(() => {
    getUsersOfProject({ projectId: currentProjectId });
    document.body.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.body.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  // useEffect(() => {
  //   getDetailSprint({ sprintId: currentSprintId, projectId: currentProjectId });
  // }, [currentTask]);

  const filterTask = event => {
    const value = event.target && event.target.value;
    if (!currentProjectId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      if (currentSprintId) {
        searchBacklogTask({ projectId: currentProjectId, key: value });
        setAutoCompleteTask(true);
        setKeySearchTask(value);
      }
    }
  };

  const handleAddTask = task => {
    if (!task || !task.id || !currentSprintId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      addTaskToSprint({ sprintId: currentSprintId, taskId: task.id, projectId: task.projectId });
    }
  };

  const handleAddAllTask = () => {
    addTaskToSprint({ sprintId: currentSprintId, taskId: null, projectId: null, type: 1 });
  };

  const handleRemoveTaskToBacklog = (e, task) => {
    if (!task || !task.id) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      removeTaskToBacklog({ taskId: task.id, projectId: task.projectId });
    }
    e.stopPropagation();
  };

  const removeAllTaskToBacklog = () => {
    removeTaskToBacklog({ taskId: null, projectId: null, type: 1, sprintId: currentSprintId });
  };

  const handleRemoveAllocation = allocation => {
    if (!allocation || !allocation.id || !currentProjectId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      removeAllocation({ allocationId: allocation.id, projectId: currentProjectId });
    }
  };

  const openAddAllocationDialog = () => {
    getUsersOfProject({ projectId: currentProjectId });
    setOpenAllocationDialog(true);
  };

  const closeAddAllocationDialog = () => setOpenAllocationDialog(false);

  const handleAddAllocation = data => {
    addAllocation({
      sprintId: currentSprintId,
      data
    });
  };

  const handleCloseDialog = () => {
    _clearCurrentTask();
    setIsUpdateTask(false);
  };

  const handleDeleteBacklogTask = () => {
    _deleteTask({ taskId: currentTask.id, currentProjectId });
    setIsUpdateTask(false);
  };

  const handleUpdateBacklogTask = data => {
    const { content, estimate, description, tags, priority, dependencies, assignee, dueDate, type, userRelate } = data;
    const { id } = currentTask;

    const dataTag = tags.map(tag => tag.text);
    _updateBacklogTask({
      taskId: id,
      estimate,
      content,
      description,
      tag: dataTag,
      projectId: currentProjectId,
      priority,
      assignee,
      dependencies,
      dueDate,
      type,
      userRelate
    });
    setIsUpdateTask(false);
  };

  const handleOpenUpdateTask = taskId => {
    _getDetailTask({ taskId, projectId: currentProjectId });
    setIsUpdateTask(true);
  };

  const computedTasks = tasks => {
    const tags = [];
    const tasksGrouped = [];

    for (const task of tasks) {
      const jsonTags = JSON.parse(task.tag);
      const tag = calculateLength(jsonTags) ? jsonTags[0] : '';

      if (!tags.includes(tag)) {
        tags.push(tag);

        const i = tags.length - 1;

        tasksGrouped[i] = { group: tag, totalEstimate: task.estimate || 0, tasks: [] };
        tasksGrouped[i].tasks.push(task);
      } else {
        tasksGrouped[tags.indexOf(tag)].totalEstimate =
          tasksGrouped[tags.indexOf(tag)].totalEstimate + (task.estimate || 0);
        tasksGrouped[tags.indexOf(tag)].tasks.push(task);
      }
    }
    return tasksGrouped;
  };

  const computedAllocations = (allocations, tasks, allocationLabels) => {
    const tasksGrouped = computedTasks(tasks);
    return allocationLabels.map(group => {
      let allocationGrouped = [];
      allocations.forEach(allocation => {
        if (
          (allocation.labels && allocation.labels.name.toUpperCase()) === group.group.name.toUpperCase() ||
          (allocation.labels === null && group.group.name.toUpperCase() === 'NOGROUP')
        ) {
          allocationGrouped = [...allocationGrouped, allocation];
        }
      });
      const taskGroup = tasksGrouped.find(
        taskGroup =>
          taskGroup.group.toUpperCase() === group.group.name.toUpperCase() ||
          (taskGroup.group === '' && group.group.name.toUpperCase() === 'NOGROUP')
      );
      return {
        ...group,
        allocations: allocationGrouped,
        totalTimeCurrent: taskGroup ? taskGroup.totalEstimate : 0
      };
    });
  };

  const handleOpenUpdateAllocation = allocationSelected => {
    setIsUpdateAllocation(true);
    setAllocationSelected(allocationSelected);
  };

  const handleCloseUpdateAllocation = () => {
    setIsUpdateAllocation(false);
    setAllocationSelected(null);
  };

  const computedOldAllocationRaws = allocations => {
    const startDate = sprint && sprint.startDate;
    const endDate = sprint && sprint.endDate;

    const allocationsUnduplicated = [];
    allocations.forEach(allocation => {
      if (allocationsUnduplicated.length === 0) {
        allocationsUnduplicated.push(allocation);
      } else {
        let isAdd = true;
        for (let i = 0; i < allocationsUnduplicated.length; i++) {
          if (allocation.userId === allocationsUnduplicated[i].userId) {
            isAdd = false;
            break;
          }
        }
        if (isAdd) {
          allocationsUnduplicated.push(allocation);
        }
      }
    });

    let allocationRaws = [];
    allocationsUnduplicated.forEach((allocation, i) => {
      if (i === 0) {
        allocationRaws = [
          ...allocationRaws,
          {
            users: [{ value: allocation.User.id, label: allocation.User.name }],
            type: allocation.labels
              ? { value: allocation.labels.id, label: allocation.labels.name.toUpperCase() }
              : null,
            effort: allocation.effortPercent,
            startDate,
            endDate
          }
        ];
      } else {
        const allocationRawsEntries = allocationRaws.entries();
        let isNoAdd = true;
        for (const [iRaw, allocationRaw] of allocationRawsEntries) {
          if (
            ((!allocationRaw.type && !allocation.labels) ||
              (allocationRaw.type && allocation.labels && allocationRaw.type.value === allocation.labels.id)) &&
            parseInt(allocationRaw.effort) === parseInt(allocation.effortPercent) &&
            new Date(allocationRaw.startDate).getTime() === new Date(allocation.startDate).getTime() &&
            new Date(allocationRaw.endDate).getTime() === new Date(allocation.endDate).getTime()
          ) {
            allocationRaws = [...allocationRaws];
            allocationRaws[iRaw].users = [
              ...allocationRaws[iRaw].users,
              {
                value: allocation.User.id,
                label: allocation.User.name
              }
            ];
            isNoAdd = false;
            break;
          }
        }
        if (isNoAdd) {
          allocationRaws = [
            ...allocationRaws,
            {
              users: [{ value: allocation.User.id, label: allocation.User.name }],
              type: allocation.labels
                ? { value: allocation.labels.id, label: allocation.labels.name.toUpperCase() }
                : null,
              effort: allocation.effortPercent,
              startDate,
              endDate
            }
          ];
        }
      }
    });

    const groups = [];
    allocationRaws.forEach(allocationRaw => {
      if (groups.length === 0) {
        groups.push(allocationRaw);
      } else {
        let isAdd = true;
        for (let i = 0; i < groups.length; i++) {
          let isExistUser = false;
          groups[i].users.forEach(user => {
            if (user.value === (allocationRaw.users[0] && allocationRaw.users[0].value)) {
              isExistUser = true;
            }
          });

          if (
            (allocationRaw.type && allocationRaw.type.value) === (groups[i].type && groups[i].type.value) &&
            allocationRaw.effort === groups[i].effort &&
            !isExistUser
          ) {
            groups[i].users.push(allocationRaw.users[0]);
            isAdd = false;
            break;
          } else if (
            (allocationRaw.type && allocationRaw.type.value) === (groups[i].type && groups[i].type.value) &&
            allocationRaw.effort === groups[i].effort
          ) {
            isAdd = false;
          }
        }
        if (isAdd) {
          groups.push(allocationRaw);
        }
      }
    });

    return groups;
  };

  const startDate = sprint && sprint.startDate;
  const endDate = sprint && sprint.endDate;
  const suggestions = tags.map(suggestion => ({ id: suggestion, text: suggestion }));

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'margin-top-15px'}>
          <div className="date-time-area">
            <div className="datetime">
              <FieldInput
                type={'text'}
                defaultValue={formatDateTime(startDate, 'DD/MM/YYYY')}
                placeholder={'start_date'}
                className="d-flex mb-4"
                name="startDate"
                disabled={true}
              />
              <FieldInput
                type={'text'}
                defaultValue={formatDateTime(endDate, 'DD/MM/YYYY')}
                placeholder={'end_date'}
                className="d-flex"
                name="end_date"
                disabled={true}
              />
            </div>
            <Text className="font-size-15px sprint-duration">
              {t('sprint_duration')}: {DurationTime(startDate, moment(endDate).add(1, 'second'))}
            </Text>
          </div>
          <div className="dialog-sprint-content-col">
            <div className="col-left">
              <div className="d-flex justify-content-center head-left mb-3">
                <Text className={'font-size-15px'}>{t('Tasks')}</Text>
                <div className="clear-flex" />
                <Text className={'font-size-15px'}>
                  {t('workload')}: {toFixed(workloadTasks(sprint && sprint.tasks), 2)}h
                </Text>
              </div>
              <div className="task-area-col" ref={wrapperRef}>
                <div className="task-area-col-main">
                  {sprint && sprint.tasks
                    ? computedTasks(sprint.tasks).map((group, gIndex) => (
                        <div key={gIndex} className="mb-4">
                          <h6 className="mb-3 pb-1 border-bottom">
                            {group.group ? group.group.toUpperCase() : 'nogroup'.toUpperCase()} - {group.totalEstimate}h
                          </h6>
                          {group.tasks.map((task, tIndex) => (
                            <TaskCard
                              key={tIndex}
                              task={task}
                              onUpdate={() => handleOpenUpdateTask(task.id)}
                              onRemove={e => handleRemoveTaskToBacklog(e, task)}
                            />
                          ))}
                        </div>
                      ))
                    : null}
                  {isUpdateTask && (
                    <TaskDialog
                      isOpen={isUpdateTask}
                      editMod={true}
                      title={t('update_task')}
                      suggestions={suggestions}
                      onClose={handleCloseDialog}
                      onDelete={handleDeleteBacklogTask}
                      onOK={handleUpdateBacklogTask}
                    />
                  )}
                </div>
                <div className={styles.wrapAddTask}>
                  <input
                    className={`form-control filter-task ${styles.inputTask}`}
                    name="task"
                    placeholder={t('type_to_search_or_click_for_all')}
                    onFocus={e => filterTask(e)}
                    onChange={e => filterTask(e)}
                  />
                  <div className="action-btn">
                    <Button
                      outline
                      title={t('remove_all_task_with_state_untouched_to_backlog')}
                      className="mr-1"
                      color="danger"
                      size="sm"
                      onClick={removeAllTaskToBacklog}
                    >
                      <i className={'fa fa-arrow-circle-o-down'} /> {t('remove_all')}
                    </Button>
                    <Button
                      outline
                      title={t('add_all_task_backlog_into_sprint')}
                      color="primary"
                      size="sm"
                      onClick={handleAddAllTask}
                    >
                      <i className={'fa fa-arrow-circle-o-up'} /> {t('add_all')}
                    </Button>
                  </div>
                </div>
                {searchBacklog.length > 0 && autoCompleteTask ? (
                  <div className="auto-complete">
                    {searchBacklog.map((task, index) => {
                      const tags = parseJSON(task.tag);
                      return (
                        <TaskSuggestCard key={index} tags={tags} task={task} onClick={() => handleAddTask(task)} />
                      );
                    })}
                  </div>
                ) : null}
              </div>
            </div>
            <div className="col-right">
              <div className="d-flex justify-content-center head-right mb-3">
                <Text className={'font-size-15px'}>{t('Allocations')}</Text>
                <div className="clear-flex" />
                <Text className={'font-size-15px'}>
                  {t('workforce')}: {toFixed(workloadAllocations(sprint && sprint.allocations), 2)}h
                </Text>
              </div>
              <div className="allocation-area-col">
                <div className="allocation-area-col-main">
                  {sprint && sprint.tasks && sprint.allocations && allocationLabels
                    ? computedAllocations(sprint.allocations, sprint.tasks, allocationLabels).map((group, gIndex) => (
                        <Fragment key={gIndex}>
                          {calculateLength(group.allocations) || group.totalTimeCurrent ? (
                            <div className="mb-4">
                              <h6 className="mb-3 pb-1 border-bottom">
                                {`${group.group.name.toUpperCase()} - ${group.totalTimeCurrent}/${toFixed(
                                  workloadAllocations(group.allocations),
                                  2
                                )}h`}
                              </h6>
                              {group.allocations.map((allocation, aIndex) => (
                                <AllocationCard
                                  id={aIndex}
                                  key={aIndex}
                                  onUpdate={() => handleOpenUpdateAllocation(allocation)}
                                  allocation={allocation}
                                  onRemove={e => {
                                    setOpenAlertConfirmDeleteAllocation(true);
                                    setSelectedAllocation(allocation);
                                    e.stopPropagation();
                                  }}
                                  currentProjectId={currentProjectId}
                                  appPermissions={appPermissions}
                                />
                              ))}
                            </div>
                          ) : null}
                        </Fragment>
                      ))
                    : null}
                </div>

                {openAllocationDialog && (
                  <AddAllocation
                    isOpen={openAllocationDialog}
                    users={usersOfProject}
                    allocationLabels={allocationLabels}
                    oldAllocationRaws={
                      !calculateLength(allocations) && calculateLength(oldAllocations)
                        ? computedOldAllocationRaws(oldAllocations)
                        : []
                    }
                    status={status}
                    close={closeAddAllocationDialog}
                    addAllocation={handleAddAllocation}
                    allocations={allocations}
                    startDate={sprint && sprint.startDate}
                    endDate={sprint && sprint.endDate}
                  />
                )}

                {isUpdateAllocation && (
                  <UpdateAllocation
                    isOpen={isUpdateAllocation}
                    users={usersOfProject}
                    allocationLabels={allocationLabels}
                    allocationSelected={allocationSelected}
                    close={handleCloseUpdateAllocation}
                    _updateAllocation={_updateAllocation}
                    status={status}
                  />
                )}
              </div>
              <ConfirmationModal
                isOpen={openAlertConfirmDeleteAllocation}
                txtTitle={t('confirm_remove')}
                txtCancel={t('cancel')}
                txtConfirm={t('confirm')}
                funConfirm={() => handleRemoveAllocation(selectedAllocation)}
                funClose={() => setOpenAlertConfirmDeleteAllocation(false)}
              >
                {t('are_you_sure_remove_this_allocation')}
              </ConfirmationModal>
              <div className="area-create-allocation">
                {isAllowPermission(appPermissions, CREATE_ALLOCATION, currentProjectId) && (
                  <Button
                    disabled={!currentSprintId}
                    className="btn-add-allocation color-base"
                    onClick={openAddAllocationDialog}
                  >
                    <i className="fa fa-plus" />
                  </Button>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
}

SprintManagement.propTypes = {
  sprint: any,
  searchBacklogTask: func,
  currentProjectId: any,
  currentSprintId: any,
  status: any,
  searchBacklog: array,
  usersOfProject: array,
  removeTaskToBacklog: func,
  addTaskToSprint: func,
  getDetailSprint: func,
  removeAllocation: func,
  addAllocation: func,
  getUsersOfProject: func,
  setKeySearchTask: func,
  appPermissions: array,
  allocationLabels: array,
  currentTask: object,
  _clearCurrentTask: func,
  _getDetailTask: func,
  _deleteTask: func,
  _updateBacklogTask: func,
  _updateAllocation: func,
  tags: array,
  allocations: array,
  oldAllocations: array
};

export default memo(SprintManagement);
