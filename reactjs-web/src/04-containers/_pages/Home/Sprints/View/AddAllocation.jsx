import Select from 'react-select';
import DatePicker from 'react-datepicker';
import styles from './AddAllocation.module.scss';
import React, { useState, useEffect, memo } from 'react';
import { bool, func, array, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import {
  ADD_ALLOCATION_SUCCESS,
  ADD_ALLOCATION_FAILED
  // ADD_ALLOCATION_PROGRESS
} from '../../../../../07-constants/sprint';
import {
  formatDateTime,
  getDateAt000000ZeroGMT,
  getDateAt235959ZeroGMT,
  calculateLength
} from '../../../../../08-helpers/common';

const formatAllocationRawsToSend = allocationRaws => {
  let allocations = [];

  allocationRaws.forEach(
    allocationRaw =>
      allocationRaw.users &&
      allocationRaw.users.forEach(user => {
        allocations = [
          ...allocations,
          {
            userId: user.value,
            label: allocationRaw.type && allocationRaw.type.value,
            effortPercent: allocationRaw.effort,
            startDate: allocationRaw.startDate,
            endDate: allocationRaw.endDate
          }
        ];
      })
  );
  return allocations;
};

function AddAllocation({
  isOpen,
  users,
  allocationLabels,
  oldAllocationRaws,
  status,
  close,
  addAllocation,
  startDate,
  endDate,
  allocations
}) {
  const [allocationRaws, setAllocationRaws] = useState([
    {
      users: null,
      type: null,
      effort: null,
      startDate,
      endDate
    }
  ]);

  useEffect(() => {
    if (status === ADD_ALLOCATION_SUCCESS) {
      setAllocationRaws([
        {
          users: null,
          type: null,
          effort: null,
          startDate,
          endDate
        }
      ]);
      close();
    }
    if (status === ADD_ALLOCATION_FAILED) {
      setAllocationRaws(allocationRaws);
    }
  }, [status]);

  // did mount and unmount
  useEffect(() => {
    if (!calculateLength(allocations)) {
      setAllocationRaws(oldAllocationRaws);
    }
    return () => {
      setAllocationRaws([
        {
          users: null,
          type: null,
          effort: null,
          startDate,
          endDate
        }
      ]);
    };
  }, []);

  // on change oldAllocationRaws
  // useEffect(() => {
  //   if (!calculateLength(allocations) && status !== ADD_ALLOCATION_PROGRESS && status !== ADD_ALLOCATION_FAILED) {
  //     setAllocationRaws(oldAllocationRaws);
  //   }
  // }, [oldAllocationRaws]);

  const handleChangeAllocation = (index, valueField, nameField) => {
    const newAllocationRaw = allocationRaws.map((allocation, i) => {
      if (i === index)
        return {
          ...allocation,
          [nameField]: valueField
        };
      return allocation;
    });
    setAllocationRaws(newAllocationRaw);
  };

  const handleAddAllocationRaw = () => {
    const newAllocationRaw = [
      ...allocationRaws,
      {
        users: null,
        type: null,
        effort: null,
        startDate,
        endDate
      }
    ];
    setAllocationRaws(newAllocationRaw);
  };

  const handleRemoveAllocationRaw = index => {
    const newAllocationRaw = allocationRaws.filter((allocation, i) => i !== index);
    setAllocationRaws(newAllocationRaw);
  };

  const handleAddAllocation = () => {
    const data = formatAllocationRawsToSend(allocationRaws);
    if (calculateLength(data)) addAllocation(data);
  };

  const userOptions = users.map(user => ({ value: user.User.id, label: user.User.name }));
  const allocationOptions = allocationLabels
    .map(label => ({ value: label.group.id, label: label.group.name }))
    .filter(label => label.label.toUpperCase() !== 'NOGROUP');

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} centered size="xl" fade={false} backdrop="static" toggle={close}>
          <ModalHeader toggle={close}>{t('add_allocation')}</ModalHeader>
          <ModalBody>
            {calculateLength(allocationRaws)
              ? allocationRaws.map((allocation, i) => (
                  <Row key={`key-${i}`} className={`mb-2 ${styles.row2}`}>
                    <Col className="px-2 mb-3">
                      <Select
                        className={styles.selectUser}
                        placeholder={t('select_users')}
                        isMulti
                        closeMenuOnSelect={false}
                        options={userOptions}
                        value={allocation.users}
                        onChange={users => handleChangeAllocation(i, users, 'users')}
                      />
                    </Col>
                    <Select
                      className={`mx-2 mb-3 ${styles.selectType}`}
                      placeholder={t('select_group')}
                      options={allocationOptions}
                      value={allocation.type}
                      onChange={type => handleChangeAllocation(i, type, 'type')}
                    />
                    <Input
                      className={`mx-2 mb-3 focus-border ${styles.inputEffort}`}
                      type="number"
                      placeholder={t('effort')}
                      value={allocation.effort || ''}
                      onChange={e => handleChangeAllocation(i, Number(e.target.value), 'effort')}
                    />
                    <DatePicker
                      dropdownMode={'scroll'}
                      customInput={<Input />}
                      className={`mx-2 mb-3 ${styles.inputDate}`}
                      placeholderText={t('start_date')}
                      value={formatDateTime(allocation.startDate, 'DD/MM/YYYY')}
                      onChange={startDate =>
                        handleChangeAllocation(i, startDate ? getDateAt000000ZeroGMT(startDate) : null, 'startDate')
                      }
                    />
                    <DatePicker
                      dropdownMode={'scroll'}
                      customInput={<Input />}
                      className={`mx-2 mb-3 ${styles.inputDate}`}
                      placeholderText={t('end_date')}
                      value={formatDateTime(allocation.endDate, 'DD/MM/YYYY')}
                      onChange={endDate =>
                        handleChangeAllocation(i, endDate ? getDateAt235959ZeroGMT(endDate) : null, 'endDate')
                      }
                    />
                    <div
                      className={`d-flex align-items-center mx-2 mb-3 text-warning ${styles.btnRmAlc}`}
                      onClick={() => handleRemoveAllocationRaw(i)}
                    >
                      <i className="fa fa-times-circle" />
                    </div>
                  </Row>
                ))
              : null}
            <div className="d-flex justify-content-center">
              <div className={'cursor-pointer'} onClick={handleAddAllocationRaw}>
                {/* eslint-disable-next-line */}
                <a className={`fa fa-plus-circle ${styles.btnAddAlc} text-success`} />
              </div>
            </div>
          </ModalBody>
          <ModalFooter className="d-flex justify-content-between">
            <Button className={'outline-base-btn'} size={'sm'} onClick={close}>
              {t('cancel')}
            </Button>
            <Button
              className={'base-btn'}
              disabled={!calculateLength(formatAllocationRawsToSend(allocationRaws))}
              size={'sm'}
              onClick={handleAddAllocation}
            >
              {t('add')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

AddAllocation.propTypes = {
  isOpen: bool,
  users: array,
  allocationLabels: array,
  oldAllocationRaws: array,
  status: string,
  close: func,
  addAllocation: func,
  startDate: string,
  endDate: string,
  allocations: array
};

export default memo(AddAllocation);
