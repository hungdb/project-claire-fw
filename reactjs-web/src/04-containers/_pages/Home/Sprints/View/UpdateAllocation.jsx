import styles from './UpdateAllocation.module.scss';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import React, { memo, useEffect, useState } from 'react';
import { array, bool, func, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { UPDATE_ALLOCATION_SUCCESS } from '../../../../../07-constants/sprint';
import { formatDateTime, getDateAt000000ZeroGMT, getDateAt235959ZeroGMT } from '../../../../../08-helpers/common';

function UpdateAllocation({ isOpen, users, allocationLabels, allocationSelected, status, close, _updateAllocation }) {
  const [isClose, setIsClose] = useState(false);
  const [initialData, setInitialData] = useState({});
  const [isBtnUpdateDisable, setIsBtnUpdateDisable] = useState(true);
  const [userOptions, setUserOptions] = useState([]);
  const [allocationOptions, setAllocationOptions] = useState([]);
  const [user, setUser] = useState(null);
  const [type, setType] = useState(null);
  const [effort, setEffort] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  useEffect(() => {
    const userOptions = users.map(user => ({ value: user.User.id, label: user.User.name }));
    const user = userOptions.find(user => user.value === allocationSelected.userId) || null;
    const allocationOptions = allocationLabels
      .map(label => ({ value: label.group.id, label: label.group.name }))
      .filter(label => label.label.toUpperCase() !== 'NOGROUP');
    const type = allocationOptions.find(alc => alc.value === allocationSelected.label) || null;

    setUserOptions(userOptions);
    setAllocationOptions(allocationOptions);
    setUser(user);
    setType(type);
    setEffort(allocationSelected.effortPercent);
    setStartDate(allocationSelected.startDate);
    setEndDate(allocationSelected.endDate);
    setInitialData({
      type,
      effort: allocationSelected.effortPercent,
      startDate: allocationSelected.startDate,
      endDate: allocationSelected.endDate
    });
  }, []);

  useEffect(() => {
    if (isClose && status === UPDATE_ALLOCATION_SUCCESS) {
      close();
    }
  }, [status]);

  useEffect(() => {
    if (
      (type && initialData.type && type.value !== initialData.type.value) ||
      (type !== null && initialData.type === null) ||
      effort !== initialData.effort ||
      new Date(startDate).getTime() !== new Date(initialData.startDate).getTime() ||
      new Date(endDate).getTime() !== new Date(initialData.endDate).getTime()
    ) {
      if (isBtnUpdateDisable) {
        setIsBtnUpdateDisable(false);
      }
    } else if (!isBtnUpdateDisable) {
      setIsBtnUpdateDisable(true);
    }
  });

  const handleUpdateAllocation = () => {
    const value = type ? type.value : null;

    _updateAllocation({
      allocationId: allocationSelected.id,
      label: value,
      effortPercent: effort,
      startDate,
      endDate
    });
    setIsClose(true);
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal isOpen={isOpen} centered fade={false} backdrop="static" toggle={close}>
          <ModalHeader toggle={close}>Update allocation</ModalHeader>
          <ModalBody className="pb-2">
            <Row className={styles.row2}>
              <Col xs={12} className="px-2 mb-3">
                <Select
                  className="w-100"
                  placeholder={t('select_user')}
                  isDisabled
                  options={userOptions}
                  value={user}
                  onChange={user => setUser(user)}
                />
              </Col>
              <Col xs={6} className="px-2 mb-3">
                <Select
                  className="w-100"
                  placeholder={t('select_group')}
                  options={allocationOptions}
                  value={type}
                  onChange={type => setType(type)}
                />
              </Col>

              <Col xs={6} className="px-2 mb-3">
                <Input
                  className="w-100"
                  type="number"
                  placeholder={t('effort')}
                  value={effort || ''}
                  onChange={e => setEffort(Number(e.target.value))}
                  onFocus={e => e.target.select()}
                />
              </Col>

              <Col xs={6} className="px-2 mb-3">
                <DatePicker
                  dropdownMode={'scroll'}
                  customInput={<Input />}
                  className="w-100"
                  placeholderText={t('start_date')}
                  value={formatDateTime(startDate, 'DD/MM/YYYY')}
                  onChange={startDate => setStartDate(startDate ? getDateAt000000ZeroGMT(startDate) : null)}
                  onFocus={e => e.target.select()}
                />
              </Col>

              <Col xs={6} className="px-2 mb-3">
                <DatePicker
                  dropdownMode={'scroll'}
                  customInput={<Input />}
                  className="w-100"
                  placeholderText={t('end_date')}
                  value={formatDateTime(endDate, 'DD/MM/YYYY')}
                  onChange={endDate => setEndDate(endDate ? getDateAt235959ZeroGMT(endDate) : null)}
                  onFocus={e => e.target.select()}
                />
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter className="d-flex justify-content-between">
            <Button className={'outline-base-btn'} size={'sm'} onClick={close}>
              {t('cancel')}
            </Button>
            <Button className={'base-btn'} size={'sm'} onClick={handleUpdateAllocation} disabled={isBtnUpdateDisable}>
              {t('Update')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
}

UpdateAllocation.propTypes = {
  isOpen: bool,
  users: array,
  allocationLabels: array,
  allocationSelected: object,
  status: string,
  close: func,
  _updateAllocation: func
};

export default memo(UpdateAllocation);
