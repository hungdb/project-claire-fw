import React, { memo } from 'react';
import { func, object, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Badge } from 'reactstrap';
import { calculateLength } from '../../../../../08-helpers/common';

const TaskSuggestCard = ({ onClick, task, tags }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <div className="task-area-col-main-search" onClick={onClick}>
        <div className="task-card">
          <span>{`#${task.taskCode ? task.taskCode : task.id} ${task.content}`}</span>
          <br />
          {calculateLength(tags) ? (
            <span className={'mr-1'}>
              <i className={'fa fa-tags font-size-13'} />
            </span>
          ) : null}
          {calculateLength(tags)
            ? tags.map((tag, index) => (
                <Badge key={index} color="success" className={'mb-1 mr-1'}>
                  {tag}
                </Badge>
              ))
            : null}
          <span>{task.estimate}h</span>
        </div>
      </div>
    )}
  </NamespacesConsumer>
);

TaskSuggestCard.propTypes = {
  onClick: func,
  task: object,
  tags: array
};

TaskSuggestCard.defaultProps = {};

export default memo(TaskSuggestCard);
