import React, { memo } from 'react';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import { number, func, object, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { formatDateTime, isAllowPermission, toFixed, promiseDelayImport } from '../../../../../08-helpers/common';
import { workloadAllocation } from '../../../../../05-utils/commonUtils';
import { REMOVE_ALLOCATION } from '../../../../../07-constants/app-permission';

const UserAvatar = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/UserAvatar')),
  <Skeleton circle={true} width={40} height={40} />
);

const Text = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Text')),
  <Skeleton width={155.33} />
);

const AllocationCard = ({ id, onUpdate, appPermissions, allocation, currentProjectId, onRemove }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <div className="d-flex justify-content-center allocation-card cursor-pointer" onClick={onUpdate}>
        <div className="col-3 col-md-3 flex-column d-flex justify-content-center">
          <UserAvatar
            className="avatar"
            id={`${id}`}
            user={allocation.User}
            src={`/profile/${allocation.User && allocation.User.id}`}
          />
        </div>
        <div className="col-9 col-md-9 pt-1 pb-1">
          <Text>
            <span>@{allocation.User && allocation.User.name}</span>
            <span>
              {' '}
              ({toFixed(workloadAllocation(allocation.startDate, allocation.endDate, allocation.effortPercent), 2)}
              h)
            </span>
          </Text>
          <div className="more-infor">
            <span>{allocation.effortPercent || 0}%</span> &nbsp;
            <span>{formatDateTime(allocation.startDate, 'DD/MM/YYYY')}</span> -{' '}
            <span>{formatDateTime(allocation.endDate, 'DD/MM/YYYY')}</span>
          </div>
        </div>
        {isAllowPermission(appPermissions, REMOVE_ALLOCATION, currentProjectId) && (
          <span className="btn-remove" onClick={e => onRemove(e)}>
            x
          </span>
        )}
      </div>
    )}
  </NamespacesConsumer>
);

AllocationCard.propTypes = {
  onUpdate: func,
  appPermissions: array,
  allocation: object,
  currentProjectId: number,
  onRemove: func,
  id: number
};

AllocationCard.defaultProps = {};

export default memo(AllocationCard);
