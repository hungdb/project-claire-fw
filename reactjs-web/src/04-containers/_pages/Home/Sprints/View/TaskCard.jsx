import React, { memo } from 'react';
import styles from './SprintManagement.module.scss';
import { func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Badge } from 'reactstrap';

const TaskCard = ({ onUpdate, onRemove, task }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <div className={`task-card ${styles.task} pb-1`} onClick={onUpdate}>
        <p className="mb-0">{`#${task.taskCode ? task.taskCode : task.id} ${task.content}`}</p>
        <Badge className="mr-1" color="info">
          {task.WorkflowState && task.WorkflowState.name}
        </Badge>
        <Badge color="primary">
          <i className={'fa fa-clock-o'} /> {task.estimate}h
        </Badge>
        <span className="btn-remove" onClick={e => onRemove(e)}>
          x
        </span>
      </div>
    )}
  </NamespacesConsumer>
);

TaskCard.propTypes = {
  onUpdate: func,
  task: object,
  onRemove: func
};

TaskCard.defaultProps = {};

export default memo(TaskCard);
