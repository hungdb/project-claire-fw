import React, { memo } from 'react';
import { Button, ButtonGroup } from 'reactstrap';
import { func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { BOARD_TAB } from '../../../../../05-utils/commonData';

const ButtonSwitchBoard = ({ activeTabBoard, onClick }) => (
  <NamespacesConsumer>
    {t => (
      <ButtonGroup className="button-group-tab">
        {Object.values(BOARD_TAB).map(tab => (
          <Button
            key={tab.value}
            onClick={() => onClick(tab.value)}
            active={activeTabBoard === tab.value}
            className="tabs-btn"
          >
            {t(tab.label)}
          </Button>
        ))}
      </ButtonGroup>
    )}
  </NamespacesConsumer>
);

ButtonSwitchBoard.propTypes = {
  onClick: func,
  activeTabBoard: number
};

ButtonSwitchBoard.defaultProps = {};

export default memo(ButtonSwitchBoard);
