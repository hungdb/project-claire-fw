import TabBoard from './TabBoard';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsersByProject } from '../../../../../01-actions/sprint';
import {
  getDetailTask,
  statisticalActiveSprint,
  getWorkflowByProject,
  moveTask,
  clearCurrentTask,
  getUsersOfTask,
  setTypeGetTask,
  setActiveTabBoard,
  filterUserOnBoard,
  clearFilterUserOnBoard
} from '../../../../../01-actions/sprint';

const mapStateToProps = store => ({
  status: store.sprint.status,
  appPermissions: store.auth.permissions,
  activeSprint: store.sprint.activeSprint,
  workflowProject: store.sprint.workflowProject,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  typeGetTask: store.sprint.typeGetTask,
  activeTabBoard: store.sprint.activeTabBoard,
  usersIntoProject: store.sprint.usersIntoProject,
  selectedUserBoard: store.sprint.selectedUserBoard,
  toggleFullBoard: store.common.toggleFullBoard
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getDetailTask,
      statisticalActiveSprint,
      getWorkflowByProject,
      moveTask,
      clearCurrentTask,
      getUsersOfTask,
      setTypeGetTask,
      setActiveTabBoard,
      getUsersByProject,
      filterUserOnBoard,
      clearFilterUserOnBoard
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabBoard);
