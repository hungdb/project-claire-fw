import TaskCard from '../../../../../03-components/TaskCard';
import Board from 'react-trello';
import React, { memo } from 'react';
import { array, func } from 'prop-types';
import LaneHeader from '../../../../../03-components/LaneHeader';

const SprintBoard = ({ workflowProject, handleDragEnd, handleDetailTask }) => (
  <Board
    draggable
    laneDraggable={false}
    editable={true}
    hideCardDeleteIcon
    cardDragClass="draggingCard"
    data={{ lanes: workflowProject }}
    handleDragEnd={handleDragEnd}
    onCardClick={handleDetailTask}
    customCardLayout
    customLaneHeader={<LaneHeader />}
    className="board"
  >
    <TaskCard />
  </Board>
);

SprintBoard.propTypes = {
  workflowProject: array,
  handleDragEnd: func,
  handleDetailTask: func
};

SprintBoard.defaultProps = {};

export default memo(SprintBoard);
