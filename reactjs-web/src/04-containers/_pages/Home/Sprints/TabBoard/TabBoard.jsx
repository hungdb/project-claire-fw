import i18n from '../../../../../i18n';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import ZoomBoard from '../../../../../03-components/ZoomBoard';
import React, { memo, useState, useEffect, useMemo } from 'react';
import { string, array, object, number, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { MOVE_TASK_SUCCESS, MOVE_TASK_FAILED } from '../../../../../07-constants/sprint';
import { isAllowPermission, promiseDelayImport, getOptionUserRelate } from '../../../../../08-helpers/common';
import { TYPE_GET_TASK, BOARD_TAB, TOAST_ERROR_STYLE, STATUS_FULL_BOARD } from '../../../../../05-utils/commonData';
import {
  LIST_WORKFLOW_STATE_BY_PROJECT,
  PROGRESS_SPRINT_ACTIVE_BY_PROJECT,
  PERM_FILTER_USER_ON_BOARD
} from '../../../../../07-constants/app-permission';
import SelectUser from '../../../../../03-components/SelectPropsBox';

const Board = LazyComponent(promiseDelayImport(import('./Board'), 2000), <SpinLoader />);
const DetailTaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/detail-task-dialog'));
const Loader = LazyComponent(() => import('../../../../../03-components/Loader'));
const SprintProgress = LazyComponent(() => import('../../../../../03-components/SprintProgress'));
const Text = LazyComponent(promiseDelayImport(import('../../../../../03-components/Text')), <Skeleton width={70} />);
const Model = LazyComponent(promiseDelayImport(import('../../../../../03-components/ModelInfo')));
const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);

const ButtonSwitchBoard = LazyComponent(
  promiseDelayImport(import('./ButtonSwitchBoard')),
  <Skeleton width={235} height={33} />
);

let sprintsBoardCaches = null;

function TabBoard({
  status,
  appPermissions,
  activeSprint,
  workflowProject,
  projectId,
  getDetailTask,
  statisticalActiveSprint,
  getWorkflowByProject,
  moveTask,
  clearCurrentTask,
  setTypeGetTask,
  history,
  match,
  typeGetTask,
  activeTabBoard,
  setActiveTabBoard,
  usersIntoProject,
  getUsersByProject,
  filterUserOnBoard,
  clearFilterUserOnBoard,
  selectedUserBoard,
  toggleFullBoard
}) {
  const [loader, setLoader] = useState(false);
  const [showDetailTask, setShowDetailTask] = useState(false);
  const [taskId, setTaskId] = useState(null);

  useEffect(() => {
    const projectId = Number(match.params.projectId);
    const filterType = Number(match.params.filterType);
    const taskId = Number(match.params.taskId);
    const stringUrl = localStorage.getItem('sprintsBoardCaches');

    if (stringUrl) {
      const urls = JSON.parse(stringUrl);
      sprintsBoardCaches = new Map(urls);
      for (const url of urls) {
        if (url[0] === projectId) {
          history.push(`/sprints/${projectId}/board/${filterType ? filterType : url[1]}`);
          if (url[1] === 2) {
            getWorkflowByProject({ projectId, type: TYPE_GET_TASK.ALL });
            setTypeGetTask(TYPE_GET_TASK.ALL);
          } else {
            getWorkflowByProject({ projectId, type: TYPE_GET_TASK.MY_TASK });
            setTypeGetTask(TYPE_GET_TASK.MY_TASK);
          }
          setActiveTabBoard(url[1]);
          break;
        }
      }
    } else {
      sprintsBoardCaches = new Map();
      history.push(`/sprints/${projectId}/board/${filterType ? filterType : 1}`);
      setActiveTabBoard(filterType ? filterType : 1);
    }
    if (filterType === 2) {
      getWorkflowByProject({ projectId, type: TYPE_GET_TASK.ALL });
      setTypeGetTask(TYPE_GET_TASK.ALL);
    } else {
      getWorkflowByProject({ projectId, type: TYPE_GET_TASK.MY_TASK });
      setTypeGetTask(TYPE_GET_TASK.MY_TASK);
    }

    if (projectId && filterType && taskId) {
      clearCurrentTask();
      getDetailTask({ taskId, projectId });
      setShowDetailTask(true);
      setTaskId(taskId);
    }
  }, []);

  useEffect(() => {
    const projectId = Number(match.params.projectId);
    const stringUrl = localStorage.getItem('sprintsBoardCaches');
    if (stringUrl) {
      const urls = JSON.parse(stringUrl);
      for (const url of urls) {
        if (url[0] === projectId) {
          history.push(`/sprints/${projectId}/board/${url[1]}`);
          if (url[1] === 2) {
            getWorkflowByProject({ projectId, type: TYPE_GET_TASK.ALL });
            getUsersByProject({ projectId });
            setTypeGetTask(TYPE_GET_TASK.ALL);
          } else {
            getWorkflowByProject({ projectId, type: TYPE_GET_TASK.MY_TASK });
            setTypeGetTask(TYPE_GET_TASK.MY_TASK);
          }
          setActiveTabBoard(url[1]);
          break;
        }
      }
    }
  }, [projectId]);

  const handleDetailTask = cardId => {
    history.push(`/sprints/${projectId}/board/${activeTabBoard}/${cardId}`);
    clearCurrentTask();
    getDetailTask({ taskId: Number(cardId), projectId });
    setShowDetailTask(true);
    setTaskId(Number(cardId));
  };

  useEffect(() => {
    const { taskId } = match && match.params && match.params;
    if (taskId && !showDetailTask) {
      handleDetailTask(taskId);
    }
  }, [match]);

  useEffect(() => {
    switch (status) {
      case MOVE_TASK_SUCCESS:
      case MOVE_TASK_FAILED:
        setLoader(false);
        statisticalActiveSprint({ projectId });
        getUsersByProject({ projectId });
        getWorkflowByProject({
          projectId,
          type: typeGetTask
        });
        break;
      default:
        break;
    }
  }, [status]);

  const changeTabBoard = index => {
    history.push(`/sprints/${projectId}/board/${index}`);
    sprintsBoardCaches.set(projectId, index);
    localStorage.setItem('sprintsBoardCaches', JSON.stringify([...sprintsBoardCaches]));
    if (index === BOARD_TAB.MY_TASK.value) {
      getWorkflowByProject({ projectId, type: TYPE_GET_TASK.MY_TASK });
      setTypeGetTask(TYPE_GET_TASK.MY_TASK);
      clearFilterUserOnBoard();
    } else if (index === BOARD_TAB.ALL.value) {
      getWorkflowByProject({ projectId, type: TYPE_GET_TASK.ALL, assignee: selectedUserBoard });
      setTypeGetTask(TYPE_GET_TASK.ALL);
    }
    statisticalActiveSprint({ projectId });
    getUsersByProject({ projectId });
    setActiveTabBoard(index);
  };

  const handleDragEnd = (cardId, sourceLaneId, targetLaneId, position, cardDetails) => {
    history.push(`/sprints/${projectId}/board/${activeTabBoard}`);
    if (sourceLaneId !== targetLaneId) {
      setLoader(true);
      if (!cardId || !sourceLaneId || !targetLaneId || position === undefined || !cardDetails) {
        toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
        setLoader(false);
      } else {
        moveTask({
          cardId,
          sourceLaneId,
          targetLaneId,
          position,
          cardDetails,
          projectId: cardDetails.projectId
        });
      }
    }
  };

  const closeTask = () => {
    history.push(`/sprints/${projectId}/board/${activeTabBoard}`);
    setShowDetailTask(false);
    setTaskId(null);
  };

  const onDirectToTask = taskId => setTimeout(() => handleDetailTask(taskId), 300);

  const userBoardOptions = useMemo(() => getOptionUserRelate(usersIntoProject), [usersIntoProject]);

  const handleFilterUser = e => {
    filterUserOnBoard(e && e.value);
    statisticalActiveSprint({ projectId });
    getWorkflowByProject({ projectId, type: TYPE_GET_TASK.ALL, assignee: e && e.value });
  };

  return (
    <NamespacesConsumer>
      {t => (
        <div>
          <div className={'pull-right'}>
            <Text>{activeSprint && activeSprint.name}</Text>
          </div>
          {toggleFullBoard === STATUS_FULL_BOARD.ZOOM_OUT &&
            isAllowPermission(appPermissions, PROGRESS_SPRINT_ACTIVE_BY_PROJECT, projectId) && (
              <div className="sprint-progress-area margin-bottom-40px">
                <div className={'d-flex'}>
                  <AppText type={'title-header'} className={'margin-right-10px'}>
                    {t('Sprint progress')}
                  </AppText>
                  <Model
                    id={1}
                    title={t('note_color')}
                    colorBtn={'text-info'}
                    label={t('helper')}
                    iconAwesome={'fa-info-circle'}
                    styleBtn={{ marginLeft: '8px', fontSize: '25px' }}
                    classes={'text-center'}
                  >
                    <div className="row">
                      <div
                        className={'col-md-4'}
                        style={{ width: '30px', height: '20px', backgroundColor: '#f79029' }}
                      />
                      <div className={'col-md-8'}>{t('day_off')}</div>
                    </div>
                    <div className="row">
                      <div
                        className={'col-md-4'}
                        style={{ width: '30px', height: '20px', backgroundColor: '#099eec' }}
                      />
                      <div className={'col-md-8'}>{t('completed')}</div>
                    </div>
                    <div className="row">
                      <div
                        className={'col-md-4'}
                        style={{ width: '30px', height: '20px', backgroundColor: '#bce1f4' }}
                      />
                      <div className={'col-md-8'}>{t('today')}</div>
                    </div>
                    <div className="row">
                      <div
                        className={'col-md-4'}
                        style={{ width: '30px', height: '20px', backgroundColor: '#f5f6f8' }}
                      />
                      <div className={'col-md-8'}>{t('available')}</div>
                    </div>
                  </Model>
                </div>
                {projectId && activeSprint ? (
                  <SprintProgress
                    completedTime={activeSprint.totalEstimateTaskCompleted}
                    totalTime={activeSprint.totalEstimateAllTasks}
                    startDate={activeSprint.startDate}
                    endDate={activeSprint.endDate}
                  />
                ) : (
                  <div className="text-center text-muted">{t('no_sprint_active')}</div>
                )}
              </div>
            )}

          {isAllowPermission(appPermissions, LIST_WORKFLOW_STATE_BY_PROJECT, projectId) && (
            <div className="sprint-board-area">
              <div className={'d-flex'}>
                <AppText type={'title-header'}>{t('Board')}</AppText>
                <ZoomBoard />
              </div>
              {activeSprint && (
                <div className={'d-flex justify-content-end margin-bottom-10px mt-4'}>
                  <ButtonSwitchBoard activeTabBoard={activeTabBoard} onClick={value => changeTabBoard(value)} />
                </div>
              )}
              {typeGetTask === TYPE_GET_TASK.ALL &&
                activeSprint &&
                isAllowPermission(appPermissions, PERM_FILTER_USER_ON_BOARD) && (
                  <div className={'d-flex justify-content-end'}>
                    <SelectUser
                      options={userBoardOptions}
                      value={selectedUserBoard}
                      onChange={handleFilterUser}
                      placeholder="select_user"
                      className={'margin-bottom-10px w-20p'}
                      isClearable={true}
                    />
                  </div>
                )}
              <div className="container-task-management">
                {projectId && activeSprint ? (
                  <Board
                    workflowProject={workflowProject}
                    handleDragEnd={handleDragEnd}
                    handleDetailTask={handleDetailTask}
                  />
                ) : (
                  <div className="text-center text-muted">{t('no_sprint_active')}</div>
                )}
              </div>
              <Loader isOpen={loader} />
            </div>
          )}
          {showDetailTask && (
            <DetailTaskDialog
              isOpen={showDetailTask}
              value={{
                taskId: taskId,
                projectId
              }}
              onClose={closeTask}
              onDirectToTask={taskId => onDirectToTask(taskId)}
            />
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabBoard.propTypes = {
  status: string,
  appPermissions: array,
  activeSprint: object,
  workflowProject: array,
  projectId: number,
  getDetailTask: func,
  statisticalActiveSprint: func,
  getWorkflowByProject: func,
  moveTask: func,
  clearCurrentTask: func,
  getUsersOfTask: func,
  setTypeGetTask: func,
  history: object,
  match: object,
  typeGetTask: number,
  activeTabBoard: number,
  setActiveTabBoard: func,
  usersIntoProject: array,
  getUsersByProject: func,
  filterUserOnBoard: func,
  clearFilterUserOnBoard: func,
  selectedUserBoard: number,
  toggleFullBoard: number
};

export default memo(TabBoard);
