import React, { memo } from 'react';
import { Button, ButtonGroup } from 'reactstrap';
import { array, number, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { SPRINT_TAB } from '../../../../05-utils/commonData';

const MenuLeft = ({ visibleTabs, activeTab, onClick }) => (
  <NamespacesConsumer>
    {t => (
      <ButtonGroup className="button-group-tab">
        {Object.values(SPRINT_TAB).map(
          tab =>
            (tab === SPRINT_TAB.SPRINT_BOARD || visibleTabs.includes(tab.value)) && (
              <Button
                key={tab.value}
                onClick={() => onClick(tab.value)}
                active={activeTab === tab.value}
                className="tabs-btn"
              >
                {t(tab.label)}
              </Button>
            )
        )}
      </ButtonGroup>
    )}
  </NamespacesConsumer>
);

MenuLeft.propTypes = {
  visibleTabs: array,
  activeTab: number,
  onClick: func
};

MenuLeft.defaultProps = {};

export default memo(MenuLeft);
