import './style.css';
import Skeleton from 'react-loading-skeleton';
import RenderRoutes from '../../../../09-routes/RenderRoutes';
import planImg from '../../../../06-assets/img/plan.svg';
import LazyComponent from '../../../../03-components/LazyComponent';
import React, { memo, useState, useEffect, Fragment } from 'react';
import { TYPE_GET_TASK } from '../../../../05-utils/commonData';
import { func, object, array, number, string } from 'prop-types';
import { matchPath } from 'react-router-dom';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission, calculateLength, promiseDelayImport } from '../../../../08-helpers/common';
import { SPRINT_TAB, SPRINT_THROUGHOUT_TAB, STATUS_FULL_BOARD } from '../../../../05-utils/commonData';
import {
  EVENT_NEW_COMMENT,
  EVENT_NOTIFICATION,
  EVENT_RELOAD_BOARD,
  EVENT_UPDATE_COMMENT,
  EVENT_DELETE_COMMENT,
  EVENT_DELETE_COMMENT_ISSUE,
  EVENT_NEW_COMMENT_ISSUE,
  EVENT_UPDATE_COMMENT_ISSUE
} from '../../../../07-constants/socket';
import {
  connectSocket,
  errorListener,
  subscribeEvent,
  removeListener,
  disconnectSocket
} from '../../../../08-helpers/socket';
import {
  GET_SPRINTS_BY_PROJECT,
  GET_SPRINTS_DIFF_COMPLETED_BY_PROJECT,
  LIST_WORKFLOW_STATE_BY_PROJECT,
  PROGRESS_SPRINT_ACTIVE_BY_PROJECT,
  LIST_ATTACHMENT_BY_PROJECT,
  LIST_DOCUMENT_WIKI,
  FILTER_TASKS_BY_PROJECT,
  PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT,
  CREATE_ALLOCATION,
  ADD_TASK_TO_SPRINT,
  SPRINT_PROGRESS_OF_MEMBER,
  GET_LIST_ISSUES,
  GET_ISSUE,
  CREATE_ISSUE,
  UPDATE_ISSUE,
  REMOVE_ISSUE,
  PERM_LIST_MY_PROJECTS,
  STATISTICAL_ISSUE,
  ADD_MY_SKILL_IN_SPRINT,
  ADD_USER_SKILL_IN_SPRINT,
  LOG_BEHAVIOR_SPRINT
} from '../../../../07-constants/app-permission';

const MenuLeft = LazyComponent(promiseDelayImport(import('./MenuLeft')), <Skeleton width={586} height={33} />);
const MenuRight = LazyComponent(promiseDelayImport(import('./MenuRight')), <Skeleton width={235} height={33} />);
const ProjectBullet = LazyComponent(
  promiseDelayImport(import('../Projects/ProjectBullet')),
  <Skeleton width={120} height={37} />
);

let sprintsProjectCaches = null;

function Sprint({
  getMyProjects,
  getSuggestTags,
  getSprints,
  selectedProject,
  statisticalActiveSprint,
  getWorkflowByProject,
  getSprintDiffCompleted,
  clearDetailSprint,
  clearDetailBacklogTask,
  clearBacklog,
  switchSprintProject,
  myProjects,
  addNotification,
  appPermissions,
  clearBoard,
  clearSprintsList,
  fireSocketAddComment,
  history,
  location,
  match,
  fireSocketUpdateComment,
  fireSocketDeleteComment,
  typeGetTask,
  setTypeGetTask,
  setActiveTabBoard,
  route,
  urlCache,
  fireSocketAddCommentIssue,
  fireSocketDeleteCommentIssue,
  fireSocketUpdateCommentIssue,
  clearListProjects,
  clearSprintsDiffCompleted,
  projectId,
  selectedUserBoard,
  toggleFullBoard
}) {
  const [visibleTabs, setVisibleTabs] = useState([]);
  const [activeTab, setActiveTab] = useState(SPRINT_TAB.SPRINT_BOARD.value);

  const reloadMyProjects = () => {
    clearListProjects();
    if (isAllowPermission(appPermissions, PERM_LIST_MY_PROJECTS)) {
      getMyProjects();
    }
  };

  const refreshUnmount = () => {
    clearDetailBacklogTask();
    clearBacklog();
    clearDetailSprint();
    clearSprintsList();
    clearSprintsDiffCompleted();
  };

  useEffect(() => {
    const socket = connectSocket();
    errorListener({ socket });
    subscribeEvent({
      socket,
      event: EVENT_RELOAD_BOARD,
      callback: () => {
        const projectId = selectedProject && selectedProject.id;
        if (projectId) {
          statisticalActiveSprint({ projectId });
          getWorkflowByProject({
            projectId,
            type: typeGetTask,
            assignee: selectedUserBoard
          });
        }
      }
    });
    subscribeEvent({
      socket,
      event: EVENT_NOTIFICATION,
      callback: res => {
        addNotification({ type: res.type, data: res.data });
        switch (res.type) {
          case EVENT_NEW_COMMENT:
            fireSocketAddComment({ data: res.data });
            break;
          case EVENT_UPDATE_COMMENT:
            fireSocketUpdateComment({ data: res.data });
            break;
          case EVENT_DELETE_COMMENT:
            fireSocketDeleteComment({ data: res.data });
            break;
          case EVENT_NEW_COMMENT_ISSUE:
            fireSocketAddCommentIssue({ data: res.data });
            break;
          case EVENT_UPDATE_COMMENT_ISSUE:
            fireSocketUpdateCommentIssue({ data: res.data });
            break;
          case EVENT_DELETE_COMMENT_ISSUE:
            fireSocketDeleteCommentIssue({ data: res.data });
            break;
          default:
            break;
        }
      }
    });

    const stringUrl = localStorage.getItem('sprintsProjectCaches');
    if (stringUrl) {
      sprintsProjectCaches = new Map(JSON.parse(stringUrl));
    } else {
      sprintsProjectCaches = new Map();
    }
    return () => {
      refreshUnmount();
      removeListener({ socket, events: [EVENT_RELOAD_BOARD, EVENT_NOTIFICATION] });
      disconnectSocket({ socket });
    };
  }, [projectId, typeGetTask, selectedUserBoard]);

  useEffect(() => {
    reloadMyProjects();
  }, [appPermissions]);

  const clearAll = () => {
    clearBoard();
    clearSprintsList();
    clearSprintsDiffCompleted();
  };

  const reloadPage = project => {
    const { Project } = project;
    if (Project || Project.id) {
      const allow = role => isAllowPermission(appPermissions, role, Project.id);
      if (allow(PROGRESS_SPRINT_ACTIVE_BY_PROJECT)) statisticalActiveSprint({ projectId: Number(Project.id) });
      if (allow(GET_SPRINTS_BY_PROJECT)) getSprints({ projectId: Project.id });
      if (allow(GET_SPRINTS_DIFF_COMPLETED_BY_PROJECT)) getSprintDiffCompleted({ projectId: Project.id });
    }
    clearAll();
  };

  useEffect(() => {
    const projectId = match && match.params && match.params.projectId;
    if (projectId && calculateLength(myProjects)) {
      myProjects.some(p => {
        if (Number(projectId) === p.id) {
          switchSprintProject(p);
          return true;
        }
        return false;
      });
      reloadPage({ Project: { id: projectId } });
      for (const [i, r] of Object.entries(route.routes)) {
        if (matchPath(location.pathname, r.path)) {
          setActiveTab(Number(i) + 1);
          break;
        }
      }
    } else {
      switchSprintProject(null);
    }
  }, [myProjects]);

  const changeTab = index => {
    if (!selectedProject) return;

    const projectId = selectedProject.id;
    switch (index) {
      case SPRINT_TAB.BACKLOG.value: {
        getSuggestTags(projectId);
        if (isAllowPermission(appPermissions, GET_SPRINTS_BY_PROJECT, projectId)) getSprints({ projectId });
        history.push(`/sprints/${projectId}/backlog`);
        sprintsProjectCaches.set(projectId, `${match.url}/backlog`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_TAB.SPRINT_BOARD.value: {
        statisticalActiveSprint({ projectId });
        getWorkflowByProject({ projectId, type: typeGetTask });
        const stringUrl = localStorage.getItem('sprintsBoardCaches');
        let isExistBoardCache = false;
        if (stringUrl) {
          const urls = JSON.parse(stringUrl);
          for (const url of urls) {
            if (url[0] === projectId) {
              isExistBoardCache = true;
              history.push(`/sprints/${projectId}/board/${url[1]}`);
              break;
            }
          }
          if (!isExistBoardCache) history.push(`/sprints/${projectId}/board/1`);
        } else {
          history.push(`/sprints/${projectId}/board/1`);
        }
        sprintsProjectCaches.set(projectId, `${match.url}/board`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_TAB.SPRINT.value: {
        history.push(`/sprints/${projectId}/sprint`);
        sprintsProjectCaches.set(projectId, `${match.url}/sprint`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_THROUGHOUT_TAB.WIKI.value: {
        history.push(`/sprints/${projectId}/wiki`);
        sprintsProjectCaches.set(projectId, `${match.url}/wiki`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_TAB.PROGRESS.value: {
        history.push(`${match.url}/progress`);
        sprintsProjectCaches.set(projectId, `${match.url}/progress`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_THROUGHOUT_TAB.ISSUE.value: {
        history.push(`${match.url}/issues`);
        sprintsProjectCaches.set(projectId, `${match.url}/issues`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      case SPRINT_TAB.LOG.value: {
        history.push(`/sprints/${projectId}/log`);
        sprintsProjectCaches.set(projectId, `${match.url}/log`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
        break;
      }
      default:
        break;
    }
    clearDetailBacklogTask();
  };

  useEffect(() => {
    if (selectedProject) {
      // calculate visible tabs
      const allow = role => isAllowPermission(appPermissions, role, selectedProject.id);
      const tabConditions = [
        { value: 1, allow: true },
        {
          value: 2,
          allow:
            allow(GET_SPRINTS_BY_PROJECT) ||
            allow(PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT) ||
            allow(FILTER_TASKS_BY_PROJECT)
        },
        {
          value: 3,
          allow: allow(CREATE_ALLOCATION) || allow(ADD_TASK_TO_SPRINT)
        },
        {
          value: 4,
          allow: allow(LIST_ATTACHMENT_BY_PROJECT) && allow(LIST_DOCUMENT_WIKI)
        },
        {
          value: 5,
          allow: allow(SPRINT_PROGRESS_OF_MEMBER)
        },
        {
          value: 6,
          allow:
            allow(GET_LIST_ISSUES) ||
            allow(GET_ISSUE) ||
            allow(CREATE_ISSUE) ||
            allow(UPDATE_ISSUE) ||
            allow(REMOVE_ISSUE) ||
            allow(STATISTICAL_ISSUE)
        },
        {
          value: 7,
          allow: allow(ADD_MY_SKILL_IN_SPRINT) || allow(ADD_USER_SKILL_IN_SPRINT) || allow(LOG_BEHAVIOR_SPRINT)
        }
      ];
      let currentVisubleTabs = visibleTabs;
      tabConditions.forEach(tab => {
        if (tab.allow) {
          if (!visibleTabs.includes(tab.value)) {
            currentVisubleTabs = [...currentVisubleTabs, tab.value];
          }
        } else {
          const index = visibleTabs.findIndex(t => t === tab.value);
          if (index >= 0) {
            currentVisubleTabs = [...currentVisubleTabs.slice(0, index), ...currentVisubleTabs.slice(index + 1)];
          }
        }
      });

      if (currentVisubleTabs !== visibleTabs) {
        // something changed
        setVisibleTabs(currentVisubleTabs);
        if (!currentVisubleTabs.includes(activeTab)) {
          changeTab(currentVisubleTabs[0]);
        }
      }
    }
  }, [selectedProject, appPermissions]);

  useEffect(() => {
    if (urlCache !== location.pathname) {
      localStorage.setItem('sprintsUrlCache', location.pathname);
    }
  }, [urlCache, location]);

  useEffect(() => {
    const activePath = location.pathname.split('/').length > 2 && location.pathname.split('/')[3];
    if (activePath) {
      for (const [i, r] of Object.entries(route.routes)) {
        if (matchPath(location.pathname, r.path)) {
          const newTab = Number(i) + 1;
          if (activeTab !== newTab) {
            setActiveTab(newTab);
          }
          break;
        }
      }
    }
  }, [location]);

  const changeChooseProject = project => {
    if (project) {
      const allow = role => isAllowPermission(appPermissions, role, project.id);
      if (allow(PROGRESS_SPRINT_ACTIVE_BY_PROJECT)) statisticalActiveSprint({ projectId: project.id });
      if (allow(LIST_WORKFLOW_STATE_BY_PROJECT))
        getWorkflowByProject({ projectId: project.id, type: TYPE_GET_TASK.MY_TASK });
      if (allow(GET_SPRINTS_BY_PROJECT)) getSprints({ projectId: project.id });
      if (allow(GET_SPRINTS_DIFF_COMPLETED_BY_PROJECT)) getSprintDiffCompleted({ projectId: project.id });
      setTypeGetTask(TYPE_GET_TASK.MY_TASK);
      setActiveTabBoard(1);
    }
    clearAll();
  };

  const changeRouteProject = projectId => {
    const stringUrl = localStorage.getItem('sprintsProjectCaches');

    if (stringUrl) {
      const urls = JSON.parse(stringUrl);
      let isCache = false;
      for (const url of urls) {
        if (url[0] === projectId) {
          history.push(url[1]);
          isCache = true;
          break;
        }
      }
      if (!isCache) {
        history.push(`/sprints/${projectId}/board/1`);
        sprintsProjectCaches.set(projectId, `/sprints/${projectId}/board`);
        localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
      }
    } else {
      history.push(`/sprints/${projectId}/board/1`);
      sprintsProjectCaches.set(projectId, `/sprints/${projectId}/board`);
      localStorage.setItem('sprintsProjectCaches', JSON.stringify([...sprintsProjectCaches]));
    }

    getSuggestTags(projectId);
    // getSprints({ projectId });
    // if (selectedProject) {
    //   statisticalActiveSprint({ projectId });
    //   getWorkflowByProject({
    //     projectId,
    //     type: typeGetTask
    //   });
    // }
  };

  const projectIdParams = Number(match.params.projectId);
  const myProjectsActive = calculateLength(myProjects) ? myProjects.filter(p => p.isActive) : [];

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="page sprint-area">
          {toggleFullBoard === STATUS_FULL_BOARD.ZOOM_OUT && (
            <div className={'left margin-bottom-15px'}>
              {myProjectsActive.map((p, index) => (
                <div
                  className={'d-inline-flex margin-bottom-5px margin-right-10px min-width-120px'}
                  key={index}
                  onClick={() => {
                    switchSprintProject(p);
                    changeChooseProject(p);
                    changeRouteProject(p.id);
                  }}
                >
                  <ProjectBullet selected={p === selectedProject || p.id === projectIdParams} value={p} />
                </div>
              ))}
            </div>
          )}

          {selectedProject ? (
            <Fragment>
              {toggleFullBoard === STATUS_FULL_BOARD.ZOOM_OUT && (
                <div className={'d-flex justify-content-end margin-bottom-40px'}>
                  <MenuLeft visibleTabs={visibleTabs} activeTab={activeTab} onClick={value => changeTab(value)} />
                  <div className={'ml-3'} />
                  <MenuRight visibleTabs={visibleTabs} activeTab={activeTab} onClick={value => changeTab(value)} />
                </div>
              )}
              <RenderRoutes routes={route.routes} />
            </Fragment>
          ) : (
            <div className={'no-project-selected'}>
              <img src={planImg} alt={'no-project-selected'} />
              <div className={'text-center'}>{t('select_a_project')}</div>
            </div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

Sprint.propTypes = {
  getMyProjects: func,
  getSuggestTags: func,
  getSprints: func,
  selectedProject: object,
  projectId: number,
  statisticalActiveSprint: func,
  getWorkflowByProject: func,
  getSprintDiffCompleted: func,
  clearDetailSprint: func,
  clearDetailBacklogTask: func,
  clearBacklog: func,
  switchSprintProject: func,
  myProjects: array,
  addNotification: func,
  appPermissions: array,
  clearBoard: func,
  clearSprintsList: func,
  fireSocketAddComment: func,
  history: object,
  location: object,
  match: object,
  fireSocketUpdateComment: func,
  fireSocketDeleteComment: func,
  typeGetTask: number,
  setTypeGetTask: func,
  setActiveTabBoard: func,
  route: object,
  urlCache: string,
  fireSocketAddCommentIssue: func,
  fireSocketDeleteCommentIssue: func,
  fireSocketUpdateCommentIssue: func,
  clearListProjects: func,
  clearSprintsDiffCompleted: func,
  selectedUserBoard: number,
  toggleFullBoard: number
};

export default memo(Sprint);
