import './stylesTabLog.css';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { memo, useState, useEffect } from 'react';
import { array, func, number, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import {
  ADD_MY_SKILL_IN_SPRINT,
  ADD_USER_SKILL_IN_SPRINT,
  LOG_BEHAVIOR_SPRINT
} from '../../../../../07-constants/app-permission';

const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <div className="pull-right">
    <Skeleton width={123.5} height={34} />
  </div>
);

const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);
const BehaviorSprintDialog = LazyComponent(() => import('../../../../dialog/behaviour-dialog/behavior-sprint-dialog'));
const SprintUserSkillDialog = LazyComponent(() => import('../../../../dialog/sprint-skill-dialog/LogMySkillDialog'));
const SprintAdminSkillDialog = LazyComponent(() => import('../../../../dialog/sprint-skill-dialog/LogUserSkillDialog'));

const UserBehaviorTable = LazyComponent(
  promiseDelayImport(import('./UserBehaviorTable/'), 1500),
  <SpinLoader type="grow" color="primary" />
);
const MySkillTable = LazyComponent(
  promiseDelayImport(import('./MySkillTable'), 1500),
  <SpinLoader type="grow" color="primary" />
);
const UserSkillTable = LazyComponent(
  promiseDelayImport(import('./UserSkillTable'), 1500),
  <SpinLoader type="grow" color="primary" />
);

const TabLog = ({ appPermissions, activeSprint, projectId, getUsers }) => {
  const [state, setState] = useState({
    isUserSkillDialog: false,
    isAdminSkillDialog: false,
    showBehaviorDialog: false
  });

  const { isUserSkillDialog, isAdminSkillDialog, showBehaviorDialog } = state;

  useEffect(() => {
    getUsers();
  }, []);

  const renderDialog = () => {
    return (
      <>
        <SprintUserSkillDialog
          isOpen={isUserSkillDialog}
          onClose={() => setState({ ...state, isUserSkillDialog: false })}
        />
        <SprintAdminSkillDialog
          isOpen={isAdminSkillDialog}
          onClose={() => setState({ ...state, isAdminSkillDialog: false })}
        />
        <BehaviorSprintDialog
          isOpen={showBehaviorDialog}
          projectId={projectId}
          sprintId={activeSprint && activeSprint.id}
          onClose={() => setState({ ...state, showBehaviorDialog: false })}
        />
      </>
    );
  };

  const renderContent = t => {
    return (
      <div>
        {activeSprint && activeSprint.id && isAllowPermission(appPermissions, ADD_MY_SKILL_IN_SPRINT, projectId) && (
          <div className={'margin-bottom-40px'}>
            <AppText type={'title-header'} className={'margin-right-10px'}>
              {t('log_my_skill')}
            </AppText>
            <Button
              title={t('log_my_skill_more')}
              type={'orange'}
              className={'mr-1 pull-right'}
              onClick={() => setState({ ...state, isUserSkillDialog: true })}
            >
              <i className={'fa fa-plus margin-right-5px'} /> {t('log_my_skill')}
            </Button>
            <div className={'text-center mt-5'}>
              <MySkillTable />
            </div>
          </div>
        )}
        {activeSprint && activeSprint.id && isAllowPermission(appPermissions, ADD_USER_SKILL_IN_SPRINT, projectId) && (
          <div className={'margin-bottom-40px'}>
            <AppText type={'title-header'} className={'margin-right-10px'}>
              {t('log_user_skill')}
            </AppText>
            <Button
              title={t('log_user_skill_more')}
              type={'orange'}
              className={'mr-1 pull-right'}
              onClick={() => setState({ ...state, isAdminSkillDialog: true })}
            >
              <i className={'fa fa-plus margin-right-5px'} /> {t('log_user_skill')}
            </Button>
            <div className={'text-center mt-5'}>
              <UserSkillTable />
            </div>
          </div>
        )}
        {projectId && isAllowPermission(appPermissions, LOG_BEHAVIOR_SPRINT, projectId) && (
          <div className={'margin-bottom-40px'}>
            <AppText type={'title-header'} className={'margin-right-10px'}>
              {t('behaviour_management.log_behaviour')}
            </AppText>
            <Button
              title={t('behavior_log')}
              type={'orange'}
              className={'mr-auto mb-4 pull-right'}
              onClick={() => setState({ ...state, showBehaviorDialog: true })}
            >
              <i className={'fa fa-plus margin-right-5px'} /> {t('behaviour_management.log_behaviour')}
            </Button>
            <div className={'text-center mt-5'}>
              <UserBehaviorTable />
            </div>
          </div>
        )}
      </div>
    );
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          {renderDialog(t)}
          {renderContent(t)}
        </>
      )}
    </NamespacesConsumer>
  );
};

TabLog.propTypes = {
  appPermissions: array,
  activeSprint: object,
  projectId: number,
  getUsers: func
};

export default memo(TabLog);
