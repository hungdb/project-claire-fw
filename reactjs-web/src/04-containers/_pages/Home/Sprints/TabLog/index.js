import TabLog from './TabLog';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUsers } from '../../../../../01-actions/adminproject';
import { getListProject } from '../../../../../01-actions/kpiLeader';

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  activeSprint: state.sprint.activeSprint,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getListProject,
      getUsers
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabLog);
