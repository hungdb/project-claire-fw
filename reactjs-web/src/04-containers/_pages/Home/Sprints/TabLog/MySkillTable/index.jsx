import Markdown from 'react-markdown';
import breaks from 'remark-breaks';
import connect from 'react-redux/es/connect/connect';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { array, number, func, object } from 'prop-types';
import { listUserSkills, removeUserSkill } from '../../../../../../01-actions/sprint';
import { PERM_LIST_MY_USER_SKILLS } from '../../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { linkRenderTarget } from '../../../../../../08-helpers/component';

const BaseDialog = LazyComponent(() => import('../../../../../../03-components/Dialog'));
const DataTable = LazyComponent(() => import('./DataTable'));

const MySkillTable = ({
  appPermissions,
  activeSprint,
  projectId,
  userId,
  allMySkillByUser,
  listUserSkills,
  removeUserSkill
}) => {
  const [state, setState] = useState({
    isOpenDialogDescription: false,
    description: ''
  });

  const { isOpenDialogDescription, description } = state;

  useEffect(() => {
    listUserSkills(activeSprint.id);
  }, []);

  useEffect(() => {
    if (isAllowPermission(appPermissions, PERM_LIST_MY_USER_SKILLS, projectId)) {
      listUserSkills();
    }
  }, [projectId]);

  const renderMarkdown = description => (
    <div className="markdown-content pt-2 pt-2">
      <Markdown plugins={[breaks]} source={description} renderers={{ link: props => linkRenderTarget(props) }} />
    </div>
  );

  const onCloseDialog = () => setState({ isOpenDialogDescription: false, description: '' });

  const onViewDescription = description => {
    setState({
      isOpenDialogDescription: true,
      description
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <BaseDialog
            isOpen={isOpenDialogDescription}
            dialogWidth={'lg'}
            title={t('description')}
            labelBtnCancel={t('OK')}
            dialogContent={renderMarkdown(description)}
            onClose={onCloseDialog.bind(this)}
          />
          <DataTable
            userId={userId}
            removeUserSkill={removeUserSkill}
            allMySkillByUser={allMySkillByUser}
            onViewDescription={onViewDescription}
          />
        </>
      )}
    </NamespacesConsumer>
  );
};

MySkillTable.propTypes = {
  appPermissions: array,
  activeSprint: object,
  projectId: number,
  userId: number,
  allMySkillByUser: array,
  listUserSkills: func,
  removeUserSkill: func
};

const mapStateToProps = state => ({
  userId: state.auth.user.id,
  appPermissions: state.auth.permissions,
  activeSprint: state.sprint.activeSprint,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  allMySkillByUser: state.sprint.allMySkillByUser
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listUserSkills,
      removeUserSkill
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(MySkillTable));
