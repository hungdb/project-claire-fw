import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo, useState } from 'react';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { array, number, func } from 'prop-types';
import { calculateLength, formatDateTime } from '../../../../../../08-helpers/common';

const ConfirmationModal = LazyComponent(() =>
  import('../../../../../../03-components/ConfirmationModal/ConfirmationModal')
);
const TableHead = LazyComponent(() => import('../../../../../../03-components/TableHead'));

const classes = ['w-5p', 'w-15p', 'w-10p', 'w-15p', 'w-5p'];
const fields = ['no', 'skill_name', 'description', 'created_at', 'action'];

const MySkillTable = ({ userId, allMySkillByUser, removeUserSkill, onViewDescription }) => {
  const [state, setState] = useState({
    isOpenConfirmModal: false,
    skillId: ''
  });

  const { isOpenConfirmModal, skillId } = state;

  const getMySkills = skills => (calculateLength(skills) ? skills.filter(s => s.userId === userId) : []);

  const _renderTableContent = mySkills => {
    const result = mySkills.map((s, index) => (
      <tr key={s.id}>
        <td className={'text-center align-middle'}>{index + 1}</td>
        <td className={'text-center align-middle'}>{s.skill.name}</td>
        <td className={'text-left align-middle'}>
          <i
            className="fa fa-eye text-center align-middle font-size-14 color-base icon-show-description cursor-pointer"
            onClick={() => onViewDescription(s.note)}
          />
        </td>
        <td className={'text-center align-middle'}>{formatDateTime(s.createdAt, 'LLLL', 1)}</td>
        <td className={'text-center align-middle'}>
          <span className={'cursor-pointer'}>
            <i
              className="fa fa fa-trash-o font-size-14 color-base-remove-btn"
              onClick={() => setState({ skillId: s.id, isOpenConfirmModal: true })}
            />
          </span>
        </td>
      </tr>
    ));
    return <tbody>{result}</tbody>;
  };

  const _renderNoUserSkill = (length, t) => (
    <tbody>
      <td colSpan={length}>{t('not_found')}</td>
    </tbody>
  );

  const onCloseConfirmModal = () => setState({ isOpenConfirmModal: false, skillId: '' });

  const onRemoveSkill = () => {
    removeUserSkill(skillId);
    onCloseConfirmModal();
  };

  const mySkills = getMySkills(allMySkillByUser);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} classes={classes} />
              </tr>
            </thead>
            {calculateLength(mySkills) ? _renderTableContent(mySkills) : _renderNoUserSkill(calculateLength(fields), t)}
          </Table>
          <ConfirmationModal isOpen={isOpenConfirmModal} funConfirm={onRemoveSkill} funClose={onCloseConfirmModal}>
            {t('are_you_sure')}
          </ConfirmationModal>
        </>
      )}
    </NamespacesConsumer>
  );
};

MySkillTable.propTypes = {
  userId: number,
  allMySkillByUser: array,
  removeUserSkill: func,
  onViewDescription: func
};

export default memo(MySkillTable);
