import connect from 'react-redux/es/connect/connect';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import React, { memo, useState, useEffect } from 'react';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { array, number, func, object } from 'prop-types';
import { removeLogBehaviorInSprint, getListLogBehaviorInSprint } from '../../../../../../01-actions/sprint';
import { PERM_GET_LIST_LOG_BEHAVIOR_IN_SPRINT } from '../../../../../../07-constants/app-permission';
import { isAllowPermission, formatDateTime, calculateLength } from '../../../../../../08-helpers/common';
import { TYPE_LOG_BEHAVIOR } from '../../../../../../05-utils/commonData';

const ConfirmationModal = LazyComponent(() =>
  import('../../../../../../03-components/ConfirmationModal/ConfirmationModal')
);
const UserBehaviorDetail = LazyComponent(() => import('./../dialog/UserBehaviorDetail'));
const TableHead = LazyComponent(() => import('../../../../../../03-components/TableHead'));

const fields = ['no', 'users', 'type', 'point', 'created_at', 'action'];
const classes = ['w-5p', 'w-15p', 'w-20p', 'w-15p', 'w-20p', 'w-10p text-center'];

const UserSkillTable = ({
  appPermissions,
  activeSprint,
  projectId,
  allUsersBehaviorSprint,
  getListLogBehaviorInSprint,
  removeLogBehaviorInSprint,
  selectedProject
}) => {
  const [state, setState] = useState({
    showBehaviorDialog: false,
    behaviorItemDetail: null,
    behaviorId: '',
    isOpenConfirmModal: false
  });

  const { showBehaviorDialog, behaviorItemDetail, isOpenConfirmModal, behaviorId } = state;

  const checkRoleAndLoad = () => {
    if (isAllowPermission(appPermissions, PERM_GET_LIST_LOG_BEHAVIOR_IN_SPRINT, projectId)) {
      getListLogBehaviorInSprint(activeSprint && activeSprint.id);
    }
  };

  useEffect(() => {
    checkRoleAndLoad();
  }, []);

  useEffect(() => {
    checkRoleAndLoad();
  }, [activeSprint, selectedProject]);

  const viewDetailBehavior = logBehaviorId => {
    const behaviorItemDetail = allUsersBehaviorSprint.find(u => u.id === logBehaviorId);
    setState({
      showBehaviorDialog: true,
      behaviorItemDetail
    });
  };

  const _renderTableContent = (data, t) => {
    if (calculateLength(data)) {
      const result = data.map((item, index) => {
        const point = item.pointAdd - item.pointSub;
        const type =
          item.type === TYPE_LOG_BEHAVIOR.RESPONSIBILITY
            ? t('behaviour_management.responsibility')
            : t('behaviour_management.dedication');
        return (
          <tr key={item.id}>
            <td>{index + 1}</td>
            <td>{item.User.name}</td>
            <td>{type}</td>
            <td>{point}</td>
            <td>{formatDateTime(item.createdAt, 'LLLL', 1)}</td>
            <td className="text-center">
              <span className={'cursor-pointer'}>
                <i className="fa fa-eye color-base mr-2" onClick={() => viewDetailBehavior(item.id)} />
                <i
                  className="fa fa fa-trash-o font-size-14 color-base-remove-btn"
                  onClick={() => setState({ isOpenConfirmModal: true, behaviorId: item.id })}
                />
              </span>
            </td>
          </tr>
        );
      });
      return <tbody>{result}</tbody>;
    }
  };

  const _renderNoUserSkill = (length, t) => (
    <tbody>
      <tr>
        <td colSpan={length} className="text-center text-muted">
          {t('not_found')}
        </td>
      </tr>
    </tbody>
  );

  const onCloseConfirmModal = () => setState({ ...state, isOpenConfirmModal: false, behaviorId: '' });
  const onRemoveBehavior = () => {
    removeLogBehaviorInSprint(behaviorId);
    onCloseConfirmModal();
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} classes={classes} />
              </tr>
            </thead>
            {calculateLength(allUsersBehaviorSprint)
              ? _renderTableContent(allUsersBehaviorSprint, t)
              : _renderNoUserSkill(calculateLength(fields), t)}
          </Table>
          {behaviorItemDetail && (
            <UserBehaviorDetail
              isOpen={showBehaviorDialog}
              data={behaviorItemDetail}
              onClose={() => setState({ ...state, showBehaviorDialog: false })}
            />
          )}
          <ConfirmationModal isOpen={isOpenConfirmModal} funClose={onCloseConfirmModal} funConfirm={onRemoveBehavior}>
            {t('are_you_sure')}
          </ConfirmationModal>
        </div>
      )}
    </NamespacesConsumer>
  );
};

UserSkillTable.propTypes = {
  appPermissions: array,
  activeSprint: object,
  projectId: number,
  allUsersBehaviorSprint: array,
  getListLogBehaviorInSprint: func,
  removeLogBehaviorInSprint: func,
  selectedProject: object
};

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  activeSprint: state.sprint.activeSprint,
  sprintId: state.sprint.activeSprint && state.sprint.activeSprint.id,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  allUsersBehaviorSprint: state.sprint.allUsersBehaviorSprint,
  selectedProject: state.sprint.selectedProject
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      removeLogBehaviorInSprint,
      getListLogBehaviorInSprint
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(UserSkillTable));
