import React, { memo } from 'react';
import { Table } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { array, number, func } from 'prop-types';
import { formatDateTime, calculateLength } from '../../../../../../08-helpers/common';

import LazyComponent from '../../../../../../03-components/LazyComponent';
const TableHead = LazyComponent(() => import('../../../../../../03-components/TableHead'));

const fields = ['no', 'User Name', 'actor', 'skill_name', 'description', 'created_at', 'action'];
const classes = ['w-5p', 'w-10p', 'w-10p', 'w-10p', 'w-20p', 'w-20p', 'w-5p'];

const UserSkillTable = ({
  // userId,
  listUserSkillsSprint,
  onOpenDescription,
  onRemove
}) => {
  const _renderTableContent = mySkills => {
    const result = mySkills.map((s, index) => (
      <tr key={s.id}>
        <td className={'text-center align-middle'}>{index + 1}</td>
        <td className={'text-center align-middle'}>{s.user && s.user.name}</td>
        <td className={'text-center align-middle'}>{s.actorAs && s.actorAs.name}</td>
        <td className={'text-center align-middle'}>{s.skill && s.skill.name}</td>
        <td className={'text-left align-middle'}>
          <i
            className="fa fa-eye text-center align-middle font-size-14 color-base icon-show-description cursor-pointer"
            onClick={() => onOpenDescription(s.note)}
          />
        </td>
        <td className={'text-center align-middle'}>{formatDateTime(s.createdAt, 'LLLL', 1)}</td>
        <td className={'text-center align-middle'}>
          <span className={'cursor-pointer'}>
            <i className="fa fa fa-trash-o font-size-14 color-base-remove-btn" onClick={() => onRemove(s.id)} />
          </span>
        </td>
      </tr>
    ));
    return <tbody>{result}</tbody>;
  };

  const _renderNoUserSkill = (length, t) => (
    <tbody>
      <td colSpan={length}>{t('not_found')}</td>
    </tbody>
  );

  const userSkills = calculateLength(listUserSkillsSprint) ? listUserSkillsSprint : [];
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <Table bordered responsive striped hover>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} classes={classes} />
              </tr>
            </thead>
            {calculateLength(userSkills)
              ? _renderTableContent(userSkills)
              : _renderNoUserSkill(calculateLength(fields), t)}
          </Table>
        </>
      )}
    </NamespacesConsumer>
  );
};

UserSkillTable.propTypes = {
  userId: number,
  listUserSkillsSprint: array,
  onOpenDescription: func,
  onRemove: func
};

export default memo(UserSkillTable);
