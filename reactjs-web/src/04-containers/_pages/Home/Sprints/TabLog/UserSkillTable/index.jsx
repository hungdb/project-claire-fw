import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import { array, number, func, object } from 'prop-types';
import {
  getListUserSkillsBySprint as _getListUserSkillsBySprint,
  removeUserSkillsInSprint as _removeUserSkillsInSprint
} from '../../../../../../01-actions/sprint';
import { PERM_LIST_USER_SKILLS_BY_SPRINT } from '../../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../../08-helpers/common';
import { linkRenderTarget } from '../../../../../../08-helpers/component';

import LazyComponent from '../../../../../../03-components/LazyComponent';
import Markdown from 'react-markdown';
import breaks from 'remark-breaks';
const BaseDialog = LazyComponent(() => import('../../../../../../03-components/Dialog'));
const ConfirmationModal = LazyComponent(() =>
  import('../../../../../../03-components/ConfirmationModal/ConfirmationModal')
);
const DataTable = LazyComponent(() => import('./DataTable'));

const UserSkillTable = ({
  appPermissions,
  activeSprint,
  projectId,
  listUserSkillsSprint,
  _getListUserSkillsBySprint,
  _removeUserSkillsInSprint
}) => {
  const [state, setState] = useState({
    description: '',
    skillUserItemId: '',
    isOpenConfirmModal: false,
    isOpenDialogDescription: false
  });

  const { isOpenDialogDescription, isOpenConfirmModal, description, skillUserItemId } = state;

  useEffect(() => {
    if (isAllowPermission(appPermissions, PERM_LIST_USER_SKILLS_BY_SPRINT, projectId)) {
      _getListUserSkillsBySprint(activeSprint.id);
    }
  }, []);

  const onCloseConfirmModal = () => setState({ ...state, isOpenConfirmModal: false, skillUserItemId: '' });

  const _renderMarkdown = description => (
    <Markdown
      plugins={[breaks]}
      source={description}
      className="markdown-content"
      renderers={{ link: props => linkRenderTarget(props) }}
    />
  );

  const onCloseDialog = () => setState({ ...state, isOpenDialogDescription: false, description: '' });

  const onRemoveUserSkill = () => {
    _removeUserSkillsInSprint(skillUserItemId);
    onCloseConfirmModal();
  };

  const onOpenCOnfirmModal = skillUserItemId => {
    setState({
      ...state,
      isOpenConfirmModal: true,
      skillUserItemId
    });
  };

  const onOpenDescription = description => {
    setState({
      ...state,
      isOpenDialogDescription: true,
      description
    });
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <>
          <BaseDialog
            isOpen={isOpenDialogDescription}
            dialogWidth={'lg'}
            title={t('description')}
            labelBtnCancel={t('OK')}
            dialogContent={_renderMarkdown(description)}
            onClose={onCloseDialog.bind(this)}
          />
          <DataTable
            listUserSkillsSprint={listUserSkillsSprint}
            onOpenDescription={onOpenDescription}
            onRemove={onOpenCOnfirmModal}
            _removeUserSkillsInSprint={_removeUserSkillsInSprint}
          />
          <ConfirmationModal isOpen={isOpenConfirmModal} funClose={onCloseConfirmModal} funConfirm={onRemoveUserSkill}>
            {t('are_you_sure')}
          </ConfirmationModal>
        </>
      )}
    </NamespacesConsumer>
  );
};

UserSkillTable.propTypes = {
  appPermissions: array,
  activeSprint: object,
  projectId: number,
  userId: number,
  listUserSkillsSprint: array,
  _getListUserSkillsBySprint: func,
  _removeUserSkillsInSprint: func
};

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  activeSprint: state.sprint.activeSprint,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  listUserSkillsSprint: state.sprint.listUserSkillsSprint
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      _getListUserSkillsBySprint,
      _removeUserSkillsInSprint
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(UserSkillTable));
