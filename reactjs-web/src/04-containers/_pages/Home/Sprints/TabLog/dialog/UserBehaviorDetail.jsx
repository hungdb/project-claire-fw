import React, { memo } from 'react';
import LazyComponent from '../../../../../../03-components/LazyComponent';
import { NamespacesConsumer } from 'react-i18next';
import { TYPE_APPLY_BEHAVIOR_OBJECT, TYPE_ACTION_BEHAVIOR_OBJECT } from './../../../../../../05-utils/commonData';
import { calculateLength } from './../../../../../../08-helpers/common';
import { bool, func, array } from 'prop-types';

const BaseDialog = LazyComponent(() => import('./../../../../../../03-components/Dialog'));
const TableHead = LazyComponent(() => import('./../../../../../../03-components/TableHead'));

const UserBehaviorDetail = ({ isOpen, onClose, data }) => {
  const renderBehaviorItem = item => {
    const point =
      item.behavior && item.behavior.typeAction === TYPE_ACTION_BEHAVIOR_OBJECT.SUB
        ? `- ${item.behavior && item.behavior.point}`
        : `+ ${item.behavior && item.behavior.point}`;
    return (
      <>
        <td>{item.behavior && item.behavior.activity}</td>
        <td>{point}</td>
      </>
    );
  };

  const renderTableContent = (data, t) => {
    if (calculateLength(data)) {
      const result = data.map((item, index) => {
        const type =
          item.behavior && item.behavior.typeApply === TYPE_APPLY_BEHAVIOR_OBJECT.SPRINT_RESPONSIBILITY
            ? t('behaviour_management.responsibility')
            : t('behaviour_management.dedication');
        if (index === 0) {
          return (
            <tr key={item.id}>
              <td rowSpan={calculateLength(data)} className="align-middle text-bold">
                {type}
              </td>
              {renderBehaviorItem(item)}
            </tr>
          );
        }
        return <tr key={item.id}>{renderBehaviorItem(item)}</tr>;
      });
      return <tbody>{result}</tbody>;
    }
  };

  const renderTableDetail = t => {
    const fields = ['type', 'activity', 'point'];
    const tableContent = (
      <table className="table table-bordered behavior-table">
        <thead>
          <tr>
            <TableHead fields={fields} t={t} />
          </tr>
        </thead>
        {renderTableContent(data.logDetailBehaviors, t)}
      </table>
    );
    return <div className="table-responsive">{tableContent}</div>;
  };
  const user = data.User && data.User.name;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <BaseDialog
          isOpen={isOpen}
          fullWidth={true}
          title={`${t('admin_panel_child.user_behaviour')}: ${user}`}
          onClose={onClose}
          dialogWidth="lg"
          dialogContent={renderTableDetail(t)}
        />
      )}
    </NamespacesConsumer>
  );
};

UserBehaviorDetail.propTypes = {
  isOpen: bool,
  onClose: func,
  data: array
};

export default memo(UserBehaviorDetail);
