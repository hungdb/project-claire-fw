import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo } from 'react';
import { array, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { deleteSprint, updateSprint } from '../../../../../01-actions/sprint';
import { EDIT_SPRINT } from '../../../../../07-constants/app-permission';
import { calculateLength, isAllowPermission } from '../../../../../08-helpers/common';

const SprintList = LazyComponent(() => import('../SprintList/SprintList'));
const TableHead = LazyComponent(() => import('../../../../../03-components/TableHead'));

const fields = [
  'no',
  'name',
  'status',
  'start_date',
  'end_date',
  'duration',
  'completed_task',
  'workload',
  'workforce',
  'members',
  'action'
];

function SprintsView({ appPermissions, projectId, sprints, updateSprint, deleteSprint }) {
  return (
    <NamespacesConsumer>
      {t => (
        <div className="sprint-backlog-area-table">
          <Table responsive striped hover bordered>
            <thead>
              <tr>
                <TableHead fields={fields} t={t} />
              </tr>
            </thead>
            {calculateLength(sprints) ? (
              <SprintList
                sprints={sprints}
                onUpdate={updateSprint}
                onDelete={deleteSprint}
                isEdit={isAllowPermission(appPermissions, EDIT_SPRINT, projectId)}
              />
            ) : (
              <tbody>
                <tr>
                  <td colSpan={11} className={'text-muted text-center font-size-14'}>
                    {t('no_sprint')}
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      )}
    </NamespacesConsumer>
  );
}

SprintsView.propTypes = {
  appPermissions: array,
  projectId: number,
  sprints: array,
  updateSprint: func,
  deleteSprint: func
};

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  sprints: state.sprint.sprints
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateSprint,
      deleteSprint
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(SprintsView));
