import Sprint from './Sprint';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getMyProjects, clearListProjects } from '../../../../01-actions/project';
import { addNotification } from '../../../../01-actions/notification';
import { getTasksInProject } from '../../../../01-actions/project-task-view';
import {
  clearBacklog,
  getSuggestTags,
  changeEstimate,
  updateBacklogTask,
  getDetailTask,
  clearDetailBacklogTask,
  deleteTask,
  getSprints,
  getTaskBacklog,
  getSprintDiffCompleted,
  clearDetailSprint,
  statisticalActiveSprint,
  getWorkflowByProject,
  switchSprintProject,
  clearBoard,
  clearSprintsList,
  setTypeGetTask,
  setActiveTabBoard,
  clearSprintsDiffCompleted
} from '../../../../01-actions/sprint';
import {
  fireSocketAddComment,
  fireSocketDeleteComment,
  fireSocketUpdateComment,
  fireSocketAddCommentIssue,
  fireSocketDeleteCommentIssue,
  fireSocketUpdateCommentIssue
} from '../../../../01-actions/comment';

const mapStateToProps = store => {
  return {
    // auth
    appPermissions: store.auth.permissions,
    roles: store.auth.roles,

    // common
    error: store.common.error,

    // project
    myProjects: store.project.projects,
    projectError: store.project.projectError,
    selectedProject: store.sprint.selectedProject,

    // sprint
    status: store.sprint.status,
    backlog: (store.sprint.backlog && store.sprint.backlog.rows) || [],
    total: (store.sprint.backlog && store.sprint.backlog.count) || 0,
    tags: store.sprint.tags,
    currentTask: store.sprint.currentTask,
    sprintError: store.sprint.sprintError,
    searchBacklog: store.sprint.searchBacklog,
    activeSprint: store.sprint.activeSprint,
    workflowProject: store.sprint.workflowProject,
    rowsPerPage: store.projectTaskView.filter.rowsPerPage,
    page: store.projectTaskView.filter.page,
    projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
    sprintId: store.projectTaskView.filter.sprint ? store.projectTaskView.filter.sprint.value : null,
    userId: store.projectTaskView.filter.assignee ? store.projectTaskView.filter.assignee.value : null,
    priorityId: store.projectTaskView.filter.priority ? store.projectTaskView.filter.priority.value : null,
    statusId: store.projectTaskView.filter.status ? store.projectTaskView.filter.status.value : null,
    startDate: store.projectTaskView.filter.startDate,
    search: store.projectTaskView.filter.search,
    typeGetTask: store.sprint.typeGetTask,
    activeTabBoard: store.sprint.activeTabBoard,
    urlCache: store.sprint.urlCache,
    selectedUserBoard: store.sprint.selectedUserBoard,

    // common
    toggleFullBoard: store.common.toggleFullBoard
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getMyProjects,
      clearBacklog,
      getSuggestTags,
      changeEstimate,
      updateBacklogTask,
      getDetailTask,
      clearDetailBacklogTask,
      deleteTask,
      getSprints,
      getTaskBacklog,
      getSprintDiffCompleted,
      clearDetailSprint,
      statisticalActiveSprint,
      getWorkflowByProject,
      switchSprintProject,
      addNotification,
      clearBoard,
      fireSocketAddComment,
      clearSprintsList,
      getTasksInProject,
      fireSocketUpdateComment,
      fireSocketDeleteComment,
      setTypeGetTask,
      setActiveTabBoard,
      fireSocketAddCommentIssue,
      fireSocketDeleteCommentIssue,
      fireSocketUpdateCommentIssue,
      clearListProjects,
      clearSprintsDiffCompleted
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sprint);
