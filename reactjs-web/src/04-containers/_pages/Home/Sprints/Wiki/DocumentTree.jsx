import 'rc-tree/assets/index.css';
import styles from './DocumentTree.module.scss';
import Tree from 'rc-tree';
import React, { memo, useRef, useState } from 'react';
import { array, func, object } from 'prop-types';
import { connect } from 'react-redux';
import { selectDocument, updateDocument } from '../../../../../01-actions/wiki';

function DocumentTree({ documents, selectedProject, _selectDocument, _updateDocument }) {
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [isRootTree, setIsRootTree] = useState(true);

  const wrapTree = useRef(null);

  const compare = (a, b) => {
    const titleA = a.title.toUpperCase();
    const titleB = b.title.toUpperCase();
    let comparison = 0;
    if (titleA > titleB) {
      comparison = 1;
    } else if (titleA < titleB) {
      comparison = -1;
    }
    return comparison;
  };

  const formatData = data => {
    // console.log('dataRaw', data);
    const documents = [...data].map(document => ({
      ...document,
      key: document.id,
      title: document.subject,
      children: []
    }));

    let docLeng = null;
    do {
      docLeng = documents.length;
      for (let i = 0; i < documents.length; ) {
        let parent = null;
        let isChild = true;
        for (let j = 0; j < documents.length; j++) {
          if (documents[i].parentId === documents[j].id && i !== j) {
            parent = j;
          }
          if (documents[i].id === documents[j].parentId && i !== j) {
            isChild = false;
            break;
          }
        }
        // console.log(i, parent, isChild);
        if ((parent || parent === 0) && isChild) {
          documents[parent].children.push(documents[i]);
          documents[parent].children.sort(compare);
          documents.splice(i, 1);
        } else {
          i++;
        }
        // console.log('data tree', documents);
      }
    } while (docLeng > documents.length);
    return documents.sort(compare);
  };

  const onSelect = (selectedKeys, info) => {
    if (info.selectedNodes[0]) {
      _selectDocument(info.selectedNodes[0].props);
    } else {
      _selectDocument(null);
    }
  };

  const onExpand = expandedKeys => {
    setExpandedKeys(expandedKeys);
    setAutoExpandParent(false);
  };

  const onDragStart = info => {
    if (info.node) {
      _selectDocument(info.node.props);
    } else {
      _selectDocument(null);
    }
  };

  const onDragEnter = info => {
    setExpandedKeys(info.expandedKeys);
  };

  const onDragEnd = ({ event, node }) => {
    if (isRootTree) {
      _updateDocument({
        projectId: node.props.projectId,
        documentId: node.props.id,
        parentId: null
      });
    }
    setIsRootTree(true);
  };

  const onDrop = info => {
    setIsRootTree(false);
    _updateDocument({
      projectId: selectedProject.id,
      documentId: info.dragNode.props.eventKey,
      parentId: info.node.props.eventKey
    });
  };

  return (
    <div className={styles.documentTree} ref={wrapTree}>
      <Tree
        className="d-inline-block"
        showIcon={false}
        showLine
        expandedKeys={expandedKeys}
        onExpand={onExpand}
        autoExpandParent={autoExpandParent}
        defaultExpandAll
        onSelect={onSelect}
        treeData={formatData(documents)}
        draggable
        onDragStart={onDragStart}
        onDragEnter={onDragEnter}
        onDragEnd={onDragEnd}
        onDrop={onDrop}
      />
    </div>
  );
}

DocumentTree.propTypes = {
  documents: array,
  selectedProject: object,
  _selectDocument: func,
  _updateDocument: func
};

const mapState = state => ({
  documents: state.wiki.documents,
  selectedProject: state.sprint.selectedProject
});

const mapDispatch = {
  _selectDocument: selectDocument,
  _updateDocument: updateDocument
};

export default connect(
  mapState,
  mapDispatch
)(memo(DocumentTree));
