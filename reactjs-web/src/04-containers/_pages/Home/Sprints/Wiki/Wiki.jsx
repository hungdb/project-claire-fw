import './style.css';
import i18n from '../../../../../i18n';
import cx from 'classnames';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useState, useEffect, memo } from 'react';
import { toast } from 'react-toastify';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import { object, number, array, func } from 'prop-types';
import { Row, Col, Collapse } from 'reactstrap';
import {
  PAGINATE_DOCUMENT_LIMIT,
  PAGINATE_ATTACHMENT_LIMIT,
  TOAST_ERROR_STYLE
} from '../../../../../05-utils/commonData';
import {
  setStatusDocumentUI,
  getDocuments,
  getAttachments,
  loadMoreAttachments,
  searchAttachments,
  removeAttachment,
  downloadFile,
  uploadFile
} from '../../../../../01-actions/wiki';
import { calculateLength, isAllowPermission, promiseDelayImport } from '../../../../../08-helpers/common';
import {
  ADD_ATTACHMENT_BY_PROJECT,
  CREATE_DOCUMENT_WIKI,
  LIST_ATTACHMENT_BY_PROJECT,
  LIST_DOCUMENT_WIKI
} from '../../../../../07-constants/app-permission';

const Document = LazyComponent(() => import('./Document.jsx'));
const DocumentDetail = LazyComponent(() => import('./DocumentDetail'));
const ConfirmationModal = LazyComponent(() => import('../../../../../03-components/ConfirmationModal'));
const Upload = LazyComponent(() => import('../../../../../03-components/Upload'));
const DocumentTree = LazyComponent(promiseDelayImport(import('./DocumentTree')), <Skeleton count={10} />);
const Text = LazyComponent(promiseDelayImport(import('../../../../../03-components/Text')), <Skeleton width={100} />);

const InputIcon = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/InputIcon')),
  <Skeleton width={213.68} height={38} />
);
const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <Skeleton width={142.34} height={34} />
);
const AttachmentCard = LazyComponent(
  promiseDelayImport(import('./AttachmentCard')),
  <Skeleton width={350} height={55} />
);

function Wiki({
  selectedProject,
  statusDocumentUI,
  setStatusDocumentUI,
  getDocuments,
  getAttachments,
  loadMoreAttachments,
  searchAttachments,
  attachments,
  attachmentTotal,
  removeAttachment,
  downloadFile,
  uploadFile,
  appPermissions
}) {
  const [attachmentKeySearch, setAttachmentKeySearch] = useState(null);
  const [attachmentOffset, setAttachmentOffset] = useState(0);
  const [attachmentAction, setAttachmentAction] = useState(false);
  const [attachmentHoverActor, setAttachmentHoverActor] = useState(0);
  const [openAlert, setOpenAlert] = useState(false);
  const [attachment, setAttachment] = useState(null);
  const [openUpload, setOpenUpload] = useState(false);
  const [collapseDocument, setCollapseDocument] = useState(true);
  const [collapseAttachment, setCollapseAttachment] = useState(true);

  const projectId = selectedProject.id;

  useEffect(() => {
    if (isAllowPermission(appPermissions, LIST_DOCUMENT_WIKI, projectId)) {
      getDocuments({
        projectId,
        subject: null,
        limit: PAGINATE_DOCUMENT_LIMIT,
        offset: 0
      });
    }
    if (isAllowPermission(appPermissions, LIST_ATTACHMENT_BY_PROJECT, projectId))
      getAttachments({
        projectId,
        name: null,
        limit: PAGINATE_ATTACHMENT_LIMIT,
        offset: 0
      });
    setAttachmentOffset(0);
    setAttachmentKeySearch(null);
  }, [selectedProject]);

  const openAlertAttachment = attachment => {
    setOpenAlert(true);
    setAttachment(attachment);
  };

  const closeAlert = () => setOpenAlert(false);

  const loadMoreAttachment = () => {
    const offset = attachmentOffset + PAGINATE_ATTACHMENT_LIMIT;
    loadMoreAttachments({
      projectId,
      name: attachmentKeySearch,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset
    });
    setAttachmentOffset(offset);
  };

  const searchAttachment = value => {
    searchAttachments({
      projectId,
      name: value,
      limit: PAGINATE_ATTACHMENT_LIMIT,
      offset: 0
    });
    setAttachmentOffset(0);
    setAttachmentKeySearch(value);
  };

  const handleRemoveAttachment = () => {
    if (!projectId || !attachment || (attachment && !attachment.id)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      removeAttachment({
        attachmentId: attachment.id,
        projectId
      });
    }
    closeAlert();
  };

  const handleDownloadAttachment = attachment => {
    if (!projectId || !attachment || (attachment && !attachment.url)) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      downloadFile({
        filePath: attachment.url,
        projectId
      });
    }
  };

  const handleUpload = acceptedFiles => {
    if (!projectId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      uploadFile({
        files: acceptedFiles,
        projectId
      });
    }
    setAttachmentKeySearch(null);
    setAttachmentOffset(0);
  };

  const toggleUpload = () => {
    setOpenUpload(!openUpload);
  };

  return (
    isAllowPermission(appPermissions, LIST_ATTACHMENT_BY_PROJECT, projectId) &&
    isAllowPermission(appPermissions, LIST_DOCUMENT_WIKI, projectId) && (
      <NamespacesConsumer>
        {t => (
          <div className="wiki-page">
            {!!statusDocumentUI && <Document />}
            {isAllowPermission(appPermissions, LIST_DOCUMENT_WIKI, projectId) && (
              <div className="default-paper margin-bottom-15px">
                <Row className="wiki-group-title" onClick={() => setCollapseDocument(!collapseDocument)}>
                  <Col xs={true}>
                    <Text className={'heading font-size-15'}>{t('documents')}</Text>
                  </Col>
                  <Col xs="auto">
                    <i className={cx('fa', collapseDocument ? 'fa-chevron-down' : 'fa-chevron-up')} />
                  </Col>
                </Row>
                <Collapse isOpen={collapseDocument} className="document-content">
                  <Row>
                    <Col lg={4} className="mb-3">
                      <DocumentTree />
                    </Col>
                    <Col lg={8} className="mb-3">
                      <div className="text-right">
                        {isAllowPermission(appPermissions, CREATE_DOCUMENT_WIKI, projectId) && (
                          <Button type={'orange'} onClick={() => setStatusDocumentUI(1)}>
                            <i className={'fa fa-plus margin-right-5px'} /> {t('add_document')}
                          </Button>
                        )}
                      </div>
                      <DocumentDetail />
                    </Col>
                  </Row>
                </Collapse>
              </div>
            )}

            {isAllowPermission(appPermissions, LIST_ATTACHMENT_BY_PROJECT, projectId) && (
              <div className="default-paper margin-bottom-15px attachment-container">
                <Row className="wiki-group-title" onClick={() => setCollapseAttachment(!collapseAttachment)}>
                  <Col xs={true}>
                    <Text className={'heading font-size-15'}>{t('attachments')}</Text>
                  </Col>
                  <Col xs="auto">
                    <i className={cx('fa', collapseAttachment ? 'fa-chevron-down' : 'fa-chevron-up')} />
                  </Col>
                </Row>
                <Collapse isOpen={collapseAttachment} className="document-content">
                  <div className="d-flex justify-content-between align-items-center mb-4">
                    <div className="d-flex align-items-center mr-3 input-search">
                      <InputIcon
                        awesomeIcon="fa fa-search"
                        type="text"
                        placeholder={t('search')}
                        handleInput={searchAttachment}
                      />
                    </div>
                    {isAllowPermission(appPermissions, ADD_ATTACHMENT_BY_PROJECT, projectId) && (
                      <Button type={'orange'} onClick={() => toggleUpload()}>
                        {!openUpload ? (
                          <i className={'fa fa-plus margin-right-5px'} />
                        ) : (
                          <i className={'fa fa-minus margin-right-5px'} />
                        )}
                        {!openUpload ? t('upload_file') : t('close_upload_file')}
                      </Button>
                    )}
                  </div>
                  {openUpload && (
                    <Row className="mb-4">
                      <Col md={12}>
                        <Upload onDrop={(acceptedFiles, rejectedFiles) => handleUpload(acceptedFiles, rejectedFiles)} />
                      </Col>
                    </Row>
                  )}
                  <Row className="attachment-area mb-4">
                    {attachments.map((attachment, index) => (
                      <Col md="6" xl="4" key={index}>
                        <AttachmentCard
                          onMouseEnter={() => {
                            setAttachmentAction(true);
                            setAttachmentHoverActor(attachment.id);
                          }}
                          onMouseLeave={() => {
                            setAttachmentAction(false);
                            setAttachmentHoverActor(attachment.id);
                          }}
                          attachment={attachment}
                          projectId={projectId}
                          attachmentAction={attachmentAction}
                          attachmentHoverActor={attachmentHoverActor}
                          appPermissions={appPermissions}
                          onRemove={() => openAlertAttachment(attachment)}
                          onDownload={() => handleDownloadAttachment(attachment)}
                        />
                      </Col>
                    ))}
                  </Row>
                  {calculateLength(attachments) < attachmentTotal ? (
                    <div className="viewmore text-center text-uppercase mb-4">
                      <i className="fa fa-repeat mr-1" />
                      <span onClick={() => loadMoreAttachment()}>{t('view_more')}</span>
                    </div>
                  ) : null}
                  <ConfirmationModal
                    isOpen={openAlert}
                    txtTitle={t('confirm_remove')}
                    txtCancel={t('cancel')}
                    txtConfirm={t('confirm')}
                    funConfirm={handleRemoveAttachment}
                    funClose={closeAlert}
                  >
                    {t('are_you_sure_remove_this_attachment')}
                  </ConfirmationModal>
                </Collapse>
              </div>
            )}
          </div>
        )}
      </NamespacesConsumer>
    )
  );
}

Wiki.propTypes = {
  selectedProject: object,
  statusDocumentUI: number,
  setStatusDocumentUI: func,
  getDocuments: func,
  getAttachments: func,
  loadMoreAttachments: func,
  searchAttachments: func,
  attachments: array,
  attachmentTotal: number,
  removeAttachment: func,
  downloadFile: func,
  uploadFile: func,
  appPermissions: array
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  status: store.wiki.status,
  statusDocumentUI: store.wiki.statusDocumentUI,
  attachments: store.wiki.attachments,
  attachmentTotal: store.wiki.attachmentTotal,
  selectedProject: store.sprint.selectedProject
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setStatusDocumentUI,
      getDocuments,
      getAttachments,
      loadMoreAttachments,
      searchAttachments,
      removeAttachment,
      downloadFile,
      uploadFile
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(Wiki));
