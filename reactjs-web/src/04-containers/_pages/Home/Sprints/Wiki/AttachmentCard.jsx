import React, { memo } from 'react';
import { func, object, number, bool, array } from 'prop-types';
import { formatDateTime, truncateText, isAllowPermission, getIconFile } from '../../../../../08-helpers/common';
import { DOWNLOAD_ATTACHMENT, REMOVE_ATTACHMENT_BY_PROJECT } from '../../../../../07-constants/app-permission';

const AttachmentCard = ({
  onMouseEnter,
  onMouseLeave,
  attachment,
  projectId,
  attachmentAction,
  attachmentHoverActor,
  appPermissions,
  onRemove,
  onDownload
}) => {
  return (
    <div className={'file-item d-flex align-items-center mb-3'} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
      <i className={`${getIconFile(attachment.mimeType)} file-icon mr-3`} />
      <div className="w-100">
        <div title={attachment.name}>{truncateText(attachment.name, 30)}</div>
        <div className="d-flex justify-content-between align-items-center">
          <small>
            <i className="fa fa-clock-o" /> {formatDateTime(attachment.createdAt, 'LLLL', 1)}
          </small>
          {isAllowPermission(appPermissions, REMOVE_ATTACHMENT_BY_PROJECT, projectId) &&
            attachmentAction &&
            attachment.id === attachmentHoverActor && (
              <span className="at-btn-remove" onClick={onRemove}>
                <i className="fa fa fa-trash-o" />
              </span>
            )}
          {isAllowPermission(appPermissions, DOWNLOAD_ATTACHMENT, projectId) &&
            attachmentAction &&
            attachment.id === attachmentHoverActor && (
              <span className="at-btn-download" onClick={onDownload}>
                {/*<i className="fa fa-cloud-download"/>*/}
                <i className="fa fa-eye" />
              </span>
            )}
        </div>
      </div>
    </div>
  );
};

AttachmentCard.propTypes = {
  onMouseEnter: func,
  onMouseLeave: func,
  attachment: object,
  projectId: number,
  attachmentAction: bool,
  attachmentHoverActor: number,
  appPermissions: array,
  onRemove: func,
  onDownload: func
};

export default memo(AttachmentCard);
