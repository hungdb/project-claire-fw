import styles from './DocumentDetail.module.scss';
import breaks from 'remark-breaks';
import ConfirmationModal from '../../../../../03-components/ConfirmationModal';
import ReactMarkdown from 'react-markdown';
import React, { Fragment, memo, useState } from 'react';
import { array, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { removeDocument, setStatusDocumentUI } from '../../../../../01-actions/wiki';
import { REMOVE_DOCUMENT_WIKI, UPDATE_DOCUMENT_WIKI } from '../../../../../07-constants/app-permission';
import { formatDateTime, isAllowPermission } from '../../../../../08-helpers/common';
import { linkRenderTarget } from '../../../../../08-helpers/component';

function DocumentDetail({ appPermissions, selectedProject, document, _removeDocument, _setStatusDocumentUI }) {
  const [statusAlert, setStatusAlert] = useState(false);

  const openAlertRemove = () => setStatusAlert(true);

  const closeAlertRemove = () => setStatusAlert(false);

  const handleRemoveDocument = () => {
    const projectId = selectedProject.id;
    const documentId = document.id;

    _removeDocument({
      documentId,
      projectId
    });
  };

  return (
    <NamespacesConsumer>
      {t =>
        document && (
          <Fragment>
            <div className="mb-3">
              <h2>{document.subject}</h2>
              <small className={styles.smallText}>
                <i className="fa fa-user-circle-o" /> {document.User.name}
                <span> </span>
                <i className="fa fa-clock-o" /> {formatDateTime(document.createdAt, 'LLLL', 1)}
              </small>
            </div>
            <div className={styles.markdownContainer}>
              <ReactMarkdown
                plugins={[breaks]}
                className="markdown-content d-inline-block"
                source={document.content}
                renderers={{ link: props => linkRenderTarget(props) }}
              />
            </div>
            <div className="text-right">
              {isAllowPermission(appPermissions, UPDATE_DOCUMENT_WIKI, selectedProject.id) && (
                <Button className={'base-btn mr-3'} size={'md'} onClick={() => _setStatusDocumentUI(3)}>
                  {t('edit_document')}
                </Button>
              )}
              {isAllowPermission(appPermissions, REMOVE_DOCUMENT_WIKI, selectedProject.id) && (
                <Button className={'outline-base-remove-btn'} size={'md'} onClick={openAlertRemove}>
                  {t('remove_document')}
                </Button>
              )}
              <ConfirmationModal
                isOpen={statusAlert}
                txtTitle={t('confirm_remove')}
                txtCancel={t('cancel')}
                txtConfirm={t('confirm')}
                funConfirm={() => handleRemoveDocument()}
                funClose={closeAlertRemove}
              >
                {t('are_you_sure_remove_this_document')}
              </ConfirmationModal>
            </div>
          </Fragment>
        )
      }
    </NamespacesConsumer>
  );
}

DocumentDetail.propTypes = {
  appPermissions: array,
  selectedProject: object,
  document: object,
  _removeDocument: func,
  _setStatusDocumentUI: func
};

const mapState = state => ({
  appPermissions: state.auth.permissions,
  selectedProject: state.sprint.selectedProject,
  document: state.wiki.selectedDocument
});

const mapDispatch = {
  _removeDocument: removeDocument,
  _setStatusDocumentUI: setStatusDocumentUI
};

export default connect(
  mapState,
  mapDispatch
)(memo(DocumentDetail));
