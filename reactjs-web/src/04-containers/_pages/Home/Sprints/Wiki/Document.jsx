import i18n from '../../../../../i18n';
import FieldInput from '../../../../../03-components/FieldInput';
import MarkdownEditor from '../../../../../03-components/MarkdownEditor';
import React, { memo, useEffect, useState } from 'react';
import { array, func, number, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { addDocument, setStatusDocumentUI, updateDocument } from '../../../../../01-actions/wiki';
import { TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';

function Document({
  selectedDocument,
  selectedProject,
  statusDocumentUI,
  setStatusDocumentUI,
  addDocument,
  updateDocument
}) {
  const [subject, setSubject] = useState('');
  const [content, setContent] = useState('');

  useEffect(() => {
    if (statusDocumentUI !== 1) {
      setSubject(selectedDocument.subject);
      setContent(selectedDocument.content);
    }
    if (!statusDocumentUI) {
      setSubject('');
      setContent('');
    }
  }, [statusDocumentUI]);

  const changeTitle = value => setSubject(value);

  const changeContent = v => setContent(v);

  const handleAddDocument = () => {
    const selectedProjectId = selectedProject.id;
    const parentId = selectedDocument ? selectedDocument.id : null;
    if (!selectedProjectId || !subject || !content) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      addDocument({ projectId: selectedProjectId, subject, content, parentId });
    }
    setStatusDocumentUI(0);
  };

  const handleUpdateDocument = () => {
    const selectedProjectId = selectedProject && selectedProject.id;
    const parentId = (selectedDocument && selectedDocument.parentId) || null;
    const documentId = selectedDocument && selectedDocument.id;

    if (
      selectedProjectId === null ||
      selectedProjectId === undefined ||
      !subject ||
      !content ||
      documentId === null ||
      documentId === undefined
    ) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      updateDocument({
        documentId,
        projectId: selectedProjectId,
        subject,
        content,
        parentId
      });
    }
    setStatusDocumentUI(0);
  };

  return (
    <NamespacesConsumer>
      {t => (
        <div>
          <Modal
            isOpen={!!statusDocumentUI}
            toggle={() => setStatusDocumentUI()}
            centered={true}
            size="lg"
            backdrop="static"
          >
            <ModalHeader toggle={() => setStatusDocumentUI()}>
              {statusDocumentUI === 1 && t('new_document')}

              {statusDocumentUI === 3 && t('update_document')}
            </ModalHeader>
            <ModalBody>
              {(statusDocumentUI === 1 || statusDocumentUI === 3) && (
                <FieldInput
                  classes="mb-5"
                  placeholder={'subject'}
                  type="text"
                  defaultValue={subject}
                  onChangeValue={changeTitle}
                />
              )}
              {(statusDocumentUI === 1 || statusDocumentUI === 3) && (
                <MarkdownEditor label={t('content')} value={content} onChange={content => changeContent(content)} />
              )}
            </ModalBody>
            <ModalFooter>
              <div className="d-flex justify-content-end align-items-center w-100p">
                <Button
                  className={'margin-right-10px outline-base-btn'}
                  size={'sm'}
                  onClick={() => setStatusDocumentUI(0)}
                >
                  {t('cancel')}
                </Button>
                {statusDocumentUI === 1 && (
                  <Button
                    className={'base-btn'}
                    size={'sm'}
                    disabled={!subject || !content}
                    onClick={() => handleAddDocument()}
                  >
                    {t('add')}
                  </Button>
                )}
                {statusDocumentUI === 3 && (
                  <Button
                    className={'base-btn'}
                    size={'sm'}
                    disabled={!subject || !content}
                    onClick={() => handleUpdateDocument()}
                  >
                    {t('save')}
                  </Button>
                )}
              </div>
            </ModalFooter>
          </Modal>
        </div>
      )}
    </NamespacesConsumer>
  );
}

Document.propTypes = {
  selectedDocument: object,
  selectedProject: object,
  statusDocumentUI: number,
  setStatusDocumentUI: func,
  addDocument: func,
  updateDocument: func,
  appPermissions: array
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  statusDocumentUI: store.wiki.statusDocumentUI,
  selectedProject: store.sprint.selectedProject,
  selectedDocument: store.wiki.selectedDocument
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setStatusDocumentUI,
      addDocument,
      updateDocument
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(Document));
