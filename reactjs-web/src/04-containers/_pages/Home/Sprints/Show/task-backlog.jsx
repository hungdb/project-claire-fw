import i18n from '../../../../../i18n';
import moment from 'moment';
import ChangeEstimateDialog from '../../../../dialog/change-estimate/change-estimate';
import Tag from '../../../../../03-components/Tag';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { parseJSON } from '../../../../../08-helpers/common';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';

class TaskBackLogDetail extends PureComponent {
  state = {
    openEstimate: false
  };

  static propTypes = {
    currentTask: PropTypes.object,
    changeEstimate: PropTypes.func,
    isEstimate: PropTypes.bool
  };

  onHandleClose = () => {
    this.setState({ openEstimate: false });
  };

  onHandleClickOpenEstimate = () => {
    this.setState({ openEstimate: true, estimate: 0 });
  };

  changeEstimate = data => {
    const { taskId, estimate, projectId } = data;
    const { changeEstimate } = this.props;
    if (!taskId || !estimate) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      changeEstimate({ taskId, estimate, projectId });
    }
    this.onHandleClose();
  };

  render() {
    const { currentTask } = this.props;
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            <div className="d-flex">
              <div className="task-content">{`#${currentTask.id} ${currentTask.content}`}</div>
            </div>
            <div>
              <div className={'margin-top-15px'}>{currentTask.description}</div>
              <div className="justify-content-between">
                <span className="strong-font">{t('created_at')}</span>
                <span>{moment(currentTask.createdAt).toString()}</span>
              </div>
              <div className="justify-content-between">
                <span className="strong-font">{t('created_by')}</span>
                <span>@{currentTask.creatorAs && currentTask.creatorAs.name}</span>
              </div>
              <div className="justify-content-between">
                <span className="strong-font">{t('tags')}</span>
                {currentTask.tag && parseJSON(currentTask.tag)
                  ? parseJSON(currentTask.tag).map((tag, index) => {
                      return <Tag key={index} name={tag} />;
                    })
                  : null}
              </div>
              <div className="justify-content-between">
                <span className="strong-font">{t('Estimate')}</span>
                <span>{currentTask.estimate ? currentTask.estimate + 'h' : t('unestimated')}</span>
                {this.props.isEstimate && (
                  <span className="link-style margin-left-15px" onClick={this.onHandleClickOpenEstimate}>
                    {t('change')}
                  </span>
                )}
                <ChangeEstimateDialog
                  isOpen={this.state.openEstimate}
                  value={this.props.currentTask}
                  onClose={() => {
                    this.onHandleClose();
                  }}
                  onOK={value => {
                    this.changeEstimate(value);
                  }}
                />
              </div>
            </div>
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

export default TaskBackLogDetail;
