import React, { memo } from 'react';
import { Badge } from 'reactstrap';
import { oneOfType, string, number } from 'prop-types';

const ProgressMulti = ({ children, color, className }) => (
  <Badge color={color} className={className}>
    {children}
  </Badge>
);

ProgressMulti.propTypes = {
  children: oneOfType([string, number]),
  color: string,
  className: string
};

export default memo(ProgressMulti);
