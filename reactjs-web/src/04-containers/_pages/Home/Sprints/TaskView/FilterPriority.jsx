import Select from 'react-select';
import React, { memo, useEffect } from 'react';
import { string, number, func, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleFilterPriority, handleClearFilterPriority } from '../../../../../01-actions/project-task-view';
import { PRIORITY_OPTIONS } from '../../../../../05-utils/commonData';

function FilterPriority({ priority, handleFilterPriority, handleClearFilterPriority, projectId }) {
  useEffect(() => {
    return () => {
      handleClearFilterPriority();
    };
  }, [projectId]);

  const value = PRIORITY_OPTIONS.find(p => p.value === priority) || '';

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('priority')}
          isClearable={true}
          value={value}
          onChange={handleFilterPriority}
          options={PRIORITY_OPTIONS}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterPriority.propTypes = {
  priority: oneOfType([string, number]),
  handleFilterPriority: func,
  handleClearFilterPriority: func,
  projectId: number
};

const mapStateToProps = store => ({
  priority: store.projectTaskView.filter.priority,
  projectId: store.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterPriority,
      handleClearFilterPriority
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterPriority));
