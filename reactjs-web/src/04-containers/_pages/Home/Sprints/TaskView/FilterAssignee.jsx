import Select from 'react-select';
import React, { memo, useEffect } from 'react';
import { string, number, func, array, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getUsersInProject,
  handleFilterAssignee,
  handleClearFilterAssignee
} from '../../../../../01-actions/project-task-view';
import { GET_USERS_OF_PROJECT } from '../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../08-helpers/common';

function FilterAssignee({
  appPermissions,
  projectId,
  assignee,
  optionsUser,
  handleFilterAssignee,
  getUsersInProject,
  handleClearFilterAssignee
}) {
  useEffect(() => {
    if (isAllowPermission(appPermissions, GET_USERS_OF_PROJECT, projectId)) {
      getUsersInProject(projectId);
    }
    return () => {
      handleClearFilterAssignee();
    };
  }, [projectId]);

  const value = optionsUser.find(u => u.value === assignee) || '';

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('assignee')}
          isClearable={true}
          isSearchable={true}
          value={value}
          onChange={handleFilterAssignee}
          options={optionsUser}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterAssignee.propTypes = {
  appPermissions: array,
  projectId: number,
  assignee: oneOfType([string, number]),
  optionsUser: array,
  handleFilterAssignee: func,
  getUsersInProject: func,
  handleClearFilterAssignee: func
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  projectId: store.sprint.selectedProject.id,
  assignee: store.projectTaskView.filter.assignee,
  optionsUser: store.projectTaskView.optionsUser
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterAssignee,
      getUsersInProject,
      handleClearFilterAssignee
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterAssignee));
