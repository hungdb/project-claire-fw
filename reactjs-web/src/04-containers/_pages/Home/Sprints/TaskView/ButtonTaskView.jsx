import React, { memo } from 'react';
import { oneOfType, string, number, func, element, node } from 'prop-types';

const ButtonTaskView = ({ children, className, onClick, title }) => (
  <div className={className} onClick={onClick} title={title}>
    {children}
  </div>
);

ButtonTaskView.propTypes = {
  children: oneOfType([string, number, element, node]),
  className: string,
  onClick: func,
  title: string
};

export default memo(ButtonTaskView);
