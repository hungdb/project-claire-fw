import Select from 'react-select';
import React, { memo, useEffect } from 'react';
import { array, func, string, number, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleFilterSprint, handleClearFilterSprint } from '../../../../../01-actions/project-task-view';

function FilterSprint({ sprint, handleFilterSprint, sprints, handleClearFilterSprint, projectId }) {
  const options = [
    {
      value: -1,
      label: 'Backlog'
    },
    ...sprints.map(e => ({
      value: e.id,
      label: e.name
    }))
  ];

  useEffect(() => {
    return () => {
      handleClearFilterSprint();
    };
  }, [projectId]);

  const value = options.find(s => s.value === sprint) || '';

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('sprint')}
          isClearable={true}
          isSearchable={true}
          value={value}
          onChange={handleFilterSprint}
          options={options}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterSprint.propTypes = {
  sprint: oneOfType([string, number]),
  sprints: array,
  handleFilterSprint: func,
  handleClearFilterSprint: func,
  projectId: number
};

const mapStateToProps = store => ({
  sprint: store.projectTaskView.filter.sprint,
  sprints: store.sprint.sprints,
  projectId: store.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterSprint,
      handleClearFilterSprint
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterSprint));
