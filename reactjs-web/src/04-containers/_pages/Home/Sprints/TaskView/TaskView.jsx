import './styles.css';
import queryString from 'query-string';
import XLSX from 'xlsx';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { memo, useEffect, useState } from 'react';
import { array, func, number, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { NOT_CLOSED_STATUS_TASK, ROWS_PER_PAGE_TASK_OPTIONS } from '../../../../../05-utils/commonData';
import {
  formatDateTime,
  isAllowPermission,
  promiseDelayImport,
  calculateLength
} from '../../../../../08-helpers/common';
import {
  handleFilterPage,
  handleFilterQueryUrl,
  handleFilterRowsPerPage,
  handleFilterViewType,
  handleClearAllFilter
} from '../../../../../01-actions/project-task-view';
import {
  FILTER_TASKS_BY_PROJECT,
  PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT
} from '../../../../../07-constants/app-permission';

const Pagination = LazyComponent(() => import('../../../../../03-components/Pagination'));
const FontAwesomeIcon = LazyComponent(() => import('../../../../../03-components/FontAwesomeIcon'));
const ProgressBar = LazyComponent(() => import('./ProgressBar'));
const FilterAssignee = LazyComponent(promiseDelayImport(import('./FilterAssignee')), <Skeleton height={38} />);
const FilterPriority = LazyComponent(promiseDelayImport(import('./FilterPriority')), <Skeleton height={38} />);
const FilterSearch = LazyComponent(promiseDelayImport(import('./FilterSearch')), <Skeleton height={38} />);
const FilterSprint = LazyComponent(promiseDelayImport(import('./FilterSprint')), <Skeleton height={38} />);
const FilterStartDate = LazyComponent(promiseDelayImport(import('./FilterStartDate')), <Skeleton height={38} />);
const FilterStatus = LazyComponent(promiseDelayImport(import('./FilterStatus')), <Skeleton height={38} />);
const GanttChart = LazyComponent(promiseDelayImport(import('./GanttChart')), <SpinLoader />);
const TableView = LazyComponent(promiseDelayImport(import('./TableView')), <SpinLoader />);
const ButtonTaskView = LazyComponent(
  promiseDelayImport(import('./ButtonTaskView')),
  <Skeleton width={43.22} height={36} />
);

function TaskView({
  rowsPerPage,
  page,
  totalTasks,
  projectId,
  projectName,
  appPermissions,
  sprint,
  priority,
  assignee,
  status,
  startDate,
  search,
  match,
  location,
  history,
  handleFilterRowsPerPage,
  handleFilterPage,
  handleFilterQueryUrl,
  tasks,
  viewType,
  handleFilterViewType,
  handleClearAllFilter
}) {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  useEffect(() => {
    const parsed = queryString.parse(location.search);

    const viewType = parsed.viewType ? Number(parsed.viewType) : 1;
    const limit = parsed.limit ? Number(parsed.limit) : ROWS_PER_PAGE_TASK_OPTIONS[0];
    const page = parsed.page ? Number(parsed.page) : 1;
    const sprint = parsed.sprint ? Number(parsed.sprint) : null;
    const priority = parsed.priority ? Number(parsed.priority) : null;
    const assignee = parsed.assignee ? Number(parsed.assignee) : null;
    const status = parsed.status ? Number(parsed.status) : NOT_CLOSED_STATUS_TASK.value;
    const startDate = parsed.startDate ? new Date(parsed.startDate) : null;
    const search = parsed.search ? parsed.search : '';

    handleFilterQueryUrl(sprint, priority, assignee, status, startDate, search, limit, page, viewType);
  }, []);

  // Handle URL
  useEffect(() => {
    let queryArr = [];
    const taskId = match.params.taskId;

    if (!taskId) {
      if (sprint) queryArr = [...queryArr, `sprint=${JSON.stringify(sprint)}`];
      if (priority) queryArr = [...queryArr, `priority=${JSON.stringify(priority)}`];
      if (assignee) queryArr = [...queryArr, `assignee=${JSON.stringify(assignee)}`];
      if (status) queryArr = [...queryArr, `status=${JSON.stringify(status)}`];
      if (startDate) queryArr = [...queryArr, `startDate=${startDate}`];
      if (search) queryArr = [...queryArr, `search=${search}`];
      if (viewType) queryArr = [...queryArr, `viewType=${JSON.stringify(viewType)}`];
      if (rowsPerPage) queryArr = [...queryArr, `limit=${JSON.stringify(rowsPerPage)}`];
      if (page) queryArr = [...queryArr, `page=${JSON.stringify(page)}`];
      const query = queryArr.reduce((total, item) => {
        if (!total) return `${total}?${item}`;
        return `${total}&${item}`;
      }, '');
      history.push(`/sprints/${projectId}/backlog${query}`);
    }
  }, [sprint, priority, assignee, status, startDate, search, rowsPerPage, page, viewType]);

  useEffect(() => {
    return () => {
      handleClearAllFilter();
    };
  }, []);

  const changeTabView = i => handleFilterViewType(i);

  const toggle = () => setDropdownOpen(!dropdownOpen);

  const exportFile = extension => {
    const data =
      (calculateLength(tasks) &&
        tasks.map((item, index) => {
          return {
            No: index + 1,
            Content: item.text,
            Type: item.typeText,
            Tag: item.tag.toString(),
            Assignee: item.assignee,
            Sprint: item.sprint,
            Status: item.status,
            Priority: item.priority,
            Estimate: item.estimate + 'h',
            StartDate: formatDateTime(item.start_date, 'LLLL', 1),
            DueDate: formatDateTime(item.due_date, 'LLLL', 1),
            EndDate: formatDateTime(item.end_date, 'LLLL', 1)
          };
        })) ||
      [];
    const ws = XLSX.utils.json_to_sheet(data);
    const wb = XLSX.utils.book_new();
    const fileName = `[${projectName}]_tasks_at_${formatDateTime(new Date(), 'DD_MM_YYYY_HH:mm', 1)}.${extension}`;
    XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');
    XLSX.writeFile(wb, fileName);
  };

  const isListTask = isAllowPermission(appPermissions, FILTER_TASKS_BY_PROJECT, projectId);

  const _renderBtnTabView = t => (
    <div className="d-flex justify-content-end align-items-center mb-2 mb-xl-3">
      <ButtonTaskView
        className={'btn-tab-view d-flex justify-content-center align-items-center'}
        title={t('export_tasks')}
      >
        <Dropdown toggle={toggle} isOpen={dropdownOpen}>
          <DropdownToggle color={'white'} outline style={{ boxShadow: 'none' }}>
            <FontAwesomeIcon classes="fa fa-download" />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => exportFile('xlsx')}>{t('file_excel')}</DropdownItem>
            <DropdownItem onClick={() => exportFile('csv')}>{t('file_csv')}</DropdownItem>
            <DropdownItem onClick={() => exportFile('ods')}>{t('file_ods')}</DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </ButtonTaskView>

      <div className="vertical" />

      <ButtonTaskView
        className={
          'btn-tab-view d-flex justify-content-center align-items-center' + (viewType === 1 ? ' selected' : '')
        }
        onClick={() => changeTabView(1)}
        title={t('show_table')}
      >
        <i className={'icon fa fa-list-alt'} />
      </ButtonTaskView>

      <div className="vertical" />

      <ButtonTaskView
        className={
          'btn-tab-view d-flex justify-content-center align-items-center' + (viewType === 2 ? ' selected' : '')
        }
        onClick={() => changeTabView(2)}
        title={t('show_chart')}
      >
        <i className={'icon fa fa-bar-chart'} />
      </ButtonTaskView>
    </div>
  );

  const _renderFilter = (
    <Row className="row-custom">
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterSprint />
      </Col>
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterPriority />
      </Col>
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterAssignee />
      </Col>
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterStatus />
      </Col>
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterStartDate />
      </Col>
      <Col md="4" xl="2" className="px-2 mb-3">
        <FilterSearch />
      </Col>
    </Row>
  );

  return (
    <NamespacesConsumer>
      {t => (
        <div className="p-4 default-paper backlog-area">
          <div className="my-4">
            {isAllowPermission(appPermissions, PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT, projectId) && <ProgressBar />}
          </div>
          <div className={'line mb-3'} />
          {isListTask && (
            <Row className="row-custom align-items-center">
              <Col md="12" xl="10" className="px-2">
                {_renderFilter}
              </Col>
              <Col md="12" xl="2" className="px-2">
                {_renderBtnTabView(t)}
              </Col>
            </Row>
          )}
          {isListTask && viewType === 1 && <TableView />}
          {isListTask && viewType === 2 && (
            <div className="pt-3">
              <GanttChart />
            </div>
          )}
          {isListTask && (
            <div className="mt-3">
              <Pagination
                totalItems={totalTasks}
                pageSize={rowsPerPage}
                value={rowsPerPage}
                onSelect={page => handleFilterPage(page)}
                onChange={e => handleFilterRowsPerPage(e.value)}
                rowsPerPageOptions={ROWS_PER_PAGE_TASK_OPTIONS}
                isClearable={false}
              />
            </div>
          )}
          {isListTask && viewType === 1 && (
            <div className={'margin-top-5px text-muted font-size-12 font-italic'}>* {t('estimated_due_date')}</div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TaskView.propTypes = {
  rowsPerPage: number,
  page: number,
  totalTasks: number,
  projectId: number,
  projectName: string,
  appPermissions: array,
  sprint: number,
  priority: number,
  assignee: number,
  status: number,
  startDate: string,
  search: string,
  match: object,
  location: object,
  history: object,
  handleFilterRowsPerPage: func,
  handleFilterPage: func,
  handleFilterQueryUrl: func,
  tasks: array,
  viewType: number,
  handleFilterViewType: func,
  handleClearAllFilter: func
};

const mapStateToProps = store => ({
  rowsPerPage: store.projectTaskView.filter.rowsPerPage,
  page: store.projectTaskView.filter.page,
  totalTasks: store.projectTaskView.totalTask,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  projectName: store.sprint.selectedProject && store.sprint.selectedProject.name,

  viewType: store.projectTaskView.filter.viewType,
  sprint: store.projectTaskView.filter.sprint,
  priority: store.projectTaskView.filter.priority,
  assignee: store.projectTaskView.filter.assignee,
  status: store.projectTaskView.filter.status,
  startDate: store.projectTaskView.filter.startDate,
  search: store.projectTaskView.filter.search,
  tasks: store.projectTaskView.tasks,

  appPermissions: store.auth.permissions
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterRowsPerPage,
      handleFilterPage,
      handleFilterQueryUrl,
      handleFilterViewType,
      handleClearAllFilter
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TaskView))
);
