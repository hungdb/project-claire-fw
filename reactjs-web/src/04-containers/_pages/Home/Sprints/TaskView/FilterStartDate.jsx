import 'react-datepicker/dist/react-datepicker.css';
import { func, object, number } from 'prop-types';
import React, { memo, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleFilterStartDate, handleClearFilterStartDate } from '../../../../../01-actions/project-task-view';

function FilterStartDate({ startDate, handleFilterStartDate, handleClearFilterStartDate, projectId }) {
  useEffect(() => {
    return () => {
      handleClearFilterStartDate();
    };
  }, [projectId]);

  return (
    <NamespacesConsumer>
      {t => (
        <div className={'input-filter-search d-flex justify-content-between align-items-center'}>
          <div className="date-container">
            <DatePicker
              className="data-picker"
              placeholderText={t('start_date')}
              isClearable={true}
              selected={startDate}
              onChange={handleFilterStartDate}
            />
          </div>
          <i className={'icon fa fa-calendar mr-1 ml-2'} aria-hidden="true" />
        </div>
      )}
    </NamespacesConsumer>
  );
}

FilterStartDate.propTypes = {
  startDate: object,
  handleFilterStartDate: func,
  handleClearFilterStartDate: func,
  projectId: number
};

const mapStateToProps = store => ({
  startDate: store.projectTaskView.filter.startDate,
  projectId: store.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterStartDate,
      handleClearFilterStartDate
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterStartDate));
