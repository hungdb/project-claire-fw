import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import i18n from '../../../../../i18n';
import React, { memo, useRef, useEffect } from 'react';
import { object, number, string, array, func } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTasksInProject } from '../../../../../01-actions/project-task-view';

function GanttChart({
  getTasksInProject,
  rowsPerPage,
  page,
  projectId,
  sprintId,
  userId,
  priorityId,
  statusId,
  startDate,
  search,
  tasks,
  dependenTasks
}) {
  const ganttContainer = useRef(null);

  const initGanttChart = () => {
    window.gantt.config.readonly = true;
    // window.gantt.config.show_errors = false;
    window.gantt.config.scale_height = 50;
    window.gantt.config.scale_unit = 'month';
    window.gantt.config.date_scale = '%F, %Y';
    window.gantt.config.subscales = [{ unit: 'day', step: 1, date: '%j, %D' }];
    window.gantt.config.columns = [
      { name: 'text', label: i18n.t('task'), width: 370, min_width: 370 },
      { name: 'typeText', label: i18n.t('task_type'), width: 50, min_width: 50, align: 'center' },
      { name: 'estimate', label: i18n.t('estimate'), width: 50, min_width: 50, align: 'center' },
      { name: 'start_date', label: i18n.t('start'), width: 100, min_width: 100, align: 'center' },
      { name: 'end_date', label: i18n.t('end'), width: 100, min_width: 100, align: 'center' },
      { name: 'assignee', label: i18n.t('owner'), width: 100, min_width: 100, align: 'center' },
      { name: 'sprint', label: i18n.t('sprint'), width: 100, min_width: 100, align: 'center' },
      { name: 'status', label: i18n.t('status'), width: 80, min_width: 80, align: 'center' },
      { name: 'priority', label: i18n.t('priority'), width: 80, min_width: 80, align: 'center' }
    ];
    window.gantt.config.layout = {
      css: 'gantt_container',
      cols: [
        {
          width: window.innerWidth >= 992 ? 425 : 200,
          rows: [
            {
              view: 'grid',
              scrollX: 'scrollHorGrid',
              scrollY: 'scrollVer',
              scrollable: true
            },
            {
              view: 'scrollbar',
              id: 'scrollHorGrid'
            }
          ]
        },
        {
          rows: [
            {
              view: 'timeline',
              scrollX: 'scrollHorTimeline',
              scrollY: 'scrollVer'
            },
            {
              view: 'scrollbar',
              id: 'scrollHorTimeline'
            }
          ]
        },
        {
          view: 'scrollbar',
          id: 'scrollVer'
        }
      ]
    };
    window.gantt.init(ganttContainer.current);
  };

  const resizeGanttChart = () => {
    clearTimeout(window.timer);
    window.timer = setTimeout(() => {
      initGanttChart();
    }, 300);
  };

  const parseData = (data, links) => {
    for (let i = 0; i < data.length; i++) {
      let obj = data[i];
      if (!obj.start_date) {
        obj = { ...obj, start_date: new Date() };
      }
      if (!obj.end_date) {
        obj = { ...obj, end_date: new Date() };
      }
      data[i] = obj;
    }
    window.gantt.parse({
      data: data,
      links
    });
  };

  useEffect(() => {
    getTasksInProject(rowsPerPage, page + 1, projectId, sprintId, userId, priorityId, statusId, startDate, search);

    window.addEventListener('resize', resizeGanttChart);

    initGanttChart();
    parseData(tasks, dependenTasks);
    return () => {
      window.removeEventListener('resize', resizeGanttChart);
      window.gantt.clearAll();
    };
  }, []);

  useEffect(() => {
    getTasksInProject(rowsPerPage, page + 1, projectId, sprintId, userId, priorityId, statusId, startDate, search);
  }, [rowsPerPage, page, projectId, sprintId, userId, priorityId, statusId, startDate, search]);

  useEffect(() => {
    window.gantt.clearAll();
    parseData(tasks, dependenTasks);
  }, [tasks, dependenTasks, rowsPerPage]);

  return <div ref={ganttContainer} style={{ width: '100%', height: '65vh' }} />;
}

GanttChart.propTypes = {
  getTasksInProject: func,
  rowsPerPage: number,
  page: number,
  projectId: number,
  sprintId: number,
  userId: number,
  priorityId: number,
  statusId: number,
  startDate: object,
  search: string,
  tasks: array,
  dependenTasks: array
};

const mapStateToProps = state => ({
  rowsPerPage: state.projectTaskView.filter.rowsPerPage,
  page: state.projectTaskView.filter.page,
  projectId: state.sprint.selectedProject.id,
  sprintId: state.projectTaskView.filter.sprint ? state.projectTaskView.filter.sprint : null,
  userId: state.projectTaskView.filter.assignee ? state.projectTaskView.filter.assignee : null,
  priorityId: state.projectTaskView.filter.priority ? state.projectTaskView.filter.priority : null,
  statusId: state.projectTaskView.filter.status ? state.projectTaskView.filter.status : null,
  startDate: state.projectTaskView.filter.startDate,
  search: state.projectTaskView.filter.search,
  tasks: state.projectTaskView.tasks.reverse(),
  dependenTasks: state.projectTaskView.dependenTasks
});

const mapDispatchToProps = dispatch => bindActionCreators({ getTasksInProject }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(GanttChart));
