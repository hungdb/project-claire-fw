import moment from 'moment';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useEffect, useState } from 'react';
import { array, func, number, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Badge, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getTasksInProject } from '../../../../../01-actions/project-task-view';
import { PRIORITY_OPTIONS } from '../../../../../05-utils/commonData';
import { UPDATE_TASK_BACKLOG } from '../../../../../07-constants/app-permission';
import { formatDateTime, isAllowPermission } from '../../../../../08-helpers/common';
import { getEnv } from '../../../../../env';
import {
  clearCurrentTask,
  deleteTask,
  getDetailTask,
  getUsersOfTask,
  updateBacklogTask
} from '../../../../../01-actions/sprint';

const TableHeadCol = LazyComponent(() => import('../../../../../03-components/TableHead'));
const Tag = LazyComponent(() => import('../../../../../03-components/Tag'));
const TaskType = LazyComponent(() => import('../../../../../03-components/TaskType'));
const DetailTaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/detail-task-dialog'));
const TaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/task-dialog'));
const TooltipWithChildren = LazyComponent(() => import('../../../../../03-components/TooltipWithChildren'));

const LINK_DOMAIN_PROJECT = getEnv('LINK_DOMAIN_PROJECT');

const fields = [
  'content',
  'task_type',
  'tag',
  'assignee',
  'sprint',
  'status',
  'priority',
  'estimate',
  'start_date',
  'due_date',
  'end_date',
  'action'
];

const arrayClass = ['w-30p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-10p'];

const p6x_center = 'text-center align-middle';

function TableView({
  rowsPerPage,
  page,
  projectId,
  sprintId,
  userId,
  priorityId,
  statusId,
  startDate,
  search,
  appPermissions,
  tags,
  tasks,
  currentTask,
  match,
  history,
  actions
}) {
  const [showDetailTask, setShowDetailTask] = useState(false);
  const [taskId, setTaskId] = useState(null);
  const [isUpdateTask, setIsUpdateTask] = useState(false);

  const viewDetailTask = taskId => {
    actions.clearCurrentTask();
    actions.getDetailTask({ taskId, projectId });
    actions.getUsersOfTask({ taskId, projectId });
    setShowDetailTask(true);
    setTaskId(taskId);
  };

  const openUpdateTask = taskId => {
    actions.getDetailTask({ taskId, projectId });
    setIsUpdateTask(true);
  };

  // get tasks
  useEffect(() => {
    actions.getTasksInProject(rowsPerPage, page, projectId, sprintId, userId, priorityId, statusId, startDate, search);
  }, [actions, rowsPerPage, page, projectId, sprintId, userId, priorityId, statusId, startDate, search]);

  // show detail when reload page
  useEffect(() => {
    const taskId = match.params.taskId;
    if (taskId && !isNaN(taskId)) {
      viewDetailTask(taskId);
    }
  }, []);

  const deleteBacklogTask = () => {
    actions.deleteTask({ taskId: currentTask.id, projectId });
    actions.getTasksInProject(
      rowsPerPage,
      page + 1,
      projectId,
      sprintId,
      userId,
      priorityId,
      statusId,
      startDate,
      search
    );
    setIsUpdateTask(false);
    history.push(`/sprints/${projectId}/backlog`);
  };

  const computeDefaultEndDate = (startTaskDate, time) => {
    const dayDuration = Math.trunc((time - (time % 8)) / 8);
    const defaultStartDate = moment(startTaskDate);

    for (let i = 0; i < dayDuration; i++) {
      defaultStartDate.add(1, 'day');
      // if Sat then add 2 days, Sun then add one day
      if ([0, 6].includes(defaultStartDate.day())) {
        defaultStartDate.add(defaultStartDate.day() === 0 ? 1 : 2, 'day');
      }
    }
    return defaultStartDate;
  };

  const updateBacklogTask = data => {
    const { content, estimate, description, tags, priority, dependencies, assignee, dueDate, type, userRelate } = data;
    const { id } = currentTask;

    const dataTag = tags.map(tag => tag.text);
    actions.updateBacklogTask({
      taskId: id,
      estimate,
      content,
      description,
      tag: dataTag,
      projectId,
      priority,
      assignee,
      dependencies,
      dueDate,
      type,
      userRelate
    });

    actions.getTasksInProject(rowsPerPage, page, projectId, sprintId, userId, priorityId, statusId, startDate, search);
    setIsUpdateTask(false);
    history.push(`/sprints/${projectId}/backlog`);
  };

  const changeTaskRoute = taskId => {
    history.push(`/sprints/${projectId}/backlog/${taskId}`);
  };

  const closeDialog = () => {
    actions.clearCurrentTask();
    setShowDetailTask(false);
    setIsUpdateTask(false);
    history.push(`/sprints/${projectId}/backlog`);
  };

  const handleDetailTask = taskId => {
    viewDetailTask(taskId);
    changeTaskRoute(taskId, 1);
  };

  const onDirectToTask = taskId => setTimeout(() => handleDetailTask(taskId), 300);

  const suggestions = tags.map(suggestion => ({
    id: suggestion,
    text: suggestion
  }));

  // Share link
  const onClickCopyShareLink = item => {
    const linkShare = `${LINK_DOMAIN_PROJECT}/sprints/${projectId}/backlog/${item.id}`;
    var textField = document.createElement('textarea');
    textField.innerText = linkShare;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
  };

  const renderBtn = (row, t) => (
    <>
      {isAllowPermission(appPermissions, UPDATE_TASK_BACKLOG, projectId) && (
        <TooltipWithChildren id={`update-${row.id}`} placement={'top'} content={t('update')}>
          <span
            className={'cursor-pointer'}
            onClick={() => {
              openUpdateTask(row.id);
              changeTaskRoute(row.id);
            }}
          >
            <i className="fa fa-pencil color-base" />
          </span>
        </TooltipWithChildren>
      )}
      <TooltipWithChildren id={`show-${row.id}`} placement={'top'} content={t('show')}>
        <span
          className={'margin-left-5px cursor-pointer'}
          onClick={() => {
            viewDetailTask(row.id);
            changeTaskRoute(row.id);
          }}
        >
          <i className="fa fa-eye color-base" />
        </span>
      </TooltipWithChildren>
      <TooltipWithChildren id={`clipboard-${row.id}`} placement={'top'} content={t('copy_link_task')}>
        <span className={'margin-left-5px cursor-pointer'} onClick={() => onClickCopyShareLink(row)}>
          <i className="fa fa-clipboard color-base" />
        </span>
      </TooltipWithChildren>
    </>
  );

  return (
    <NamespacesConsumer>
      {t => (
        <div className="table-wrap">
          <Table className="table-view" hover responsive striped bordered>
            <thead>
              <tr>
                <TableHeadCol fields={fields} t={t} classes={arrayClass} />
              </tr>
            </thead>
            <tbody>
              {tasks.map(row => {
                return (
                  <tr className="table-row font-size-14" key={row.id}>
                    <td className="p-6px">{row.text}</td>
                    <td className={p6x_center}>
                      <TaskType type={row.typeCode} />
                    </td>
                    <td className={p6x_center}>
                      {row.tag.map((t, i) => {
                        return (
                          <p key={i} className="mb-1">
                            <Tag name={t} />
                          </p>
                        );
                      })
                      // row.tag && JSON.parse(row.tag).map((e, i) => <Tag key={ i } name={ e }/>)
                      }
                    </td>
                    <td className={p6x_center}>{row.assignee}</td>
                    <td className={p6x_center}>{row.sprint}</td>
                    <td className="p-6px">
                      {row.status === 'untouched' ? (
                        <Badge color="info" className="badge-status">
                          {t('untouched')}
                        </Badge>
                      ) : row.status === 'progress' ? (
                        <Badge color="warning" className="badge-status">
                          {t('progress')}
                        </Badge>
                      ) : row.status === 'holding' ? (
                        <Badge color="primary" className="badge-status">
                          {t('holding')}
                        </Badge>
                      ) : row.status === 'terminated' ? (
                        <Badge color="success" className="badge-status">
                          {t('terminated')}
                        </Badge>
                      ) : (
                        row.status
                      )}
                    </td>
                    <td className={p6x_center}>
                      {row.priority === PRIORITY_OPTIONS[2].label ? (
                        <span className="text-muted">{t('low')}</span>
                      ) : row.priority === PRIORITY_OPTIONS[1].label ? (
                        <span className="text-info">{t('medium')}</span>
                      ) : row.priority === PRIORITY_OPTIONS[0].label ? (
                        <span className="text-warning">{t('high')}</span>
                      ) : (
                        row.priority
                      )}
                    </td>
                    {/*<td className='p-6px'>*/}
                    {/*<div>{row.create_date ? formatDateTime(row.create_date, 'DD/MM/YYYY') : '' }</div>*/}
                    {/*</td>*/}
                    <td className={p6x_center}>{row.estimate + 'h'}</td>
                    <td className={p6x_center}>{row.start_date ? formatDateTime(row.start_date, 'DD/MM/YYYY') : ''}</td>
                    <td className={p6x_center}>
                      {row.due_date
                        ? formatDateTime(row.due_date, 'DD/MM/YYYY')
                        : row.start_date
                        ? formatDateTime(computeDefaultEndDate(row.start_date, row.estimate), 'DD/MM/YYYY') + ' *'
                        : ''}
                    </td>
                    <td className={p6x_center}>{row.end_date ? formatDateTime(row.end_date, 'DD/MM/YYYY') : ''}</td>
                    <td className={p6x_center}>{renderBtn(row, t)}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          {showDetailTask && (
            <DetailTaskDialog
              isOpen={showDetailTask}
              value={{ taskId, projectId }}
              onClose={closeDialog}
              onDirectToTask={taskId => onDirectToTask(taskId)}
            />
          )}
          {isUpdateTask && (
            <TaskDialog
              isOpen={isUpdateTask}
              editMod={true}
              title={t('update_task')}
              // value={ currentTask }
              suggestions={suggestions}
              onClose={closeDialog}
              onDelete={deleteBacklogTask}
              onOK={updateBacklogTask}
            />
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TableView.propTypes = {
  rowsPerPage: number,
  page: number,
  projectId: number,
  sprintId: number,
  userId: number,
  priorityId: number,
  statusId: number,
  startDate: object,
  search: string,
  totalTasks: number,
  appPermissions: array,
  tags: array,
  tasks: array,
  currentTask: object,
  classes: object,
  location: object,
  match: object,
  history: object,
  actions: object,
  clearCurrentTask: func,
  getUsersOfTask: func,
  getDetailTask: func
};

const mapStateToProps = state => ({
  rowsPerPage: state.projectTaskView.filter.rowsPerPage,
  page: state.projectTaskView.filter.page,
  projectId: state.sprint.selectedProject.id,
  sprintId: state.projectTaskView.filter.sprint ? state.projectTaskView.filter.sprint : null,
  userId: state.projectTaskView.filter.assignee ? state.projectTaskView.filter.assignee : null,
  priorityId: state.projectTaskView.filter.priority ? state.projectTaskView.filter.priority : null,
  statusId: state.projectTaskView.filter.status ? state.projectTaskView.filter.status : null,
  startDate: state.projectTaskView.filter.startDate,
  search: state.projectTaskView.filter.search,
  tasks: state.projectTaskView.tasks,
  totalTasks: state.projectTaskView.totalTask,
  appPermissions: state.auth.permissions,
  currentTask: state.sprint.currentTask,
  tags: state.sprint.tags
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      clearCurrentTask,
      getDetailTask,
      getUsersOfTask,
      deleteTask,
      updateBacklogTask,
      getTasksInProject
    },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TableView))
);
