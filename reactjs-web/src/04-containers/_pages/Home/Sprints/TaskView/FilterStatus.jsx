import Select from 'react-select';
import React, { useEffect, memo } from 'react';
import { string, number, array, func, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getWorkFlowStateInProject,
  handleFilterStatus,
  handleClearFilterStatus
} from '../../../../../01-actions/project-task-view';
import { LIST_WORKFLOW_STATE_BY_PROJECT } from '../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../08-helpers/common';

function FilterStatus({
  appPermissions,
  projectId,
  status,
  optionsStatus,
  handleFilterStatus,
  getWorkFlowStateInProject,
  handleClearFilterStatus
}) {
  useEffect(() => {
    if (isAllowPermission(appPermissions, LIST_WORKFLOW_STATE_BY_PROJECT, projectId))
      getWorkFlowStateInProject(projectId);

    return () => {
      handleClearFilterStatus();
    };
  }, [projectId]);

  const value = optionsStatus.find(s => s.value === status) || '';

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('status')}
          isClearable={true}
          value={value}
          onChange={handleFilterStatus}
          options={optionsStatus}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterStatus.propTypes = {
  appPermissions: array,
  projectId: number,
  status: oneOfType([string, number]),
  optionsStatus: array,
  handleFilterStatus: func,
  getWorkFlowStateInProject: func,
  handleClearFilterStatus: func
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  projectId: store.sprint.selectedProject.id,
  status: store.projectTaskView.filter.status,
  optionsStatus: store.projectTaskView.optionsStatus
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterStatus,
      getWorkFlowStateInProject,
      handleClearFilterStatus
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterStatus));
