import InputIcon from '../../../../../03-components/InputIcon';
import { func, string, number } from 'prop-types';
import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleFilterSearch, handleClearFilterSearch } from '../../../../../01-actions/project-task-view';

function FilterSearch({ search, handleFilterSearch, handleClearFilterSearch, projectId }) {
  useEffect(() => {
    return () => {
      handleClearFilterSearch();
    };
  }, [projectId]);

  const changeSearch = value => handleFilterSearch(value);

  return (
    <div className="d-flex align-items-center mr-3 input-search">
      <InputIcon
        awesomeIcon="fa fa-search"
        type="text"
        placeholder="search"
        handleInput={changeSearch}
        value={search}
      />
    </div>
  );
}

FilterSearch.propTypes = {
  search: string,
  handleFilterSearch: func,
  handleClearFilterSearch: func,
  projectId: number
};

const mapStateToProps = store => ({
  search: store.projectTaskView.filter.search,
  projectId: store.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterSearch,
      handleClearFilterSearch
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterSearch));
