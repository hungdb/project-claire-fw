import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { Fragment, memo, useEffect, useMemo } from 'react';
import { object, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getStatistical } from '../../../../../01-actions/project-task-view';
import { percent, promiseDelayImport } from '../../../../../08-helpers/common';

const BadgeTotal = LazyComponent(
  promiseDelayImport(import('./BadgeTotal')),
  <div className="border-radius-backlog">
    <Skeleton height={28} />
  </div>
);

const ProgressMulti = LazyComponent(promiseDelayImport(import('./ProgressMulti')), <Skeleton />);
const Text = LazyComponent(promiseDelayImport(import('./../../../../../03-components/Text')), <Skeleton width={60} />);

function ProgressBar({ getStatistical, untouched, progress, holding, terminated, projectId }) {
  useEffect(() => {
    getStatistical(projectId);
  }, [projectId]);

  const total = untouched + progress + holding + terminated;
  const percentUntouched = useMemo(() => percent(untouched, total), [untouched, total]);
  const percentProgress = useMemo(() => percent(progress, total), [progress, total]);
  const percentHolding = useMemo(() => percent(holding, total), [holding, total]);
  const percentTerminated = useMemo(() => percent(terminated, total), [terminated, total]);

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          <ProgressMulti
            percentUntouched={percentUntouched}
            percentProgress={percentProgress}
            percentHolding={percentHolding}
            percentTerminated={percentTerminated}
          />
          <Text className="badge-text mb-2 text-left font-weight-bold">
            {terminated}/{total}
          </Text>
          <Row>
            <Col xs="6" sm="3" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('untouched')}</Text>
              <BadgeTotal color="info" className="badge-backlog px-4 py-2">
                {untouched}
              </BadgeTotal>
            </Col>
            <Col xs="6" sm="3" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('progress')}</Text>
              <BadgeTotal color="warning" className="badge-backlog px-4 py-2">
                {progress}
              </BadgeTotal>
            </Col>
            <Col xs="6" sm="3" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('holding')}</Text>
              <BadgeTotal color="primary" className="badge-backlog px-4 py-2">
                {holding}
              </BadgeTotal>
            </Col>
            <Col xs="6" sm="3" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('terminated')}</Text>
              <BadgeTotal color="success" className="badge-backlog px-4 py-2">
                {terminated}
              </BadgeTotal>
            </Col>
          </Row>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

ProgressBar.propTypes = {
  classes: object,
  getStatistical: func,
  untouched: number,
  progress: number,
  holding: number,
  terminated: number,
  projectId: number
};

const mapStateToProps = state => ({
  untouched: state.projectTaskView.progressBar.untouched,
  progress: state.projectTaskView.progressBar.progress,
  holding: state.projectTaskView.progressBar.holding,
  terminated: state.projectTaskView.progressBar.terminated,
  projectId: state.sprint.selectedProject.id
});

const mapDispatchToProp = dispatch => {
  return bindActionCreators(
    {
      getStatistical
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProp
)(memo(ProgressBar));
