import React, { memo } from 'react';
import { Progress } from 'reactstrap';
import { number } from 'prop-types';

const ProgressMulti = ({ percentUntouched, percentProgress, percentHolding, percentTerminated }) => (
  <Progress multi>
    <Progress bar color="info" value={percentUntouched} />
    <Progress bar color="warning" value={percentProgress} />
    <Progress bar color="primary" value={percentHolding} />
    <Progress bar color="success" value={percentTerminated} />
  </Progress>
);

ProgressMulti.propTypes = {
  percentUntouched: number,
  percentProgress: number,
  percentHolding: number,
  percentTerminated: number
};

export default memo(ProgressMulti);
