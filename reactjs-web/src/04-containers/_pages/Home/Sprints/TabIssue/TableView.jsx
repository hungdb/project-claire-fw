import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { memo, useEffect, useState } from 'react';
import queryString from 'query-string';
import { array, number, func, object, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Badge, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { formatDateTime, isAllowPermission } from '../../../../../08-helpers/common';
import {
  listIssues,
  statisticalIssueOfProject,
  removeIssue,
  updateIssue,
  handleFilterBySearch,
  handleFilterByStatus,
  handleFilterByCreator,
  handleFilterByLimit,
  handleFilterByPage,
  clearFilterCreator,
  clearFilterLimit,
  clearFilterPage,
  clearFilterSearch,
  clearFilterStatus
} from '../../../../../01-actions/issue';
import { createBacklogTask, getUsersByProject } from '../../../../../01-actions/sprint';
import { ROWS_PER_PAGE_OPTIONS, ISSUE_STATUS, TASK_TYPE_OBJECT } from '../../../../../05-utils/commonData';
import { UPDATE_ISSUE } from '../../../../../07-constants/app-permission';
import { getEnv } from '../../../../../env';
import {
  CREATE_ISSUE_SUCCESS,
  CREATE_MULTIPLE_ISSUE_SUCCESS,
  CLOSE_ALL_ISSUES_SUCCESS
} from '../../../../../07-constants/issue';
import { CREATE_TASK_SUCCESS } from '../../../../../07-constants/sprint';

const ViewIssueDialog = LazyComponent(() => import('../../../../dialog/issue/view-issue'));
const TableHeadCol = LazyComponent(() => import('../../../../../03-components/TableHead'));
const TaskType = LazyComponent(() => import('../../../../../03-components/TaskType'));
const Pagination = LazyComponent(() => import('../../../../../03-components/Pagination'));
const TaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/task-dialog'));
const IssueDialog = LazyComponent(() => import('../../../../dialog/issue/issue-dialog'));
const ConfirmationModal = LazyComponent(() => import('../../../../../03-components/ConfirmationModal'));
const TooltipWithChildren = LazyComponent(() => import('../../../../../03-components/TooltipWithChildren'));

const LINK_DOMAIN_PROJECT = getEnv('LINK_DOMAIN_PROJECT');

const fields = [
  'subject',
  'status',
  'type',
  'task_state',
  'estimate',
  'comments',
  'assignee',
  'creator',
  'createdAt',
  'action'
];

const arrayClass = ['w-30p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-5p', 'w-10p'];

const p6x_center = 'text-center align-middle';

function TableView({
  appPermissions,
  issues,
  total,
  projectId,
  listIssues,
  match,
  history,
  removeIssue,
  usersIntoProject,
  updateIssue,
  getUsersByProject,
  userId,
  subject,
  status,
  creator,
  handleFilterBySearch,
  handleFilterByStatus,
  handleFilterByCreator,
  limit,
  page,
  handleFilterByLimit,
  handleFilterByPage,
  location,
  clearFilterCreator,
  clearFilterLimit,
  clearFilterPage,
  clearFilterSearch,
  clearFilterStatus,
  statusIssue,
  statusSprint,
  createBacklogTask
}) {
  const [flagChange, setFlagChange] = useState(false);
  const [issueId, setIssueId] = useState(null);
  const [issue, setIssue] = useState(null);
  const [isOpenShowIssue, setIsOpenShowIssue] = useState(false);
  const [isOpenUpdateIssue, setIsOpenUpdateIssue] = useState(false);
  const [isAlertRemoveIssue, setIsAlertRemoveIssue] = useState(false);
  const [isAlertCloseIssue, setIsAlertCloseIssue] = useState(false);
  const [isOpenCreateTask, setIsOpenCreateTask] = useState(false);
  const [taskItem, setTaskItem] = useState({
    subject: null,
    issueId: null,
    description: null,
    typeTask: TASK_TYPE_OBJECT.NORMAL,
    estimate: 0,
    assignee: null,
    userRelate: []
  });

  const clearIssueState = () => {
    setIssueId(null);
    setIssue(null);
  };

  // handle when create, close issue
  useEffect(() => {
    const isGenerateTask = statusSprint === CREATE_TASK_SUCCESS;

    if (isGenerateTask) {
      handleFilterByStatus({ value: ISSUE_STATUS.OPEN });
      handleFilterByPage(1);
      setFlagChange(flagChange => !flagChange);
      statisticalIssueOfProject({ projectId });
    }
  }, [statusSprint]);

  useEffect(() => {
    const isCreatedSingle = statusIssue === CREATE_ISSUE_SUCCESS;
    const isCreatedFile = statusIssue === CREATE_MULTIPLE_ISSUE_SUCCESS;
    const isCloseAll = statusIssue === CLOSE_ALL_ISSUES_SUCCESS;

    if (isCloseAll) {
      handleFilterByStatus({ value: ISSUE_STATUS.OPEN });
      handleFilterByPage(1);
      setFlagChange(flagChange => !flagChange);
      statisticalIssueOfProject({ projectId });
    }

    if (isCreatedSingle || isCreatedFile) {
      handleFilterByStatus({ value: ISSUE_STATUS.OPEN });
      handleFilterByPage(1);
      setFlagChange(flagChange => !flagChange);
    }
  }, [statusIssue]);

  // update props by change url
  useEffect(() => {
    const parsed = queryString.parse(location.search);

    const subjectQuery = parsed.subject ? parsed.subject : '';
    const statusQuery = parsed.status ? Number(parsed.status) : ISSUE_STATUS.OPEN;
    const creatorQuery = parsed.creator ? Number(parsed.creator) : 0;
    const limitQuery = parsed.limit ? Number(parsed.limit) : ROWS_PER_PAGE_OPTIONS[1];
    const pageQuery = parsed.page ? Number(parsed.page) : 1;
    handleFilterBySearch(subjectQuery);
    handleFilterByStatus({ value: statusQuery });
    handleFilterByCreator({ value: creatorQuery });
    handleFilterByLimit(limitQuery);
    handleFilterByPage(pageQuery);
  }, []);

  // Handle URL
  useEffect(() => {
    let queryArr = [];
    const issueId = match.params.issueId;

    if (!issueId) {
      if (subject) queryArr = [...queryArr, `subject=${subject}`];
      if (status) queryArr = [...queryArr, `status=${JSON.stringify(status)}`];
      if (creator) queryArr = [...queryArr, `creator=${JSON.stringify(creator)}`];
      if (limit) queryArr = [...queryArr, `limit=${JSON.stringify(limit)}`];
      if (page) queryArr = [...queryArr, `page=${JSON.stringify(page)}`];
      const query = queryArr.reduce((total, item) => {
        if (!total) return `${total}?${item}`;
        return `${total}&${item}`;
      }, '');
      history.push(`/sprints/${projectId}/issues${query}`);
    }
  }, [subject, status, creator, limit, page]);

  useEffect(() => {
    listIssues({
      status,
      limit,
      page,
      projectId,
      subject,
      creator
    });
  }, [projectId, subject, status, creator, limit, page, flagChange]);

  // clear data filter when change project
  useEffect(() => {
    return () => {
      clearFilterCreator();
      clearFilterLimit();
      clearFilterPage();
      clearFilterSearch();
      clearFilterStatus();
      clearIssueState();
    };
  }, []);

  // show detail when reload page
  useEffect(() => {
    const issueId = match.params.issueId;
    if (issueId && !isNaN(issueId)) {
      setIssueId(issueId);
      setIsOpenShowIssue(true);
    }
  }, []);

  // change option paginate
  const handleChangePage = page => {
    listIssues({
      limit,
      page,
      projectId,
      status,
      subject,
      creator
    });
  };

  const handleChangeRowsPerPage = limit => {
    listIssues({
      limit,
      page,
      projectId,
      status,
      subject,
      creator
    });
  };

  // generate issue -> task
  const submitCreateTask = data => {
    const {
      content,
      estimate,
      description,
      tags,
      priority,
      dependencies,
      assignee,
      dueDate,
      files,
      type,
      issueId,
      userRelate
    } = data;
    const dataTag = tags.map(tag => tag.text);

    createBacklogTask({
      issueId,
      projectId,
      estimate,
      content,
      description,
      tag: dataTag,
      priority,
      assignee,
      dependencies,
      dueDate,
      files,
      type,
      userRelate
    });
    setIsOpenCreateTask(false);
  };

  const generateTask = issue => {
    setIsOpenCreateTask(true);
    const typeTask = Object.values(TASK_TYPE_OBJECT).includes(issue.typeTask)
      ? issue.typeTask
      : TASK_TYPE_OBJECT.NORMAL;
    setTaskItem({
      subject: issue.subject,
      issueId: issue.id,
      description: issue.content,
      typeTask,
      estimate: issue.estimate,
      assignee: issue.assignee,
      userRelate: issue.userRelate
    });
  };

  // Remove Issue
  const openAlertRemove = () => setIsAlertRemoveIssue(true);

  const closeAlertRemove = () => setIsAlertRemoveIssue(false);

  const handleRemoveIssue = () => {
    removeIssue({ id: issue.id });
    setIsOpenUpdateIssue(false);
    closeAlertRemove();
    clearIssueState();
  };

  // Update Issue
  const closeIssueDialog = () => {
    setIsOpenUpdateIssue(false);
    clearIssueState();
  };

  const onUpdateIssue = ({ subject, content, estimate, userRelate, assignee, typeTask }) => {
    updateIssue({
      id: issue.id,
      subject,
      content,
      estimate,
      userRelate,
      assignee,
      typeTask
    });
    setIsOpenUpdateIssue(false);
    clearIssueState();
  };

  const onClickUpdateIssue = item => {
    setIsOpenUpdateIssue(true);
    setIssue(item);
    getUsersByProject({ projectId });
  };

  // Show Issue
  const onClickShowIssue = item => {
    history.push(`/sprints/${projectId}/issues/${item.id}`);
    setIssueId(item.id);
    setIsOpenShowIssue(true);
  };

  const onCloseViewIssue = () => {
    setIsOpenShowIssue(false);
    history.push(`/sprints/${projectId}/issues`);
    clearIssueState();
  };

  // Close Issue
  const onClickCloseIssue = item => {
    setIsAlertCloseIssue(true);
    setIssueId(item.id);
  };

  const handleConfirmCloseIssue = () => {
    updateIssue({
      id: issueId,
      status: ISSUE_STATUS.CLOSE
    });
    setIsAlertCloseIssue(false);
    clearIssueState();
  };

  const handleCloseIssue = () => {
    setIsAlertCloseIssue(false);
    clearIssueState();
  };

  // Share link
  const onClickCopyShareLink = item => {
    const linkShare = `${LINK_DOMAIN_PROJECT}/sprints/${projectId}/issues/${item.id}`;
    var textField = document.createElement('textarea');
    textField.innerText = linkShare;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
  };

  // render issue status
  const renderIssueStatus = (item, t) => (
    <>
      {item.status === ISSUE_STATUS.OPEN && (
        <Badge color="danger" className="badge-info">
          {t('open')}
        </Badge>
      )}

      {item.status === ISSUE_STATUS.SOLVING && (
        <Badge color="warning" className="badge-info">
          {t('solving')}
        </Badge>
      )}

      {item.status === ISSUE_STATUS.CLOSE && (
        <Badge color="info" className="badge-info">
          {t('close')}
        </Badge>
      )}
    </>
  );

  // render action
  const renderAction = (item, t) => (
    <>
      {isAllowPermission(appPermissions, UPDATE_ISSUE, projectId) && userId === item.creator && (
        <TooltipWithChildren id={`update-issue-${item.id}`} placement={'top'} content={t('update')}>
          <span className={'cursor-pointer'} onClick={() => onClickUpdateIssue(item)}>
            <i className="fa fa-pencil color-base" />
          </span>
        </TooltipWithChildren>
      )}
      <TooltipWithChildren id={`show-issue-${item.id}`} placement={'top'} content={t('show')}>
        <span className={'margin-left-5px cursor-pointer'} onClick={() => onClickShowIssue(item)}>
          <i className="fa fa-eye color-base" />
        </span>
      </TooltipWithChildren>
      {item.status === ISSUE_STATUS.OPEN && (
        <TooltipWithChildren id={`generate-task-${item.id}`} placement={'top'} content={t('generate_a_new_task')}>
          <span className={'margin-left-5px cursor-pointer'} onClick={() => generateTask(item)}>
            <i className="fa fa-plus color-base" />
          </span>
        </TooltipWithChildren>
      )}
      {isAllowPermission(appPermissions, UPDATE_ISSUE, projectId) &&
        (item.status === ISSUE_STATUS.SOLVING || item.status === ISSUE_STATUS.OPEN) && (
          <TooltipWithChildren id={`close-${item.id}`} placement={'top'} content={t('close')}>
            <span className={'margin-left-5px cursor-pointer'} onClick={() => onClickCloseIssue(item)}>
              <i className="fa fa-times color-base" />
            </span>
          </TooltipWithChildren>
        )}
      <TooltipWithChildren id={`clipboard-${item.id}`} placement={'top'} content={t('copy_link_to_issue')}>
        <span className={'margin-left-5px cursor-pointer'} onClick={() => onClickCopyShareLink(item)}>
          <i className="fa fa-clipboard color-base" />
        </span>
      </TooltipWithChildren>
    </>
  );

  // render rows issue
  const renderIssues = t => (
    <>
      {issues.map(row => {
        return (
          <tr className="table-row font-size-14" key={row.id}>
            <td className="p-6px">{`#${row.number} ${row.subject}`}</td>
            <td className={p6x_center}>{renderIssueStatus(row, t)}</td>
            <td className={p6x_center}>
              <TaskType type={row.typeTask} />
            </td>
            <td className={p6x_center}>
              {row.task && row.task.WorkflowState ? <Badge color="info">{row.task.WorkflowState.name}</Badge> : null}
            </td>
            <td className={p6x_center}>{row.estimate + 'h'}</td>
            <td className={p6x_center}>{row.totalComments}</td>
            <td className={p6x_center}>{row.assigneeAs && row.assigneeAs.name}</td>
            <td className={p6x_center}>{row.user && row.user.name}</td>
            <td className={p6x_center}>
              <div>{formatDateTime(row.createdAt, 'DD/MM/YYYY')}</div>
            </td>
            <td className={p6x_center}>{renderAction(row, t)}</td>
          </tr>
        );
      })}
    </>
  );

  return (
    <NamespacesConsumer>
      {t => (
        <>
          <div className="table-wrap">
            <Table className="table-view" hover responsive striped bordered>
              <thead>
                <tr>
                  <TableHeadCol fields={fields} t={t} classes={arrayClass} />
                </tr>
              </thead>
              <tbody>{renderIssues(t)}</tbody>
            </Table>
          </div>
          <div className="mt-3">
            <Pagination
              totalItems={total}
              pageSize={limit}
              value={limit}
              onSelect={page => handleChangePage(page)}
              onChange={e => handleChangeRowsPerPage(e.value)}
              rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
              isClearable={false}
            />
          </div>
          {isOpenCreateTask && (
            <TaskDialog
              title={t('solving_issue')}
              isOpen={isOpenCreateTask}
              taskItem={{
                content: taskItem.subject,
                description: taskItem.description,
                issueId: taskItem.issueId,
                type: taskItem.typeTask,
                estimate: taskItem.estimate,
                assignee: taskItem.assignee,
                userRelate: taskItem.userRelate
              }}
              onClose={() => setIsOpenCreateTask(false)}
              onOK={submitCreateTask}
            />
          )}
          {isOpenShowIssue && (
            <ViewIssueDialog isOpen={isOpenShowIssue} value={{ issueId }} onClose={() => onCloseViewIssue()} />
          )}
          {isOpenUpdateIssue && (
            <>
              <IssueDialog
                isOpen={isOpenUpdateIssue}
                value={{ usersIntoProject }}
                onClose={closeIssueDialog}
                onOK={onUpdateIssue}
                issueItem={issue}
                onRemove={openAlertRemove}
              />
              <ConfirmationModal
                isOpen={isAlertRemoveIssue}
                txtTitle={t('confirm_remove')}
                txtCancel={t('cancel')}
                txtConfirm={t('confirm')}
                funConfirm={() => handleRemoveIssue()}
                funClose={() => closeAlertRemove()}
              >
                {t('are_you_sure_remove_this_issue')}
              </ConfirmationModal>
            </>
          )}
          {isAlertCloseIssue && (
            <ConfirmationModal
              isOpen={isAlertCloseIssue}
              txtTitle={t('confirm_close_issue')}
              txtCancel={t('cancel')}
              txtConfirm={t('confirm')}
              funConfirm={() => handleConfirmCloseIssue()}
              funClose={() => handleCloseIssue()}
            >
              {t('are_you_sure_close_this_issue')}
            </ConfirmationModal>
          )}
        </>
      )}
    </NamespacesConsumer>
  );
}

TableView.propTypes = {
  appPermissions: array,
  issues: array,
  total: number,
  limit: number,
  page: number,
  projectId: number,
  listIssues: func,
  match: object,
  history: object,
  removeIssue: func,
  usersIntoProject: array,
  updateIssue: func,
  getUsersByProject: func,
  userId: number,
  subject: string,
  status: number,
  creator: number,
  handleFilterBySearch: func,
  handleFilterByStatus: func,
  handleFilterByCreator: func,
  handleFilterByLimit: func,
  handleFilterByPage: func,
  location: object,
  clearFilterCreator: func,
  clearFilterLimit: func,
  clearFilterPage: func,
  clearFilterSearch: func,
  clearFilterStatus: func,
  statusIssue: string,
  statusSprint: string,
  createBacklogTask: func
};

const mapStateToProps = state => ({
  appPermissions: state.auth.permissions,
  issues: (state.issue && state.issue.issues && state.issue.issues.data) || [],
  total: (state.issue && state.issue.issues && state.issue.issues.total) || 0,
  limit: (state.issue && state.issue.issues && state.issue.issues.limit) || ROWS_PER_PAGE_OPTIONS[1],
  page: (state.issue && state.issue.issues && state.issue.issues.page) || 1,
  projectId: state.sprint.selectedProject && state.sprint.selectedProject.id,
  usersIntoProject: state.sprint.usersIntoProject,
  userId: state.auth && state.auth.user && state.auth.user.id,
  subject: state.issue.filter.subject,
  status: state.issue.filter.status,
  creator: state.issue.filter.creator,
  statusIssue: state.issue && state.issue.status,
  statusSprint: state.sprint && state.sprint.status
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listIssues,
      statisticalIssueOfProject,
      createBacklogTask,
      removeIssue,
      updateIssue,
      getUsersByProject,
      handleFilterBySearch,
      handleFilterByStatus,
      handleFilterByCreator,
      handleFilterByLimit,
      handleFilterByPage,
      clearFilterCreator,
      clearFilterLimit,
      clearFilterPage,
      clearFilterSearch,
      clearFilterStatus
    },
    dispatch
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(TableView))
);
