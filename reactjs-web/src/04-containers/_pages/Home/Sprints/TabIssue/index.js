import TabIssue from './TabIssue';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUsersByProject } from '../../../../../01-actions/sprint';
import {
  createIssue,
  getIssue,
  handleFilterBySearch,
  createMultipleIssue,
  closeAllIssues,
  clearIssue
} from '../../../../../01-actions/issue';

const mapStateToProps = store => ({
  // auth
  appPermissions: store.auth.permissions,
  status: store.sprint.status,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  suggestTasks: store.sprint.suggestTasks,
  usersIntoProject: store.sprint.usersIntoProject,
  statusIssue: store.issue.status,
  selectedProject: store.sprint.selectedProject
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createIssue,
      getIssue,
      handleFilterBySearch,
      getUsersByProject,
      createMultipleIssue,
      closeAllIssues,
      clearIssue
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabIssue);
