import Select from 'react-select';
import React, { memo, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsersInProject } from '../../../../../01-actions/project-task-view';
import { handleFilterByCreator, clearFilterCreator } from '../../../../../01-actions/issue';
import { GET_USERS_OF_PROJECT } from '../../../../../07-constants/app-permission';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { func, number, array } from 'prop-types';

function FilterCreator({
  appPermissions,
  projectId,
  creator,
  optionsUser,
  handleFilterByCreator,
  getUsersInProject,
  clearFilterCreator
}) {
  useEffect(() => {
    if (isAllowPermission(appPermissions, GET_USERS_OF_PROJECT, projectId)) {
      getUsersInProject(projectId);
    }
    return () => {
      clearFilterCreator();
    };
  }, [projectId]);

  const value = optionsUser.find(u => u.value === creator) || '';

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('creator')}
          isClearable={true}
          isSearchable={true}
          value={value}
          onChange={handleFilterByCreator}
          options={optionsUser}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterCreator.propTypes = {
  appPermissions: array,
  projectId: number,
  creator: number,
  optionsUser: array,
  handleFilterByCreator: func,
  getUsersInProject: func,
  clearFilterCreator: func
};

const mapStateToProps = store => ({
  appPermissions: store.auth.permissions,
  projectId: store.sprint.selectedProject.id,
  creator: store.issue.filter.creator,
  optionsUser: store.projectTaskView.optionsUser
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByCreator,
      getUsersInProject,
      clearFilterCreator
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterCreator));
