import FieldInput from '../../../../../03-components/FieldInput';
import React, { memo, useEffect } from 'react';
import { string, func, number } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { handleFilterBySearch, clearFilterSearch } from '../../../../../01-actions/issue';
import { debounce } from 'throttle-debounce';

const FilterSearch = ({ subject, handleFilterBySearch, clearFilterSearch, projectId }) => {
  const changeFilter = debounce(500, value => handleFilterBySearch(value));

  useEffect(() => {
    return () => {
      clearFilterSearch();
    };
  }, [projectId]);

  return (
    <FieldInput
      className={'w-100'}
      type={'text'}
      onChangeValue={changeFilter}
      defaultValue={subject || ''}
      placeholder={'search_by_subject'}
      awesomeIcon={'fa fa-search'}
      isDebounce={true}
    />
  );
};

FilterSearch.propTypes = {
  subject: string,
  handleFilterBySearch: func,
  clearFilterSearch: func,
  projectId: number
};

FilterSearch.defaultProps = {};

const mapStateToProps = state => ({
  subject: state.issue.filter.subject,
  projectId: state.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterBySearch,
      clearFilterSearch
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(memo(FilterSearch))
);
