import React, { memo } from 'react';
import { Progress } from 'reactstrap';
import { number } from 'prop-types';

const ProgressMulti = ({ percentOpen, percentSolving, percentClose }) => (
  <Progress multi>
    <Progress bar color="danger" value={percentOpen} />
    <Progress bar color="warning" value={percentSolving} />
    <Progress bar color="info" value={percentClose} />
  </Progress>
);

ProgressMulti.propTypes = {
  percentOpen: number,
  percentSolving: number,
  percentClose: number
};

export default memo(ProgressMulti);
