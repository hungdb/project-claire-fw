import './styles.css';
import XLSX from 'xlsx';
import LazyComponent from '../../../../../03-components/LazyComponent';
import Skeleton from 'react-loading-skeleton';
import i18n from '../../../../../i18n';
import classNames from 'classnames';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { memo, useState } from 'react';
import { array, string, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import {
  CREATE_ISSUE,
  PERM_CREATE_MULTIPLE_ISSUE,
  PERM_CLOSE_ALL_ISSUE,
  GET_LIST_ISSUES,
  PERM_STATICAL_STATUS_OF_ISSUE
} from '../../../../../07-constants/app-permission';
import { Col, Row } from 'reactstrap';
import { mimeTypeFormat } from '../../../../../08-helpers/mime_types';
import { CREATE_MULTIPLE_ISSUE_PROGRESS } from '../../../../../07-constants/issue';
import { toast } from 'react-toastify';
import { promiseDelayImport, isAllowPermission, getDuplicateObjectInArray } from '../../../../../08-helpers/common';
import { TASK_TYPE_OBJECT, TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';
import { getEnv } from '../../../../../env';

const FilterStatus = LazyComponent(promiseDelayImport(import('./FilterStatus')), <Skeleton height={38} />);
const FilterSearch = LazyComponent(promiseDelayImport(import('./FilterSearch')), <Skeleton height={38} />);
const FilterCreator = LazyComponent(promiseDelayImport(import('./FilterCreator')), <Skeleton height={38} />);

const TableView = LazyComponent(promiseDelayImport(import('./TableView')), <SpinLoader />);

const ProgressBar = LazyComponent(() => import('./ProgressBar'));

const Upload = LazyComponent(() => import('../../../../../03-components/Upload'));

const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);

const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <span className={'margin-left-10px'}>
    <Skeleton width={120} height={34} />
  </span>
);

const IssueDialog = LazyComponent(() => import('../../../../dialog/issue/issue-dialog'));

const LINK_FORMAT_FILE_ISSUE = getEnv('LINK_FORMAT_FILE_ISSUE');

function TabIssue({
  appPermissions,
  statusIssue,
  createMultipleIssue,
  projectId,
  createIssue,
  usersIntoProject,
  closeAllIssues,
  getUsersByProject
}) {
  const [openUploadIssue, setOpenUploadIssue] = useState(false);
  const [isCreated, setIsCreated] = useState(false);

  const toggleUploadIssue = () => {
    setOpenUploadIssue(!openUploadIssue);
  };

  const onCreateIssue = value => {
    createIssue({ ...value, projectId });
    setIsCreated(false);
  };

  const clickCloseAllIssue = () => {
    closeAllIssues({ projectId });
  };

  const handleUploadIssue = acceptedFiles => {
    if (acceptedFiles.length === 0 || !acceptedFiles[0]) {
      return toast.error(i18n.t('file_not_accepted'), TOAST_ERROR_STYLE);
    }

    const reader = new FileReader();
    reader.onload = evt => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: 'binary' });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_json(ws);

      if (!Array.isArray(data)) {
        return toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
      }
      if (data.length === 0) {
        return toast.error(i18n.t('missing_required_value'), TOAST_ERROR_STYLE);
      }
      if (data.length > getEnv('LIMIT_ROW_ISSUE_IMPORT_FROM_FILE')) {
        return toast.error(i18n.t('invalid_number_of_rows'), TOAST_ERROR_STYLE);
      }
      const isMissingRequiredValue = data.some(item => !item || !item.subject);
      if (isMissingRequiredValue) {
        return toast.error(i18n.t('missing_required_value'), TOAST_ERROR_STYLE);
      }
      const dataDuplicate = getDuplicateObjectInArray(['subject', 'content'])(data);
      if (Array.isArray(dataDuplicate) && dataDuplicate.length > 0) {
        return toast.error(i18n.t('duplicate_row_data_error'), TOAST_ERROR_STYLE);
      }

      const isInvalidEstimateTime = data.some(item => {
        if (item && item.estimate) {
          if (item.estimate < 0 || isNaN(item.estimate)) return true;
        }
        return false;
      });
      if (isInvalidEstimateTime) {
        return toast.error(i18n.t('invalid_column_estimate_in_file'), TOAST_ERROR_STYLE);
      }

      const isInvalidType = data.some(item => {
        if (item && item.type) {
          if (item.type !== TASK_TYPE_OBJECT.NORMAL && item.type !== TASK_TYPE_OBJECT.BUG) return true;
        }
        return false;
      });
      if (isInvalidType) {
        return toast.error(i18n.t('invalid_column_type_in_file'), TOAST_ERROR_STYLE);
      }

      const payload = {
        data,
        projectId
      };
      createMultipleIssue(payload);
    };
    reader.readAsBinaryString(acceptedFiles[0]);
  };

  const _renderFilter = (
    <Row className="row-custom">
      <Col md="8" sm="4" xs="12" className="px-2 mb-3">
        <FilterSearch />
      </Col>
      <Col md="2" sm="2" xs="12" className="px-2 mb-3">
        <FilterStatus />
      </Col>
      <Col md="2" sm="2" xs="12" className="px-2 mb-3">
        <FilterCreator />
      </Col>
    </Row>
  );

  const isListTask = isAllowPermission(appPermissions, GET_LIST_ISSUES, projectId);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'issue-area'}>
          <div className={'d-flex align-items-center justify-content-between margin-bottom-15px'}>
            <AppText type={'title-header'}>{t('issues')}</AppText>
            <div>
              {isAllowPermission(appPermissions, PERM_CREATE_MULTIPLE_ISSUE, projectId) && (
                <Button
                  type={'orange'}
                  className={classNames('margin-left-10px', openUploadIssue || 'min-width-120px')}
                  onClick={toggleUploadIssue}
                >
                  <i className={'fa fa-plus margin-right-5px'} />
                  {t(openUploadIssue ? 'close_import_file' : 'import_file')}
                </Button>
              )}
              {isAllowPermission(appPermissions, CREATE_ISSUE, projectId) && (
                <Button
                  type={'orange'}
                  className={'margin-left-10px min-width-120px'}
                  onClick={() => {
                    getUsersByProject({ projectId });
                    setIsCreated(true);
                  }}
                >
                  <i className={'fa fa-plus margin-right-5px'} /> {t('add_issue')}
                </Button>
              )}
              {isAllowPermission(appPermissions, PERM_CLOSE_ALL_ISSUE, projectId) && (
                <Button
                  title={t('close_all_issue_with_task_relate_terminated')}
                  type={'orange'}
                  className="margin-left-10px min-width-120px"
                  onClick={clickCloseAllIssue}
                >
                  <i className={'fa fa-times margin-right-5px'} />
                  {t('close_all_issue')}
                </Button>
              )}
            </div>
          </div>
          {openUploadIssue && (
            <Row className="mb-4">
              <Col md={12}>
                <Upload
                  mimes={[mimeTypeFormat.xls, mimeTypeFormat.xlsx, mimeTypeFormat.csv]}
                  onDrop={handleUploadIssue}
                  multiple={false}
                  disabled={statusIssue === CREATE_MULTIPLE_ISSUE_PROGRESS}
                />
                <div
                  className={'font-size-15px margin-bottom-15px cursor-pointer'}
                  style={{ color: '#0067B1', textDecoration: 'underline', float: 'right' }}
                  onClick={() => {
                    const win = window.open(LINK_FORMAT_FILE_ISSUE, '_blank');
                    win.focus();
                  }}
                >
                  <i className={'fa fa-external-link'} /> {t('format_file_issue')}
                </div>
              </Col>
            </Row>
          )}

          <div className="p-4 default-paper">
            <div className="my-4">
              {isAllowPermission(appPermissions, PERM_STATICAL_STATUS_OF_ISSUE, projectId) && <ProgressBar />}
            </div>

            {isListTask && (
              <Row className="row-custom align-items-center">
                <Col md="12" className="px-2">
                  {_renderFilter}
                </Col>
              </Row>
            )}

            {isListTask && <TableView />}
          </div>
          {isCreated && (
            <IssueDialog
              isOpen={isCreated}
              value={{ usersIntoProject }}
              onClose={() => setIsCreated(false)}
              onOK={onCreateIssue}
            />
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabIssue.propTypes = {
  appPermissions: array,
  statusIssue: string,
  createMultipleIssue: func,
  projectId: number,
  createIssue: func,
  usersIntoProject: array,
  closeAllIssues: func,
  getUsersByProject: func
};

TabIssue.defaultProps = {};

export default memo(TabIssue);
