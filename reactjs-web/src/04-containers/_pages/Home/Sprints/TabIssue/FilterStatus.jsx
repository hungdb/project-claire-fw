import Select from 'react-select';
import React, { memo, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleFilterByStatus, clearFilterStatus } from '../../../../../01-actions/issue';
import { number, func } from 'prop-types';

function FilterStatus({ status, handleFilterByStatus, clearFilterStatus, projectId }) {
  const options = [
    { value: 4, label: 'ALL' },
    { value: 1, label: 'OPEN' },
    { value: 3, label: 'SOLVING' },
    { value: 2, label: 'CLOSE' }
  ];

  useEffect(() => {
    return () => {
      clearFilterStatus();
    };
  }, [projectId]);

  return (
    <NamespacesConsumer>
      {t => (
        <Select
          placeholder={t('status')}
          isClearable={false}
          isSearchable={true}
          value={options.find(o => o.value === status)}
          onChange={handleFilterByStatus}
          options={options}
        />
      )}
    </NamespacesConsumer>
  );
}

FilterStatus.propTypes = {
  status: number,
  handleFilterByStatus: func,
  projectId: number,
  clearFilterStatus: func
};

const mapStateToProps = store => ({
  status: store.issue.filter.status,
  projectId: store.sprint.selectedProject.id
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFilterByStatus,
      clearFilterStatus
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(FilterStatus));
