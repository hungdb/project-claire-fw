import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { Fragment, memo, useEffect, useMemo } from 'react';
import { object, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { statisticalIssueOfProject } from '../../../../../01-actions/issue';
import { percent, promiseDelayImport } from '../../../../../08-helpers/common';

const BadgeTotal = LazyComponent(
  promiseDelayImport(import('./BadgeTotal')),
  <div className="border-radius-backlog">
    <Skeleton height={28} />
  </div>
);

const ProgressMulti = LazyComponent(promiseDelayImport(import('./ProgressMulti')), <Skeleton />);
const Text = LazyComponent(promiseDelayImport(import('./../../../../../03-components/Text')), <Skeleton width={60} />);

function ProgressBar({ open, solving, close, all, projectId, statisticalIssueOfProject }) {
  useEffect(() => {
    statisticalIssueOfProject({ projectId });
  }, [projectId]);

  const percentOpen = useMemo(() => percent(open, all), [open, all]);
  const percentSolving = useMemo(() => percent(solving, all), [solving, all]);
  const percentClose = useMemo(() => percent(close, all), [close, all]);

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          <ProgressMulti percentOpen={percentOpen} percentSolving={percentSolving} percentClose={percentClose} />
          <Text className="badge-text mb-2 text-left font-weight-bold">
            {close}/{all}
          </Text>
          <Row>
            <Col xs="12" sm="4" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('open')}</Text>
              <BadgeTotal color="danger" className="badge-backlog px-4 py-2">
                {open}
              </BadgeTotal>
            </Col>
            <Col xs="12" sm="4" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('solving')}</Text>
              <BadgeTotal color="warning" className="badge-backlog px-4 py-2">
                {solving}
              </BadgeTotal>
            </Col>
            <Col xs="12" sm="4" className="mb-3 text-center">
              <Text className="badge-text mb-1 text-uppercase">{t('close')}</Text>
              <BadgeTotal color="info" className="badge-backlog px-4 py-2">
                {close}
              </BadgeTotal>
            </Col>
          </Row>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
}

ProgressBar.propTypes = {
  classes: object,
  statisticalIssueOfProject: func,
  projectId: number,
  open: number,
  solving: number,
  close: number,
  all: number
};

const mapStateToProps = state => ({
  open: (state.issue && state.issue.statisticalIssues && state.issue.statisticalIssues.open) || 0,
  solving: (state.issue && state.issue.statisticalIssues && state.issue.statisticalIssues.solving) || 0,
  close: (state.issue && state.issue.statisticalIssues && state.issue.statisticalIssues.close) || 0,
  all: (state.issue && state.issue.statisticalIssues && state.issue.statisticalIssues.all) || 0,
  projectId: state.sprint.selectedProject.id
});

const mapDispatchToProp = dispatch => {
  return bindActionCreators(
    {
      statisticalIssueOfProject
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProp
)(memo(ProgressBar));
