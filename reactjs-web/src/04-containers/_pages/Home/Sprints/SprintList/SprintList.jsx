import './sprint-list.css';
import LazyComponent from '../../../../../03-components/LazyComponent';
import i18n from '../../../../../i18n';
import moment from 'moment';
import React, { useState, memo } from 'react';
import { workloadAllocations } from '../../../../../05-utils/commonUtils';
import { array, func, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import {
  DurationTime,
  formatDateTime,
  isLimitDisplay,
  toFixed,
  randomUniqueString
} from '../../../../../08-helpers/common';
import { TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';

const UserAvatar = LazyComponent(() => import('../../../../../03-components/UserAvatar'));
const Status = LazyComponent(() => import('../../../../../03-components/Status'));
const Model = LazyComponent(() => import('../../../../../03-components/ModelInfo'));
const DialogUpdateSprint = LazyComponent(() => import('../../../../dialog/sprint/update/update-sprint'));

const MAX_ITEM = 4;

const _renderMembers = (allocations, t) => {
  switch (isLimitDisplay(allocations, MAX_ITEM)) {
    case 'max':
      return (
        <div style={{ display: 'inline-flex' }}>
          {allocations.map((allocation, indexMax1) => {
            if (indexMax1 + 1 < MAX_ITEM) {
              return (
                <UserAvatar
                  className="avatar"
                  id={`${randomUniqueString()}-${indexMax1}`}
                  user={allocation.User}
                  src={`/profile/${allocation.User && allocation.User.id}`}
                />
              );
            }
            return null;
          })}
          <div className={'more-people'}>
            <Model
              id={randomUniqueString()}
              title={t('members')}
              label={t('more')}
              styleBtn={{
                width: '100%',
                fontSize: '25px',
                border: '1px solid',
                borderRadius: '50%',
                padding: '3px 5px 1px 5px'
              }}
            >
              <div className={'d-flex pb-2 justify-content-between'}>
                {allocations.map((allocation, indexMax2) => {
                  return (
                    <UserAvatar
                      key={indexMax2}
                      className="avatar"
                      id={`${randomUniqueString()}-${indexMax2}`}
                      user={allocation.User}
                      src={`/profile/${allocation.User && allocation.User.id}`}
                    />
                  );
                })}
              </div>
            </Model>
          </div>
        </div>
      );
    case 'min':
      return (
        <div style={{ display: 'inline-flex' }}>
          {allocations.map((allocation, indexMin) => {
            return (
              <UserAvatar
                key={indexMin}
                className="avatar"
                id={`${randomUniqueString()}-${indexMin}`}
                user={allocation.User}
                src={`/profile/${allocation.User && allocation.User.id}`}
              />
            );
          })}
        </div>
      );
    default:
      return null;
  }
};

function SprintList({ sprints, onUpdate, onDelete, isEdit }) {
  const [open, setOpen] = useState(false);
  const [sprint, setSprint] = useState(null);

  const onShowDialogUpdateSprint = sprint => {
    setOpen(true);
    setSprint(sprint);
  };

  const onHandleClose = () => {
    setOpen(false);
    setSprint(null);
  };

  const onUpdateSprint = data => {
    const { id, status, name, startDate, endDate, projectId } = data;
    if (!id || !status || !name || !startDate || !endDate) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      onUpdate({ sprintId: id, status, name, startDate, endDate, projectId });
    }
    onHandleClose();
  };

  const onDeleteSprint = (sprintId, projectId) => {
    if (!sprintId) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      onDelete({ sprintId, projectId });
    }
    onHandleClose();
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <tbody>
          {sprints.map((sprint, index) => {
            const workload = sprint ? sprint.tasks.reduce((acc, { estimate }) => acc + estimate, 0) : 0;
            const workforce = sprint ? workloadAllocations(sprint.allocations) : 0;
            return (
              <tr key={index}>
                <td className={'font-size-14'}>{index + 1}</td>
                <td className={'font-size-14'}>{sprint.name}</td>
                <td className={'font-size-14'}>
                  <Status type={sprint.status} />
                </td>
                <td className={'font-size-14'}>{formatDateTime(sprint.startDate, 'DD/MM/YYYY')}</td>
                <td className={'font-size-14'}>{formatDateTime(sprint.endDate, 'DD/MM/YYYY')}</td>
                <td className={'font-size-14'}>
                  {DurationTime(sprint.startDate, moment(sprint.endDate).add(1, 'second'))}
                </td>
                <td className={'font-size-14'}>
                  {sprint.totalTaskCompleted}/{sprint.totalTask}
                </td>
                <td className={'font-size-14'}>{`${toFixed(workload, 2)} (h)`}</td>
                <td className={'font-size-14'}>{`${toFixed(workforce, 2)} (h)`}</td>
                <td>{_renderMembers(sprint.members, t)}</td>
                <td className={'font-size-14'}>
                  {isEdit && (
                    <span onClick={() => onShowDialogUpdateSprint(sprint)} className={'cursor-pointer'}>
                      <i className={'fa fa-pencil color-base'} />
                    </span>
                  )}
                </td>
              </tr>
            );
          })}
          <DialogUpdateSprint
            isOpen={open}
            title={t('update_task')}
            value={sprint}
            onClose={() => onHandleClose()}
            onDelete={(sprintId, projectId) => onDeleteSprint(sprintId, projectId)}
            onOK={value => onUpdateSprint(value)}
          />
        </tbody>
      )}
    </NamespacesConsumer>
  );
}

SprintList.propTypes = {
  sprints: array,
  onShowDialogUpdateSprint: func,
  onUpdate: func,
  onDelete: func,
  isEdit: bool
};

export default memo(SprintList);
