import React, { memo } from 'react';
import { Button, ButtonGroup } from 'reactstrap';
import { array, func, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { SPRINT_THROUGHOUT_TAB } from '../../../../05-utils/commonData';

const MenuRight = ({ visibleTabs, onClick, activeTab }) => (
  <NamespacesConsumer>
    {t => (
      <ButtonGroup className="button-group-tab">
        {Object.values(SPRINT_THROUGHOUT_TAB).map(
          tab =>
            visibleTabs.includes(tab.value) && (
              <Button
                key={tab.value}
                onClick={() => onClick(tab.value)}
                active={activeTab === tab.value}
                className="tabs-btn"
              >
                {t(tab.label)}
              </Button>
            )
        )}
      </ButtonGroup>
    )}
  </NamespacesConsumer>
);

MenuRight.propTypes = {
  visibleTabs: array,
  activeTab: number,
  onClick: func
};

MenuRight.defaultProps = {};

export default memo(MenuRight);
