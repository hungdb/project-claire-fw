import i18n from '../../../../../i18n';
import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import SpinLoader from '../../../../../03-components/SpinLoader';
import React, { memo, useState, useEffect } from 'react';
import { string, array, number, func, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { isAllowPermission } from '../../../../../08-helpers/common';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import {
  GET_SPRINTS_BY_PROJECT,
  CREATE_NEW_SPRINT,
  CREATE_NEW_TASK,
  FILTER_TASKS_BY_PROJECT,
  PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT
} from '../../../../../07-constants/app-permission';
import {
  UPDATE_SPRINT_SUCCESS,
  DELETE_SPRINT_SUCCESS,
  CREATE_SPRINT_SUCCESS,
  CREATE_TASK_SUCCESS,
  UPDATE_TASK_SUCCESS,
  DELETE_TASK_SUCCESS
} from '../../../../../07-constants/sprint';
import { TOAST_ERROR_STYLE } from '../../../../../05-utils/commonData';

const TaskView = LazyComponent(() => import('../TaskView/TaskView'));
const CreateSprintDialog = LazyComponent(() => import('../../../../dialog/sprint/create/create-sprint'));
const TaskDialog = LazyComponent(() => import('../../../../dialog/backlog-task/task-dialog'));
const AppText = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/AppText')),
  <Skeleton width={150} />
);
const Button = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/Button')),
  <div className="pull-right">
    <Skeleton width={123.5} height={34} />
  </div>
);
const SprintsView = LazyComponent(promiseDelayImport(import('../SprintsView')), <SpinLoader width={128} height={38} />);

function TabBackLog({
  status,
  appPermissions,
  projectId,
  createSprint,
  createBacklogTask,
  clearCurrentTask,
  getSprints,
  getSuggestTags,
  getTasksInProject,
  rowsPerPage,
  page,
  sprintId,
  userId,
  priorityId,
  statusId,
  startDate,
  search,
  currentTask
}) {
  const [isCreateSprint, setIsCreateSprint] = useState(false);
  const [isCreateTask, setIsCreateTask] = useState(false);

  // const callReload = () => {
  // if (isAllowPermission(appPermissions, GET_SPRINTS_BY_PROJECT, projectId))
  //   getSprints({ projectId });
  // };

  // useEffect(() => {
  //   callReload();
  // }, [projectId]);

  useEffect(() => {
    switch (status) {
      case CREATE_TASK_SUCCESS:
      case UPDATE_TASK_SUCCESS:
      case DELETE_TASK_SUCCESS:
        getSuggestTags(projectId);
        getTasksInProject(rowsPerPage, page, projectId, sprintId, userId, priorityId, statusId, startDate, search);
        break;
      case UPDATE_SPRINT_SUCCESS:
      case DELETE_SPRINT_SUCCESS:
      case CREATE_SPRINT_SUCCESS:
        getSprints({ projectId });
        break;
      default:
        break;
    }
  }, [status]);

  const openCreateSprint = () => {
    setIsCreateSprint(true);
  };

  const submitCreateSprint = data => {
    const { name, startDate, endDate } = data;
    if (!projectId || !name || !startDate || !endDate) {
      toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
    } else {
      createSprint({ projectId, name, startDate, endDate });
    }
    setIsCreateSprint(false);
  };

  const openCreateTask = () => {
    setIsCreateTask(true);
  };

  const submitCreateTask = data => {
    const {
      content,
      estimate,
      description,
      tags,
      priority,
      dependencies,
      assignee,
      dueDate,
      files,
      type,
      userRelate
    } = data;
    const dataTag = tags.map(tag => tag.text);

    if (!currentTask) {
      createBacklogTask({
        projectId,
        estimate,
        content,
        description,
        tag: dataTag,
        priority,
        assignee,
        dependencies,
        dueDate,
        files,
        type,
        userRelate
      });
    }

    clearCurrentTask();
    setIsCreateTask(false);
  };

  const closeDialog = () => {
    clearCurrentTask();
    setIsCreateSprint(false);
    setIsCreateTask(false);
  };

  const permissionBacklog = [PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT, FILTER_TASKS_BY_PROJECT, CREATE_NEW_TASK];
  const permissionTableBacklog = [PROGRESS_STATISTICAL_ALL_TASKS_BY_PROJECT, FILTER_TASKS_BY_PROJECT];
  const permissionSprint = [GET_SPRINTS_BY_PROJECT, CREATE_NEW_SPRINT];
  return (
    <NamespacesConsumer>
      {t => (
        <div>
          {permissionBacklog.some(permissionName => isAllowPermission(appPermissions, permissionName, projectId)) && (
            <div className="sprint-backlog-area-task">
              <div className={'d-flex align-items-center justify-content-between margin-bottom-15px'}>
                <AppText type={'title-header'}>{t('Tasks')}</AppText>
                {isAllowPermission(appPermissions, CREATE_NEW_TASK, projectId) && (
                  <Button type={'orange'} onClick={openCreateTask}>
                    <i className={'fa fa-plus margin-right-5px'} /> {t('create_task')}
                  </Button>
                )}
                {isCreateTask && (
                  <TaskDialog
                    isOpen={isCreateTask}
                    title={t('create_task')}
                    onClose={closeDialog}
                    onOK={submitCreateTask}
                  />
                )}
              </div>
              {permissionTableBacklog.some(permissionName =>
                isAllowPermission(appPermissions, permissionName, projectId)
              ) && (
                <div className="sprint-taskview">
                  <TaskView />
                </div>
              )}
            </div>
          )}

          {permissionSprint.every(permissionName => isAllowPermission(appPermissions, permissionName, projectId)) && (
            <div className={'margin-top-40px'}>
              <div className={'d-flex align-items-center justify-content-between margin-bottom-15px'}>
                <AppText type={'title-header'}>{t('Sprints')}</AppText>
                {isAllowPermission(appPermissions, CREATE_NEW_SPRINT, projectId) && (
                  <Button type={'orange'} onClick={openCreateSprint}>
                    <i className={'fa fa-plus margin-right-5px'} /> {t('create_sprint')}
                  </Button>
                )}
                <CreateSprintDialog
                  isOpen={isCreateSprint}
                  status={status}
                  title={t('create_sprint')}
                  onClose={closeDialog}
                  onOK={submitCreateSprint}
                />
              </div>
              {isAllowPermission(appPermissions, GET_SPRINTS_BY_PROJECT, projectId) && <SprintsView />}
            </div>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
}

TabBackLog.propTypes = {
  status: string,
  appPermissions: array,
  projectId: number,
  createSprint: func,
  createBacklogTask: func,
  clearCurrentTask: func,
  getSprints: func,
  getSuggestTags: func,
  getTasksInProject: func,
  rowsPerPage: number,
  page: number,
  sprintId: number,
  userId: number,
  priorityId: number,
  statusId: number,
  startDate: object,
  search: string,
  currentTask: object
};

export default memo(TabBackLog);
