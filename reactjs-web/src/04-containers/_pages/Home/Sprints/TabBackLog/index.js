import TabBackLog from './TabBackLog.jsx';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTasksInProject } from '../../../../../01-actions/project-task-view';
import {
  createSprint,
  createBacklogTask,
  getSprints,
  getSuggestTags,
  clearCurrentTask,
  deleteTask
} from '../../../../../01-actions/sprint';

const mapStateToProps = store => ({
  status: store.sprint.status,
  appPermissions: store.auth.permissions,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  rowsPerPage: store.projectTaskView.filter.rowsPerPage,
  page: store.projectTaskView.filter.page,
  sprintId: store.projectTaskView.filter.sprint ? store.projectTaskView.filter.sprint : null,
  userId: store.projectTaskView.filter.assignee ? store.projectTaskView.filter.assignee : null,
  priorityId: store.projectTaskView.filter.priority ? store.projectTaskView.filter.priority : null,
  statusId: store.projectTaskView.filter.status ? store.projectTaskView.filter.status : null,
  startDate: store.projectTaskView.filter.startDate,
  search: store.projectTaskView.filter.search,
  currentTask: store.sprint.currentTask
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createSprint,
      createBacklogTask,
      clearCurrentTask,
      getSprints,
      getSuggestTags,
      getTasksInProject,
      deleteTask
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabBackLog);
