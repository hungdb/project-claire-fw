import Skeleton from 'react-loading-skeleton';
import LazyComponent from '../../../../../03-components/LazyComponent';
import React, { useState, useEffect, memo } from 'react';
import { number, array, string, object, func } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { promiseDelayImport } from '../../../../../08-helpers/common';
import {
  REMOVE_TASK_TO_BACKLOG_SUCCESS,
  ADD_TASK_TO_SPRINT_SUCCESS,
  REMOVE_ALLOCATION_SUCCESS,
  ADD_ALLOCATION_SUCCESS
} from '../../../../../07-constants/sprint';

const SelectPropsBox = LazyComponent(
  promiseDelayImport(import('../../../../../03-components/SelectPropsBox')),
  <Skeleton height={38} />
);

const SprintManagement = LazyComponent(() => import('../View/SprintManagement'));

function TabSprint({
  projectId,
  status,
  usersOfProject,
  sprintsDiffCompleted,
  searchBacklog,
  sprint,
  allocationLabels,
  currentTask,
  match,
  history,
  location,
  searchBacklogTask,
  removeTaskToBacklog,
  addTaskToSprint,
  getDetailSprint,
  removeAllocation,
  addAllocation,
  getUsersOfProject,
  setKeySearchTask,
  getListAllocationLabels,
  keySearchTask,
  _clearCurrentTask,
  _getDetailTask,
  _deleteTask,
  _updateBacklogTask,
  _updateAllocation,
  getSprintDiffCompleted,
  tags,
  appPermissions,
  roles,
  allocations,
  oldAllocations,
  tasks,
  clearSprintsDiffCompleted
}) {
  const [currentSprintId, setCurrentSprintId] = useState(null);

  const callReload = () => {
    // if (projectId && calculateLength(sprintsDiffCompleted)) {
    //   const projectIdOfSprint = sprintsDiffCompleted[0].projectId;
    //   if (projectIdOfSprint === projectId)
    //     return;
    //   getSprintDiffCompleted({ projectId });
    // }
    if (projectId) {
      getSprintDiffCompleted({ projectId });
      // searchBacklogTask({ projectId, key: keySearchTask });
    }
  };

  useEffect(() => {
    getListAllocationLabels();
    return () => {
      setCurrentSprintId(null);
      clearSprintsDiffCompleted();
    };
  }, []);

  useEffect(() => {
    callReload();
  }, [projectId]);

  useEffect(() => {
    const sprintId = Number(match.params.sprintId);

    if (sprintsDiffCompleted.length && !sprintId) {
      const lastSprint = sprintsDiffCompleted.slice(-1)[0].id;
      history.replace(`/sprints/${projectId}/sprint/${lastSprint}`);
      setCurrentSprintId(lastSprint);
    }
  }, [sprintsDiffCompleted]);

  useEffect(() => {
    switch (status) {
      case REMOVE_TASK_TO_BACKLOG_SUCCESS:
      case ADD_TASK_TO_SPRINT_SUCCESS:
        if (currentSprintId) {
          getDetailSprint({ sprintId: currentSprintId, projectId });
          searchBacklogTask({ projectId, key: keySearchTask });
        }
        break;
      case REMOVE_ALLOCATION_SUCCESS:
      case ADD_ALLOCATION_SUCCESS:
        if (currentSprintId) {
          getDetailSprint({ sprintId: currentSprintId, projectId });
        }
        break;
      default:
        break;
    }
  }, [status]);

  // useEffect(() => {
  //   // if (!currentSprintId && match.params.sprintId) {
  //   // const sprintId = Number(match.params.sprintId);
  //   // setCurrentSprintId(sprintId);
  //   // getDetailSprint({ sprintId, projectId });
  //   searchBacklogTask({ projectId, key: keySearchTask });
  //   // }
  // }, [projectId]);

  useEffect(() => {
    if (location.pathname) {
      const sprintId = Number(match.params.sprintId);
      if (sprintId) {
        setCurrentSprintId(sprintId);
        getDetailSprint({
          sprintId: Number(match.params.sprintId),
          projectId
        });
      }
    }
  }, [location]);

  const changeSprintRoute = sprintId => {
    history.push(`/sprints/${match.params.projectId}/sprint/${sprintId}`);
  };

  const sprintOptions =
    sprintsDiffCompleted &&
    sprintsDiffCompleted.map(p => ({
      label: p.name,
      value: p.id
    }));

  return (
    <NamespacesConsumer>
      {t =>
        projectId ? (
          <div className="create-sprint-area">
            <div className="selector-sprint">
              <SelectPropsBox
                options={sprintOptions}
                value={currentSprintId}
                onChange={select => changeSprintRoute(select && select.value)}
                isClearable={false}
                placeholder={'select_sprint'}
              />
            </div>
            {currentSprintId ? (
              <SprintManagement
                sprint={sprint}
                searchBacklogTask={searchBacklogTask}
                currentProjectId={projectId}
                currentSprintId={currentSprintId}
                searchBacklog={searchBacklog}
                removeTaskToBacklog={removeTaskToBacklog}
                addTaskToSprint={addTaskToSprint}
                getDetailSprint={getDetailSprint}
                status={status}
                removeAllocation={removeAllocation}
                addAllocation={addAllocation}
                getUsersOfProject={getUsersOfProject}
                usersOfProject={usersOfProject}
                setKeySearchTask={setKeySearchTask}
                keySearchTask={keySearchTask}
                appPermissions={appPermissions}
                roles={roles}
                allocationLabels={allocationLabels}
                currentTask={currentTask}
                _clearCurrentTask={_clearCurrentTask}
                _getDetailTask={_getDetailTask}
                _deleteTask={_deleteTask}
                _updateBacklogTask={_updateBacklogTask}
                _updateAllocation={_updateAllocation}
                tags={tags}
                tasks={tasks}
                allocations={allocations}
                oldAllocations={oldAllocations}
              />
            ) : (
              <div className={'text-center text-muted margin-top-15px'}>{t('no_sprint_selected')}</div>
            )}
          </div>
        ) : (
          <div className="create-sprint-area text-center">
            <h5 className="mb-0">{t('select_project_require')}</h5>
          </div>
        )
      }
    </NamespacesConsumer>
  );
}

TabSprint.propTypes = {
  projectId: number,
  status: string,
  usersOfProject: array,
  sprintsDiffCompleted: array,
  searchBacklog: array,
  sprint: object,
  allocationLabels: array,
  currentTask: object,
  match: object,
  history: object,
  location: object,
  searchBacklogTask: func,
  removeTaskToBacklog: func,
  addTaskToSprint: func,
  getDetailSprint: func,
  removeAllocation: func,
  addAllocation: func,
  getUsersOfProject: func,
  setKeySearchTask: func,
  getListAllocationLabels: func,
  keySearchTask: string,
  _clearCurrentTask: func,
  _getDetailTask: func,
  _deleteTask: func,
  _updateBacklogTask: func,
  _updateAllocation: func,
  getSprintDiffCompleted: func,
  tags: array,
  appPermissions: array,
  roles: array,
  allocations: array,
  oldAllocations: array,
  tasks: array,
  clearSprintsDiffCompleted: func
};

export default memo(TabSprint);
