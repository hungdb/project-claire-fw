import TabSprint from './TabSprint';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  clearCurrentTask,
  getDetailTask,
  deleteTask,
  updateBacklogTask,
  clearSprintsDiffCompleted
} from '../../../../../01-actions/sprint';
import { getUsersOfProject } from '../../../../../01-actions/project';
import {
  searchBacklogTask,
  removeTaskToBacklog,
  addTaskToSprint,
  getDetailSprint,
  removeAllocation,
  addAllocation,
  setKeySearchTask,
  getSprintDiffCompleted,
  getListAllocationLabels,
  updateAllocation
} from '../../../../../01-actions/sprint';

const mapStateToProps = store => ({
  // auth
  appPermissions: store.auth.permissions,
  roles: store.auth.roles,

  status: store.sprint.status,
  sprint: store.sprint.sprint,
  projectId: store.sprint.selectedProject && store.sprint.selectedProject.id,
  searchBacklog: store.sprint.searchBacklog,
  usersOfProject: store.project.usersOfProject,
  sprintsDiffCompleted: store.sprint.sprintsDiffCompleted,
  keySearchTask: store.sprint.keySearchTask,
  allocationLabels: store.sprint.allocationLabels,
  currentTask: store.sprint.currentTask,
  tags: store.sprint.tags,
  allocations: store.sprint.sprint && store.sprint.sprint.allocations,
  oldAllocations: store.sprint.sprint && store.sprint.sprint.beforeAllocations
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getSprintDiffCompleted,
      searchBacklogTask,
      removeTaskToBacklog,
      addTaskToSprint,
      getDetailSprint,
      removeAllocation,
      addAllocation,
      getUsersOfProject,
      setKeySearchTask,
      getListAllocationLabels,
      _clearCurrentTask: clearCurrentTask,
      _getDetailTask: getDetailTask,
      _deleteTask: deleteTask,
      _updateBacklogTask: updateBacklogTask,
      _updateAllocation: updateAllocation,
      clearSprintsDiffCompleted
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabSprint);
