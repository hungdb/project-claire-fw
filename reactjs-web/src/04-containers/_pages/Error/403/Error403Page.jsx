import React from 'react';
import { Button } from 'reactstrap';
import { object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

const Error403Page = ({ history }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <div className="pages-error text-center align-middle">
        <h1 className={'mt-5'}>{t('forbidden_error')}</h1>
        <a href="/">
          <Button className={'outline-base-btn'} size={'sm'} onClick={() => history.push('/')}>
            {t('dashboard')}
          </Button>
        </a>
      </div>
    )}
  </NamespacesConsumer>
);

Error403Page.propTypes = {
  history: object
};

Error403Page.defaultProps = {};

export default Error403Page;
