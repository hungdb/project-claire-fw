import React from 'react';
import { NamespacesConsumer } from 'react-i18next';

const Error404Page = () => (
  <NamespacesConsumer ns="translations">
    {t => (
      <div className="pages-error text-center align-middle">
        <h1 className={'mt-5'}>{t('not_found_error')}</h1>
      </div>
    )}
  </NamespacesConsumer>
);

export default Error404Page;
