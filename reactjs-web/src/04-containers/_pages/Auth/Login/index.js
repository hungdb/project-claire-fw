import Login from './Login';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isTokenExpired } from '../../../../08-helpers/jwt';
import {
  getOAuthLink,
  getMyRolePermission,
  getUserInfo,
  login,
  showRejectedReason,
  storeTokenFirebase
} from '../../../../01-actions/auth';

const mapStateToProps = store => {
  return {
    isAuthenticated: !isTokenExpired(store.auth.loginToken),
    authStatus: store.auth.status,
    authError: store.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      login,
      getOAuthLink,
      getMyRolePermission,
      getUserInfo,
      showRejectedReason,
      storeTokenFirebase
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
