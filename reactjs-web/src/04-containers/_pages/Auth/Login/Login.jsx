import './style.css';
import logo from '../../../../06-assets/img/logo.png';
import queryString from 'query-string';
import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { PRE_LOGIN_SUCCESS, PRE_LOGIN_FAILED } from '../../../../07-constants/auth';
import { NamespacesConsumer } from 'react-i18next';
import { Button } from 'reactstrap';
import { object, func, any, bool } from 'prop-types';
import { askForPermissionToReceiveNotifications } from '../../../../10-services/firebase';
import { ToastContainer } from 'react-toastify';
import { clearAllCache } from '../../../../05-utils/commonUtils';

const LoginHOC = prop => WrappedComponent => {
  return props => {
    const { authStatus, getUserInfo, registerReceiveNotify, toggleDisabled } = props;
    const checkLogin = () => {
      if (authStatus === PRE_LOGIN_SUCCESS) {
        getUserInfo();
        registerReceiveNotify();
      }
      if (authStatus === PRE_LOGIN_FAILED) {
        toggleDisabled();
      }
    };

    useEffect(() => {
      checkLogin();
    }, [authStatus]);
    return props[prop] ? <Redirect to="/sprints" /> : <WrappedComponent {...props} />;
  };
};

const LoginPage = ({ getOAuthLink, disabled }) => {
  return (
    <NamespacesConsumer ns="translations">
      {(t, { i18n }) => (
        <div className="pages-auth-login text-center">
          <img id="rotate-logo" className="logo-bap" src={logo} alt="logo-bap" />
          <h1 className="title-system">{t('BAP PROJECT SYSTEM')}</h1>

          <Button
            size="lg"
            disabled={disabled}
            className={disabled ? 'progress-button' : 'btn-login hvr-pop'}
            onClick={getOAuthLink}
          >
            {t('Log in with Identity℠')}
          </Button>

          <div className="language margin-top-40px">
            <Button
              outline
              color="info"
              size="sm"
              className="hvr-pulse-shrink"
              onClick={() => i18n.changeLanguage('en')}
            >
              {t('en')}
            </Button>
            -
            <Button
              outline
              color="info"
              size="sm"
              className="hvr-pulse-shrink"
              onClick={() => i18n.changeLanguage('vi')}
            >
              {t('vn')}
            </Button>
            -
            <Button
              outline
              color="info"
              size="sm"
              className="hvr-pulse-shrink"
              onClick={() => i18n.changeLanguage('ja')}
            >
              {t('ja')}
            </Button>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

const WrapLogin = LoginHOC('isAuthenticated')(LoginPage);

const Login = ({
  isAuthenticated,
  storeTokenFirebase,
  getOAuthLink,
  location,
  login,
  authStatus,
  getUserInfo,
  // history,
  showRejectedReason
}) => {
  const [disabled, setDisabled] = useState(false);

  const _registerReceiveNotify = async () => {
    const tokenFirebase = await askForPermissionToReceiveNotifications();
    if (tokenFirebase) {
      storeTokenFirebase({ tokenFirebase });
    }
  };

  const _getOAuthLink = () => getOAuthLink();

  const handleLogin = () => {
    const values = queryString.parse(location.search);
    const provider = values.provider;
    const code = values.code;
    const reason = values.reason;
    if (provider && code) {
      setDisabled(true);
      login({ provider, code });
    } else if (reason) {
      showRejectedReason(reason);
    }
  };

  useEffect(() => {
    clearAllCache();
    handleLogin();
  }, []);

  return (
    <>
      <ToastContainer />
      <WrapLogin
        isAuthenticated={isAuthenticated}
        authStatus={authStatus}
        getOAuthLink={_getOAuthLink}
        disabled={disabled}
        getUserInfo={getUserInfo}
        registerReceiveNotify={_registerReceiveNotify}
        toggleDisabled={() => setDisabled(false)}
      />
    </>
  );
};

Login.propTypes = {
  location: object,
  history: object,
  login: func,
  getOAuthLink: func,
  reloadAccessToken: func,
  getUserInfo: func,
  showRejectedReason: func,
  storeTokenFirebase: func,
  authError: any,
  authStatus: any,
  isAuthenticated: bool
};

Login.defaultProps = {};

export default Login;
