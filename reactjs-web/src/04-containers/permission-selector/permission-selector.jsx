import React, { PureComponent } from 'react';
import Select from 'react-select';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';

class PermissionSelector extends PureComponent {
  static propTypes = {
    permissions: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <div>
            <Select
              placeholder={t('select_permission')}
              options={this.props.permissions.map(p => ({
                label: p.permissionName + (p.projectRelated ? ' *' : ''),
                value: p.permissionName
              }))}
              onChange={value => {
                this.props.onChange && this.props.onChange(value);
              }}
            />
          </div>
        )}
      </NamespacesConsumer>
    );
  }
}

export default PermissionSelector;
