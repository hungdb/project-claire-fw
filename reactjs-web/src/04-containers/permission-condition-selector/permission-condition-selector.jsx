import React, { PureComponent } from 'react';
import Select from 'react-select';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';

class PermissionConditionSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      listPermissionCondition: [
        { label: 'Subset', value: 'subset' },
        { label: 'EditableFields', value: 'editableFields' },
        { label: 'RegexStringValue', value: 'regexStringValue' }
      ]
    };
  }
  static propTypes = {
    onChange: PropTypes.func.isRequired
  };

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_a_condition')}
            options={this.state.listPermissionCondition.map(p => ({ label: p.label, value: p.value }))}
            onChange={value => {
              this.props.onChange && this.props.onChange(value);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

export default PermissionConditionSelector;
