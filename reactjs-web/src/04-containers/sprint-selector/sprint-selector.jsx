import React, { PureComponent } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';

class SprintSelector extends PureComponent {
  state = {
    sprint: null
  };

  static propTypes = {
    sprints: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  // static getDerivedStateFromProps(props, state) {
  //   if ((state.preSprint && state.preSprint.value) !== (state.sprint && state.sprint.value)) {
  //     return { preSprint: state.sprint, sprint: state.sprint };
  //   }
  //   return { sprint: null };
  // }

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_sprint')}
            value={this.state.sprint}
            options={this.props.sprints.map(p => ({ label: p.name, value: p.id }))}
            onChange={value => {
              this.setState({ sprint: value });
              this.props.onChange && this.props.onChange(value);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    sprints: store.sprint.sprintsDiffCompleted
  };
};

export default connect(mapStateToProps)(SprintSelector);
