import React, { PureComponent } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';
import PropTypes from 'prop-types';

class CheckListItemSelector extends PureComponent {
  constructor() {
    super();
    this.state = {
      value: null,
      selectOptions: []
    };
  }

  static propTypes = {
    listItemsCheck: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_item_check_list')}
            options={this.props.listItemsCheck.map(p => ({ label: p.name, value: p.id }))}
            onChange={option => {
              this.props.onChange && this.props.onChange(option);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    listItemsCheck: store.checkList.listItemsCheck
  };
};

export default connect(mapStateToProps)(CheckListItemSelector);
