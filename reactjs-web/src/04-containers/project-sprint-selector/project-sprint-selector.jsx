import React, { PureComponent } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';

class ProjectSprintSelector extends PureComponent {
  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_project')}
            options={this.props.projects.map(p => ({ label: p.Project.name, value: p.Project.id }))}
            onChange={value => {
              this.props.onChange && this.props.onChange(value);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    projects: store.project.projects
  };
};

export default connect(mapStateToProps)(ProjectSprintSelector);
