import PropTypes from 'prop-types';
import React, { useEffect, useState, memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { connect } from 'react-redux';
import Select from 'react-select';
import { customBorderColor } from '../../08-helpers/common';

function PermissionSchemeSelector({ permissionSchemes, onChange, value }) {
  const [currentValue, setCurrentValue] = useState(null);
  const [selectOptions, setSelectOptions] = useState([]);

  useEffect(() => {
    if (value || permissionSchemes) {
      setCurrentValue(value);
      setSelectOptions(permissionSchemes.map(s => ({ value: s.id, label: s.name })));
    }
  });

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Select
          placeholder={t('select_project_permission_scheme')}
          value={currentValue ? selectOptions.find(o => o.value === currentValue.id) : null}
          styles={customBorderColor}
          options={selectOptions}
          onChange={option => {
            onChange && onChange(option);
          }}
        />
      )}
    </NamespacesConsumer>
  );
}

PermissionSchemeSelector.propTypes = {
  permissionSchemes: PropTypes.array,
  onChange: PropTypes.func,
  value: PropTypes.any
};

const mapStateToProps = store => {
  return {
    permissionSchemes: store.permissionScheme.permissionSchemes
  };
};

export default connect(mapStateToProps)(memo(PermissionSchemeSelector));
