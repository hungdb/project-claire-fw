import React, { PureComponent } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';

class RoleUserSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      roles: null
    };
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if (nextProps.values !== prevState.values) {
  //     const roles = [];
  //     nextProps.listRoles.map(p => {
  //       nextProps.values.map(r => {
  //         if (p.id === r.roleId)
  //           roles.push({ label: p.name, value: p.id })
  //       });
  //     });
  //
  //     return { values: nextProps.values, roles: roles[ 0 ] }
  //   }
  // }

  checkDefaultValues() {
    if (!this.state.roles) {
      if (this.props.listRoles) {
        let selected = [];
        if (this.props.values) {
          this.props.values.forEach(r => {
            let matched = this.props.listRoles.find(r2 => r2.id === r.roleId);
            if (matched) {
              selected.push({ label: matched.name, value: matched.id });
            }
          });
        }
        this.setState({ roles: selected });
      }
    }
  }

  componentDidMount() {
    this.checkDefaultValues();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.checkDefaultValues();
  }

  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_role_user')}
            value={this.state.roles}
            options={this.props.listRoles.filter(r => !r.isSystemRole).map(p => ({ label: p.name, value: p.id }))}
            onChange={value => {
              this.setState({ roles: value });
              this.props.onChange && this.props.onChange(value);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    listRoles: store.permission.listRoles
  };
};

export default connect(mapStateToProps)(RoleUserSelector);
