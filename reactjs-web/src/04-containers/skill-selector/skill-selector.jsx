import React, { PureComponent } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { NamespacesConsumer } from 'react-i18next';

class SkillSelector extends PureComponent {
  render() {
    return (
      <NamespacesConsumer ns="translations">
        {t => (
          <Select
            placeholder={t('select_skill')}
            options={this.props.allSkills.map(p => ({ label: p.name, value: p.id }))}
            onChange={value => {
              this.props.onChange && this.props.onChange(value);
            }}
          />
        )}
      </NamespacesConsumer>
    );
  }
}

const mapStateToProps = store => {
  return {
    allSkills: store.skill.allSkills
  };
};

export default connect(mapStateToProps)(SkillSelector);
