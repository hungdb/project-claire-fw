import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
// import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './05-utils/commonStyles.css';
import './i18n';
import 'react-toastify/dist/ReactToastify.css';
import 'hover.css/css/hover-min.css';
import 'react-datepicker/dist/react-datepicker.css';
import { history, store } from './store';
import routes from './09-routes/routes';
import RenderRoutes from './09-routes/RenderRoutes';
import { initializeFirebase } from './10-services/firebase';
// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    {/* <BrowserRouter> */}
    <ConnectedRouter history={history}>
      <RenderRoutes routes={routes} />
    </ConnectedRouter>
    {/* </BrowserRouter> */}
  </Provider>,
  document.getElementById('root')
);
// registerServiceWorker();
initializeFirebase();
