import _ from 'lodash';

const mimeTypeFormat = {
  xls: 'application/vnd.ms-excel',
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  csv: 'text/csv'
};

const mimeTypes = [
  {
    mimeType: {
      name: 'AAC audio',
      extension: 'aac',
      mime: 'audio/aac',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'AbiWord document',
      extension: 'abw',
      mime: 'application/x-abiword',
      icon: 'fa fa-file-word-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Archive document',
      extension: 'arc',
      mime: 'application/x-freearc',
      icon: 'fa fa-file-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'AVI: Audio Video Interleave',
      extension: 'avi',
      mime: 'video/x-msvideo',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Amazon Kindle eBook format',
      extension: 'azw',
      mime: 'application/vnd.amazon.ebook',
      icon: 'fa fa-book'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Any kind of binary data',
      extension: 'bin',
      mime: 'application/octet-stream',
      icon: 'fa fa-file-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Windows OS/2 Bitmap Graphics',
      extension: 'bmp',
      mime: 'image/bmp',
      icon: 'fa fa-file-image-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'BZip archive',
      extension: 'bz',
      mime: 'application/x-bzip',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Cascading Style Sheets (CSS)',
      extension: 'text/css',
      mime: 'css',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'BZip2 archive',
      extension: 'bz2',
      mime: 'application/x-bzip2',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'C-Shell script',
      extension: 'csh',
      mime: 'application/x-csh',
      icon: 'fa fa-file-code-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Comma-separated values (CSV)',
      extension: 'csv',
      mime: 'text/csv',
      icon: 'fa fa-file-text-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Word',
      extension: 'doc',
      mime: 'application/msword',
      icon: 'fa fa-file-word-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'MS Embedded OpenType fonts',
      extension: 'eot',
      mime: 'application/vnd.ms-fontobject',
      icon: 'fa fa-font'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Word (OpenXML)',
      extension: 'docx',
      mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      icon: 'fa fa-file-word-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Graphics Interchange Format (GIF)',
      extension: 'gif',
      mime: 'image/gif',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Electronic publication (EPUB)',
      extension: 'epub',
      mime: 'application/epub+zip',
      icon: 'fa fa-file-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'HyperText Markup Language (HTML)',
      extension: 'html',
      mime: 'text/html',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Icon format',
      extension: 'ico',
      mime: 'image/vnd.microsoft.icon',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'iCalendar format',
      extension: 'ics',
      mime: 'text/calendar',
      icon: 'fa fa-calendar-check-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Java Archive (JAR)',
      extension: 'jar',
      mime: 'application/java-archive',
      icon: 'fa fa-file-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JPEG images',
      extension: 'jpeg',
      mime: 'image/jpeg',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JPG images',
      extension: 'jpg',
      mime: 'image/jpg',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'JavaScript',
      extension: 'js',
      mime: 'text/javascript',
      icon: 'fa fa-file-code-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'JSON format',
      extension: 'json',
      mime: 'application/json',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Musical Instrument Digital Interface (MIDI)',
      extension: 'midi',
      mime: 'audio/midi audio/x-midi',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'JavaScript module',
      extension: 'mjs',
      mime: 'application/javascript',
      icon: ''
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'MP3 audio',
      extension: 'mp3',
      mime: 'audio/mpeg',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'MPEG Video',
      extension: 'mpeg',
      mime: 'video/mpeg',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Apple Installer Package',
      extension: 'mpkg',
      mime: 'application/vnd.apple.installer+xml',
      icon: 'fa fa-file-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument presentation document',
      extension: 'odp',
      mime: 'application/vnd.oasis.opendocument.presentation',
      icon: 'fa fa-file-powerpoint-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument spreadsheet document',
      extension: 'ods',
      mime: 'application/vnd.oasis.opendocument.spreadsheet',
      icon: 'fa fa-file-excel-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OpenDocument text document',
      extension: 'odt',
      mime: 'application/vnd.oasis.opendocument.text',
      icon: 'fa fa-file-word-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'OGG audio',
      extension: 'oga',
      mime: 'audio/ogg',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OGG video',
      extension: 'ogv',
      mime: 'video/ogg',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OGG',
      extension: 'ogx',
      mime: 'application/ogg',
      icon: 'fa fa-file-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'OpenType font',
      extension: 'otf',
      mime: 'font/otf',
      icon: 'fa fa-font'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Portable Network Graphics',
      extension: 'png',
      mime: 'image/png',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Adobe Portable Document Format (PDF)',
      extension: 'pdf',
      mime: 'application/pdf',
      icon: 'fa fa-file-pdf-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft PowerPoint',
      extension: 'ppt',
      mime: 'application/vnd.ms-powerpoint',
      icon: 'fa fa-file-powerpoint-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft PowerPoint (OpenXML)',
      extension: 'pptx',
      mime: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      icon: 'fa fa-file-powerpoint-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'RAR archive',
      extension: 'rar',
      mime: 'application/x-rar-compressed',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Rich Text Format (RTF)',
      extension: 'rtf',
      mime: 'application/rtf',
      icon: 'fa fa-file-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Bourne shell script',
      extension: 'sh',
      mime: 'application/x-sh',
      icon: 'fa fa-file-code-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'Scalable Vector Graphics (SVG)',
      extension: 'svg',
      mime: 'image/svg+xml',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Small web format (SWF) or Adobe Flash document',
      extension: 'swf',
      mime: 'application/x-shockwave-flash',
      icon: 'fa fa-file-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Tape Archive (TAR)',
      extension: 'tar',
      mime: 'application/x-tar',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Tagged Image File Format (TIFF)',
      extension: 'tiff',
      mime: 'image/tiff',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'TrueType Font',
      extension: 'ttf',
      mime: 'font/ttf',
      icon: 'fa fa-font'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Text, (generally ASCII or ISO 8859-n)',
      extension: 'txt',
      mime: 'text/plain',
      icon: 'fa fa-file-text-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Visio',
      extension: 'vsd',
      mime: 'application/vnd.visio',
      icon: 'fa fa-file-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Waveform Audio Format',
      extension: 'wav',
      mime: 'audio/wav',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBM audio',
      extension: 'weba',
      mime: 'audio/webm',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBM video',
      extension: 'webm',
      mime: 'video/webm',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'WEBP image',
      extension: 'webp',
      mime: 'image/webp',
      icon: 'fa fa-file-image-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Web Open Font Format (WOFF)',
      extension: 'woff',
      mime: 'font/woff',
      icon: 'fa fa-font'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Web Open Font Format (WOFF)',
      extension: 'woff2',
      mime: 'font/woff2',
      icon: 'fa fa-font'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XHTML',
      extension: 'xhtml',
      mime: 'application/xhtml+xml',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Excel',
      extension: 'xls',
      mime: 'application/vnd.ms-excel',
      icon: 'fa fa-file-excel-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'Microsoft Excel (OpenXML)',
      extension: 'xlsx',
      mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      icon: 'fa fa-file-excel-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XML application/xml if not readable from casual users (RFC 3023, section 3)',
      extension: 'xml',
      mime: 'text/xml',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XML application/xml if readable from casual users (RFC 3023, section 3)',
      extension: 'xml',
      mime: 'text/xml',
      icon: 'fa fa-file-code-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'XUL',
      extension: 'xul',
      mime: 'application/vnd.mozilla.xul+xml',
      icon: 'fa fa-file-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: 'ZIP archive',
      extension: 'zip',
      mime: 'application/zip',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: '3GPP audio/video container',
      extension: '3gp',
      mime: 'video/3gpp',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: "3GPP audio/video container if it doesn't contain video",
      extension: '3gp',
      mime: 'audio/3gpp',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: '3GPP2 audio/video container',
      extension: '3g2',
      mime: 'video/3gpp2',
      icon: 'fa fa-file-video-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: "3GPP2 audio/video container if it doesn't contain video",
      extension: '3g2',
      mime: 'audio/3gpp2',
      icon: 'fa fa-file-audio-o'
    },
    allow: 0
  },
  {
    mimeType: {
      name: '7-zip archive',
      extension: '7z',
      mime: 'application/x-7z-compressed',
      icon: 'fa fa-file-archive-o'
    },
    allow: 1
  },
  {
    mimeType: {
      name: 'SKETCH',
      extension: 'sketch',
      mime: 'application/octet-stream',
      icon: 'fa fa-file-o'
    },
    allow: 1
  }
];

const allows = mimeTypes.filter(m => m.allow);
const allowMimeTypes = _.uniq(_.compact(allows.map(a => a.mimeType.mime)));
const allowMimeTypesByType = type => {
  return allowMimeTypes.filter(item => {
    const arr = item.split('/');
    return Array.isArray(arr) && arr.length > 0 && arr[0] === type;
  });
};

export { mimeTypes, allowMimeTypes, mimeTypeFormat, allowMimeTypesByType };
