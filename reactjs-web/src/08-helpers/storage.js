/*
 *=================================================================================================
 * Local Storage Helper
 *=================================================================================================
 */
export const getKey = key => {
  return (localStorage && localStorage.getItem(key)) || null;
};

export const setKey = (key, value) => {
  if (!value || value.length <= 0) {
    return;
  }
  if (localStorage) {
    return localStorage.setItem(key, value);
  }
};

export const removeKey = key => {
  return (localStorage && localStorage.removeItem(key)) || null;
};
