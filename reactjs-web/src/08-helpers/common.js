/*
 *=================================================================================================
 * Common Helper
 *=================================================================================================
 */
import { getKey } from './storage';
import moment from 'moment';
import * as R from 'ramda';
import queryString from 'query-string';
import { getEnv } from '../env';
import { USER_ACCESS_TOKEN } from '../07-constants/common';
import { callApi, defaultPostConfig } from '../05-utils/commonUtils';
import { mimeTypes } from './mime_types';
import {
  BORDER_COLOR_DEFAULT,
  RANK_SKILL_NORMAL,
  RANK_SKILL_PM,
  PM_NAME_SKILL,
  SKILL_RANK_TYPE,
  RANK_SKILL_ADMIN_CB,
  RANK_SKILL_HR_RECRUITER,
  RANK_SKILL_ACCOUNTING,
  RANK_SKILL_COPYWRITER,
  RANK_SKILL_DIGITAL_MARKETING,
  RANK_SKILL_BRAND_MARKETING,
  RANK_SKILL_SALES,
  RANK_SKILL_BRSE,
  PROJECT_MANAGER_NAME
} from '../05-utils/commonData';
import { setKey } from '../08-helpers/storage';
import { LIGHT_BACKGROUND_TOGGLE, FULL_BOARD_TOGGLE } from '../07-constants/common';
import { STATUS_LIGHT, STATUS_FULL_BOARD } from '../05-utils/commonData';

export const updateStatus = (type, data) => {
  return {
    type,
    data
  };
};

export const reloadAccessToken = loginToken => {
  // console.log("reloadAccessToken", loginToken, loginToken.token);
  return new Promise(resolve => {
    callApi(
      getEnv('REACT_APP_API_SERVER') + '/api/rpc',
      defaultPostConfig(
        {
          action: 'get_access_token',
          loginToken: loginToken.token
        },
        false
      ),
      null,
      data => {
        resolve({ success: true, result: data });
      },
      err => {
        resolve({ success: false, error: err });
      }
    );
  });
};

export const getTokenBearer = () => {
  const token = JSON.parse(getKey(USER_ACCESS_TOKEN));
  return `Bearer ${token && token.token}`;
  //-- check if token expired then reload token
  // if (Date.now() >= token.tokenExpiration) {
  //   const result = await reloadAccessToken();
  //   if (result.success) {
  //     return `Bearer ${ result.result.token }`;
  //   } else {
  //     return 'Invalid token';
  //   }
  // }
  // else{
  //
  // }
};

// export const clearStorage = () => {
//   removeKey(USER_ACCESS_TOKEN);
//   removeKey(USER_INFO);
// };

export const truncateText = (text, maxLength = 50) => {
  if (text === undefined || text === null) return null;
  if (text.length > maxLength) {
    // eslint-disable-next-line
    text = text.substr(0, maxLength) + '...';
  }
  return text;
};

export const checkValue = data => {
  return data ? data : 'none';
};
//
// export const isValue = (data) => {
//   return data ? <i className="fa fa-check"/> : <i className="fa fa-uncheck"/>;
// };

export const displayNumberRecord = (page, limit, index) => {
  return parseInt(page) === 1 ? index + 1 : page * limit - limit + index + 1;
};

export const getFullname = (firstName, lastName) => {
  if (!firstName || !lastName) return 'none';
  return `${lastName} ${firstName}`;
};

export const parseJSON = data => {
  if (!data && !Array.isArray(data)) return [];
  return JSON.parse(data);
};

export const parseJSONHasValue = (data, value = []) => {
  try {
    if (!data) return value;
    return JSON.parse(data);
  } catch (e) {
    return value;
  }
};

// export const getCurrentState = (states) => {
//   if (!states && !Array.isArray(states)) return null;
//   const state = states[ 0 ] && states[ 0 ].toStates && states[ 0 ].toStates.name;
//   if (!state) return null;
//   return state;
// };

export const dateTimeFormatter = dateTime => {
  if (dateTime === null || dateTime === undefined) return '---';
  return dateTime.substring(0, 19).replace('T', ' ');
};

export const DurationTime = (startDate, endDate) => {
  if (!startDate || !endDate) return '---';
  const end = moment(endDate).utc();
  const start = moment(startDate).utc();
  const weeks = end.diff(start, 'weeks');
  const newStart = start.add(weeks, 'weeks');
  const days = end.diff(newStart, 'days');
  return `${weeks || 0}w ${days || 0}d`;
};

export const calculateLength = array => {
  if (!array || !array.hasOwnProperty('length')) return 0;
  return array.length;
};

export const isLimitDisplay = (array, limit) => {
  if (!array || !array.hasOwnProperty('length')) return 'none';
  if (array.length >= limit) return 'max';
  return 'min';
};

export const formatDateTime = (dateTime, format = 'YYYY-MM-DD HH:mm:ss.SSS', type = 0) => {
  if (!dateTime) return '';
  if (type) return moment(dateTime).format(format);
  return moment(dateTime)
    .utc()
    .format(format);
  // return dateTime;
};

export const linkImage = uri => {
  if (!uri) return null;
  return `${getEnv('S3_BUCKET')}/${uri}`;
};

export const fromNow = date => {
  if (!date || !moment.isMoment(moment(date))) return '---';
  return moment(date).fromNow();
};

export const getDateAt000000ZeroGMT = date => {
  return moment(
    `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} 00:00:00Z`,
    'DD/MM/YYYY HH:mm:ssZ'
  ).toDate();
};

export const getDateAt235959ZeroGMT = date => {
  return moment(
    `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} 23:59:59Z`,
    'DD/MM/YYYY HH:mm:ssZ'
  ).toDate();
};

// export const durationHoursWithNow = (startDate) => {
//   if (!startDate || !moment.isMoment(moment(startDate))) return 0;
//   const end = moment();
//   const start = moment(startDate);
//   const duration = moment.duration(end.diff(start));
//   const seconds = duration.asSeconds();
//   return seconds / (60 * 60);
// };

export const getPriority = value => {
  switch (value) {
    case 1:
      return 'LOW';
    case 2:
      return 'MEDIUM';
    case 3:
      return 'HIGH';
    default:
      return null;
  }
};

const checkTimeString = time => {
  const array = time.split(':');
  if (!Array.isArray(array) || (Array.isArray(array) && array.length !== 2) || isNaN(array[0]) || isNaN(array[1])) {
    return null;
  }
  const num1 = parseInt(array[0], 10);
  const num2 = parseInt(array[1], 10);

  if (num1 < 0 || num1 > 23 || num2 < 0 || num2 > 60) {
    return null;
  }
  return {
    hours: num1,
    minutes: num2
  };
};

const convertToHours = time => {
  const result = checkTimeString(time);
  if (!result) return 0;
  return result.hours + result.minutes / 60;
};

const convertTotalHours = (hours = 0, minutes = 0, seconds = 0, milliseconds = 0) => {
  if (isNaN(hours) || isNaN(minutes) || isNaN(seconds) || isNaN(milliseconds)) return 0;
  return hours + minutes / 60 + seconds / (60 * 60) + milliseconds / (60 * 60 * 60);
};

const convertMonthString = month => {
  if (month.toString().length === 1) return `0${month}`;
  if (month < 1) return 1;
  if (month > 12) return 12;
  return month;
};

const convertDayString = day => {
  if (day.toString().length === 1) return `0${day}`;
  return day;
};

const daysInMonth = (year, month) => {
  return moment(`${year}-${convertMonthString(month)}`, 'YYYY-MM').daysInMonth();
};

const isDayOff = (year, month, day) => {
  const date = moment(`${year}-${convertMonthString(month)}-${convertDayString(day)}`);
  const dayOfWeek = date.weekday(); // ISO
  return dayOfWeek === 6 || dayOfWeek === 0;
};

const totalTimeWorkInDay = ({
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type,
  startYear,
  startMonth,
  startDay
}) => {
  let total = 0;
  if (type === 1 && !isDayOff(startYear, startMonth, startDay) && startTotalHours < endTotalHours) {
    if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      START_TIME_HOUR_MORNING < endTotalHours &&
      endTotalHours < END_TIME_HOUR_MORNING
    ) {
      total = total + (endTotalHours - START_TIME_HOUR_MORNING);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      END_TIME_HOUR_MORNING <= endTotalHours &&
      endTotalHours <= START_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      0 <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_MORNING &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total =
        total +
        (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
        (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (START_TIME_HOUR_MORNING < startTotalHours && endTotalHours < END_TIME_HOUR_MORNING) {
      total = total + (endTotalHours - startTotalHours);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      END_TIME_HOUR_MORNING <= endTotalHours &&
      endTotalHours <= START_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours) + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      START_TIME_HOUR_MORNING < startTotalHours &&
      startTotalHours < END_TIME_HOUR_MORNING &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_MORNING - startTotalHours) + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (
      END_TIME_HOUR_MORNING <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_AFTERNOON &&
      START_TIME_HOUR_AFTERNOON < endTotalHours &&
      endTotalHours < END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (endTotalHours - START_TIME_HOUR_AFTERNOON);
    } else if (
      END_TIME_HOUR_MORNING <= startTotalHours &&
      startTotalHours <= START_TIME_HOUR_AFTERNOON &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
    } else if (START_TIME_HOUR_AFTERNOON < startTotalHours && endTotalHours < END_TIME_HOUR_AFTERNOON) {
      total = total + (endTotalHours - startTotalHours);
    } else if (
      START_TIME_HOUR_AFTERNOON < startTotalHours &&
      startTotalHours < END_TIME_HOUR_AFTERNOON &&
      endTotalHours >= END_TIME_HOUR_AFTERNOON
    ) {
      total = total + (END_TIME_HOUR_AFTERNOON - startTotalHours);
    }
  }
  return total;
};

const totalTimeWorksDaysInMonthOfYear = ({
  startDay,
  endDay,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type,
  year,
  month
}) => {
  let total = 0;
  let i = startDay;
  while (i <= endDay) {
    if (type === 1 && !isDayOff(year, month, i)) {
      if (i === startDay) {
        if (0 <= startTotalHours && startTotalHours <= START_TIME_HOUR_MORNING) {
          total =
            total +
            (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
            (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (START_TIME_HOUR_MORNING < startTotalHours && startTotalHours < END_TIME_HOUR_MORNING) {
          total =
            total + (END_TIME_HOUR_MORNING - startTotalHours) + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (END_TIME_HOUR_MORNING <= startTotalHours && startTotalHours <= START_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
        } else if (START_TIME_HOUR_AFTERNOON < startTotalHours && startTotalHours < END_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_AFTERNOON - startTotalHours);
        } else {
          // do nothing
        }
      } else if (startDay < i && i < endDay) {
        total =
          total +
          (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING) +
          (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON);
      } else if (i === endDay) {
        if (0 <= endTotalHours && endTotalHours <= START_TIME_HOUR_MORNING) {
          //-- do nothing
        } else if (START_TIME_HOUR_MORNING < endTotalHours && endTotalHours < END_TIME_HOUR_MORNING) {
          total = total + (endTotalHours - START_TIME_HOUR_MORNING);
        } else if (END_TIME_HOUR_MORNING <= endTotalHours && endTotalHours <= START_TIME_HOUR_AFTERNOON) {
          total = total + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        } else if (START_TIME_HOUR_AFTERNOON < endTotalHours && endTotalHours < END_TIME_HOUR_AFTERNOON) {
          total =
            total + (endTotalHours - START_TIME_HOUR_AFTERNOON) + (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        } else {
          total =
            total +
            (END_TIME_HOUR_AFTERNOON - START_TIME_HOUR_AFTERNOON) +
            (END_TIME_HOUR_MORNING - START_TIME_HOUR_MORNING);
        }
      }
    }
    i += 1;
  }
  return total;
};

const totalTimeWorksDaysInMonthsOfYear = ({
  startDay,
  endDay,
  startMonth,
  endMonth,
  year,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type
}) => {
  let initMonth = startMonth;
  let initStartDay, initStartTotalHours, initEndDay, initEndTotalHours;
  let total = 0;
  while (initMonth <= endMonth) {
    if (initMonth === startMonth) {
      initStartDay = startDay;
      initStartTotalHours = startTotalHours;
      const initEndDateMonth = moment(
        `${year}-${convertMonthString(initMonth)}-${daysInMonth(year, initMonth)}T23:59:59.000`
      );
      initEndDay = initEndDateMonth.date();
      initEndTotalHours = convertTotalHours(
        initEndDateMonth.hours(),
        initEndDateMonth.minutes(),
        initEndDateMonth.seconds(),
        initEndDateMonth.milliseconds()
      );
    } else if (initMonth === endMonth) {
      const initStartDateMonth = moment(`${year}-${convertMonthString(initMonth)}-01T00:00:00.000`);
      initStartDay = initStartDateMonth.date();
      initStartTotalHours = convertTotalHours(
        initStartDateMonth.hours(),
        initStartDateMonth.minutes(),
        initStartDateMonth.seconds(),
        initStartDateMonth.milliseconds()
      );
      initEndDay = endDay;
      initEndTotalHours = endTotalHours;
    } else if (initMonth < endMonth) {
      const initStartDateMonth = moment(`${year}-${convertMonthString(initMonth)}-01T00:00:00.000`);
      initStartDay = initStartDateMonth.date();
      initStartTotalHours = convertTotalHours(
        initStartDateMonth.hours(),
        initStartDateMonth.minutes(),
        initStartDateMonth.seconds(),
        initStartDateMonth.milliseconds()
      );
      const initEndDateMonth = moment(
        `${year}-${convertMonthString(initMonth)}-${daysInMonth(year, initMonth)}T23:59:59.000`
      );
      initEndDay = initEndDateMonth.date();
      initEndTotalHours = convertTotalHours(
        initEndDateMonth.hours(),
        initEndDateMonth.minutes(),
        initEndDateMonth.seconds(),
        initEndDateMonth.milliseconds()
      );
    }

    total =
      total +
      totalTimeWorksDaysInMonthOfYear({
        startDay: initStartDay,
        endDay: initEndDay,
        startTotalHours: initStartTotalHours,
        endTotalHours: initEndTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        year,
        month: initMonth
      });
    initMonth += 1;
  }
  return total;
};

const totalTimeWorksDaysInYears = ({
  startYear,
  endYear,
  startDay,
  endDay,
  startMonth,
  endMonth,
  startTotalHours,
  endTotalHours,
  START_TIME_HOUR_MORNING,
  END_TIME_HOUR_MORNING,
  START_TIME_HOUR_AFTERNOON,
  END_TIME_HOUR_AFTERNOON,
  type
}) => {
  let initYear = startYear;
  let initStartDay, initStartTotalHours, initEndDay, initEndTotalHours, initStartMonth, initEndMonth;
  let total = 0;
  while (initYear <= endYear) {
    /*
     *    startDay
     *    endDay
     *    startTotalHours
     *    endTotalHours
     */
    if (initYear === startYear) {
      initStartDay = startDay;
      initStartTotalHours = startTotalHours;
      const initEndDateYear = moment(`${initYear}-12-31T23:59:59.000`);
      initStartMonth = startMonth;
      initEndMonth = 12;
      initEndDay = initEndDateYear.date();
      initEndTotalHours = convertTotalHours(
        initEndDateYear.hours(),
        initEndDateYear.minutes(),
        initEndDateYear.seconds(),
        initEndDateYear.milliseconds()
      );
    } else if (initYear === endYear) {
      const initStartDateYear = moment(`${initYear}-01-01T00:00:00.000`);
      initStartMonth = 1;
      initEndMonth = endMonth;
      initStartDay = initStartDateYear.date();
      initStartTotalHours = convertTotalHours(
        initStartDateYear.hours(),
        initStartDateYear.minutes(),
        initStartDateYear.seconds(),
        initStartDateYear.milliseconds()
      );
      initEndDay = endDay;
      initEndTotalHours = endTotalHours;
    } else if (initYear < endYear) {
      const initStartDateYear = moment(`${initYear}-01-01T00:00:00.000`);
      initStartDay = initStartDateYear.date();
      initStartTotalHours = convertTotalHours(
        initStartDateYear.hours(),
        initStartDateYear.minutes(),
        initStartDateYear.seconds(),
        initStartDateYear.milliseconds()
      );
      const initEndDateYear = moment(`${initYear}-12-31T23:59:59.000`);
      initEndDay = initEndDateYear.date();
      initStartMonth = 1;
      initEndMonth = 12;
      initEndTotalHours = convertTotalHours(
        initEndDateYear.hours(),
        initEndDateYear.minutes(),
        initEndDateYear.seconds(),
        initEndDateYear.milliseconds()
      );
    }

    if (initStartMonth === initEndMonth) {
      total =
        total +
        totalTimeWorksDaysInMonthOfYear({
          startDay: initStartDay,
          endDay: initEndDay,
          startTotalHours: initStartTotalHours,
          endTotalHours: initEndTotalHours,
          START_TIME_HOUR_MORNING,
          END_TIME_HOUR_MORNING,
          START_TIME_HOUR_AFTERNOON,
          END_TIME_HOUR_AFTERNOON,
          type,
          year: initYear,
          month: initStartMonth // or initEndMonth
        });
    } else {
      total =
        total +
        totalTimeWorksDaysInMonthsOfYear({
          startDay: initStartDay,
          endDay: initEndDay,
          startMonth: initStartMonth,
          endMonth: initEndMonth,
          year: initYear,
          startTotalHours: initStartTotalHours,
          endTotalHours: initEndTotalHours,
          START_TIME_HOUR_MORNING,
          END_TIME_HOUR_MORNING,
          START_TIME_HOUR_AFTERNOON,
          END_TIME_HOUR_AFTERNOON,
          type
        });
    }
    initYear += 1;
  }
  return total;
};

/**
 * type = 1: no count t7, cn
 * type = 2: count t7, cn
 */
export const durationHoursWork = (startDate, endDate, type = 1) => {
  if (!startDate || !moment.isMoment(moment(startDate)) || !endDate || !moment.isMoment(moment(endDate))) return 0;

  const START_TIME_HOUR_MORNING = convertToHours(getEnv('START_TIME_MORNING'));
  const END_TIME_HOUR_MORNING = convertToHours(getEnv('END_TIME_MORNING'));
  const START_TIME_HOUR_AFTERNOON = convertToHours(getEnv('START_TIME_AFTERNOON'));
  const END_TIME_HOUR_AFTERNOON = convertToHours(getEnv('END_TIME_AFTERNOON'));

  const end = moment(endDate);

  const start = moment(startDate);

  const startYear = start.year();
  const startDay = start.date();
  const startMonth = start.month() + 1;
  const startHours = start.hours();
  const startMinutes = start.minutes();
  const startSeconds = start.seconds();
  const startMilliseconds = start.milliseconds();
  const startTotalHours = convertTotalHours(startHours, startMinutes, startSeconds, startMilliseconds);

  const endYear = end.year();
  const endDay = end.date();
  const endMonth = end.month() + 1;
  const endHours = end.hours();
  const endMinutes = end.minutes();
  const endSeconds = end.seconds();
  const endMilliseconds = end.milliseconds();
  const endTotalHours = convertTotalHours(endHours, endMinutes, endSeconds, endMilliseconds);

  if (START_TIME_HOUR_MORNING < END_TIME_HOUR_MORNING < START_TIME_HOUR_AFTERNOON < END_TIME_HOUR_AFTERNOON) {
    if (startDay === endDay && startTotalHours < endTotalHours && startMonth === endMonth && startYear === endYear) {
      return totalTimeWorkInDay({
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        startYear,
        startMonth,
        startDay
      });
    } else if (startDay < endDay && startMonth === endMonth && startYear === endYear) {
      return totalTimeWorksDaysInMonthOfYear({
        startDay,
        endDay,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type,
        year: startYear,
        month: startMonth
      });
    } else if (startMonth < endMonth && startYear === endYear) {
      return totalTimeWorksDaysInMonthsOfYear({
        startDay,
        endDay,
        startMonth,
        endMonth,
        year: startYear,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type
      });
    } else if (startYear < endYear) {
      return totalTimeWorksDaysInYears({
        startYear,
        endYear,
        startDay,
        endDay,
        startMonth,
        endMonth,
        startTotalHours,
        endTotalHours,
        START_TIME_HOUR_MORNING,
        END_TIME_HOUR_MORNING,
        START_TIME_HOUR_AFTERNOON,
        END_TIME_HOUR_AFTERNOON,
        type
      });
    }
  }
  return 0;
};

export const isAllowPermission = (permissions, permissionName, projectCondition) => {
  const isArray = Array.isArray(permissionName);
  const relevantPermissions =
    !!permissions &&
    (isArray
      ? permissions.filter(per => permissionName.includes(per.permission))
      : permissions.filter(per => per.permission === permissionName));

  return (
    (!projectCondition && !!relevantPermissions.length) ||
    (!!relevantPermissions &&
      relevantPermissions.some(per => {
        //-- no condition or project depend condition must match
        return (
          per.permissionCondition.length === 0 ||
          per.permissionCondition.some(
            cond => cond.name === 'project_depend' && cond.value.includes(Number(projectCondition))
          )
        );
      }))
  );
};

export const percent = (value, total) => {
  if (!Number(value) || !Number(total)) return 0;
  return (value / total) * 100;
};

export const getIconFile = typeFile => {
  const mimeType = mimeTypes.find(m => m.mimeType.mime === typeFile);
  if (mimeType) return mimeType.mimeType.icon || 'fa fa-file-o';
  return 'fa fa-file-o';
};

// export const tokenDecoded = () => {
//   const token = getKey(USER_ACCESS_TOKEN);
//   if (!token)
//     return 0;
//   const decoded = decode(token);
//   return decoded.owner;
// };
//
// export const totalWorkDays = (startDate, endDate) => {
//   if (!startDate || !moment.isMoment(moment(startDate)) || !endDate || !moment.isMoment(moment(endDate))) return 0;
//   let totalDay = 0;
//   let start = moment(`${ moment(startDate).utc().format('YYYY-MM-DD') }T00:01:00.000`).utc();
//   const end = moment(`${ moment(endDate).utc().format('YYYY-MM-DD') }T23:59:00.000`).utc();
//   while (start <= end) {
//     const dayOfWeek = start.weekday(); // ISO
//     if (dayOfWeek >= 1 && dayOfWeek <= 5) {
//       totalDay = totalDay + 1;
//     }
//     start = start.add(1, 'days');
//   }
//   return totalDay;
// };
//
export const customBorderColor = {
  control: (base, state) => ({
    ...base,
    boxShadow: state.isFocused ? `0 0 0 1px ${BORDER_COLOR_DEFAULT}` : 0,
    borderColor: state.isFocused ? BORDER_COLOR_DEFAULT : base.borderColor,
    '&:hover': {
      borderColor: state.isFocused ? BORDER_COLOR_DEFAULT : base.borderColor
    }
  })
};

export const groupBy = key => array =>
  array.reduce((objectsByKeyValue, obj) => {
    const value = obj[key];
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
    return objectsByKeyValue;
  }, {});

export const getStartOfMonth = utcString =>
  moment(utcString)
    .utc()
    .startOf('month')
    .format();

export const getEndOfMonth = utcString =>
  moment(utcString)
    .utc()
    .endOf('month')
    .format();

export const getStartOfYear = utcString =>
  moment(utcString)
    .utc()
    .startOf('year')
    .format();

export const getEndOfYear = utcString =>
  moment(utcString)
    .utc()
    .endOf('year')
    .format();

export const getDate = utcString =>
  moment(utcString)
    .utc()
    .get('date');

export const getMonth = utcString =>
  moment(utcString)
    .utc()
    .get('month');

export const getYear = utcString =>
  moment(utcString)
    .utc()
    .get('year');

export const sortObjectInArray = (arr, attributes, asc = true) => {
  arr.sort((a, b) => {
    for (let i = 0; i < attributes.length; i += 1) {
      if (a[attributes[i]] < b[attributes[i]]) return asc ? -1 : 1;
      if (a[attributes[i]] > b[attributes[i]]) return asc ? 1 : -1;
      if (i === attributes.length) return 0;
    }
    return 0;
  });
  return;
};

export const attributeAverageInArray = (array, attribute) => {
  let value = 0;
  let average = 0;
  for (let i = 0; i < array.length; i++) {
    value += array[i][attribute];
  }
  average = value / array.length;
  return average;
};

export const convertDate = date => {
  const d = new Date(date);

  return new Date(
    d.getUTCFullYear(),
    d.getUTCMonth(),
    d.getUTCDate(),
    d.getUTCHours(),
    d.getUTCMinutes(),
    d.getUTCSeconds()
  );
};

const roundStartDate = dateStr => {
  const date = new Date(dateStr);

  if (date.getSeconds() !== 0) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 60);
  }
  return dateStr;
};

const roundEndDate = dateStr => {
  const date = new Date(dateStr);

  if (date.getSeconds() !== 59) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), -1);
  }
  return dateStr;
};

export const formatAllocations = (list, startDateChart, endDateChart) => {
  const data = [];
  let minDate = new Date().toString();
  let maxDate = null;

  list.forEach(l =>
    l.users.forEach(u =>
      u.allocations.forEach(a => {
        new Date(minDate) >= new Date(a.startDate) && (minDate = a.startDate);
        new Date(maxDate) <= new Date(a.endDate) && (maxDate = a.endDate);
      })
    )
  );
  startDateChart && (minDate = startDateChart);
  endDateChart && (maxDate = endDateChart);

  list.forEach(l => {
    const groupId = data.length + 1;

    data.push({
      id: groupId,
      level: 1,
      skill: l.skill && l.skill.name,
      linkIcon: l.skill && l.skill.linkIcon,
      user: '',
      projects: null,
      freeEffort: 100,
      title: `${l.skill.name} (${l.users.length})`,
      full_name: '',
      text: l.skill.name,
      tooltip: [],
      open: true
    });

    l.users.forEach(u => {
      const splitId = data.length + 1;

      data.push({
        id: splitId,
        level: 2,
        skill: l.skill.name,
        user: u.name,
        avatar: u.avatar,
        projects: u.allocations.map(a => a.project),
        freeEffort: 100,
        title: u.name,
        full_name: u.name,
        text: '',
        tooltip: [],
        parent: groupId,
        render: 'split'
      });

      const dates = [];

      u.allocations.forEach(a => {
        dates.push(a.startDate);
        dates.push(a.endDate);
      });
      !dates.includes(minDate) && dates.push(minDate);
      !dates.includes(maxDate) && dates.push(maxDate);
      dates.sort((a, b) => new Date(a) - new Date(b));

      let datesIncludeDayOff = [...dates];
      const timeStartDay = +new Date(dates[0]) + 86400000;
      const timeEndDay = +new Date(dates[dates.length - 1]) - 86400000;
      for (let timeCurrentDay = timeStartDay; timeCurrentDay < timeEndDay; timeCurrentDay += 86400000) {
        switch (new Date(timeCurrentDay).getDay()) {
          case 6:
            datesIncludeDayOff = [...datesIncludeDayOff, new Date(timeCurrentDay)];
            break;
          case 0:
            datesIncludeDayOff = [...datesIncludeDayOff, new Date(timeCurrentDay + 86399000)];
            break;

          default:
            break;
        }
      }

      const arrangedDatesIncludeDayOff = [...datesIncludeDayOff].sort((a, b) => new Date(a) - new Date(b));
      const dateGroupsIncludeDayOff = arrangedDatesIncludeDayOff.map((d, i) => ({
        startDate: roundStartDate(d),
        endDate: roundEndDate(arrangedDatesIncludeDayOff[i + 1])
      }));
      dateGroupsIncludeDayOff.pop();

      const dateGroups = dateGroupsIncludeDayOff.filter(
        dates =>
          new Date(dates.startDate).getUTCDay() !== 6 &&
          new Date(dates.startDate).getUTCDay() !== 0 &&
          new Date(dates.endDate).getUTCDay() !== 6 &&
          new Date(dates.endDate).getUTCDay() !== 0
      );
      const timeIntervals = dateGroups.map(dg => {
        const tooltip = [];
        let projects = '';
        let effort = 0;

        u.allocations.forEach(a => {
          if (new Date(dg.startDate) >= new Date(a.startDate) && new Date(dg.endDate) <= new Date(a.endDate)) {
            tooltip.push({ project: a.project, effort: a.effortPercent });
            projects = projects ? `${projects} - ${a.project}` : a.project;
            effort += a.effortPercent;
          }
        });

        return {
          tooltip,
          projects,
          effort,
          startDate: dg.startDate,
          endDate: dg.endDate
        };
      });

      timeIntervals.forEach(t => {
        const c = new Date(t.endDate) - new Date(t.startDate) <= 2 * 24 * 60 ** 2 * 1000;
        const text = c ? `${t.effort}%` : `${t.effort}% : ${t.projects}`;

        data.push({
          id: data.length + 1,
          level: 3,
          skill: l.skill.name,
          user: u.name,
          projects: t.projects.split(' - '),
          freeEffort: 100 - t.effort,
          text: t.projects ? text : '0 %',
          tooltip: t.tooltip,
          start_date: convertDate(t.startDate),
          end_date: convertDate(t.endDate),
          parent: splitId
        });
      });
    });
  });
  return data;
};

export const filterAllocations = (listData, startDate, endDate, freeEffort, skill, project, user) => {
  const datafilter = listData
    .filter(ld => skill === null || ld.skill === skill.value)
    .filter(ld => user === '' || ld.user === '' || ld.user.includes(user))
    .filter(ld => project === null || ld.projects === null || ld.projects.includes(project.value))
    .filter(ld => freeEffort === null || ld.freeEffort >= freeEffort)
    .filter(
      ld =>
        startDate === null ||
        !ld.end_date ||
        new Date(ld.end_date).getTime() > new Date(startDate).getTime() + 7 * 60 ** 2 * 1000
    )
    .filter(ld => endDate === null || !ld.start_date || new Date(ld.start_date) < new Date(endDate));

  const users = datafilter.filter(df => df.level === 2);
  const alcs = datafilter.filter(df => df.level === 3);
  const userHasChilds = users.filter(u => alcs.some(a => u.user === a.user));
  const userRemoves = users.filter(u => {
    let isRemove = true;

    userHasChilds.forEach(uhc => {
      if (u.user === uhc.user) {
        isRemove = false;
      }
    });
    return isRemove;
  });

  const filterUserByEffort = datafilter.filter(df => {
    let isRest = true;

    userRemoves.forEach(u => {
      df.user === u.user && (isRest = false);
    });
    return isRest;
  });

  const injectTotalUserAffterFilter = filterUserByEffort
    .map(df => {
      if (!df.parent) {
        let total = 0;

        filterUserByEffort.forEach(d => {
          df.id === d.parent && total++;
        });
        return { ...df, title: `${df.skill} (${total})` };
      }
      return df;
    })
    .filter(df => {
      if (!df.parent) {
        let total = 0;

        filterUserByEffort.forEach(d => {
          df.id === d.parent && total++;
        });
        return total;
      }
      return df;
    });
  return injectTotalUserAffterFilter;
};

export const getDuplicateObjectInArray = attributes =>
  R.pipe(
    R.project(attributes),
    R.reduce(
      ({ results, foundOnce }, item) =>
        R.contains(item, foundOnce)
          ? { results: R.append(item, results), foundOnce }
          : { results, foundOnce: R.append(item, foundOnce) },
      { results: [], foundOnce: [] }
    ),
    R.prop('results'),
    R.uniq
  );

export const toFixed = (number, fractionDigits) => {
  if (isNaN(number)) return 0;
  return parseFloat(number.toFixed(fractionDigits));
};

export const convertUTCString = date => {
  return moment(new Date(date))
    .utc()
    .format();
};

export const isValidDate = d => {
  return d instanceof Date && !isNaN(d);
};

export const excelDateToJSDate = date => {
  return new Date(Math.round((date - 25569) * 86400 * 1000));
};

export const getRankOfSkill = (point, name) => {
  if (!name) {
    return null;
  }
  let listRanks = [];
  const isPM = PM_NAME_SKILL.indexOf(name.toUpperCase()) !== -1;
  if (isPM) {
    listRanks = RANK_SKILL_PM;
  } else {
    switch (name.toUpperCase()) {
      case SKILL_RANK_TYPE.adminCB.toUpperCase():
        listRanks = RANK_SKILL_ADMIN_CB;
        break;
      case SKILL_RANK_TYPE.hrRecruiter.toUpperCase():
        listRanks = RANK_SKILL_HR_RECRUITER;
        break;
      case SKILL_RANK_TYPE.accounting.toUpperCase():
        listRanks = RANK_SKILL_ACCOUNTING;
        break;
      case SKILL_RANK_TYPE.copywriter.toUpperCase():
        listRanks = RANK_SKILL_COPYWRITER;
        break;
      case SKILL_RANK_TYPE.digitalMarketing.toUpperCase():
        listRanks = RANK_SKILL_DIGITAL_MARKETING;
        break;
      case SKILL_RANK_TYPE.brandMarketing.toUpperCase():
        listRanks = RANK_SKILL_BRAND_MARKETING;
        break;
      case SKILL_RANK_TYPE.sales.toUpperCase():
        listRanks = RANK_SKILL_SALES;
        break;
      case SKILL_RANK_TYPE.brse.toUpperCase():
        listRanks = RANK_SKILL_BRSE;
        break;
      default:
        listRanks = RANK_SKILL_NORMAL;
    }
  }

  for (const rank of Object.values(listRanks)) {
    const isZeroValue = point === 0 && rank.min === 0;
    if ((rank.min < point || isZeroValue) && point <= rank.max) {
      return rank;
    }
  }
};

export const getRanksByTypeSkill = skill => {
  let ranks = [];
  switch (skill) {
    case SKILL_RANK_TYPE.pm:
      ranks = RANK_SKILL_PM;
      break;
    case SKILL_RANK_TYPE.adminCB:
      ranks = RANK_SKILL_ADMIN_CB;
      break;
    case SKILL_RANK_TYPE.hrRecruiter:
      ranks = RANK_SKILL_HR_RECRUITER;
      break;
    case SKILL_RANK_TYPE.accounting:
      ranks = RANK_SKILL_ACCOUNTING;
      break;
    case SKILL_RANK_TYPE.copywriter:
      ranks = RANK_SKILL_COPYWRITER;
      break;
    case SKILL_RANK_TYPE.digitalMarketing:
      ranks = RANK_SKILL_DIGITAL_MARKETING;
      break;
    case SKILL_RANK_TYPE.brandMarketing:
      ranks = RANK_SKILL_BRAND_MARKETING;
      break;
    case SKILL_RANK_TYPE.sales:
      ranks = RANK_SKILL_SALES;
      break;
    case SKILL_RANK_TYPE.brse:
      ranks = RANK_SKILL_BRSE;
      break;
    case SKILL_RANK_TYPE.normal:
      ranks = RANK_SKILL_NORMAL;
      break;
    default:
      ranks = RANK_SKILL_NORMAL;
  }
  return Object.values(ranks);
};

export const transferToArrayValue = (arrayObject, property = 'id') => {
  const outputArray = [];
  if (!arrayObject || !Array.isArray(arrayObject)) {
    return outputArray;
  }
  arrayObject.forEach(object => {
    if (object[property] !== undefined) outputArray.push(object[property]);
  });
  return outputArray;
};

export const isChangeObject = (newObject, oldObject) => {
  for (const prop in newObject) {
    if (newObject[prop] !== oldObject[prop]) {
      return true;
    }
  }
  return false;
};

export const getEndDateLocal = date =>
  moment(date)
    .endOf('date')
    .toDate();

export const getDateUTCtoLocal = date => {
  const utcDate = moment(date).utc();
  return new Date(utcDate.year(), utcDate.month(), utcDate.date(), utcDate.hour(), utcDate.minute(), utcDate.second());
};

export const formatAllocationPlans = (allocationPlanUsers, startDateChart, endDateChart) => {
  const data = [];
  const nowDate = new Date();
  const toDayTime = Date.UTC(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
  let minDate = new Date(toDayTime - 2 * 24 * 60 ** 2 * 1000);
  let maxDate = new Date(toDayTime + 5 * 24 * 60 ** 2 * 1000 - 1000);

  allocationPlanUsers.forEach(u =>
    u.allocationsPlanAs.forEach(a => {
      new Date(minDate) >= new Date(a.startDate) && (minDate = a.startDate);
      new Date(maxDate) <= new Date(a.endDate) && (maxDate = a.endDate);
    })
  );
  startDateChart && (minDate = startDateChart);
  endDateChart && (maxDate = endDateChart);

  allocationPlanUsers.forEach(u => {
    const skills = u.skills.map(item => item.id);
    const idDataUser = data.length + 1;
    data.push({
      id: idDataUser,
      level: 2,
      user: u.name,
      avatar: u.avatar,
      freeEffort: 100,
      title: u.name,
      full_name: u.name,
      text: '',
      tooltip: [],
      render: 'split',
      skills,
      infoMentor: u.infoMentor
    });

    const dates = [];

    u.allocationsPlanAs.forEach(a => {
      dates.push(a.startDate);
      dates.push(a.endDate);
    });
    !dates.includes(minDate) && dates.push(minDate);
    !dates.includes(maxDate) && dates.push(maxDate);
    dates.sort((a, b) => new Date(a) - new Date(b));

    let datesIncludeDayOff = [...dates];
    const timeStartDay = +new Date(dates[0]) + 86400000;
    const timeEndDay = +new Date(dates[dates.length - 1]) - 86400000;
    for (let timeCurrentDay = timeStartDay; timeCurrentDay < timeEndDay; timeCurrentDay += 86400000) {
      switch (new Date(timeCurrentDay).getDay()) {
        case 6:
          datesIncludeDayOff = [...datesIncludeDayOff, new Date(timeCurrentDay)];
          break;
        case 0:
          datesIncludeDayOff = [...datesIncludeDayOff, new Date(timeCurrentDay + 86399000)];
          break;

        default:
          break;
      }
    }

    const arrangedDatesIncludeDayOff = [...datesIncludeDayOff].sort((a, b) => new Date(a) - new Date(b));
    const dateGroupsIncludeDayOff = arrangedDatesIncludeDayOff.map((d, i) => ({
      startDate: roundStartDate(d),
      endDate: roundEndDate(arrangedDatesIncludeDayOff[i + 1])
    }));
    dateGroupsIncludeDayOff.pop();

    const dateGroups = dateGroupsIncludeDayOff.filter(
      dates =>
        new Date(dates.startDate).getUTCDay() !== 6 &&
        new Date(dates.startDate).getUTCDay() !== 0 &&
        new Date(dates.endDate).getUTCDay() !== 6 &&
        new Date(dates.endDate).getUTCDay() !== 0
    );
    const timeIntervals = dateGroups.map(dg => {
      const tooltip = [];
      let effort = 0;

      u.allocationsPlanAs.forEach(a => {
        if (new Date(dg.startDate) >= new Date(a.startDate) && new Date(dg.endDate) <= new Date(a.endDate)) {
          tooltip.push({ effort: a.effortPercent });
          effort += a.effortPercent;
        }
      });

      return {
        tooltip,
        effort,
        startDate: dg.startDate,
        endDate: dg.endDate
      };
    });

    timeIntervals.forEach(t => {
      data.push({
        id: data.length + 1,
        level: 3,
        user: u.name,
        freeEffort: 100 - t.effort,
        text: t.effort ? `${t.effort}%` : '0 %',
        tooltip: t.tooltip,
        start_date: convertDate(t.startDate),
        end_date: convertDate(t.endDate),
        parent: idDataUser
      });
    });
  });
  return data;
};

export const filterAllocationPlans = ({ listData, startDate, endDate, freeEffort, user, skill, mentor }) => {
  const datafilter = listData.filter(ld => {
    let isFilter = true;
    isFilter = isFilter && (!freeEffort || ld.freeEffort >= freeEffort);
    if (isFilter) {
      const endDateTime = new Date(ld.end_date).getTime();
      const startDateTime = new Date(startDate).getTime() + 7 * 60 ** 2 * 1000;
      isFilter = !startDate || !ld.end_date || endDateTime > startDateTime;
    }
    isFilter = isFilter && (!endDate || !ld.start_date || new Date(ld.start_date) < new Date(endDate));

    if (ld.level === 2) {
      if (isFilter && mentor) {
        isFilter = ld.infoMentor && ld.infoMentor.id === mentor.value;
      }
      if (isFilter && skill) {
        isFilter = Array.isArray(ld.skills) && ld.skills.includes(skill.value);
      }
      isFilter = isFilter && (!user || !ld.user || ld.user.includes(user));
    }
    return isFilter;
  });
  return datafilter;
};

export const removeDuplicateObjInArray = ({ list, key }) => {
  return list.reduce((unique, o) => {
    if (!unique.some(obj => obj[key] === o[key])) unique.push(o);
    return unique;
  }, []);
};

export const calculateRemainingRate = (totalTime, endDate, startDate, estimate) => {
  let remainingRate = 100;
  if (!totalTime || !endDate) {
    const remainingWithNow = durationHoursWork(startDate, new Date());
    if (remainingWithNow < estimate) {
      remainingRate = (100 * (estimate - remainingWithNow)) / estimate;
    } else {
      remainingRate = 0;
    }
  } else {
    const remainingWithNow = durationHoursWork(endDate, new Date());
    if (remainingWithNow + totalTime < estimate) {
      remainingRate = (100 * (estimate - remainingWithNow - totalTime)) / estimate;
    } else {
      remainingRate = 0;
    }
  }
  return remainingRate;
};

export const randomUniqueString = () => {
  return (
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15)
  );
};

export const promiseDelayImport = (importFunc, timeout = 1000) => () => {
  return new Promise(resolve => {
    setTimeout(() => resolve(importFunc), timeout);
  });
};

export const safelyParseJSON = json => {
  let parsed;
  try {
    parsed = JSON.parse(json);
  } catch (e) {
    return [];
  }
  return parsed;
};

export const filterUserRelateInProject = (usersIntoProject, userRelate) => {
  const data =
    calculateLength(usersIntoProject) && calculateLength(userRelate)
      ? removeDuplicateObjInArray({ list: usersIntoProject, key: 'userId' }).filter(item =>
          userRelate.find(value => item.userId === value)
        )
      : [];

  return data.map(item => ({
    ...item,
    label: item.User && item.User.name,
    value: item.User && item.User.id
  }));
};

export const getOptionsBranchFromListUser = listAdmins => {
  const options = listAdmins
    .filter(user => user.branch)
    .map(user => {
      return {
        label: user.branch.name,
        value: user.branch.id
      };
    });
  return removeDuplicateObjInArray({ list: options, key: 'value' });
};

export const mapArrayGetValue = array => {
  return calculateLength(array) ? array.map(item => item.value) : [];
};

export const getOptionUserRelate = data => {
  const result = calculateLength(data)
    ? data
        .reduce((cum, user) => {
          const u = cum.find(u => u.userId === user.userId);
          if (!u) {
            return [...cum, user];
          }
          return cum;
        }, [])
        .map(user => {
          return {
            value: user && user.userId,
            label: user && user.User && user.User.name
          };
        })
    : [];
  return result;
};

export const getAllPMByProject = data => {
  const result = [];
  calculateLength(data) &&
    data.forEach(p => {
      p.members.forEach(m => {
        m.roles.forEach(r => {
          if (r.name.toUpperCase() === PROJECT_MANAGER_NAME) result.push({ value: m.id, label: m.name, userId: m.id });
        });
      });
    });
  return removeDuplicateObjInArray({ list: result, key: 'userId' });
};

export const getRolesOption = data => {
  const result = [];
  calculateLength(data) &&
    data.forEach(p => {
      p.members.forEach(m => {
        m.roles.forEach(r => {
          result.push({ value: r.id, label: r.name, userId: r.id });
        });
      });
    });
  return removeDuplicateObjInArray({ list: result, key: 'userId' });
};

export const getUserOption = data => {
  const result = [];
  calculateLength(data) &&
    data.forEach(p => {
      p.members.forEach(m => {
        result.push({ value: m.id, label: m.name, userId: m.id });
      });
    });
  return removeDuplicateObjInArray({ list: result, key: 'userId' });
};

export const getProjectOption = projects => {
  return calculateLength(projects) ? projects.map(p => ({ value: p.id, label: p.name })) : [];
};

export const onQueryStringUrl = (value, name, location) => {
  const urlQuery = queryString.parse(location.search);
  if (value || value === 0) {
    urlQuery[name] = value;
  } else {
    delete urlQuery[name];
  }
  return queryString.stringify(urlQuery);
};

export const onRewriteUrl = (history, location, value, name) => {
  history.push(`${location.pathname}?${onQueryStringUrl(value, name, location)}`);
};

export const getAllMentorsOption = users => {
  const result = calculateLength(users)
    ? users
        .filter(user => user.infoMentor)
        .map(mentor => {
          const label = mentor.infoMentor.name;
          const userId = mentor.infoMentor.id;
          return { label, value: userId, userId } || [];
        })
    : [];
  return removeDuplicateObjInArray({ list: result, key: 'userId' });
};

export const getArrayMember = arrayUsers => {
  let arrayMember = [];
  calculateLength(arrayUsers) &&
    // eslint-disable-next-line
    arrayUsers.map(user => {
      if (user) {
        const indexUser = arrayMember.findIndex(a => a.userId === user.userId);
        if (indexUser !== -1) {
          const userUpdate = arrayMember[indexUser];
          userUpdate.role = [...userUpdate.role, user.ProjectRole];
          arrayMember = [...arrayMember.slice(0, indexUser), userUpdate, ...arrayMember.slice(indexUser + 1)];
        } else {
          const userAddition = {
            userId: user.userId,
            User: user.User,
            role: [user.ProjectRole]
          };
          arrayMember = [...arrayMember, userAddition];
        }
      }
    });
  return arrayMember;
};

export const calcKPIBehaviorMonth = (rawData = []) => {
  const DEFAULT_BEHAVIOR_POINT = getEnv('DEFAULT_BEHAVIOR_POINT');
  const KPI_MAXIMUM_POINT = getEnv('KPI_MAXIMUM_POINT');
  const keyGroupMonth = 'startOfMonth';
  const keyUserGroup = 'userId';
  const data = rawData.map(item => ({
    ...item,
    [`${keyGroupMonth}`]: getStartOfMonth(item.evaluatedAt)
  }));
  const listKpiBehaviour = [];
  const groupData = groupBy(keyGroupMonth)(data);

  for (const startMonthDate in groupData) {
    if (groupData.hasOwnProperty(startMonthDate)) {
      const kpiData = groupData[startMonthDate];
      const groupUserData = groupBy(keyUserGroup)(kpiData);
      for (const userId in groupUserData) {
        const hasOwnProperty = groupUserData.hasOwnProperty(userId);
        if (hasOwnProperty && Array.isArray(groupUserData[userId]) && groupUserData[userId].length > 0) {
          let point = DEFAULT_BEHAVIOR_POINT;
          let totalPointAdd = 0;
          let totalPointSub = 0;
          let logDetailBehaviors = [];
          const actors = [];
          for (let i = 0; i < groupUserData[userId].length; i++) {
            const kpiBehaviour = groupUserData[userId][i];
            point += kpiBehaviour.point;
            totalPointAdd += kpiBehaviour.totalPointAdd;
            totalPointSub += kpiBehaviour.totalPointSub;
            logDetailBehaviors = logDetailBehaviors.concat(kpiBehaviour.logDetailBehaviors);
            const isExistActor = actors.some(item => item.id === kpiBehaviour.actor);
            if (!isExistActor) {
              actors.push(kpiBehaviour.infoActor);
            }
          }
          if (point < 0) {
            point = 0;
          } else if (point > KPI_MAXIMUM_POINT) {
            point = KPI_MAXIMUM_POINT;
          }
          listKpiBehaviour.push({
            ...groupUserData[userId][0],
            point,
            totalPointAdd,
            totalPointSub,
            logDetailBehaviors,
            actors,
            items: groupUserData[userId]
          });
        }
      }
    }
  }
  sortObjectInArray(listKpiBehaviour, [keyGroupMonth], false);
  return listKpiBehaviour;
};

export const totalByPriority = (data, priority) => {
  return data.reduce((total, item) => total + item[priority], 0);
};

export const getStatusToggleLight = () => {
  const status = getKey(LIGHT_BACKGROUND_TOGGLE);
  if (!status) {
    setKey(LIGHT_BACKGROUND_TOGGLE, STATUS_LIGHT.ON);
    return STATUS_LIGHT.ON;
  }
  return Number(status);
};

export const getStatusToggleFullBoard = () => {
  const status = getKey(FULL_BOARD_TOGGLE);
  if (!status) {
    setKey(FULL_BOARD_TOGGLE, STATUS_FULL_BOARD.ZOOM_OUT);
    return STATUS_FULL_BOARD.ZOOM_OUT;
  }
  return Number(status);
};
