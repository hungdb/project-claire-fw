/*
 *=================================================================================================
 * Socket Helper
 *=================================================================================================
 */

import openSocket from 'socket.io-client';
import { getEnv } from '../env';
import { getKey } from './storage';
import { USER_ACCESS_TOKEN } from '../07-constants/common';
// import { toast } from 'react-toastify';
// import i18n from '../i18n';
// import { TOAST_ERROR_STYLE } from '../05-utils/commonData'

export const connectSocket = () => {
  const socketToken = JSON.parse(getKey(USER_ACCESS_TOKEN));
  return openSocket(getEnv('REACT_APP_API_SERVER'), {
    query: `token=${!!socketToken && socketToken.token}`
  });
};

export const errorListener = ({ socket }) => {
  return (
    socket &&
    socket.on('error', error => {
      // if (error.type === 'UnauthorizedError' || error.code === 'invalid_token')
      // toast.error(i18n.t('user_token_has_expired'), TOAST_ERROR_STYLE);
      // toast.error(i18n.t('Oops! something socket error.'), TOAST_ERROR_STYLE);
    })
  );
};

export const removeListener = ({ socket, events }) => {
  return events && events.map(event => socket && socket.removeListener(event));
};

export const disconnectSocket = ({ socket }) => {
  return socket && socket.disconnect();
};

export const subscribeEvent = ({ socket, event, callback }) => {
  return (
    socket &&
    socket.on('connect', () => {
      socket.on(event, callback);
    })
  );
};
