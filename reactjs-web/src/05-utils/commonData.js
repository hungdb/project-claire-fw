import { PERM_LIST_ALLOCATION_PLAN_OF_USERS, PERM_CREATE_ALLOCATION_PLAN } from '../07-constants/app-permission';
import { css } from 'glamor';

const COLORS = {
  RED: '#CE1225',
  GREEN: '#00c91a'
};

const EFFORT_TYPES = {
  UNTOUCHED: 'untouched',
  HOLDING: 'holding',
  PROGRESS: 'progress',
  TERMINATED: 'terminated'
};

const EFFORT_TYPES_ARRAY = ['untouched', 'holding', 'progress', 'terminated'];

const PAGINATE_DOCUMENT_LIMIT = 10;
const PAGINATE_ATTACHMENT_LIMIT = 9;
const PAGINATE_COMMENT_LIMIT = 10;
const PAGINATE_NOTIFICATION_LIMIT = 10;
const PAGINATE_COMMENT_ISSUE_LIMIT = 5;

const PRIORITY_OPTIONS = [{ value: 3, label: 'High' }, { value: 2, label: 'Medium' }, { value: 1, label: 'Low' }];

const COMPARISON_ESTIMATE_OPTIONS = [
  { value: 1, label: '< 8h' },
  { value: 2, label: '= 8h' },
  { value: 3, label: '> 8h' }
];

const ISSUE_OPTION = {
  Open: { value: 1, label: 'Open' },
  CLOSED: { value: 2, label: 'Closed' },
  SOLVING: { value: 3, label: 'Solving' }
};

const ISSUE_OPTIONS = Object.values(ISSUE_OPTION);

const TASK_TYPE_OPTIONS = [{ value: 1, label: 'Normal' }, { value: 2, label: 'Bug' }];

const USERS_ALL_TABS_TYPE = [
  {
    label: 'all',
    value: 1
  },
  {
    label: 'users_project',
    value: 2
  },
  {
    label: 'users_out_system',
    value: 3
  },
  {
    label: 'users_not_active',
    value: 4
  }
];

const PRIORITY_OBJECT = {
  HIGH: 3,
  MEDIUM: 2,
  LOW: 1
};

const TASK_TYPE_OBJECT = {
  NORMAL: 1,
  BUG: 2
};

const TYPE_GET_TASK = {
  ALL: 1,
  MY_TASK: 2
};

const ARRAY_TYPE_APPLY = [
  {
    value: 1,
    label: 'GENERAL'
  },
  {
    value: 2,
    label: 'SPRINT RESPONSIBILITY'
  },
  {
    value: 3,
    label: 'SPRINT DEDICATION'
  }
];

const TYPE_APPLY_BEHAVIOR_OBJECT = {
  GENERAL: 1,
  SPRINT_RESPONSIBILITY: 2,
  SPRINT_DEDICATION: 3
};

const ARRAY_TYPE_ACTION = [
  {
    value: 1,
    label: 'ADD'
  },
  {
    value: 2,
    label: 'SUB'
  }
];

const ARRAY_IS_APPLY = [
  {
    value: 1,
    label: 'ALLOWED'
  },
  {
    value: 0,
    label: 'NOT ALLOWED'
  }
];

const TYPE_ACTION_BEHAVIOR_OBJECT = {
  ADD: 1,
  SUB: 2
};

const TYPE_SUB_APPLY = [
  {
    value: 1,
    label: 'STANDARD'
  },
  {
    value: 2,
    label: 'BONUS'
  }
];

const POINT_KPI_LEADER = [
  {
    label: 'VERY GOOD',
    value: 140
  },
  {
    label: 'GOOD',
    value: 120
  },
  {
    label: 'NORMAL',
    value: 100
  },
  {
    label: 'BAD',
    value: 80
  },
  {
    label: 'VERY BAD',
    value: 60
  }
];

const BORDER_COLOR_DEFAULT = '#0A9FED';

const SKILL_TYPES = {
  normal: 1,
  project: 2,
  yourSelf: 3
};

const KPI_TYPE = {
  behavior: { label: 'Behavior', value: '1' },
  skill: { label: 'Skill', value: '2' },
  projectMember: { label: 'Project member (sprint)', value: '3' },
  projectLeader: { label: 'Project leader (sprint)', value: '4' }
};

const KPI_TYPE_OPTIONS = [KPI_TYPE.behavior, KPI_TYPE.skill, KPI_TYPE.projectMember, KPI_TYPE.projectLeader];

const KPI_TYPE_TIME = {
  month: { label: 'month', value: '1' },
  year: { label: 'year', value: '2' }
};

const KPI_TYPE_TIME_OPTIONS = [KPI_TYPE_TIME.month, KPI_TYPE_TIME.year];

const ROWS_PER_PAGE_TASK_OPTIONS = [10, 50, 100, 1000, 10000];
const ROWS_PER_PAGE_SKILL_OPTIONS = [10, 50, 100, 200];
const ROWS_PER_PAGE_OPTIONS = [5, 10, 50, 100, 200, 1000, 10000];

const EVALUATE_KPI_SKILL = {
  POINT_ONE: 90,
  POINT_TWO: 50
};

const ISSUE_STATUS = {
  OPEN: 1,
  CLOSE: 2,
  SOLVING: 3,
  ALL: 4
};

const TYPES_KPI_SPRINT = {
  MEMBER: 1,
  LEADER: 2
};

const TYPE_LOG_BEHAVIOR = {
  RESPONSIBILITY: 1,
  DEDICATION: 2
};

const TYPE_KPI_COEFFICIENT = {
  KPI_SKILL: 1,
  KPI_LEADER: 2,
  KPI_PROJECT: 3
};

const NOT_CLOSED_STATUS_TASK = { value: -1, label: 'NOT CLOSED' };

const SUBSCRIBE_METHODS = [
  { value: 1, label: 'email', class: 'fa-envelope' },
  { value: 2, label: 'SMS', class: 'fa-commenting' },
  { value: 3, label: 'call', class: 'fa-phone-square' }
];

const MODE_DIALOG = {
  ADD: 'add',
  SAVE: 'save'
};

const SHORT_CUTS_MARKDOWN_EDITOR = [
  {
    action: 'Cmd-Alt-T',
    label: 'drawTable'
  },
  {
    action: "Cmd-'",
    label: 'toggleBlockquote'
  },
  {
    action: 'Cmd-B',
    label: 'toggleBold'
  },
  {
    action: 'Cmd-E',
    label: 'cleanBlock'
  },
  {
    action: 'Cmd-H',
    label: 'toggleHeadingSmaller'
  },
  {
    action: 'Cmd-I',
    label: 'toggleItalic'
  },
  {
    action: 'Cmd-K',
    label: 'drawLink'
  },
  {
    action: 'Cmd-L',
    label: 'toggleUnorderedList'
  },
  {
    action: 'Cmd-P',
    label: 'togglePreview'
  },
  {
    action: 'Ctrl-Alt-C',
    label: 'toggleCodeBlock'
  },
  {
    action: 'Ctrl-Alt-I',
    label: 'drawImage'
  },
  {
    action: 'Cmd-Alt-L',
    label: 'toggleOrderedList'
  },
  {
    action: 'Shift-Cmd-H',
    label: 'toggleHeadingBigger'
  },
  {
    action: 'F9',
    label: 'toggleSideBySide'
  },
  {
    action: 'F11',
    label: 'toggleFullScreen'
  }
];

const PROJECT_ACTIVE_STATUS = [
  { label: 'INACTIVE', value: 0 },
  { label: 'ACTIVE', value: 1 },
  { label: 'FINISHED', value: 2 }
];

const CUSTOMER_STATUS = [
  {
    label: 'ACTIVE',
    value: 1
  },
  {
    label: 'INACTIVE',
    value: 2
  }
];

// const CHART_COLOR_AREA = [ '#EF402B', '#0687A1', '#B4CF0A', '#E4B412', '#77264F', '#49AA57', '#289382', '#47B39E', '#F0CB4F', '#F9AF5A', '#DF5A49', '#F9B10F', '#05847E', '#DF9C45', '#DB400C' ];
const CHART_COLOR_AREA = [
  '#8E3C44',
  '#46C8E3',
  '#D3E753',
  '#F9DC7D',
  '#D64D93',
  '#4DD661',
  '#35BAA5',
  '#3DD0B3',
  '#F0DB93',
  '#F2C590',
  '#F98779',
  '#F6C24C',
  '#12C9C1',
  '#EAA750',
  '#F46535'
];

const TYPE_LIST_USER = {
  DEFAULT: 1, // not user admin (user init system)
  ALL: 2
};

const PROJECT_MANAGER_NAME = 'PROJECT MANAGER';

const SKILL_RANK_TYPE = {
  accounting: 'Accounting',
  adminCB: 'Admin C&B',
  brandMarketing: 'Brand Marketing',
  brse: 'BRSE',
  copywriter: 'Copywriter',
  digitalMarketing: 'Digital Marketing',
  hrRecruiter: 'HR Recruiter',
  normal: 'Normal',
  pm: 'PM',
  sales: 'Sales'
};

const SKILL_RANK_TYPES = Object.values(SKILL_RANK_TYPE)
  .map(skill => ({
    value: skill,
    label: skill
  }))
  .sort((a, b) => {
    if (a.label > b.label) {
      return 1;
    }
    if (a.label < b.label) {
      return -1;
    }
    return 0;
  });

const RANK_SKILL_PM = {
  LV1: {
    min: 0,
    max: 25,
    label: 'LV1',
    type: SKILL_RANK_TYPE.pm,
    backgroundColor: '#9933FF'
  },
  LV2: {
    min: 25,
    max: 50,
    label: 'LV2',
    type: SKILL_RANK_TYPE.pm,
    backgroundColor: '#9900CC'
  },
  LV3: {
    min: 50,
    max: 75,
    label: 'LV3',
    type: SKILL_RANK_TYPE.pm,
    backgroundColor: '#660099'
  },
  LV4: {
    min: 75,
    max: 100,
    label: 'LV4',
    type: SKILL_RANK_TYPE.pm,
    backgroundColor: '#660066'
  }
};

const RANK_SKILL_NORMAL = {
  F1: {
    min: 0,
    max: 20,
    label: 'F1',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#333333'
  },
  F2: {
    min: 20,
    max: 30,
    label: 'F2',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#000000'
  },
  J1: {
    min: 30,
    max: 40,
    label: 'J1',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#00cc00'
  },
  J2: {
    min: 40,
    max: 50,
    label: 'J2',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#006600'
  },
  J3: {
    min: 50,
    max: 60,
    label: 'J3',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#0000FF'
  },
  J4: {
    min: 60,
    max: 70,
    label: 'J4',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#000099'
  },
  S1: {
    min: 70,
    max: 80,
    label: 'S1',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#FF6666'
  },
  S2: {
    min: 80,
    max: 90,
    label: 'S2',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#CC0000'
  },
  S3: {
    min: 90,
    max: 100,
    label: 'S3',
    type: SKILL_RANK_TYPE.normal,
    backgroundColor: '#660000'
  }
};

const RANK_SKILL_ADMIN_CB = {
  EXECUTIVE: {
    min: 0,
    max: 20,
    label: 'Executive',
    type: SKILL_RANK_TYPE.adminCB,
    backgroundColor: '#333333'
  },
  EXECUTIVE_JUNIOR: {
    min: 20,
    max: 40,
    label: 'Executive Junior',
    type: SKILL_RANK_TYPE.adminCB,
    backgroundColor: '#000000'
  },
  EXECUTIVE_SENIOR: {
    min: 40,
    max: 60,
    label: 'Executive Senior',
    type: SKILL_RANK_TYPE.adminCB,
    backgroundColor: '#00cc00'
  },
  EXECUTIVE_LEADER: {
    min: 60,
    max: 80,
    label: 'Executive Leader',
    type: SKILL_RANK_TYPE.adminCB,
    backgroundColor: '#006600'
  },
  MANAGER: {
    min: 80,
    max: 100,
    label: 'Manager',
    type: SKILL_RANK_TYPE.adminCB,
    backgroundColor: '#0000FF'
  }
};
const RANK_SKILL_HR_RECRUITER = {
  FRESHER: {
    min: 0,
    max: 20,
    label: 'Fresher',
    type: SKILL_RANK_TYPE.hrRecruiter,
    backgroundColor: '#333333'
  },
  JUNIOR: {
    min: 20,
    max: 40,
    label: 'Junior',
    type: SKILL_RANK_TYPE.hrRecruiter,
    backgroundColor: '#000000'
  },
  SENIOR: {
    min: 40,
    max: 60,
    label: 'Senior',
    type: SKILL_RANK_TYPE.hrRecruiter,
    backgroundColor: '#00cc00'
  },
  LEADER: {
    min: 60,
    max: 80,
    label: 'Leader',
    type: SKILL_RANK_TYPE.hrRecruiter,
    backgroundColor: '#006600'
  },
  MANAGER: {
    min: 80,
    max: 100,
    label: 'Manager',
    type: SKILL_RANK_TYPE.hrRecruiter,
    backgroundColor: '#0000FF'
  }
};

const RANK_SKILL_ACCOUNTING = {
  EXECUTIVE: {
    min: 0,
    max: 33,
    label: 'Accountant Executive',
    type: SKILL_RANK_TYPE.accounting,
    backgroundColor: '#333333'
  },
  GENERALIST: {
    min: 33,
    max: 66,
    label: 'Accountant Generalist',
    type: SKILL_RANK_TYPE.accounting,
    backgroundColor: '#000000'
  },
  CHIEF: {
    min: 66,
    max: 100,
    label: 'Chief Accountant',
    type: SKILL_RANK_TYPE.accounting,
    backgroundColor: '#006600'
  }
};

const RANK_SKILL_COPYWRITER = {
  FRESHER: {
    min: 0,
    max: 20,
    label: 'Fresher',
    type: SKILL_RANK_TYPE.copywriter,
    backgroundColor: '#333333'
  },
  JUNIOR: {
    min: 20,
    max: 40,
    label: 'Junior',
    type: SKILL_RANK_TYPE.copywriter,
    backgroundColor: '#000000'
  },
  SENIOR: {
    min: 40,
    max: 60,
    label: 'Senior',
    type: SKILL_RANK_TYPE.copywriter,
    backgroundColor: '#006600'
  },
  LEADER: {
    min: 60,
    max: 80,
    label: 'Team Leader',
    type: SKILL_RANK_TYPE.copywriter,
    backgroundColor: '#0000FF'
  },
  MANAGER: {
    min: 80,
    max: 100,
    label: 'Manager',
    type: SKILL_RANK_TYPE.copywriter,
    backgroundColor: '#CC0000'
  }
};

const RANK_SKILL_DIGITAL_MARKETING = {
  FRESHER: {
    min: 0,
    max: 25,
    label: 'Fresher',
    type: SKILL_RANK_TYPE.digitalMarketing,
    backgroundColor: '#333333'
  },
  JUNIOR: {
    min: 25,
    max: 50,
    label: 'Junior',
    type: SKILL_RANK_TYPE.digitalMarketing,
    backgroundColor: '#000000'
  },
  SENIOR: {
    min: 50,
    max: 75,
    label: 'Senior',
    type: SKILL_RANK_TYPE.digitalMarketing,
    backgroundColor: '#006600'
  },
  MANAGER: {
    min: 75,
    max: 100,
    label: 'Manager',
    type: SKILL_RANK_TYPE.digitalMarketing,
    backgroundColor: '#0000FF'
  }
};

const RANK_SKILL_BRAND_MARKETING = {
  EXECUTIVE: {
    min: 0,
    max: 33,
    label: 'Executive',
    type: SKILL_RANK_TYPE.brandMarketing,
    backgroundColor: '#333333'
  },
  ASSISTANT: {
    min: 33,
    max: 66,
    label: 'Assistant',
    type: SKILL_RANK_TYPE.brandMarketing,
    backgroundColor: '#000000'
  },
  MANAGER: {
    min: 66,
    max: 100,
    label: 'Manager',
    type: SKILL_RANK_TYPE.brandMarketing,
    backgroundColor: '#006600'
  }
};

const RANK_SKILL_SALES = {
  TRAINEE: {
    min: 0,
    max: 20,
    label: 'Trainee',
    type: SKILL_RANK_TYPE.sales,
    backgroundColor: '#333333'
  },
  EXECUTIVE: {
    min: 20,
    max: 40,
    label: 'Executive',
    type: SKILL_RANK_TYPE.sales,
    backgroundColor: '#000000'
  },
  SUPERVISOR: {
    min: 40,
    max: 60,
    label: 'Supervisor',
    type: SKILL_RANK_TYPE.sales,
    backgroundColor: '#006600'
  },
  MANAGER: {
    min: 60,
    max: 80,
    label: 'Manager',
    type: SKILL_RANK_TYPE.sales,
    backgroundColor: '#0000FF'
  },
  DIRECTOR: {
    min: 80,
    max: 100,
    label: 'Director',
    type: SKILL_RANK_TYPE.sales,
    backgroundColor: '#CC0000'
  }
};

const RANK_SKILL_BRSE = {
  LV1: {
    min: 0,
    max: 33,
    label: 'LV1',
    type: SKILL_RANK_TYPE.brse,
    backgroundColor: '#333333'
  },
  LV2: {
    min: 33,
    max: 66,
    label: 'LV2',
    type: SKILL_RANK_TYPE.brse,
    backgroundColor: '#000000'
  },
  LV3: {
    min: 66,
    max: 100,
    label: 'LV3',
    type: SKILL_RANK_TYPE.brse,
    backgroundColor: '#006600'
  }
};

const PM_NAME_SKILL = ['PM', 'PROJECT MANAGER', 'LEADER'];

const PROJECT_TAB = {
  checklist: {
    value: 1,
    label: 'checklist',
    path: 'checklist'
  },
  userRole: {
    value: 2,
    label: 'users_and_roles',
    path: 'users-roles'
  },
  alertNotifi: {
    value: 3,
    label: 'alert_and_notification',
    path: 'alert-notifi'
  },
  deployment: {
    value: 4,
    label: 'deployment',
    path: 'deployment'
  },
  info: {
    value: 5,
    label: 'info',
    path: 'info'
  }
};

const PROJECT_TABS = [
  PROJECT_TAB.userRole,
  PROJECT_TAB.alertNotifi,
  PROJECT_TAB.checklist,
  PROJECT_TAB.deployment,
  PROJECT_TAB.info
];

const ROUNDED_NUMBER_KPI = 3;

const MAX_FILE_UPLOAD = 3;

const MILISECONDS_IN_DAY = 24 * 60 ** 2 * 1000;

const SPRINT_TAB = {
  SPRINT_BOARD: { value: 1, label: 'sprint_board' },
  BACKLOG: { value: 2, label: 'backlog' },
  SPRINT: { value: 3, label: 'sprint' },
  PROGRESS: { value: 5, label: 'progress' },
  LOG: { value: 7, label: 'log' }
};

const SPRINT_THROUGHOUT_TAB = {
  WIKI: { value: 4, label: 'wiki' },
  ISSUE: { value: 6, label: 'issue' }
};

const BOARD_TAB = {
  MY_TASK: { value: 1, label: 'my_task' },
  ALL: { value: 2, label: 'all' }
};

const ALLOCATION_PLAN_TAB = {
  MANAGEMENT: {
    value: 1,
    label: 'management',
    permissions: PERM_CREATE_ALLOCATION_PLAN
  },
  CHART: {
    value: 2,
    label: 'chart',
    permissions: PERM_LIST_ALLOCATION_PLAN_OF_USERS
  }
};

const TAB_SKILL = {
  ALL_SKILL_BY_USERS: {
    value: 0,
    label: 'all_skill_users'
  },
  USER_SKILL_KPI: {
    value: 1,
    label: 'user_skill_kpi'
  },
  SKILL: {
    value: 2,
    label: 'skill'
  },
  USER_SKILL: {
    value: 3,
    label: 'user_skill'
  },
  SKILL_CHART: {
    value: 4,
    label: 'summary_chart'
  }
};

const TOAST_ERROR_STYLE = {
  className: css({
    color: '#721c24',
    backgroundColor: '#f8d7da',
    borderColor: '#f5c6cb',
    border: '1px solid transparent',
    borderRadius: '0.25rem'
  }),
  progressClassName: css({
    background: '#ffffff'
  })
};

const TOAST_SUCCESS_STYLE = {
  className: css({
    color: '#155724',
    backgroundColor: '#d4edda',
    borderColor: '#f5c6cb',
    border: '1px solid transparent',
    borderRadius: '0.25rem'
  }),
  progressClassName: css({
    background: '#ffffff'
  })
};

const SUBSCRIPTION_METHODS = [{ value: 1, label: 'email' }, { value: 2, label: 'SMS' }, { value: 3, label: 'call' }];

const STATUS_LIGHT = {
  ON: 1,
  OFF: 2
};

const STATUS_FULL_BOARD = {
  ZOOM_IN: 1,
  ZOOM_OUT: 2
};

export {
  COLORS,
  EFFORT_TYPES,
  EFFORT_TYPES_ARRAY,
  PAGINATE_DOCUMENT_LIMIT,
  PAGINATE_ATTACHMENT_LIMIT,
  PRIORITY_OPTIONS,
  PAGINATE_NOTIFICATION_LIMIT,
  PRIORITY_OBJECT,
  PAGINATE_COMMENT_LIMIT,
  PAGINATE_COMMENT_ISSUE_LIMIT,
  TYPE_GET_TASK,
  BORDER_COLOR_DEFAULT,
  TASK_TYPE_OPTIONS,
  TASK_TYPE_OBJECT,
  ISSUE_OPTION,
  ISSUE_OPTIONS,
  COMPARISON_ESTIMATE_OPTIONS,
  SKILL_TYPES,
  KPI_TYPE,
  KPI_TYPE_OPTIONS,
  KPI_TYPE_TIME,
  KPI_TYPE_TIME_OPTIONS,
  ROWS_PER_PAGE_TASK_OPTIONS,
  ROWS_PER_PAGE_OPTIONS,
  ROWS_PER_PAGE_SKILL_OPTIONS,
  ARRAY_TYPE_APPLY,
  ARRAY_TYPE_ACTION,
  ARRAY_IS_APPLY,
  TYPE_APPLY_BEHAVIOR_OBJECT,
  TYPE_ACTION_BEHAVIOR_OBJECT,
  EVALUATE_KPI_SKILL,
  ISSUE_STATUS,
  POINT_KPI_LEADER,
  TYPES_KPI_SPRINT,
  TYPE_LOG_BEHAVIOR,
  NOT_CLOSED_STATUS_TASK,
  USERS_ALL_TABS_TYPE,
  SUBSCRIBE_METHODS,
  MODE_DIALOG,
  SHORT_CUTS_MARKDOWN_EDITOR,
  PROJECT_ACTIVE_STATUS,
  TYPE_KPI_COEFFICIENT,
  CHART_COLOR_AREA,
  TYPE_LIST_USER,
  PROJECT_MANAGER_NAME,
  RANK_SKILL_NORMAL,
  RANK_SKILL_PM,
  PM_NAME_SKILL,
  SKILL_RANK_TYPE,
  PROJECT_TAB,
  PROJECT_TABS,
  ROUNDED_NUMBER_KPI,
  TYPE_SUB_APPLY,
  SKILL_RANK_TYPES,
  RANK_SKILL_ADMIN_CB,
  RANK_SKILL_HR_RECRUITER,
  RANK_SKILL_ACCOUNTING,
  RANK_SKILL_COPYWRITER,
  RANK_SKILL_DIGITAL_MARKETING,
  RANK_SKILL_BRAND_MARKETING,
  RANK_SKILL_SALES,
  RANK_SKILL_BRSE,
  CUSTOMER_STATUS,
  MAX_FILE_UPLOAD,
  MILISECONDS_IN_DAY,
  SPRINT_TAB,
  SPRINT_THROUGHOUT_TAB,
  BOARD_TAB,
  ALLOCATION_PLAN_TAB,
  TAB_SKILL,
  TOAST_ERROR_STYLE,
  TOAST_SUCCESS_STYLE,
  SUBSCRIPTION_METHODS,
  STATUS_LIGHT,
  STATUS_FULL_BOARD
};
