// import queryString from 'querystring';
import 'isomorphic-fetch';
import moment from 'moment';
import { getTokenBearer, reloadAccessToken } from '../08-helpers/common';
import { isTokenExpired } from '../08-helpers/jwt';
import { USER_ACCESS_TOKEN, USER_LOGIN_TOKEN, USER_INFO } from '../07-constants/common';
import { setKey } from '../08-helpers/storage';
// import openSocket from 'socket.io-client';
// import { getTokenBearer } from '../08-helpers/common';
import { getDateAt000000ZeroGMT, parseJSONHasValue } from '../08-helpers/common';

// export const parseUrlQuery = (query) => {
//   if (query.indexOf('?') >= 0) query = query.substr(query.indexOf('?') + 1);
//   return queryString.parse(query);
// };
import { removeKey } from '../08-helpers/storage';

export const setLoop = (fn, timeout, failureContinue) => {
  fn()
    .then(() => {
      setTimeout(setLoop, timeout, fn, timeout, failureContinue);
    })
    .catch(() => {
      if (failureContinue) {
        setTimeout(setLoop, timeout, fn, timeout, failureContinue);
      }
    });
};

export const defaultFormDataConfig = (body, withAuthentication = true) => {
  let headers = {};
  if (withAuthentication) {
    headers = Object.assign({}, headers, { Authorization: withAuthentication });
  }
  return {
    method: 'POST',
    body,
    headers
  };
};

export const cleanUndefinedProperties = obj => {
  let newObject = {};
  Object.keys(obj).forEach(key => {
    if (obj[key] !== undefined) {
      newObject[key] = obj[key];
    }
  });
  return newObject;
};

export const defaultPostConfig = (body, withAuthentication = true) => {
  let headers = {
    'Content-Type': 'application/json'
  };
  if (withAuthentication) {
    headers = Object.assign({}, headers, { Authorization: withAuthentication });
  }
  return {
    method: 'POST',
    body: JSON.stringify(body),
    headers
  };
};

export const checkAccessToken = () => {
  //-- WARNING: as this always call project api, we inject token as Authorization header here
  return new Promise(resolve => {
    const accessToken = parseJSONHasValue(localStorage.getItem(USER_ACCESS_TOKEN), null);
    if (!accessToken || (accessToken && isTokenExpired(accessToken))) {
      const loginToken = parseJSONHasValue(localStorage.getItem(USER_LOGIN_TOKEN), null);
      if (!loginToken || (loginToken && isTokenExpired(loginToken))) {
        resolve({ success: false, error: { code: 'TOKEN_EXPIRED' } });
      } else {
        reloadAccessToken(loginToken).then(result => {
          if (result.success) {
            setKey(USER_ACCESS_TOKEN, JSON.stringify(result.result));
          }
          resolve(result);
        });
      }
    } else {
      resolve({ success: false, error: { code: 'TOKEN_NOT_EXPIRED' } });
    }
  });
};

export const callApi = (url, config, onRequestFinish, onRequestSuccess, onRequestFailure, onTokenFailure) => {
  const doCall = (newUrl, newConfig) => {
    fetch(newUrl, newConfig)
      .then(response => {
        onRequestFinish && onRequestFinish();
        if (config.isFileDownload) {
          onRequestSuccess && onRequestSuccess(response);
          return null;
        } else {
          return response.json();
        }
      })
      .then(data => {
        if (data && !config.isFileDownload) {
          data.success && onRequestSuccess && onRequestSuccess(data.result);
          !data.success && onRequestFailure && onRequestFailure(data.error);
        }
      })
      .catch(err => {
        console.log(err);
        onRequestFinish && onRequestFinish();
        onRequestFailure && onRequestFailure({ code: 'NETWORK_ERROR' });
      });
  };

  // doCall(url, config);
  if (config.headers['Authorization']) {
    checkAccessToken().then(result => {
      if (result.success || (result.error && result.error.code === 'TOKEN_NOT_EXPIRED')) {
        if (config.headers['Authorization']) {
          config = Object.assign({}, config, {
            headers: { ...config.headers, Authorization: getTokenBearer() }
          });
        }
        doCall(url, config);
      } else {
        onTokenFailure && onTokenFailure();
      }
    });
  } else {
    doCall(url, config);
  }
};

export const workloadTasks = tasks => {
  let total = 0;
  if (!tasks || !tasks.hasOwnProperty('length')) return total;
  tasks.map(task => (total += task.estimate));
  return total;
};

export const workloadAllocation = (startDate, endDate, effortPercent) => {
  if (
    !startDate ||
    !moment.isMoment(moment(startDate)) ||
    !endDate ||
    !moment.isMoment(moment(endDate)) ||
    !effortPercent
  )
    return 0;
  let totalDay = 0;
  let start = moment(startDate).utc();
  const end = moment(endDate).utc();
  while (start <= end) {
    const dayOfWeek = start.weekday(); // ISO
    if (dayOfWeek >= 1 && dayOfWeek <= 5) {
      totalDay = totalDay + 1;
    }
    start = start.add(1, 'days');
  }
  return (8 * totalDay * effortPercent) / 100;
};

export const workloadAllocations = allocations => {
  let total = 0;
  if (!allocations || !allocations.hasOwnProperty('length')) return total;
  allocations.map(
    allocation => (total += workloadAllocation(allocation.startDate, allocation.endDate, allocation.effortPercent))
  );
  return total;
};

export const processDataAllocation = (arr, startDate, endDate) => {
  const today = new Date();
  // months from 1-12
  const month = today.getMonth() + 1;
  const day = today.getDate();
  const now = day + '/' + month;
  let minDate = new Date();
  let maxDate = new Date();

  if (startDate && endDate) {
    minDate = startDate;
    maxDate = endDate;
  } else {
    minDate.setDate(minDate.getDate() - 30);
    maxDate.setDate(maxDate.getDate() + 30);
  }

  const projects = ['Date', { role: 'annotation' }, 'Weekend'];
  const result = [];

  arr.forEach(allo => {
    if (projects.includes(allo.project) === false) projects.push(allo.project);
  });
  minDate = new Date(minDate);
  maxDate = new Date(maxDate);
  result.push(projects);
  // get allocation every day
  for (let d = minDate; d <= maxDate; d.setDate(d.getDate() + 1)) {
    // months from 1-12
    const month = d.getMonth() + 1;
    const day = d.getDate();
    const newdate = day + '/' + month;
    const dataOneDay = [newdate];

    for (let i = 1; i < projects.length; i++) {
      dataOneDay[i] = 0;
    }
    dataOneDay[1] = now === newdate ? 'Today' : null;
    if (d.getDay() === 0 || d.getDay() === 6) dataOneDay[2] = 100;
    arr.forEach(allocation => {
      const startDateData = new Date(allocation.startDate);
      const endDateData = new Date(allocation.endDate);
      const day = getDateAt000000ZeroGMT(d);
      if (startDateData <= day && day <= endDateData && d.getDay() !== 0 && d.getDay() !== 6) {
        const indexOfProject = projects.indexOf(allocation.project);
        dataOneDay[indexOfProject] += allocation.effortPercent;
      }
    });
    result.push(dataOneDay);
  }
  return result;
};

export const clearAllCache = () => {
  removeKey(USER_LOGIN_TOKEN);
  removeKey(USER_ACCESS_TOKEN);
  removeKey(USER_INFO);
  // clear cache
  removeKey('sprintsBoardCaches');
  removeKey('sprintsIssueCaches');
  removeKey('sprintsProjectCache');
  removeKey('sprintsProjectCaches');
  removeKey('sprintsUrlCache');
};

// const socket = ({ url, config }) => {
//   const option = config ? config : {};
//   return new Promise((resolve, reject) => {
//     const socket = openSocket(url, {
//       ...option
//     });
//
//     socket.on('error', function (error) {
//       reject(error);
//     });
//
//     return resolve(socket);
//   });
// };

// export const connectSocket = (url,
//                        config,
//                        onRequestFinish,
//                        onRequestSuccess,
//                        onRequestFailure) => {
//
//   return socket({ url, config })
//     .then((data) => {
//       onRequestSuccess && onRequestSuccess(data);
//     })
//     .catch((err) => {
//       onRequestFinish && onRequestFinish();
//       onRequestFailure && onRequestFailure({ code: 'SOCKET_ERROR' });
//     });
// };

// export {
//   callApi,
//   parseUrlQuery,
//   setLoop,
//   workloadTasks,
//   workloadAllocation,
//   workloadAllocations,
//   connectSocket
// };
