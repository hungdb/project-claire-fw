import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import auth from './02-reducers/auth';
import admins from './02-reducers/adminproject';
import group from './02-reducers/group';
import permission from './02-reducers/permission';
import project from './02-reducers/project';
import workflow from './02-reducers/workflow';
import common from './02-reducers/common';
// import rolePermissionProject from './02-reducers/rolePermissionProject';
import sprint from './02-reducers/sprint';
import permissionScheme from './02-reducers/permission-scheme';
import userInProject from './02-reducers/userInProject';
import checkList from './02-reducers/check-list';
import allocation from './02-reducers/allocation';
import task from './02-reducers/task';
import projectTaskView from './02-reducers/project-task-view';
import wiki from './02-reducers/wiki';
import notification from './02-reducers/notification';
import comment from './02-reducers/comment';
import skill from './02-reducers/skill';
import sprintProgress from './02-reducers/sprint-progress';
import setting from './02-reducers/setting';
import userNote from './02-reducers/user-note';
import issue from './02-reducers/issue';
import adminAllocations from './02-reducers/admin-allocations';
import behaviour from './02-reducers/behaviours';
import kpi from './02-reducers/kpi';
import kpiLeader from './02-reducers/kpiLeader';
import kpiCoefficient from './02-reducers/kpi-cofficient';
import allocationLabel from './02-reducers/allocation-label';
import sale from './02-reducers/sale';
import allocationPlan from './02-reducers/allocation-plan';
import market from './02-reducers/market';
import customer from './02-reducers/customer';
import alertSystem from './02-reducers/alert-system';

export default history =>
  combineReducers({
    router: connectRouter(history),
    auth,
    admins,
    group,
    permission,
    project,
    workflow,
    permissionScheme,
    common,
    // rolePermissionProject,
    sprint,
    userInProject,
    checkList,
    allocation,
    task,
    projectTaskView,
    wiki,
    notification,
    comment,
    skill,
    behaviour,
    kpiLeader,
    kpiCoefficient,
    sprintProgress,
    setting,
    userNote,
    issue,
    adminAllocations,
    kpi,
    allocationLabel,
    sale,
    allocationPlan,
    market,
    customer,
    alertSystem
  });
