import './style.css';
import imgAvt from './../../06-assets/default_avatar.png';
import React, { useState, memo } from 'react';
import { object, oneOf, oneOfType, string, number, func } from 'prop-types';
import { linkImage } from './../../08-helpers/common';
import { Tooltip } from 'reactstrap';
import { Link } from 'react-router-dom';

const UserAvatar = ({ user, placement, id, onClick, src, className }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggle = () => setTooltipOpen(!tooltipOpen);

  return (
    <div>
      {src ? (
        <Link to={src}>
          <img
            className={`user-avatar cursor-pointer ${className}`}
            alt={user && user.name}
            src={user && user.avatar ? linkImage(user.avatar) : imgAvt}
            id={`Tooltip-${id}`}
            onClick={onClick}
          />
          <Tooltip placement={placement} target={`Tooltip-${id}`} isOpen={tooltipOpen} toggle={toggle}>
            {user && user.name}
          </Tooltip>
        </Link>
      ) : (
        <>
          <img
            className={'user-avatar cursor-pointer'}
            alt={user && user.name}
            src={user && user.avatar ? linkImage(user.avatar) : imgAvt}
            id={`Tooltip-${id}`}
            onClick={onClick}
          />
          <Tooltip placement={placement} target={`Tooltip-${id}`} isOpen={tooltipOpen} toggle={toggle}>
            {user && user.name}
          </Tooltip>
        </>
      )}
    </div>
  );
};

UserAvatar.propTypes = {
  user: object,
  placement: oneOf([
    'auto',
    'auto-start',
    'auto-end',
    'top',
    'top-start',
    'top-end',
    'right',
    'right-start',
    'right-end',
    'bottom',
    'bottom-start',
    'bottom-end',
    'left',
    'left-start',
    'left-end'
  ]),
  id: oneOfType([string, number]),
  onClick: func,
  src: string,
  className: string
};

UserAvatar.defaultProps = {
  placement: 'top',
  id: 'example'
};

export default memo(UserAvatar);
