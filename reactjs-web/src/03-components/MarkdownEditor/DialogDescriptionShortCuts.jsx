import TableHead from '../../03-components/TableHead';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func, oneOf, oneOfType, string } from 'prop-types';
import { SHORT_CUTS_MARKDOWN_EDITOR } from '../../05-utils/commonData';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table } from 'reactstrap';

const DialogDescriptionShortCuts = ({ isOpen, closeDialogShortCuts, dialogWidth, backdrop }) => {
  const fields = ['description_shortcuts.label', 'description_shortcuts.action'];
  const classes = ['align-middle text-center', 'align-middle text-center'];

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal size={dialogWidth} centered={true} isOpen={isOpen} toggle={closeDialogShortCuts} backdrop={backdrop}>
          <ModalHeader toggle={closeDialogShortCuts}> {t('description_shortcuts.title')} </ModalHeader>
          <ModalBody>
            <Table responsive hover bordered striped>
              <thead>
                <tr>
                  <TableHead fields={fields} t={t} classes={classes} />
                </tr>
              </thead>
              <tbody>
                {SHORT_CUTS_MARKDOWN_EDITOR.map((item, index) => (
                  <tr key={index}>
                    <td className={'align-middle text-center'}> {item.label} </td>
                    <td className={'align-middle text-center'}> {item.action} </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter className="d-flex justify-content-flex-end margin-right-30px">
            <Button onClick={closeDialogShortCuts} className={'outline-base-btn'} size={'sm'}>
              {t('OK')}
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

DialogDescriptionShortCuts.propTypes = {
  isOpen: bool,
  closeDialogShortCuts: func,
  dialogWidth: string,
  backdrop: oneOfType([bool, oneOf(['static'])])
};

DialogDescriptionShortCuts.defaultProps = {
  dialogWidth: 'xl',
  backdrop: 'static'
};

export default memo(DialogDescriptionShortCuts);
