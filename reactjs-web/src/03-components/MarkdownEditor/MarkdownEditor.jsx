import 'simplemde/dist/simplemde.min.css';
import SimpleMDE from 'react-simplemde-editor';
import DialogDescriptionShortCuts from './DialogDescriptionShortCuts';
import React, { Fragment, memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { string, bool, func } from 'prop-types';

const toolBar = [
  'bold',
  'italic',
  'strikethrough',
  '|',
  'heading',
  'heading-smaller',
  'heading-bigger',
  '|',
  'heading-1',
  'heading-2',
  'heading-3',
  '|',
  'code',
  'quote',
  'unordered-list',
  'ordered-list',
  '|',
  'clean-block',
  'link',
  'image',
  'table',
  'horizontal-rule',
  '|',
  'preview',
  'side-by-side',
  'fullscreen',
  '|',
  'guide'
];

const shortCuts = {
  drawTable: 'Cmd-Alt-T', // bind Cmd-Alt-T to drawTable action, which doesn't come with a default shortcut,
  toggleBlockquote: "Cmd-'",
  toggleBold: 'Cmd-B',
  cleanBlock: 'Cmd-E',
  toggleHeadingSmaller: 'Cmd-H',
  toggleItalic: 'Cmd-I',
  drawLink: 'Cmd-K',
  toggleUnorderedList: 'Cmd-L',
  togglePreview: 'Cmd-P',
  toggleCodeBlock: 'Ctrl-Alt-C',
  drawImage: 'Cmd-Alt-I',
  toggleOrderedList: 'Cmd-Alt-L',
  toggleHeadingBigger: 'Shift-Cmd-H',
  toggleSideBySide: 'F9',
  toggleFullScreen: 'F11'
};

const MarkdownEditor = ({ label, value, placeholder, isShowToolbar, className, onChange }) => {
  const [isOpenDialogShortcuts, setIsOpenDialogShortcuts] = useState(false);
  const toggleDialogInfoShortcuts = flag => setIsOpenDialogShortcuts(flag);
  const onChangeContent = content => onChange && onChange(content);

  return (
    <NamespacesConsumer>
      {t => (
        <Fragment>
          {isOpenDialogShortcuts && (
            <DialogDescriptionShortCuts
              isOpen={isOpenDialogShortcuts}
              closeDialogShortCuts={() => toggleDialogInfoShortcuts(false)}
            />
          )}
          <SimpleMDE
            className={className}
            label={label}
            value={value}
            onChange={value => onChangeContent(value)}
            options={{
              autoDownloadFontAwesome: true,
              autofocus: false,
              blockStyles: {
                bold: '**',
                code: '```',
                italic: '_'
              },
              lineWrapping: true,
              placeholder: placeholder || '',
              spellChecker: true,
              shortcuts: shortCuts,
              status: ['autosave', 'lines', 'words', 'cursor'],
              tabSize: 2,
              toolbar: isShowToolbar
                ? [
                    ...toolBar,
                    {
                      name: 'guid short cut',
                      action: () => toggleDialogInfoShortcuts(true),
                      className: 'fa fa-keyboard-o',
                      title: t('description_shortcuts.guide_short_cuts')
                    }
                  ]
                : false,
              toolbarTips: true
            }}
          />
        </Fragment>
      )}
    </NamespacesConsumer>
  );
};

MarkdownEditor.propTypes = {
  label: string,
  value: string,
  className: string,
  placeholder: string,
  isShowToolbar: bool,
  onChange: func.isRequired
};

MarkdownEditor.defaultProps = {
  isShowToolbar: true
};

export default memo(MarkdownEditor);
