import './style.css';
import React, { memo } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { bool, func, string, element, object, oneOfType, oneOf } from 'prop-types';

const BaseDialog = ({
  isOpen,
  onClose,
  title,
  dialogContent,
  dialogWidth,
  actionBtn,
  labelBtnCancel,
  classes,
  actionBtnLeft,
  backdrop
}) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Modal
          size={dialogWidth}
          centered={true}
          isOpen={isOpen}
          className={classes}
          toggle={() => onClose && onClose()}
          backdrop={backdrop}
        >
          <div>
            <ModalHeader toggle={() => onClose && onClose()}>{title}</ModalHeader>
            <ModalBody>{dialogContent}</ModalBody>
            <ModalFooter>
              <div className={'flex-grow-1 d-flex justify-content-between'}>
                <div>{actionBtnLeft}</div>
                <div>
                  <Button
                    className={'margin-right-10px outline-base-btn'}
                    size={'sm'}
                    onClick={() => {
                      onClose && onClose();
                    }}
                  >
                    {labelBtnCancel || t('cancel')}
                  </Button>
                  {actionBtn}
                </div>
              </div>
            </ModalFooter>
          </div>
        </Modal>
      )}
    </NamespacesConsumer>
  );
};

BaseDialog.propTypes = {
  isOpen: bool,
  fullWidth: bool,
  onClose: func,
  title: string,
  dialogContent: element,
  actionBtn: element,
  actionBtnLeft: element,
  dialogWidth: string,
  labelBtnCancel: string,
  classes: string,
  styleContent: object,
  styleTitle: object,
  backdrop: oneOfType([bool, oneOf(['static'])])
};

BaseDialog.defaultProps = {
  backdrop: 'static'
};

export default memo(BaseDialog);
