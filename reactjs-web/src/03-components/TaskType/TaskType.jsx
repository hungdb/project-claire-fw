import './style.css';
import cx from 'classnames';
import React, { memo } from 'react';
import { number } from 'prop-types';
import { Badge } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';

const TaskType = ({ type }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <Badge
        className={'badge-task-type'}
        color={cx({
          primary: type === 1,
          danger: type === 2
        })}
      >
        {t(
          cx({
            task: type === 1,
            bug: type === 2
          })
        )}
      </Badge>
    )}
  </NamespacesConsumer>
);

TaskType.propTypes = {
  type: number
};

TaskType.defaultProps = {
  type: 1
};

export default memo(TaskType);
