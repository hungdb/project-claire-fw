import './style.css';
import Chart from 'react-google-charts';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { array, number } from 'prop-types';
import { CHART_COLOR_AREA } from '../../05-utils/commonData';

const EffortChart = ({ data, height, width }) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Chart
            width={width}
            className={'allocation-user-chart'}
            chartType="ColumnChart"
            loader={<div className={'margin-top-15px text-center font-size-15px text-muted'}>{t('loading_chart')}</div>}
            data={data}
            options={{
              isStacked: true,
              height: height,
              legend: { position: 'top', maxLines: 3 },
              vAxis: {
                viewWindow: { min: 0, max: 100 },
                backgroundColor: '#fff',
                minValue: 0
              },
              colors: CHART_COLOR_AREA,
              annotation: {
                1: {
                  style: 'line'
                }
              },
              bar: {
                groupWidth: '100%'
              }
            }}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

EffortChart.propTypes = {
  data: array,
  height: number,
  width: number
};

EffortChart.defaultProps = {
  data: []
};

export default memo(EffortChart);
