import './Button.css';
import cx from 'classnames';
import React, { memo } from 'react';
import { string, func, oneOfType, node, element, oneOf, bool } from 'prop-types';
import { Button } from 'reactstrap';

const CustomButton = ({ className, onClick, children, type, size, disabled, active, title }) => (
  <Button
    className={`${className} ${cx({
      'orange-btn': type === 'orange',
      'default-btn': type === 'gray'
    })}`}
    onClick={onClick}
    size={size}
    disabled={disabled}
    active={active}
    title={title}
  >
    {children}
  </Button>
);

CustomButton.propTypes = {
  className: string,
  onClick: func,
  children: oneOfType([string, node, element]),
  size: string,
  type: oneOf(['orange', 'gray', 'normal']),
  disabled: bool,
  active: bool,
  title: string
};

CustomButton.defaultProps = {
  size: 'md',
  type: 'normal',
  disabled: false,
  title: ''
};

export default memo(CustomButton);
