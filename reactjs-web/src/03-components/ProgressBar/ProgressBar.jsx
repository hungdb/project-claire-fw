import './style.css';
import cx from 'classnames';
import BadgeColor from '../BadgeColor';
import React, { memo } from 'react';
import { string, number, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Progress } from 'reactstrap';
import { getRankOfSkill } from '../../08-helpers/common';

const ProgressBar = ({ name, value, textHident, displayRank }) => {
  const rank = displayRank && getRankOfSkill(value, name);

  return (
    <NamespacesConsumer ns="translations">
      {() => (
        <div className={`${textHident ? 'text-hident' : ''} progressBar row mx-0`}>
          <h6 className="fieldName col-auto px-0">{name}</h6>
          <Progress
            value={value}
            color={cx({
              'skill-fresher': value <= 30,
              'skill-junior': value <= 80,
              'skill-senior': value <= 100
            })}
            className="customBar col px-0"
          >
            {value}
          </Progress>
          {rank && (
            <h6 className="rankName col-auto px-0">
              <BadgeColor backgroundColor={rank.backgroundColor}>{rank.label}</BadgeColor>
            </h6>
          )}
        </div>
      )}
    </NamespacesConsumer>
  );
};

ProgressBar.propTypes = {
  name: string,
  value: number,
  textHident: bool,
  displayRank: bool
};

ProgressBar.defaultProps = {
  name: '',
  value: 0,
  displayRank: true
};

export default memo(ProgressBar);
