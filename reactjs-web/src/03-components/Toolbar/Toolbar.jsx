import withStyles from 'react-jss';
import styles from './styles';
import i18n from '../../i18n';
import Notification from '../Notification';
import logo from '../../06-assets/images/logo-2.png';
import avatarDefault from '../../06-assets/images/avatar-1.png';
import Light from '../Light';
import React, { memo, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { NamespacesConsumer } from 'react-i18next';
import { matchPath, Link } from 'react-router-dom';
import { getEnv } from '../../env';
import { logout as _logout } from '../../01-actions/auth';
import { CHECKIN, IDENTITY, ASSET } from '../../07-constants/localLinks';
import { isAllowPermission } from '../../08-helpers/common';
import { func, object, array } from 'prop-types';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import {
  PERM_FILTER_ALLOCATION,
  PERM_GET_LIST_KPI_SPRINT,
  PERM_GET_LIST_KPI_TO_CHART,
  PERM_GET_MY_LIST_KPI_TO_CHART,
  PERM_GET_SKILLS_OF_ALL_USERS,
  PERM_GET_STATE_OF_ALERT_SYSTEM,
  PERM_GET_USERS_OF_ALL_PROJECT,
  PERM_LIST_ALL_PERMISSIONS,
  PERM_LIST_ALLOCATION_OF_USER,
  PERM_LIST_ALLOCATION_OF_USERS,
  // PERM_LIST_BEHAVIOURS,
  PERM_LIST_GROUPS,
  PERM_LIST_MY_CURRENT_TASKS,
  PERM_LIST_MY_GROUPS,
  PERM_LIST_SKILLS_BY_USER_WITH_LATEST_EVALUATION,
  PERM_LIST_TASK_OF_ALL_USERS,
  PERM_LIST_WORKFLOW,
  PERM_STATISTICAL_SPRINT_ACTIVE_BY_PROJECT,
  PERM_CREATE_MULTIPLE_KPI_BEHAVIOR,
  PERM_GET_LIST_KPI_BEHAVIOR,
  PERM_CREATE_KPI_BEHAVIOR,
  PERM_REMOVE_KPI_BEHAVIOR,
  PERM_CREATE_BEHAVIOR,
  PERM_UPDATE_BEHAVIOR,
  PERM_REMOVE_BEHAVIOR,
  PERM_LIST_SALE_CATEGORY,
  PERM_LIST_ALLOCATION_PLAN_OF_USERS,
  PERM_CREATE_SALE_CATEGORY,
  PERM_LIST_ALLOCATION_PLANS,
  PERM_GET_LIST_MARKET,
  PERM_CREATE_MARKET,
  PERM_GET_LIST_ALERT_SUBSCRIPTION
} from '../../07-constants/app-permission';

const mainRoutes = [
  {
    permission: [PERM_LIST_MY_CURRENT_TASKS],
    i18n_name: 'tasks',
    path: '/tasks'
  },
  {
    permission: [PERM_STATISTICAL_SPRINT_ACTIVE_BY_PROJECT],
    i18n_name: 'sprints',
    path: '/sprints'
  },
  {
    permission: [PERM_FILTER_ALLOCATION],
    i18n_name: 'allocations',
    path: '/allocations'
  },
  {
    permission: [PERM_LIST_GROUPS, PERM_LIST_MY_GROUPS],
    i18n_name: 'projects',
    path: '/projects'
  },
  {
    permission: [
      PERM_LIST_ALLOCATION_OF_USER,
      PERM_LIST_SKILLS_BY_USER_WITH_LATEST_EVALUATION,
      PERM_GET_MY_LIST_KPI_TO_CHART
    ],
    i18n_name: 'profile',
    path: '/profile'
  }
];
const adminRoutes = [
  {
    permission: [PERM_LIST_ALL_PERMISSIONS],
    i18n_name: 'admin_panel_child.user_role',
    path: '/users'
  },
  {
    permission: [PERM_LIST_TASK_OF_ALL_USERS],
    i18n_name: 'admin_panel_child.user_task_summary',
    path: '/user-tasks'
  },
  {
    permission: [PERM_LIST_ALLOCATION_OF_USERS],
    i18n_name: 'admin_panel_child.user_admin_summary',
    path: '/admin-allocations'
  },
  {
    permission: [PERM_LIST_ALLOCATION_PLAN_OF_USERS, PERM_LIST_ALLOCATION_PLANS],
    i18n_name: 'admin_panel_child.allocation_plan_summary',
    path: '/admin-allocation-plans'
  },
  {
    permission: [PERM_GET_SKILLS_OF_ALL_USERS],
    i18n_name: 'admin_panel_child.user_skill',
    path: '/skills'
  },
  {
    permission: [
      // PERM_LIST_BEHAVIOURS,
      PERM_CREATE_MULTIPLE_KPI_BEHAVIOR,
      PERM_GET_LIST_KPI_BEHAVIOR,
      PERM_CREATE_KPI_BEHAVIOR,
      PERM_REMOVE_KPI_BEHAVIOR,
      PERM_CREATE_BEHAVIOR,
      PERM_UPDATE_BEHAVIOR,
      PERM_REMOVE_BEHAVIOR
    ],
    i18n_name: 'admin_panel_child.user_behaviour',
    path: '/behaviours'
  },
  {
    permission: [PERM_GET_LIST_KPI_TO_CHART],
    i18n_name: 'admin_panel_child.user_KPI',
    path: '/kpi'
  },
  {
    permission: [PERM_GET_USERS_OF_ALL_PROJECT],
    i18n_name: 'admin_panel_child.projects',
    path: '/admin-projects'
  },
  {
    permission: [PERM_GET_LIST_KPI_SPRINT],
    i18n_name: 'admin_panel_child.kpi_leader',
    path: '/kpi-leader'
  },
  {
    permission: [PERM_LIST_WORKFLOW],
    i18n_name: 'admin_panel_child.work_flow',
    path: '/workflows'
  },
  {
    permission: [PERM_LIST_SALE_CATEGORY, PERM_CREATE_SALE_CATEGORY],
    i18n_name: 'admin_panel_child.sale',
    path: '/sales'
  },
  {
    permission: [PERM_GET_LIST_MARKET, PERM_CREATE_MARKET],
    i18n_name: 'admin_panel_child.customer',
    path: '/customer'
  },
  {
    permission: [PERM_GET_LIST_ALERT_SUBSCRIPTION],
    i18n_name: 'admin_panel_child.alert_system',
    path: '/alert-system'
  },
  {
    permission: [PERM_GET_STATE_OF_ALERT_SYSTEM],
    i18n_name: 'admin_panel_child.setting',
    path: '/settings'
  }
];

const renderLocalUserLink = ({ t, classes, permissions }) => {
  return [CHECKIN, IDENTITY, ASSET].map((item, index) => (
    <>
      {isAllowPermission(permissions, item.permission) && (
        <DropdownItem
          key={index}
          tag="a"
          href={item.link}
          target="_blank"
          className={classes.dropdownMenuLink}
          style={{
            // background: item.color,
            color: item.color
          }}
        >
          {t(item.title)}
        </DropdownItem>
      )}
    </>
  ));
};

const getMainMenu = (mainRoutes, permissions, classes, sprintsUrlCache, pathname) =>
  mainRoutes
    .filter(r => {
      return r.permission.some(permissionName => isAllowPermission(permissions, permissionName));
    })
    .map((r, i) => (
      <NavItem key={i}>
        <Link
          className={
            'd-block px-4 py-2 font-weight-400 ' +
            classes.mainMenuLink +
            (matchPath(pathname, r.path) ? ' ' + classes.activedMainMenuLink : '')
          }
          to={sprintsUrlCache && r.path === '/sprints' ? sprintsUrlCache : r.path}
        >
          {i18n.t(r.i18n_name)}
        </Link>
      </NavItem>
    ));

const getAdminMenu = (adminRoutes, permissions, classes) =>
  adminRoutes
    .filter(r => {
      return r.permission.some(permissionName => isAllowPermission(permissions, permissionName));
    })
    .map((r, i) => (
      <DropdownItem key={i} className={'px-4 py-2 ' + classes.dropdownMenuLink} tag={Link} to={r.path}>
        {i18n.t(r.i18n_name)}
      </DropdownItem>
    ));

const Toolbar = ({ classes, location: { pathname }, currentUser, _logout, permissions }) => {
  const [isOpenAdmin, setIsOpenAdmin] = useState(false);
  const sprintsUrlCache = localStorage.getItem('sprintsUrlCache');
  const _renderMainMenu = getMainMenu(mainRoutes, permissions, classes, sprintsUrlCache, pathname);
  const _renderAdminMenu = getAdminMenu(adminRoutes, permissions, classes);

  const handleOpenAdmin = () => setIsOpenAdmin(!isOpenAdmin);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Navbar dark expand="md" className={classes.navbar}>
          <img className={classes.logo} src={logo} alt="logo" />
          <NavbarToggler onClick={handleOpenAdmin} />
          <Collapse isOpen={isOpenAdmin} navbar>
            <Nav className="mr-auto" navbar>
              {_renderMainMenu}
            </Nav>
            <Light />
            <Nav navbar>
              {_renderAdminMenu && !!_renderAdminMenu.length && (
                <UncontrolledDropdown nav inNavbar className="px-4">
                  <DropdownToggle
                    nav
                    className={
                      classes.mainMenuLink +
                      'font-weight-400 ' +
                      (adminRoutes.find(r => matchPath(pathname, r.path)) ? ' ' + classes.activedMainMenuLink : '')
                    }
                  >
                    <i className={'fa fa-cogs ' + classes.iconAdminPanel} />
                  </DropdownToggle>
                  <DropdownMenu right className={classes.dropAdmin}>
                    {_renderAdminMenu}
                  </DropdownMenu>
                </UncontrolledDropdown>
              )}
            </Nav>
            <Notification />
            <UncontrolledDropdown>
              <DropdownToggle nav className={'px-0'}>
                <img
                  className={classes.avatar}
                  alt="avatar"
                  src={
                    !!currentUser && currentUser.profilePhoto
                      ? `${getEnv('S3_BUCKET')}/${currentUser.profilePhoto}`
                      : avatarDefault
                  }
                />
              </DropdownToggle>
              <DropdownMenu right className={`${classes.dropdownUser}`}>
                {renderLocalUserLink({ t, classes, permissions })}
                <DropdownItem className={classes.dropdownMenuLink + ' cursor-pointer'} onClick={_logout}>
                  {t('logout')} ({currentUser && currentUser.username})
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Collapse>
        </Navbar>
      )}
    </NamespacesConsumer>
  );
};

Toolbar.propTypes = {
  classes: object,
  location: object,
  currentUser: object,
  _logout: func,
  permissions: array
};

Toolbar.defaultProps = {};

const mapStateToProps = state => ({
  permissions: state.auth.permissions,
  currentUser: state.auth.user
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _logout
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(styles)(memo(Toolbar))));
