const styles = {
  navbar: {
    paddingTop: 2,
    paddingBottom: 2,
    background: '#0A9FED',
  },
  logo: {
    width: 32,
    height: 32,
    marginRight: 32,
    borderRadius: 32,
  },
  iconAdminPanel: {
    fontSize: '24px !important',
    color: 'rgba(255,255,255,.5)',
    margin: 'auto'
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 30,
  },
  mainMenuLink: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'rgba(255, 255, 255, .7) !important',
    transition: '0.3s all',
    '&:hover': {
      color: '#fff !important',
      textDecoration: 'none',
    },
  },
  activedMainMenuLink: {
    color: '#fff !important',
  },
  dropdownMenuLink: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, .6)',
    transition: '0.3s all',
    '&:hover': {
      textDecoration: 'none',
      color: 'rgba(0, 0, 0, .6)',
    },
    '&:active': {
      color: 'inherit',
      background: 'none',
      outline: 'none'
      // color: '#fff',
      // background: '#0A9FED',
      // outlineColor: 'transparent',
      // border: 'none',
      // backgroundColor: 'initial'
    }
  },
  dropAdmin: {
    top: 42,
    border: '1px solid rgba(0, 0, 0, .1)',
    borderRadius: 0,
    boxShadow: '0 0 5px 0 rgba( 0, 0, 0, 0.12)',
    marginTop: '5px',
    '&:before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: -6,
      right: 37,
      width: 10,
      height: 10,
      borderTop: '1px solid rgba(0, 0, 0, .15)',
      borderLeft: '1px solid rgba(0, 0, 0, .15)',
      backgroundColor: '#fff',
      transform: 'rotate(45deg)',
    },
  },
  dropdownUser: {
    top: '0px !important',
    left: '14px !important',
    minWidth: 140,
    border: '1px solid rgba(0, 0, 0, .1)',
    borderRadius: 0,
    boxShadow: '0 0 5px 0 rgba( 0, 0, 0, 0.12)',
    '&:before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: -6,
      right: 22,
      width: 10,
      height: 10,
      borderTop: '1px solid rgba(0, 0, 0, .15)',
      borderLeft: '1px solid rgba(0, 0, 0, .15)',
      backgroundColor: '#fff',
      transform: 'rotate(45deg)',
    },
  },
};

export default styles;
