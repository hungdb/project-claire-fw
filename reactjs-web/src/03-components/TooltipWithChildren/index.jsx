import React, { useState } from 'react';
import { Tooltip } from 'reactstrap';
import { element, string, number, oneOfType } from 'prop-types';

const TooltipWithChildren = ({ children, placement, id, content }) => {
  const [isOpenToolTip, setIsOpenToolTip] = useState(false);

  const toggle = () => setIsOpenToolTip(!isOpenToolTip);

  return (
    <span>
      <span id={'Tooltip-' + id}>{children}</span>
      <Tooltip placement={placement} isOpen={isOpenToolTip} target={'Tooltip-' + id} toggle={toggle}>
        {content}
      </Tooltip>
    </span>
  );
};

TooltipWithChildren.propTypes = {
  children: element,
  placement: string,
  content: string,
  id: oneOfType([string, number])
};

export default TooltipWithChildren;
