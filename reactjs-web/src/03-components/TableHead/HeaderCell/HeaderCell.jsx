import React, { memo } from 'react';
import { string, func } from 'prop-types';

const HeaderCell = ({ name, t, className }) => (
  <th className={ `${ className } font-size-14 font-weight-500 text-center align-middle` }>{ t(name) }</th>
);

HeaderCell.propTypes = {
  name: string,
  t: func,
  className: string
};

HeaderCell.defaultProps = {
  name: '',
  className: ''
};

export default memo(HeaderCell);