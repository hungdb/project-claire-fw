import HeaderCell from './HeaderCell';
import React, { memo } from 'react';
import { array, func } from 'prop-types';

const TableHead = ({ fields, t, classes }) => {
  return fields.map((name, i) => <HeaderCell key={i} name={name} t={t} className={classes[i]} />);
};

TableHead.propTypes = {
  fields: array,
  t: func,
  classes: array
};

TableHead.defaultProps = {
  fields: [],
  classes: []
};

export default memo(TableHead);
