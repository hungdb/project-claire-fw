import ReactSelect from 'react-select';
import { any, array, func, string, bool } from 'prop-types';
import React, { memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { customBorderColor } from '../../08-helpers/common';

const SelectStateBox = props => {
  const {
    options,
    onChange,
    placeholder,
    keyValue,
    keyLabel,
    isDisabled,
    isClearable,
    isSearchable,
    classes,
    ...rest
  } = props;
  const [selected, setValueSelect] = useState(null);

  return (
    <NamespacesConsumer>
      {t => (
        <ReactSelect
          className={`react-select-state ${classes}`}
          isDisabled={isDisabled}
          placeholder={t(placeholder)}
          isClearable={isClearable}
          styles={customBorderColor}
          isSearchable={isSearchable}
          options={keyValue && keyLabel ? options.map(p => ({ label: p[keyLabel], value: p[keyValue] })) : options}
          value={selected}
          onChange={(option, e) => {
            setValueSelect(option);
            let selectedObject = option;
            if (keyValue) {
              const optionValue = option ? option.value : option;
              selectedObject = options.find(p => p[keyValue] === optionValue);
            }
            onChange(selectedObject, e);
          }}
          {...rest}
        />
      )}
    </NamespacesConsumer>
  );
};

SelectStateBox.propTypes = {
  placeholder: string,
  options: array,
  onChange: func,
  keyValue: any,
  keyLabel: any,
  isDisabled: bool,
  isClearable: bool,
  isSearchable: bool,
  classes: string
};

SelectStateBox.defaultProps = {
  options: [],
  isDisabled: false,
  isClearable: true,
  isSearchable: true,
  classes: ''
};

export default memo(SelectStateBox);
