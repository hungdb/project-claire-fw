import './style.css';
import React, { memo, useState } from 'react';
import { instanceOf, func, string } from 'prop-types';
import { Input } from 'reactstrap';

const SuggestionTextField = ({ defaultValue, onChangeToSelect, suggestions, onClickToSelect, ...restProps }) => {
  const [activeSuggestion, setActiveSuggestion] = useState(0);
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [userInput, setUserInput] = useState('');

  const onChange = e => {
    const userInput = e.currentTarget.value;
    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = suggestions.filter(
      suggestion => suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );
    setActiveSuggestion(0);
    setFilteredSuggestions(filteredSuggestions);
    setShowSuggestions(true);
    setUserInput(e.currentTarget.value);
    onChangeToSelect(e.currentTarget.value);
  };

  const onClick = e => {
    setActiveSuggestion(0);
    setFilteredSuggestions([]);
    setShowSuggestions(false);
    setUserInput(e.currentTarget.innerText);
    onClickToSelect(e.currentTarget.innerText);
  };

  const onKeyDown = e => {
    // User pressed the enter key
    if (e.keyCode === 13) {
      setActiveSuggestion(0);
      setShowSuggestions(false);
      setUserInput(filteredSuggestions[activeSuggestion]);
    } else if (e.keyCode === 38) {
      // User pressed the up arrow
      if (activeSuggestion === 0) return;
      setActiveSuggestion(activeSuggestion => activeSuggestion - 1);
    } else if (e.keyCode === 40) {
      // User pressed the down arrow
      if (activeSuggestion - 1 === filteredSuggestions.length) return;
      setActiveSuggestion(activeSuggestion => activeSuggestion + 1);
    }
  };

  let suggestionsListComponent;

  if (showSuggestions && userInput) {
    if (filteredSuggestions.length) {
      suggestionsListComponent = (
        <ul className="suggestions">
          {filteredSuggestions.map((suggestion, index) => {
            let className;

            // Flag the active suggestion with a class
            if (index === activeSuggestion) {
              className = 'suggestion-active';
            }

            return (
              <li className={className} key={suggestion} onClick={onClick}>
                <p className="mb-0">{suggestion}</p>
              </li>
            );
          })}
        </ul>
      );
    }
  }

  return (
    <>
      <Input
        type="text"
        onChange={onChange}
        onKeyDown={onKeyDown}
        value={defaultValue}
        {...restProps}
        className="flex-grow-1 full focus-border"
      />
      {suggestionsListComponent}
    </>
  );
};

SuggestionTextField.propTypes = {
  suggestions: instanceOf(Array),
  onChangeToSelect: func,
  onClickToSelect: func,
  defaultValue: string
};

SuggestionTextField.defaultProps = {
  suggestions: []
};

export default memo(SuggestionTextField);
