import React, { memo } from 'react';
import Chart from 'react-google-charts';
import { NamespacesConsumer } from 'react-i18next';
import { array, number } from 'prop-types';

const KPIChartYear = ({ data, vMax, ...props }) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Chart
            chartType="LineChart"
            loader={<div className={'margin-top-15px text-center font-size-15px text-muted'}>{t('loading_chart')}</div>}
            data={data}
            options={{
              hAxis: {
                title: t('year'),
                format: '#'
              },
              vAxis: {
                title: t('point'),
                viewWindow: { min: 0, max: vMax }
              },
              series: {
                1: { curveType: 'function' }
              }
            }}
            {...props}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

KPIChartYear.propTypes = {
  data: array,
  vMax: number
};

KPIChartYear.defaultProps = {
  data: []
};

export default memo(KPIChartYear);
