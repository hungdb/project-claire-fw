import React, { memo } from 'react';
import { array, number } from 'prop-types';
import Skeleton from 'react-loading-skeleton';
import { calculateLength } from '../../08-helpers/common';

const TabsPlaceHolder = ({ widthPerItem, height }) => (
  <div role="tablist" className="d-flex">
    {widthPerItem.map((w, index) =>
      index !== calculateLength(widthPerItem) ? (
        <div key={index} style={{ width: `${w}px`, marginRight: '6px' }}>
          <Skeleton height={height} />
        </div>
      ) : (
        <div key={index} style={{ width: `${w}px` }}>
          <Skeleton height={height} />
        </div>
      )
    )}
  </div>
);

TabsPlaceHolder.defaultProps = {
  fallback: null
};

TabsPlaceHolder.propTypes = {
  widthPerItem: array,
  height: number
};

export default memo(TabsPlaceHolder);
