import './style.css';
import moment from 'moment';
import React, { useState, useEffect, memo } from 'react';
import { Link } from 'react-router-dom';
import { Progress } from 'reactstrap';
import { NamespacesConsumer } from 'react-i18next';
import { string, array, number } from 'prop-types';
import { workloadAllocation as hourOfWork } from '../../05-utils/commonUtils';
import { formatDateTime, durationHoursWork } from '../../08-helpers/common';

const ProjectCard = ({ efforts, timeCompleted, startDate, endDate, projectName, sprint, projectId }) => {
  const [progress, setProgress] = useState(0);
  const [progressDay, setProgressDay] = useState(0);

  useEffect(() => {
    let totalHourEffort = 0;
    for (let i = 0; i < efforts.length; i++) {
      const itemHour = hourOfWork(efforts[i].startDate, efforts[i].endDate, efforts[i].effortPercent);
      totalHourEffort += itemHour;
    }
    const progress = (timeCompleted / totalHourEffort) * 100;
    // const timeStart = moment(this.props.startDate, 'YYYY-MM-DD HH:mm:ss.SSS').valueOf();
    // const timeEnd = moment(this.props.endDate, 'YYYY-MM-DD HH:mm:ss.SSS').valueOf();
    const totalTimes = durationHoursWork(startDate, endDate);
    const totalSpentTimes = durationHoursWork(startDate, moment());
    // const progressDayValue = ((moment().valueOf() - timeStart)*100/(timeEnd - timeStart));
    const progressDayValue = totalSpentTimes <= totalTimes ? (totalSpentTimes * 100) / totalTimes : 100;
    setProgress(progress);
    setProgressDay(progressDayValue < 0 ? 0 : progressDayValue);
  }, [efforts, timeCompleted, startDate, endDate]);

  const projectUrl = `/sprints/${projectId}/board/1`;

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Link className={'disable-text-link'} to={projectUrl}>
          <div className="mb-2 bg-white component-sprint">
            <div className="d-flex justify-content-between first-content">
              <div className="project-name">{projectName}</div>
              <div className="project-sprint">{sprint}</div>
            </div>
            {efforts &&
              efforts.map((item, index) => {
                return (
                  <div className={'margin-top-5px project-effort'} key={index}>
                    {t('effort')}: {item.effortPercent}% from {formatDateTime(item.startDate, 'DD/MM/YYYY')} to{' '}
                    {formatDateTime(item.endDate, 'DD/MM/YYYY')}{' '}
                  </div>
                );
              })}
            <div className={'margin-top-5px'}>
              {progress >= progressDay ? (
                <Progress className="progress-sprint" multi animated={true}>
                  <Progress bar className="progress-day" value={progressDay} animated={true} tag="progress">
                    {t('Today')}
                  </Progress>
                  <Progress
                    bar
                    className={progress >= 50 ? 'progress-effort-success' : 'progress-effort-fail'}
                    value={progress - progressDay}
                    animated={true}
                    tag="progress"
                  >
                    {t('Completed')}
                  </Progress>
                </Progress>
              ) : (
                <Progress className="progress-sprint" multi animated={true}>
                  <Progress
                    bar
                    className={progress >= 50 ? 'progress-effort-success' : 'progress-effort-fail'}
                    value={progress}
                    animated={true}
                    tag="progress"
                  >
                    {t('Completed')}
                  </Progress>
                  <Progress bar className="progress-day" value={progressDay - progress} animated={true} tag="progress">
                    {t('Today')}
                  </Progress>
                </Progress>
              )}
              <div className={'d-flex justify-content-between margin-top-5px'}>
                <div>
                  <span className={'datetime'}>{formatDateTime(startDate, 'DD/MM/YYYY')}</span>
                </div>
                <div>
                  <span className={'datetime'}>{formatDateTime(endDate, 'DD/MM/YYYY')}</span>
                </div>
              </div>
            </div>
          </div>
        </Link>
      )}
    </NamespacesConsumer>
  );
};

ProjectCard.propTypes = {
  projectName: string,
  sprint: string,
  efforts: array,
  startDate: string,
  endDate: string,
  projectId: number,
  timeCompleted: number
};

ProjectCard.defaultProps = {};

export default memo(ProjectCard);
