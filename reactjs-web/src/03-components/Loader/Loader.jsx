import './style.css';
import React, { memo } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { bool, string, oneOf } from 'prop-types';
import { Spinner, Modal, ModalBody } from 'reactstrap';

const Loader = ({ isOpen, type, color }) => (
  <NamespacesConsumer ns="translations">
    {() => (
      <Modal isOpen={isOpen} centered zIndex={9999} fade={false} size={'sm'} className={'loader-area'}>
        <ModalBody className={'text-center align-center'}>
          <Spinner color={color} type={type} />
        </ModalBody>
      </Modal>
    )}
  </NamespacesConsumer>
);

Loader.propTypes = {
  isOpen: bool,
  type: string,
  color: oneOf(['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'])
};

Loader.defaultProps = {
  type: 'border',
  color: 'primary'
};

export default memo(Loader);
