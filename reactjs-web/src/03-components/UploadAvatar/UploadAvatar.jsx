import './style.css';
import i18n from '../../i18n';
import React, { memo, createRef } from 'react';
import { func, array, string, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { toast } from 'react-toastify';
import { TOAST_ERROR_STYLE } from '../../05-utils/commonData';

const onChangeFile = onSelectImage => event => {
  try {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      onSelectImage(file, reader.result);
    };
    reader.readAsDataURL(file);
  } catch (e) {
    return toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
  }
};

const UploadAvatar = ({ mimes, defaultAvatar, onSelectImage, disableEdit }) => {
  const inputRef = createRef();

  const onSelectFile = () => {
    inputRef.current.click();
    document.getElementById('file-input').addEventListener('change', onChangeFile(onSelectImage));
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className="upload-avatar text-center">
          <img alt={t('avatar')} src={defaultAvatar} />
          <span
            className={`cursor-pointer upload-btn ${disableEdit && 'd-none'} `}
            onClick={onSelectFile}
            title="Edit customer"
          >
            <i className="fa fa-4x fa-pencil color-base-btn" />
          </span>
          <input ref={inputRef} multiple={false} accept={mimes && mimes.toString()} type="file" id="file-input" />
        </div>
      )}
    </NamespacesConsumer>
  );
};

UploadAvatar.propTypes = {
  mimes: array,
  onSelectImage: func,
  defaultAvatar: string,
  disableEdit: bool
};

UploadAvatar.defaultProps = {};

export default memo(UploadAvatar);
