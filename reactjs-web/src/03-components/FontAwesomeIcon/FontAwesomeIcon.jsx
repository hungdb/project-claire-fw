import React, { memo } from 'react';
import { string, func } from 'prop-types';

const FontAwesomeIcon = ({ classes, onClick }) => (
  <i className={`fa ${classes}`} aria-hidden="true" onClick={onClick} />
);

FontAwesomeIcon.propTypes = {
  classes: string,
  onClick: func
};

FontAwesomeIcon.defaultProps = {};

export default memo(FontAwesomeIcon);
