import './styles.css';
import React, { useRef, memo } from 'react';
import { any, func, string } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { debounce } from 'throttle-debounce';

function InputIcon({ awesomeIcon, type, placeholder, value, handleInput }) {
  const textInput = useRef(null);

  return (
    <NamespacesConsumer ns={'translations'}>
      {t => (
        <div className="d-flex justify-content-between align-items-center input-icon">
          <input
            type={type}
            placeholder={t(placeholder)}
            defaultValue={value}
            ref={textInput}
            onChange={debounce(500, () => {
              handleInput(textInput.current.value);
            })}
          />
          <i className={`${awesomeIcon} mr-1 ml-2`} aria-hidden="true" />
        </div>
      )}
    </NamespacesConsumer>
  );
}

InputIcon.propTypes = {
  awesomeIcon: string,
  type: string,
  placeholder: string,
  value: any,
  handleInput: func
};

export default memo(InputIcon);
