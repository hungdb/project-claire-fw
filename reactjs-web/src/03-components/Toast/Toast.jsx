import './style.css';
import React, { memo } from 'react';
import { string } from 'prop-types';

const Toast = ({ code, extraAction, message, env }) => (
  <div>
    {code}
    <br />
    {env !== 'LCL' ? null : (
      <>
        {extraAction}
        <br />
      </>
    )}
    {message}
  </div>
);

Toast.propTypes = {
  code: string,
  extraAction: string,
  message: string,
  env: string
};

Toast.defaultProps = {
  code: '',
  extraAction: '',
  message: '',
  env: 'LCL'
};

export default memo(Toast);
