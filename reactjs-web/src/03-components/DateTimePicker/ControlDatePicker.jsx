import './style.css';
import 'react-datepicker/dist/react-datepicker.css';
import DatePicker from 'react-datepicker';
import React, { memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { func, object, string, bool, number, oneOfType } from 'prop-types';
import { formatDateTime } from '../../08-helpers/common';

const ControlDatePicker = ({
  value,
  format,
  onChange,
  placeholder,
  isLocalFormat,
  disabled,
  className,
  showTimeSelect
}) => {
  const [hasBorderDate, setHasBorderDate] = useState(false);
  const handleBorderDate = flag => setHasBorderDate(flag);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div
          className={`date-picker-input d-flex justify-content-between align-items-center
            ${hasBorderDate ? 'has-border' : ''}
            ${disabled && 'disabled'}`}
        >
          <div className={'date-picker-container ' + className}>
            <DatePicker
              disabled={disabled}
              showTimeSelect={showTimeSelect}
              value={value ? formatDateTime(value, format, isLocalFormat) : ''}
              className={'date-picker'}
              dropdownMode={'select'}
              isClearable={!disabled}
              selected={value}
              onFocus={() => handleBorderDate(true)}
              onBlur={() => handleBorderDate(false)}
              onChange={onChange}
              placeholderText={t(placeholder)}
            />
          </div>
          <i className={'date-picker-icon fa fa-calendar mr-1 ml-2'} aria-hidden="true" />
        </div>
      )}
    </NamespacesConsumer>
  );
};

ControlDatePicker.propTypes = {
  onChange: func,
  value: object,
  format: string,
  placeholder: string,
  isLocalFormat: oneOfType([number, bool]),
  disabled: bool,
  className: string,
  showTimeSelect: bool
};

ControlDatePicker.defaultProps = {};

export default memo(ControlDatePicker);
