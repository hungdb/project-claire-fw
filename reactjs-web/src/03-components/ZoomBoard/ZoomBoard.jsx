import './style.css';
import TooltipWithChildren from '../../03-components/TooltipWithChildren';
import React, { memo } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toggleFullBoard } from '../../01-actions/common';
import { func, number } from 'prop-types';
import { setKey } from '../../08-helpers/storage';
import { FULL_BOARD_TOGGLE } from '../../07-constants/common';
import { STATUS_FULL_BOARD } from '../../05-utils/commonData';
import { NamespacesConsumer } from 'react-i18next';

const ZoomBoard = ({ status, _toggleFullBoard }) => {
  const toggleFullBoard = () => {
    if (!status) {
      setKey(FULL_BOARD_TOGGLE, STATUS_FULL_BOARD.ZOOM_OUT);
      _toggleFullBoard(STATUS_FULL_BOARD.ZOOM_OUT);
    }
    if (status === STATUS_FULL_BOARD.ZOOM_OUT) {
      setKey(FULL_BOARD_TOGGLE, STATUS_FULL_BOARD.ZOOM_IN);
      _toggleFullBoard(STATUS_FULL_BOARD.ZOOM_IN);
    }
    if (status === STATUS_FULL_BOARD.ZOOM_IN) {
      setKey(FULL_BOARD_TOGGLE, STATUS_FULL_BOARD.ZOOM_OUT);
      _toggleFullBoard(STATUS_FULL_BOARD.ZOOM_OUT);
    }
  };

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <TooltipWithChildren
          id={'toggle-full-board'}
          placement={'top'}
          content={t(status === STATUS_FULL_BOARD.ZOOM_IN ? 'zoom_out_board' : 'zoom_in_board')}
        >
          <span className={'full-board-toggle'} onClick={toggleFullBoard}>
            <i
              className={`fa ${
                status === STATUS_FULL_BOARD.ZOOM_IN ? 'fa-arrow-circle-o-down' : 'fa-arrow-circle-o-up'
              }`}
            />
          </span>
        </TooltipWithChildren>
      )}
    </NamespacesConsumer>
  );
};

ZoomBoard.propTypes = {
  _toggleFullBoard: func,
  status: number
};

ZoomBoard.defaultProps = {};

const mapStateToProps = state => ({
  status: state.common.toggleFullBoard
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _toggleFullBoard: toggleFullBoard
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(ZoomBoard));
