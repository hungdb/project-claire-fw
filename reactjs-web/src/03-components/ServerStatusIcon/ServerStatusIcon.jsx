import React, { memo } from 'react';
import { COLORS } from '../../05-utils/commonData';
import { bool, string } from 'prop-types';

const ServerStatusIcon = ({ className, active }) => (
  <div style={{ color: active ? COLORS.GREEN : COLORS.RED }} className={className}>
    <i className="fa fa-circle" aria-hidden="true" />
  </div>
);

ServerStatusIcon.propTypes = {
  className: string,
  active: bool
};

ServerStatusIcon.defaultProps = {
  className: '',
  active: false
};

export default memo(ServerStatusIcon);
