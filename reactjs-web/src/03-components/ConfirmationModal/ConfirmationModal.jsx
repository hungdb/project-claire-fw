import React, { memo } from 'react';
import { bool, string, func, element, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const ConfirmationModal = ({
  isOpen,
  txtTitle,
  txtCancel,
  txtConfirm,
  funConfirm,
  funClose,
  funCloseParent,
  children
}) => (
  <NamespacesConsumer>
    {t => (
      <Modal isOpen={isOpen} centered toggle={funClose} fade={true} zIndex="9999" backdrop="static">
        <ModalHeader toggle={funClose}>{t(txtTitle)}</ModalHeader>
        <ModalBody>{children}</ModalBody>
        <ModalFooter>
          <Button className={'outline-base-btn'} size={'sm'} onClick={funClose}>
            {t(txtCancel)}
          </Button>
          <Button
            className={'base-btn'}
            size={'sm'}
            onClick={() => {
              funConfirm();
              funClose();
              funCloseParent && funCloseParent();
            }}
          >
            {t(txtConfirm)}
          </Button>
        </ModalFooter>
      </Modal>
    )}
  </NamespacesConsumer>
);

ConfirmationModal.propTypes = {
  isOpen: bool,
  txtTitle: string,
  txtCancel: string,
  txtConfirm: string,
  funConfirm: func,
  funClose: func,
  funCloseParent: func,
  children: oneOfType([element, string])
};

ConfirmationModal.defaultProps = {
  txtTitle: 'alert',
  txtContent: 'are_you_sure',
  txtConfirm: 'confirm',
  txtCancel: 'cancel'
};

export default memo(ConfirmationModal);
