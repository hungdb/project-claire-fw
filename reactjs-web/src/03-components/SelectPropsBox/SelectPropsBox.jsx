import ReactSelect from 'react-select';
import React, { memo } from 'react';
import makeAnimated from 'react-select/lib/animated';
import { array, func, any, string, bool } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { customBorderColor } from '../../08-helpers/common';

const SelectPropsBox = props => {
  const {
    value,
    options,
    onChange,
    placeholder,
    keyValue,
    keyLabel,
    isDisabled,
    isClearable,
    isSearchable,
    classes,
    isAnimatate,
    isMulti,
    ...rest
  } = props;
  const selectedObject = options.find(p => p[keyValue] === value);

  return (
    <NamespacesConsumer>
      {t => (
        <ReactSelect
          isMulti={isMulti}
          className={classes}
          isDisabled={isDisabled}
          placeholder={t(placeholder)}
          isClearable={isClearable}
          styles={customBorderColor}
          isSearchable={isSearchable}
          options={options.map(p => ({
            label: p[keyLabel],
            value: p[keyValue]
          }))}
          components={isAnimatate && makeAnimated()}
          value={
            isMulti
              ? value
              : selectedObject
              ? {
                  label: selectedObject[keyLabel],
                  value: selectedObject[keyValue]
                }
              : null
          }
          onChange={onChange}
          {...rest}
        />
      )}
    </NamespacesConsumer>
  );
};

SelectPropsBox.propTypes = {
  placeholder: string,
  options: array,
  onChange: func,
  value: any,
  keyValue: any,
  keyLabel: any,
  isDisabled: bool,
  isClearable: bool,
  isSearchable: bool,
  isAnimatate: bool,
  classes: string,
  isMulti: bool
};

SelectPropsBox.defaultProps = {
  options: [],
  keyValue: 'value',
  keyLabel: 'label',
  isDisabled: false,
  isClearable: true,
  isSearchable: true,
  classes: ''
};

export default memo(SelectPropsBox);
