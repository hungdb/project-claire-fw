import React, { lazy, Suspense } from 'react';
import { func, element } from 'prop-types';

const LazyComponent = (importFunc, fallback = null) => {
  const Lazy = lazy(importFunc);
  return props => (
    <Suspense fallback={fallback}>
      <Lazy {...props} />
    </Suspense>
  );
};

LazyComponent.defaultProps = {
  fallback: null
};

LazyComponent.propTypes = {
  fallback: element,
  importFunc: func
};

export default LazyComponent;
