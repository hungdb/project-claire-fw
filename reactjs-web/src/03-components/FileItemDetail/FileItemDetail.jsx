import './style.css';
import React, { memo, useState } from 'react';
import { getIconFile, truncateText, formatDateTime } from '../../08-helpers/common';
import { object, number, func, bool } from 'prop-types';

const FileItemDetail = ({
  attachment,
  index,
  enableRemove,
  onRemoveAttachments,
  onOpenAlertAttachment,
  enableDownload
}) => {
  const [attachmentAction, setAttachmentAction] = useState(false);
  const handleAttachmentAction = flag => setAttachmentAction(flag);

  return (
    <div
      className={`file file-item d-flex align-items-center mb-3 ${attachmentAction && ' hover'} `}
      onMouseEnter={() => handleAttachmentAction(true)}
      onMouseLeave={() => handleAttachmentAction(false)}
    >
      <i className={`${getIconFile(attachment.mimeType || attachment.type)} mr-3 file-icon`} />
      <div className="w-100">
        <div title={attachment.name}>{truncateText(attachment.name, 30)}</div>
        <div className="d-flex justify-content-between align-items-center file-item-action">
          <small>
            <i className="fa fa-clock-o" />
            &nbsp;
            {formatDateTime(attachment.createdAt || attachment.lastModifiedDate, 'LLLL', 1)}
          </small>
          {enableRemove && (
            <span className="at-btn-remove" onClick={() => onRemoveAttachments(attachment.id || index)}>
              <i className="fa fa fa-trash-o" />
            </span>
          )}
          {enableDownload && attachment.id && (
            <span className="at-btn-download" onClick={() => onOpenAlertAttachment(attachment)}>
              <i className="fa fa-eye" />
            </span>
          )}
        </div>
      </div>
    </div>
  );
};

FileItemDetail.propTypes = {
  attachment: object,
  index: number,
  onRemoveAttachments: func,
  onOpenAlertAttachment: func,
  enableRemove: bool,
  enableDownload: bool,
};

FileItemDetail.defaultProps = {
  enableDownload: true
};

export default memo(FileItemDetail);
