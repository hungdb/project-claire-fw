import './style.css';
import React, { useState, memo, useEffect } from 'react';
import { Tabs, Tab } from '@material-ui/core';
import { NamespacesConsumer } from 'react-i18next';
import { array, func } from 'prop-types';

const renderAllTabs = (dataFields, t, dataFull) => {
  if (dataFull && Array.isArray(dataFull)) {
    return (
      dataFull &&
      dataFull.map((item, index) => (
        <Tab
          key={index}
          className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
          value={index + 1}
          label={`${t(item.label)} (${item.total || 0})`}
        />
      ))
    );
  }
  return (
    dataFields &&
    dataFields.map((item, index) => (
      <Tab
        key={index}
        className={'tabs-btn font-size-14px font-weight-400 min-width-90px min-height-30px'}
        value={index + 1}
        label={t(item)}
      />
    ))
  );
};

const TabsController = ({ allTabsField, allTabs, onChangeTabs, tabId }) => {
  const [stateSelectedTab, setStateSelectedTab] = useState(1);

  const setSelectedTab = tabId => {
    setStateSelectedTab(tabId);
    onChangeTabs && onChangeTabs(tabId);
  };

  const init = () => {
    if (tabId) setStateSelectedTab(tabId);
  };

  useEffect(() => {
    init();
  }, [tabId]);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Tabs
          value={stateSelectedTab || false}
          className={'tabs min-height-auto'}
          classes={{
            indicator: 'hidden-tabs-indicator'
          }}
          onChange={(e, value) => setSelectedTab(value)}
        >
          {renderAllTabs(allTabsField, t, allTabs)}
        </Tabs>
      )}
    </NamespacesConsumer>
  );
};

TabsController.propTypes = {
  allTabsField: array,
  allTabs: array,
  onChangeTabs: func
};

TabsController.defaultProps = {};

export default memo(TabsController);
