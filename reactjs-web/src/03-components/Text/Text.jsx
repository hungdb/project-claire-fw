import React, { memo } from 'react';
import { string, func, oneOfType, node, element, object } from 'prop-types';

const Text = ({ className, click, children, style }) => (
  <div className={className} onClick={click} style={style}>
    {children}
  </div>
);

Text.propTypes = {
  className: string,
  click: func,
  children: oneOfType([string, node, element]),
  style: object
};

Text.defaultProps = {};

export default memo(Text);
