import './style.css';
import cx from 'classnames';
import React, { memo } from 'react';
import { string, any, func } from 'prop-types';

const AppText = ({ children, onClick, type }) => (
  <div
    className={cx({
      default: !type,
      'section-header': type === 'section-header',
      'title-header': type === 'title-header',
      'content-header': type === 'content-header',
      'content-sub-header': type === 'content-sub-header',
      'title-menu': type === 'title-menu'
    })}
    onClick={() => {
      onClick && onClick();
    }}
  >
    {children}
  </div>
);

AppText.propTypes = {
  children: any,
  className: string,
  type: string,
  onClick: func
};

AppText.defaultProps = {
  type: ''
};

export default memo(AppText);
