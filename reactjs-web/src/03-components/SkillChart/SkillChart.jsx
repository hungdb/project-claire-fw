import { array, string } from 'prop-types';
import React, { memo } from 'react';
import Chart from 'react-google-charts';
import { NamespacesConsumer } from 'react-i18next';

const SkillChart = ({ data, title, ...props }) => {
  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div>
          <Chart
            chartType="BarChart"
            loader={<div className={'margin-top-15px text-center font-size-15px text-muted'}>{t('loading_chart')}</div>}
            data={data}
            options={{
              chartArea: {
                top: 80,
                bottom: 80
              },
              title,
              legend: { position: 'none' },
              hAxis: {
                // title: t('people'),
                minValue: 0,
                format: '#'
              },
              vAxis: {
                title: t('skill')
              }
            }}
            {...props}
          />
        </div>
      )}
    </NamespacesConsumer>
  );
};

SkillChart.propTypes = {
  data: array,
  title: string
};

SkillChart.defaultProps = {
  data: [],
  title: ''
};

export default memo(SkillChart);
