import './style.css';
import Dropzone from 'react-dropzone';
import cx from 'classnames';
import React, { memo } from 'react';
import { func, array } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { allowMimeTypes } from '../../08-helpers/mime_types';

const Upload = ({ mimes, onDrop, ...props }) => (
  <NamespacesConsumer ns="translations">
    {t => (
      <Dropzone accept={mimes.toString()} onDrop={onDrop} {...props}>
        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => {
          return (
            <div
              {...getRootProps()}
              className={cx(
                'dropzone text-center',
                { 'dropzone-active': isDragActive },
                { 'dropzone-reject': isDragReject }
              )}
            >
              <input {...getInputProps()} />
              {isDragActive ? (
                isDragReject ? (
                  <p>{t('unsupported_file_type')}</p>
                ) : (
                  <p>{t('drop_files_here')}</p>
                )
              ) : (
                <p>{t('try_dropping_some_files_here_or_click_to_select_files_to_upload')}</p>
              )}
              <i className="fa fa-cloud-upload fa-5x" />
            </div>
          );
        }}
      </Dropzone>
    )}
  </NamespacesConsumer>
);

Upload.propTypes = {
  mimes: array,
  onDrop: func
};

Upload.defaultProps = {
  mimes: allowMimeTypes
};

export default memo(Upload);
