import React, { memo, useState } from 'react';
import { node, string, any, oneOf, oneOfType, bool, number } from 'prop-types';
import { Modal, ModalHeader, ModalBody, Tooltip } from 'reactstrap';

const ModelInfo = ({
  children,
  title,
  iconAwesome,
  label,
  styleBtn,
  styleModel,
  backdrop,
  size,
  classes,
  placement,
  id,
  btnColor
}) => {
  const [openModel, setOpenModel] = useState(false);
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggleOpenModel = flag => setOpenModel(flag);
  const toggleOpenTooltip = () => setTooltipOpen(!tooltipOpen);

  return (
    <div>
      {/* eslint-disable-next-line */}
      <a
        className={`fa ${iconAwesome} cursor-pointer ${btnColor}`}
        id={`Tooltip-${id}`}
        style={styleBtn}
        onClick={() => toggleOpenModel(true)}
      />
      <Tooltip placement={placement} target={`Tooltip-${id}`} isOpen={tooltipOpen} toggle={toggleOpenTooltip}>
        {label}
      </Tooltip>
      <Modal
        size={size}
        centered={true}
        isOpen={openModel}
        className={classes}
        toggle={() => toggleOpenModel(false)}
        backdrop={backdrop}
        style={styleModel}
      >
        <ModalHeader toggle={() => toggleOpenModel(false)}>{title}</ModalHeader>
        <ModalBody>{children}</ModalBody>
      </Modal>
    </div>
  );
};

ModelInfo.propTypes = {
  children: node,
  title: string,
  label: string,
  iconAwesome: string,
  styleBtn: any,
  color: oneOf(['default', 'inherit', 'primary', 'secondary']),
  styleModel: any,
  backdrop: oneOfType([bool, oneOf(['static'])]),
  size: string,
  classes: string,
  placement: oneOf([
    'auto',
    'auto-start',
    'auto-end',
    'top',
    'top-start',
    'top-end',
    'right',
    'right-start',
    'right-end',
    'bottom',
    'bottom-start',
    'bottom-end',
    'left',
    'left-start',
    'left-end'
  ]),
  id: oneOfType([string, number]),
  btnColor: string
};

ModelInfo.defaultProps = {
  label: 'label',
  iconAwesome: 'fa fa-ellipsis-h',
  color: 'default',
  styleModel: {},
  classes: 'text-center',
  backdrop: true,
  size: '',
  placement: 'right',
  id: '',
  btnColor: 'text-info'
};

export default memo(ModelInfo);
