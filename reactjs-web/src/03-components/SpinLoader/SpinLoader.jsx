import './style.css';
import React from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { string } from 'prop-types';
import { Spinner } from 'reactstrap';

const SpinLoader = ({ color, type }) => (
  <NamespacesConsumer ns="translations">
    {() => (
      <div className="spinner-loader">
        <Spinner color={color} type={type} />
      </div>
    )}
  </NamespacesConsumer>
);

SpinLoader.propTypes = {
  color: string,
  type: string
};

SpinLoader.defaultProps = {
  color: 'primary',
  type: 'grow'
};

export default SpinLoader;
