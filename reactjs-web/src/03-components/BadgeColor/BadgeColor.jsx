import withStyles from 'react-jss';
import React, { memo } from 'react';
import { Badge } from 'reactstrap';
import { string, any, object } from 'prop-types';

const styles = {
  root: props => ({
    backgroundColor: props.backgroundColor,
    textColor: props.labelColor
  })
};

const BadgeColor = ({ children, classes }) => <Badge className={classes.root}>{children}</Badge>;

BadgeColor.propTypes = {
  backgroundColor: string,
  textColor: string,
  children: any,
  classes: object
};

BadgeColor.defaultProps = {
  backgroundColor: '',
  textColor: '',
  children: ''
};

export default withStyles(styles)(memo(BadgeColor));
