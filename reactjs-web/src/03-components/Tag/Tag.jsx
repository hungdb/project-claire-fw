import './style.css';
import React, { memo } from 'react';
import { string } from 'prop-types';

const Tag = ({ name, classTag }) => <span className={classTag}>{name}</span>;

Tag.propTypes = {
  name: string,
  classTag: string
};

Tag.defaultProps = {
  name: '',
  classTag: 'tag-default'
};

export default memo(Tag);
