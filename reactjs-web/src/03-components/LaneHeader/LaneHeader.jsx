import './styles.css';
import cx from 'classnames';
import React, { memo } from 'react';
import { string, number } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { Badge } from 'reactstrap';
import { EFFORT_TYPES } from '../../05-utils/commonData';

const LaneHeader = ({ title, total, effortType }) => {
  return (
    <NamespacesConsumer>
      {t => (
        <div className={'lane-header-custom'}>
          <span className={'title-lane-header'}>{title}</span>
          {effortType && (
            <Badge
              color={cx({
                info: effortType === EFFORT_TYPES.UNTOUCHED,
                danger: effortType === EFFORT_TYPES.HOLDING,
                warning: effortType === EFFORT_TYPES.PROGRESS,
                success: effortType === EFFORT_TYPES.TERMINATED
              })}
              className={'effort-type-lane-header'}
            >
              {effortType}
            </Badge>
          )}
          <span className={'total-lane-header'}>{total}</span>
          <div className={'line-break'} />
        </div>
      )}
    </NamespacesConsumer>
  );
};

LaneHeader.propTypes = {
  title: string,
  total: number,
  effortType: string
};

LaneHeader.defaultProps = {
  title: '',
  total: 0,
  effortType: ''
};

export default memo(LaneHeader);
