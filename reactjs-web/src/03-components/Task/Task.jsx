import './style.css';
import React, { memo, useState, useEffect } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { string, number, func, bool } from 'prop-types';
import { calculateRemainingRate, truncateText } from '../../08-helpers/common';

const Task = ({ isSelect, taskContent, estimate, startDate, totalTime, endDate, selectTask, taskId }) => {
  const [background, setBackground] = useState('');
  const colorWarning = '#fde8e7';
  const colorOK = '#dff3fb';

  const init = () => {
    let remainingRate = calculateRemainingRate(totalTime, endDate, startDate, estimate);
    const bg = `linear-gradient(90deg, ${
      remainingRate >= 50 ? colorOK : colorWarning
      } ${remainingRate}%, white ${remainingRate}%`;
    setBackground(bg);
  };

  useEffect(() => {
    init();
  }, [totalTime, endDate, startDate, estimate]);

  const selectTaskAction = () => selectTask(taskId);

  return (
    <NamespacesConsumer ns="translations">
      {() => (
        <div
          className={`component-task ${isSelect ? 'active' : ''} mb-1`}
          style={{ background }}
          title={taskContent}
          onClick={selectTaskAction}
        >
          <div className={'d-flex'}>
            <div className={'font-size-15 margin-right-5px'}>{truncateText(taskContent, 50)}</div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

Task.propTypes = {
  taskContent: string,
  taskId: number,
  selectTask: func,
  isSelect: bool,
  estimate: number,
  totalTime: number,
  startDate: string,
  endDate: string
};

Task.defaultProps = {
  taskTag: [],
  isState: true,
  isRemainingTime: true
};

export default memo(Task);
