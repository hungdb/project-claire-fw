import i18n from './../../i18n';
import Upload from '../Upload';
import React, { Fragment, memo, useState } from 'react';
import { NamespacesConsumer } from 'react-i18next';
import { Button } from 'reactstrap';
import { func, element } from 'prop-types';
import { toast } from 'react-toastify';
import { calculateLength } from '../../08-helpers/common';
import { TOAST_ERROR_STYLE } from '../../05-utils/commonData';

const handleUpload = onHandleUpload => (acceptedFiles, rejectedFiles) => {
  if (calculateLength(rejectedFiles)) {
    toast.error(i18n.t('Oops! something error.'), TOAST_ERROR_STYLE);
  }
  if (calculateLength(acceptedFiles)) {
    onHandleUpload(acceptedFiles);
  }
};

const GroupFileUpload = ({ notes, onHandleUpload, ...rest }) => {
  const [openUpload, setOpenUpload] = useState(false);
  const toggleUpload = () => setOpenUpload(!openUpload);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Fragment>
          <Button className={'orange-btn mb-4'} size={'md'} onClick={toggleUpload}>
            {!openUpload ? (
              <i className={'fa fa-plus margin-right-5px'} />
            ) : (
              <i className={'fa fa-minus margin-right-5px'} />
            )}
            {!openUpload ? t('upload_file') : t('close_upload_file')}
          </Button>
          {notes}
          {openUpload && (
            <div className="d-flex justify-content-center mt-3">
              <Upload onDrop={handleUpload(onHandleUpload)} {...rest} />
            </div>
          )}
        </Fragment>
      )}
    </NamespacesConsumer>
  );
};

GroupFileUpload.propTypes = {
  onHandleUpload: func,
  notes: element
};

GroupFileUpload.defaultProps = {};

export default memo(GroupFileUpload);
