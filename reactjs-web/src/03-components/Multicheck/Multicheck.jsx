import React, { memo } from 'react';
import { string, bool, array, func, object } from 'prop-types';

const Multicheck = ({ className, items, onChange, styleCheckbox, styleContainer, disable }) => (
  <div className={'d-flex justify-content-around ' + (className || '')} aria-disabled={disable}>
    {items.map((item, index) => (
      <div
        className={'hover-response d-inline-flex cursor-pointer min-width-120px no-text-select'}
        key={index}
        style={styleContainer}
        onClick={() => {
          onChange && onChange(index, !item.checked);
        }}
      >
        {item.checked ? (
          <div className={'main-color'} style={styleCheckbox}>
            <i className={'fa fa-check-square-o font-size-22'} />
          </div>
        ) : (
          <i className={'fa fa-square-o font-size-22'} style={styleCheckbox} />
        )}
        <div className={'margin-left-5px'}>{item.label}</div>
      </div>
    ))}
  </div>
);

Multicheck.propTypes = {
  className: string,
  disable: bool,
  items: array,
  onChange: func,
  styleContainer: object,
  styleCheckbox: object
};

Multicheck.defaultProps = {
  className: '',
  disable: false,
  items: [],
  styleCheckbox: {},
  styleContainer: {}
};

export default memo(Multicheck);
