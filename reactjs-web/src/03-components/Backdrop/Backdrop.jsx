import './Backdrop.css';
import React, { memo } from 'react';
import { func } from 'prop-types';

const Backdrop = ({ click }) => <div className="backdrop" onClick={click} />;

Backdrop.propTypes = {
  click: func
};

Backdrop.defaultProps = {};

export default memo(Backdrop);
