import withStyles from 'react-jss';
import styles from './styles';
import defaultAvatar from '../../06-assets/default_avatar.png';
import React, { memo, useState, useEffect } from 'react';
import { string, array, any, number, object } from 'prop-types';
import { Badge } from 'reactstrap';
import { EFFORT_TYPES, TASK_TYPE_OBJECT } from '../../05-utils/commonData';
import { calculateLength, calculateRemainingRate, linkImage, formatDateTime } from '../../08-helpers/common';
import { NamespacesConsumer } from 'react-i18next';

const getTaskCode = (taskCode, id) => (taskCode ? taskCode : id);

const getJoinedUser = (relateUsers = [], assigneeId) => relateUsers.filter(u => u.id !== assigneeId).filter(Boolean);

const TaskCard = ({
  effortType,
  estimate,
  startDate,
  totalTime,
  endDate,
  classes,
  type,
  totalComments,
  createdAt,
  dueDate,
  taskCode,
  id,
  relateUsers,
  assignee,
  status,
  title,
  time,
  priority
}) => {
  const [background, setBackground] = useState('');
  const [border, setBorder] = useState('none');
  const colorWarning = '#fde8e7';
  const colorOK = '#dff3fb';

  const init = () => {
    if (effortType === EFFORT_TYPES.PROGRESS) {
      let remainingRate = calculateRemainingRate(totalTime, endDate, startDate, estimate);
      const bg = `linear-gradient(90deg, ${
        remainingRate >= 50 ? colorOK : colorWarning
      } ${remainingRate}%, white ${remainingRate}%`;
      setBackground(bg);
    } else {
      setBackground('');
    }
    if (type === TASK_TYPE_OBJECT.BUG) {
      const bd = '1px solid #f9bcb9';
      setBorder(bd);
    } else {
      setBorder('');
    }
  };

  useEffect(() => {
    init();
  }, [type, effortType, totalTime, endDate, startDate, estimate]);

  const _taskCode = getTaskCode(taskCode, id);
  const joinedUser = getJoinedUser(relateUsers, assignee && assignee.id);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <div className={'p-2 ' + classes.card} style={{ background, borderRadius: '3px', border }}>
          <div className="d-flex mb-2">
            {status &&
              status.map((value, index) => (
                <Badge className={'mr-1 mb-1 px-2 py-1 ' + classes.statusBadge} color="primary" key={index}>
                  {value}
                </Badge>
              ))}
          </div>
          <div className={'mb-2 ' + classes.title}> #{_taskCode + ' ' + title}</div>
          {
            // render end date and total comments
            <div className={'d-flex justify-content-between mb-2 font-size-12'}>
              {dueDate && (
                <Badge className={'mb-0 margin-right-10px max-height-18px pt-1'} title={t('due_date')} color={'danger'}>
                  {formatDateTime(dueDate, 'DD/MM')}
                </Badge>
              )}
              <Badge
                title={endDate ? t('end_date') : startDate ? t('start_date') : t('created_at')}
                className={'task-end-date mb-0 max-height-18px pt-1'}
                color={'info'}
              >
                {endDate
                  ? formatDateTime(endDate, 'DD/MM')
                  : startDate
                  ? formatDateTime(startDate, 'DD/MM')
                  : createdAt
                  ? formatDateTime(createdAt, 'DD/MM')
                  : null}
              </Badge>
              {totalComments ? (
                <p className={'total-comments mb-0 ml-auto'}>
                  {totalComments}
                  <i className="fa fa-comment ml-1 color-base-btn" />
                </p>
              ) : null}
            </div>
          }
          <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex">
              <img
                src={assignee && assignee.avatar ? linkImage(assignee.avatar) : defaultAvatar}
                alt={assignee && assignee.name}
                title={assignee && assignee.name}
                className={'mr-1 ' + classes.avatar}
              />
              {calculateLength(joinedUser) === 0 ? null : (
                <div
                  className={
                    'd-flex justify-content-center align-items-center ' + classes.avatar + ' ' + classes.moreAvatar
                  }
                >
                  + {joinedUser.length}
                </div>
              )}
              <div className={calculateLength(joinedUser) > 0 ? 'px-1' : 'px-2'} />
            </div>
            <div>
              <Badge className={'px-2 py-1 ' + classes.timeBadge} title={t('estimate')}>
                {time}
              </Badge>
              {priority === 1 && (
                <Badge
                  title={t('priority')}
                  color="secondary"
                  className={'ml-1 px-2 py-1 padding-top-4px padding-bottom-2px ' + classes.priorityBadge}
                >
                  {t('low')}
                </Badge>
              )}
              {priority === 2 && (
                <Badge
                  title={t('priority')}
                  color="warning"
                  className={'ml-1 px-2 py-1 padding-top-4px padding-bottom-2px ' + classes.priorityBadge}
                >
                  {t('medium')}
                </Badge>
              )}
              {priority === 3 && (
                <Badge
                  title={t('priority')}
                  color="danger"
                  className={'ml-1 px-2 py-1 padding-top-4px padding-bottom-2px ' + classes.priorityBadge}
                >
                  {t('high')}
                </Badge>
              )}
            </div>
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

TaskCard.propTypes = {
  id: string,
  title: string,
  description: string,
  time: string,
  status: array,
  createdAt: string,
  avatar: any,
  effortType: string,
  estimate: number,
  totalTime: number,
  startDate: string,
  endDate: string,
  dueDate: string,
  priority: number,
  assignee: object,
  taskCode: string,
  relateUsers: array,
  classes: object,
  type: number,
  totalComments: number
};

TaskCard.defaultProps = {};

export default withStyles(styles)(memo(TaskCard));
