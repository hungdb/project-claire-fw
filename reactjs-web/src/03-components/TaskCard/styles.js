const styles = {
  card: {
    '&:hover': {
      background: '#fff',
    },
  },
  title: {
    display: '-webkit-box',
    overflow: 'hidden',
    height: 39,
    WebkitLineClamp: 2,
    WebkitBoxOrient: 'vertical',
    fontSize: 13,
    fontWeight: 'bold',
    whiteSpace: 'normal',
    color: 'rgba(52, 58, 64, 0.8)',
  },
  avatar: {
    width: 23,
    height: 23,
    borderRadius: 50,
  },
  moreAvatar: {
    fontSize: 10,
    fontWeight: 'bold',
    color: 'rgba(52, 58, 64, 0.8)',
    background: '#E2E2E2',
    marginTop: 1,
    marginRight: 3
  },
  timeBadge: {
    border: '1px solid #17a2b8',
    color: '#17a2b8',
    background: '#fff',
    fontSize: 10
  },
  priorityBadge: {
    fontSize: '10px',
    padding: '4px 0 2px 0'
  },
  statusBadge: {
    fontSize: '10px',
    background: '#099eec',
    color: '#fff'
  }
};

export default styles;
