import React, { memo } from 'react';
import { any } from 'prop-types';
import { formatDateTime } from '../../08-helpers/common';

const ProgressMulti = ({ date }) => <div className={'datetime'}>{formatDateTime(date, 'DD/MM/YYYY')}</div>;

ProgressMulti.propTypes = {
  date: any
};

ProgressMulti.defaultProps = {};

export default memo(ProgressMulti);
