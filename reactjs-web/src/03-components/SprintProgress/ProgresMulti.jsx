import React, { memo } from 'react';
import { array, number } from 'prop-types';
import { Progress } from 'reactstrap';

const ProgressMulti = ({ progressDay, unit }) => (
  <Progress multi>
    {progressDay.map((p, i) => {
      if (p.codeColor === 0) {
        return <Progress key={i} bar className={'day-off-progress'} value={unit} />;
      } else if (p.codeColor === 1) {
        return <Progress key={i} bar animated className={'complete-progress'} value={unit} />;
      } else if (p.codeColor === 2) {
        return <Progress key={i} bar animated className={'today-progress'} value={unit} />;
      } else {
        return <Progress key={i} bar className={'none-progress'} value={unit} />;
      }
    })}
  </Progress>
);

ProgressMulti.propTypes = {
  progressDay: array,
  unit: number
};

ProgressMulti.defaultProps = {};

export default memo(ProgressMulti);
