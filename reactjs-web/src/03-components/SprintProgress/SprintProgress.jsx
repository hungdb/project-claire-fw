import './style.css';
import moment from 'moment';
import LazyComponent from '../LazyComponent';
import Skeleton from 'react-loading-skeleton';
import React, { memo } from 'react';
import { oneOfType, number, string, object } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';
import { promiseDelayImport } from '../../08-helpers/common';

const ProgresMulti = LazyComponent(promiseDelayImport(import('./ProgresMulti')), <Skeleton />);
const Date = LazyComponent(promiseDelayImport(import('./Date')), <Skeleton width={80} />);

const computeDaysBetweenDates = (startDate, endDate) => {
  const dates = [];
  const startMDate = moment(startDate)
    .utc()
    .startOf('day');
  const endMDate = moment(endDate)
    .utc()
    .startOf('day');

  do {
    switch (startMDate.isoWeekday()) {
      case 6:
      case 7:
        dates.push({
          day: startMDate.toDate(),
          isDayOff: true
        });
        break;
      default:
        dates.push({
          day: startMDate.toDate(),
          isDayOff: false
        });
        break;
    }
  } while (startMDate.add(1, 'days').diff(endMDate) <= 0);

  return dates;
};

const computeDayProgress = (dates, computedTime, totalTime) => {
  const currMDate = moment()
    .utc()
    .startOf('day');
  const dayWork = dates.filter(d => !d.isDayOff).length;
  let computedDay = Math.floor((computedTime * dayWork) / totalTime);

  return dates.map((d, i) => {
    if (d.isDayOff) {
      computedDay++;
      return { ...d, codeColor: 0 };
    } else if (computedDay > i) {
      return { ...d, codeColor: 1 };
    } else if (
      moment(d.day)
        .utc()
        .startOf('day')
        .diff(currMDate) <= 0
    ) {
      return { ...d, codeColor: 2 };
    } else {
      return { ...d, codeColor: 3 };
    }
  });
};

const calculateUnit = dates => 100 / dates.length;

const SprintProgress = ({ completedTime, totalTime, startDate, endDate }) => {
  const dates = computeDaysBetweenDates(startDate, endDate);
  const progressDay = computeDayProgress(dates, completedTime, totalTime);
  const unit = calculateUnit(dates);

  return (
    <NamespacesConsumer ns="translations">
      {() => (
        <div className="mt-5 pb-2 rounded bg-white sprint-progress-bar">
          <ProgresMulti progressDay={progressDay} unit={unit} />
          <div className="d-flex justify-content-between mt-2">
            <Date date={startDate} />
            <Date date={endDate} />
          </div>
        </div>
      )}
    </NamespacesConsumer>
  );
};

SprintProgress.propTypes = {
  completedTime: number,
  totalTime: number,
  startDate: oneOfType([string, object]),
  endDate: oneOfType([string, object])
};

SprintProgress.defaultProps = {};

export default memo(SprintProgress);
