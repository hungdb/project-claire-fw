import cx from 'classnames';
import React, { memo } from 'react';
import { func, oneOf } from 'prop-types';

const handleText = text => {
  return text && text.charAt(0).toUpperCase() + text.slice(1);
};

const Status = ({ type }) => (
  <span
    className={cx({
      'text-secondary': type === 'unactive',
      'text-warning': type === 'active',
      'text-success': type === 'completed'
    })}
  >
    {handleText(type)}
  </span>
);

Status.propTypes = {
  click: func,
  type: oneOf(['active', 'unactive', 'completed'])
};

Status.defaultProps = {
  type: 'unactive'
};

export default memo(Status);
