import './styles.css';
import React, { memo, useState } from 'react';
import { Input } from 'reactstrap';
import { string, func, bool, number, oneOfType } from 'prop-types';
import { NamespacesConsumer } from 'react-i18next';

const FieldInput = ({
  onChangeValue,
  classes,
  placeholder,
  defaultValue,
  type,
  disabled,
  name,
  awesomeIcon,
  isDebounce,
  ...restProps
}) => {
  const [hasBorder, setHasBorder] = useState(false);
  const toggleBorder = flag => setHasBorder(flag);

  return (
    <NamespacesConsumer>
      {t => (
        <>
          {isDebounce ? (
            <Input
              type={type}
              disabled={disabled}
              placeholder={t(placeholder)}
              defaultValue={defaultValue}
              onChange={e => onChangeValue(e.target.value, name)}
              className={`input-control ${classes}`}
              onFocus={() => toggleBorder(true)}
              onBlur={() => toggleBorder(false)}
              style={
                !hasBorder
                  ? { border: '1px solid #cccccc' }
                  : {
                      border: '1px solid #0A9FED',
                      boxShadow: '0 0 0 1px #0A9FED'
                    }
              }
              {...restProps}
            />
          ) : (
            <Input
              type={type}
              disabled={disabled}
              placeholder={t(placeholder)}
              value={defaultValue}
              onChange={e => onChangeValue(e.target.value, name)}
              className={`input-control ${classes}`}
              onFocus={() => toggleBorder(true)}
              onBlur={() => toggleBorder(false)}
              style={
                !hasBorder
                  ? { border: '1px solid #cccccc' }
                  : {
                      border: '1px solid #0A9FED',
                      boxShadow: '0 0 0 1px #0A9FED'
                    }
              }
              {...restProps}
            />
          )}
          {awesomeIcon ? <i className={`${awesomeIcon} field-input-icon`} /> : null}
        </>
      )}
    </NamespacesConsumer>
  );
};

FieldInput.propTypes = {
  classes: string,
  placeholder: string,
  onChangeValue: func,
  defaultValue: oneOfType([string, number]),
  type: string,
  name: string,
  disabled: bool,
  awesomeIcon: string,
  isDebounce: bool
};

FieldInput.defaultProps = {
  isDebounce: false
};

export default memo(FieldInput);
