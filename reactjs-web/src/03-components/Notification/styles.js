const styles = {
  btnNoti: {
    width: 40,
    height: 40,
    '&:hover': {
      cursor: 'pointer',
    },
  },
  noti: {
    top: '2px !important',
    left: '61px !important',
    width: 400,
    border: '1px solid rgba(0, 0, 0, .1)',
    borderRadius: 0,
    boxShadow: '0 0 5px 0 rgba( 0, 0, 0, 0.12)',
    '&:before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: -5,
      right: 74,
      width: 10,
      height: 10,
      borderTop: '1px solid rgba(0, 0, 0, .15)',
      borderLeft: '1px solid rgba(0, 0, 0, .15)',
      backgroundColor: '#fff',
      transform: 'rotate(45deg)',
    },
  },
  notiWrapItem: {
    maxHeight: 461,
    overflowY: 'scroll',
  },
  iconNoti: {
    fontSize: '24px !important',
    color: 'rgba(255,255,255,.5)',
  },
  totalNewNoti: {
    top: 0,
    right: 0,
    width: 18,
    height: 18,
    borderRadius: 18,
    fontSize: 12,
    color: '#fff',
    background: '#FF3B3B',
  },
  item: {
    display: 'flex',
    alignItems: 'center',
    padding: '10px 16px',
    borderBottom: '1px solid #D2D2D2',
    whiteSpace: 'normal',
    background: '#F5F5F5',
    transition: 'all 0.3s',
    '&:hover': {
      background: '#f2f2f2',
    },
    '&:active': {
      background: '#fff',
    },
    '&:focus': {
      outline: 'none',
    },
  },
  avatar: {
    width: '54px',
    height: '54px',
    borderRadius: '50%',
  },
  message: {
    display: '-webkit-box',
    height: '36px',
    overflow: 'hidden',
    marginBottom: '2px',
    WebkitLineClamp: 2,
    WebkitBoxOrient: 'vertical',
    fontSize: '13px',
    lineHeight: '18px',
    color: '#2c2c2c',
  },
  note: {
    fontWeight: 'bold',
  },
  time: {
    maxWidth: '254px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    fontSize: '13px',
    lineHeight: '18px',
    whiteSpace: 'nowrap',
    color: '#9b9b9b',
  },
  viewMore: {
    paddingTop: '4px',
    textAlign: 'center',
    fontSize: '14px',
    lineHeight: '20px',
    color: '#0976d3',
    '&:focus': {
      outline: 'none',
    },
    '&:hover': {
      color: '#0066ff',
      cursor: 'pointer'
    }
  },
  noNotify: {
    padding: '5px'
  },
  read: {
    background: '#fff',
  },
  noRead: {
    background: '#edf2fa',
  },
  headerDropdownMenu: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: '8px',
    marginLeft: '10px',
    marginRight: '10px',
    borderBottom: '1px solid #D2D2D2'
  }
};

export default styles;
