import imgAvt from '../../06-assets/default_avatar.png';
import AppText from '../AppText';
import withStyles from 'react-jss';
import styles from './styles';
import React, { Fragment, memo, useState, useEffect } from 'react';
import { number, array, object, func, string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { NamespacesConsumer } from 'react-i18next';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { PAGINATE_NOTIFICATION_LIMIT } from '../../05-utils/commonData';
import { calculateLength, linkImage, fromNow } from '../../08-helpers/common';
import {
  getMyNotifications,
  loadMoreMyNotifications,
  readNotification,
  readAllNotifications
} from '../../01-actions/notification';
import { switchSprintProject } from '../../01-actions/sprint';
import { switchProjectGroup } from '../../01-actions/group';
import { GET_GROUP_SUCCESS } from '../../07-constants/group';
import { setSelectedSettingTab, switchProject } from '../../01-actions/project';
import {
  TYPE_TASK_STATE_CHANGE,
  TYPE_PROJECT_ROLE_CHANGE,
  TYPE_NEW_TASK,
  TYPE_REMOVE_TASK,
  TYPE_TIME_ACTIVE_SPRINT_CHANGE,
  TYPE_NEW_COMMENT,
  TYPE_ASSIGN_TASK_PROJECT
} from '../../07-constants/notification';

const redirectNotify = (
  n,
  history,
  location,
  projects,
  projectGroups,
  readNotification,
  projectGroupsStatus,
  switchProject,
  switchProjectGroup,
  switchSprintProject,
  setSelectedSettingTab
) => {
  const data = JSON.parse(n.data);

  // get project id by assign task notify
  const assignTaskProjectId = data.task && data.task.project && data.task.project.id;
  const projectAssignTask = projects.find(p => p.id === assignTaskProjectId);

  // get project id by comment to task notify
  const commentTaskProjectId = data.task && data.task.projectId;
  const projectCommentTask = projects.find(p => p.id === commentTaskProjectId);

  // get selected group project by groupid when user get notify from project page
  const groupId = n.project && n.project.groupId;
  const projectId = n.project && n.project.id;
  const selectedProjectGroup = projectGroups.find(g => g.id === groupId);
  const project = projects.find(p => p.id === projectId);
  const projectRoleUrl = `/projects/${groupId}/${projectId}/users-roles`;

  if (data) {
    switch (n.type) {
      case TYPE_PROJECT_ROLE_CHANGE:
        if (groupId && projectId) history.push(projectRoleUrl);
        if (location.pathname.includes('projects') && groupId && selectedProjectGroup) {
          switchProjectGroup(selectedProjectGroup);
          switchProject(project);
          projectGroupsStatus === GET_GROUP_SUCCESS && setSelectedSettingTab(2);
        }
        break;
      case TYPE_ASSIGN_TASK_PROJECT:
        if (data.task && data.task.project && data.task.project.id && data.task.id)
          history.push(`/sprints/${data.task.project.id}/board/1/${data.task && data.task.id}`);
        if (location.pathname.includes('sprints') && projects && assignTaskProjectId)
          switchSprintProject(projectAssignTask);
        break;
      case TYPE_TASK_STATE_CHANGE:
      case TYPE_NEW_COMMENT:
        if (data.task && data.task.projectId && data.task.id)
          history.push(`/sprints/${data.task && data.task.projectId}/board/1/${data.task && data.task.id}`);
        if (location.pathname.includes('sprints') && projects && commentTaskProjectId)
          switchSprintProject(projectCommentTask);
        break;
      case TYPE_NEW_TASK:
        if (data && data.task && data.task.project && data.task.project.id)
          history.push(`/sprints/${data.task.project.id}/board/1`);
        break;
      case TYPE_TIME_ACTIVE_SPRINT_CHANGE:
        if (n.sprint && n.sprint.project && n.sprint.project.id)
          history.push(`/sprints/${n.sprint.project.id}/board/1`);
        break;
      default:
        break;
    }
  }
  !n.isRead && readNotification(n.id);
};

const _renderNotifyItem = (
  n,
  i,
  t,
  classes,
  history,
  location,
  projects,
  projectGroups,
  readNotification,
  projectGroupsStatus,
  switchProject,
  switchProjectGroup,
  switchSprintProject,
  setSelectedSettingTab
) => (
  <DropdownItem
    key={i}
    className={`${classes.item} ${n.isRead ? classes.read : classes.noRead} cursor-pointer`}
    onClick={() =>
      redirectNotify(
        n,
        history,
        location,
        projects,
        projectGroups,
        readNotification,
        projectGroupsStatus,
        switchProject,
        switchProjectGroup,
        switchSprintProject,
        setSelectedSettingTab
      )
    }
  >
    <div className="mr-2">
      <img
        className={classes.avatar}
        src={n.actorAs && n.actorAs.avatar ? linkImage(n.actorAs.avatar) : imgAvt}
        alt="avatar"
      />
    </div>
    <div>
      <div className={classes.message}>
        <span className={'mr-1 ' + classes.note}>{n.actorAs && n.actorAs.name}</span>
        {n.type === TYPE_TASK_STATE_CHANGE && (
          <span>
            {t('updated_status_of_task')} <strong>{n.task && n.task.content}</strong>
          </span>
        )}

        {n.type === TYPE_PROJECT_ROLE_CHANGE && (
          <span>
            {t('changed_your_role_to_access_by_project')} <strong>{n.project && n.project.name}</strong>
          </span>
        )}

        {n.type === TYPE_NEW_TASK && (
          <span>
            {t('added_a_new_task_into_current_sprint_of_project')} <strong>{n.project && n.project.name}</strong>
          </span>
        )}

        {n.type === TYPE_REMOVE_TASK && (
          <span>
            {t('removed_a_task_out_current_sprint_of_project')} <strong>{n.project && n.project.name}</strong>
          </span>
        )}

        {n.type === TYPE_TIME_ACTIVE_SPRINT_CHANGE && (
          <span>
            {t('updated_new_state_of_sprint_into_project')} <strong>{n.project && n.project.name}</strong>
          </span>
        )}

        {n.type === TYPE_NEW_COMMENT && (
          <span>
            {t('commented_into_task')} <strong>{n.task && n.task.content}</strong>
          </span>
        )}

        {n.type === TYPE_ASSIGN_TASK_PROJECT && (
          <span>
            {t('assign_you_to_task')} <strong>{n.task && n.task.content}</strong>
          </span>
        )}
      </div>
      <div className={classes.time}>{fromNow(n.createdAt)}</div>
    </div>
  </DropdownItem>
);

const Notification = ({
  classes,
  notifications,
  totalNewNotification,
  total,
  readAllNotifications,
  getMyNotifications,
  loadMoreMyNotifications,
  history,
  location,
  projects,
  projectGroups,
  readNotification,
  projectGroupsStatus,
  switchProject,
  switchProjectGroup,
  switchSprintProject,
  setSelectedSettingTab
}) => {
  const limit = PAGINATE_NOTIFICATION_LIMIT;
  const [offset, setOffset] = useState(0);

  const loadNotify = () => {
    getMyNotifications({ limit, offset: 0 });
    setOffset(0);
  };

  const loadMore = () => {
    setOffset(limit + offset);
    loadMoreMyNotifications({ limit, offset: limit + offset });
  };

  useEffect(() => {
    loadNotify();
  }, []);

  return (
    <NamespacesConsumer ns="translations">
      {t => (
        <Fragment>
          <UncontrolledDropdown>
            <DropdownToggle
              nav
              className={'d-flex justify-content-center align-items-center position-relative mr-3 ' + classes.btnNoti}
              onClick={() => loadNotify()}
            >
              {totalNewNotification > 0 && (
                <div
                  className={
                    'd-flex justify-content-center align-items-center position-absolute ' + classes.totalNewNoti
                  }
                >
                  {totalNewNotification > 100 ? '99+' : totalNewNotification}
                </div>
              )}
              <i className={'fa fa-bell ' + (totalNewNotification > 0 ? 'text-white ' : '') + classes.iconNoti} />
            </DropdownToggle>
            <DropdownMenu right className={classes.noti}>
              <div className={classes.notiWrapItem}>
                <div className={classes.headerDropdownMenu}>
                  <AppText>{t('Notification')}</AppText>
                  <AppText
                    onClick={() => {
                      totalNewNotification > 0 && readAllNotifications();
                    }}
                  >
                    <span className={classes.viewMore + ' cursor-pointer'}>{t('read_all_notification')}</span>
                  </AppText>
                </div>
                {calculateLength(notifications) ? (
                  notifications.map((n, i) =>
                    _renderNotifyItem(
                      n,
                      i,
                      t,
                      classes,
                      history,
                      location,
                      projects,
                      projectGroups,
                      readNotification,
                      projectGroupsStatus,
                      switchProject,
                      switchProjectGroup,
                      switchSprintProject,
                      setSelectedSettingTab
                    )
                  )
                ) : (
                  <div className="text-center">
                    <span className={classes.noNotify}>{t('no_notifications')}</span>
                  </div>
                )}
                {calculateLength(notifications) < total ? (
                  <div className={classes.viewMore} onClick={() => loadMore()}>
                    {t('view_more')}
                  </div>
                ) : null}
              </div>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Fragment>
      )}
    </NamespacesConsumer>
  );
};

Notification.propTypes = {
  history: object,
  classes: object,
  notifications: array,
  total: number,
  totalNewNotification: number,
  getMyNotifications: func,
  loadMoreMyNotifications: func,
  readNotification: func,
  readAllNotifications: func,
  projects: array,
  projectGroups: array,
  projectGroupsStatus: string,
  location: object,
  switchProjectGroup: func,
  switchSprintProject: func,
  setSelectedSettingTab: func,
  switchProject: func
};

Notification.defaultProps = {};

const mapStateToProps = state => ({
  notifications: state.notification.notifications,
  total: state.notification.total,
  totalNewNotification: state.notification.totalNewNotification,
  projects: state.project.projects,
  projectGroupsStatus: state.group.status,
  projectGroups: state.group.projectGroups
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getMyNotifications,
      loadMoreMyNotifications,
      readNotification,
      readAllNotifications,
      switchSprintProject,
      switchProjectGroup,
      setSelectedSettingTab,
      switchProject
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(memo(Notification)))
);
