import './style.css';
import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toggleLight } from '../../01-actions/common';
import { func, number } from 'prop-types';
import { setKey } from '../../08-helpers/storage';
import { LIGHT_BACKGROUND_TOGGLE } from '../../07-constants/common';
import { STATUS_LIGHT } from '../../05-utils/commonData';

const Light = ({ status, _toggleLight }) => {
  const toggleLight = () => {
    if (!status) {
      setKey(LIGHT_BACKGROUND_TOGGLE, STATUS_LIGHT.ON);
      _toggleLight(STATUS_LIGHT.ON);
      document.body.classList.remove('bg-black');
    }
    if (status === STATUS_LIGHT.ON) {
      setKey(LIGHT_BACKGROUND_TOGGLE, STATUS_LIGHT.OFF);
      _toggleLight(STATUS_LIGHT.OFF);
      document.body.classList.add('bg-black');
    }
    if (status === STATUS_LIGHT.OFF) {
      setKey(LIGHT_BACKGROUND_TOGGLE, STATUS_LIGHT.ON);
      _toggleLight(STATUS_LIGHT.ON);
      document.body.classList.remove('bg-black');
    }
  };

  const init = () => {
    if (status === STATUS_LIGHT.ON) {
      document.body.classList.remove('bg-black');
    }
    if (status === STATUS_LIGHT.OFF) {
      document.body.classList.add('bg-black');
    }
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <span className={'light-background-toggle'} onClick={toggleLight}>
      <i className={`fa fa-lightbulb-o ${status === STATUS_LIGHT.OFF ? 'off' : 'on'}`} />
    </span>
  );
};

Light.propTypes = {
  _toggleLight: func,
  status: number
};

Light.defaultProps = {};

const mapStateToProps = state => ({
  status: state.common.toggleLight
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      _toggleLight: toggleLight
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(memo(Light));
